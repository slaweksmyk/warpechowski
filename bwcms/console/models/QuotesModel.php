<?php

/**
 * Model Quotes
 * 
 * Management and downloaded data from API with method:
 * getApi, saveApi, updateApi
 *
 * @author Tomasz Zalucki <tomek@fuleo.pl >
 * @version 1.0.0 2017
 */

namespace console\models;

use Yii;
use SoapClient;
use common\modules\quotes\models\Quotes;

class QuotesModel extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'xmod_quotes';
    }

    public function getParamApi() {
        return Yii::$app->params['oConfig'];
    }

    /**
     * getApi.
     * @param string $method Name of method from API
     * @param string record defined in API CRM array output data
     * @param array $params Param to method
     * @return array
     */
    public function getApi($method, $record, $params = false) {
        try {
            $client = new SoapClient($this->getParamApi()->api_url);
            $apiSet = ['methodName' => $method];
            if ($params) {
                $apiParams = ['params' => $params];
                $apiSet = array_merge($apiSet, $apiParams);
            }
            $apiSet = json_encode($apiSet);
            if ($method) {
                $result = $client->getCallmethod($this->getParamApi()->api_key, $apiSet);
                $json_string = json_decode($result, JSON_PRETTY_PRINT);
                if (isset($json_string['errors'])) {
                    return $json_string;
                } else {
                    return $json_string['result'][$record];
                }
            } else {
                return false;
            }
        } catch (exception $e) {
            
        }
    }

    public function saveApi($row) {
        /*
         * Save new model
         */
        $object = new Quotes();

        $object->name = $row['name'];
        $object->bid = $row['bid'];
        $object->ask = $row['ask'];
        $object->mid = $row['mid'];
        $object->time = $row['time'];
        $object->date_created = Yii::$app->formatter->asTime('now', 'yyyy-MM-dd HH:mm:ss');
        $object->save();
    }

    public function updateApi($row) {
        /*
         * Update created models
         * Create new model when model is emtpy
         */
        $object = Quotes::find()->where(['name' => $row['name']])->one();

        if ($object) {
            
            if ((intval(strtotime($row['time'])) - intval(strtotime($object->time)) > 0)) {
                
                
                $object->name           = $row['name'];
                $object->bid            = $row['bid'];
                $object->ask            = $row['ask'];
                $object->mid            = $row['mid'];
                $object->time           = $row['time'];
                $object->date_updated   = Yii::$app->formatter->asTime('now', 'yyyy-MM-dd HH:mm:ss');
                $object->update();
                
                
                return 'Zaktualizowano: '.$object->name;
                
                
            }else{
                return 'Nie zaktualizowano: '.$object->name;
            }
            
        } else {
            $this->saveApi($row);
        }
    }

    public function updateAllApi() {
        /*
         * Search all apartaments and is_active record change velue 0
         * We don't show investments on site when investments are not active in crm api
         */
        $this->updateAll(['is_active' => 0]);
    }

}
