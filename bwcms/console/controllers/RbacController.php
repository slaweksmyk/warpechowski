<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
/**
 * Init RBAC , write to console: php yii rbac/init
 */
class RbacController extends Controller {

    public function actionInit() {
        $auth = Yii::$app->authManager;
        
        $auth->removeAll(); //Remove old data for table
        
        //Create rules admin, editor, user
        $admin = $auth->createRole('admin');
        $editor = $auth->createRole('editor');
        $user = $auth->createRole('user');
        
        // save to DB
        $auth->add($admin);
        $auth->add($editor);
        $auth->add($user);
        
        // Create permission. For example, viewing the admin panel viewAdminPage and edit pages of website updatePages
        $viewAdminPage = $auth->createPermission('adminPanelView');
        $viewAdminPage->description = 'Access to the admin panel';
        
        $updatePages = $auth->createPermission('adminPagesUpdate');
        $updatePages->description = 'Edit website pages';
        
        // Save permissions to DB
        $auth->add($viewAdminPage);
        $auth->add($updatePages);
        
        // For the role of the 'editor' we will add a resolution: $viewAdminPage,
        // For the role of the 'admin' we will add a resolution: $viewAdminPage, $updatePages
        // For the role of the 'user' we will add a resolution: none
        
        $auth->addChild($editor, $viewAdminPage);

        $auth->addChild($admin, $editor);
        
        $auth->addChild($admin, $updatePages);


        $auth->assign($admin, 1);
        $auth->assign($editor, 2);
        $auth->assign($user, 3);
    }
}