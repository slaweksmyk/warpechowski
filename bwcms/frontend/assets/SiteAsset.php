<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class SiteAsset extends AssetBundle
{
    public $sourcePath = '@frontend/assets';
    public $depends = [
        'frontend\assets\AppAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}
