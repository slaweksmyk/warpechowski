<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    
    /* Default */
    public $css = [
        "css/library.bundle.css",
        "css/bundle.css",
    ];
    public $js = [
        "js/cookies.js",
        "js/library.bundle.js",
        "js/d3slider.js",
        "js/bundle.js",
    ];

    
    /* Tablet only */
    public $tabletCss = [

    ];
    public $tabletJs = [
        
    ];
    
    
    /* Mobile only */
    public $mobileCss = [

    ];
    public $mobileJs = [
        
    ];
    
    
    public $depends = [
        'yii\web\YiiAsset',
    ];
    
    public function init(){
        parent::init();
        
        if(\Yii::$app->devicedetect->isMobile() && !\Yii::$app->devicedetect->isTablet()){
            $this->css = (!empty($this->mobileCss))? $this->mobileCss : $this->css; 
            $this->js = (!empty($this->mobileJs))? $this->mobileJs : $this->js; 
        } else if(\Yii::$app->devicedetect->isTablet()){
            $this->css = (!empty($this->tabletCss))? $this->tabletCss : $this->css; 
            $this->js = (!empty($this->tabletJs))? $this->tabletJs : $this->js; 
        }
        
    }
    
}
