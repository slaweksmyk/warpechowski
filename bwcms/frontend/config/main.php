<?php

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'redirectSystem', 'languageSetter', 'ajaxHandler', 'shopReminders'],
    'controllerNamespace' => 'frontend\controllers',
    'homeUrl' => '/',
    'components' => [
        'assetManager' => [
            'appendTimestamp' => true,
        ],
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl' => '/',
            'class' => 'common\hooks\yii2\base\Request',
        ],
        'user' => [
            'identityClass' => 'common\modules\users\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            'name' => 'bwcms_admin',
            'cookieParams' => [
                'httpOnly' => true,
               // 'secure' => true,
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'baseUrl' => '/',
            'suffix' => '/',
            'rules' => [
                ['pattern' => '', 'route' => 'site/index'],
                ['pattern' => '<category>/<slug>', 'route' => 'site/view', 'suffix' => ''],
                ['pattern' => '<category>/<subcategory>/<slug>', 'route' => 'site/view', 'suffix' => ''],
                ['pattern' => '<icategory>/<subcategory>/<category>/<slug>', 'route' => 'site/view', 'suffix' => ''],
                ['pattern' => '<slug>', 'route' => 'site/view', 'suffix' => '']
            ]
        ],
        'languageSetter' => [
            'class' => 'frontend\components\LanguageSetter',
        ],
        'shopReminders' => [
            'class' => 'frontend\components\ShopReminders',
        ],
        'redirectSystem' => [
            'class' => 'common\components\RedirectSystem',
        ],
        'ajaxHandler' => [
            'class' => 'frontend\components\AjaxHandler',
        ],
    ],
    'params' => $params,
    'modules' => [
    //'gii' => [ 'class' => 'yii\gii\Module', 'allowedIPs' => ['*'] ],
    ]
];
