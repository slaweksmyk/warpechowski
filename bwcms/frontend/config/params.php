<?php
return [
    'adminEmail' => 'info@laboratoriumartystyczne.pl',
    
    /*
     * Custom cache system
     */
    'pageCacheSystem' => 
    [
        'enabled' => true,
        'cacheTime' => 5, // in secounds
        'cacheDir' => "/cache/pages/"
    ]
];
