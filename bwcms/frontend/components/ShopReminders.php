<?php

/**
 * PHP version 7
 *
 * @author Throk <k.borecki@throk.pl>
 * @copyright (c) 2018 Throk
 * @version 1.0
 */
namespace frontend\components;

use Yii;
use yii\base\Component;
use common\modules\shop\models\Cart;

class ShopReminders extends Component
{
    
    /**
     * The constructor.
     */
    public function init(){
        parent::init();
        
        $oCartRowset = Cart::find()->where(["IS NOT", "user_id", NULL])->andWhere([">", "TIMESTAMPDIFF(HOUR,`create_date`,`update_date`)", 48])->andWhere(["=", "is_reminded", 0])->all();
        
        foreach($oCartRowset as $oCart){
            $oUser = $oCart->getUser()->one();
            if (strpos($oUser->email, '@') !== false) {
                Yii::$app->mailer->compose()
                    ->setTo($oUser->email)
                    ->setSubject("Porzucony koszyk")
                    ->setTextBody("Porzuciłeś koszyk o nazwie: {$oCart->name}")
                    ->send();
                    
                $oCart->is_reminded = 1;
                $oCart->save();
            }
        }
        
    }
    
}