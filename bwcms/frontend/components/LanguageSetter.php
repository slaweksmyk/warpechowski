<?php

/**
 * Language Setter
 * 
 * Set application language
 * 
 * PHP version 7
 *
 * @author Throk <k.borecki@throk.pl>
 * @copyright (c) 2018 Throk
 * @version 1.0
 */
namespace frontend\components;
 
use Yii;
use yii\base\Component;
use common\modules\pages\models\PagesSite_i18n;
use common\modules\articles\models\Article_i18n;
 
class LanguageSetter extends Component
{
    
    private $aFileConfig;
    
    /**
     * The constructor.
     * Is setting language into default Yii language app
     */
    public function init()
    {
        parent::init();
        
        $this->aFileConfig = yii\helpers\ArrayHelper::merge([], require Yii::getAlias('@common')."/config/i18n.php");
        $oConfig = Yii::$app->params['oConfig'];
        
        $sUrlLanguage = $this->getLanguageFromUrl();
        if($sUrlLanguage != ""){
            /* Language is set in URL */
            $this->setCurrentLanguage($sUrlLanguage);

            if(Yii::$app->request->referrer){
                $aUrlData = parse_url(Yii::$app->request->referrer);
                $sUrl = trim($aUrlData["path"], "/");
                $aUrl = explode("/", $sUrl);
                
				if(!isset($_GET["set"])){
					if($aUrl[0] != $this->getShortFromLanguage($sUrlLanguage)){
						$this->detectWhereToRedirect($oConfig, $sUrlLanguage);
					}
				}
            }
        } else {
            /* Set default language */
            $this->setCurrentLanguage($oConfig->default_language);
        }
    }

    /**
     * Detect where to redirect after changing language
     * @param \common\modules\config\models\Config $oConfig
     * @param string $sUrlLanguage
     */
    private function detectWhereToRedirect(\common\modules\config\models\Config $oConfig, string $sUrlLanguage)
    {
        $sUrlToRedirect = "";

        $sNewUrl = $this->getNewPage($sUrlLanguage);
        if(substr(Yii::$app->request->referrer, -1) != "/"){
            $sNewUrl = $this->getNewArticle($sUrlLanguage, $sNewUrl);
        }

        if($sUrlLanguage == $oConfig->default_language){
            $sUrlToRedirect = "/";
            $sUrlToRedirect .= $sNewUrl;
        } else {
            $sShortUrlLanguage = $this->getShortFromLanguage($sUrlLanguage);

            if($sNewUrl != ""){
                $sUrlToRedirect = "/{$sShortUrlLanguage}/{$sNewUrl}";
            }
        }
        
        
        if($sUrlToRedirect != ""){
            $this->redirectToUrl($sUrlToRedirect);
        }
    }
    
    /**
     * Return new page url
     * @param string $sUrlLanguage
     * @return string
     */
    private function getNewPage(string $sUrlLanguage) : string
    {
        $sSlug = "";
        $sArticleSlug = "";
        $sLastLanguage = "";
        
        $aUrlData = parse_url(Yii::$app->request->referrer);
        $sUrl = trim($aUrlData["path"], "/");
        $aUrl = explode("/", $sUrl);
        
        /* Detect if the page was in other than default language - get slug and language */
        if(is_array($aUrl) && count($aUrl) > 1){
            $sSlug = $aUrl[1];
            $sLastLanguage = $this->getLanguageFromShort($aUrl[0]);
        }

        if($sLastLanguage != "" && $sSlug != ""){
            $oCurrentPage = PagesSite_i18n::find()->where(["=", "url", $sSlug])->andWhere(["=", "language", $sLastLanguage])->one();
        } else {
            $aUrl = explode("/", $sUrl);
            if(count($aUrl) == 2){
                $sArticleSlug = $aUrl[1];
                $oCurrentPage = PagesSite_i18n::find()->where(["=", "url", $aUrl[0]])->one();
            } else {
                $oCurrentPage = PagesSite_i18n::find()->where(["=", "url", $sUrl])->one();
            }
        }
        
        if($oCurrentPage){
            $oNewPage = PagesSite_i18n::find()->where(["=", "page_site_id", $oCurrentPage->page_site_id])->andWhere(["=", "language", $sUrlLanguage])->one();
            return "{$oNewPage->url}/{$sArticleSlug}";
        }
        
        return "";
    }
    
    /**
     * Return new page url
     * @param string $sUrlLanguage
     * @param string $sPageUrl
     * @return string
     */
    private function getNewArticle(string $sUrlLanguage, string $sPageUrl) : string
    {
        $sPageSlug = "";
        $sArticleSlug = "";
        $sLastLanguage = "";

        $aUrl = explode("/", $sPageUrl);
        if(count($aUrl) == 2){
            $sPageSlug = $aUrl[0];
            $sArticleSlug = $aUrl[1];
        }
        
        $aUrlData = parse_url(Yii::$app->request->referrer);
        $aUrl = explode("/", trim($aUrlData["path"], "/"));
        if(is_array($aUrl) && count($aUrl) > 1){
            $sArticleSlug = end($aUrl);
            $sLastLanguage = $this->getLanguageFromShort($aUrl[0]);
        }

        if($sLastLanguage != "" && $sArticleSlug != ""){
            $oCurrentArticle = Article_i18n::find()->where(["=", "slug", $sArticleSlug])->andWhere(["=", "language", $sLastLanguage])->one();
        } else {
            $oCurrentArticle = Article_i18n::find()->where(["=", "slug", $sArticleSlug])->one();
        }

        if($oCurrentArticle){
            $oNewArticle = Article_i18n::find()->where(["=", "article_id", $oCurrentArticle->article_id])->andWhere(["=", "language", $sUrlLanguage])->one();
            if($oNewArticle){
                return "{$sPageSlug}/{$oNewArticle->slug}";
            } else {
                return "{$sPageSlug}/";
            }
        } else {
            /* Fix for products */
            return "{$sPageSlug}/{$sArticleSlug}";
        }

        return $sPageUrl;
    }
    
    
    /***************** Tools ******************/

    /**
     * Returns current language
     * @return String
     */
    private function getCurrentLanguage()
    {
        return Yii::$app->session->get('frontend_language');
    }
    
    /**
     * Set current application language
     * @param string $sLang
     */
    private function setCurrentLanguage(string $sLang)
    {
        \Yii::$app->language = Yii::$app->session->set('frontend_language', $sLang);
    }
    
    /**
     * Redirects to url
     * @param string $sUrl
     */
    private function redirectToUrl(string $sUrl)
    {
        Yii::$app->getResponse()->redirect($sUrl."?set=true")->send();
        Yii::$app->end();
    }
    
    /**
     * Return language set in URL
     * @return string
     */
    private function getLanguageFromUrl() : string
    {
        $aUrl  = parse_url(Yii::$app->request->absoluteUrl);
        $aPath = array_filter(explode("/", $aUrl["path"]));
        $sShortUrlLanguage = current($aPath);
        
        $sFullLang = $this->getLanguageFromShort($sShortUrlLanguage);
        return $sFullLang != "" ? $sFullLang : "";
    }
    
    /**
     * Get full language from short one
     * @param string $sShortLanguage
     * @return string
     */
    private function getLanguageFromShort(string $sShortLanguage) : string
    {
        foreach($this->aFileConfig["languages"] as $sConfigLanguage){
            $sShortConfigLanguage = $this->getShortFromLanguage($sConfigLanguage);
            
            if($sShortConfigLanguage == $sShortLanguage){
                return $sConfigLanguage;
            }
        } 
         return "";       
    }
    
    /**
     * Return short language code from full language
     * @param string $sLanguage
     * @return string
     */
    private function getShortFromLanguage(string $sLanguage) : string
    {
        return strtolower(current(explode("-", $sLanguage)));
    }
    
}