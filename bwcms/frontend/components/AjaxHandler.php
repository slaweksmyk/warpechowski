<?php

/**
 * PHP version 7
 *
 * @author Throk <k.borecki@throk.pl>
 * @copyright (c) 2016 Throk
 * @version 1.0
 */

namespace frontend\components;

use Yii;
use yii\base\Component;
use common\modules\articles\models\ArticleComment;
use common\helpers\TranslateHelper;
use common\modules\articles\models\Article;

class AjaxHandler extends Component {

    /**
     * The constructor.
     */
    public function init() {

        if (Yii::$app->request->isAjax) {
            if (Yii::$app->request->get('funct')) {
                header('Content-type: application/json');
                $this->{Yii::$app->request->get('funct')}();

                Yii::$app->end();
                exit;
            }
        }

        parent::init();
    }
    
    private function checkDate() {
        
        $min = Yii::$app->request->get('min');
        $max = Yii::$app->request->get('max');
        $search = Yii::$app->request->get('search');
        
        //$oArticleRowset = Article::find()->where(['<=', 'year', $min])->andWhere(['>=', 'year', $max])->andWhere(['=', "status_id", 2])->andWhere(['LIKE', "title", $search])->orderBy('id ASC')->all();
        
        $oArticleRowsetQuery = Article::find()->where(['=', 'category_id', 2])->andWhere(['=', "status_id", 2]);
        
        if($search){
            $oArticleRowsetQuery->andWhere(['LIKE', "title", $search]);
        }
        
        if($min){
            $oArticleRowsetQuery->andWhere(['>=', 'year', $min]);
        }
        
        if($max){
            $oArticleRowsetQuery->andWhere(['<=', 'year', $max]);
        }
        
        $oArticleRowset = $oArticleRowsetQuery->orderBy('id ASC')->all();
        $data = '';
        foreach($oArticleRowset as $index => $oArticle){
            if ($oArticle->is_more_info == 1) {
                $data .= '<li data-number="#'.$oArticle->number.'" class="is_more_info">
                    <a href="'.$oArticle->getAbsoluteUrl().'" title="'.TranslateHelper::t('GO_TO').$oArticle->title.'">
                        <h3>'.$oArticle->title.'</h3><br />
                        <p>'.$oArticle->place.', '.$oArticle->city.', '.$oArticle->date_text.' '.$oArticle->year.'</p>
                    </a>
                </li>';
            } else {
                $data .= '<li data-number="#'.$oArticle->number.'" class="">
                    <h3>'.$oArticle->title.'</h3><br />
                    <p>'.$oArticle->place.', '.$oArticle->city.', '.$oArticle->date_text.' '.$oArticle->year.'</p>
                </li>';
            }
        }
        
        echo json_encode(['data' => str_replace('\n', '', $data)]);
    
    }

    private function submitComment() {
        $oComment = new ArticleComment();
        $oComment->parent_id = Yii::$app->request->post('parentID', NULL);
        $oComment->article_id = Yii::$app->request->post('articleID', NULL);
        $oComment->text = Yii::$app->request->post('text', NULL);
        $oComment->author = Yii::$app->request->post('author', NULL);
        $oComment->is_active = 0;
        $oComment->date_created = date("Y-m-d H:i:s");
        $oComment->save();

        echo json_encode(["status" => "OK"]);
    }

    private function acceptComment() {
        $oComment = ArticleComment::find()->where(["=", "id", Yii::$app->request->post('iCommentID')])->one();
        $oComment->is_active = 1;
        $oComment->save();

        echo json_encode(["status" => "OK"]);
    }

    private function banComment() {
        $oComment = ArticleComment::find()->where(["=", "id", Yii::$app->request->post('iCommentID')])->one();
        $oComment->is_active = -1;
        $oComment->save();

        echo json_encode(["status" => "OK"]);
    }

    private function moderateComment() {
        $oComment = ArticleComment::find()->where(["=", "id", Yii::$app->request->post('iCommentID')])->one();
        $oComment->is_active = 0;
        $oComment->save();

        echo json_encode(["status" => "OK"]);
    }

}
