<?php

/**
 * Prepare meta tags
 * 
 * PHP version 7
 *
 * @author Throk <k.borecki@throk.pl>
 * @copyright (c) 2016 Throk
 * @version 1.0
 */
namespace frontend\components;
 
use Yii;
use yii\web\View;
use yii\helpers\Url;
use yii\base\Component;

use common\modules\files\models\File;
use common\modules\shop\models\Product;
use common\modules\articles\models\Article;
use common\modules\pages\models\PagesSite;

class PrepareMetaTags extends Component
{
    public static $metaTags = [];
    public static $linkTags = [];
    public static $title;

    /**
     * The constructor.
     */
    public function init(){
        parent::init();
        
        if(isset(Yii::$app->params['oPage'])){
            if(isset(Yii::$app->params['slug'])){
                $oArticle = Article::find()->where(["=", "slug", Yii::$app->params['slug']])->one();
                $oProduct = Product::find()->where(["=", "slug", Yii::$app->params['slug']])->one();

                if($oArticle){
                    $this->prepareMeta($oArticle);
                } else if($oProduct) {
                    $this->prepareMeta($oProduct);
                } else {
                    $this->prepareMeta(Yii::$app->params['oPage']);
                }
            } else {
                $this->prepareMeta(Yii::$app->params['oPage']);
            }
        }
        $this->prepareLanguages();
    }
    
    private function prepareLanguages(){
        $config = yii\helpers\ArrayHelper::merge(
            [], require Yii::getAlias('@common')."/config/i18n.php"
        );

        $aUrl = parse_url(Yii::$app->request->absoluteUrl);
        $aPath = array_filter(explode("/", $aUrl["path"]));
        $aTestLang = current($aPath);

        foreach($config["languages"] as $sLang){
            $sShort = strtolower(current(explode("-", $sLang)));
            $sShortCurrentLang = strtolower(current(explode("-", Yii::$app->session->get('frontend_language'))));

            if($aTestLang == $sShort){
                $aUrl["path"] = str_replace("en/", "", $aUrl["path"]);
            }
        }

        $oConfig = Yii::$app->params['oConfig'];
        foreach($config["languages"] as $sLang){
            $sShort = strtolower(current(explode("-", $sLang)));
            $sShortCurrentLang = strtolower(current(explode("-", $oConfig->default_language)));

            if($sShort == $sShortCurrentLang){
                $sUrl = $aUrl["scheme"]."://".$aUrl["host"].$aUrl["path"];
            } else {
                $sShort = strtolower(current(explode("-", $sLang)));
                $sUrl = $aUrl["scheme"]."://".$aUrl["host"]."/".$sShort.$aUrl["path"];
            }

            Yii::$app->view->registerLinkTag(["rel" => "alternate", "hreflang" => $sLang, "href" => $sUrl]);
        }
    }
	
    private function prepareMeta($oObject){
        $oConfig = Yii::$app->params['oConfig'];

        $sDescription = "";
        $sKeywords = "";
        $sTitle = "";
        $sImage = "";
        
        if(isset($oObject->seo_description) && $oObject->seo_description != ""){
            $sDescription = $oObject->seo_description;
        } else if(isset($oObject->short_description) && $oObject->short_description != ""){
            $sDescription = $oObject->short_description;
        } else if(isset($oObject->description) && $oObject->description != ""){
            $sDescription = $oObject->description;
        }

        if(isset($oObject->seo_keywords)){
            $sKeywords = $oObject->seo_keywords;
        }

        if(isset($oObject->seo_title) && $oObject->seo_title != ""){
            $sTitle = $this->prepareDataToMeta($oObject->seo_title);
        } elseif(isset($oObject->name)){
            $sTitle = $this->prepareDataToMeta($oObject->name);
        } elseif(isset($oObject->title)){
            $sTitle = $this->prepareDataToMeta($oObject->title);
        }
        
        if($oConfig->seo_static_title){
            if(isset($oObject->name)){
                $oObject->name = $sTitle." - {$oConfig->seo_static_title}";
                Yii::$app->params['oPage']->seo_title = $oObject->name;
            } else {
                $oObject->title = $sTitle." - {$oConfig->seo_static_title}";
                Yii::$app->params['oPage']->seo_title = $oObject->title;
            }   
        } else {
            if(isset($oObject->name)){
                $oObject->name = $sTitle;
            } else {
                $oObject->title = $sTitle;
            }
            Yii::$app->params['oPage']->seo_title = $sTitle;
        }
        
        if(isset($oObject->thumbnail_id) && !is_null($oObject->thumbnail_id)){
            $sImage = trim(Url::to("/",true), "/").File::getThumbnail($oObject->thumbnail_id, 1200, 660, "matched");
        }

        $oPagesSite = PagesSite::find()->where(["=", "id", Yii::$app->params['oPage']['id']])->one();
        SELF::$title = $sTitle;
        
        if (isset($oPagesSite['canonical'])) { Yii::$app->view->registerLinkTag(["rel" => "canonical", "href" => $oPagesSite['canonical']]);}

        $sDescription = trim(strip_tags($sDescription));
        
        self::$metaTags[] = ["name" => "generator", "content" => "BlackWolf CMS"];
        self::$metaTags[] = ["name" => "robots", "content" => "noodp, index, follow"];
        self::$metaTags[] = ["name" => "description", "content" => $this->prepareDataToMeta($sDescription)];
        self::$metaTags[] = ["name" => "keywords", "content" => $this->prepareDataToMeta($sKeywords)];
        if($oConfig->seo_gsv != ""){
            self::$metaTags[] = ["name" => "google-site-verification", "content" => $oConfig->seo_gsv];
        }
        
        if($oConfig->seo_ga != ""){
            Yii::$app->view->registerJs(
                "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                ga('create', '{$oConfig->seo_ga}', 'auto');
                ga('send', 'pageview');",
                View::POS_END
            );
        }
        
        if($oConfig->seo_gtm != ""){
            Yii::$app->view->registerJs(
                "(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                })(window,document,'script','dataLayer','{$oConfig->seo_gtm}');",
                View::POS_END
            );
        }
        
        if($oConfig->seo_fb_pixel != ""){
            Yii::$app->view->registerJs(
                "!function(f,b,e,v,n,t,s)
                {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window, document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
                fbq('init', '{$oConfig->seo_fb_pixel}');
                fbq('track', 'PageView');",
                View::POS_END
            );
        }
        
        if($oConfig->custom_scripts_head != ""){
            Yii::$app->view->registerJs($oConfig->custom_scripts_head, View::POS_HEAD);
        }
        
        if($oConfig->custom_scripts_footer != ""){
            Yii::$app->view->registerJs($oConfig->custom_scripts_footer, View::POS_END);
        }
        
        $aData = [
            "title" => $sTitle, 
            "description" => $sDescription, 
            "keywords" => $sKeywords,
            'image' => $sImage
        ];
        
        $this->prepareFacebookMeta($aData, $oConfig);
        $this->prepareTwitterMeta($aData, $oConfig);
    }
    
    private function prepareGooglePlusMeta($aData, $oConfig){
        self::$metaTags[] = ["rel" => "publisher", "href" => $oConfig->seo_gp_profile];
    }
    
    private function prepareTwitterMeta($aData, $oConfig){
        self::$metaTags[] = ["name" => "twitter:card", "content" => "summary_large_image"];
        self::$metaTags[] = ["name" => "twitter:site", "content" => $oConfig->seo_tw_profile];
        self::$metaTags[] = ["name" => "twitter:title", "content" => $aData["title"]];
        self::$metaTags[] = ["name" => "twitter:description", "content" => $aData["description"]];
        self::$metaTags[] = ["name" => "twitter:image:alt", "content" => $aData["title"]];
        self::$metaTags[] = ["name" => "twitter:image", "content" => $aData["image"]];
    }
    
    private function prepareFacebookMeta($aData, $oConfig){
        self::$metaTags[] = ["property" => "og:title", "content" => $aData["title"]];
        self::$metaTags[] = ["property" => "og:description", "content" => $aData["description"]];
        self::$metaTags[] = ["property" => "og:site_name", "content" => $oConfig->page_name];
        self::$metaTags[] = ["property" => "og:url", "content" => Yii::$app->request->absoluteUrl];
        self::$metaTags[] = ["property" => "og:type", "content" => "website"];
        self::$metaTags[] = ["property" => "og:locale", "content" => str_replace("-", "_", Yii::$app->language)];
        self::$metaTags[] = ["property" => "og:image", "content" => $aData["image"]];
        if($oConfig->seo_fb_app != ""){
            self::$metaTags[] = ["property" => "fb:app_id", "content" => $oConfig->seo_fb_app];
        }

        if($oConfig->seo_fb_image){
            $oImage = File::find()->where(["=", "id", $oConfig->seo_fb_image])->one();
            if($oImage){
                self::$metaTags[] = ["property" => "og:image", "content" => Url::to("/",true)."upload/{$oImage->filename}"];
            }
        }
    }
    
    private function prepareDataToMeta($sData){
        return trim(strip_tags($sData));
    }

    public static function register($view){
        foreach(SELF::$metaTags as $aMeta){
            $view->registerMetaTag($aMeta);
        }
        foreach(SELF::$linkTags as $aLink){
            $view->registerLinkTag($aLink);
        }
        return SELF::$title;
    }
    
}