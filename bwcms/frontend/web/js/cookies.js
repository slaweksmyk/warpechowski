var text = "Strona korzysta z plików cookies w celu realizacji usług i zgodnie z Polityką Plików Cookies. Możesz określić warunki przechowywania lub dostępu do plików cookies w Twojej przeglądarce.";
var more = "";

window.onload = initCookiesInfo;

function initCookiesInfo()
{
    var check = getCookie("cookies-accept");
    if(check !== "true"){
        var oBar = prepareBar();

        document.body.appendChild(oBar);
    }
}

function prepareBar(){
    var oText = document.createTextNode(text);
    var oBar  = document.createElement("div");
    var oContainer = document.createElement("div");;
    var oLink = prepareLink();

    oBar.id = "cookie-bar";
    
    oBar.style.width = "100%";
    oBar.style.position = "fixed";
    oBar.style.bottom = "-1px";
    oBar.style.left = "0px";
    oBar.style.background = "#39373f";
    oBar.style.color = "#e4e4e4";
    oBar.style.textAlign = "center";
    oBar.style.fontSize = "12px";
    oBar.style.padding = "3px 10px";
    oBar.style.zIndex = "99999";
    oBar.style.boxShadow = "0 -2px 5px rgba(0,0,0,.1)";
    
    oContainer.style.maxWidth = "1170px";
    oContainer.style.margin = "0 auto";
    oContainer.style.position = "relative";
    
    oBar.appendChild(oContainer);

    oContainer.appendChild(oText);
    if(more !== ""){ oContainer.appendChild(prepareMore()); }
    oContainer.appendChild(oLink);

    return oBar;
}

function prepareMore(){
    var oText = document.createTextNode("Więcej informacji");
    var oLink = document.createElement("a");
    
    oLink.href = more;
    oLink.target = "_blank";
    
    oLink.style.textDecoration = "underline";
    oLink.style.cursor = "pointer";
    oLink.style.marginLeft = "5px";
    
    oLink.appendChild(oText);
    
    return oLink;
}

function prepareLink(){
    var oText = document.createTextNode("×");
    var oLink  = document.createElement("a");
    
    oLink.title = "Zamknij powiadomienie";
    
    oLink.style.cursor = "pointer";
    oLink.style.marginLeft = "5px";
    oLink.style.fontSize = "16px";
    oLink.style.position = "absolute";
    oLink.style.right = "-5px";
    oLink.style.top = "-2px";
    
    oLink.appendChild(oText);
    
    oLink.addEventListener("click", saveCookie);
    
    return oLink;
}

function saveCookie(){
    setCookie("cookies-accept", "true", 256);
    
    var oBar = document.getElementById("cookie-bar");
    fade(oBar);
}

function setCookie(cname, cvalue, exdays){
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function fade(element) {
    var op = 1;
    var timer = setInterval(function () {
        if (op <= 0.1){
            clearInterval(timer);
            element.style.display = 'none';
            element.outerHTML = "";
            delete element;
        }
        element.style.opacity = op;
        element.style.filter = 'alpha(opacity=' + op * 100 + ")";
        op -= op * 0.1;
    }, 20);
}