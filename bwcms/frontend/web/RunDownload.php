<?php
namespace frontend\web;

use Yii;
use yii\web\Application;
use yii\helpers\BaseUrl;
use common\modules\files\models\File;

class RunDownload extends Application
{
    
    /**
     * init application
     */
    public function init()
    {
        parent::init();

        $request = Yii::$app->request;
        $oFile = File::find()->where(["id" => $request->get("id")])->one();
        
        header("Content-Description: File Transfer"); 
        header("Content-Type: {$oFile->type}"); 
        header("Content-Disposition: attachment; filename='{$oFile->name}.{$oFile->getExtension()}'"); 

        readfile(BaseUrl::home(true)."upload/{$oFile->filename}"); 
        exit;
    }

}