<?php

namespace frontend\web;

use Yii;
use yii\helpers\Url;
use yii\web\Application;

use common\modules\pages\models\PagesSite;

use common\modules\articles\models\Article;
use common\modules\categories\models\ArticlesCategories;

use common\modules\shop\models\Product;
use common\modules\shop\models\Category;

use common\modules\tags\models\Tag;

use common\modules\authors\models\Author;

class RunSitemap extends Application
{
    
    /**
     * init application
     */
    public function init()
    {
        parent::init();

        $request = Yii::$app->request;
        $sitemap = new Sitemap();
        switch($request->get('type')){
            case "pages":
                $sitemap->generatePages();
                break;

            case "articles":
                $sitemap->generateArticles();
                break;
            
            case "categories":
                $sitemap->generateCategories();
                break;
            
            case "shop-categories":
                $sitemap->generateShopCategories();
                break;
            
            case "products":
                $sitemap->generateProducts();
                break;
            
            case "tags":
                $sitemap->generateTags();
                break;
            
            case "authors":
                $sitemap->generateAuthors();
                break;


            default:
                $sitemap->generateIndex();
                break;
        }
        exit;
    }
}

class Sitemap 
{
	private $url, $types;

	function __construct(){
            header('Content-type: text/xml');

            $this->url = Url::home(true);
            $this->types = array("pages", "articles", "categories", "products", "shop-categories", "tags", "authors");
	}
	
	public function generateIndex(){
            $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" />');
            foreach($this->types as $type){
                $sitemap = $xml->addChild('sitemap');
                $sitemap->addChild('loc', "{$this->url}{$type}.xml");
            }

            echo $xml->asXML();
	}
	
	public function generatePages(){
            $urlset = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" />');

            $oPageRowset = PagesSite::find()->all();
            foreach($oPageRowset as $oPage){
                if($oPage->is_active){
                    if($oPage->url === "/"){
                        $link = $this->url;
                    } else {
                        $link = "{$this->url}{$oPage->url}/";
                    }

                    $url = $urlset->addChild('url');
                    $url->addChild('loc', $link);
                    $url->addChild('lastmod', date("c", strtotime($oPage->updated_at)));
                }
            }

            echo $urlset->asXML();
	}
	
	public function generateArticles(){
            $urlset = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" />');

            $oArticleRowset = Article::find()->where(["=", "is_active", 1])->all();
            foreach($oArticleRowset as $Article){
                $url = $urlset->addChild('url');
                $url->addChild('loc', $Article->getAbsoluteUrl());
                $url->addChild('lastmod', date("c", strtotime($Article->date_updated)));
            }

            echo $urlset->asXML();
	}
	
	public function generateCategories(){
            $urlset = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" />');

            $oCategoryRowset = ArticlesCategories::find()->where(["=", "is_active", 1])->all();
            foreach($oCategoryRowset as $Category){
                $url = $urlset->addChild('url');
                $url->addChild('loc', $Category->getAbsoluteUrl());
                $url->addChild('lastmod', date("c", strtotime($Category->date_updated)));
            }

            echo $urlset->asXML();
	}
        
	public function generateProducts(){
            $urlset = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" />');

            $oProductRowset = Product::find()->where(["=", "is_active", 1])->all();
            foreach($oProductRowset as $Product){
                $url = $urlset->addChild('url');
                $url->addChild('loc', $Product->getAbsoluteUrl());
                $url->addChild('lastmod', date("c", strtotime($Product->date_updated)));
            }

            echo $urlset->asXML();
	}
        
	public function generateShopCategories(){
            $urlset = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" />');

            $oCategoryRowset = Category::find()->all();
            foreach($oCategoryRowset as $Category){
                $url = $urlset->addChild('url');
                $url->addChild('loc', $Category->getAbsoluteUrl());
                $url->addChild('lastmod', date("c", strtotime($Category->date_updated)));
            }

            echo $urlset->asXML();
	}
        
	public function generateTags(){
            $urlset = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" />');

            $oObjectRowset = Tag::find()->all();
            foreach($oObjectRowset as $Object){
                $url = $urlset->addChild('url');
                $url->addChild('loc', $Object->getAbsoluteUrl());
                $url->addChild('lastmod', date("c", strtotime($Object->date_updated)));
            }

            echo $urlset->asXML();
	}
        
	public function generateAuthors(){
            $urlset = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" />');

            $oObjectRowset = Author::find()->all();
            foreach($oObjectRowset as $Object){
                $url = $urlset->addChild('url');
                $url->addChild('loc', $Object->getAbsoluteUrl());
                $url->addChild('lastmod', date("c", strtotime($Object->date_updated)));
            }

            echo $urlset->asXML();
	}
}