<?php
/**
 * Run Frontend application
 * 
 * Load modules, rules, curent page info and start application
 * 
 * PHP version 7
 *
 * @author Volodymyr Shelelo <contact@profficode.com>
 * @copyright (c) 2016 Volodymyr Shelelo
 * @version 1.0
 */

namespace frontend\web;

use Yii;
use yii\helpers\Url;
use yii\web\Application;
use common\modules\pages\models\PagesSite;

use frontend\components\PrepareMetaTags;

class RunApplication extends Application
{
    
    /**
     * init application
     */
    public function init()
    {
        parent::init();

        if(Yii::$app->request->get('disablePreview')){
            Yii::$app->session->set('preview_mode', false);
            header('Location: '.Url::to("/", true));
            exit;
        }

        $urlConfig = PagesSite::getUrlConfig();
                
        $this->modules = $urlConfig['modules'];
        $this->getUrlManager()->addRules($urlConfig['rules']);
        $page = PagesSite::getByUrl([Yii::$app->getDb()->tablePrefix.'pages_site.id', Yii::$app->getDb()->tablePrefix.'pages_site_i18n.name', Yii::$app->getDb()->tablePrefix.'pages_site_i18n.description', Yii::$app->getDb()->tablePrefix.'pages_site_i18n.slug', Yii::$app->getDb()->tablePrefix.'pages_site_i18n.url', 'module_id', 'layout_id', Yii::$app->getDb()->tablePrefix.'pages_site_i18n.seo_title', Yii::$app->getDb()->tablePrefix.'pages_site_i18n.seo_description', Yii::$app->getDb()->tablePrefix.'pages_site_i18n.seo_keywords']);

        if( $page['page'] && $page['layout'] ){
            $this->params['oPage'] = $page['page'];
            $this->params['oLayout'] = $page['layout'];
            $this->layout = $page['layout']['layout'].'/index'; 
        } else {
            $this->layout = "main/index";
        }
 
        new PrepareMetaTags();
    }

}