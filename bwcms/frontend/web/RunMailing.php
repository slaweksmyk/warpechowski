<?php

namespace frontend\web;

use Yii;
use yii\web\Application;
use common\modules\mailing\models\MailingEmail;
use common\modules\mailing\models\MailingArchive;

class RunMailing extends Application
{
    
    /**
     * init application
     */
    public function init()
    {
        parent::init();
        
        if(Yii::$app->request->get('archive')){
            $oArchive = MailingArchive::findOne(["=", "id", Yii::$app->request->get('archive')]);
            $oArchive->is_read = 1;
            $oArchive->date_view = Yii::$app->formatter->asTime('now', 'yyyy-MM-dd HH:mm:ss');
            $oArchive->save();

            header('Content-Type: image/png');
            header('Content-Length: ' . filesize(Yii::getAlias('@webroot').'/logo.png'));
            readfile(Yii::getAlias('@webroot').'/logo.png');
        }
        
        if(Yii::$app->request->get('unsubscribe')){
            $oEmail = MailingEmail::findOne(["=", "id", Yii::$app->request->get('unsubscribe')]);
            $oEmail->delete();
            
            exit("Zostałeś pomyślnie wypisany z listy mailingowej.");
        }
        
        exit;
    }
}
