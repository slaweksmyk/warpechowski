/* 
 * 
 * Hello World Plugin!
 * 
 * @author Tomasz Załucki <tomek@fuleo.pl>
 * 
 * @version 1.0.0 alpha
 * 
 */

;
(function ($, window, undefined) {

    $.fn.helloworld = function (options) {
        console.log('Hello World!');
    };
 
//    var settings = $.extend({
//        // options
//    }, options);


    $(document).ready(function () {

        $('body').helloworld();

    });

})(jQuery, window);