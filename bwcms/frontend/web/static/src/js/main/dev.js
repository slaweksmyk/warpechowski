/**
 *
 * Dev.js
 *
 */

/**
 * 
 * DOCUMENT READY
 *  
 */

$(document).ready(function () {
    
    // niceScroll init
    if ($(window).width() > 767) {
       $(function () {
           $(".content").niceScroll();
       });
    }
    
    // get cookies
    var fontSize = getCookie("font-size");
    var contrast = getCookie("contrast");
    
    //add class id cookies exists
    if (fontSize) {
        $('body').addClass(fontSize);
    }
    if (contrast) {
        $('body').addClass(contrast);
    }
   
    // if font size changed
    $( ".font-size-item a" ).click(function(event) {
        event.preventDefault();
        
        fontSize = getCookie("font-size");
        
        var newSize = $(this).data('class');
        $('body').removeClass(fontSize);
        
        setCookie("font-size", newSize, 256);
        $('body').addClass(newSize);
        
        return false;
    });
   
    // if contrast changed
    $( ".contrast-item a" ).click(function(event) {
        event.preventDefault();
        
        contrast = getCookie("contrast");
        
        var newContrast = $(this).data('class');
    
        $('body').removeClass(contrast);
        
        setCookie("contrast", newContrast, 256);
        $('body').addClass(newContrast);
        
        return false;
    });
    
    var min = $('input[name=min]').val();
    var max = $('input[name=max]').val();
    var min_start = $('#slider').data('min');
    var max_start = $('#slider').data('max');
    var search = $('input[name=search]').val();
    
    slider(min,max,min_start,max_start);
    
    $('body').on('keyup', '#handle-one', function (event) {
        if (event.keyCode === 37) {
            //console.log('left arrow: '+min);
            
            if (min >= min_start) {
              min--;
              $('#slider').html('');
              slider(min,max,min_start,max_start);
              
                $('input[name=min]').val(min);
                $('input[name=max]').val(max);
                $.get("/", { funct: 'checkDate', min: min, max: max, search: search })
                    .done(function( data ) {
                        var obj = JSON.parse(data);
                        $('.performances-items').html(obj.data);
                    });
              
              $( "#handle-one" ).focus();
            }
        }
    });
          
    $('body').on('keyup', '#handle-one', function (event) {
        if (event.keyCode === 39) {
            //console.log('left arrow: '+min);
            
            if (min <= max_start) {
              min++;
              $('#slider').html('');
              slider(min,max,min_start,max_start);
              
                $('input[name=min]').val(min);
                $('input[name=max]').val(max);
                $.get("/", { funct: 'checkDate', min: min, max: max, search: search })
                    .done(function( data ) {
                        var obj = JSON.parse(data);
                        $('.performances-items').html(obj.data);
                    });
                    
              $( "#handle-one" ).focus();
            }
        }
    });
    
    $('body').on('keyup', '#handle-two', function (event) {
          if (event.keyCode === 37) {
              //console.log('right arrow: '+max);
              
              if (max >= min_start) {
                max--;
                $('#slider').html('');
                slider(min,max,min_start,max_start);

                  $('input[name=min]').val(min);
                  $('input[name=max]').val(max);
                  $.get("/", { funct: 'checkDate', min: min, max: max, search: search })
                      .done(function( data ) {
                          var obj = JSON.parse(data);
                          $('.performances-items').html(obj.data);
                      });

                $( "#handle-two" ).focus();
              }

          }
      });
          
    $('body').on('keyup', '#handle-two', function (event) {
        if (event.keyCode === 39) {
            //console.log('right arrow: '+max);
              
              if (max <= max_start) {
                max++;
                $('#slider').html('');
                slider(min,max,min_start,max_start);

                  $('input[name=min]').val(min);
                  $('input[name=max]').val(max);
                  $.get("/", { funct: 'checkDate', min: min, max: max, search: search })
                      .done(function( data ) {
                          var obj = JSON.parse(data);
                          $('.performances-items').html(obj.data);
                      });

                $( "#handle-two" ).focus();
              }
        }
    });
     

    //Slider performances date
    function slider(min,max,min_start,max_start) { 
        d3.select('#slider').call(
        d3.slider().axis(true).min(min_start).max(max_start).step(1).value( [ min, max ] )
        .on("slide", function(evt, value) {
            console.log(value[ 0 ]+'-'+value[ 1 ]);
            $('input[name=min]').val(value[ 0 ]);
            $('input[name=max]').val(value[ 1 ]);
            $.get("/", { funct: 'checkDate', min: value[ 0 ], max: value[ 1 ], search: search })
                .done(function( data ) {
                    var obj = JSON.parse(data);
                    $('.performances-items').html(obj.data);
                });
            })
    );
    }     
    
    var lightbox = $('.gallery a').simpleLightbox({
        captionSelector: 'self',
        captionType: 'data',
        captionsData: 'caption',
        close: false,
        showCounter: false,
        navText: ['<img src="/img/strzalka-galeria-lewo.png" />','<img src="/img/strzalka-galeria-prawo.png" />']
    });
    
    $('.gallery a').on('shown.simplelightbox', function () {
        var author =  $('.sl-caption span').data('author');
        $('.sl-image').prepend('<iframe width="700" height="394" src="" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
        
        if ($(this).data('video')) {
            $('.sl-image').find('img').css('display', 'none').addClass( 'img-hide' ).removeClass( 'img-show' );
            $('.sl-image').css('width', '700px').css('height', '394px');
            $('.sl-image').find('iframe').attr('src', 'https://www.youtube.com/embed/'+$(this).data('video'));
        } else {
            $('.sl-image').find('img').css('display', 'block').addClass( 'img-show' ).removeClass( 'img-hide' );
             $('.sl-image').find('iframe').css('display', 'none');
            $('.sl-image').css('width', $('.sl-image img').width()+'px').css('height', $('.sl-image img').height()+'px');
        }
        
        var image_width = $('.sl-image').width();
        var image_height = $('.sl-image').height();
        
//        setTimeout(function(){ 
//            var is_src_video = $('.sl-image').find('iframe').attr('src');
//            if (is_src_video) {
//                $('.sl-image').find('img').css('display', 'none');
//            }
//        }, 100);
        
        //$('.sl-navigation').attr('style', $('.sl-image').attr('style')).css('width', image_width+'px').css('height', image_height+'px');
        $('.sl-navigation').css('width', image_width+'px').css('height', image_height+'px');
        
        $('.sl-image').prepend('<div class="sl-top"><p>'+author+'</p><span class="sl-close">zamknij</span></div>');
    });
    
    // if click next
    $('body').on('nextImageLoaded.simplelightbox', '.gallery a', function () {
        if ($(this).data('video')) {
            $('.sl-image').find('img').css('display', 'none').addClass( 'img-hide' ).removeClass( 'img-show' );
            $('.sl-image').css('width', '700px').css('height', '394px');
            $('.sl-image').find('iframe').attr('src', 'https://www.youtube.com/embed/'+$(this).data('video')).css('display', 'block');
        } else {
            $('.sl-image').find('img').css('display', 'block').addClass( 'img-show' ).removeClass( 'img-hide' );
            $('.sl-image').css('width', $('.sl-image img').width()+'px').css('height', $('.sl-image img').height()+'px');
            $('.sl-image').find('iframe').attr('src', '').css('display', 'none');
        }
        
        var image_width = $('.sl-image').width();
        var image_height = $('.sl-image').height();
        
        $('.sl-navigation').css('width', image_width+'px').css('height', image_height+'px');
        
        setTimeout(function(){ 
            var author =  $('.sl-caption span').data('author');
            $('.sl-image .sl-top p').html(author);
//            var is_src_video = $('.sl-image').find('iframe').attr('src');
//            if (is_src_video) {
//                $('.sl-image').find('img').css('display', 'none');
//            }
        }, 300);
    });
    
    // if click prev
    $('.gallery a').on('prevImageLoaded.simplelightbox', function () {
        
        if ($(this).data('video')) {
            $('.sl-image').find('img').css('display', 'none').addClass( 'img-hide' ).removeClass( 'img-show' );
            $('.sl-image').css('width', '700px').css('height', '394px');
            $('.sl-image').find('iframe').attr('src', 'https://www.youtube.com/embed/'+$(this).data('video')).css('display', 'block');
        } else {
            $('.sl-image').find('img').css('display', 'block').addClass( 'img-show' ).removeClass( 'img-hide' );
            $('.sl-image').css('width', $('.sl-image img').width()+'px').css('height', $('.sl-image img').height()+'px');
            $('.sl-image').find('iframe').attr('src', '').css('display', 'none');
        }
        
        var image_width = $('.sl-image').width();
        var image_height = $('.sl-image').height();
        
        $('.sl-navigation').css('width', image_width+'px').css('height', image_height+'px');
        
        setTimeout(function(){ 
            var author =  $('.sl-caption span').data('author');
            $('.sl-image .sl-top p').html(author);
//            var is_src_video = $('.sl-image').find('iframe').attr('src');
//            if (is_src_video) {
//                $('.sl-image').find('img').css('display', 'none');
//            }
        }, 300);
        
    });
    

    //Slider gallery
    if ($(window).width() <= 767) {
    $('.gallery').slick({
        dots: true,
        arrows: false,
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        rows: 1,
        responsive: [
          {
            breakpoint: 767,
            settings: {
                slidesToShow: 3
            }
          },
          {
            breakpoint: 640,
            settings: {
                slidesToShow: 2
            }
          },
          {
            breakpoint: 500,
            settings: {
                slidesToShow: 2
            }
          }
        ]
    });
    } else {
    $('.gallery').slick({
        dots: true,
        arrows: false,
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        rows: 3,
        responsive: [
          {
            breakpoint: 1400,
            settings: {
                rows: 3,
                slidesToShow: 2
            }
          },
          {
            breakpoint: 992,
            settings: {
                rows: 2,
                slidesToShow: 1
            }
          }
        ]
    });
    }
    
    var divider;
    $(".slide").each(function (index) {
        divider = $(this).width() / $(this).height();
        if ($(this).find("img").width() / $(this).find("img").height() < divider) {
            $(this).find("img").css("width", "100%");
        } else {
            $(this).find("img").css("height", "100%");
        }
    });
    
});

/**
 * 
 * DOCUMENT SCROLL
 *  
 */
$(window).scroll(function () {
    
});


$(window).resize(function () {

  
});


function saveCookie(){
    setCookie("cookies-accept", "true", 256);
    
    var oBar = document.getElementById("cookie-bar");
    fade(oBar);
}

function setCookie(cname, cvalue, exdays){
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

