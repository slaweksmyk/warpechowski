            <?php include("header.php"); ?>
            <section class="archiwum">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="content">
                                <h1>Archiwum</h1>
                                
                                <div class="row">
                                    <?php for ($i = 1; $i <= 3; $i++) { ?>
                                    <div class="col-item">
                                        <a href="performance.php">
                                            <div class="img">
                                                <img src="../dist/img/_0007_1981-03-06-Zbigniew-Warpechowski-RĄSIA-performance-fot-TS.png" />
                                            </div>
                                            <div class="text">
                                                <span>#<?= $i ?></span>
                                                <h2>Rysunek w kącie</h2>
                                                <p>
                                                    wiosna 1971<br />
                                                    Galeria Foksal<br />
                                                    Warszawa
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                    <?php } ?>
                                </div>

                            </div> 
                            <?php include("mouse.php"); ?>
                        </div>
                    </div>

                </div>
            </section>
            <?php include("footer.php"); ?>
