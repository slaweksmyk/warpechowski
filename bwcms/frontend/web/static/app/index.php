            <?php include("header.php"); ?>
            <section class="home">
                <div class="container-fluid">
                    <div class="row">
                        <div class="offset-0 col-12 offset-lg-6 col-lg-6 offset-xl-7 col-xl-5">
                            <div class="content">
                                <h1>Zbigniew Warpechowski</h1>
                                <div class="short_description">
                                    <p>
                                        Urodził się w 1938 roku w Płoskach na Wołyniu. Mieszka i pracuje w Sandomierzu.
                                    </p>
                                </div>
                                <p>
                                    Urodził się w 1938 roku w Płoskach na Wołyniu. Mieszka i pracuje w Sandomierzu. W latach 1956-1962 studiował architekturę na Politechnice Krakowskiej, a w latach 1964-65 przez rok był studentem na Wydziale Form Przemysłowych w Akademii Sztuk Pięknych w Krakowie.
                                </p>
                                <p>
                                    Od 1967 roku Warpechowski uprawia sztukę performance, co czyni go prekursorem tego nurtu, nie tylko w Polsce, ale i na arenie międzynarodowej. Do tej pory jest autorem blisko 300 wystąpień w wielu krajach Europy, Azji i Ameryki Północnej. W 1985 roku został członkiem Grupy Krakowskiej, a od 1986 roku należy do legendarnej Black Market – międzynarodowej grupy zrzeszającej pionierów sztuki performance. Wykonał scenografię do ośmiu filmów fabularnych, w tym czterech w reżyserii Grzegorza Królikiewicza. W 1997 roku otrzymał Nagrodę Ministra Kultury i Sztuki za 30-letnią działalność w dziedzinie performance, a w 2009 Złoty Medal „Zasłużony Kulturze Gloria Artis”.
                                    Warpechowski rozpoczął karierę artystyczną od malarstwa figuratywnego. Po przerwaniu studiów na krakowskiej ASP pochłonęło go pisanie wierszy i to właśnie poezja stała się bodźcem prowadzącym tego artystę na drogę performance. „Na początku to, co robiłem nazywałem akcjami” – mówi artysta. – „Termin performance po raz pierwszy usłyszałem w 1975 roku w Marsylii.
                                    Pierwszymi akcjami/performance Warpechowskiego był Kwadrans poetycki (wraz z Tomaszem Stańko) w krakowskim klubie SAiW, i wykonany wkrótce potem Kwadrans poetycki z towarzyszeniem adapteru i fortepianu (1967).
                                </p>
                                <p>
                                    W 1971 roku Warpechowski wykonał w Biurze Poezji Andrzeja Partuma pierwszy z serii performance z rybą pt: Woda, w kolejnym z 1973 roku pt: Dialog z rybą przemawiał do trzymanej w rękach, wyjętej z wody ryby. To samo zwierzę pojawia się też w jednym z ikonicznych dla Warpechowskiego performance pt: Dialog ze śmiercią (1976).
                                    Ważnym performance w karierze Warpechowskiego był Champion of Golgotha (1978), do którego to tematu wracał jeszcze niejednokrotnie aż do 1994 roku. Poruszał w nim kwestię idolatrii (bałwochwalstwa), tworząc figurę Chrystusa jako współczesnego idola sportowego.
                                </p>
                                <p>
                                    W dorobku Warpechowskiego jest też sporo performance, w których artysta, w imię autentyzmu ekspresji, męczył swoje ciało, kaleczył się i biczował, jak np. Porozumienie z 1981 roku, w którym artysta odnosił się do aktualnej sytuacji politycznej w Polsce. W 1984 roku podczas performance pt: Marsz w Stuttgarcie podpalił sobie włosy zwilżone wcześniej benzyną.
                                    Od pewnego czasu Warpechowski, który sam nazywa siebie „konserwatystą awangardowym”, wykonuje performance o charakterze bardzo krytycznym wobec współczesnej kultury konsumpcyjnej, dominacji mediów, postmodernizmu i liberalizmu obyczajowego, jak np: Róg pamięci(1997), czy Wariant nadrzędny (2004).
                                </p>
                                <p>
                                    W 1974 roku na Zjeździe Marzycieli w Elblągu Warpechowski ogłosił swój pierwszy manifest teoretyczny Artysta jest, w którym podkreślił znaczenie „działania indywidualnego artysty i na własną odpowiedzialność” w procesie powstawania awangardowego dzieła sztuki.
                                    Od tego czasu opublikował wiele tekstów i książek między innymi Podręcznik (1990), Zasobnik (1998), Podnośnik (2001), Statecznik (2004),Podręcznik bis (2006) oraz ostatnio wydany Konserwatyzm Awangardowy (2014), w których opisuje swoje przemyślenia na temat kultury współczesnej, filozofii, wiary i  osobistych doświadczeń.
                                    Zbigniew Warpechowski jest też autorem rzeźb i obiektów np: seria Tatuaży (1983-1987), Autorytet (2010) czy Słowo (2009).
                                </p>
                                <p>
                                    Ostatnio coraz częściej skłania się też ku malarstwu w jego niemal klasycznej formie, zarówno pod względem techniki, jak i poruszanych tematów, np: Kain i Abel (2007) czy wciąż realizowana seria Główki (lub Trupki).
                                    Prace artysty znajdują się między innymi w zbiorach Muzeum sztuki w Łodzi, Muzeum Narodowego w Warszawie i Wrocławiu, Muzeum im. Leona Wyczółkowskiego w Bydgoszczy, Muzeum w Koszalinie.
                                </p>

                            </div> 
                            <?php include("mouse.php"); ?>
                        </div>
                    </div>

                </div>
            </section>
            <?php include("footer.php"); ?>
