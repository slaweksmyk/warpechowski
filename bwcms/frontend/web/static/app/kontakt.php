            <?php include("header.php"); ?>
            <section class="contact">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="content">
                                <h1>Kontakt</h1>
                                
                                <div class="row">
                                    <form>
                                        <div class="row">
                                            <div class="col-12 col-md-5">
                                                <div class="form-group field-contactdefault-name required has-error">
                                                    <input type="text" id="contactdefault-name" class="input-standard" name="ContactDefault[name]" placeholder="Imię i Nazwisko" aria-required="true" aria-invalid="true">
                                                </div>
                                                <div class="form-group field-contactdefault-name required has-error">
                                                    <input type="text" id="contactdefault-name" class="input-standard" name="ContactDefault[name]" placeholder="Imię i Nazwisko" aria-required="true" aria-invalid="true">
                                                </div>
                                                <div class="form-group field-contactdefault-name required has-error">
                                                    <input type="text" id="contactdefault-name" class="input-standard" name="ContactDefault[name]" placeholder="Imię i Nazwisko" aria-required="true" aria-invalid="true">
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-7">
                                                <div class="form-group field-contactdefault-name required has-error">
                                                    <textarea id="contactdefault-body" class="input-standard" name="ContactDefault[body]" placeholder="Treść Wiadomości" aria-required="true" aria-invalid="true"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-12 custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                                                    <label class="custom-control-label" for="customCheck1">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi cursus, metus sit amet condimentum tempus, orci velit tincidunt eros, porta molestie ipsum urna ut diam. Morbi sed tristique erat. Maecenas sed mollis tortor. Donec eget gravida orci.</label>
                                                    
                                            </div>
                                            <div class="col-12 text-center">
                                                <p class="required">*pola obowiązkowe</p>
                                                <button class="btn" type="submit">wyślij wiadomość</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="row text-center realizatons">
                                    <h3>
                                        Projekt zrealizowany przez:
                                    </h3>
                                    <img class="img-light" alt="Logo Galeria Monopol" src="../dist/img/logo-monopol.png" />
                                    <img class="img-dark" alt="Logo Galeria Monopol" src="../dist/img/logo-monopol-biale.png" />
                                    <p>galeria@galeriamonopol.pl</p>
                                    <div>
                                         <img alt="Logo Ministerstwo Kultury i Dziedzictwa narodowego" src="../dist/img/logo-ministerstwo.png" />
                                         <p>
                                             Dofinansowano ze środków Ministra Kultury<br />
                                             i Dziedzictwa Narodowego pochodzacych<br />
                                             z Funduszu promocji Kultury
                                         </p>
                                    </div>
                                </div>

                            </div> 
                            <?php include("mouse.php"); ?>
                        </div>
                    </div>

                </div>
            </section>
            <?php include("footer.php"); ?>
