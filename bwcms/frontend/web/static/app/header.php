<!DOCTYPE html>
<html lang="pl">
    <head prefix="og: http://ogp.me/ns#">
        <meta charset="UTF-8">
        <title>Warpechowski</title>
        <meta http-equiv="x-ua-compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="robots" content="index,fallow">
        <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <link href="https://fonts.googleapis.com/css?family=Comfortaa:300,400,700|Montserrat:100,200,300,400,500,600,700,800,900|Poppins:100,200,300,400,500,600,700,800,900&amp;subset=latin-ext" rel="stylesheet">

        <meta property="og:locale" content="pl_PL">
        <meta property="og:url" content="http://">
        <meta property="og:title" content="">
        <meta property="og:site_name" content="">
        <meta property="og:description" content="">
        <meta property="og:type" content="website">
        <meta property="og:image" content="http://">
        <meta property="og:image:type" content="image/png">
        <meta property="og:image:width" content="201">
        <meta property="og:image:height" content="201">
        <link href="../dist/css/library.bundle.css" rel="stylesheet">
        <link href="../dist/css/bundle.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="../dist/js/hack.bundle.js"></script>
        <![endif]-->
    </head>

    <body>
        <main>
            <header>
                <div class="container-fluid">
                    <div class="top row">
                        <nav class="navbar navbar-expand-lg navbar-light">
                            <a class="navbar-brand" href="/" title="Zbigniew Warpechowski">Zbigniew Warpechowski</a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                
                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav mr-auto">
                                    <li class="nav-item active">
                                        <a class="nav-link" href="performances.php" title="Szkolenia">performances</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="archiwum.php" title="Doradztwo">archiwum</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="kontakt.php" title="Kontakt">kontakt</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                        <div class="font-size">
                            <p>
                                Rozmiar czcionki:
                            </p>
                            <ul>
                                <li class="font-size-item active">
                                    <a data-class="fontDefault" class="font-size-default" href="#">A</a>
                                </li>
                                <li class="font-size-item">
                                    <a data-class="fontBig" class="font-size-big" href="#">A</a>
                                </li>
                                <li class="font-size-item">
                                    <a data-class="fontBigger" class="font-size-bigger" href="#">A</a>
                                </li>
                            </ul>
                        </div>
                        <div class="contrast">
                            <p>
                                Kontrast:
                            </p>
                            <ul>
                                <li class="contrast-item active">
                                    <a data-class="contrastLight" class="contrast-light" href="#">A</a>
                                </li>
                                <li class="contrast-item">
                                    <a data-class="contrastDark" class="contrast-dark" href="#">A</a>
                                </li>
                            </ul>
                        </div>
                        <div class="lang">
                            <ul>
                                <li class="active">
                                    <a href="/pl/">PL</a>
                                </li>
                                <li>
                                    <a href="/en/">EN</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </header>