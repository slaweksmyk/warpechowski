            <?php include("header.php"); ?>
            <section class="performances">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 col-md-11 col-lg-6 col-xl-5">
                            <div class="content content-left">
                                <h1>Performances</h1>
                                <form>
                                    <input class="form-control" type="text" name="search" value="" placeholder="wpisz szukaną frazę..." />
                                    <input class="btn" type="submit" value="szukaj" />
                                </form>
                                
                                <h2>wybierz okres twórczości</h2>
                                <div id="slider"></div>
                                
                                <ul class="performances-items">
                                    <?php for ($i = 1; $i <= 10; $i++) { ?>
                                    <li data-number="#<?= $i ?>">
                                        <a href="performance.php">
                                            <h3>Improwizacje</h3><br />
                                            <p>Kraków, rynek 1967, rynek 1967</p>
                                        </a>
                                    </li>
                                    <?php } ?>
                                    
                                </ul>

                            </div> 
                            <?php include("mouse.php"); ?>
                        </div>
                    </div>

                </div>
            </section>
            <?php include("footer.php"); ?>
