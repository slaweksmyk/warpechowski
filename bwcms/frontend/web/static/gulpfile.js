'use strict';
/*
 *
 * Management Gulp file
 *
 * @author: Tomasz Załucki <tomek@fuleo.pl>
 * gulp.series - wykonywanie sekwencyjne, po kolei
 * gulp.parallel  - wykonywanie równoległe
 * Gulp version: 4
 */
const gulp = require('gulp');
const babel = require('gulp-babel');
var autoprefixer = require('gulp-autoprefixer');
var promise = require('es6-promise').polyfill();
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var gulpMerge = require('gulp-merge');
var minify = require('gulp-minify');
var del = require('del');

/*
 * 
 * @type Module Sprite
 */
//var buffer = require('vinyl-buffer');
//var imagemin = require('gulp-imagemin');
//var merge = require('merge-stream');
//var csso = require('gulp-csso');
//var spritesmith = require('gulp.spritesmith');
//var spritesmithmulti = require('gulp.spritesmith-multi')
//

//Deklaracja zmiennych
const assetsDir = "src/";
//const assetsDist = 'dist/';
const assetsDist = '../../../../bwcms/frontend/web/';


//Definicja tasków
gulp.task('clean', () => {
    //return del.sync('dist');
    return true
})


//gulp.task('spritemulti', function () {
//  return gulp.src(assetsDir + 'img/**/*.*')
//    .pipe(spritesmithmulti())
//    .on('error', function (err) {
//      console.log(err)
//    })
//    .pipe(gulp.dest(assetsDist + 'img2'))
//})
//gulp.task('sprite', function () {
//    // Generate our spritesheet 
//    var spriteData = gulp.src(assetsDir + 'img/icons/*.png').pipe(spritesmith({
//        imgName: '../img/icons.png',
//        cssName: 'icons.css'
//    }));
//    // Pipe image stream through image optimizer and onto disk 
//    var imgStream = spriteData.img
//            // DEV: We must buffer our stream into a Buffer for `imagemin` 
//            .pipe(buffer())
//            .pipe(imagemin())
//            .pipe(gulp.dest(assetsDist + 'img'));
//    // Pipe CSS stream through CSS optimizer and onto disk 
//    var cssStream = spriteData.css
//            .pipe(csso())
//            .pipe(gulp.dest(assetsDir + 'css'));
//    // Return a merged stream to handle both `end` events 
//    return merge(imgStream, cssStream);
//});
//gulp.task('sprite-bg', function () {
//    // Generate our spritesheet 
//    var spriteData = gulp.src(assetsDir + 'img/bg/*.jpg').pipe(spritesmith({
//        imgName: '../img/bg.jpg',
//        cssName: 'bg.css'
//    }));
//    // Pipe image stream through image optimizer and onto disk 
//    var imgStream = spriteData.img
//            // DEV: We must buffer our stream into a Buffer for `imagemin` 
//            .pipe(buffer())
//            .pipe(imagemin())
//            .pipe(gulp.dest(assetsDist + 'img'));
//    // Pipe CSS stream through CSS optimizer and onto disk 
//    var cssStream = spriteData.css
//            .pipe(csso())
//            .pipe(gulp.dest(assetsDir + 'css'));
//    // Return a merged stream to handle both `end` events 
//    return merge(imgStream, cssStream);
//});
//gulp.task('sprite-logo', function () {
//    // Generate our spritesheet 
//    var spriteData = gulp.src(assetsDir + 'img/logo/*.png').pipe(spritesmith({
//        imgName: 'logo.png',
//        cssName: 'logo.css'
//    }));
//    // Pipe image stream through image optimizer and onto disk 
//    var imgStream = spriteData.img
//            // DEV: We must buffer our stream into a Buffer for `imagemin` 
//            .pipe(buffer())
//            .pipe(imagemin())
//            .pipe(gulp.dest(assetsDist + 'img'));
//
//    // Pipe CSS stream through CSS optimizer and onto disk 
//    var cssStream = spriteData.css
//            .pipe(csso())
//            .pipe(gulp.dest(assetsDir + 'css'));
//    // Return a merged stream to handle both `end` events 
//    return merge(imgStream, cssStream);
//});
gulp.task('scss', (done) => {
    gulpMerge(
            gulp.src(assetsDir + 'scss/*.scss')
            .pipe(sourcemaps.init())
            .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
            .pipe(concat('bundle.css'))
            .pipe(gulp.dest(assetsDist + 'css'))
            );
    done();
});
//gulp.task('scss-tablet', function () {
//    return gulp.src(assetsDir + 'scss/tablet/*.scss')
//            .pipe(sourcemaps.init())
//            .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
//            //.pipe(sourcemaps.write(assetsDist+'css/maps'))
//            .pipe(concat('tablet.css'))
//            .pipe(gulp.dest(assetsDist + 'css'));
//});
//gulp.task('amp', function () {
//    return gulp.src(assetsDir + 'scss/amp/*.scss')
//            .pipe(sourcemaps.init())
//            .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
//            //.pipe(sourcemaps.write(assetsDist+'css/maps'))
//            .pipe(concat('amp.css'))
//            .pipe(gulp.dest(assetsDist + 'css'));
//});
//gulp.task('scss-phone', function () {
//    return gulp.src(assetsDir + 'scss/phone/*.scss')
//            .pipe(sourcemaps.init())
//            .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
//            //.pipe(sourcemaps.write(assetsDist+'css/maps'))
//            .pipe(concat('phone.css'))
//            .pipe(gulp.dest(assetsDist + 'css'));
//});

gulp.task('js', (done) => {
    gulpMerge(
            gulp.src([assetsDir + 'js/main/*.js', assetsDir + 'js/plugins/*.js'])
            .pipe(babel({
                presets: ['@babel/env']
            }))
            .pipe(uglify())
            .pipe(concat('bundle.js'))
            .pipe(gulp.dest(assetsDist + 'js'))
            );
    done();
});
//gulp.task('js-tablet', function () {
//    return gulpMerge(
//            gulp.src(assetsDir + 'js/tablet/*.js')
//            .pipe(uglify())
//            .pipe(concat('tablet.js'))
//            .pipe(gulp.dest(assetsDist + 'js'))
//            );
//});
//gulp.task('js-phone', function () {
//    return gulpMerge(
//            gulp.src(assetsDir + 'js/phone/*.js')
//            .pipe(uglify())
//            .pipe(concat('phone.js'))
//            .pipe(gulp.dest(assetsDist + 'js'))
//            );
//});
//// node_modules JS
gulp.task('js-library', (done) => {
    gulpMerge(
            gulp.src([
                'node_modules/jquery/dist/jquery.min.js',
                'node_modules/popper.js/dist/umd/popper.min.js',
                'node_modules/bootstrap/dist/js/bootstrap.min.js',
                'node_modules/slick-carousel/slick/slick.min.js',
                'node_modules/simplelightbox/dist/simple-lightbox.min.js',
                //'node_modules/lightbox2/dist/js/lightbox.min.js'
                        // 'node_modules/jquery-ticker/jquery.ticker.min.js',
                        // 'node_modules/jquery.marquee/jquery.marquee.min.js',

            ])
            .pipe(uglify())
            .pipe(concat('library.bundle.js'))
            .pipe(gulp.dest(assetsDist + 'js'))
            );
    done();
});
//// node_modules CSS
gulp.task('css-library', (done) => {
    gulpMerge(
            gulp.src([
                'node_modules/bootstrap/dist/css/bootstrap.min.css',
                'node_modules/slick-carousel/slick/slick.css',
                'node_modules/slick-carousel/slick/slick-theme.css',
                'node_modules/simplelightbox/dist/simplelightbox.min.css',
                //'node_modules/lightbox2/dist/css/lightbox.min.css'
            ])
            .pipe(concat('library.bundle.css'))
            .pipe(gulp.dest(assetsDist + 'css'))
            );
    done();
});

gulp.task('hack', (done) => {
    gulpMerge(
            gulp.src(assetsDir + 'js/hack/*.js')
            .pipe(uglify())
            .pipe(concat('hack.bundle.js'))
            .pipe(gulp.dest(assetsDist + 'js'))
            );
    done();
});

//first developer task
gulp.task('first', gulp.series('scss', 'js', 'js-library', 'css-library', 'hack'));
//gulp.task('first', [
//    //'sprite',
//   // 'sprite-logo',
//   // 'sprite-bg',
//    'scss',
//    // 'scss-tablet',
//    // 'scss-phone',
//    'js',
//    // 'js-tablet',
//    // 'js-phone',
//   // 'hack',
//    'js-library',
//   // 'css-library',
//            // 'amp'
//]);

// Gulp watcher
// Console: gulp watch
//gulp.task('watch', function () {
//    //scss changes
//    gulp.watch(assetsDir + 'scss/*/*.scss', ['scss']);
//    //  gulp.watch('dev/scss/tablet/**/*.scss', ['scss-tablet']);
//    //   gulp.watch('dev/scss/phone/**/*.scss', ['scss-phone']);
//    //  gulp.watch('dev/scss/amp/**/*.scss', ['amp']);
//    //js changes
//    gulp.watch(assetsDir + 'js/*/*.js', ['js']);
//    // gulp.watch('dev/js/tablet/**/*.js', ['js-tablet']);
//    //gulp.watch('dev/js/phone/**/*.js', ['js-phone']);
//});
// Watch files
function watchFiles() {
    gulp.watch(assetsDir + 'scss/**/*.scss', gulp.parallel('scss', 'js'));
}
// watch
gulp.task("watch", gulp.parallel(watchFiles));