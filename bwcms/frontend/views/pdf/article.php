<?php
header('Content-Type: text/html; charset=utf-8');
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>

    <body>
        <div style="clear:both;width: 100%;height:80px">
            <div style="float:right;text-align: right;width:40%;margin-bottom: 10px;">
                <?php if($oArticle->getAuthor()){ ?>
                    <strong style="display: block;">aut. <?= $oArticle->getAuthor()->firstname ?> <?= $oArticle->getAuthor()->surname ?></strong>
                <?php } ?>
                <span style="display: block;"><?= date("d.m.Y", strtotime($oArticle->date_display)) ?></span>
            </div>
        </div>

        <div style="clear:both;">
            <h1 style="text-transform: uppercase;font-weight:36px;font-weight:700"><?= $oArticle->title ?></h1>
            <?= $oArticle->summary_description ?>
            <hr>

            <?= $oArticle->short_description ?>

            <div class="news-text">
                <?= $oArticle->getFullDescription($this,false) ?>
            </div>
        </div>

    </body>
</html>