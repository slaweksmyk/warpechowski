<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use frontend\assets\AppAsset;
    use frontend\components\PrepareMetaTags;
    use common\modules\pages\models\PagesSite;
    
    header("HTTP/1.0 404 Not Found");
    $oPage = PagesSite::find()->where(["=", "id", 1])->one();
    Yii::$app->params["oPage"] = $oPage;

    AppAsset::register($this);
    PrepareMetaTags::register($this);

    $device = "pc";
    if(\Yii::$app->devicedetect->isMobile() && !\Yii::$app->devicedetect->isTablet()){
        $device = "mobile";
    }
    if(\Yii::$app->devicedetect->isTablet()){
        $device = "tablet";
    }
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= current(explode("-", Yii::$app->language)); ?>" >
    <head prefix="og: http://ogp.me/ns#">
        <meta charset="<?= Yii::$app->charset; ?>">
        <meta http-equiv="x-ua-compatible" content="IE=edge">
        <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">

        <link rel="alternate" href="<?= Url::home(true) ?>feed/" type="application/rss+xml">
        <link rel="shortcut icon" href="/favicon.ico" />
        <link rel="manifest" href="/manifest.json">
        
        <?= Html::csrfMetaTags(); ?>
        <title><?php if($oPage->seo_title != ""){ echo Html::encode($oPage->seo_title); } else { echo Html::encode($oPage->name); } ?></title>
        <?php $this->head(); ?>
        
        <?php $aSections = []; /*$aSections = ["default"];*/ ?>
        <script>
            function Section() {
                return  {"items": [<?php foreach($aSections as $index => $sSection){ ?>{id: <?= ($index+1) ?>,"url": "/sections/",options: {iPageID : <?= $oPage->id ?>,sView : "<?= $sSection ?>",sDevice : "<?= $device ?>"}},<?php } ?>]}
            }
        </script>
        
        <?php $this->head(); ?>
    </head>
    <body>
        <?php  $this->beginBody(); if(Yii::$app->session->get('preview_mode', null)){ echo $this->render('preview_bar'); } ?>
        
        <section id="error-404" data-item="error-404" class="section block block-title block-subtitle error-404 mb-30">
            <div class="container">  
                <div class="error-404-content">
                    <h1>404</h1>
                    <span>Wybrana strona nie zostala odnaleziona</span>
                    <a href="/" title="Strona główna">Przejdź do strony głównej</a>
                </div>  
                </div>
            </div>
        </section>
        
        <?php $this->endBody(); ?>
    </body>
</html>
<?php $this->endPage(); ?>
<?php exit; ?>