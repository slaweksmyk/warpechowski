<?php

function get_layout_positions()
{
    return [
        'Header' => [
            'HOME_MAIN_MENU' => ['name' => 'Treść strony', 'class' => 'col-10'],
            'HOME_LANG' => ['name' => 'Wybór języka', 'class' => 'col-2'],
        ],
        'Treść strony' => [
            'HOME_CONTENT' => ['name' => 'Treść strony', 'class' => 'col-12'],
        ],
        
    ];
}
