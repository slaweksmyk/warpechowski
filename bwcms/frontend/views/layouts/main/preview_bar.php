<?php
use yii\helpers\Html;
?>

<style>
    body { padding-bottom: 30px; }
</style>

<div class="preview-mode">
    Strona jest aktualnie w trybie podglądu. Nieopublikowane treści są widoczne <u>wyłącznie dla Ciebie</u>.
    <a href="/index.php?disablePreview=true">Przejdź do wersji LIVE</a>
</div>