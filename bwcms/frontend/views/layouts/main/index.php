<?php
$oPage = Yii::$app->params['oPage'];

use yii\helpers\Url;
use yii\helpers\Html;
use frontend\assets\AppAsset;
use frontend\components\PrepareMetaTags;
use common\helpers\TranslateHelper;

AppAsset::register($this);
PrepareMetaTags::register($this);
    
?>

<?php 
$this->beginPage() ;
use common\modules\articles\models\Article;    
?>
<!DOCTYPE html>
<html lang="<?= current(explode("-", Yii::$app->language)); ?>">
    <head prefix="og: http://ogp.me/ns#">
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-131037015-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-131037015-1');
        </script>

        <meta charset="<?= Yii::$app->charset; ?>">
        <meta http-equiv="x-ua-compatible" content="IE=edge">
        <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <link href="https://fonts.googleapis.com/css?family=Comfortaa:300,400,700|Montserrat:100,200,300,400,500,600,700,800,900|Poppins:100,200,300,400,500,600,700,800,900&amp;subset=latin-ext" rel="stylesheet">
        <script src="https://d3js.org/d3.v3.min.js"></script>
        <link rel="alternate" href="<?= Url::home(true) ?>feed/" type="application/rss+xml">
        <link rel="shortcut icon" href="/img/favicon.ico" />
        <link rel="manifest" href="/manifest.json">

        <?= Html::csrfMetaTags(); ?>

        <title><?= $oPage->seo_title != "" ? Html::encode($oPage->seo_title) : Html::encode($oPage->name); ?></title>
        
        <?php $this->head(); ?>
        <?php
        $cookies = Yii::$app->request->cookies;
        $fontSize = $cookies->getValue('font-size');
        $contrast = $cookies->getValue('contrast');
        ?>
    </head>
    <body class="<?php if ($fontSize) { ?><?= $fontSize ?><?php } ?> <?php if ($contrast) { ?><?= $contrast ?><?php } ?>">
        <?php $this->beginBody(); if(Yii::$app->session->get('preview_mode', null)){ echo $this->render('preview_bar'); } ?>

        
        <main>
            <header>
                <div class="container-fluid">
                    <div class="top row">
                        <nav class="navbar navbar-expand-lg navbar-light">
                            <a class="navbar-brand" href="/" title="Zbigniew Warpechowski">Zbigniew Warpechowski</a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <?= Yii::$app->WidgetHelper->getWidgets($oPage, 'HOME_MAIN_MENU'); ?>
                        </nav>
                        <div class="font-size">
                            <p>
                                <?= TranslateHelper::t('FONT_SIZE') ?>:
                            </p>
                            <ul>
                                <li class="font-size-item active">
                                    <a data-class="fontDefault" class="font-size-default" href="#" title="Czcionka podstawowa">A</a>
                                </li>
                                <li class="font-size-item">
                                    <a data-class="fontBig" class="font-size-big" href="#" title="Czcionka średnia">A</a>
                                </li>
                                <li class="font-size-item">
                                    <a data-class="fontBigger" class="font-size-bigger" href="#" title="Czcionka duża">A</a>
                                </li>
                            </ul>
                        </div>
                        <div class="contrast">
                            <p>
                                <?= TranslateHelper::t('CONTRAST') ?>:
                            </p>
                            <ul>
                                <li class="contrast-item active">
                                    <a data-class="contrastLight" class="contrast-light" href="#" title="Kontrast domyślny">A</a>
                                </li>
                                <li class="contrast-item">
                                    <a data-class="contrastDark" class="contrast-dark" href="#" title="Kontrast czarno-biały">A</a>
                                </li>
                            </ul>
                        </div>
                        <?= Yii::$app->WidgetHelper->getWidgets($oPage, 'HOME_LANG'); ?>
                    </div>
                </div>
            </header>
            <?= Yii::$app->WidgetHelper->getWidgets($oPage, 'HOME_CONTENT'); ?>
            <footer>
                <a class="bw" href="http://blackwolfcms.pl/" title="oprogramowanie: Black Wolf CMS">oprogramowanie: <strong>Black Wolf CMS</strong></a>
                <a class="la" href="http://laboratoriumartystyczne.pl/" title="projekt i realizacja: Laboratorium Artystyczne">projekt i realizacja: <strong>Laboratorium Artystyczne</strong></a>
            </footer>        
        </main>
        <?php $this->endBody(); ?>
    </body>
</html>
<?php $this->endPage(); ?>