<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\modules\articles\models\Article;

class SiteController extends Controller {

    private $exception;

    public function behaviors() {
        if(YII_DEBUG){
            return [];
        } else {
            return [];
            /*return [
                [
                    'class' => 'yii\filters\PageCache',
                    'duration' => 60,
                    'variations' => [
                        isset(\Yii::$app->params['oPage']) ? \Yii::$app->params['oPage']->id : null,
                        isset(\Yii::$app->params['slug']) ? \Yii::$app->params['slug'] : null,
                        \Yii::$app->request->get('page', null),
                        \Yii::$app->request->get('type', null),
                        \Yii::$app->request->get('search', null),
                        \Yii::$app->request->get('tag', null),
                        \Yii::getAlias('@device')
                    ],
                ],
            ];*/
        }
    }

    public function init() {
        parent::init();
    }

    /**
     * Before load controller actions
     * 
     * @param string $action
     * @return boolean
     */
    public function afterAction($action, $result) {
        if ($this->exception) {
            return $this->actionError();
        }

        if (isset(Yii::$app->params['slug'])) {
            $oPage = Yii::$app->params['oPage'];

            return $this->render('index', [
                'oPage' => $oPage
            ]);
        }

        return parent::afterAction($action, $result);
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [];
    }

    /**
     * Show single page action
     *
     * @return mixed
     */
    public function actionIndex() {
        $oPage = Yii::$app->params['oPage'];

        if ($oPage) {
            return $this->render('index', [
                'oPage' => $oPage
            ]);
        } else {
            return $this->render('error', [
                'oPage' => $oPage
            ]);
        }
    }

    /**
     * Show single page action
     *
     * @return mixed
     */
    public function actionView() {
        if (isset(Yii::$app->params['oPage'])) {
            $oPage = Yii::$app->params['oPage'];

            if ($oPage) {
                return $this->render('index', [
                    'oPage' => $oPage
                ]);
            } else {
                return $this->render('error', [
                    'oPage' => $oPage
                ]);
            }
        }
    }

    /**
     * Error page action
     * 
     * @return string
     */
    public function actionError() {
        $this->exception = \Yii::$app->errorHandler->exception;

        if ($this->exception !== null) {
            return $this->render('error', []);
        }
    }

}
