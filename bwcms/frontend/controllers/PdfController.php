<?php
namespace frontend\controllers;

use Yii;
use kartik\mpdf\Pdf;
use common\modules\articles\models\Article;

class PdfController extends \yii\web\Controller
{

    public function actionIndex($article)
    {
        $oArticle = Article::find()->where(["=", "slug", $article])->one();
        
        $sHtml = $this->renderPartial('article', ['oArticle' => $oArticle]);
        
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8, 
            'format' => Pdf::FORMAT_A4, 
            'orientation' => Pdf::ORIENT_PORTRAIT, 
            'destination' => Pdf::DEST_DOWNLOAD, 
            'content' => $sHtml,
            'filename' => $article.'_'.Yii::$app->params['serviceName'].'.pdf',
            'cssFile' => "@frontend/web/css/style.css"
        ]);

        return $pdf->render(); 
    }

}
