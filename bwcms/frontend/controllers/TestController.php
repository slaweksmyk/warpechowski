<?php
namespace frontend\controllers;

use Yii;
use common\modules\shop\models\ShopOrders;
use common\modules\shop\models\ShopPayment;

class TestController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $oOrder   = ShopOrders::findOne(1);
        $oPayment = ShopPayment::findOne(3);
        
        $sClass = "common\widgets\shop\payment\models\\{$oPayment->api}";
        $oPaymentAPI = new $sClass($oPayment->api, $oOrder);
        $oPaymentAPI->send();

        Yii::$app->end();
    }

}
