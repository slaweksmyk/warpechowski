<?php
namespace frontend\controllers;

use Yii;
use common\modules\pages\models\PagesSite;

class SectionsController extends \yii\web\Controller
{
    private $oPage = null;
    
    public function actionIndex()
    {
        switch(Yii::$app->request->get('sDevice')){
            
            case "tablet":
                $template = "index_tablet";
                break;
                
            case "mobile":
                $template = "index_mobile";
                break;
            
            default:
                $template = "index";
        }

        $iPageID = Yii::$app->request->get('iPageID');
        $sView   = Yii::$app->request->get('sView');
        
        if($sView == "group") { return; }
        
        $this->oPage = PagesSite::find()->where(["=", "id", $iPageID])->one();

        if(file_exists($this->getViewPath()."/{$sView}/{$template}.php")){
            echo $this->renderPartial("{$sView}/{$template}", [
                'oPage' => $this->oPage
            ]);
        } else {
            echo $this->renderPartial("{$sView}/index", [
                'oPage' => $this->oPage
            ]);
        }

        exit;
    }

}
