<?php
namespace frontend\controllers;

use yii\helpers\Url;
use common\modules\files\models\File;
use common\modules\articles\models\Article;

class FeedController extends \yii\web\Controller
{
    private $sLastUpdatedDate, $sMainUrl = "";
    private $aRSSArticles = [];
    private $oConfig;
    
    public function actionIndex()
    {
        $this->sLastUpdatedDate = date("D, d M Y G:i:s T");
        $this->sMainUrl = Url::home(true);
        $this->oConfig = Yii::$app->params['oConfig'];

        $this->getArticles();
        $this->genereteOutput();
    }
    
    private function getArticles(){
        $oArtileRowset = Article::find()->joinWith("status")->andWhere(["=", "is_published", 1])->orderBy("date_display DESC");
        $oArtileRowset->limit(\Yii::$app->request->get('amount', 10));
        
        if(\Yii::$app->request->get('category') != ""){
            $oArtileRowset->andWhere(["=", "category_id", \Yii::$app->request->get('category')]);
        }
        
        $oArtileRowset = $oArtileRowset->all();

        if(\Yii::$app->request->get('type') != ""){
            $aArticleRowset = [];
            foreach($oArtileRowset as $oArticle){
                if($oArticle->hasType(\Yii::$app->request->get('type'))){
                    $aArticleRowset[] = $oArticle;
                }
            }
        } else {
            $aArticleRowset = $oArtileRowset;
        }
        
        foreach($aArticleRowset as $index => $oArticle){
            if($index == 0){
                if($oArticle->date_updated){
                    $sLastUpdatedDate = date("D, d M Y G:i:s T", strtotime($oArticle->date_updated));
                } else if($oArticle->date_display){
                    $sLastUpdatedDate = date("D, d M Y G:i:s T", strtotime($oArticle->date_display));
                } else {
                    $sLastUpdatedDate = date("D, d M Y G:i:s T", strtotime($oArticle->date_created));
                }
            }
            $this->aRSSArticles[] = $oArticle;
        }
    }
    
    private function genereteOutput(){
       header('Content-type: application/xml; charset="utf-8"');
        $rssXML = "<?xml version='1.0' encoding='utf-8'?>";
        $rssXML .= "<rss version='2.0' xmlns:atom='http://www.w3.org/2005/Atom'>";
            $rssXML .= "<channel>";
                $rssXML .= "<atom:link href='{$this->sMainUrl}feed/' rel='self' type='application/rss+xml' />";
                $rssXML .= "<title>{$this->oConfig->page_name}</title>";
                $rssXML .= "<description><![CDATA[{$this->oConfig->page_name}]]></description>";
                $rssXML .= "<link>{$this->sMainUrl}</link>";
                if($this->oConfig->getSeoFbImage()->one()){
                    $rssXML .= "<image>";
                        $rssXML .= "<title>{$this->oConfig->page_name}</title>";
                        $rssXML .= "<link>{$this->sMainUrl}</link>";
                        $rssXML .= "<url>".$this->sMainUrl.File::getThumbnail($this->oConfig->getSeoFbImage()->one()->id, 150, 225)."</url>";
                    $rssXML .= "</image>";
                }
                $rssXML .= "<language>". strtolower(current(explode("-", \Yii::$app->language)))."</language>";
                $rssXML .= "<pubDate>{$this->sLastUpdatedDate}</pubDate>";
                $rssXML .= "<lastBuildDate>{$this->sLastUpdatedDate}</lastBuildDate>";
                $rssXML .= "<ttl>15</ttl>";
                $rssXML .= "<copyright><![CDATA[Copyright (C) ".date("Y")."  ]]></copyright>";

                foreach($this->aRSSArticles as $oArticle){
                    $rssXML .= "<item>";
                        $rssXML .= "<title><![CDATA[".str_replace("\"", "'", $oArticle->title)."]]></title>";
                        $rssXML .= "<link>{$oArticle->getAbsoluteUrl()}</link>";
                        $rssXML .= "<description><![CDATA[".trim(strip_tags(str_replace("\"", "'", $oArticle->short_description)))."]]></description>";
                        
                        if($oArticle->getAuthor()){
                            $rssXML .= "<author>{$oArticle->getAuthor()->email} ({$oArticle->getAuthor()->firstname} {$oArticle->getAuthor()->surname})</author>";
                        }
                        
                        $rssXML .= "<category domain='".($oArticle->getCategory() ? $oArticle->getCategory()->getAbsoluteUrl() : "")."'>".($oArticle->getCategory() ? $oArticle->getCategory()->name : "")."</category>";
                        
                        if($oArticle->hasThumbnail()){
                            $oFile = File::find()->where(["=", "id", $oArticle->thumbnail_id])->one();
                            $rssXML .= "<enclosure url='".trim($this->sMainUrl, "/").$oArticle->getThumbnail(400, 700, "matched")."' type='{$oFile->type}' length='{$oFile->size}' />";
                        }
                        
                        $rssXML .= "<guid isPermaLink='true'>{$oArticle->getAbsoluteUrl()}</guid>";
                        $rssXML .= "<pubDate>".date("D, d M Y G:i:s T", strtotime($oArticle->date_display))."</pubDate>";
                        $rssXML .= "<comments>{$oArticle->getAbsoluteUrl()}#comments</comments>";
                    $rssXML .= "</item>"; 
                }

            $rssXML .= "</channel>"; 
        $rssXML .= "</rss>"; 
        
        echo $rssXML;
        exit;
    }

}
