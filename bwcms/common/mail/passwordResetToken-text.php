<?php

/* @var $this yii\web\View */
/* @var $user common\modules\users\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl([$page_url.'/reset-password', 'token' => $user->password_reset_token]);
?>
Hello <?= $user->username ?>,

Follow the link below to reset your password:

<?= $resetLink ?>
