<?php
namespace common\hooks\yii2\grid;

class GridView extends \yii\grid\GridView
{
    public $pager = [
        'firstPageLabel' => 'pierwsza',
        'lastPageLabel' => 'ostatnia',
        'class' => 'common\hooks\yii2\widgets\LinkPager'
    ];
}