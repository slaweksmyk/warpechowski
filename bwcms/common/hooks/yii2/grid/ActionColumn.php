<?php
namespace common\hooks\yii2\grid;

use Yii;
use yii\helpers\Html;

class ActionColumn extends \yii\grid\ActionColumn
{
    protected function initDefaultButton($name, $iconName, $additionalOptions = array()) {
        if (!isset($this->buttons[$name]) && strpos($this->template, '{' . $name . '}') !== false) {
            $this->buttons[$name] = function ($url, $model, $key) use ($name, $iconName, $additionalOptions) {
                switch ($name) {
                    case 'view':
                        $class = "btn-warning";
                        $title = Yii::t('yii', 'View');
                        break;
                    case 'update':
                        $class = "btn-primary";
                        $title = Yii::t('yii', 'Update');
                        break;
                    case 'delete':
                        $class = "btn-danger";
                        $title = Yii::t('yii', 'Delete');
                        break;
                    default:
                        $class = "btn-warning-yellow";
                        $title = ucfirst($name);
                }
                $options = array_merge([
                    'title' => $title,
                    'aria-label' => $title,
                    'data-pjax' => '0',
                    'data-toggle' => 'tooltip',
                    'data-placement' => 'bottom'
                ], $additionalOptions, $this->buttonOptions);
                $icon = Html::tag('span', '', ['class' => "btn $class btn-xs glyphicon glyphicon-$iconName"]);
                return Html::a($icon, $url, $options);
            };
        }
    }
    
}