<?php
namespace common\hooks\yii2\base;

use Yii;
use common\modules\logs\models\Log;

class Module extends \yii\base\Module
{
    private $allowedActions = ['view','delete','create','update'];
    public function beforeAction($action) {
        $sActionType = strtolower(str_replace("action", "", $action->actionMethod));

        if(in_array($sActionType, $this->allowedActions)){
            $sModule = current(explode("-", $this->id));
            
            if(!Yii::$app->user->can("{$sModule}_{$sActionType}")){
                Yii::$app->getResponse()->redirect(Yii::$app->homeUrl)->send();
                exit;
            }
        }
        
        return parent::beforeAction($action);
    }
    
    private $loggedActions = ['create','update', 'delete'];
    public function afterAction($action, $result) {
        $sActionType = strtolower(str_replace("action", "", $action->actionMethod));

        $aActionRaw = explode("_", $sActionType);
        if(count($aActionRaw) > 1){
            $aActionRaw[0] = ucfirst($aActionRaw[0]);
            $aActionRaw[1] = ucfirst($aActionRaw[1]);
            $sActionType = "{$aActionRaw[0]}/{$aActionRaw[1]}";
        }
        
        if(Yii::$app->request->isPost){
            $sName = "";
            foreach(Yii::$app->request->post() as $aPost){
                if(isset($aPost["name"])){
                    $sName = $aPost["name"];
                } else if(isset($aPost["title"])){
                    $sName = $aPost["title"];
                } else if(isset($aPost["email"])){
                    $sName = $aPost["email"];
                } else if(isset($aPost["url"])){
                    $sName = $aPost["url"];
                } else if(isset($aPost["code"])){
                    $sName = $aPost["code"];
                } else if(isset($aPost["firstname"])){
                    $sName = $aPost["firstname"];
                } else if(isset($aPost["id"])){
                    $sName = "ID: ".$aPost["id"];
                } 
            }
            Log::addRecord($this->id, $sActionType, $sName, null);
        }
        
        return parent::afterAction($action, $result);
    }
    
}