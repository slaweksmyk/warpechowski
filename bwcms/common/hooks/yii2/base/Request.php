<?php

/**
 * Extend Request
 * 
 * @author Tomasz Załucki <tomek@fuleo.pl>
 * @copyright (c) 2018 Tomasz Załucki
 * @version 1.0
 */

namespace common\hooks\yii2\base;

use common\helpers\XssHelper;

class Request extends \yii\web\Request
{
    /*
     * Method __Contruct
     * Catch all request and send to xss helper
     * @see XssHelper       Xss Helper 
     * 
     * @param void $_GET
     * @param void $_POST
     * @return  string
     */

    public function __construct()
    {
        /*
         * Catch all native $_GET
         */
        $_GET = XssHelper::get($_GET, false);

        /*
          * Catch all native $_POST
         */
        //$_POST = XssHelper::post($_POST, false);
    }

}
