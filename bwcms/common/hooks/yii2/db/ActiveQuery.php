<?php
namespace common\hooks\yii2\db;

use Yii;

class ActiveQuery extends \yii\db\ActiveQuery
{
    
    public function one($db = null) {
        if ($this->emulateExecution) { return false; }
        
        if(isset(Yii::$app->request->url) && strpos(Yii::$app->request->url, \Yii::$app->request->baseUrl) !== false){
            $row = $this->createCommand($db)->queryOne();
        } else {
            $sModelClass = $this->modelClass;
            $dependency = new \yii\caching\DbDependency(['sql' => 'SELECT COUNT(*) FROM ' . $sModelClass::tableName()]);
            $row = $sModelClass::getDb()->cache(function ($db) { 
                return $this->createCommand($db)->queryOne();
            }, 300, $dependency);
        }

        if ($row !== false) {
            $models = $this->populate([$row]);
            return reset($models) ?: null;
        } else {
            return null;
        }
    }
    
    public function all($db = null) {
        if ($this->emulateExecution) { return []; }

        $sModelClass = $this->modelClass;
        $dependency = new \yii\caching\DbDependency(['sql' => 'SELECT COUNT(*) FROM ' . $sModelClass::tableName()]);
        $rows = $sModelClass::getDb()->cache(function ($db) { 
            return $this->createCommand($db)->queryAll();
        }, 300, $dependency);

        return $this->populate($rows);
    }

}

?>