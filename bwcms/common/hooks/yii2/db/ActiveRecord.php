<?php

namespace common\hooks\yii2\db;

use Yii;
use common\hooks\yii2\db\ActiveQuery;
use common\helpers\XssHelper;

class ActiveRecord extends \yii\db\ActiveRecord
{
    /*
     * Label name witout xss check
     */

    public $exception = ['short_description', 'full_description', 'seo_description', 'description', 'summary_description'];

    public static function find()
    {
        return Yii::createObject(ActiveQuery::className(), [get_called_class()]);
    }

    /*
     * Function catch all model and added xss filter.
     * Use $exception - Exception label for xss filter.
     * @see XssHelper::multi           Multi Xss Haleper
     * 
     * @param string $name
     * $param string $value
     * @return string
     */

    public function __set($name, $value)
    {

        if (!in_array($name, $this->exception)) {
            //$value = XssHelper::post($value, false);
        }
        parent::__set($name, $value);
    }

}

?>