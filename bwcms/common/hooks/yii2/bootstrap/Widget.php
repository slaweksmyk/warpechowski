<?php
namespace common\hooks\yii2\bootstrap;

use common\modules\widget\models\WidgetItems;

class Widget extends \yii\bootstrap\Widget
{
    public $md_limit = 10;
    public $layout_enabled, $layout;
    public $layout_tablet_enabled, $layout_tablet;
    public $layout_mobile_enabled, $layout_mobile;
    public $limit_pc, $limit_tablet, $limit_mobile;
    
    public function init() {
        if(\Yii::$app->devicedetect->isMobile() && !\Yii::$app->devicedetect->isTablet()){
            $this->md_limit = $this->limit_mobile;
        } else if(\Yii::$app->devicedetect->isTablet()){
            $this->md_limit = $this->limit_tablet;
        } else {
            $this->md_limit = $this->limit_pc;
        }
        
        parent::init();
    }

    public function display($aAttributes, $sSubdir = null){
        $sLayout = $this->layout;
        $isEnabled = $this->layout_enabled;
        
        if(\Yii::$app->devicedetect->isMobile() && !\Yii::$app->devicedetect->isTablet()){
            $sLayout = $this->layout_mobile;
            $isEnabled = $this->layout_mobile_enabled;
        } else if(\Yii::$app->devicedetect->isTablet()){
            $sLayout = $this->layout_tablet;
            $isEnabled = $this->layout_tablet_enabled;
        }

        if($sSubdir){
            $sLayout = "{$sLayout}/{$sSubdir}";
        }
        
        if($isEnabled){
            if(YII_DEBUG){
                $oWidgetItem = WidgetItems::find()->where(["=", "id", $this->widget_item_id])->one();
                $editURL = "<div style='position: relative; height: 0px; width: 0px; float: left; z-index: 9999;'><a href='".\Yii::$app->request->baseUrl."widgets/widget/edit/?id={$this->widget_item_id}' target='_blank' style='position: absolute; top: -20px; background: #f6f6f6;padding: 5px;box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24); font-size: 11px; white-space: nowrap;'>[{$oWidgetItem->custom_name}]</a></div>";
                return $editURL.$this->render($sLayout, $aAttributes);
            } else {
                return $this->render($sLayout, $aAttributes);
            }
            
        }
    }
    
}