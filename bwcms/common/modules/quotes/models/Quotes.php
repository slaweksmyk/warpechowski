<?php

namespace common\modules\quotes\models;

use Yii;

/**
 * This is the model class for table "xmod_quotes".
 *
 * @property integer $id
 * @property string $name
 * @property double $bid
 * @property double $ask
 * @property double $mid
 * @property string $time
 */
class Quotes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_quotes}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bid', 'ask', 'mid'], 'number'],
            [['time'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_quotes', 'ID'),
            'name' => Yii::t('backend_module_quotes', 'Name'),
            'bid' => Yii::t('backend_module_quotes', 'Bid'),
            'ask' => Yii::t('backend_module_quotes', 'Ask'),
            'mid' => Yii::t('backend_module_quotes', 'Mid'),
            'time' => Yii::t('backend_module_quotes', 'Time'),
        ];
    }
}
