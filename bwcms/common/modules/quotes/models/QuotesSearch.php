<?php

namespace common\modules\quotes\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\quotes\models\Quotes;

/**
 * QuotesSearch represents the model behind the search form about `common\modules\quotes\models\Quotes`.
 */
class QuotesSearch extends Quotes
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'time', 'date_created', 'date_updated'], 'safe'],
            [['bid', 'ask', 'mid'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Quotes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'bid' => $this->bid,
            'ask' => $this->ask,
            'mid' => $this->mid,
            'time' => $this->time,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
