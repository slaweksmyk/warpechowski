<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('backend_module_quotes', 'Create Quotes');

?>
<div class="quotes-create">

    <p>
        <?= Html::a('< Wróć do listy', ['index'], ['class' => 'btn btn-outline-secondary']) ?>
    </p>
    
     <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
        </header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bid')->textInput() ?>

    <?= $form->field($model, 'ask')->textInput() ?>

    <?= $form->field($model, 'mid')->textInput() ?>

    <?= $form->field($model, 'time')->textInput() ?>

           <div class="form-group">
               <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_quotes', 'Create') : Yii::t('backend_module_quotes', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
           </div>

           <?php ActiveForm::end(); ?>
        </div>
    </section>

</div>
