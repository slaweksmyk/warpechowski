<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;
use yii\widgets\Pjax;
/* @var $searchModel common\modules\quotes\models\QuotesSearch */

$this->title = Yii::t('backend_module_quotes', 'Quotes');
?>
<div class="quotes-index">

    <p>
        <?= Html::a(Yii::t('backend_module_quotes', 'Create Quotes'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
<?php Pjax::begin(); ?>     <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <?php
 if(Yii::$app->request->get('per-page')){ echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>"; } ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?= GridView::widget([
                 'dataProvider' => $dataProvider,
                 'filterModel' => $searchModel,
        'columns' => [

                         'id',
            'name',
            'bid',
            'ask',
            'mid',
            // 'time',
            // 'date_created',
            // 'date_updated',

                     ['class' => 'common\hooks\yii2\grid\ActionColumn'],
                 ],
             ]); ?>
        </div>
    </section>
<?php Pjax::end(); ?></div>
