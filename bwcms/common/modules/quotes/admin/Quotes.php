<?php

namespace common\modules\quotes\admin;

/**
 * files module definition class
 */
class Quotes extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\quotes\admin\controllers';
    public $defaultRoute = 'quotes';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
}
