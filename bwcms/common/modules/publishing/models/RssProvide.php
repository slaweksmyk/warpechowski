<?php

namespace common\modules\publishing\models;

use Yii;
use common\modules\types\models\Type;
use common\modules\categories\models\ArticlesCategories;

/**
 * This is the model class for table "xmod_rss_provide".
 *
 * @property integer $id
 * @property string $name
 * @property integer $amount
 * @property integer $article_type_id
 * @property integer $article_category_id
 *
 * @property ArticlesCategories $articleCategory
 * @property Type $articleType
 */
class RssProvide extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_rss_provide}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount', 'article_type_id', 'article_category_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['article_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ArticlesCategories::className(), 'targetAttribute' => ['article_category_id' => 'id']],
            [['article_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Type::className(), 'targetAttribute' => ['article_type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_publishing', 'ID'),
            'name' => Yii::t('backend_module_publishing', 'Name'),
            'amount' => Yii::t('backend_module_publishing', 'Amount'),
            'article_type_id' => Yii::t('backend_module_publishing', 'Article Type ID'),
            'article_category_id' => Yii::t('backend_module_publishing', 'Article Category ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleCategory()
    {
        return $this->hasOne(ArticlesCategories::className(), ['id' => 'article_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleType()
    {
        return $this->hasOne(Type::className(), ['id' => 'article_type_id']);
    }
}
