<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\modules\types\models\Type;
use common\modules\categories\models\ArticlesCategories;

$this->title = Yii::t('backend_module_publishing', 'Update {modelClass}: ', [
    'modelClass' => 'Rss Provide',
]) . $model->name;
?>
<div class="rss-provide-update">

    <p>
        <?= Html::a('< Wróć  do listy', ['index'], ['class' => 'btn btn-outline-secondary']) ?>
    </p>

     <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
        </header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(); ?>

                <div class="row">
                    <div class="col-4">
                        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'article_category_id')->dropDownList(ArrayHelper::map(ArticlesCategories::find()->all(), 'id', 'name'), ["prompt" => "Wybierz kategorie"])->label("Kategoria artykyłów"); ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'article_type_id')->dropDownList(ArrayHelper::map(Type::find()->all(), 'id', 'name'), ["prompt" => "Wybierz typ"])->label("Typ artykułu"); ?>
                    </div>
                    <div class="col-2">
                        <?= $form->field($model, 'amount')->textInput() ?>
                    </div>
                </div>
            
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_publishing', 'Create') : Yii::t('backend_module_publishing', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

           <?php ActiveForm::end(); ?>
        </div>
    </section>

</div>
