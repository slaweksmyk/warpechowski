<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;

/* @var $searchModel common\modules\publishing\models\RssProvideSearch */

$this->title = Yii::t('backend_module_publishing', 'Rss Provides');
?>
<div class="rss-provide-index">

    <p>
        <?= Html::a(Yii::t('backend_module_publishing', 'Create Rss Provide'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
     <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <?php if(Yii::$app->request->get('per-page')){ echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>"; } ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    'name',
                    'amount',
                    [
                        'attribute' => 'article_type_id',
                        'value' => function($model){
                            $oType = $model->getArticleType()->one();
                            if($oType){
                                return $oType->name;
                            }
                        }
                    ],
                    [
                        'attribute' => 'article_category_id',
                        'value' => function($model){
                            $oCategory = $model->getArticleCategory()->one();
                            if($oCategory){
                                return $oCategory->name;
                            }
                        }
                    ],

                    ['class' => 'common\hooks\yii2\grid\ActionColumn'],
                 ],
             ]); ?>
        </div>
    </section>
</div>
