<?php

namespace common\modules\publishing\admin;

/**
 * files module definition class
 */
class Publishing extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\publishing\admin\controllers';
    public $defaultRoute = 'publishing';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
