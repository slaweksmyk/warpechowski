<section class="layout-structure" data-add="<?php echo \Yii::$app->request->baseUrl.'/widgets/widget/add/'; ?>" data-remove="<?php echo \Yii::$app->request->baseUrl.'/widgets/widget/remove/'; ?>" data-sort="<?php echo \Yii::$app->request->baseUrl.'/widgets/widget/sort/'; ?>" data-layout="<?php echo $layout_id; ?>" data-page="<?php echo $page_id; ?>" data-confirm="<?php echo \Yii::t('backend', 'confirm_delete'); ?>" >
    <h3><?php echo \Yii::t('backend', 'module_widgets'); ?>:</h3>
    <?php foreach ($positions as $key => $group) : ?>
        <div class='text-center' ><b>- <?php echo $key; ?> -</b></div>
        <div class="row" >
            <?php foreach ($group as $key => $pos) : ?>
                <div class="<?php echo $pos['class']; ?>" >
                    <div class="pos" >
                        <h4>
                            <?php echo $pos['name']; ?>
                            <span class="layout-add-new-widget" title="<?php echo \Yii::t('backend', 'module_widgets_create'); ?>" >
                                <img src="/bwcms/common/modules/layouts/admin/assets/img/plus.png" alt="add" />
                            </span>
                        </h4>
                        <div class="body" data-position="<?php echo $key; ?>" >
                            <?php 
                            foreach ($add_items as $widget_item) {
                                if( $widget_item['position'] == $key ){ echo Yii::$app->WidgetHelper->getWidgetItem( $widget_item, $type ); }
                            }
                            ?>
                        </div> 
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endforeach; ?>
</section>


<!-- Modal -->
<div id="widgetsList" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo Yii::t('backend', 'menu_widgets'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="w-list" >
                    <?php echo $widget_list; ?>
                </div>
                
                <div class="preload" >
                    <img src="/bwcms/common/modules/layouts/admin/assets/img/spin.svg" alt="preload" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal"><?php echo Yii::t('backend', 'close'); ?></button>
            </div>
        </div>
    </div>
</div>