<div class="widget<?php echo $is_active ? ' active' : ' unactive' ; if($type == 'page' && $page_id == 0){ echo ' l-on-p'; } ?>" data-id="<?php echo $id; ?>" >
    <div class="block" >
        <div class="content" >
            <b><?php echo Yii::t('backend', 'name'); ?>:</b> <?php echo $custom_name; ?>
            <br>
            <small>
                <b>Widget <?php echo $group; ?>:</b> <?php echo $dafault_name; ?>
            </small>
        </div>
        <div class="buttons" >
            <span class="glyphicon glyphicon-triangle-top" data-id="<?php echo $id; ?>" ></span>
            <span class="glyphicon glyphicon-triangle-bottom" data-id="<?php echo $id; ?>" ></span>
        </div>
    </div>
    <hr>
    <div class="block" >
        <div class="content" >
            <small><b><?php echo Yii::t('backend', 'add_in').':</b> '.($page_id > 0 ? Yii::t('backend', 'page') : Yii::t('backend', 'layout') ); ?></small>
            <br>
            <small>
                <b>id:</b> <?php echo $id; ?>
            </small>|<small>
                <b><?php echo Yii::t('backend', 'is_active').':</b> '.($is_active ? Yii::t('backend', 'yes') : Yii::t('backend', 'no') ); ?>
            </small>
        </div>
        <div class="buttons" >
            <a href="<?php echo \Yii::$app->request->baseUrl.'/widgets/widget/edit/?id='.$id; ?>" ><span class="glyphicon glyphicon-pencil" data-id="<?php echo $id; ?>" ></span></a>
            <span class="glyphicon glyphicon-trash" data-id="<?php echo $id; ?>" ></span>
        </div>
    </div>
</div>