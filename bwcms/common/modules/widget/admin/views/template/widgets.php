<!-- WIDGETS LIST -->
<section>
    <?php 
        $group = false; 
        foreach($widgets as $widget) { 
            if($widget->group !== $group){
                $group = $widget->group;
                echo "<p class='title' ><b>{$group}:</b></p>";
            }
            echo "<p class='item' data-id='{$widget->id}' >- {$widget->name}</p>";  
        }
    ?>
</section>