<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\widget\models\WidgetMain */

$this->title = Yii::t('backend', 'module_widgets_create');
?>
<div class="widget-main-create">
    
    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend', 'update') ?> <?= Yii::t('backend', 'data') ?></header>
        <div class="panel-body" >
            <?php echo $this->render('_form', [
                'model' => $model,
            ]); ?>
        </div>
    </section> 

</div>
