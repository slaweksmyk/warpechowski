<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\widget\models\WidgetMain */

$this->title = 'Widget: '.$model->name;
?>
<div class="widget-main-view">

    <p>
        <?php echo Html::a(Yii::t('backend', 'update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']); ?>
        <?php echo Html::a(Yii::t('backend', 'delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'delete_confirm'),
                'method' => 'post',
            ],
        ]); ?>
    </p>

    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend', 'module_widget_data') ?></header>
        <div class="panel-body" >
            <?php echo DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'name',
                    'action',
                    'group',
                ],
            ]); ?>
        </div>
    </section>
            
</div>
