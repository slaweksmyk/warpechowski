<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('backend', 'module_widgets_update').' '.$model->custom_name.' <small>('.($model->getPage()->one() ? $model->getPage()->one()->name : "brak" ).')</small>';
if( empty($referrer) ){ $referrer = Yii::$app->request->referrer; }
?>

<div class="widget-main-update">

    <p>
        <a class="btn btn-outline-secondary" href="<?php echo $referrer; ?>" >< <?= Yii::t('backend', 'return') ?></a>
        
        <?php echo Html::a(Yii::t('backend', 'delete'), ['removepost', 'id' => $model->id, 'referrer' => $referrer], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'delete_confirm'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    
    <?php $form = ActiveForm::begin(); ?>
        <div class="row">
            <div class="col-6">
                <section class="panel full" >
                    <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Dane podstawowe</header>
                    <div class="panel-body">
                        <div class="row">
                            <input type="hidden" name="widgetReferrer" value="<?php echo $referrer; ?>" />
                            <div class="col-12 col-sm-9" >
                                <?php echo $form->field($model, 'custom_name')->textInput(['maxlength' => 255])->label( Yii::t('backend', 'custom_name') ); ?>
                            </div>
                            <div class="col-12 col-sm-3" >
                                <?php echo $form->field($model, 'sort')->textInput(['type' => 'number'])->label("Kolejność"); ?>
                            </div>
                            <div class="col-12 col-sm-9" >
                                <?php echo $form->field($model, 'position')->dropDownList( $positions )->label( Yii::t('backend', 'position') ); ?>
                            </div>
                            <div class="col-12 col-sm-3" >
                                <?php echo $form->field($model, 'is_active')->dropDownList( array( 0 => 'Nie', 1 => 'Tak' ) )->label( Yii::t('backend', 'is_active') ); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12" style="text-align: center; padding-top: 10px;">
                                <div class="form-group" style="margin-bottom: 0px;">
                                    <?= Html::submitButton( Yii::t('backend', 'update'), ['class' => 'btn btn-primary']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-6">
                <section class="panel full" >
                    <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> 
                        Widoczność na urządzeniach
                        <i class="glyphicon glyphicon-info-sign pull-right" data-toggle="popover" data-placement="bottom" data-content="<?= "common/widgets/".$model->getWidget()->one()->folder ?>" style="margin-right: 15px;"></i>
                    </header>
                    <div class="panel-body" >
                        <div class="row">
                            <div class="col-2">
                                <img src="<?= \Yii::$app->request->baseUrl ?>images/ico/desktop.png" style="margin: 0 auto; display: block; margin-top: 20px;" alt="pc" data-toggle="tooltip" data-placement="bottom" title="PC"/>
                            </div>
                            <div class="col-3">
                                <?= $form->field($model, 'data[layout_enabled]')->dropDownList([1 => "aktywny", 0 => "nieaktywny"])->label("Widoczność"); ?>
                            </div>
                            <div class="col-4">
                                <?= $form->field($model, 'data[layout]')->dropDownList( Yii::$app->WidgetHelper->getWidgetLayouts( $widget ) )->label( Yii::t('backend', 'layout') ); ?>
                            </div>
                            <div class="col-3">
                                <?php echo $form->field($model, 'data[limit_pc]')->textInput(["type" => "number"])->label("Limit"); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-2">
                                <img src="<?= \Yii::$app->request->baseUrl ?>images/ico/tablet.png" style="margin: 0 auto; display: block; margin-top: 20px;" alt="tablet" data-toggle="tooltip" data-placement="bottom" title="Tablet"/>
                            </div>
                            <div class="col-3">
                                <?= $form->field($model, 'data[layout_tablet_enabled]')->dropDownList([1 => "aktywny", 0 => "nieaktywny"])->label("Widoczność"); ?>
                            </div>
                            <div class="col-4">
                                <?= $form->field($model, 'data[layout_tablet]')->dropDownList( Yii::$app->WidgetHelper->getWidgetLayouts( $widget ) )->label( Yii::t('backend', 'layout') ); ?>
                            </div>
                            <div class="col-3">
                                <?php echo $form->field($model, 'data[limit_tablet]')->textInput(["type" => "number"])->label("Limit"); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-2">
                                <img src="<?= \Yii::$app->request->baseUrl ?>images/ico/phone.png" style="margin: 0 auto; display: block; margin-top: 20px;" alt="mobile" data-toggle="tooltip" data-placement="bottom" title="Mobile"/>
                            </div>
                            <div class="col-3">
                                <?= $form->field($model, 'data[layout_mobile_enabled]')->dropDownList([1 => "aktywny", 0 => "nieaktywny"])->label("Widoczność"); ?>
                            </div>
                            <div class="col-4">
                                <?= $form->field($model, 'data[layout_mobile]')->dropDownList( Yii::$app->WidgetHelper->getWidgetLayouts( $widget ) )->label( Yii::t('backend', 'layout') ); ?>
                            </div>
                            <div class="col-3">
                                <?php echo $form->field($model, 'data[limit_mobile]')->textInput(["type" => "number"])->label("Limit"); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12" style="text-align: center; padding-top: 10px;">
                                <div class="form-group" style="margin-bottom: 0px;">
                                    <?= Html::submitButton( Yii::t('backend', 'update'), ['class' => 'btn btn-primary']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-12">
                <div class="row">
                    <div class="col-12" style="text-align: center;">
                        <hr/>
                        <b>Konfiguracja dodatkowa</b>
                        <hr/>
                    </div>
                </div>
            </div>
            
            <div class="col-12">
                <section class="panel full" >
                    <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Ustawienia dodatkowe</header>
                    <div class="panel-body" >
                        <?php echo Yii::$app->controller->renderFile($dataItemView, [ 'form' => $form, 'model' => $model, 'positions' => $positions, 'widget' => $widget, 'referrer' => $referrer ]); ?>
                    </div>
                </section>
            </div>
            
        </div>
    <?php ActiveForm::end(); ?>
    
</div>
