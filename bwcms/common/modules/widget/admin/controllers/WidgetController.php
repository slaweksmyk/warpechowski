<?php
/**
 * Widgets
 * 
 * PHP version 7
 *
 * @author Volodymyr Shelelo <contact@profficode.com>
 * @copyright (c) 2016 Volodymyr Shelelo
 * @version 1.0
 */

namespace common\modules\widget\admin\controllers;

use Yii;
use common\modules\widget\models\WidgetMain;
use common\modules\widget\models\WidgetMainSearch;
use common\modules\widget\models\WidgetItems;
use common\modules\layouts\models\PagesSiteLayout;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * WidgetController implements the CRUD actions for WidgetMain model.
 */
class WidgetController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'removepost' => ['POST']
                ],
            ],
        ];
    }

    /**
     * Lists all WidgetMain models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new WidgetMainSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if(Yii::$app->request->get('per-page')){
            $dataProvider->setPagination(['pageSize' => Yii::$app->request->get('per-page')]);
        }
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single WidgetMain model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new WidgetMain model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new WidgetMain();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing WidgetMain model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing WidgetMain model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        
        return $this->redirect(['index']);
    }

    /**
     * Finds the WidgetMain model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return WidgetMain the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = WidgetMain::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    
    //--------------------------------------------------------------------------
    //----- WIGET ITEMS --------------------------------------------------------
    //--------------------------------------------------------------------------
    
    /**
     *  Add Item widget (AJAX)
     */
    public function actionAdd()
    {
        $widgetModel = new WidgetMain();
        $itemsModel = new WidgetItems();
        
        $widgetID  = Yii::$app->request->post('addw_widget_id');
        $layoutID = Yii::$app->request->post('addw_layout_id');
        $pageID = Yii::$app->request->post('addw_page_id');
        $position = Yii::$app->request->post('addw_position');
        
        $widget = $widgetModel->findOne($widgetID);

        if($widget) {
            $lastSort = $itemsModel->find()->select('sort')->where([ 'layout_id' => $layoutID, 'position' => $position ])->orderBy('sort DESC')->one();
            
            if($lastSort){ $nextSort = $lastSort['sort'] + 1; } 
            else { $nextSort = 1; }
            
            if($pageID == 0){ $pageID = null; }
            
            $itemsModel->sort = $nextSort;
            $itemsModel->widget_id = $widgetID;
            $itemsModel->layout_id = $layoutID;
            $itemsModel->page_id = $pageID;
            $itemsModel->position = $position;
            $itemsModel->is_active = 0;
            $itemsModel->custom_name = Yii::t('backend', 'no_name');
            
            if($pageID > 0){ $typeW = 'page'; } 
            else { $typeW = 'layout'; }
            
            $save = $itemsModel->save();
        } else {
            $save = false;
        }
        
        if( $save ){
            $id = $itemsModel->getPrimaryKey();
            return $id;
            /*$view = Yii::getAlias('@common/modules/widget/admin/views/template/item.php');

            return $this->renderFile($view, [
                'id' => $id,
                'group' => $widget->group,
                'dafault_name' => $widget->name,
                'custom_name' => $itemsModel->custom_name,
                'is_active' => $itemsModel->is_active,
                'page_id' => $itemsModel->page_id,
                'type'  => $typeW
            ]);*/
        }
    }
    
    
    /**
     * Remove widget item (AJAX)
     */
    public function actionRemove()
    {
        $itemsModel = new WidgetItems();
        $itemID  = Yii::$app->request->post('remw_id');
        $item = $itemsModel->findOne($itemID);
        
        if( $item ){
            $item->delete();
        }
    }
    
    
    /**
     * Remove widget item (POST)
     */
    public function actionRemovepost($id, $referrer)
    {
        $itemsModel = new WidgetItems();
        $item = $itemsModel->findOne($id);
        
        if( $item ){
            $delete = $item->delete();
        }
        if( $delete ){
            return $this->redirect( $referrer );
        }
    }
    
    
    /**
     * Sort widget in position
     */
    public function actionSort()
    {
        $itemID  = Yii::$app->request->post('sortw_id');
        $sort = Yii::$app->request->post('sortw_sort');
        
        $itemsModel = new WidgetItems();
        $item = $itemsModel->findOne($itemID);
        
        $all = $itemsModel->find()->select(['id'])->where([ 'layout_id' => $item->layout_id, 'position' => $item->position ])->orderBy('sort ASC')->all();
        $key = array_search($itemID, array_column($all, 'id'));

        switch ($sort) {
            case 'top':
                $secondID = $all[$key - 1]['id']; 
                $second = $itemsModel->findOne($secondID);
                $item->sort = $item->sort - 1;
                $second->sort = $second->sort + 1;
                break;
            
            case 'bottom':
                $secondID = $all[$key + 1]['id']; 
                $second = $itemsModel->findOne($secondID);
                $item->sort = $item->sort + 1;
                $second->sort = $second->sort - 1;
                break;
        }
        
        $item->save();
        $second->save();
    }
    
    
    /**
     * Edit Widget item data
     * 
     * @param int $id
     * @return view
     */
    public function actionEdit($id)
    {
        $widgetModel = new WidgetMain();
        $itemsModel = new WidgetItems();
        $layoutModel = new PagesSiteLayout();
        $needSave = false; 
        $changePos = false;
        $widgetReferrer = false;

        $item = $itemsModel->findOne($id);
        $widget = $widgetModel->findOne($item->widget_id);
        $layout = $layoutModel->findOne($item->layout_id);

        $positions = Yii::$app->LayoutsHelper;
        $positions = $positions::getLayoutPositions($layout->layout);

        $posList = array();
        
        foreach ($positions as $section){
            foreach ($section as $pos => $val){
                $posList[$pos] = $val['name'];
            }
        }
        
        $dataItemView = Yii::getAlias('@common/widgets/'.$widget->folder.'/data.php');

        if ( $item->load(Yii::$app->request->post()) ) {
            $needSave = true;
            if( $item->isAttributeChanged('position') ){ $changePos = true; }
            
            $PostItem = Yii::$app->request->post('WidgetItems');

            $item->data = serialize($PostItem['data']);
        }
        
        if( $changePos ){
            $lastSort = $itemsModel->find()->select('sort')->where([ 'layout_id' => $item->layout_id, 'position' => $item->position ])->orderBy('sort DESC')->one();
            if($lastSort){ $nextSort = $lastSort['sort'] + 1; } 
            else { $nextSort = 1; }
            $item->sort = $nextSort;
        }
        
        if( $needSave ){ //Save changes
            $item->save(); 
            $widgetReferrer = Yii::$app->request->post('widgetReferrer');
        } 
        
        if( $item->data && is_string($item->data) ){ $item->data = unserialize($item->data); }
        
        return $this->render('edit', [
            'dataItemView' => $dataItemView,
            'model' => $item,
            'widget' => $widget,
            'positions' => $posList,
            'referrer' => $widgetReferrer
        ]);
    }
    
    
}
