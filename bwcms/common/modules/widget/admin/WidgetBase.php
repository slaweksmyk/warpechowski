<?php

namespace common\modules\widget\admin;

/**
 * WidgetBase module definition class
 */
class WidgetBase extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\widget\admin\controllers';
    public $defaultRoute = 'widget';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
