<?php

namespace common\modules\widget\models;

use Yii;
use common\modules\widget\models\WidgetMain;

/**
 * This is the model class for table "widget_group".
 *
 * @property integer $id
 * @property string $name
 *
 * @property WidgetMain[] $widgets
 */
class WidgetGroup extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%widget_group}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWidgets()
    {
        return $this->hasMany(WidgetMain::className(), ['group' => 'name']);
    }
}
