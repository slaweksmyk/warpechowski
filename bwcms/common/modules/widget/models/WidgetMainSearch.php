<?php

namespace common\modules\widget\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\widget\models\WidgetMain;

/**
 * WidgetMainSearch represents the model behind the search form about `common\modules\widget\models\WidgetMain`.
 */
class WidgetMainSearch extends WidgetMain
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'action', 'group'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WidgetMain::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'action', $this->action])
            ->andFilterWhere(['like', 'group', $this->group]);

        return $dataProvider;
    }
}
