<?php

namespace common\modules\widget\models;

use Yii;
use common\modules\widget\models\WidgetGroup;
use common\modules\widget\models\WidgetItems;
use common\modules\modules\models\Module;

/**
 * This is the model class for table "widget".
 *
 * @property string $id
 * @property string $module_id
 * @property string $name
 * @property string $action
 * @property string $folder
 * @property string $group
 *
 * @property WidgetGroup $group0
 * @property Module $module
 * @property WidgetItems[] $widgetItems
 */
class WidgetMain extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%widget}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['module_id'], 'integer'],
            [['name', 'action', 'folder', 'group'], 'string', 'max' => 255],
            [['group'], 'exist', 'skipOnError' => true, 'targetClass' => WidgetGroup::className(), 'targetAttribute' => ['group' => 'name']],
            [['module_id'], 'exist', 'skipOnError' => true, 'targetClass' => Module::className(), 'targetAttribute' => ['module_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_widget', 'ID'),
            'module_id' => Yii::t('backend_module_widget', 'Module ID'),
            'name' => Yii::t('backend_module_widget', 'Name'),
            'action' => Yii::t('backend_module_widget', 'Action'),
            'folder' => Yii::t('backend_module_widget', 'Folder'),
            'group' => Yii::t('backend_module_widget', 'Group'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup0()
    {
        return $this->hasOne(WidgetGroup::className(), ['name' => 'group']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModule()
    {
        return $this->hasOne(Module::className(), ['id' => 'module_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWidgetItems()
    {
        return $this->hasMany(WidgetItems::className(), ['widget_id' => 'id']);
    }
}
