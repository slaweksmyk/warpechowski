<?php

namespace common\modules\widget\models;

use Yii;
use common\modules\widget\models\WidgetMain;
use common\modules\pages\models\PagesSite;
use common\modules\layouts\models\PagesSiteLayout;
use common\modules\widget\models\WidgetItems_i18n;

use creocoder\translateable\TranslateableBehavior;

/**
 * This is the model class for table "widget_items".
 *
 * @property string $id
 * @property string $widget_id
 * @property string $layout_id
 * @property string $page_id
 * @property string $position
 * @property string $sort
 * @property string $custom_name
 * @property integer $is_active
 * @property string $data
 *
 * @property Widget $widget
 * @property PagesSiteLayout $layout
 * @property PagesSite $page
 * @property WidgetItemsI18n[] $widgetItemsI18ns
 */
class WidgetItems extends \common\hooks\yii2\db\ActiveRecord
{
    protected $aTranslateColumns = ['is_active', 'data', 'custom_name'];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%widget_items}}';
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => $this->aTranslateColumns,
                'translationLanguageAttribute' => 'language',
            ],
        ];
    } 
    
    /**
     * @inheritdoc
     */
    public function getTranslations()
    {
        return $this->hasMany(WidgetItems_i18n::className(), ['widget_items_id' => 'id']);
    }    

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['widget_id', 'layout_id', 'page_id', 'sort', 'is_active'], 'integer'],
            [['data'], 'string'],
            [['position', 'custom_name'], 'string', 'max' => 255],
            [['widget_id'], 'exist', 'skipOnError' => true, 'targetClass' => WidgetMain::className(), 'targetAttribute' => ['widget_id' => 'id']],
            [['layout_id'], 'exist', 'skipOnError' => true, 'targetClass' => PagesSiteLayout::className(), 'targetAttribute' => ['layout_id' => 'id']],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => PagesSite::className(), 'targetAttribute' => ['page_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_widget', 'ID'),
            'widget_id' => Yii::t('backend_module_widget', 'Widget ID'),
            'layout_id' => Yii::t('backend_module_widget', 'Layout ID'),
            'page_id' => Yii::t('backend_module_widget', 'Page ID'),
            'position' => Yii::t('backend_module_widget', 'Position'),
            'sort' => Yii::t('backend_module_widget', 'Sort'),
            'custom_name' => Yii::t('backend_module_widget', 'Custom Name'),
            'is_active' => Yii::t('backend_module_widget', 'Is Active'),
            'data' => Yii::t('backend_module_widget', 'Data'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWidget()
    {
        return $this->hasOne(WidgetMain::className(), ['id' => 'widget_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLayout()
    {
        return $this->hasOne(PagesSiteLayout::className(), ['id' => 'layout_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(PagesSite::className(), ['id' => 'page_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWidgetItemsI18ns()
    {
        return $this->hasMany(WidgetItemsI18n::className(), ['widget_items_id' => 'id']);
    }
    
    public function __set($name, $value) {
        if(in_array($name, $this->aTranslateColumns)){
            $this->translate(Yii::$app->session->get('backend_language'))->{$name} = $value;
        }
        
        parent::__set($name, $value);
    }
    
    public function __get($name) {
        if(in_array($name, $this->aTranslateColumns)){
            if(strpos(Yii::$app->request->url, \Yii::$app->request->baseUrl) !== false){
                $translatedAttr = $this->translate(Yii::$app->session->get('backend_language'))->{$name};
            } else {
                $translatedAttr = $this->translate(Yii::$app->session->get('frontend_language'))->{$name};
            }

            if($translatedAttr){
                return $translatedAttr;
            }
        }

        return parent::__get($name);
    }
    
}
