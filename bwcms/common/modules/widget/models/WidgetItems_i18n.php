<?php

namespace common\modules\widget\models;

use Yii;

/**
 * This is the model class for table "widget_items_i18n".
 *
 * @property integer $id
 * @property string $widget_items_id
 * @property string $language
 * @property string $custom_name
 * @property integer $is_active
 * @property string $data
 *
 * @property WidgetItems $widgetItems
 */
class WidgetItems_i18n extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%widget_items_i18n}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['widget_items_id', 'is_active'], 'integer'],
            [['data'], 'string'],
            [['language', 'custom_name'], 'string', 'max' => 255],
            [['widget_items_id'], 'exist', 'skipOnError' => true, 'targetClass' => WidgetItems::className(), 'targetAttribute' => ['widget_items_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'widget_items_id' => 'Widget Items ID',
            'language' => 'Language',
            'custom_name' => 'Custom Name',
            'is_active' => 'Is Active',
            'data' => 'Data',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWidgetItems()
    {
        return $this->hasOne(WidgetItems::className(), ['id' => 'widget_items_id']);
    }
}
