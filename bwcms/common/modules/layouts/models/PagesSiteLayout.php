<?php
/**
 * Module Layouts - main model
 * 
 * PHP version 7
 *
 * @author Volodymyr Shelelo <contact@profficode.com>
 * @copyright (c) 2016 Volodymyr Shelelo
 * @version 1.0
 */

namespace common\modules\layouts\models;

use Yii;

/**
 * This is the model class for table "pages_site_layout".
 *
 * @property integer $id
 * @property string $name
 * @property string $layout
 */
class PagesSiteLayout extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pages_site_layout}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'layout'], 'string', 'max' => 255],
            [['name', 'layout'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'layout' => 'Layout',
        ];
    }
}
