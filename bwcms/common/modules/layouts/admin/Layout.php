<?php
/**
 * Module: Pages Layouts (backend)
 * 
 * Module init
 * 
 * PHP version 7
 *
 * @author Volodymyr Shelelo <contact@profficode.com>
 * @copyright (c) 2016 Volodymyr Shelelo
 * @version 1.0
 */

namespace common\modules\layouts\admin;

/**
 * layout module definition class
 */
class Layout extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\layouts\admin\controllers';
    public $defaultRoute = 'layout';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
