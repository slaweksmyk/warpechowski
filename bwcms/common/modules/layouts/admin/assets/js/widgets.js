
function widgetAddEvents(){
    
    var $positionBody = null,
        $position = null;
    
    $('.layout-add-new-widget').on('click', function(){
        $positionBody = $(this).parent().next('.body');
        $position = $positionBody.data('position');
        
        $("#widgetsList").modal();
    });

    // click events ---

    $('.w-list .item').on('click', function(){
        var $widgetID = $(this).data('id');
        addWidgetAJAX($positionBody, $position, $widgetID);
    });
    
    $("section.layout-structure .pos .body").on('click', '.glyphicon-trash', function(){
        var $itemID = $(this).data('id');
        var $widget = $(this).parent().parent().parent();
        removeWidgetAJAX($itemID, $widget);
    });
    
    $("section.layout-structure .pos .body").on('click', '.glyphicon-triangle-top', function(){
        var $itemID = $(this).data('id');
        var $widget = $(this).parent().parent().parent();
        sortWidgetAJAX($itemID, 'top', $widget);
    });
    
    $("section.layout-structure .pos .body").on('click', '.glyphicon-triangle-bottom', function(){
        var $itemID = $(this).data('id');
        var $widget = $(this).parent().parent().parent();
        sortWidgetAJAX($itemID, 'bottom', $widget);
    });
    
}
widgetAddEvents();

// ADD WIDGET ------------------------------------------------------------------

function addWidgetAJAX($positionBody, $position, $widgetID){
    var $url = $('.layout-structure').data('add');
    var $layoutID = $('.layout-structure').data('layout');
    var $pageID = $('.layout-structure').data('page');
    
    $("#widgetsList .preload").addClass('active');
    
    $.ajax({
        method: "POST",
        url: $url,
        data: { addw_position: $position, addw_widget_id: $widgetID, addw_layout_id: $layoutID, addw_page_id: $pageID }
    }).done(function( $msg ) {
        //$positionBody.append($msg);
        //$("#widgetsList .preload").removeClass('active');
        //$("#widgetsList").modal('hide');
        
        window.location.href = $("#homeDir").text()+"widgets/widget/edit/?id="+$msg;
    });   
}

// REMOVE WIDGET ---------------------------------------------------------------

function removeWidgetAJAX($itemID, $widget){
    var $url = $('.layout-structure').data('remove'),
        $confirm = $('.layout-structure').data('confirm');
        $widget.parent().addClass('preload');

    if ( confirm($confirm) ) {
        $.ajax({
            method: "POST",
            url: $url,
            data: { remw_id: $itemID }
        }).done(function() {
            $widget.parent().removeClass('preload');
            $widget.remove();
            location.reload();
        });
    }
}


// SORT WIDGET -----------------------------------------------------------------

function sortWidgetAJAX($itemID, $action, $widget){
    var $ajax = false, 
        $url = $('.layout-structure').data('sort');
    
    if( $action === 'top' && $widget.prev().hasClass('widget') ){
        $ajax = true;
    } else if( $action === 'bottom' && $widget.next().hasClass('widget') ){
        $ajax = true;
    }
    
    if($ajax){
        $widget.parent().addClass('preload');
        
        $.ajax({
            method: "POST",
            url: $url,
            data: { sortw_id: $itemID, sortw_sort: $action }
        }).done(function() {
            if( $action === 'top' ){
                var $prevW = $widget.prev().clone( true );
                $widget.prev().remove();
                $widget.after($prevW);
            } else {
                var $nextW = $widget.next().clone( true );
                $widget.next().remove();
                $widget.before($nextW);
            }
            
            $widget.parent().removeClass('preload');
        });
    }
}


// vertical align MODAL --------------------------------------------------------

$(function(){
    function reposition() {
        var modal = $(this),
            dialog = modal.find('.modal-content');
            modal.css('display', 'block');
            dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2.5));
    }
    // Reposition when a modal is shown
    $('.modal').on('show.bs.modal', reposition);
    // Reposition when the window is resized
    $(window).on('resize', function() {
        $('.modal:visible').each(reposition);
    });
});

