<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\layouts\models\PagesSiteLayout */

$this->title = \Yii::t('backend', 'menu_layouts').': '.$model->name;

//add CSS
$this->registerCssFile('/bwcms/common/modules/layouts/admin/assets/css/template.css');

//add JS
$this->registerJsFile('/bwcms/common/modules/layouts/admin/assets/js/widgets.js', ['depends' => [yii\web\JqueryAsset::className()]]);
?>

<div class="pages-site-layout-view">

    <p>
        <?php echo Html::a(\Yii::t('backend', 'update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']); ?>
        <?php echo Html::a(\Yii::t('backend', 'delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'delete_confirm'),
                'method' => 'post',
            ],
        ]); ?>
    </p>

    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?php echo \Yii::t('backend', 'data'); ?></header>
        <div class="panel-body" >
            <?php echo DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    [
                        'attribute' => 'name',
                        'label' => \Yii::t('backend', 'name')
                    ],
                    [
                        'attribute' => 'layout',
                        'label' => \Yii::t('backend', 'file')
                    ],
                ],
            ]); ?>
            
            <?php echo $structure; ?>
        </div>
    </section>

</div>
