<?php

/* @var $this yii\web\View */
/* @var $model common\modules\layouts\models\PagesSiteLayout */

$this->title = \Yii::t('backend', 'menu_layouts_update').': ID'.$model->id;
?>

<div class="pages-site-layout-update">

    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?php echo \Yii::t('backend', 'data'); ?></header>
        <div class="panel-body" >
        <?php echo $this->render('_form', [
            'model' => $model,
            'layouts' => $layouts
        ]); ?>
        </div>
    </section>

</div>
