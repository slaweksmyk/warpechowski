<?php use yii\helpers\Html; use yii\widgets\ActiveForm; ?>

 <div class="pages-site-layout-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'name')->textInput(['maxlength' => true])->label(\Yii::t('backend', 'name_1')); ?>

    <?php $params = [ 'prompt' => '- '.\Yii::t('backend', 'select').' -' ]; ?>
    <?php echo $form->field($model, 'layout')->dropDownList($layouts, $params)->label(\Yii::t('backend', 'file')); ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? \Yii::t('backend', 'create') : \Yii::t('backend', 'update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>