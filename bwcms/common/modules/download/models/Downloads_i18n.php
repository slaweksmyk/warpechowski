<?php

namespace common\modules\download\models;

use Yii;

/**
 * This is the model class for table "{{%xmod_articles_i18n}}".
 *
 * @property integer $id
 * @property string $language
 * @property string $title
 * @property string $short_description
 * @property string $full_description
 */
class Downloads_i18n extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_downloads_i18n}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
            [['id', 'download_id'], 'integer'],
            [['language', 'name'], 'string', 'max' => 255],
        ];
    }
}
