<?php

namespace common\modules\download\models;

use Yii;
use common\modules\files\models\File;
use creocoder\translateable\TranslateableBehavior;
use common\modules\download\models\Downloads_i18n;
use common\modules\download\models\DownloadsCategories;

/**
 * This is the model class for table "{{%xmod_downloads}}".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $file_id
 * @property string $name
 * @property string $date_added
 *
 * @property XmodDownloadsCategories $category
 * @property Files $file
 */
class Downloads extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_downloads}}';
    }
    
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => ['title', 'short_description', 'full_description'],
                'translationLanguageAttribute' => 'language',
            ],
        ];
    } 
    
    public function getTranslations()
    {
        return $this->hasMany(Downloads_i18n::className(), ['download_id' => 'id']);
    }   

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'file_id'], 'integer'],
            [['date_added'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => DownloadsCategories::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['file_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['file_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_download', 'ID'),
            'category_id' => Yii::t('backend_module_download', 'Category ID'),
            'file_id' => Yii::t('backend_module_download', 'File ID'),
            'name' => Yii::t('backend_module_download', 'Name'),
            'date_added' => Yii::t('backend_module_download', 'Date Added'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(DownloadsCategories::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(File::className(), ['id' => 'file_id']);
    }
    
    /**
     * @inheritdoc
     */
    public function __set($name, $value) {
        if(in_array($name, ["name"])){
            $this->translate(Yii::$app->session->get('backend_language'))->{$name} = $value;
        }
        
        parent::__set($name, $value);
    }
    
    public function __get($name) {
        if(in_array($name, ["name"])){
            if(strpos(Yii::$app->request->url, \Yii::$app->request->baseUrl) !== false){
                $translatedAttr = $this->translate(Yii::$app->session->get('backend_language'))->{$name};
            } else {
                $translatedAttr = $this->translate(Yii::$app->session->get('frontend_language'))->{$name};
            }

            if($translatedAttr){
                return $translatedAttr;
            }
        }

        return parent::__get($name);
    }
    
}
