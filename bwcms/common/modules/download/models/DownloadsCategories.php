<?php

namespace common\modules\download\models;

use Yii;
use common\modules\download\models\Downloads;
use common\modules\download\models\DownloadsCategories_i18n;
use creocoder\translateable\TranslateableBehavior;

/**
 * This is the model class for table "{{%xmod_downloads_categories}}".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $name
 *
 * @property XmodDownloads[] $xmodDownloads
 * @property DownloadsCategories $parent
 * @property DownloadsCategories[] $downloadsCategories
 */
class DownloadsCategories extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_downloads_categories}}';
    }
    
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => ['title', 'short_description', 'full_description'],
                'translationLanguageAttribute' => 'language',
            ],
        ];
    } 
    
    public function getTranslations()
    {
        return $this->hasMany(DownloadsCategories_i18n::className(), ['download_category_id' => 'id']);
    }   

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => DownloadsCategories::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_download', 'ID'),
            'parent_id' => Yii::t('backend_module_download', 'Parent ID'),
            'name' => Yii::t('backend_module_download', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(Downloads::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(DownloadsCategories::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildrens()
    {
        return $this->hasMany(DownloadsCategories::className(), ['parent_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     */
    public function __set($name, $value) {
        if(in_array($name, ["name"])){
            $this->translate(Yii::$app->session->get('backend_language'))->{$name} = $value;
        }
        
        parent::__set($name, $value);
    }
    
    public function __get($name) {
        if(in_array($name, ["name"])){
            if(strpos(Yii::$app->request->url, \Yii::$app->request->baseUrl) !== false){
                $translatedAttr = $this->translate(Yii::$app->session->get('backend_language'))->{$name};
            } else {
                $translatedAttr = $this->translate(Yii::$app->session->get('frontend_language'))->{$name};
            }

            if($translatedAttr){
                return $translatedAttr;
            }
        }

        return parent::__get($name);
    }
    
}
