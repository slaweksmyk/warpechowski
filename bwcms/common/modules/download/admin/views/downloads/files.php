<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;
use common\modules\download\models\DownloadsCategories;

$this->title = Yii::t('backend_module_download', 'files-list');
?>
<div class="downloads-categories-index">

    <p>
        <?= Html::a("< Wróć do kategorii", ['index'], ['class' => 'btn btn-outline-secondary']) ?>
        <?= Html::a(Yii::t('backend_module_download', 'Add file'), ['add-file'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <?php if(Yii::$app->request->get('per-page')){ echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>"; } ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    'name',

                    [
                        'class' => 'common\hooks\yii2\grid\ActionColumn',
                        'template' => '{update-file} {delete-file}',
                        'buttons' => [
                            'update-file' => function ($url) {
                                return Html::a(
                                    '<span class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></span>',
                                    $url, 
                                    [
                                        'title' => Yii::t('backend', 'edit'),
                                        'data-pjax' => '0',
                                    ]
                                );
                            },
                            'delete-file' => function ($url) {
                                return Html::a(
                                    '<span class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></span>',
                                    $url, 
                                    [
                                        'title' => Yii::t('backend', 'delete'),
                                        'data-pjax' => '0',
                                    ]
                                );
                            },    
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </section>
    
    
    
</div>
