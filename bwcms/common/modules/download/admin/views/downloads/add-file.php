<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\modules\files\models\File;
use common\modules\download\models\DownloadsCategories;

$this->title = Yii::t('backend_module_download', 'Add file');
?>
<div class="downloads-categories-create">

    <p>
        <?= Html::a("< Wróć do listy", ['index'], ['class' => 'btn btn-outline-secondary']) ?>
    </p>

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
        </header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(); ?>
            
            <div class="row">
                <div class="col-12">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-6">
                    <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(DownloadsCategories::find()->all(), 'id', 'name')); ?>
                </div>
                <div class="col-6">
                    <?= $form->field($model, 'file_id')->dropDownList(ArrayHelper::map(File::find()->all(), 'id', 'name')); ?>
                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_download', 'Create') : Yii::t('backend_module_download', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </section>

</div>
