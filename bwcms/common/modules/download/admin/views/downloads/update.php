<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\modules\download\models\DownloadsCategories;

$this->title = Yii::t('backend_module_download', 'Update {modelClass}: ', [
    'modelClass' => 'Downloads Categories',
]) . $model->name;
?>

<p>
    <?= Html::a("< Wróć do listy", ['index'], ['class' => 'btn btn-outline-secondary']) ?>
</p>

<div class="downloads-categories-update">

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
        </header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(); ?>
            
            <div class="row">
                <div class="col-6">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-6">
                    <?= $form->field($model, 'parent_id')->dropDownList(ArrayHelper::map(DownloadsCategories::find()->all(), 'id', 'name'),['prompt'=>'- wybierz rodzica -']); ?>
                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_download', 'Create') : Yii::t('backend_module_download', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </section>

</div>
