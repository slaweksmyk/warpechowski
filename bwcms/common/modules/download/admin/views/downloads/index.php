<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;
use common\modules\download\models\DownloadsCategories;

$this->title = Yii::t('backend_module_download', 'files-category');
?>
<div class="downloads-categories-index">

    <p>
        <?= Html::a(Yii::t('backend_module_download', 'Create Downloads Categories'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
            <form id="per-page">
                <span><?= Yii::t('backend', 'quantity_per_page'); ?>:</span>
                <select name="per-page" class="form-control">
                    <?php if(Yii::$app->request->get('per-page')){ echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>"; } ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    [
                        'attribute' => 'parent_id',
                        'format' => 'raw',
                        'contentOptions'=>['style'=>'width: 145px;'],
                        'value' => function ($data) {
                            if(is_null($data["parent_id"])){ return ""; }
                            return DownloadsCategories::findOne(["id" => $data["parent_id"]])->name;
                        }
                    ],
                    'name',

                    
                    [
                        'class' => 'common\hooks\yii2\grid\ActionColumn',
                        'template' => '{update} {view} {files} {delete}',
                        'contentOptions' => ['style' => 'width: 130px;'],
                        'buttons' => [
                            'update' => function ($url) {
                                return Html::a(
                                                '<span class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></span>', $url, [
                                            'title' => Yii::t('backend', 'edit'),
                                            'data-toggle' => "tooltip",
                                            'data-placement' => "bottom"
                                                ]
                                );
                            },
                            'view' => function ($url) {
                                return Html::a(
                                                '<span class="btn btn-warning-yellow btn-xs"><span class="glyphicon glyphicon-eye-open"></span></span>', $url, [
                                            'title' => Yii::t('backend', 'view'),
                                            'data-toggle' => "tooltip",
                                            'data-placement' => "bottom"
                                                ]
                                );
                            },
                            'files' => function ($url) {
                                return Html::a(
                                                '<span class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-list-alt"></span></span>', $url, [
                                            'title' => Yii::t('backend', 'list-files'),
                                            'data-toggle' => "tooltip",
                                            'data-placement' => "bottom"
                                                ]
                                );
                            },
                            'delete' => function ($url) {
                                return Html::a(
                                                '<span class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></span>', $url, [
                                            'title' => Yii::t('backend', 'delete'),
                                            'data-toggle' => "tooltip",
                                            'data-placement' => "bottom",
                                            'data-pjax' => 0,
                                            'data-confirm' => 'Czy na pewno usunąć ten element?',
                                            'data-method' => 'post',
                                                ]
                                );
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </section>
    
    
    
</div>
