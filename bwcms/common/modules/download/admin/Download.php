<?php

namespace common\modules\download\admin;

/**
 * files module definition class
 */
class Download extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\download\admin\controllers';
    public $defaultRoute = 'downloads';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
