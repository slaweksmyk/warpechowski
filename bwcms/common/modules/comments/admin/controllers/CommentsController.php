<?php

namespace common\modules\comments\admin\controllers;

use Yii;
use common\modules\articles\models\ArticleComment;
use common\modules\articles\models\ArticleCommentSearch;
use common\modules\logs\models\Log;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CommentsController implements the CRUD actions for ArticleComment model.
 */
class CommentsController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ArticleComment models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new ArticleCommentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionStatus($id = null, $status = null) {

        $request = Yii::$app->request;

        if ($request->get('id') && $request->get('status')) {

            $oComment = $this->findModel($id);
            if ($status == 'accept') {
                $oComment->is_active = 1;
            } elseif ($status == 'decline') {
                $oComment->is_active = -1;
            }

            $oComment->save();
            Log::addRecord($this->module->id, "update", "Komentarz do artykułu: {$oComment->getArticle()->one()->title}", $oComment->id);
        }

        if ($request->post()) {

            $status = $request->post('status');

            if ($status == 'accept' || $status == 'decline') {

                foreach ($request->post('id') as $val) {

                    $oComment = $this->findModel($val);

                    if ($status == 'accept') {
                        echo
                        $oComment->is_active = 1;
                    } elseif ($status == 'decline') {
                        $oComment->is_active = -1;
                    }

                    $oComment->save();
                    Log::addRecord($this->module->id, "update", "Komentarz do artykułu: {$oComment->getArticle()->one()->title}", $oComment->id);
                }
            }
        }
        return true;
    }

    /**
     * Deletes an existing ArticleComment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ArticleComment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ArticleComment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = ArticleComment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
