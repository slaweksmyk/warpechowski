<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;
use yii\widgets\Pjax;

$this->registerJsFile('/bwcms/common/modules/comments/admin/assets/js/script.js');
$this->registerCssFile('/bwcms/common/modules/comments/admin/assets/css/style.css');

$this->title = Yii::t('backend_module_comments', 'Article Comments');

$this->registerJs(<<<JS
    $('body').on('click','[data-action="comment-action"]', function() { 
        var ob = $(this);
        ob.find('span').attr('class','').addClass('glyphicon glyphicon-repeat loading');
        
        var key = ob.closest('[data-key]').data('key');
        var action = ob.data('action-value');
       
        
        $.get($("#homeDir").text()+"comments/comments/status/", {id: key, status: action})
                .done(function (data) {
        
                    if(action=='accept'){
                        $('[data-key="'+ key +'"]').attr('class','').addClass('active-row');
                        $('[data-key="'+ key +'"]').find('[data-action="comment-action"][data-action-value="accept"] span').attr('class','').addClass('glyphicon glyphicon-ok');
                        $('[data-key="'+ key +'"]').find('.text-status').text('Aktywny');
       
                    }
                    if(action=='decline'){
        
                        $('[data-key="'+ key +'"]').attr('class','').addClass('inactive-row');
                        $('[data-key="'+ key +'"]').find('[data-action="comment-action"][data-action-value="decline"] span').attr('class','').addClass('glyphicon glyphicon-remove');
                        $('[data-key="'+ key +'"]').find('.text-status').text('Odrzucony');
                    }
                  
                });
        
        // $.pjax.reload({container:"#items"});
   });
        
         $('[data-action="multi-action"]').on('click', function() { 
         
         var select = $(this).parent().find('[data-item="action-select"] option:selected').val();
        $(this).addClass('disabled').text('Zmieniam...');
        
         var val = [];
        $(':checkbox:checked').each(function(i){
        
            val[i] = $(this).val();
        
            if(select=='accept'){
                $('[data-key="'+ val[i] +'"]').attr('class','').addClass('active-row');
                $('[data-key="'+ val[i] +'"]').find('.text-status').text('Aktywny');
            }
            if(select=='decline'){
                $('[data-key="'+ val[i] +'"]').attr('class','').addClass('inactive-row');
                $('[data-key="'+ val[i] +'"]').find('.text-status').text('Odrzucony');
        
            }
        
        });
        
        $.post($("#homeDir").text()+"comments/comments/status/", {id: val, status: select})
                .done(function (data) {
                    
                $('[data-action="multi-action"]').removeClass('disabled').text('Zastosuj');
                $('input[type="checkbox"]').prop("checked", false);
                    });
         });
JS
);
?>
<div class="article-comment-index">

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <?php
                    if (Yii::$app->request->get('per-page')) {
                        echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>";
                    }
                    ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >

            <div class="row">
                <div class="col-12">
                    <div class="form-inline">
                        <span class="action-text">Dla wybranych elementów:</span>
                        <div class="form-group">
                            <select class="form-control action-select action-action" data-item="action-select">
                                <option disabled selected>Wybierz akcje</option>
                                <option value="accept">Zatwierdź</option>
                                <option value="decline">Odrzuć</option>
                            </select>
                        </div>

                        <button class="btn btn-success" data-action="multi-action">Zastosuj</button>
                    </div>
                </div>
            </div>
            <hr>
            <?php Pjax::begin(['timeout' => 2000, 'id' => 'items']); ?> 
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'rowOptions' => function($model) {
                    if ($model->is_active == -1 || $model->is_active == -2 || $model->is_active == -3) {
                        return ['class' => 'inactive-row'];
                    }
                    if ($model->is_active == 0) {
                        return ['class' => 'new-row'];
                    }

                    if ($model->is_active == 1) {
                        return ['class' => 'active-row'];
                    }
                },
                'columns' => [
                    [
                        'class' => 'yii\grid\CheckboxColumn',
                    ],
                    'id',
                    [
                        'attribute' => 'article.title',
                        'format' => 'raw',
                        'contentOptions' => ['style' => 'width: 140px'],
                        'value' => function($model) {
                            $oArticle = $model->getArticle()->one();

                            if (!is_null($oArticle)) {
                                return "<a href='".\Yii::$app->params['serviceDomain']."/{$oArticle->getUrl()}' title='{$oArticle->title}' style='text-decoration: underline;' target='_blank'>{$oArticle->title}</a>";
                            }
                        }
                    ],
                    [
                        'attribute' => "text",
                        'format' => 'raw',
//                        'contentOptions' => ['title' => "Kliknij, aby rozwinąć treść", 'style' => 'width: 400px', 'class' => 'toggled-td'],
                        'value' => function($model) {
                            return nl2br($model->text);
                        }
                    ],
                    'author',
                    'date_created',
                    [
                        'attribute' => "is_active",
                        'filter' => [1 => 'Aktywne', 0 => 'Nowe', -1 => 'Odrzucone'],
                        'format' => 'raw',
                        'label' =>'Status',
                        'contentOptions' => ['class' => 'text-center text-status'],
                        'value' => function ($model) {
                                if($model->is_active == 1){
                                    return 'Aktywne';
                                }elseif($model->is_active == 0){
                                    return 'Nowe';
                                }else{
                                    return 'Odrzucone';
                                }
                                   
                        }
                    ],
                    [
                        'class' => 'common\hooks\yii2\grid\ActionColumn',
                        'template' => '{accept} {block} {delete}',
                        'contentOptions' => ['style' => 'width: 100px;'],
                        'buttons' => [
                            'accept' => function ($model) {
                                return '<span class="btn btn-success btn-xs" title="Zatwierdź" data-toggle="tooltip" data-action="comment-action" data-action-value="accept"><span class="glyphicon glyphicon-ok"></span></span>';
                            },
                            'block' => function ($data) {
                                return '<span class="btn btn-warning btn-xs" title="Odrzuć" data-toggle="tooltip" data-action="comment-action" data-action-value="decline"><span class="glyphicon glyphicon-remove"></span></span>';
                            }
                        ],
//                            'block' => function ($url) {
//                                return Html::button(
//                                                '<span class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-remove"></span></span>', $url, [
//                                            'title' => "Odrzuć",
//                                            'data-toggle' => "tooltip",
//                                            'data-placement' => "bottom"
//                                                ]
//                                );
//                            },
                    ],
                ],
            ]);
            ?>
            <?php Pjax::end() ?>
             <hr>
             <div class="row">
                <div class="col-12">
                    <div class="form-inline">
                        <span class="action-text">Dla wybranych elementów:</span>
                        <div class="form-group">
                            <select class="form-control action-select action-action" data-item="action-select">
                                <option disabled selected>Wybierz akcje</option>
                                <option value="accept">Zatwierdź</option>
                                <option value="decline">Odrzuć</option>
                            </select>
                        </div>

                        <button class="btn btn-success" data-action="multi-action">Zastosuj</button>
                    </div>
                </div>
            </div>
           
        </div>
    </section>
</div>
