<?php

namespace common\modules\comments\admin;

/**
 * files module definition class
 */
class Comments extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\comments\admin\controllers';
    public $defaultRoute = 'comments';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
}
