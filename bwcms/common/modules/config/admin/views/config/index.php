<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('backend_module_config', "main_title");

$config = yii\helpers\ArrayHelper::merge(
    [], require Yii::getAlias('@common')."/config/i18n.php"
);
?>
<div class="config-update">
    <?php $form = ActiveForm::begin(); ?>
        <section class="panel full" >
            <header>
                <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend_module_config', "section_main") ?>            
            </header>
            <div class="panel-body" >
                <div class="config-form">
                    <div class="row">
                        <div class="col-6"><?= $form->field($model, 'page_name')->textInput(['maxlength' => true]) ?></div>
                        <div class="col-2"><?= $form->field($model, 'default_language')->dropDownList(array_combine($config["languages"], $config["languages"])) ?></div>
                        <div class="col-2"><?= $form->field($model, 'is_ssl')->dropDownList([0 => Yii::t('backend', 'no'), 1 => Yii::t('backend', 'yes')]) ?></div>
                        <div class="col-2"><?= $form->field($model, 'is_url_www')->dropDownList([0 => Yii::t('backend_module_config', 'non-www'), 1 => Yii::t('backend_module_config', 'www')]) ?></div>
                    </div>

                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_config', 'Create') : "Zapisz zmiany", ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                </div>
            </div>
        </section>
    
        <section class="panel full" >
            <header>
                <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend_module_config', "section_smtp") ?>            
            </header>
            <div class="panel-body" >
                <div class="config-form">
                    <div class="row">
                        <div class="col-12"><?= $form->field($model, 'default_email')->textInput(['maxlength' => true]) ?></div>
                    </div>
                    <div class="row">
                        <div class="col"><?= $form->field($model, 'smtp_host')->textInput(['maxlength' => true]) ?></div>
                        <div class="col"><?= $form->field($model, 'smtp_username')->textInput(['maxlength' => true]) ?></div>
                        <div class="col"><?= $form->field($model, 'smtp_password')->passwordInput(['maxlength' => true]) ?></div>
                        <div class="col"><?= $form->field($model, 'smtp_port')->textInput() ?></div>
                        <div class="col"><?= $form->field($model, 'smtp_encryption')->dropDownList([null => "Brak", "SSL" => "SSL", "TLS" => "TLS"]) ?></div>
                    </div>
                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_config', 'Create') : "Zapisz zmiany", ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                </div>
            </div>
        </section>

    <?php ActiveForm::end(); ?>
</div>
