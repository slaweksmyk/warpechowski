<?php

namespace common\modules\config\admin;

/**
 * files module definition class
 */
class Config extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\config\admin\controllers';
    public $defaultRoute = 'config';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
