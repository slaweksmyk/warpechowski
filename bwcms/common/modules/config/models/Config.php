<?php

namespace common\modules\config\models;

use Yii;
use common\modules\files\models\File;

/**
 * This is the model class for table "configuration". 
 * 
 * @property integer $id
 * @property string $page_name
 * @property string $default_email
 * @property string $smtp_host
 * @property string $smtp_username
 * @property string $smtp_password
 * @property integer $smtp_port
 * @property integer $cache_database
 * @property integer $cache_database_duration
 * @property integer $cache_full_page
 * @property integer $cache_full_page_time
 * @property integer $cache_assets
 * @property integer $cache_assets_js_compile
 * @property integer $cache_assets_js_compress
 * @property integer $cache_assets_js_compress_comments
 * @property integer $cache_assets_css_compile
 * @property integer $cache_assets_css_compress
 * @property integer $cache_assets_css_compress_file
 * @property string $seo_static_title
 * @property string $seo_gsv
 * @property string $seo_ga
 * @property string $seo_gtm
 * @property integer $seo_fb_app
 * @property string $seo_fb_profile
 * @property integer $seo_fb_image
 * @property string $seo_fb_pixel
 * @property string $seo_tw_profile
 * @property integer $seo_tw_image
 * @property string $seo_tw_image_alt
 * @property string $seo_gp_profile
 * @property string $seo_yt_profile
 * @property integer $is_ssl
 * @property integer $is_url_www
 * @property string $company_name
 * @property string $company_street
 * @property string $company_city
 * @property string $company_zip
 * @property string $company_nip
 * @property string $company_regon
 * @property string $company_krs
 * @property string $company_krs_info
 * @property string $company_phone
 * @property string $company_phone_second
 * @property string $company_fax
 * @property string $company_email
 * @property string $company_email_second
 * 
 * @property Files $seoFbImage
 * @property Files $seoTwImage
 */
class Config extends \common\hooks\yii2\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%configuration}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['smtp_port', 'cache_database', 'cache_database_duration', 'cache_full_page', 'cache_full_page_time', 'cache_html', 'cache_css', 'cache_js', 'seo_fb_app', 'seo_fb_image', 'seo_tw_image', 'is_ssl', 'is_url_www'], 'integer'],
            [['cache_full_page'], 'required'],
            [['custom_scripts_head', 'custom_scripts_footer'], 'safe'],
            [['page_name', 'default_email', 'default_language', 'smtp_host', 'smtp_username', 'smtp_password', 'smtp_encryption', 'seo_static_title', 'seo_gsv', 'seo_ga', 'seo_gtm', 'seo_fb_profile', 'seo_fb_pixel', 'seo_tw_profile', 'seo_tw_image_alt', 'seo_gp_profile','seo_yt_profile', 'company_name', 'company_street', 'company_city', 'company_zip', 'company_nip', 'company_regon', 'company_krs', 'company_krs_info', 'company_phone', 'company_phone_second', 'company_fax', 'company_email', 'company_email_second', 'company_name', 'company_street', 'company_city', 'company_zip', 'company_nip', 'company_regon', 'company_krs', 'company_krs_info', 'company_phone', 'company_phone_second', 'company_fax', 'company_email', 'company_email_second'], 'string', 'max' => 255],
            [['seo_fb_image'], 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['seo_fb_image' => 'id']],
            [['seo_tw_image'], 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['seo_tw_image' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('backend_module_config', 'ID'),
            'default_language' => 'Domyślny język',
            'page_name' => Yii::t('backend_module_config', 'Page Name'),
            'default_email' => Yii::t('backend_module_config', 'Default Email'),
            'smtp_host' => Yii::t('backend_module_config', 'Smtp Host'),
            'smtp_username' => Yii::t('backend_module_config', 'Smtp Username'),
            'smtp_password' => Yii::t('backend_module_config', 'Smtp Password'),
            'smtp_port' => Yii::t('backend_module_config', 'Smtp Port'),
            'smtp_encryption' => "Szyfrowanie",
            'cache_database' => Yii::t('backend_module_config', 'Cache Database'),
            'cache_database_duration' => Yii::t('backend_module_config', 'Cache Database Duration'),
            'cache_full_page' => Yii::t('backend_module_config', 'Cache Full Page'),
            'cache_full_page_time' => Yii::t('backend_module_config', 'Cache Full Page Time'),
            'cache_assets' => Yii::t('backend_module_config', 'Cache Assets'),
            'cache_assets_js_compile' => Yii::t('backend_module_config', 'Cache Assets Js Compile'),
            'cache_assets_js_compress' => Yii::t('backend_module_config', 'Cache Assets Js Compress'),
            'cache_assets_js_compress_comments' => Yii::t('backend_module_config', 'Cache Assets Js Compress Comments'),
            'cache_assets_css_compile' => Yii::t('backend_module_config', 'Cache Assets Css Compile'),
            'cache_assets_css_compress' => Yii::t('backend_module_config', 'Cache Assets Css Compress'),
            'cache_assets_css_compress_file' => Yii::t('backend_module_config', 'Cache Assets Css Compress File'),
            'seo_static_title' => Yii::t('backend_module_config', 'Seo Static Title'),
            'seo_gsv' => Yii::t('backend_module_config', 'Seo Gsv'),
            'seo_ga' => Yii::t('backend_module_config', 'Seo Ga'),
            'seo_gtm' => Yii::t('backend_module_config', 'Seo Gtm'),
            'seo_fb_app' => Yii::t('backend_module_config', 'Seo Fb App'),
            'seo_fb_profile' => Yii::t('backend_module_config', 'Seo Fb Profile'),
            'seo_fb_image' => Yii::t('backend_module_config', 'Seo Fb Image'),
            'seo_fb_pixel' => Yii::t('backend_module_config', 'Seo Fb Pixel'),
            'seo_tw_profile' => Yii::t('backend_module_config', 'Seo Tw Profile'),
            'seo_tw_image' => Yii::t('backend_module_config', 'Seo Tw Image'),
            'seo_tw_image_alt' => Yii::t('backend_module_config', 'Seo Tw Image Alt'),
            'seo_gp_profile' => Yii::t('backend_module_config', 'Seo Gp Profile'),
            'seo_yt_profile' => Yii::t('backend_module_config', 'Seo Yt Profile'),
            'is_ssl' => Yii::t('backend_module_config', 'Is Ssl'),
            'is_url_www' => Yii::t('backend_module_config', 'Is Url Www'),
            'company_name' => Yii::t('backend_module_config', 'Company Name'),
            'company_street' => Yii::t('backend_module_config', 'Company Street'),
            'company_city' => Yii::t('backend_module_config', 'Company City'),
            'company_zip' => Yii::t('backend_module_config', 'Company Zip'),
            'company_nip' => Yii::t('backend_module_config', 'Company Nip'),
            'company_regon' => Yii::t('backend_module_config', 'Company Regon'),
            'company_krs' => Yii::t('backend_module_config', 'Company Krs'),
            'company_krs_info' => Yii::t('backend_module_config', 'Company Krs Info'),
            'company_phone' => Yii::t('backend_module_config', 'Company Phone'),
            'company_phone_second' => Yii::t('backend_module_config', 'Company Phone Second'),
            'company_fax' => Yii::t('backend_module_config', 'Company Fax'),
            'company_email' => Yii::t('backend_module_config', 'Company Email'),
            'company_email_second' => Yii::t('backend_module_config', 'Company Email Second'),
            'custom_scripts_head' => 'Dodatkowe skrypty w nagłówku',
            'custom_scripts_footer' => 'Dodatkowe skrypty w stopce'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeoFbImage() {
        return $this->hasOne(File::className(), ['id' => 'seo_fb_image']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeoTwImage() {
        return $this->hasOne(File::className(), ['id' => 'seo_tw_image']);
    }

}
