<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;

$this->title = Yii::t('backend_module_article', 'Search');
?>
<div class="article-index">

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
            <form id="per-page">
                <span><?= Yii::t('backend', 'quantity_per_page'); ?>:</span>
                <select name="per-page" class="form-control">
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    [
                        'attribute' => 'title',
                        'headerOptions' => ['style' => 'width: 10%'],
                    ],
                    [
                        'attribute' => 'short_description',
                        'headerOptions' => ['style' => 'width: 40%'],
                        'value' => function($model){
                            return substr(strip_tags(trim($model->short_description)), 0, 350);
                        }
                    ],
                    [
                        'attribute' => 'full_description',
                        'headerOptions' => ['style' => 'width: 40%'],
                        'value' => function($model){
                            return substr(strip_tags(trim($model->full_description)), 0, 350);
                        }
                    ],
                    [
                        'class' => 'common\hooks\yii2\grid\ActionColumn',
                        'template' => '{update}',
                        'contentOptions' => ['style' => 'width: 50px;'],
                        'buttons' => [
                            'update' => function ($url, $model) {
                                return Html::a(
                                                '<span class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></span>', \Yii::$app->request->baseUrl."articles/article/update/?id={$model->id}", [
                                            'title' => Yii::t('backend', 'edit'),
                                            'data-toggle' => "tooltip",
                                            'data-placement' => "bottom"
                                                ]
                                );
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </section>   
</div>
