<?php

namespace common\modules\search\admin;

/**
 * files module definition class
 */
class Search extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\search\admin\controllers';
    public $defaultRoute = 'search';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
