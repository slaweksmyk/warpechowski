<?php

namespace common\modules\tags\admin;

/**
 * Settings module definition class
 */
class Tags extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\tags\admin\controllers';
    public $defaultRoute = 'tags';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
