<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;

/* @var $searchModel common\modules\tags\models\TagSearch */

$this->title = Yii::t('backend_module_tags', 'Tags');
?>
<div class="tag-index">

    <p>
        <?= Html::a(Yii::t('backend_module_tags', 'Create Tag'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
     <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <?php if(Yii::$app->request->get('per-page')){ echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>"; } ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?= GridView::widget([
                 'dataProvider' => $dataProvider,
                 'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    'name',
                    ['class' => 'common\hooks\yii2\grid\ActionColumn'],
                 ],
             ]); ?>
        </div>
    </section>
</div>
