<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use common\modules\pages\models\PagesSite;

$this->title = Yii::t('backend_module_tags', 'Update {modelClass}: ', [
    'modelClass' => 'Tag',
]) . $model->name;
?>
<div class="tag-update">

    <p>
        <?= Html::a('< Wróć  do listy', ['index'], ['class' => 'btn btn-outline-secondary']) ?>
    </p>

     <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
        </header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(); ?>
                
                <div class="row">
                    <div class="col-6">
                        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-6">
                        <?= $form->field($model, 'extend_page_id')->dropDownList(ArrayHelper::map(PagesSite::find()->all(), 'id', 'name'), ["prompt" => Yii::t('backend_module_articles', '- select extend -')])->label("Podstrona rozwinięcia"); ?>
                    </div>
                </div>

                <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

           <div class="form-group">
               <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_tags', 'Create') : Yii::t('backend_module_tags', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
           </div>

           <?php ActiveForm::end(); ?>
        </div>
    </section>

</div>
