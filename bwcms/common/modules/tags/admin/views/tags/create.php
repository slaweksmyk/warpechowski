<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('backend_module_tags', 'Create Tag');

?>
<div class="tag-create">

    <p>
        <?= Html::a('< Wróć do listy', ['index'], ['class' => 'btn btn-outline-secondary']) ?>
    </p>
    
     <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
        </header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

           <div class="form-group">
               <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_tags', 'Create') : Yii::t('backend_module_tags', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
           </div>

           <?php ActiveForm::end(); ?>
        </div>
    </section>

</div>
