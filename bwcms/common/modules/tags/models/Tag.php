<?php

namespace common\modules\tags\models;

use Yii;
use common\modules\pages\models\PagesSite;

use yii\helpers\BaseUrl;

/**
 * This is the model class for table "xmod_tags".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 *
 * @property XmodArticlesTags[] $xmodArticlesTags
 */
class Tag extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_tags}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'old_id'], 'string'],
            [['date_created', 'date_updated'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['extend_page_id'], 'exist', 'skipOnError' => true, 'targetClass' => PagesSite::className(), 'targetAttribute' => ['extend_page_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_tags', 'ID'),
            'name' => Yii::t('backend_module_tags', 'Name'),
            'description' => Yii::t('backend_module_tags', 'Description'),
        ];
    }
    
    public function getAbsoluteUrl(){
        return trim(BaseUrl::home(true), "/").$this->getUrl();
    }
   
    public function getUrl(){
        return $this->getExtendUrl()."".$this->slug;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExtendPage()
    {
        return $this->hasOne(PagesSite::className(), ['id' => 'extend_page_id']);
    }
    
    public function getExtendUrl(){
        if($this->getExtendPage()->one()){
            return "/{$this->getExtendPage()->one()->url}/";
        }
        
        return "/";
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getXmodArticlesTags()
    {
        return $this->hasMany(XmodArticlesTags::className(), ['tag_id' => 'id']);
    }
}
