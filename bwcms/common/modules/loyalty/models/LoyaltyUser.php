<?php

namespace common\modules\loyalty\models;

use Yii;
use common\modules\users\models\User;

/**
 * This is the model class for table "xmod_loyalty_users".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $points
 *
 * @property User $user
 */
class LoyaltyUser extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_loyalty_users}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'points'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_loyalty', 'ID'),
            'user_id' => Yii::t('backend_module_loyalty', 'User ID'),
            'points' => Yii::t('backend_module_loyalty', 'Points'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
