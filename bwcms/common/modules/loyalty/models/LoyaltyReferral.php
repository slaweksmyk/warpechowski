<?php

namespace common\modules\loyalty\models;

use Yii;
use common\modules\users\models\User;

/**
 * This is the model class for table "xmod_loyalty_referrals".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $ref_user_id
 *
 * @property User $user
 * @property User $user0
 */
class LoyaltyReferral extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_loyalty_referrals}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'ref_user_id'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['ref_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['ref_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_loyalty', 'ID'),
            'user_id' => Yii::t('backend_module_loyalty', 'User ID'),
            'ref_user_id' => Yii::t('backend_module_loyalty', 'Ref User ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefUser()
    {
        return $this->hasOne(User::className(), ['id' => 'ref_user_id']);
    }
}
