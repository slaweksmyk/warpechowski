<?php

namespace common\modules\loyalty\models;

use Yii;
use common\modules\shop\models\ShopDiscountCodes;

/**
 * This is the model class for table "xmod_loyalty_referrals_config".
 *
 * @property integer $id
 * @property integer $is_enabled
 * @property integer $discount_code
 *
 * @property ShopDiscountCodes $discountCode
 */
class LoyaltyReferralConfig extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_loyalty_referrals_config}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_enabled', 'discount_code'], 'integer'],
            [['discount_code'], 'exist', 'skipOnError' => true, 'targetClass' => ShopDiscountCodes::className(), 'targetAttribute' => ['discount_code' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_loyalty', 'ID'),
            'is_enabled' => Yii::t('backend_module_loyalty', 'Is Enabled'),
            'discount_code' => Yii::t('backend_module_loyalty', 'Discount Code'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscountCode()
    {
        return $this->hasOne(ShopDiscountCodes::className(), ['id' => 'discount_code']);
    }
}
