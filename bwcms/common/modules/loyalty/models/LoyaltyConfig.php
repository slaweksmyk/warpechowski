<?php

namespace common\modules\loyalty\models;

use Yii;

/**
 * This is the model class for table "xmod_loyalty_config".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $auto_calc
 * @property string $auto_calc_perc
 */
class LoyaltyConfig extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_loyalty_config}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'auto_calc'], 'integer'],
            [['auto_calc_perc'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_loyalty', 'ID'),
            'type' => Yii::t('backend_module_loyalty', 'Type'),
            'auto_calc' => Yii::t('backend_module_loyalty', 'Auto Calc'),
            'auto_calc_perc' => Yii::t('backend_module_loyalty', 'Auto Calc Perc'),
        ];
    }
}
