<?php

namespace common\modules\loyalty\models;

use Yii;
use common\modules\shop\models\Product;

/**
 * This is the model class for table "xmod_loyalty_products".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $points
 *
 * @property Product $product
 */
class LoyaltyProduct extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_loyalty_products}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'points'], 'integer'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_loyalty', 'ID'),
            'product_id' => Yii::t('backend_module_loyalty', 'Product ID'),
            'points' => Yii::t('backend_module_loyalty', 'Points'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
