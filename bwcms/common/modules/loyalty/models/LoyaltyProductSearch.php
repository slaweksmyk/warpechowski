<?php

namespace common\modules\loyalty\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\loyalty\models\LoyaltyProduct;

/**
 * LoyaltyProductSearch represents the model behind the search form about `common\modules\loyalty\models\LoyaltyProduct`.
 */
class LoyaltyProductSearch extends LoyaltyProduct
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'product_id', 'points'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LoyaltyProduct::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'product_id' => $this->product_id,
            'points' => $this->points,
        ]);

        return $dataProvider;
    }
}
