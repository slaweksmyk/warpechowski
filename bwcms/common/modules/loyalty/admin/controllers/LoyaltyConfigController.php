<?php

namespace common\modules\loyalty\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use common\modules\shop\models\Product;
use common\modules\loyalty\models\LoyaltyConfig;
use common\modules\loyalty\models\LoyaltyProduct;

/**
 * LoyaltyUserController implements the CRUD actions for LoyaltyUser model.
 */
class LoyaltyConfigController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LoyaltyUser models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = $this->findModel(1);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->render('update', [
                'model' => $model,
            ]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionGenerate(){
        $oLoyaltyConfig = $this->findModel(1);
        $oProductRowset = Product::find()->all();
        
        LoyaltyProduct::deleteAll();
        
        foreach($oProductRowset as $oProduct){
            $iPoints = intval($oProduct->price_brutto * ($oLoyaltyConfig->auto_calc_perc/100));
            
            $oLoyaltyProduct = new LoyaltyProduct();
            $oLoyaltyProduct->product_id = $oProduct->id;
            $oLoyaltyProduct->points = $iPoints;
            $oLoyaltyProduct->save();
        }
        
        return $this->redirect(['loyalty-config']);
    }

    /**
     * Finds the LoyaltyUser model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LoyaltyUser the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LoyaltyConfig::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
