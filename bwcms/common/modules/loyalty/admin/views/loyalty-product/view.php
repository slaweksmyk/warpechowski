<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->getProduct()->one()->name;
?>
<div class="loyalty-product-view">

    <p>
        <?= Html::a(Yii::t('backend_module_loyalty', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend_module_loyalty', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend_module_loyalty', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
        </header>
        <div class="panel-body" >
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
            'product_id',
            'points',
                ],
            ]) ?>
        </div>
    </section>  

</div>
