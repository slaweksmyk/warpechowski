<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = "Konfiguracja ogólna";
?>
<div class="loyalty-product-update">
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
        </header>
        <div class="panel-body" >
            <div class="loyalty-product-form">
                <?php $form = ActiveForm::begin(); ?>
                
                <div class="row">
                    <div class="col-3">
                        <?= $form->field($model, 'auto_calc')->dropDownList([0 => Yii::t('backend_module_articles', 'no'), 1 => Yii::t('backend_module_articles', 'yes')]) ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'auto_calc_perc')->textInput() ?>
                    </div>
                </div>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_loyalty', 'Create') : Yii::t('backend_module_loyalty', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    
                    <?= Html::a('Wygeneruj', ['generate'], ['class' => 'btn btn-warning']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </section>   
</div>
