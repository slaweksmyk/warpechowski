<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = $model->getUser()->one()->getData()->one()->firstname." ".$model->getUser()->one()->getData()->one()->surname;
?>
<div class="loyalty-user-update">
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
        </header>
        <div class="panel-body" >
            <div class="loyalty-user-form">
                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'points')->textInput() ?>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_loyalty', 'Create') : Yii::t('backend_module_loyalty', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </section>   
</div>
