<?php

 use common\hooks\yii2\grid\GridView;

$this->title = Yii::t('backend_module_loyalty', 'Loyalty Users');
?>
<div class="loyalty-user-index">

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        'id',
                        [
                            'attribute' => 'user_id',
                            'value' => function($data){
                                $oUserData = $data->getUser()->one()->getData()->one();
                                return "{$oUserData->firstname} {$oUserData->surname}";
                            }
                        ],
                        'points',
                        [
                            'class' => 'common\hooks\yii2\grid\ActionColumn',
                            'template' => '{view} {update}'
                        ],
                    ],
            ]); ?>
        </div>
    </section>   
</div>
