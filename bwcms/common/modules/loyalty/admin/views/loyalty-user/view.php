<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->getUser()->one()->getData()->one()->firstname." ".$model->getUser()->one()->getData()->one()->surname;
?>
<div class="loyalty-user-view">

    <p>
        <?= Html::a(Yii::t('backend_module_loyalty', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
        </header>
        <div class="panel-body" >
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
            'user_id',
            'points',
                ],
            ]) ?>
        </div>
    </section>  

</div>
