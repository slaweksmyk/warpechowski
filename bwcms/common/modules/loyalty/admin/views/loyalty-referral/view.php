<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->getUser()->one()->getData()->one()->firstname." ".$model->getUser()->one()->getData()->one()->surname;
?>
<div class="loyalty-referral-view">

    <p>
        <?= Html::a(Yii::t('backend_module_loyalty', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend_module_loyalty', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend_module_loyalty', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
        </header>
        <div class="panel-body" >
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
            'user_id',
            'ref_user_id',
                ],
            ]) ?>
        </div>
    </section>  

</div>
