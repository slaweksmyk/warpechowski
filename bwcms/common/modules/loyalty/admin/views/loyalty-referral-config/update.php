<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use common\modules\shop\models\ShopDiscountCodes;

$this->title = "Konfiguracja polecanych";
?>
<div class="loyalty-product-update">
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
        </header>
        <div class="panel-body" >
            <div class="loyalty-product-form">
                <?php $form = ActiveForm::begin(); ?>
                
                <div class="row">
                    <div class="col-3">
                        <?= $form->field($model, 'is_enabled')->dropDownList([0 => Yii::t('backend_module_articles', 'no'), 1 => Yii::t('backend_module_articles', 'yes')]) ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'discount_code')->dropDownList(ArrayHelper::map(ShopDiscountCodes::find()->all(), 'id', 'code'), ['prompt' => "- wybierz kod rabatowy -"]) ?>
                    </div>
                </div>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_loyalty', 'Create') : Yii::t('backend_module_loyalty', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </section>   
</div>
