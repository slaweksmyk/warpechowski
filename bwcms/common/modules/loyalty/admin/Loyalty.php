<?php

namespace common\modules\loyalty\admin;

use Yii;
/**
 * Promotions module definition class
 */
class Loyalty extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\loyalty\admin\controllers';
    public $defaultRoute = 'loyalty-users';

    /**
     * @inheritdoc
     */
    public function init()
    {
        $aUrlParts = explode("/", str_replace(\Yii::$app->request->baseUrl, "", Yii::$app->getRequest()->url));
        $this->defaultRoute = current($aUrlParts);

        parent::init();
    }
}
