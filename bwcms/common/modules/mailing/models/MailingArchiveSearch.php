<?php

namespace common\modules\mailing\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\mailing\models\MailingArchive;

/**
 * MailingArchiveSearch represents the model behind the search form about `common\modules\mailing\models\MailingArchive`.
 */
class MailingArchiveSearch extends MailingArchive
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'email_id', 'is_read'], 'integer'],
            [['date_send', 'date_view'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MailingArchive::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'email_id' => $this->email_id,
            'is_read' => $this->is_read,
            'date_view' => $this->date_view,
        ]);

        return $dataProvider;
    }
}
