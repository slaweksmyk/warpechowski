<?php

namespace common\modules\mailing\models;

use Yii;

/**
 * This is the model class for table "{{%xmod_mailing_config}}".
 *
 * @property integer $id
 * @property string $smtp_host
 * @property string $smtp_port
 * @property string $smtp_login
 * @property string $smtp_password
 * @property string $email_from
 * @property string $freshmail_api
 * @property string $freshmail_secret
 * @property integer $email_per_cycle
 */
class MailingConfig extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_mailing_config}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['freshmail_api', 'freshmail_secret', 'category_title','footer','ads_link','ads_img','logo_link','logo_img','ads_title','logo_title','color'], 'string'],
            [['smtp_host', 'smtp_port', 'smtp_login', 'smtp_password', 'from_email', 'from_name', 'email_per_cycle', 'timeout_per_cycle'], 'required', 'message' => 'To pole jest wymagane'],
            [['email_per_cycle', 'timeout_per_cycle', 'category_id','category_int','ads_active'], 'integer'],
            [['smtp_host', 'smtp_port', 'smtp_login', 'smtp_password', 'from_email', 'from_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_mailing', 'ID'),
            'smtp_host' => Yii::t('backend_module_mailing', 'Smtp Host'),
            'smtp_port' => Yii::t('backend_module_mailing', 'Smtp Port'),
            'smtp_login' => Yii::t('backend_module_mailing', 'Smtp Login'),
            'smtp_password' => Yii::t('backend_module_mailing', 'Smtp Password'),
            'email_from' => Yii::t('backend_module_mailing', 'Email From'),
            'email_per_cycle' => Yii::t('backend_module_mailing', 'Email Per Cycle'),
            'freshmail_api' => Yii::t('backend_module_mailing', 'Freshmail Api'),
            'freshmail_secret' => Yii::t('backend_module_mailing', 'Freshmail Secret'),
            'footer' => Yii::t('backend_module_mailing', 'Footer'),
            'category_id' => Yii::t('backend_module_mailing', 'Category ID'),
            'category_title' => Yii::t('backend_module_mailing', 'Category Title'),
            'category_int' => Yii::t('backend_module_mailing', 'Category Int'),
            
             'ads_link' => Yii::t('backend_module_mailing', 'Ads Link'),
            'ads_img' => Yii::t('backend_module_mailing', 'Ads Img'),
            'ads_title' => Yii::t('backend_module_mailing', 'Ads Title'),
            'ads_active' => Yii::t('backend_module_mailing', 'Ads Active'),
            'logo_link' => Yii::t('backend_module_mailing', 'Logo Link'),
            'logo_img' => Yii::t('backend_module_mailing', 'Logo Img'),
            'logo_title' => Yii::t('backend_module_mailing', 'Logo Title'),
        ];
    }
}
