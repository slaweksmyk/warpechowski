<?php

namespace common\modules\mailing\models;

use Yii;
use common\modules\mailing\models\MailingEmail;

/**
 * This is the model class for table "{{%xmod_mailing_archive}}".
 *
 * @property integer $id
 * @property integer $template_id
 * @property integer $email_id
 * @property integer $is_read
 *
 * @property XmodMailingEmails $email
 * @property XmodMailingTemplates $template
 */
class MailingArchive extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_mailing_archive}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email_id', 'is_read', 'group_id'], 'integer'],
            [['date_view'], 'safe'],
            [['email_id'], 'exist', 'skipOnError' => true, 'targetClass' => MailingEmail::className(), 'targetAttribute' => ['email_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_mailing', 'ID'),
            'email_id' => Yii::t('backend_module_mailing', 'Email ID'),
            'is_read' => Yii::t('backend_module_mailing', 'Is Read'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmail()
    {
        return $this->hasOne(MailingEmail::className(), ['id' => 'email_id']);
    }

}
