<?php

namespace common\modules\mailing\models;

use Yii;
use common\modules\mailing\models\MailingEmail;

/**
 * This is the model class for table "{{%xmod_mailing_groups}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $hash
 * @property string $description
 *
 * @property XmodMailingEmails[] $xmodMailingEmails
 */
class MailingGroups extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_mailing_groups}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','hash'], 'string', 'max' => 255],
            [['description'], 'string'],
            [[ 'date_created', 'date_updated'], 'safe'],
            [[ 'user_created'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_mailing', 'ID'),
            'name' => Yii::t('backend_module_mailing', 'Name'),
            'hash' => Yii::t('backend_module_mailing', 'Hash'),
            'description' => Yii::t('backend_module_mailing', 'Description'),
            'date_created' => Yii::t('backend_module_mailing', 'Date Created'),
            'date_updated' => Yii::t('backend_module_mailing', 'Date Updated'),
            'user_created' => Yii::t('backend_module_mailing', 'User Created')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmails()
    {
        return $this->hasMany(MailingEmail::className(), ['group_id' => 'id']);
    }
}
