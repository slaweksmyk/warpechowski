<?php

namespace common\modules\mailing\models;

use Yii;
use common\modules\mailing\models\MailingGroups;
use common\modules\mailing\models\MailingArchive;

/**
 * This is the model class for table "{{%xmod_mailing_emails}}".
 *
 * @property integer $id
 * @property integer $group_id
 * @property string $email
 * @property integer $is_active
 *
 * @property XmodMailingArchive[] $xmodMailingArchives
 * @property XmodMailingGroups $group
 */
class MailingEmail extends \common\hooks\yii2\db\ActiveRecord {
    
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%xmod_mailing_emails}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['group_id', 'is_active','is_accept_rules'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
            [['email','firstname'], 'string', 'max' => 255],
            //[['email'], 'unique'],
            ['email', 'trim'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'message' => Yii::t('backend_module_user', 'This email address has already been taken.')],
            [['email'], 'required'],
               ['is_accept_rules', 'required', 'requiredValue' => 1, 'message' => Yii::t('backend_module_user','Accept Rules')],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => MailingGroups::className(), 'targetAttribute' => ['group_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('backend_module_mailing', 'ID'),
            'group_id' => Yii::t('backend_module_mailing', 'Group ID'),
            'email' => Yii::t('backend_module_mailing', 'Email'),
            'is_active' => Yii::t('backend_module_mailing', 'Is Active'),
            'date_created' => Yii::t('backend_module_mailing', 'Date Created'),
            'date_updated' => Yii::t('backend_module_mailing', 'Date Updated'),
            'firstname' => Yii::t('backend_module_mailing', 'Firstname'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMailingArchives() {
        return $this->hasMany(MailingArchive::className(), ['email_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup() {
        return $this->hasOne(MailingGroups::className(), ['id' => 'group_id']);
    }

}
