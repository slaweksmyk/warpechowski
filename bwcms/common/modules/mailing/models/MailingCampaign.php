<?php

namespace common\modules\mailing\models;

use Yii;
use common\modules\users\models\User;
use common\modules\articles\models\{
    Article,
    ArticleSearch
};
use common\modules\mailing\models\{
    MailingGroups,
    MailingTemplate,
    MailingConfig
};
use common\modules\files\models\File;
use common\helpers\TextHelper;

/**
 * This is the model class for table "xmod_mailing_campaign".
 *
 * @property integer $id
 * @property integer $template_id
 * @property integer $user_id
 * @property string $date_send
 * @property string $name
 * @property string $title
 * @property string $description
 * @property string $date_published
 * @property string $date_created
 * @property string $date_updated
 * @property string $date_accepted
 * @property string $data
 * @property string $status
 * @property string $day
 * @property string $day
 *
 * @property MailingArchive[] $MailingArchives
 * @property MailingTemplates $template
 */
class MailingCampaign extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%xmod_mailing_campaign}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title'], 'required'],
            [['template_id', 'day', 'quantity_media'], 'integer'],
            [['date_send', 'date_published', 'date_created', 'date_updated', 'date_accepted', 'data', 'status', 'error', 'hash', 'hash_test', 'error_send'], 'safe'],
            [['description', 'description_second'], 'string'],
            [['name', 'title'], 'string', 'max' => 255],
            [['template_id'], 'exist', 'skipOnError' => true, 'targetClass' => MailingTemplate::className(), 'targetAttribute' => ['template_id' => 'id']],
            [['create_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['create_user_id' => 'id']],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => MailingGroups::className(), 'targetAttribute' => ['group_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('backend_module_mailing', 'ID'),
            'template_id' => Yii::t('backend_module_mailing', 'Template ID'),
            'date_send' => Yii::t('backend_module_mailing', 'Date Send'),
            'name' => Yii::t('backend_module_mailing', 'Name'),
            'title' => Yii::t('backend_module_mailing', 'Title'),
            'description' => Yii::t('backend_module_mailing', 'Description'),
            'date_published' => Yii::t('backend_module_mailing', 'Date Published'),
            'date_created' => Yii::t('backend_module_mailing', 'Date Created'),
            'date_updated' => Yii::t('backend_module_mailing', 'Date Updated'),
            'date_accepted' => Yii::t('backend_module_mailing', 'Date Accepted'),
            'data' => Yii::t('backend_module_mailing', 'Data'),
            'status' => Yii::t('backend_module_mailing', 'Status'),
            'day' => Yii::t('backend_module_mailing', 'Day'),
            'create_user_id' => Yii::t('backend_module_mailing', 'Create User ID'),
            'group_id' => Yii::t('backend_module_mailing', 'Group ID'),
            'quantity_media' => Yii::t('backend_module_mailing', 'Quantity Media'),
            'description_second' => Yii::t('backend_module_mailing', 'Description Second'),
            'hash' => Yii::t('backend_module_mailing', 'Hash'),
            'error' => Yii::t('backend_module_mailing', 'Error'),
            'error_send' => Yii::t('backend_module_mailing', 'Error Send'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMailingArchives() {
        return $this->hasMany(MailingArchive::className(), ['campaign_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHtml() {


        $formatter = \Yii::$app->formatter;
        Yii::$app->formatter->locale = 'pl-PL';


        $MailingConfig = MailingConfig::findOne(["id" => 1]);


        $articleHtml = "";

        /*
         * Header
         */
        $articleHtml = '
        <body style = "font-family: Open Sans;color: #39373f;width: 500px;background:#fff"><div style="width:500px";?>
        <div style="clear:both;width: 100%;height:80px;margin-top: 20px;">
        <div style="width:100%;text-align:center;"> 
        <a href="' . $MailingConfig->logo_link . '" alt="' . $MailingConfig->logo_title . '" style="display: block">
            <img src="' . $MailingConfig->logo_img . '" alt="' . $MailingConfig->logo_title . '" width="200">
        </a>
    </div>

</div>
<div style="clear: both;width:100%;background-color: ' . $MailingConfig->color . ';color:#fff;margin-top:20px;">
    <table style="width: 100%">
        <tr>
            <td width="30" align="center" valign="center"  style="padding-left: 5px;"><span style="display: block;width: 20px;height: 20px;background-color: #fff;float: left;">&nbsp;</span></td>
            <td width="160"><h1 style="font-weight: 700;color:#fff;font-size:24px;padding:0;margin:0;"">NEWSLETTER </h1></td>
            <td width="200"><span style="display:block;width:100%;height:3px;background-color:#fff;">&nbsp;</span></td>
            <td width="100"  style="padding-right: 5px;text-align: right"><span style="color:#fff">' . $formatter->asDate($this->date_published, 'short') . '</span></td>
        </tr>
    </table>

</div>';

        if ($MailingConfig->ads_active) {
            $articleHtml .= '
<div style="margin-top:10px;">
    <span style="font-size: 12px; color:#ccc;padding-bottom:10px;display: block">REKLAMA</span>
    <a href="' . $MailingConfig->ads_link . '" alt="' . $MailingConfig->ads_title . '" style="display: block">
        <img src="' . $MailingConfig->ads_img . '" width="490"  alt="' . $MailingConfig->ads_title . '">
    </a>
</div>';
        }

        /*
         * Article Extra
         */
        $data = json_decode($this->data);
        if ($data) {


            foreach ($data->article as $index => $article) {
                $oArticle = Article::findOne($article);


                $articleHtml .= '<div style="margin-top:30px;">
    <h2 style="font-size: 16px; font-weight: 600;text-transform: uppercase;display: block;">' . $oArticle->title . '</h2>
    <p style="text-align: justify;font-size: 14px;">';
                if ($oArticle->thumbnail_id) {
                    $articleHtml .= '<img src="'.$MailingConfig->logo_link.'/upload/' . File::findOne($oArticle->thumbnail_id)->filename . '" width="200"  alt="Title" style="float:left; margin-right:15px;" align="left">';
                }
                $articleHtml .= strip_tags($oArticle->short_description) . '</p> <div style="clear:both;width:100%;height:1px;"></div><a href="' . $MailingConfig->logo_link . $oArticle->getUrl() . '" title="Czytaj więcej" style="float:right; display: inline-block; ;text-align: right;color:#fff;font-size:14px;background-color: ' . $MailingConfig->color . ';text-decoration: none;padding:6px 12px;border-radius: 5px;">Czytaj więcej</a>
  <div style="clear:both;width:100%;height:1px;"></div></div>';
            }
        }
// $article = $this->hasMany(Article::className(), ['id' => 'id']);


        /*
         * Article from checked days
         */
        if ($this->day > 0) {
            $articleHtml .= '<div style="clear:both;width:100%;height: 0;"></div>
        <div style="clear: both;width:100%;background-color: ' . $MailingConfig->color . ';color:#fff;margin-top:20px;">
    <table style="width: 100%">
        <tr>
            <td width="30" align="center" valign="center" style="padding-left: 5px;"><span style="display: block;width: 20px;height: 20px;background-color: #fff;float: left;">&nbsp;</span></td>
            <td width="180"><h1 style="font-weight: 700;color:#fff;font-size:24px;padding:0;margin:0;"">WIADOMOŚCI</h1></td>
            <td width="280" style="padding-right: 5px;"><span style="display:block;width:100%;height:3px;background-color:#fff;">&nbsp;</span></td>
           
        </tr>
    </table>

</div>';

            $articleDay = Article::find($article)->where("`date_created` >= ( CURDATE() - INTERVAL {$this->day} DAY )")->all();

            if ($articleDay) {
                $articleHtml .= '<div style="clear: both;width:100%;margin-top:10px;">
    <ul  style="padding: 0 5px 0 25px;margin: 0;">';

                foreach ($articleDay as $index => $article) {
                    $articleHtml .= '<li style="color: #39373f;font-size: 15px;font-weight: 600;position: relative;border-bottom:1px solid #666666;padding: 6px 0"><a href="' . $MailingConfig->logo_link . $article->getUrl() . '"style="color:#666666;text-decoration: none">' . $article->title . '</a></li>';
                }
                $articleHtml .= '</ul></div>';
            }
        }

        /*
         * Pressroom
         */

        if ($this->description) {
            $articleHtml .= '<div style="clear: both;width:100%;background-color: ' . $MailingConfig->color . ';color:#fff;margin-top:20px;">
    <table style="width: 100%">
        <tr>
            <td width="30" align="center" valign="center"  style="padding-left: 5px;"><span style="display: block;width: 20px;height: 20px;background-color: #fff;float: left;">&nbsp;</span></td>
            <td width="210"><h1 style="font-weight: 700;color:#fff;font-size:24px;padding:0;margin:0;"">PRZEGLĄD PRASY</h1></td>
            <td width="250"  style="padding-right: 5px;"><span style="display:block;width:100%;height:3px;background-color:#fff;">&nbsp;</span></td>
           
        </tr>
    </table>
</div>';

            $articleHtml .= '<div style="clear: both;width:100%;margin-top:10px;text-align: justify;">' . $this->description . '</div>';
        }
        if ($this->quantity_media) {
            /*
             * defence today
             */
//article category 
// Category id and name we set in mailing settings

            $articleMedia = Article::find()->where(["category_id" => $MailingConfig->category_id])->limit($this->quantity_media)->all();
            if ($articleMedia) {


                $articleHtml .= '<div style="clear: both;width:100%;background-color: ' . $MailingConfig->color . ';color:#fff;margin-top:20px;">
    <table style="width: 100%">
        <tr>
            <td width="30" align="center" valign="center"  style="padding-left: 5px;"><span style="display: block;width: 20px;height: 20px;background-color: #fff;float: left;">&nbsp;</span></td>
            <td width="300"><h1 style="font-weight: 700;color:#fff;font-size:24px;padding:0;margin:0;text-transform:uppercase">' . $MailingConfig->category_title . '</h1></td>
            <td width="160"  style="padding-right: 5px;"><span style="display:block;width:100%;height:3px;background-color:#fff;">&nbsp;</span></td>
           
        </tr>
    </table>
</div> ';


                $articleHtml .= '<div style="clear: both;width:100%;margin-top:10px;text-align: justify;">
    <table style="width: 100%">';


                $count = count($articleMedia);


                foreach ($articleMedia as $key => $value) {
                    if ($key == 0) {
                        $articleHtml .= '<tr>';
                    }
                    $articleHtml .= ' <td style="background-color: #f6f6f6;padding: 10px;" width="50%"><h3 style="font-size: 14px;text-transform: uppercase;font-weight: 700;">' . TextHelper::substr($value->title, 0, $MailingConfig->category_int) . '</h3>
                <a href="' .  $MailingConfig->logo_link  . $value->getUrl() . '" title="Czytaj więcej" style="display:block;text-align: right;color:#666666;font-size:14px;">Czytaj więcej</a>
            </td>';


                    if ($key % 2) {

                        $articleHtml .= '</tr><tr>';
                    }
                    if ($count == ($key + 1)) {
                        $articleHtml .= '</tr>';
                    }
                }


                $articleHtml .= '  
    </table>
</div>';
            }//if article from category     
        }
        /*
         * Caledarium
         */
        if ($this->description_second) {
            $articleHtml .= '<div style="clear: both;width:100%;background-color: ' . $MailingConfig->color . ';color:#fff;margin-top:20px;">
    <table style="width: 100%">
        <tr>
            <td width="30" align="center" valign="center"  style="padding-left: 5px;"><span style="display: block;width: 20px;height: 20px;background-color: #fff;float: left;">&nbsp;</span></td>
            <td width="190"><h1 style="font-weight: 700;color:#fff;font-size:24px;padding:0;margin:0;"">KALENDARIUM</h1></td>
            <td width="270"  style="padding-right: 5px;"><span style="display:block;width:100%;height:3px;background-color:#fff;">&nbsp;</span></td>
           
        </tr>
    </table>
</div>';
            $articleHtml .= '<div style="clear: both;width:100%;margin-top:10px;text-align: justify;">' . $this->description_second . '</div>';
        }

        
        $articleHtml .= '<p style="color: #777;font-size: 12px;text-align: center;margin-top: 30px;margin-bottom: 30px;"><resignlink>Link rezygnacji</resignlink></p>';


        $articleHtml .= '<hr style="height:3px;width:100%;background-color:  ' . $MailingConfig->color . ';margin-top:30px;"><p style="color: #777;font-size: 12px;text-align: center;margin-top: 30px;margin-bottom: 30px;">' . $MailingConfig->footer . '</p>';



// closes divs
        $articleHtml .= '</div></body>';
       
        return $articleHtml;
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplate() {
        return $this->hasOne(MailingTemplates::className(), ['id' => 'template_id']);
    }

    public function getCreateUser() {
        return $this->hasOne(User::className(), ['id' => 'create_user_id'])->one();
    }

}
