<?php

namespace common\modules\mailing\models;

use Yii;

/**
 * This is the model class for table "{{%xmod_mailing_templates}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $topic
 * @property string $content
 *
 * @property XmodMailingArchive[] $xmodMailingArchives
 */
class MailingTemplate extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_mailing_templates}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['name', 'topic'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_mailing', 'ID'),
            'name' => Yii::t('backend_module_mailing', 'Name'),
            'topic' => Yii::t('backend_module_mailing', 'Topic'),
            'content' => Yii::t('backend_module_mailing', 'Content'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getXmodMailingArchives()
    {
        return $this->hasMany(MailingArchive::className(), ['template_id' => 'id']);
    }
}
