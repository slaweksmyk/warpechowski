<?php

namespace common\modules\mailing\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\mailing\models\MailingCampaign;

/**
 * MailingCampaignSearch represents the model behind the search form about `common\modules\mailing\models\MailingCampaign`.
 */
class MailingCampaignSearch extends MailingCampaign
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'template_id','status'], 'integer'],
            [['date_send','title'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MailingCampaign::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'template_id' => $this->template_id,
            'date_send' => $this->date_send,
            'status' => $this->status,
        ]);
        
         $query->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}
