<?php

namespace common\modules\mailing\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\mailing\models\MailingEmail;

/**
 * MailingEmailSearch represents the model behind the search form about `common\modules\mailing\models\MailingEmail`.
 */
class MailingEmailSearch extends MailingEmail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'group_id', 'is_active'], 'integer'],
            [['email','date_created','date_updated'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MailingEmail::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'group_id' => $this->group_id,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'email', $this->email]);
        $query->andFilterWhere(['like', 'date_created', $this->date_created]);
        $query->andFilterWhere(['like', 'date_updated', $this->date_updated]);

        return $dataProvider;
    }
}
