<?php

namespace common\modules\mailing\admin;

/**
 * files module definition class
 */
class Mailing extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\mailing\admin\controllers';
    public $defaultRoute = 'mailing';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
