<?php

 use common\hooks\yii2\grid\GridView;

use common\modules\mailing\models\MailingEmail;
use common\modules\mailing\models\MailingTemplate;
use common\modules\mailing\models\MailingGroups;

$oTemplate = MailingTemplate::findOne(["=", "id", $oCampaign->template_id]);
if(!is_null($oTemplate)){
	$this->title = MailingTemplate::findOne(["=", "id", $oCampaign->template_id])->name." <small>({$oCampaign->date_send})</small>";
}

?>
<div class="mailing-groups-index">

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?php $oTemplate = MailingTemplate::findOne(["=", "id", $oCampaign->template_id]); if(!is_null($oTemplate)){ echo $oTemplate->name; } ?>
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <?php if(Yii::$app->request->get('per-page')){ echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>"; } ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    [
                        'label' => "Grupa",
                        'format' => 'html',
                        'headerOptions' => ['style'=>'text-align:center'],
                        'contentOptions'=>['style'=>'width: 145px; text-align: left;'],
                        'value' => function ($data) {
							$oMailingGroups = MailingGroups::findOne(["=", "id", $data["group_id"]]);
							if(!is_null($oMailingGroups)){ return $oMailingGroups->name; }
                        }
                    ],   
                    [
                        'label' => "Adres e-mail",
                        'format' => 'html',
                        'headerOptions' => ['style'=>'text-align:center'],
                        'contentOptions'=>['style'=>'width: 145px; text-align: center;'],
                        'value' => function ($data) {
							$oEmailMailing = MailingEmail::findOne(["=", "id", $data["email_id"]]);
							if(!is_null($oEmailMailing)){ return $oEmailMailing->email; }
                        }
                    ],   
                    [
                        'label' => "Czy odczytano",
                        'format' => 'html',
                        'headerOptions' => ['style'=>'text-align:center'],
                        'contentOptions'=>['style'=>'width: 45px; text-align: center;'],
                        'value' => function ($data) {
                            return ($data["is_read"] == 1) ? 'Tak' : 'Nie';
                        }
                    ],   
                ],
            ]); ?>
        </div>
    </section>
    
</div>
