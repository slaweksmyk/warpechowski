<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\mailing\models\MailingGroups;

$this->title = Yii::t('backend_module_mailing', 'Import  Mailing Email');
?>
<div class="mailing-groups-create">

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
        </header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
                <div class="row">
                    <div class="col-6">
                        <input type="file" name="file" style="margin-top: 20px;">
                    </div>
                    <div class="col-6">
                        <div class="form-group field-mailingemail-group_id">
                            <label class="control-label" for="mailingemail-group_id">Dołącz adresy do grupy: </label>
                            <select id="mailingemail-group_id" class="form-control" name="group_id" aria-invalid="false">
                                <?php foreach(MailingGroups::find()->all() as $oGroup){ ?>
                                    <option value="<?= $oGroup->id ?>"><?= $oGroup->name ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
				<div class="row">
					<div class="col-12" style="text-align: right;">
						Maksymalny rozmiar pliku: <b><?= ini_get('upload_max_filesize') ?></b><br/>
						Maksymalny rozmiar wysyłanych informacji: <b><?= ini_get('post_max_size') ?><br/><br/>
					</div>
				</div>

                <div class="form-group">
                    <?= Html::submitButton("Importuj", ['class' => 'btn btn-success']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </section>

</div>
