<?php

use yii\helpers\Html;

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\modules\mailing\models\MailingGroups;

$this->title = Yii::t('backend_module_mailing', 'Create Mailing Email');
?>
<div class="mailing-groups-create">
    <p>
<?= Html::a(Yii::t('backend_module_mailing', 'Return'), ['emails', 'id' => $group_id], ['class' => 'btn btn-outline-secondary']) ?>  
    </p> 
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
        </header>
        <div class="panel-body" >
<?php $form = ActiveForm::begin(); ?>

            <div class="row">
                <div class="col-4"><?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
                <div class="col-4"><?php
                    //$model->group_id = isset($_GET["id"]) ? $_GET["id"] : '';

                    echo $form->field($model, 'group_id')->dropDownList(ArrayHelper::map(MailingGroups::find()->asArray()->all(), 'id', 'name'), ['options' =>
                        [
                            $group_id => ['selected' => true]
                        ]
                    ])
                    ?></div>
                <div class="col-4"><?=
                    $form->field($model, 'is_active')->dropDownList([
                        1 => Yii::t('backend', 'active'),
                        0 => Yii::t('backend', 'inactive')
                    ])
                    ?></div>
            </div>

            <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_mailing', 'Create') : Yii::t('backend_module_mailing', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

<?php ActiveForm::end(); ?>
        </div>
    </section>
</div>