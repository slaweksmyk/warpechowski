<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('backend_module_mailing', 'Update Mailing Templates');
?>
<div class="mailing-groups-create">
    <p>
        <?= Html::a(Yii::t('backend_module_mailing', 'Return'), ['templates'], ['class' => 'btn btn-outline-secondary']) ?>
    </p>  
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
        </header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(); ?>

            <div class="row">
                <div class="col-6"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
                <div class="col-6"><?= $form->field($model, 'topic')->textInput(['maxlength' => true])->label('Opis') ?></div>
            </div>

            <?= $form->field($model, 'content')->textarea(['rows' => '6']) ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_mailing', 'Create') : Yii::t('backend_module_mailing', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </section>

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Podgląd szablonu
        </header>
        <div class="panel-body" id="tinymce-preview"></div>
    </section>

</div>
