<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;

use common\modules\mailing\models\MailingArchive;
use common\modules\mailing\models\MailingTemplate;

$this->title = Yii::t('backend_module_mailing', 'Mailing Archive');
?>
<div class="mailing-groups-index">

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <?php if(Yii::$app->request->get('per-page')){ echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>"; } ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    [
                        'label' => "Szablon",
                        'format' => 'html',
                        'value' => function ($data) {
							$oTemplate = MailingTemplate::findOne(["=", "id", $data["template_id"]]);
							if(!is_null($oTemplate)){
								return MailingTemplate::findOne(["=", "id", $data["template_id"]])->name;
							} else { 
								return "- usunięty -";
							}
                        }
                    ],   
                    'date_send',
                    [
                        'label' => "Odbiorców",
                        'format' => 'html',
                        'headerOptions' => ['style'=>'text-align:center'],
                        'contentOptions'=>['style'=>'width: 145px; text-align: center;'],
                        'value' => function ($data) {
                            return count(MailingArchive::find()->where(['and', "campaign_id = {$data["id"]}"])->all());
                        }
                    ],   
                    [
                        'label' => "Odczytanych",
                        'format' => 'html',
                        'contentOptions'=>['style'=>'width: 145px; text-align: center;'],
                        'value' => function ($data) {
                            return count(MailingArchive::find()->where(['and', "campaign_id = {$data["id"]}"])->andWhere(["=", "is_read", "1"])->all());
                        }
                    ],   
                    [
                        'class' => 'common\hooks\yii2\grid\ActionColumn',
                        'template' => '{archive-emails}',
                        'buttons' => [
                            'archive-emails' => function ($url) {
                                return Html::a(
                                    '<span class="glyphicon glyphicon-list-alt"></span>',
                                    $url, 
                                    [
                                        'title' => 'Adresy e-mail',
                                        'data-pjax' => '0',
                                    ]
                                );
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </section>
    
</div>
