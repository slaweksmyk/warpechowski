<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\modules\mailing\models\MailingGroups;

$this->title = Yii::t('backend_module_mailing', 'Create Campaign');
$disabled = false;
?>
<div class="mailing-groups-create">
    <p>
        <?= Html::a(Yii::t('backend_module_mailing', 'Return'), ['campaign'], ['class' => 'btn btn-outline-secondary']) ?>  
    </p> 
    
    <?php if($model->hash && $model->status!=2){ ?>
    
    <p class="alert alert-info">Kampania występuje jako szkic w aplikacji Freshmeil(<b><?=$model->hash?></b>). Edytując ją wprowadzasz zmianę w szkicu.</p>
    <?php } ?>
    <?php if($model->status==2){ 
        $disabled = true;
        ?>
    
    <p class="alert alert-warning">Kampania została już wysłana. Edycja kampanii nie jest już możliwa.</p>
    <?php } ?>
    <?php $form = ActiveForm::begin(); ?>
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend_module_mailing', 'Settings Campaign') ?>
        </header>
        <div class="panel-body" >


            <div class="row">
                <div class="col-4"><?= $form->field($model, 'name')->textInput(['maxlength' => true, 'disabled' => $disabled]) ?></div>

                <div class="col-4"><?php
                    //$model->group_id = isset($_GET["id"]) ? $_GET["id"] : '';

                    echo $form->field($model, 'group_id')->dropDownList(ArrayHelper::map(MailingGroups::find()->asArray()->all(), 'id', 'name'),['disabled' => $disabled])
                    ?>
                </div>
                <div class="col-4"><?= $form->field($model, 'date_published')->textInput(['class' => 'form-control datepicker-today', 'disabled' => $disabled]) ?></div>

            </div>




        </div>
    </section>


    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend_module_mailing', 'Content Campaign') ?>
        </header>
        <div class="panel-body" >


            <div class="row">
                <div class="col-12"><?= $form->field($model, 'title')->textInput(['maxlength' => true, 'disabled' => $disabled]) ?></div>
                <div class="col-12">
                    <div class="row">
                        <div class="col-3">
                            <span class="campaign-add <?=$disabled ? 'disabled' : 'enabled'; ?>" <?=$disabled ? '' : 'data-toggle="modal" data-target="#campaign-material" data-template="article"'; ?>>
                                <i class="fa fa-file-text-o"></i>
                                <span><?= Yii::t('backend_module_mailing', 'Campaign Material Add Article') ?></span>
                            </span>
                        </div>
                        <div class="col-3">
                            <span class="campaign-add disabled">
                                <i class="fa fa-video-camera"></i>
                                <span><?= Yii::t('backend_module_mailing', 'Campaign Material Add Video') ?></span>
                            </span>
                        </div>
                        <div class="col-3">
                            <span class="campaign-add disabled">
                                <i class="fa fa-picture-o"></i>
                                <span><?= Yii::t('backend_module_mailing', 'Campaign Material Add Photo') ?></span>
                            </span>
                        </div>
                        <div class="col-3">
                            <span class="campaign-add disabled">
                                <i class="fa fa-picture-o"></i>
                                <i class="fa fa-picture-o"></i>
                                <i class="fa fa-picture-o"></i>
                                <span><?= Yii::t('backend_module_mailing', 'Campaign Material Add Gallery') ?></span>
                            </span>
                        </div>


                        <!-- Modal -->
                        <div class="modal" id="campaign-material" tabindex="-1" role="dialog" aria-labelledby="campaign-material" aria-hidden="true">
                            <div class="modal-dialog  modal-lg" role="document">
                                <div class="modal-content" data-item="campaign-material">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel"><?= Yii::t('backend_module_mailing', 'Content Campaign Material Add') ?>  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button></h5>

                                    </div>
                                    <div class="modal-body">
                                        <label>Wybierz artykuł</label>
                                        <select data-item="material-article" class="form-control ajax-load-articles"></select>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= Yii::t('backend_module_mailing', 'Cancel') ?></button>
                                        <button type="button" class="btn btn-primary" data-action="campaign-material-add" data-dismiss="modal"><?= Yii::t('backend_module_mailing', 'Add') ?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


                <div class="col-12">
                    <ul <?=$disabled ? '' : 'data-item="campaign-material-selected"  id="sortable"'; ?> class="material-items" >
                        <?php
                        $data = json_decode($model->data);
                        if ($data) {


                            foreach ($data->article as $index => $article) {

                                $oArticle = \common\modules\articles\models\Article::findOne($article);
                                ?>


                                <li class="material-item ui-state-default" data-id="<?= $oArticle->id ?>" <?=$disabled ? '' : 'data-item="material-item"'?>>
                                    <div class="media">
                                        <div class="media-left">
                                            <?php if ($oArticle->thumbnail_id) { ?>         
                                                <img alt="64x64" class="media-object" src="/upload/<?= common\modules\files\models\File::find()->where(["=", "id", $oArticle->thumbnail_id])->one()->filename ?>" style="width: 64px; height: 64px;">
                                            <?php } else { ?>
                                                <img alt="64x64" class="media-object" src="/upload/default-article.jpg" style="width: 64px; height: 64px;">
                                            <?php } ?>  
                                        </div>
                                        <div class="media-body"> 
                                            <h4 class="media-heading"><?= $oArticle->title ?></h4>
                                            <p><?= strip_tags($oArticle->short_description) ?></p>
                                        </div>
                                        <div class="media-right media-middle"> 
                                            <span class="label label-danger" data-action="material-delete"><i class="fa fa-trash"></i> Usuń</span>
                                        </div>
                                    </div>
                                    <input type="hidden" name="MailingCampaign[data][article][]" value="<?= $oArticle->id ?>">
                                </li>


                            <?php }
                        }
                        ?>





                    </ul>
                </div>
                <div class="col-12"><?= $form->field($model, 'description')->textarea(['maxlength' => true,'readonly' => $disabled]) ?></div>
                <div class="col-3">
                    <?=
                    $form->field($model, 'day')->dropDownList([
                        0 => 'nie wyświetlaj',
                        1 => 1,
                        2 => 2,
                        3 => 3,
                        4 => 4,
                        5 => 5,
                        6 => 6,
                        7 => 7,
                    ],['disabled' => $disabled])
                    ?>
                </div>

                <div class="col-12"><?= $form->field($model, 'description_second')->textarea(['readonly' => true]) ?></div>

                <div class="col-3">
                    <?=
                    $form->field($model, 'quantity_media')->dropDownList([
                        0 => 'nie wyświetlaj',
                        2 => 2,
                        4 => 4,
                        6 => 6,
                        8 => 8,
                        10 => 10
                            ], ['disabled' => $disabled/* 'options' => [6 => ['Selected' => 'selected']] */])
                    ?>
                </div>
            </div>
        </div>
        <div class="panel-footer"> <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_mailing', 'Create') : Yii::t('backend_module_mailing', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : ($disabled ? 'btn btn-primary disabled' : 'btn btn-primary')]) ?></div>
    </section>
<?php ActiveForm::end(); ?>
</div>
