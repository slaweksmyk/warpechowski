<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\modules\mailing\models\MailingGroups;

$this->title = Yii::t('backend_module_mailing', 'Create Mailing Email');
?>
<div class="mailing-groups-create">
    <p>
        <?= Html::a(Yii::t('backend_module_mailing', 'Return'), ['emails', 'id' => $model->group_id], ['class' => 'btn btn-outline-secondary']) ?>  
    </p> 
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
        </header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(); ?>

            <div class="row">
                <div class="col-4"><?= $form->field($model, 'email')->textInput(['maxlength' => true, 'readonly' => true]) ?></div>
                <div class="col-4"><?= $form->field($model, 'group_id')->dropDownList(ArrayHelper::map(MailingGroups::find()->asArray()->all(), 'id', 'name'), ['disabled' => true]) ?></div>
                <div class="col-4"><?= $form->field($model, 'is_active')->dropDownList([0 => Yii::t('backend', 'inactive'), 1 => Yii::t('backend', 'active')]) ?></div>


                <div class="col-12">
                    <h6><strong>Dane z api:</strong></h6>
                    <p>
                    <pre><?php print_r($api_data) ?></pre>

                    </p>
                </div>




                <div class="col-12">
                    <h6>Ostatni error:</h6>
                    <p>
                        <?= $model->error ?>
                    </p>
                </div>
            </div>
            <p><strong>Opis błedów:</strong></p>
            <table class="table">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>Opis</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1301</td>
                        <td>Adres email jest niepoprawny</td>
                    </tr>
                    <tr>
                        <td>1302</td>
                        <td>Lista subskrypcyjna nie istnieje lub brak hash'a listy</td>
                    </tr>
                    <tr>
                        <td>1303</td>
                        <td>Jedno lub więcej pól dodatkowych jest niepoprawne</td>
                    </tr>
                    <tr>
                        <td>1304</td>
                        <td>Subskrybent już istnieje w tej liście subskrypcyjnej i ma status Aktywny lub Do aktywacji</td>
                    </tr>
                    <tr>
                        <td>1305</td>
                        <td>Próbowano nadać niepoprawny status subskrybenta</td>
                    </tr>
                    <tr>
                        <td>1331</td>
                        <td>Subskrybent nie istnieje</td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_mailing', 'Create') : Yii::t('backend_module_mailing', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </section>

</div>
