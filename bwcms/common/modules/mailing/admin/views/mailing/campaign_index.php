<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;

$this->title = Yii::t('backend_module_mailing', 'Mailing Campaign');
?>
<div class="mailing-groups-index">

    <p>
        <?= Html::a(Yii::t('backend_module_mailing', 'Create Campaign'), ['create-campaign'], ['class' => 'btn btn-success']) ?>
    </p>  
    <?php if (Yii::$app->request->get('status')) { ?>

        <div class="alert alert-<?= Yii::$app->request->get('status') ?>" role="alert"><?= Yii::$app->request->get('result') ?></div>

    <?php }
    ?>

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <?php
                    if (Yii::$app->request->get('per-page')) {
                        echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>";
                    }
                    ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'rowOptions' => function ($model, $key, $index, $grid) {

                    if ($model->status == 2) {
                        return ['class' => 'active-row'];
                    }
                    if ($model->status == 1) {
                        return ['class' => 'new-row'];
                    }
                },
                'columns' => [
                    'id',
                    'title',
                    'date_created',
                    'date_published',
                    [
                        'attribute' => 'CreateUser.username',
                        'label' => Yii::t('backend_module_mailing', 'Create User Username')
                    ],
                     [
                        'attribute' => "status",
                        'filter' => [2 => 'Wysłane', 0 => 'Nowe', 1 => 'Szkic'],
                        'format' => 'raw',
                        'label' => 'Status',
                        'contentOptions' => ['class' => 'text-center text-status'],
                        'value' => function ($model) {
                                if($model->status == 1){
                                    return 'Szkic';
                                }elseif($model->status == 0){
                                    return 'Nowe';
                                }elseif($model->status == 2){
                                    return 'Wysłane';
                                }
                                   
                        }
                    ],
                    [
                        'attribute' => 'confirm',
                        'format' => 'raw',
                        'label' => Yii::t('backend_module_mailing', 'Confirm'),
                        'value' => function($data) {
                            if($data->date_published < date('Y-m-d H:i:s')){
                                 $button =  $data->status == 1  || $data->status == 0 ? '<span class="btn btn-warning btn-xs" title="Czas wysyłki nie może być wcześniejszy niż obecna godzina. Popraw czas wysyłki w edycji." data-toggle = "tooltip" data-placement = "bottom">Wyślij</span>' : '';
                            } else{
                                $button =   $data->status == 1 || $data->status == 0   ? Html::a('<span class="btn btn-success btn-xs">Wyślij</span>', ['campaign-confirm', 'id' => $data->id], [
                                        'data' => [
                                            'confirm' => 'Czy na pewno wysłać kampanię?',
                                            'method' => 'post',
                                        ]
                                    ]) : '';  
                            }
                            return  $button;
                        }
                    ],
                    [
                        'attribute' => 'test',
                        'format' => 'raw',
                        'label' => Yii::t('backend_module_mailing', 'Test'),
                        'value' => function($data) {
                            $link =  $data->status == 1 || $data->status == 0 ? '<span class="label label-default prompt" data-id="' . $data->id . '" data-method="post" data-action="prompt">Test</span>' : '';
                            return $link;
                        }
                    ],
                    [
                        'class' => 'common\hooks\yii2\grid\ActionColumn',
                        'template' => '{update-campaign} {delete-campaign}',
                        'buttons' => [
                            'update-campaign' => function ($url) {
                                return Html::a(
                                                '<span class="btn btn-primary btn-xs glyphicon glyphicon-pencil"></span>', $url, [
                                            'title' => 'Edytuj szablon',
                                            'data-pjax' => '0',
                                                ]
                                );
                            },
                            'delete-campaign' => function ($url) {
                                return Html::a(
                                                '<span class="btn btn-danger btn-xs glyphicon glyphicon-trash"></span>', $url, [
                                            'title' => 'Skasuj kampanie',
                                            'data-pjax' => '0',
                                            'data-confirm' => Yii::t('backend', 'confirm_delete'),
                                            'data-method' => 'post',
                                                ]
                                );
                            },
                        ],
                    ],
                ],
            ]);
            ?>
        </div>
    </section>

</div>
