<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;

$this->title = Yii::t('backend_module_mailing', 'Mailing Emails');
?>
<div class="mailing-groups-index">

    <p>
        <?= Html::a(Yii::t('backend_module_mailing', 'Return'), ['index'], ['class' => 'btn btn-outline-secondary']) ?>  

        <?= Html::a(Yii::t('backend_module_mailing', 'Create Emails'), ['create-email', 'id' => $groupID], ['class' => 'btn btn-success']) ?>
        <?php //Html::a(Yii::t('backend_module_mailing', 'Import Mailing Groups'), ['import-emails'], ['class' => 'btn btn-primary']) ?>
    </p>  

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <?php
                    if (Yii::$app->request->get('per-page')) {
                        echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>";
                    }
                    ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    'email',
                    'is_active',
                    [
                        'class' => 'common\hooks\yii2\grid\ActionColumn',
                        'template' => '{update-email} {delete-email}',
                        'buttons' => [
                            'update-email' => function ($url) {
                                return Html::a(
                                                '<span class="btn btn-primary btn-xs glyphicon glyphicon-pencil"></span>', $url, [
                                            'title' => 'Edytuj email',
                                            'data-pjax' => '0',
                                                ]
                                );
                            },
                            'delete-email' => function ($url) {
                                return Html::a(
                                                '<span class="btn btn-danger btn-xs glyphicon glyphicon-trash"></span>', $url, [
                                            'title' => 'Skasuj email',
                                            'data-pjax' => '0',
                                            'data-confirm' => Yii::t('backend', 'confirm_delete'),
                                            'data-method' => 'post',
                                                ]
                                );
                            },
                        ],
                    ],
                ],
            ]);
            ?>
        </div>
    </section>

</div>
