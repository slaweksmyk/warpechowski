<?php
use yii\widgets\ActiveForm;

use common\modules\mailing\models\MailingGroups;
use common\modules\mailing\models\MailingTemplate;

$this->title = Yii::t('backend_module_mailing', 'Mailing submit');

$this->registerCssFile('/bwcms/common/modules/mailing/admin/assets/css/newsletter.css');
$this->registerJsFile('/bwcms/common/modules/mailing/admin/assets/js/newsletter.js');
?>
<div id="mailing_config" style="display: none" data-tpc="<?= $model->timeout_per_cycle ?>" data-epc="<?= $model->email_per_cycle ?>"></div>

<section class="panel full">
    <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"> <?= Yii::t('backend_module_mailing', 'Mailing submit') ?></header>
    <div class="panel-cnt newsletter-box" style="overflow: initial;">
		<div class="row" style="padding: 0px;"> 
			<div class="col-12 col-sm-6 col-md-6 border-right">
				<div class="row">
					<div class="col-5 col-sm-5 col-md-5 text-left form-group">
						<span>Szablon wiadomości:</span>
					</div>
					<div class="col-7 col-sm-7 col-md-7 form-group">
						<div class="yellow-select yellow-transparent-select" id="newsletter_template_sel">
							<span id="newsletter_template">Wybierz szablon</span>
							<ul>
								<?php foreach(MailingTemplate::find()->all() as $oTemplate){ ?>
									<li data-id="<?= $oTemplate->id ?>"><a href="#"><?= $oTemplate->name ?></a></li>
								<?php } ?>
							</ul>
						</div>
					</div>
					<div class="col-5 col-sm-5 col-md-5 text-left form-group">
						<span>Wyślij do grupy:</span>
					</div>
					<div class="col-7 col-sm-7 col-md-7 form-group">
						<div class="yellow-select yellow-transparent-select">
							Wybierz grupę
							<ul>
								<?php foreach(MailingGroups::find()->all() as $oGroup){ ?>
									<li class="groupList"><a class="group-<?= $oGroup->id ?>" data-group="<?= $oGroup->id ?>" data-amount="<?= count($oGroup->getEmails()->where(["=", "is_active", 1])->all()) ?>" href="#"><?= $oGroup->name ?></a></li>
								<?php } ?>
							</ul>
						</div>
						<div class="selected-group"></div>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-6">
				<div class="row">
					<div class="progress">
					   <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%"></div>
					</div>
					<div class="col-6 col-sm-6 col-md-6 text-center border-right">
						<h3><b id="newsletter_current">0</b><br><span>wysłane wiadomości</span></h3>
					</div>
					<div class="col-6 col-sm-6 col-md-6 text-center">
						<h3><b id="newsletter_max">0</b><br><span>wszystkich wiadomości</span></h3>
					</div>
					<div class="col-12 col-sm-12 col-md-12 text-center newsletter-error">
						<span id="error"></span>
					</div>
				</div>
			</div>
		</div>
    </div>
</section>

<div class="form-group">
    <button type="submit" class="btn btn-primary yellow-btn" name="login-button" id="newsletter_submit">Wyślij wiadomość</button>  
    <a class="btn clear-btn" name="clear-button" id="newsletter_cancel">Anuluj wysyłanie X</a>                 
</div>
<br/><br/><br/>