<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;

$this->title = Yii::t('backend_module_mailing', 'Mailing Templates');
?>
<div class="mailing-groups-index">

    <p>
        <?= Html::a(Yii::t('backend_module_mailing', 'Create Template'), ['create-template'], ['class' => 'btn btn-success']) ?>
    </p>  

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <?php if (Yii::$app->request->get('per-page')) {
                        echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>";
                    } ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    'name',
                    [
                        'class' => 'common\hooks\yii2\grid\ActionColumn',
                        'template' => '{update-template} {delete-template}',
                        'buttons' => [
                            'update-template' => function ($url) {
                                return Html::a(
                                                '<span class="btn btn-primary btn-xs glyphicon glyphicon-pencil"></span>', $url, [
                                            'title' => 'Edytuj szablon',
                                            'data-pjax' => '0',
                                                ]
                                );
                            },
                            'delete-template' => function ($url) {
                                return Html::a(
                                                '<span class="btn btn-danger btn-xs glyphicon glyphicon-trash"></span>', $url, [
                                            'title' => 'Skasuj szablon',
                                            'data-pjax' => '0',
                                                ]
                                );
                            },
                        ],
                    ],
                ],
            ]);
            ?>
        </div>
    </section>

</div>
