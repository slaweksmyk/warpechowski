<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\categories\models\ArticlesCategories;
use yii\helpers\ArrayHelper;

$this->title = Yii::t('backend_module_mailing', 'Mailing config');

$this->registerCssFile('/bwcms/common/modules/mailing/admin/assets/css/newsletter.css');
$this->registerJsFile('/bwcms/common/modules/mailing/admin/assets/js/newsletter.js');
?>
<div class="newsletter-page">
    <?php $form = ActiveForm::begin(); ?>
    <section class="panel full configuration-form">
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"> Logo:
        </header>

        <div class="col-12 panel-body">
            <div class="row">
                <div class="col-3">
                    <?= $form->field($model, 'logo_link')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-3">
                    <?= $form->field($model, 'logo_title')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-3">
                    <?= $form->field($model, 'logo_img')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-3">
                    <?= $form->field($model, 'color')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
    </section>
    <section class="panel full configuration-form">
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"> Reklama:
        </header>

        <div class="col-12 panel-body">
            <div class="row">
                <div class="col-2">
                    <?= $form->field($model, 'ads_active')->dropDownList([0 => Yii::t('backend_module_articles', 'inactive'), 1 => Yii::t('backend_module_articles', 'active')]) ?>
                </div>
                <div class="col-3">
                    <?= $form->field($model, 'ads_link')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-3">
                    <?= $form->field($model, 'ads_title')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-4">
                    <?= $form->field($model, 'ads_img')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
    </section>


    <section class="panel full configuration-form">
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"> Freshmail:
        </header>

        <div class="col-12 panel-body">
            <div class="row">
                <div class="col-6">
                    <?= $form->field($model, 'freshmail_api')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-6">
                    <?= $form->field($model, 'freshmail_secret')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
    </section>
    <section class="panel full configuration-form">
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"> Kategoria:
        </header>

        <div class="col-12 panel-body">
            <div class="row">
                <div class="col-6">
                    <?= $form->field($model, 'category_title')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-4">
                    <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(ArticlesCategories::find()->asArray()->all(), 'id', 'name'), ['class' => 'form-control no-select2'])
                    ?>
                </div>
                <div class="col-2">
                    <?= $form->field($model, 'category_int')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>


    </section>

    <section class="panel full configuration-form">
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"> Stopka:
        </header>

        <div class="col-12 panel-body">
            <div class="row">
                <div class="col-12">
                    <?= $form->field($model, 'footer')->textarea(['class' => 'form-control noTynyMCE', 'rows' => 5]) ?>
                </div>

            </div>
        </div>


    </section>


    <section class="panel full configuration-form">
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"> Konfiguracja SMTP:
        </header>

        <div class="col-12 col-sm-12 col-md-12 panel-body">
            <div class="col-6 col-sm-6 col-md-6">
                <div class="row">
                    <div class="col-5 col-sm-5 col-md-5 text-right form-group">
                        <span>Adres serwera:</span>
                    </div>
                    <div class="col-7 col-sm-7 col-md-7 form-group">
                        <?= $form->field($model, 'smtp_host')->textInput(['maxlength' => true, 'class' => 'yellow-btn-transparent', 'placeholder' => 'smtp.la666.pl'])->label(false) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-5 col-sm-5 col-md-5 text-right form-group">
                        <span>Adres e-mail (login):</span>
                    </div>
                    <div class="col-7 col-sm-7 col-md-7 form-group">
                        <?= $form->field($model, 'smtp_login')->textInput(['maxlength' => true, 'class' => 'yellow-btn-transparent', 'placeholder' => 'smtp@la666.pl'])->label(false) ?>
                    </div>
                </div>
            </div>
            <div class="col-6 col-sm-6 col-md-6">
                <div class="row">
                    <div class="col-5 col-sm-5 col-md-5 text-right form-group">
                        <span>Numer portu:</span>
                    </div>
                    <div class="col-7 col-sm-7 col-md-7 form-group">
                        <?= $form->field($model, 'smtp_port')->textInput(['maxlength' => true, 'class' => 'yellow-btn-transparent', 'placeholder' => '656'])->label(false) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-5 col-sm-5 col-md-5 text-right form-group">
                        <span>Hasło:</span>
                    </div>
                    <div class="col-7 col-sm-7 col-md-7 form-group">
                        <?= $form->field($model, 'smtp_password')->passwordInput(['maxlength' => true, 'class' => 'yellow-btn-transparent', 'placeholder' => '***************'])->label(false) ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="panel full configuration-form">
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"> Pozostałe ustawienia dotyczące wysyłki:
        </header>
        <div class="col-12 col-sm-12 col-md-12 panel-body">
            <div class="col-6 col-sm-6 col-md-6">
                <div class="row">
                    <div class="col-5 col-sm-5 col-md-5 text-right form-group">
                        <span>Domyślny adres e-mail:</span>
                    </div>
                    <div class="col-7 col-sm-7 col-md-7 form-group">
                        <?= $form->field($model, 'from_email')->textInput(['maxlength' => true, 'class' => 'yellow-btn-transparent', 'placeholder' => '- adres e-mail -'])->label(false) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-5 col-sm-5 col-md-5 text-right form-group">
                        <span>Wiadomości na cykl:</span>
                    </div>
                    <div class="col-7 col-sm-7 col-md-7 form-group">
                        <?= $form->field($model, 'email_per_cycle')->textInput(['maxlength' => true, 'class' => 'yellow-btn-transparent width150', 'placeholder' => '1'])->label(false) ?>
                    </div>
                </div>
            </div>
            <div class="col-6 col-sm-6 col-md-6">
                <div class="row">
                    <div class="col-5 col-sm-5 col-md-5 text-right form-group">
                        <span>Nazwa domyślnego adresu:</span>
                    </div>
                    <div class="col-7 col-sm-7 col-md-7 form-group">
                        <?= $form->field($model, 'from_name')->textInput(['maxlength' => true, 'class' => 'yellow-btn-transparent', 'placeholder' => 'Black Wolf CMS'])->label(false) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-5 col-sm-5 col-md-5 text-right form-group">
                        <span>Częstotliwość cyklu (ms):</span>
                    </div>
                    <div class="col-7 col-sm-7 col-md-7 form-group">
                        <?= $form->field($model, 'timeout_per_cycle')->textInput(['maxlength' => true, 'class' => 'yellow-btn-transparent width150', 'placeholder' => '100'])->label(false) ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="form-group">
        <button type="submit" class="btn btn-primary yellow-btn" name="login-button">Zapisz zmiany</button>  
    </div>
    <br/><br/><br/>
    <?php ActiveForm::end(); ?>
</div>