<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\modules\mailing\models\MailingGroups;

$this->title = Yii::t('backend_module_mailing', 'Create Campaign');
?>
<div class="mailing-groups-create">
    <p>
        <?= Html::a(Yii::t('backend_module_mailing', 'Return'), ['campaign'], ['class' => 'btn btn-outline-secondary']) ?>  
    </p> 
    <?php $form = ActiveForm::begin(); ?>
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend_module_mailing', 'Settings Campaign') ?>
        </header>
        <div class="panel-body" >


            <div class="row">
                <div class="col-4"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>

                <div class="col-4"><?php
                    //$model->group_id = isset($_GET["id"]) ? $_GET["id"] : '';

                    echo $form->field($model, 'group_id')->dropDownList(ArrayHelper::map(MailingGroups::find()->asArray()->all(), 'id', 'name'))
                    ?>
                </div>
                <div class="col-4"><?= $form->field($model, 'date_published')->textInput(['class' => 'form-control datepicker-today']) ?></div>

            </div>




        </div>
    </section>


    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend_module_mailing', 'Content Campaign') ?>
        </header>
        <div class="panel-body" >


            <div class="row">
                <div class="col-12"><?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?></div>
                <div class="col-12">
                    <div class="row">
                        <div class="col-3">
                            <span class="campaign-add enabled" data-toggle="modal" data-target="#campaign-material" data-template="article">
                                <i class="fa fa-file-text-o"></i>
                                <span><?= Yii::t('backend_module_mailing', 'Campaign Material Add Article') ?></span>
                            </span>
                        </div>
                        <div class="col-3">
                            <span class="campaign-add disabled">
                                <i class="fa fa-video-camera"></i>
                                <span><?= Yii::t('backend_module_mailing', 'Campaign Material Add Video') ?></span>
                            </span>
                        </div>
                        <div class="col-3">
                            <span class="campaign-add disabled">
                                <i class="fa fa-picture-o"></i>
                                <span><?= Yii::t('backend_module_mailing', 'Campaign Material Add Photo') ?></span>
                            </span>
                        </div>
                        <div class="col-3">
                            <span class="campaign-add disabled">
                                <i class="fa fa-picture-o"></i>
                                <i class="fa fa-picture-o"></i>
                                <i class="fa fa-picture-o"></i>
                                <span><?= Yii::t('backend_module_mailing', 'Campaign Material Add Gallery') ?></span>
                            </span>
                        </div>


                        <!-- Modal -->
                        <div class="modal" id="campaign-material" tabindex="-1" role="dialog" aria-labelledby="campaign-material" aria-hidden="true">
                            <div class="modal-dialog  modal-lg" role="document">
                                <div class="modal-content" data-item="campaign-material">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel"><?= Yii::t('backend_module_mailing', 'Content Campaign Material Add') ?>  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button></h5>

                                    </div>
                                    <div class="modal-body">
                                        <label>Wybierz artykuł</label>
                                        <select data-item="material-article" class="form-control ajax-load-articles"></select>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= Yii::t('backend_module_mailing', 'Cancel') ?></button>
                                        <button type="button" class="btn btn-primary" data-action="campaign-material-add" data-dismiss="modal"><?= Yii::t('backend_module_mailing', 'Add') ?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


                <div class="col-12">
                    <ul data-item="campaign-material-selected" id="sortable" class="material-items" ></ul>
                </div>
                 <div class="col-12"><?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?></div>
                  <div class="col-3">
                     <?= $form->field($model, 'day')->dropDownList([
                          0 =>  'nie wyświetlaj', 
                         1 =>  1, 
                         2 =>  2,
                         3 =>  3,
                         4 =>  4,
                         5 =>  5,
                         6 =>  6,
                         7 =>  7,
                         ]) ?>
                 </div>
                 
                  <div class="col-12"><?= $form->field($model, 'description_second')->textarea(['maxlength' => true]) ?></div>
                
                 <div class="col-3">
                     <?= $form->field($model, 'quantity_media')->dropDownList([
                          0 =>  'nie wyświetlaj', 
                         2  =>  2, 
                         4  =>  4,
                         6  =>  6,
                         8  =>  8,
                         10 =>  10
                         ],[/*'options' => [6 => ['Selected'=>'selected']]*/]) ?>
                 </div>
                 
                 
                 
            </div>



        </div>
        <div class="panel-footer"> <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_mailing', 'Create') : Yii::t('backend_module_mailing', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?></div>
    </section>
    <?php ActiveForm::end(); ?>
</div>
