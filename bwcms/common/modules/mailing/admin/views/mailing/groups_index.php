<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;

use common\modules\mailing\models\MailingEmail;

$this->title = Yii::t('backend_module_mailing', 'Mailing Groups');
?>
<div class="mailing-groups-index">

    <p>
        <?= Html::a(Yii::t('backend_module_mailing', 'Create Mailing Groups'), ['create-groups'], ['class' => 'btn btn-success']) ?>
    </p>  
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <?php if(Yii::$app->request->get('per-page')){ echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>"; } ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    'name',
                    'description',
                    
                    'hash',
                    'date_created',
                    'date_updated',
                   
                    [
                        'label' => "Ilość odbiorców",
                        'format' => 'html',
                        'contentOptions'=>['style'=>'width: 145px; text-align: center;'],
                        'value' => function ($data) {
                            return count(MailingEmail::find()->where(['and', "group_id = {$data["id"]}"])->all());
                        }
                    ],   
                    [
                        'class' => 'common\hooks\yii2\grid\ActionColumn',
                        'template' => '{emails} {update-groups} {delete-groups}',
                        'buttons' => [
                            'emails' => function ($url) {
                                return Html::a(
                                    '<span class="btn btn-warning btn-xs glyphicon glyphicon-eye-open"></span>',
                                    $url, 
                                    [
                                        'title' => 'Adresy e-mail',
                                        'data-pjax' => '0',
                                    ]
                                );
                            },
                            'update-groups' => function ($url) {
                                return Html::a(
                                    '<span class="btn btn-primary btn-xs glyphicon glyphicon-pencil"></span>',
                                    $url, 
                                    [
                                        'title' => 'Edytuj grupę',
                                        'data-pjax' => '0',
                                    ]
                                );
                            },
                            'delete-groups' => function ($url) {
                                return Html::a(
                                    '<span class="btn btn-danger btn-xs glyphicon glyphicon-trash"></span>',
                                    $url, 
                                    [
                                        'title' => 'Skasuj grupę',
                                        'data-pjax' => '0',
                                         'data-confirm' => Yii::t('backend', 'confirm_delete'),
                                        'data-method' => 'post',

                                    ]
                                );
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </section>
    
</div>
