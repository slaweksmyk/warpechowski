<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('backend_module_mailing', 'Update Mailing Groups');
?>
<div class="mailing-groups-create">
    <p>
        <?= Html::a(Yii::t('backend_module_mailing', 'Return'), ['index'], ['class' => 'btn btn-outline-secondary']) ?>  
    </p>
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
        </header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'hash')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'description')->textarea(['class' => 'form-control noTynyMCE']) ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_mailing', 'Create') : Yii::t('backend_module_mailing', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </section>

</div>
