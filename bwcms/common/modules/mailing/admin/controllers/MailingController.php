<?php

namespace common\modules\mailing\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\hooks\yii2\swiftmailer\Mailer;
use yii\helpers\BaseUrl;
use common\modules\logs\models\Log;
use common\modules\mailing\models\{
    MailingConfig,
    MailingCampaign,
    MailingCampaignSearch,
    MailingArchive,
    MailingArchiveSearch,
    MailingGroups,
    MailingGroupsSearch,
    MailingEmail,
    MailingEmailSearch,
    MailingTemplate,
    MailingTemplateSearch
};

/**
 * MailingController implements the CRUD actions for MailingGroups model.
 */
class MailingController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MailingGroups models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MailingGroupsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('groups_index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCampaign()
    {
        $searchModel = new MailingCampaignSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy(['id' => SORT_DESC]);
        return $this->render('campaign_index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCampaignTest($id, $email = false)
    {




        $this->campaignTest($id, $email);
    }

    public function actionCampaignConfirm($id)
    {



        $result = $this->campaignSend($id);


        return $this->redirect(['campaign',
                    'status' => $result['status'],
                    'result' => $result['result'],
        ]);
    }

    private function campaignCreate($id, $test = false)
    {
        $MailingConfig = MailingConfig::findOne(["id" => 1]);
        $model = MailingCampaign::find()->where(["id" => $id, "status" => 0])->one();

        if ($model) {

            $rest = new \common\hooks\freshmail\RestApi();
            $rest->setApiKey($MailingConfig->freshmail_api);
            $rest->setApiSecret($MailingConfig->freshmail_secret);

            $data = array(
                'name' => $model->name,
                'subject' => $model->title,
                'html' => $model->getHtml(),
                // 'text' => $model->description,
                'list' => MailingGroups::findOne($model->group_id)->hash
            );

            try {
                $response = $rest->doRequest('campaigns/create', $data);


                $model->hash = $response['data']['hash'];

                $model->date_accepted = date("Y-m-d H:i:s");

                $result = [
                    'status' => 'success',
                    'result' => 'Kampania została wysłana',
                    'hash' => $response['data']['hash']
                ];
                $model->status = 1;
            } catch (\Exception $e) {
                $model->error = 'Error message: ' . $e->getMessage() . ', Error code: ' . $e->getCode() . ', HTTP code: ' . $rest->getHttpCode();
                $result = [
                    'status' => 'danger',
                    'result' => $e->getMessage(),
                    'hash' => $model->hash
                ];
            }




            $model->save();
            return $result;
        }
    }

    private function campaignSend($id)
    {

        $MailingConfig = MailingConfig::findOne(["id" => 1]);

        $model = MailingCampaign::find()->where(["id" => $id])->one();






        if (!$model->hash) {
            $result = $this->campaignCreate($id);
            $model->hash = $result['hash'];
        }




        //Data wysłania kampanii w formacie YYYY-MM-DD H:i:s
        $rest = new \common\hooks\freshmail\RestApi();
        $rest->setApiKey($MailingConfig->freshmail_api);
        $rest->setApiSecret($MailingConfig->freshmail_secret);
        $data = array(
            'hash' => $model->hash,
            'time' => $model->date_published
        );

        try {
            $response = $rest->doRequest('campaigns/send', $data);
            $model->status = 2;
            $result = [
                'status' => 'success',
                'result' => 'Kampania została wysłana',
            ];
        } catch (\Exception $ex) {
            $model->error_send = 'Error message: ' . $ex->getMessage() . ', Error code: ' . $ex->getCode() . ', HTTP code: ' . $rest->getHttpCode();


            $result = [
                'status' => 'danger',
                'result' => $ex->getMessage()
            ];
        }
        $model->save();
        return $result;
    }

    private function campaignTest(int $id, $email)
    {

        $MailingConfig = MailingConfig::findOne(["id" => 1]);

        $model = MailingCampaign::find()->where(["id" => $id])->one();








        if (!$model->hash) {
            // $hash_test = $this->campaignCreate($id, 1); // 1 = test
            //Wysyła szkic kampanii
            $hash = $this->campaignCreate($id); // 1 = test
            $model->hash = $hash['hash'];
        }

        //Data wysłania kampanii w formacie YYYY-MM-DD H:i:s
        $rest = new \common\hooks\freshmail\RestApi();
        $rest->setApiKey($MailingConfig->freshmail_api);
        $rest->setApiSecret($MailingConfig->freshmail_secret);
        $data = array(
            'hash' => $model->hash,
            'emails' => $email
        );


        try {
            $response = $rest->doRequest('campaigns/sendTest', $data);



            $result = 'Wysłano wiadomość testową na adres e-mail: ' . $email;

            $model->error_send = $result;
        } catch (\Exception $ex) {
            $response = 'Error message: ' . $ex->getMessage() . ', Error code: ' . $ex->getCode() . ', HTTP code: ' . $rest->getHttpCode();
            $model->error_send = $response;

            $result = 'Wystąpił błąd, spróbuj jeszcze raz: ' . $response;
        }

        $model->save();


        echo $result;
    }

    public function actionCreateCampaign()
    {


        $MailingConfig = MailingConfig::findOne(["id" => 1]);


        $model = new MailingCampaign();

        if ($model->load(Yii::$app->request->post())) {

            $model->create_user_id = Yii::$app->user->identity->id;
            $model->date_created = date("Y-m-d H:i:s");
            $model->data = json_encode($model->data);
            $model->name = $model->name ? $model->name : $model->title;
            $model->status = 0;
            $model->save();

            return $this->redirect(['campaign']);
        } else {
            return $this->render('campaign_create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MailingGroups model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateCampaign($id)
    {

        $MailingConfig = MailingConfig::findOne(["id" => 1]);
        $model = $this->findModelCampaign($id);

        if ($model->load(Yii::$app->request->post())) {

            $model->date_updated = date("Y-m-d H:i:s");
            $model->data = json_encode($model->data);
            $model->name = $model->name ? $model->name : $model->title;
            $model->save();


            if ($model->hash) {
                $rest = new \common\hooks\freshmail\RestApi();
                $rest->setApiKey($MailingConfig->freshmail_api);
                $rest->setApiSecret($MailingConfig->freshmail_secret);

                $data = array(
                    'id_hash' => $model->hash,
                    'name' => $model->name,
                    'subject' => $model->title,
                    'html' => $model->getHtml(),
                    'list' => MailingGroups::findOne($model->group_id)->hash
                );

                try {
                    $response = $rest->doRequest('campaigns/edit', $data);
                    $model->error_send = $response;
                } catch (\Exception $ex) {
                    $response = 'Error message: ' . $ex->getMessage() . ', Error code: ' . $ex->getCode() . ', HTTP code: ' . $rest->getHttpCode();
                    $model->error_send = $response;
                }
                $model->save();
            }
            //return $this->redirect(['update-campaign', 'id' => $id]);
        }

        return $this->render('campaign_update', [
                    'model' => $model,
        ]);
    }

    public function actionSubmit()
    {
        $model = MailingConfig::findOne(["id" => 1]);

        if (!Yii::$app->request->isPost) {
            Yii::$app->session->set('aEmails', null);
            Yii::$app->session->set('oTemplate', null);
            Yii::$app->session->set('iCampainid', null);
        }

        $oMailer = new Mailer();
        $oMailer->setTransport(
                $oMailer->createTransport([
                    'class' => 'Swift_SmtpTransport',
                    'host' => $model->smtp_host,
                    'username' => $model->smtp_login,
                    'password' => $model->smtp_password,
                    'port' => $model->smtp_port
                ])
        );

        if (Yii::$app->request->isPost) {
            $oRequest = Yii::$app->request;

            if (!is_object(Yii::$app->session->get('oTemplate'))) {
                Yii::$app->session->set('oTemplate', MailingTemplate::findOne(["id" => $oRequest->post("template_id")]));
            }

            if (!is_int(Yii::$app->session->get('iCampainid'))) {
                $oCampain = new MailingCampaign();
                $oCampain->template_id = $oRequest->post("template_id");
                $oCampain->date_send = Yii::$app->formatter->asTime('now', 'yyyy-MM-dd HH:mm:ss');
                $oCampain->save();

                Yii::$app->session->set('iCampainid', $oCampain->id);
            }

            $aGroups = json_decode($oRequest->post("groups"), true);
            $oEmail = MailingEmail::find()->where(["IN", "group_id", implode(",", $aGroups)])->limit($oRequest->post("submit_package"))->offset($oRequest->post("current"))->one();

            for ($i = 0; $i < $oRequest->post("submit_package"); $i++) {
                if (!is_null($oEmail)) {
                    $oArchive = new MailingArchive();
                    $oArchive->campaign_id = Yii::$app->session->get('iCampainid');
                    $oArchive->email_id = $oEmail->id;
                    $oArchive->group_id = $oEmail->group_id;
                    $oArchive->is_read = 0;
                    $oArchive->save();

                    $tracking = "<img src='" . str_replace(\Yii::$app->request->baseUrl, "", BaseUrl::home(true)) . "mailing_{$oArchive->id}.png' alt='mailing'/>";
                    $unsubscribe = "<a href='" . str_replace(\Yii::$app->request->baseUrl, "", BaseUrl::home(true)) . "unsubscribe-" . $oEmail->id . "' style='text-align: center; width: 100%; color: black; padding-top: 5px; display: block; font-size: 12px;'>Wypisz się z listy</a>";

                    $content = '<html xmlns="http://www.w3.org/1999/xhtml">';
                    $content .= Yii::$app->session->get('oTemplate')->content;
                    $content .= $tracking;
                    $content .= $unsubscribe;
                    $content .= "</html>";

                    $oEmailSubmit = $oMailer->compose();
                    $oEmailSubmit->setFrom([$model->from_email => $model->from_name]);
                    $oEmailSubmit->setTo($oEmail->email);
                    $oEmailSubmit->setSubject(Yii::$app->session->get('oTemplate')->topic);
                    $oEmailSubmit->setTextBody(strip_tags($content));
                    $oEmailSubmit->setHtmlBody($content);
                    $oEmailSubmit->send();
                }
            }
            echo "ok";
            Yii::$app->end();
            exit;
        } else {
            return $this->render('sys_submit', [
                        'model' => $model,
            ]);
        }
    }

    public function actionConfig()
    {
        $model = MailingConfig::findOne(["id" => 1]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['config']);
        } else {
            return $this->render('sys_config', [
                        'model' => $model,
            ]);
        }
    }

    public function actionImportEmails()
    {
        if (Yii::$app->request->isPost) {
            $aRawImport = \moonland\phpexcel\Excel::import($_FILES["file"]["tmp_name"], []);
            foreach ($aRawImport as $aImport) {
                $oEmail = new MailingEmail();
                $oEmail->group_id = Yii::$app->request->post("group_id");
                $oEmail->email = current($aImport);
                $oEmail->is_active = 1;
                $oEmail->save();
            }
            return $this->redirect(['emails', "id" => Yii::$app->request->post("group_id")]);
        } else {
            return $this->render('emails_import');
        }
    }

    /**
     * Lists all MailingEmail models.
     * @return mixed
     */
    public function actionEmails($id)
    {
        $searchModel = new MailingEmailSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (is_numeric($id)) {
            $dataProvider->query->andwhere("group_id = '{$id}'");
        }

        return $this->render('emails_index', [
                    'groupID' => $id,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all MailingGroups models.
     * @return mixed
     */
    public function actionTemplates()
    {
        $searchModel = new MailingTemplateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('template_index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTemplatesAjax()
    {
        if (Yii::$app->request->isAjax) {
            $searchModel = MailingTemplate::find()->all();
            $template = [];
            foreach ($searchModel as $key => $value) {
                $template[$key]['title'] = $value->name;
                $template[$key]['description'] = $value->topic;
                $template[$key]['content'] = $value->content;
            }
            header('Content-Type: application/json');
            echo json_encode($template);
            exit;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Lists all MailingArchive models.
     * @return mixed
     */
    public function actionArchive()
    {
        $searchModel = new MailingCampaignSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('archive_index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all MailingEmail models.
     * @return mixed
     */
    public function actionArchiveEmails($id)
    {
        $searchModel = new MailingArchiveSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (is_numeric($id)) {
            $dataProvider->query->where("campaign_id = '{$id}'");
        }

        return $this->render('archive_emails', [
                    'oCampaign' => MailingCampaign::findOne(["=", "id", $id]),
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new MailingGroups model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateGroups()
    {


        $MailingConfig = MailingConfig::findOne(["id" => 1]);


        $model = new MailingGroups();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $rest = new \common\hooks\freshmail\RestApi();
            $rest->setApiKey($MailingConfig->freshmail_api);
            $rest->setApiSecret($MailingConfig->freshmail_secret);

            $data = array(
                'name' => $model->name,
                'description' => $model->description
            );

            try {
                $response = $rest->doRequest('subscribers_list/create', $data);
                $model->hash = $response['hash'];
            } catch (\Exception $e) {
                $model->error = 'Error message: ' . $e->getMessage() . ', Error code: ' . $e->getCode() . ', HTTP code: ' . $rest->getHttpCode();
            }

            $model->user_created = Yii::$app->user->identity->id;

            $model->date_created = date("Y-m-d H:i:s");

            $model->save();



            return $this->redirect(['index']);
        } else {
            return $this->render('groups_create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new MailingEmail model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateEmail($id = null)
    {

        $MailingConfig = MailingConfig::findOne(["id" => 1]);
        $model = new MailingEmail();



        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $rest = new \common\hooks\freshmail\RestApi();
            $rest->setApiKey($MailingConfig->freshmail_api);
            $rest->setApiSecret($MailingConfig->freshmail_secret);



            $data = array(
                'email' => $model->email,
                'list' => MailingGroups::findOne($model->group_id)->hash,
                //'confirm' => $model->is_active == 1 ? 1 : 0,
                'state' => $model->is_active == 1 ? 1 : '',
            );

            try {
                $response = $rest->doRequest('subscriber/add', $data);
            } catch (\Exception $e) {
                $model->error = 'Error message: ' . $e->getMessage() . ', Error code: ' . $e->getCode() . ', HTTP code: ' . $rest->getHttpCode();
            }
            $model->date_created = date("Y-m-d H:i:s");
            $model->save();

            return $this->redirect(['emails', "id" => $id]);
        } else {
            return $this->render('emails_create', [
                        'model' => $model,
                        'group_id' => Yii::$app->request->get('id'),
            ]);
        }
    }

    /**
     * Creates a new MailingEmail model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateTemplate()
    {
        $model = new MailingTemplate();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['templates']);
        } else {
            return $this->render('template_create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MailingGroups model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateGroups($id)
    {

        $MailingConfig = MailingConfig::findOne(["id" => 1]);
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {



            $rest = new \common\hooks\freshmail\RestApi();
            $rest->setApiKey($MailingConfig->freshmail_api);
            $rest->setApiSecret($MailingConfig->freshmail_secret);

            $data = array(
                'name' => $model->name,
                'description' => $model->description,
                'hash' => $model->hash,
            );

            try {
                $response = $rest->doRequest('subscribers_list/update', $data);
            } catch (\Exception $e) {
                $model->error = 'Error message: ' . $e->getMessage() . ', Error code: ' . $e->getCode() . ', HTTP code: ' . $rest->getHttpCode();
            }
            $model->date_updated = date("Y-m-d H:i:s");

            $model->save();

            return $this->redirect(['index']);
        } else {
            return $this->render('groups_update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MailingGroups model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateEmail($id)
    {
        $MailingConfig = MailingConfig::findOne(["id" => 1]);
        $model = MailingEmail::findOne(["id" => $id]);
        $hash = MailingGroups::findOne($model->group_id)->hash;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $rest = new \common\hooks\freshmail\RestApi();
            $rest->setApiKey($MailingConfig->freshmail_api);
            $rest->setApiSecret($MailingConfig->freshmail_secret);
            $data = array(
                'email' => $model->email,
                'list' => MailingGroups::findOne($model->group_id)->hash,
                'confirm' => $model->is_active == 1 ? 1 : 0,
                'state' => $model->is_active == 1 ? 1 : '',
            );
            try {
                $response = $rest->doRequest('subscriber/edit', $data);
            } catch (\Exception $e) {
                $model->error = 'Error message: ' . $e->getMessage() . ', Error code: ' . $e->getCode() . ', HTTP code: ' . $rest->getHttpCode();
            }
            $model->date_created = date("Y-m-d H:i:s");
            $model->save();
        }


        $api_data = new \common\hooks\freshmail\RestApi();
        $api_data->setApiKey($MailingConfig->freshmail_api);
        $api_data->setApiSecret($MailingConfig->freshmail_secret);

        try {
            $api_data = $api_data->doRequest("subscriber/get/{$hash}/{$model->email}");
        } catch (\Exception $e) {
            $api_data = 'Error message: ' . $e->getMessage() . ', Error code: ' . $e->getCode() . ', HTTP code: ' . $api_data->getHttpCode();
        }

        return $this->render('emails_update', [
                    'model' => $model,
                    'api_data' => $api_data,
        ]);
    }

    /**
     * Updates an existing MailingGroups model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateTemplate($id)
    {
        $model = MailingTemplate::findOne(["id" => $id]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update-template', "id" => $id]);
        } else {
            return $this->render('template_update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MailingGroups model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteGroups($id)
    {
        $model = $this->findModel($id);
        $model->delete();

        return $this->redirect(['index']);
    }

    public function actionDeleteCampaign($id)
    {
        $model = $this->findModelCampaign($id);
        $model->delete();

        return $this->redirect(['campaign']);
    }

    /**
     * Deletes an existing MailingGroups model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteEmail($id)
    {
        $model = MailingEmail::findOne(["id" => $id]);
        $model->delete();

        return $this->redirect(['emails', "id" => $model->group_id]);
    }

    /**
     * Deletes an existing MailingGroups model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteTemplate($id)
    {
        $model = MailingTemplate::findOne(["id" => $id]);
        $model->delete();

        return $this->redirect(['templates']);
    }

    /**
     * Finds the MailingGroups model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MailingGroups the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MailingGroups::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelCampaign($id)
    {
        if (($model = MailingCampaign::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
