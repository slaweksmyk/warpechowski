$(document).ready(function(){
    var max_amount;
    var template_id;
    var stop = false;
    var current = 0;
    var timeout = 100;
    var submit_package = 1;
    var groups = [];
    
    /*
     * Events
     */
    
    $("#newsletter_template_sel li").click(function(){
        $("#newsletter_template_sel span").text($(this).text());
        
        template_id = $(this).data("id");
        $("#newsletter_template_sel").attr("data-id", $(this).data("id"));
    });
    
    $('.yellow-transparent-select ul li.groupList').click(function(){
        var amount = parseInt($("#newsletter_max").text());
        var group_id = $(this).children().data("group");
		
        $(this).addClass('option-disabled');
        amount += parseInt($(this).children().data("amount"));
		
        max_amount = amount;
        
        groups.push(group_id);
        
        $('.selected-group').append('<div>'+ $(this).text() +' <a href="#" data-group="'+ group_id +'" data-amount="'+parseInt($(this).children().data("amount"))+'">X</a></div>');
        $("#newsletter_max").text(amount);
        recalculateTime();
    });

    $('body').on('click', ".selected-group div a", function (e) {
        e.preventDefault();
        var amount = parseInt($("#newsletter_max").text());
        var group_id = $(this).data("group");
		
        
        amount -= parseInt($(this).data("amount"));
        max_amount = amount;
        
        groups.remove(group_id);

        $(this).parent().remove();
        $('.yellow-transparent-select ul li a.group-'+group_id).parent().removeClass('option-disabled');
        $("#newsletter_max").text(amount);
        recalculateTime();
    });
    
    $("#newsletter_submit").click(function(){
        max_amount = parseInt($("#newsletter_max").text());

        if(max_amount > 0 && template_id > 0){
            submitHandler();
        }
    });

    $("#newsletter_cancel").click(function(){
        stop = true;
		location.reload();
    });
    
    /*
     * Functions
     */

    function initConfig(){
        timeout = parseInt($("#mailing_config").data("tpc"));
        submit_package = parseInt($("#mailing_config").data("epc"));
    }
    
    function submitHandler(){
		setInterval(function(){
			if(current < max_amount){
				sendEmail();
				current += submit_package;
				updateStatus();
			}
		}, timeout);
    }
    
    function sendEmail(){
		$.ajax({
			url: $("#homeDir").text()+"mailing/mailing/submit/",
			method: "POST",
			data: {
				template_id: template_id, 
				current: current, 
				submit_package: submit_package, 
				groups: JSON.stringify(toObject(groups)) 
			},
			cache: false
		}).done(function( data ) {
			console.log(data)
		});
		
		
		
        /*$.post("/__cms/mailing/mailing/submit/", { template_id: template_id, current: current, submit_package: submit_package, groups: JSON.stringify(toObject(groups)) })
        .done(function( data ) {
             
        });*/
    }
    
	function updateStatus(){
        progress = (current*100)/max_amount;
		
		if(current < max_amount){
			$(".progress-bar").css("width", progress+"%");
			$("#newsletter_current").text(current);
		} else {
			$(".progress-bar").css("width", "100%");
			$("#newsletter_current").text(max_amount);
		}
	}
	
    function recalculateTime(){
        var time = parseInt((timeout*max_amount)/submit_package);

        s = time;
        var ms = s % 1000;
        s = (s - ms) / 1000;
        var secs = s % 60;
        s = (s - secs) / 60;
        var mins = s % 60;
        
        $("#error").html("Szacowany czas wysyłania: <b>" + mins + 'min. ' + secs+ "sek.</b>");
    }
    
    function toObject(arr) {
        var rv = {};
        for (var i = 0; i < arr.length; ++i)
            rv[i] = arr[i];
        return rv;
    }
    
    Array.prototype.remove = function() {
        var what, a = arguments, L = a.length, ax;
        while (L && this.length) {
            what = a[--L];
            while ((ax = this.indexOf(what)) !== -1) {
                this.splice(ax, 1);
            }
        }
        return this;
    };
    
    initConfig();
    
});