$(document).ready(function(){
    
    $('#add-files-input').change( function () {
        var $i = $(this), input = $i[0];
        if ( input.files && input.files[0] ) {
            file = input.files[0];
            fr = new FileReader();
            fr.onload = function () {
                tinymce.activeEditor.execCommand('mceInsertContent', false, fr.result);
            };
            fr.readAsText(file);
        }
    } );
    
});