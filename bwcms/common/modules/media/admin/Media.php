<?php

namespace common\modules\media\admin;

/**
 * files module definition class
 */
class Media extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\media\admin\controllers';
    public $defaultRoute = 'media';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
