<?php

namespace common\modules\media\admin\controllers;

use Yii;
use common\helpers\ValidateHelper;
use common\modules\media\models\Media;
use common\modules\media\models\MediaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


/**
 * MediaController implements the CRUD actions for Media model.
 */
class MediaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Media models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MediaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Media model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Media model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Media();
        $url = null;

        if(Yii::$app->request->isPost){
            
            switch(Yii::$app->request->post()["state"]){
                case "1":
                    return $this->createModel($model);
                    
                default:
                    $params = $this->parseUrl(Yii::$app->request->post()["Media"]["url"], $model);
                    break;
            }

            return $this->render('create', $params);
        } else {
            return $this->render('create', [
                'model' => $model,
                'url' => $url,
                'state' => 0
            ]);
        }
    }
    
    /*
     * Create media model
     */
    private function createModel($model){
        $aPost = Yii::$app->request->post()["Media"];
        
        $model->url = $aPost["url"];
        $model->name = $aPost["name"];
        $model->slug = $this->prepareURL($aPost["name"]);
        $model->description = $aPost["description"];
        $model->photo = $aPost["photo"];
        $model->save();
        
        return $this->redirect(['index']);
    }
    
    /*
     * Prepare slug
     */
    private function prepareURL($sText){
        $aReplacePL = array( 'ą' => 'a', 'ę' => 'e', 'ś' => 's', 'ć' => 'c', 'ó' => 'o', 'ń' => 'n', 'ż' => 'z', 'ź' => 'z', 'ł' => 'l', 'Ą' => 'A', 'Ę' => 'E', 'Ś' => 'S', 'Ć' => 'C', 'Ó' => 'O', 'Ń' => 'N', 'Ż' => 'Z', 'Ź' => 'Z', 'Ł' => 'L');
        $sText = str_replace(array_keys($aReplacePL), array_values($aReplacePL), $sText);
        $sText = str_replace(' ', '-', strtolower($sText));
        $sText = preg_replace('/[\-]+/', '-', $sText);
        return trim($sText, '-');
    }
    
    /*
     * Parse data from selected URL
     */
    private function parseUrl($url, $model){
        $tempImages   = array();
        $site_content = file_get_contents($url);
        $aMetaData    = get_meta_tags($url);

        preg_match_all('/<img[^>]+>/i', $site_content, $aImg);
        foreach($aImg[0] as $img){
            preg_match_all('/src\s*=\s*"(.+?)"/i', $img, $srcRaw);
            
            $image = current($srcRaw[1]);

            $tmp = explode("&", $image);
            if(count($tmp) > 1){
                $image = $tmp[0];
            }
            
            if(!preg_match("/http/", $image) && !preg_match("/https/", $image)) {
                $u = parse_url($url);
                $image = 'http://' . $u['host'] . $image;
            }

            array_push($tempImages, $image);
        }

        $aImages = array();
        foreach ($tempImages as $index => $image) {
            if(strpos($image, 'pix2') !== false || strpos($image, 'tvnuser') !== false || strpos($image, '.svg') !== false) {
                unset($tempImages[$index]);
            }

            if (strpos("http", $image) === false) {
                $u = parse_url($url);
                $image = 'http://' . $u['host'] . $image;
            }
            if (ValidateHelper::isUrl($image)) {
                $aImages[] = $image;
            }
        }

        if(isset($aMetaData["twitter:image"])){
            array_push($tempImages, $aMetaData["twitter:image"]);
        }

        preg_match_all('/<title>(.*?)<\/title>/is', $site_content, $tempTitle);
        if (@isset($tempTitle[1][0])) {
            $sTitle = $tempTitle[1][0];
        } else {
            $sTitle = null;
        }

        preg_match_all('/<description>(.*?)<\/description>/is', $site_content, $tempDescription);
        if (@isset($tempDescription[1][0])) {
            $sDescription = $tempDescription[1][0];
        } else {
            $sDescription = null;
        }
        
        $desc = "";
        if(isset($aMetaData['description'])){
            $desc = $aMetaData['description'];
        }
        
        return [
            'title' => $sTitle,
            'description' => $desc,
            'url' => $url,
            'images' => $tempImages,
            'state' => 1,
            'model' => $model,
        ];
    }

    /**
     * Updates an existing Media model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Media model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Media model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Media the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Media::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
