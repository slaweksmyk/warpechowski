<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = substr($model->name, 0, 60)."...";
?>
<div class="media-update">

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
        </header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(); ?>

            <div class="row">
                <div class="col-6"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
                <div class="col-6"><?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?></div>
                <div class="col-4"><?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?></div>
                <div class="col-4"><?= $form->field($model, 'photo')->textInput(['maxlength' => true]) ?></div>
                <div class="col-4"><?= $form->field($model, 'is_active')->dropDownList([0 =>  Yii::t('backend', 'inactive'), 1 =>  Yii::t('backend', 'active')]) ?></div>
            </div>

            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_media', 'Create') : Yii::t('backend_module_media', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </section>

</div>
