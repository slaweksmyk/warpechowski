<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('backend_module_media', 'Create Media');
?>
<div class="media-create">
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
        </header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(); ?>
            <?= Html::hiddenInput('state', $state); ?>

            <?= $form->field($model, 'url')->textInput(['maxlength' => true, 'value' => $url]) ?>

            <?php if($state === 1){ ?>
                <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'value' => $description]) ?>
                <?= $form->field($model, 'description')->textarea(['rows' => 6, 'value' => $title]) ?>
            
                <?php foreach($images as $image){ ?>
                    <div class="row">
                        <div class="col-2"></div>
                        <div class="col-8">
                            <img src="<?= $image ?>" class="img-responsive" style="margin: 0 auto;"/>
                            <?= $form->field($model, 'photo')->radio(['label' => substr($image, 0, 70)."...", "value" => $image]); ?>
                        </div>
                        <div class="col-2"></div>
                    </div>
                <?php } ?>
            <?php } ?>
            
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_media', 'Create') : Yii::t('backend_module_media', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </section>

</div>
