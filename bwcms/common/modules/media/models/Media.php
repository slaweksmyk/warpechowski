<?php

namespace common\modules\media\models;

use Yii;

/**
 * This is the model class for table "{{%xmod_media}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $url
 * @property string $photo
 * @property string $description
 * @property integer $is_active
 */
class Media extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_media}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['is_active'], 'integer'],
            [['name', 'slug', 'url', 'photo'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_media', 'ID'),
            'name' => Yii::t('backend_module_media', 'Name'),
            'slug' => Yii::t('backend_module_media', 'Slug'),
            'url' => Yii::t('backend_module_media', 'Url'),
            'photo' => Yii::t('backend_module_media', 'Photo'),
            'description' => Yii::t('backend_module_media', 'Description'),
            'is_active' => Yii::t('backend_module_media', 'Is Active'),
        ];
    }
}
