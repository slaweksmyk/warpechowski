<?php

namespace common\modules\notices\models;

use Yii;
use common\modules\notices\models\NoticesList;

/**
 * This is the model class for table "xmod_notices_emails".
 *
 * @property integer $id
 * @property integer $notice_id
 * @property string $copyto
 * @property string $content
 * @property string $subject
 *
 * @property NoticesList $notice
 */
class NoticesEmails extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_notices_emails}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notice_id'], 'integer'],
            [['content'], 'string'],
            [['copyto', 'subject'], 'string', 'max' => 255],
            [['notice_id'], 'exist', 'skipOnError' => true, 'targetClass' => NoticesList::className(), 'targetAttribute' => ['notice_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_notices', 'ID'),
            'notice_id' => Yii::t('backend_module_notices', 'Notice ID'),
            'copyto' => Yii::t('backend_module_notices', 'Copyto'),
            'content' => Yii::t('backend_module_notices', 'Content'),
            'subject' => Yii::t('backend_module_notices', 'Subject'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotice()
    {
        return $this->hasOne(NoticesList::className(), ['id' => 'notice_id']);
    }
}
