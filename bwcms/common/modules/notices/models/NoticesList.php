<?php

namespace common\modules\notices\models;

use Yii;
use common\modules\notices\models\NoticesEmail;

/**
 * This is the model class for table "xmod_notices_list".
 *
 * @property integer $id
 * @property string $name
 * @property integer $is_active
 *
 * @property NoticesEmails[] $xmodNoticesEmails
 */
class NoticesList extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_notices_list}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_active'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_notices', 'ID'),
            'name' => Yii::t('backend_module_notices', 'Name'),
            'is_active' => Yii::t('backend_module_notices', 'Is Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNoticesEmails()
    {
        return $this->hasMany(NoticesEmail::className(), ['notice_id' => 'id']);
    }
}
