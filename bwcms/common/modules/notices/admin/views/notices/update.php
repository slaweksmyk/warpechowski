<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = "Zaktualizuj powiadomienie: {$model->name}";
?>

<p>
    <?= Html::a("< Wróć", ['index'], ['class' => 'btn btn-success']) ?>
</p>

<div class="notices-list-update">

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
        </header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(); ?>
            
                <div class="row">
                    <div class="col-8"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
                    <div class="col-4"><?= $form->field($model, 'is_active')->dropDownList([0 =>  Yii::t('backend_module_articles', 'inactive'), 1 =>  Yii::t('backend_module_articles', 'active')]) ?></div>
                </div>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_notices', 'Create') : "Zaktualizuj", ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </section>   

</div>
