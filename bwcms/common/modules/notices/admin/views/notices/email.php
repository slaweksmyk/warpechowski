<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = "Zaktualizuj treść wiadomości";
?>

<p>
    <?= Html::a("< Wróć", ['index'], ['class' => 'btn btn-success']) ?>
</p>

<div class="notices-list-update">

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
        </header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(); ?>
            
                <div class="row">
                    <div class="col-12"><?= $form->field($model, 'subject')->textInput(['maxlength' => true])->label("<br/>Temat wiadomości") ?></div>
                    <div class="col-12"><?= $form->field($model, 'copyto')->textInput(['maxlength' => true])->label("Adres odbiorcy/ów (Kolejne maile należy wpisywać po przecinku. Np. ck@wp.pl,andrzej@onet.pl)") ?></div>
                </div>
            
                <div class="row">
                    <div class="col-12"><?= $form->field($model, 'content')->textarea()->label("Treść wiadomości") ?></div>
                </div>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_notices', 'Create') : "Zaktualizuj", ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </section>   

</div>
