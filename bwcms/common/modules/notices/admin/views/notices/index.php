<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;

$this->title = Yii::t('backend_module_notices', 'Notices Lists');
?>
<div class="notices-list-index">

    <p>
        <?= Html::a(Yii::t('backend_module_notices', 'Create Notices List'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'rowOptions'=>function($model){
                if(!$model->is_active){
                    return ['class' => 'inactive-row'];
                }
            },
            'columns' => [
                'id',
                [
                    'attribute' => "name",
                    'contentOptions'=>['style'=>'width: 50%;'],
                ],
                [
                    'attribute' => "is_active",
                    'contentOptions'=>['style'=>'width: 20%;'],
                ],
                [
                    'class' => 'common\hooks\yii2\grid\ActionColumn',
                    'template' => '{update} {email} {delete}',
                    'buttons' => [
                        'email' => function ($url, $model, $key) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-envelope"></span>',
                                $url, 
                                [
                                    'title' => "Edytuj treść wiadomości",
                                    'data-pjax' => '0',
                                ]
                            );
                        }
                    ],
                ],
            ],
            ]); ?>
        </div>
    </section>   
</div>
