<?php

namespace common\modules\notices\admin;

/**
 * notices module definition class
 */
class Notices extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\notices\admin\controllers';
    public $defaultRoute = 'notices';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
