<?php

namespace common\modules\forms\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\forms\models\ModuleForms;

/**
 * ModuleFormsSearch represents the model behind the search form about `common\modules\forms\models\ModuleForms`.
 */
class ModuleFormsSearch extends ModuleForms
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'widget_id'], 'integer'],
            [['recipients', 'data', 'date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ModuleForms::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'widget_id' => $this->widget_id,
        ]);

        $query->andFilterWhere(['like', 'recipients', $this->recipients])
            ->andFilterWhere(['like', 'data', $this->data])
            ->andFilterWhere(['like', 'date', $this->date]);

        return $dataProvider;
    }
}
