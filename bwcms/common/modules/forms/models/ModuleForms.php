<?php

namespace common\modules\forms\models;

use Yii;

/**
 * This is the model class for table "xmod_forms".
 *
 * @property integer $id
 * @property integer $widget_id
 * @property string $recipients
 * @property string $data
 * @property string $date
 */
class ModuleForms extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_forms}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['widget_id'], 'required'],
            [['widget_id'], 'integer'],
            [['recipients', 'data'], 'string'],
            ['date', 'default', 'value' => date('Y-m-d H:i:s')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'widget_id' => 'Widget ID',
            'recipients' => 'Recipients',
            'data' => 'Data',
            'date' => 'Date'
        ];
    }
}
