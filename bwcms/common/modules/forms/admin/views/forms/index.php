<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;
use common\modules\widget\models\WidgetItems;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\forms\models\ModuleFormsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend_module_forms', 'get_messages');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="module-forms-index">

    <?php if( $errorAction ) { ?>
        <div style="padding: 15px; background: #ffd4d4; border-radius: 7px; margin-bottom: 10px;" >
            <h5>
                <?= Yii::t('backend', 'module_module_confirm') ?>: 
                <b><?php echo $errorAction['name']; ?></b> <small>(ID <?php echo $errorAction['id']; ?>)</small>
            </h5>
        </div>
    <?php } ?>
    
    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title; ?></header>
        <div class="panel-body" >
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'class' => 'yii\grid\SerialColumn',
                    'header' => Yii::t('backend', 'LP')
                ],
                [
                    'attribute' => 'widget_id',
                    'label' => Yii::t('backend_module_forms', 'form_name'),
                    'value' => function ($data) {
                        $widgetItemModel = new WidgetItems();
                        $widgetItem = $widgetItemModel->findOne($data->widget_id);
                        if($widgetItem){
                            return $widgetItem->custom_name;
                        } else {
                            return 'ID: '.$data->widget_id;
                        }
                        
                    }
                ],       
                [
                    'attribute' => 'date',
                    'value' => function ($data) {
                        if($data->date){
                            $senddate = new DateTime($data->date);
                            return $senddate->format('Y-m-d H:i:s');
                        } else {
                            return '';
                        }
                    },
                    'label' => Yii::t('backend_module_forms', 'sending date')
                ],
                [
                    'attribute' => 'recipients',
                    'format' => 'raw',
                    'label' => Yii::t('backend_module_forms', 'recipients')
                ],
                [
                    'attribute' => 'data',
                    'label' => Yii::t('backend_module_forms', 'data'),
                    'format' => 'raw',
                    'value' => function ($data) {
                        $dataReturn = unserialize($data->data); $result = "";
                        foreach($dataReturn as $key => $value){
                            $result .= "<b>{$key}:</b> {$value}<br>"; 
                        }
                        return $result;
                    }
                ],

                [
                    'class' => 'common\hooks\yii2\grid\ActionColumn',
                    'template' => '{view} {delete}',
                    'contentOptions' => ['style' => 'width: 100px;'],
                    'buttons' => [
                        'view' => function ($url) {
                            return Html::a(
                                            '<span class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-eye-open"></span></span>', $url, [
                                        'title' => Yii::t('backend', 'view'),
                                        'data-toggle' => "tooltip",
                                        'data-placement' => "bottom"
                                            ]
                            );
                        },
                        'delete' => function ($url) {
                            return Html::a(
                                            '<span class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></span>', $url, [
                                        'title' => Yii::t('backend', 'delete'),
                                        'data-toggle' => "tooltip",
                                        'data-placement' => "bottom",
                                        'data-pjax' => 0,
                                        'data-confirm' => 'Czy na pewno usunąć ten element?',
                                        'data-method' => 'post',
                                            ]
                            );
                        },
                    ],
                ],
            ],
        ]); ?>
        </div>
    </section>
    
</div>
