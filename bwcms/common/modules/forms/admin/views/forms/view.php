<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use common\modules\widget\models\WidgetItems;

/* @var $this yii\web\View */
/* @var $model common\modules\forms\models\ModuleForms */

$this->title = Yii::t('backend_module_forms', 'message');

$this->params['breadcrumbs'][] = ['label' => 'Module Forms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

// from name
$widgetItemModel = new WidgetItems();
$widgetItem = $widgetItemModel->findOne($model->widget_id);
if($widgetItem){
    $formName = $widgetItem->custom_name;
} else {
    $formName = 'ID: '.$model->widget_id;
}

// form data
$dataReturn = unserialize($model->data); $dataUn = "";
foreach($dataReturn as $key => $value){
    $dataUn .= "<b>{$key}:</b> {$value}<br>"; 
}
?>

<div class="module-forms-view">

    <p>
        <?= Html::a(Yii::t('backend_module_forms', 'back_to_messages_list'), ['index'], ['class' => 'btn btn-outline-secondary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend_module_forms', 'message_data'); ?></header>
        <div class="panel-body" >
            <?php echo DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    [
                        'attribute' => 'widget_id',
                        'format' => 'raw',
                        'label' => Yii::t('backend_module_forms', 'form_name'),
                        'value' => $formName
                    ],
                    [
                        'attribute' => 'recipients',
                        'label' => Yii::t('backend_module_forms', 'recipients')
                    ],
                    [
                        'attribute' => 'data',
                        'label' => Yii::t('backend_module_forms', 'data'),
                        'format' => 'raw',
                        'value' => $dataUn
                    ],
                ],
            ]) ?>
        </div>
    </section>

</div>
