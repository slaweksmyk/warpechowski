<?php

namespace common\modules\forms\admin;

/**
 * forms module definition class
 */
class Forms extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\forms\admin\controllers';
    public $defaultRoute = 'forms';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
