<?php

namespace common\modules\rss\models;

use Yii;
use common\modules\users\models\User;
use common\modules\files\models\File;

/**
 * This is the model class for table "{{%xmod_rss}}".
 *
 * @property integer $id
 * @property integer $url
 * @property string $create_date
 * @property integer $create_uid
 * @property string $update_date
 *
 * @property User $createU
 */
class Rss extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_rss}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['create_uid'], 'integer'],
            [['url', 'name', 'description'], 'string'],
            [['create_date', 'update_date'], 'safe'],
            [['create_uid'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['create_uid' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_rss', 'ID'),
            'url' => Yii::t('backend', 'Url'),
            'name' => Yii::t('backend', 'Name'),
            'create_date' => Yii::t('backend_module_rss', 'Create Date'),
            'create_uid' => Yii::t('backend_module_rss', 'Create Uid'),
            'update_date' => Yii::t('backend_module_rss', 'Update Date'),
            'photo_id' => Yii::t('backend_module_rss', 'Photo Id'),
            'description' => Yii::t('backend', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateU()
    {
        return $this->hasOne(User::className(), ['id' => 'create_uid']);
    }
    
    public function getPhoto()
    {
        return File::findOne(["id" => $this->photo_id]);
    }
    
    
}
