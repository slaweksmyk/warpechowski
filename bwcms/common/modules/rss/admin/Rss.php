<?php

namespace common\modules\rss\admin;

/**
 * files module definition class
 */
class Rss extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\rss\admin\controllers';
    public $defaultRoute = 'rss';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
