<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\rss\models\Rss */

$this->title = Yii::t('backend_module_rss', 'create_rss');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend_module_rss', 'Rsses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<p>
    <?= Html::a(Yii::t('backend_module_rss', 'back_to_list_rss'), ['index'], ['class' => 'btn btn-outline-secondary']) ?>
</p>

<div class="rss-create">

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
        </header>
        <div class="panel-body" >
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </section>   

</div>
