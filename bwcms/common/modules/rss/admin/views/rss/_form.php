<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\rss\models\Rss */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rss-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-6">
            <?= $form->field($model, 'url')->textInput() ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'name')->textInput() ?>
        </div>
    </div>
    
    <?php if(!$model->isNewRecord){ ?>
        <div class="row">
            <div class="col-12">
                <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
            </div>
        </div>
    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'create') : Yii::t('backend', 'save_changes'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
