<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\rss\models\Rss */

$this->title = Yii::t('backend_module_rss', 'rss').': '.$model->url;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend_module_rss', 'Rsses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rss-view">

    <p>
        <?= Html::a(Yii::t('backend_module_rss', 'back_to_list_rss'), ['index'], ['class' => 'btn btn-outline-secondary']) ?>
        <?= Html::a(Yii::t('backend', 'edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend', 'delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend_module_rss', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
        </header>
        <div class="panel-body" >
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'url:url',
                ],
            ]) ?>
        </div>
    </section>  
</div>
