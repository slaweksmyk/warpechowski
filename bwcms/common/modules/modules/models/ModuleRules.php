<?php

namespace common\modules\modules\models;

/**
 * This is the model class for table "module_actions".
 *
 * @property integer $id
 * @property integer $module_id
 * @property string $name
 * @property string $action
 * @property string $type
 *
 * @property Modules $module
 */
class ModuleRules extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%module_rules}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['module_id'], 'integer'],
            [['controller', 'url_regexp', 'action'], 'string', 'max' => 255],
            [['module_id'], 'exist', 'skipOnError' => true, 'targetClass' => Modules::className(), 'targetAttribute' => ['module_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'module_id' => 'Module ID',
            'controller' => 'Module controller',
            'url_regexp' => 'Url regexp',
            'action' => 'Module action',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModule()
    {
        return $this->hasOne(Modules::className(), ['id' => 'module_id']);
    }
    
    
    /**
     * Get module actions by Module ID
     * 
     * @param int $module_id
     * @return boolean|array
     */
    public static function getByModuleId($module_id = false)
    {
        if($module_id) {
            return self::find()->where(['module_id' => $module_id])->all();
        } else {
            return false;
        }
    }
    
}
