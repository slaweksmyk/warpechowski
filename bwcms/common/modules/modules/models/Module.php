<?php

namespace common\modules\modules\models;

use Yii;

/**
 * This is the model class for table "modules".
 *
 * @property integer $id
 * @property string $created_at
 * @property string $name
 * @property string $description
 * @property string $site
 * @property string $admin
 * @property integer $is_active
 *
 * @property PagesAdmin[] $pagesAdmins
 */
class Module extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%modules}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // db validation
            [['is_active'], 'integer'],
            [['name', 'description', 'site', 'admin'], 'string', 'max' => 255],
            
            // default values
            ['is_active', 'default', 'value' => 1],
            ['created_at', 'default', 'value' => date('Y-m-d H:i:s')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'name' => Yii::t('backend', 'name'),
            'description' => Yii::t('backend', 'description'),
            'site' => 'Site',
            'admin' => 'Admin',
            'is_active' => Yii::t('backend', 'is_active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPagesAdmins()
    {
        return $this->hasMany(PagesAdmin::className(), ['module_id' => 'id']);
    }
    
    
    /**
     * Delete module
     * 
     * @return boolean
     */
    public function moduleDelete($protected_id)
    {
        if( in_array($this->id, $protected_id) ) {
            return false;
        } else {
            return $this->delete();
        }
    }
    
    
    /**
     * Get active modules in website
     * 
     * @param bool $admin
     * @return array
     */
    public static function getActiveModules( $admin = false ) 
    {
        if($admin){ $action = 'admin'; }else{ $action = 'site'; }
        return self::find()
                ->where(['is_active' => 1])
                ->andWhere(['not', [$action => null]])
                ->all();
    }
    
    /**
     * Get Module by ID.
     * $select: ['id', '$created_at', 'name', 'description', 'site', 'admin', 'is_active']
     * 
     * @param int $module_id
     * @param array $select
     * @return boolean|array
     */
    public static function getById($module_id = false, $select = array()) {
        if( !empty($select) && $module_id ){
            return self::find()->select($select)->where(['id' => $module_id])->one();
        } elseif( $module_id ) {
            return self::find()->where(['id' => $module_id])->one();
        } else {
            return false;
        }
    }
    
}