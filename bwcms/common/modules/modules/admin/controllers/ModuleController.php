<?php
/**
 * Module applocation modules - main BACKEND controller
 * 
 * PHP version 7
 *
 * @author Volodymyr Shelelo <contact@profficode.com>
 * @copyright (c) 2016 Volodymyr Shelelo
 * @version 1.0
 */

namespace common\modules\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use common\modules\modules\models\Module;

/**
 * ModuleController implements the CRUD actions for Module model.
 */
class ModuleController extends Controller
{
    
    /**
     * Protected modules id
     * 
     * @var array
     */
    protected $protected_id = [1, 2, 3, 4, 5, 6];
    
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    
    /**
     * Before load controller actions
     * 
     * @param string $action
     * @return boolean
     */
    public function beforeAction($action)
    {
        if ( parent::beforeAction($action) ) {
            $sActionType = strtolower(str_replace("action", "", $action->actionMethod));

            if($sActionType == "index") $sActionType = "view";
            if(!Yii::$app->user->can($this->module->id."_{$sActionType}") ){
                return $this->redirect(Yii::$app->homeUrl);
            }
            return true;
        } else {
            return false;
        }
    }
    
    
    //---------------------- ACTIONS -------------------------------------------
    

    /**
     * Lists all Module models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->request->post()){
            $oModule = Module::getById(explode("_", Yii::$app->request->post()["name"])[1]);
            $oModule->is_active = $oModule->is_active ? 0 : 1;
            $oModule->save();

            exit;
        }
        
        return $this->render('index', [
            'oModuleRowset' => Module::find()->all()
        ]);
    }
    
    public function actionCreate()
    {
        $model = new Module();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->create_at = date("Y-m-d H:i:s");

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the Module model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Module the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Module::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
