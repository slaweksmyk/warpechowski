<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\modules\models\Module */

$this->title = Yii::t('backend', 'module_module_create');
?>
<div class="module-create">
<div class="module-update">

    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend', 'module_module_data'); ?></header>
        <div class="panel-body" >
            <?php echo $this->render('_form_create', [
                'model' => $model,
            ]) ?>
        </div>
    </section>
    
</div>