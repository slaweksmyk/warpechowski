<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('backend', 'module_module');

$this->registerJsFile('/bwcms/common/modules/modules/admin/assets/js/moduleStatus.js', ['depends' => [yii\web\JqueryAsset::className()]]);
?>
<div class="module-index">

    <p>
        <?php echo Html::a(Yii::t('backend', 'module_module_create'), ['create'], ['class' => 'btn btn-success']); ?>
    </p>
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend', 'module_module_list'); ?>
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <?php if(Yii::$app->request->get('per-page')){ echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>"; } ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin();  ?>
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th><?= Yii::t('backend', 'module_name') ?></th>
                            <th style="text-align: center;"><?= Yii::t('backend', 'module_is_enabled') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($oModuleRowset as $oModule){ ?>
                            <tr>
                                <td><?= $oModule->display_name ?></td>
                                <td style="text-align: center;">
                                    <input type="hidden" name="module_<?= $oModule->id ?>" value="<?= $oModule->id ?>"/>
                                    <div class="choice_selector <?php if($oModule->is_active) { echo "active"; } ?>"></div>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php ActiveForm::end(); ?>
        </div>
    </section>
    
</div>
