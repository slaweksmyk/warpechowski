<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="module-form">
    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-4">
            <?= $form->field($model, 'admin')->textInput(['maxlength' => true, 'value' => "common\modules\[name]\admin\[Name]"]) ?>
        </div>
        <div class="col-4">
            <?= $form->field($model, 'is_active')->dropDownList([1 =>  Yii::t('backend_module_articles', 'active'), 0 =>  Yii::t('backend_module_articles', 'inactive')]) ?>
        </div>
    </div>

    <?= $form->field($model, 'description')->textarea() ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'create') : Yii::t('backend', 'update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
