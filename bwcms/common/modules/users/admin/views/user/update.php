<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\user\models\User */

$this->title = Yii::t('backend', 'module_users_update'). $model->id;
?>

<div class="user-update">

    <p>
        <?= Html::a("< Wróć", ['index'], ['class' => 'btn btn-outline-secondary']) ?>
    </p>

    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend', 'update') ?> <?= Yii::t('backend', 'data') ?></header>
        <div class="panel-body" >
            <div class="user-form">
                <?php $form = ActiveForm::begin(); ?>
                
                    <div class="row">
                        <div class="col-4">
                            <?php echo $form->field($model, 'username')->textInput(['autofocus' => true])->label("Nazwa użytkownika"); ?>
                        </div>
                        <div class="col-4">
                            <?php echo $form->field($model, 'email'); ?>
                        </div>
                        <div class="col-4">
                            <?php 
                                $roles = array();
                                foreach ($user_roles as $role) {
                                    $roles[ $role->name ] = $role->name;
                                }
                                echo $form->field($model, 'role')->dropDownList( array_reverse($roles) )->label('Grupa');
                            ?>
                        </div>
                    </div>
                
                    <div class="row">
                        <div class="col-10"><?= $form->field($model, 'password')->textInput()->label("Hasło"); ?></div>
                        <div class="col-2">
                            <div class="btn btn-primary" id="generate-code" style="margin-top: 23px;">Generuj</div>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo Html::submitButton(Yii::t('backend', 'save'), ['class' => 'btn btn-primary', 'name' => 'signup-button']); ?>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </section>

</div>