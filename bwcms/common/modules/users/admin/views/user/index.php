<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\user\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'module_users');
?>
<div class="user-index">

    <p>
        <?php echo Html::a(Yii::t('backend', 'module_users_create'), ['create'], ['class' => 'btn btn-success']); ?>
    </p>
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend', 'module_users_list') ?>
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <?php if(Yii::$app->request->get('per-page')){ echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>"; } ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?php echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'header' => Yii::t('backend', 'LP')
                    ],

                    [
                        'attribute' => 'id',
                        'contentOptions'=>['style'=>'width: 70px']
                    ],
                    [
                        'attribute' => 'username',
                        'label' => 'Nazwa użytkownika'
                    ],
                    'email:email',
                    [
                        'attribute' => 'role',
                        'label' => 'Grupa'
                    ],

                    // 'auth_key',
                    // 'password_hash',
                    // 'password_reset_token',
                    // 'status',
                    // 'created_at',
                    // 'updated_at',

                    [
                        'class' => 'common\hooks\yii2\grid\ActionColumn',
                        'contentOptions'=>['style'=>'width: 74px; text-align: center']
                    ],
                ],
            ]); ?>
        </div>
    </section>
    
</div>
