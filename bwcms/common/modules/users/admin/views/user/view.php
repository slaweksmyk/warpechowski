<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\user\models\User */

$this->title = Yii::t('backend', 'module_users_user').$model->username;
?>
<div class="user-view">

    <p>
        <?php echo Html::a(Yii::t('backend', 'update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']); ?>
        <?php echo Html::a(Yii::t('backend', 'delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'delete_confirm'),
                'method' => 'post',
            ],
        ]); ?>
    </p>

    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend', 'update') ?> <?= Yii::t('backend', 'data') ?></header>
        <div class="panel-body" >
            <?php echo DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'username',
                    'email:email',
                    'role',
                    'status',
                    'created_at',
                    'updated_at',

                    // 'auth_key',
                    // 'password_hash',
                    // 'password_reset_token',
                ],
            ]); ?>
        </div>
    </section>

</div>
