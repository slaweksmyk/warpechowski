<?php
/**
 * Module Users - main BACKEND controller
 * 
 * PHP version 7
 *
 * @author Volodymyr Shelelo <contact@profficode.com>
 * @copyright (c) 2016 Volodymyr Shelelo
 * @version 1.0
 */

namespace common\modules\users\admin\controllers;

use Yii;
use common\modules\users\models\User;
use common\modules\users\models\UserSearch;
use common\modules\users\models\SignupForm;
use common\modules\users\models\UpdateForm;
use common\modules\logs\models\Log;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UsersController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    
    /**
     * Before load controller actions
     * 
     * @param string $action
     * @return boolean
     */
    public function beforeAction($action)
    {
        if ( parent::beforeAction($action) ) {
            if( !Yii::$app->user->can('adminUsers') ){
                return $this->redirect(Yii::$app->homeUrl);
            }
            return true;
        } else {
            return false;
        }
    }
    
    
    //---------------------- ACTIONS -------------------------------------------
    

    /**
     * Lists all User models.
     * 
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if(Yii::$app->request->get('per-page')){
            $dataProvider->setPagination(['pageSize' => Yii::$app->request->get('per-page')]);
        }
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    
    /**
     * Displays a single User model.
     * 
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    
    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * 
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SignupForm();
        $post = Yii::$app->request->post();
        
        if ( $post ) {
            $model->load($post);
            $user_id = $model->signup([ 'controller' => 'admin' ]);
            if ( $user_id ) { 
                return $this->redirect(['view', 'id' => $user_id]); 
            }
        }
        
        return $this->render('create', [
            'model' => $model,
            'user_roles' => \Yii::$app->authManager->getRoles()
        ]);
    }

    
    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * 
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $updateModel = new UpdateForm();
        $model = $updateModel->findById($id);
        $post = Yii::$app->request->post();
        
        if ( $post ) {
            $model->load($post);
            $user_update = $model->updateData();
            if( $user_update ){ 
                return $this->redirect(['view', 'id' => $id]); 
            }
        }
        
        return $this->render('update', [
            'model' => $model,
            'user_roles' => \Yii::$app->authManager->getRoles()
        ]);
    }

    
    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * 
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    
    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * 
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
