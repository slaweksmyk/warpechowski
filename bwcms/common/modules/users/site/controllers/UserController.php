<?php
/**
 * Module Users - main FRONTEND controller
 * 
 * PHP version 7
 *
 * @author Volodymyr Shelelo <contact@profficode.com>
 * @copyright (c) 2016 Volodymyr Shelelo
 * @version 1.0
 */

namespace common\modules\users\site\controllers;

use Yii;

use common\modules\users\models\User;
use common\modules\users\models\LoginForm;
use common\modules\users\models\PasswordResetRequestForm;
use common\modules\users\models\ResetPasswordForm;
use common\modules\users\models\SignupForm;

use common\modules\pages\models\PagesSite;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\filters\AccessControl;


/**
 * UsersController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    
    
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [ 'class' => 'yii\web\ErrorAction' ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    
    //---------------------- ACTIONS -------------------------------------------
    

    /**
     * Show User info
     * 
     * @return mixed
     */
    public function actionIndex()
    {
        $user = Yii::$app->user;
        
        if( $user->isGuest ) {
            return $this->goHome();
        } else {
            $userModel = new User();
            $model = $userModel->find()->select(['id', 'username', 'email'])->where(['id'=>$user->id])->one();
            
            return $this->render('index', [
                'model' => $model,
                'oPage' => Yii::$app->params['oPage']
            ]);
        }
    }
    
    
     /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        
        $url = \str_replace('/login/', '', Yii::$app->request->getUrl());
        $oPage = PagesSite::getByUrl(['id', 'name', 'description', 'slug', 'url', 'module_id'], $url);
        $oPage['name'] = 'Login '.$oPage['name'];
        
        Yii::$app->params['oPage'] = $oPage;
        $model = new LoginForm();
        
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
                'oPage' => $oPage
            ]);
        }
    }
    
    
    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        
        if ( $model->load(Yii::$app->request->post()) ) {
            $save_user = $model->signup([ 'controller' => 'site' ]);
            if ( $save_user ){ return $this->goHome(); }
        }
        
        $url = \str_replace('/signup/', '', Yii::$app->request->getUrl());
        $oPage = PagesSite::getByUrl(['id', 'name', 'description', 'slug', 'url', 'module_id'], $url);
        $oPage['name'] = 'Signup '.$oPage['name'];
        
        Yii::$app->params['oPage'] = $oPage;

        return $this->render('signup', [
            'model' => $model,
            'oPage' => $oPage
        ]);
    }

    
    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    
    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        
        $url = \str_replace('/request-password-reset/', '', Yii::$app->request->getUrl());
        $oPage = PagesSite::getByUrl(['id', 'name', 'description', 'slug', 'url', 'module_id'], $url);
        $oPage['name'] = $oPage['name'].': request password reset';
        
        Yii::$app->params['oPage'] = $oPage;
        
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail($oPage['url'])) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
            'oPage' => $oPage
        ]);
    }

    
    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }
        
        $allUrlExplode = \explode('/reset-password/?token=', Yii::$app->request->getUrl());
        
        $url = \str_replace('/', '', $allUrlExplode[0]);
        $oPage = PagesSite::getByUrl(['id', 'name', 'description', 'slug', 'url', 'module_id'], $url);
        $oPage['name'] = $oPage['name'].': reset password';
        
        Yii::$app->params['oPage'] = $oPage;

        return $this->render('resetPassword', [
            'model' => $model,
            'oPage' => $oPage
        ]);
    }
    
    
    protected function errorPage() {
        return $this->render('error');
    }
    

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * 
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
}
