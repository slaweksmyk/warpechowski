<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\modules\users\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $oPage['name'];
?>
<div class="site-signup">
    <h1><?= Html::encode($oPage['name']) ?></h1>

    <?php echo $oPage['description']; ?>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?php echo $form->field($model, 'username')->textInput(['autofocus' => true]); ?>

                <?php echo $form->field($model, 'email'); ?>

                <?php echo $form->field($model, 'password')->passwordInput(); ?>

                <div class="form-group">
                    <?php echo Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']); ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
