<div class="site-index" >
    
    <div class="jumbotron">
        <h1><?php echo $oPage['name']; ?></h1>
    </div>

    <div class="body-content" >
        <?php echo $oPage['description']; ?>
        
        <p>
            <?php 
                foreach($model as $key => $value) {
                    if($value){ echo "{$key}: <b>{$value}</b><br>"; } 
                } 
            ?>
        </p>
        
    </div>
    
</div>