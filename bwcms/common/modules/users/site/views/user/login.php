<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\modules\users\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->params['breadcrumbs'][] = $oPage['name'].' - Login';
?>

<div class="site-login">
    <h1><?php echo Html::encode($oPage['name']); ?></h1>
    
    <?php echo $oPage['description']; ?>

    <p>Please fill out the following fields to login:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?php echo $form->field($model, 'username')->textInput(['autofocus' => true]); ?>

                <?php echo $form->field($model, 'password')->passwordInput(); ?>

                <?php echo $form->field($model, 'rememberMe')->checkbox(); ?>

                <div style="color:#999;margin:1em 0">
                    If you forgot your password you can 
                    <a href="/<?php echo $oPage['url']; ?>/request-password-reset/" >reset it</a>.  
                </div>

                <div class="form-group">
                    <?php echo Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
