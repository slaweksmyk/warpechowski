<?php

namespace common\modules\users\models;

use Yii;
use common\modules\files\models\File;
use common\modules\users\models\User;

/**
 * This is the model class for table "user_data_company".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $krs_id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $website
 * @property string $city
 * @property string $district
 * @property string $street
 * @property string $nip
 * @property string $regon
 * @property string $krs
 *
 * @property File $avatar
 * @property User $user
 */
class UserDataCompany extends \common\hooks\yii2\db\ActiveRecord
{
    public $krs_file,$nip_file,$regon_file;
	public $discountCategories = [];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_data_company}}';
    }

    public function rules()
    {
        return [
            [['firstname', 'surname', 'city', 'postcode', 'email', 'nip', 'mobile', 'country', 'company', 'deliver_email', 'deliver_mobile', 'street', 'street_no', 'deliver_city', 'deliver_postcode', 'deliver_street_no', 'deliver_street'], 'required', "message" => "To pole jest wymagane"],
            [['user_id', 'krs_id', 'nip_id', 'regon_id'], 'integer'],
            [['email'], 'email', "message" => "Podany mail jest niepoprawny."],
            [['email'], 'unique', "message" => "Istnieje już konto zarejestrowane na ten adres e-mail"],
            [['firstname', 'surname', 'street', 'city', 'postcode', 'email', 'phone', 'mobile', 'country', 'nip', 'nip_sulf', 'deliver_email', 'deliver_mobile', 'deliver_phone', 'deliver_street', 'deliver_city', 'deliver_postcode', 'discount_percs', 'discount_pln', 'discount_eur', 'discount_categories' ], 'safe'],
            //[['krs_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['krs_id' => 'id']],
            //[['nip_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['nip_id' => 'id']],
            //[['regon_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['regon_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['patron_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['patron_id' => 'id']],
             
            [['krs_file'], 'file', 'skipOnEmpty' => true],
            [['nip_file'], 'file', 'skipOnEmpty' => true],
            [['regon_file'], 'file', 'skipOnEmpty' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_shop', 'ID'),
            'order_id' => Yii::t('backend_module_shop', 'Order ID'),
            'user_id' => Yii::t('backend_module_shop', 'User ID'),
            'firstname' => Yii::t('backend_module_shop', 'Firstname'),
            'surname' => Yii::t('backend_module_shop', 'Surname'),
            'street' => Yii::t('backend_module_shop', 'Street'),
            'city' => Yii::t('backend_module_shop', 'City'),
            'postcode' => Yii::t('backend_module_shop', 'Postcode'),
            'email' => Yii::t('backend_module_shop', 'Email'),
            'phone' => Yii::t('backend_module_shop', 'Phone'),
            'mobile' => Yii::t('backend_module_shop', 'Mobile'),
            'country' => Yii::t('backend_module_shop', 'Country'),
            'nip' => Yii::t('backend_module_shop', 'Nip'),
            'deliver_email' => Yii::t('backend_module_shop', 'deliver_email'),
            'deliver_mobile' => Yii::t('backend_module_shop', 'deliver_mobile'),
            'deliver_phone' => Yii::t('backend_module_shop', 'deliver_phone'),
            'deliver_street' => Yii::t('backend_module_shop', 'deliver_street'),
            'deliver_city' => Yii::t('backend_module_shop', 'deliver_city'),
            'deliver_postcode' => Yii::t('backend_module_shop', 'deliver_postcode'),
			'discount_perc' => '% zniżki',
			'discount_pln' => 'zniżka (zł)',
			'discount_eur' => 'zniżka (€)'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKRS()
    {
        return $this->hasOne(File::className(), ['id' => 'krs_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNIP()
    {
        return $this->hasOne(File::className(), ['id' => 'nip_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getREGON()
    {
        return $this->hasOne(File::className(), ['id' => 'regon_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatron()
    {
        return $this->hasOne(User::className(), ['id' => 'patron_id']);
    }
}
