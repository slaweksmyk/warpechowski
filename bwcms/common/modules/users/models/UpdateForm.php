<?php

namespace common\modules\users\models;

use common\modules\users\models\User;
use common\modules\users\models\AuthAssignment;

/**
 * Signup form
 */
class UpdateForm extends \common\hooks\yii2\db\ActiveRecord
{
       
    public $password;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            [
                'username', 'unique', 'targetClass' => '\common\modules\users\models\User', 'message' => 'This username has already been taken.',
                'when' => function ($model, $attribute) { return $model->{$attribute} !== $model->getOldAttribute($attribute); },
            ],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['password', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            [
                'email', 'unique', 'targetClass' => '\common\modules\users\models\User', 'message' => 'This email address has already been taken.',
                'when' => function ($model, $attribute) { return $model->{$attribute} !== $model->getOldAttribute($attribute); },
            ],
            
            ['role', 'string', 'max' => 255],
            ['role', 'default', 'value' => 'user'],
        ];
    }
   

    /**
     * Update User
     *
     * @return User|null the saved model or null if saving fails
     */
    public function updateData()
    {
        $this->updated_at = date('Y-m-d H:i:s');
		
        $user = User::find()->where(["=", "id", $this->id])->one();
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->save();
        
        if ( $this->save() ){

            $AuthAssignment = new AuthAssignment();
            $auth_assignment = $AuthAssignment->findOne(['user_id' => $this->id]);
            
            if( $auth_assignment ){
                $user_role = \Yii::$app->authManager->getRole($this->role);
                $user_role_name = $user_role->name;
            } else {
                $user_role_name = null;
            }

            if( $user_role_name ) {
                $auth_assignment->item_name = $user_role_name;
                $auth_assignment->update(false, ['item_name']);
            }
            
            return true;
        } else {
            return null;
        }
    }
    
    
    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * 
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findById($id)
    {
        $model = $this->findOne(['id' => $id]);
        
        if ( $model ) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    
}