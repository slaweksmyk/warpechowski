<?php

namespace common\modules\users\models;

use Yii;
use common\modules\files\models\File;
use common\modules\users\models\User;

/**
 * This is the model class for table "user_data".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $avatar_id
 * @property string $firstname
 * @property string $surname
 * @property string $phone
 * @property string $city
 * @property string $district
 * @property string $street
 *
 * @property File $avatar
 * @property User $user
 */
class UserData extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_data}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'avatar_id'], 'integer'],
            [['firstname', 'surname', 'phone', 'city', 'district', 'street', 'birth_date'], 'string', 'max' => 255],
            [['avatar_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['avatar_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_user', 'ID'),
            'user_id' => Yii::t('backend_module_user', 'User ID'),
            'avatar_id' => Yii::t('backend_module_user', 'Avatar ID'),
            'firstname' => Yii::t('backend_module_user', 'Firstname'),
            'surname' => Yii::t('backend_module_user', 'Surname'),
            'phone' => Yii::t('backend_module_user', 'Phone'),
            'city' => Yii::t('backend_module_user', 'City'),
            'district' => Yii::t('backend_module_user', 'District'),
            'street' => Yii::t('backend_module_user', 'Street'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAvatar($width = 2000, $height = 2000, $type = "crop", $effect = null, $quality = 80) 
    { 
        return File::getThumbnail($this->avatar_id, $width, $height, $type, $effect, $quality);
    } 

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
