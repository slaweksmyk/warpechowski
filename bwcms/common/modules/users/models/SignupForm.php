<?php
namespace common\modules\users\models;

use yii\base\Model;
use common\modules\users\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $role;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\modules\users\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\modules\users\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            
            ['role', 'string', 'max' => 255],
            ['role', 'default', 'value' => 'user'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup( $options = array() )
    {
        if (!$this->validate()) { return null; }
        
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->role = $this->role;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        
        $userSave = $user->save();
        
        if( $userSave ) {
            $user->updateUserDate([ 'updated_at' => NULL, 'created_at' => date('Y-m-d H:i:s') ]);
            
            $auth = \Yii::$app->authManager;
            $user_role = $auth->getRole($this->role);
            $user_id = $user->getId();
            $auth->assign($user_role, $user_id);
            
            if( $options['controller'] == 'site' ) { \Yii::$app->user->login($user, $user ? 3600 * 24 * 30 : 0); }
            
            return $user_id;
        } else {
            return null;
        }
    }
    
}