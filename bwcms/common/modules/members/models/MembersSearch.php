<?php

namespace common\modules\members\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\members\models\Members;

/**
 * MembersSearch represents the model behind the search form about `common\modules\members\models\Members`.
 */
class MembersSearch extends Members
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],
            [['name', 'surname', 'nip', 'company', 'trade', 'spins', 'street', 'post_code', 'post_city', 'krs', 'email', 'number_employees', 'phone', 'founded', 'date_created', 'date_expire'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Members::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'date_created' => $this->date_created,
            'date_expire' => $this->date_expire,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'surname', $this->surname])
            ->andFilterWhere(['like', 'nip', $this->nip])
            ->andFilterWhere(['like', 'company', $this->company])
            ->andFilterWhere(['like', 'trade', $this->trade])
            ->andFilterWhere(['like', 'spins', $this->spins])
            ->andFilterWhere(['like', 'street', $this->street])
            ->andFilterWhere(['like', 'post_code', $this->post_code])
            ->andFilterWhere(['like', 'post_city', $this->post_city])
            ->andFilterWhere(['like', 'krs', $this->krs])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'number_employees', $this->number_employees])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'founded', $this->founded]);

        return $dataProvider;
    }
}
