<?php

namespace common\modules\members\models;

use Yii;

/**
 * This is the model class for table "{{%xmod_members}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $surname
 * @property string $nip
 * @property string $company
 * @property string $trade
 * @property string $spins
 * @property string $street
 * @property string $post_code
 * @property string $post_city
 * @property string $krs
 * @property string $email
 * @property string $number_employees
 * @property string $phone
 * @property string $founded
 * @property integer $status
 * @property string $date_created
 * @property string $date_expire
 */
class Members extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_members}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['date_created', 'date_expire'], 'safe'],
            [['name', 'surname', 'nip', 'company', 'trade', 'spins', 'street', 'post_code', 'post_city', 'krs', 'email', 'number_employees', 'phone', 'founded'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_members', 'ID'),
            'name' => Yii::t('backend_module_members', 'Name'),
            'surname' => Yii::t('backend_module_members', 'Surname'),
            'nip' => Yii::t('backend_module_members', 'Nip'),
            'company' => Yii::t('backend_module_members', 'Company'),
            'trade' => Yii::t('backend_module_members', 'Trade'),
            'spins' => Yii::t('backend_module_members', 'Spins'),
            'street' => Yii::t('backend_module_members', 'Street'),
            'post_code' => Yii::t('backend_module_members', 'Post Code'),
            'post_city' => Yii::t('backend_module_members', 'Post City'),
            'krs' => Yii::t('backend_module_members', 'Krs'),
            'email' => Yii::t('backend_module_members', 'Email'),
            'number_employees' => Yii::t('backend_module_members', 'Number Employees'),
            'phone' => Yii::t('backend_module_members', 'Phone'),
            'founded' => Yii::t('backend_module_members', 'Founded'),
            'status' => Yii::t('backend_module_members', 'Status'),
            'date_created' => Yii::t('backend_module_members', 'Date Created'),
            'date_expire' => Yii::t('backend_module_members', 'Date Expire'),
        ];
    }
}
