<?php
namespace common\modules\members\admin;

/**
 * menu module definition class
 */
class Members extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\members\admin\controllers';
    public $defaultRoute = 'members';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
