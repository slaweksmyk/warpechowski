<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('backend_module_download', 'Update {modelClass}: ', [
    'modelClass' => 'Members',
]) . $model->name." ".$model->surname;
?>
<div class="members-update">

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
        </header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(); ?>

            <div class="row">
                <div class="col-6"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
                <div class="col-6"><?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?></div>
            </div>
            
            <div class="row">
                <div class="col-4"><?= $form->field($model, 'date_created')->textInput() ?></div>
                <div class="col-4"><?= $form->field($model, 'date_expire')->textInput() ?></div>
                <div class="col-4"><?= $form->field($model, 'status')->dropDownList([0 => "Nieaktywny", 1 => "Aktywny"]) ?></div>
            </div>

            <div class="row">
                <div class="col-2"><?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?></div>
                <div class="col-2"><?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
                <div class="col-2"><?= $form->field($model, 'nip')->textInput(['maxlength' => true]) ?></div>
                <div class="col-2"><?= $form->field($model, 'company')->textInput(['maxlength' => true]) ?></div>
                <div class="col-2"><?= $form->field($model, 'trade')->textInput(['maxlength' => true]) ?></div>
                <div class="col-2"><?= $form->field($model, 'spins')->textInput(['maxlength' => true]) ?></div>
            </div> 

            <div class="row">
                <div class="col-2"><?= $form->field($model, 'post_city')->textInput(['maxlength' => true]) ?></div>
                <div class="col-2"><?= $form->field($model, 'krs')->textInput(['maxlength' => true]) ?></div>
                <div class="col-2"><?= $form->field($model, 'founded')->textInput(['maxlength' => true]) ?></div>
                <div class="col-2"><?= $form->field($model, 'number_employees')->textInput(['maxlength' => true]) ?></div>
                <div class="col-2"><?= $form->field($model, 'street')->textInput(['maxlength' => true]) ?></div>
                <div class="col-2"><?= $form->field($model, 'post_code')->textInput(['maxlength' => true]) ?></div>
            </div>
            
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_download', 'Create') : Yii::t('backend_module_download', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                <?= Html::a("Aktywuj", ['activate', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
            </div>
   
            <?php ActiveForm::end(); ?>
        </div>
    </section>
</div>
