<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\members\models\Members */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend_module_download', 'Members'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="members-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('backend_module_download', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend_module_download', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend_module_download', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'surname',
            'nip',
            'company',
            'trade',
            'spins',
            'street',
            'post_code',
            'post_city',
            'krs',
            'email:email',
            'number_employees',
            'phone',
            'founded',
            'status',
            'date_created',
            'date_expire',
        ],
    ]) ?>

</div>
