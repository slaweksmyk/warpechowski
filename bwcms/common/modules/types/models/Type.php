<?php

namespace common\modules\types\models;

use Yii;
use common\modules\types\models\TypeHash;

/**
 * This is the model class for table "xmod_articles_types".
 *
 * @property integer $id
 * @property string $name
 *
 * @property XmodArticlesTypesHash[] $xmodArticlesTypesHashes
 */
class Type extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_articles_types}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_types', 'ID'),
            'name' => Yii::t('backend_module_types', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(TypeHash::className(), ['type_id' => 'id']);
    }
}
