<?php

namespace common\modules\types\models;

use Yii;
use common\modules\types\models\Type;
use common\modules\articles\models\Article;

/**
 * This is the model class for table "xmod_articles_types_hash".
 *
 * @property integer $id
 * @property string $article_id
 * @property integer $type_id
 *
 * @property Article $article
 * @property Type $type
 */
class TypeHash extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_articles_types_hash}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article_id', 'type_id'], 'integer'],
            [['article_id'], 'exist', 'skipOnError' => true, 'targetClass' => Article::className(), 'targetAttribute' => ['article_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Type::className(), 'targetAttribute' => ['type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_types', 'ID'),
            'article_id' => Yii::t('backend_module_types', 'Article ID'),
            'type_id' => Yii::t('backend_module_types', 'Type ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'article_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(Type::className(), ['id' => 'type_id']);
    }
}
