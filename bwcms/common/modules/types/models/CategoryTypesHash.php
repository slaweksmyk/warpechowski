<?php

namespace common\modules\types\models;

use Yii;
use common\modules\types\models\Type;
use common\modules\categories\models\ArticlesCategories;

/**
 * This is the model class for table "xmod_articles_category_types_hash".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $type_id
 *
 * @property ArticlesCategories $category
 * @property Type $type
 */
class CategoryTypesHash extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_articles_category_types_hash}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'type_id'], 'integer'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ArticlesCategories::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Type::className(), 'targetAttribute' => ['type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_types', 'ID'),
            'category_id' => Yii::t('backend_module_types', 'Category ID'),
            'type_id' => Yii::t('backend_module_types', 'Type ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ArticlesCategories::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(Type::className(), ['id' => 'type_id']);
    }
}
