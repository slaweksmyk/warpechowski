<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->name;
?>
<div class="type-view">

    <p>
        <?= Html::a('< Wróć  do listy', ['index'], ['class' => 'btn btn-outline-secondary']) ?>
    </p>

     <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
        </header>
        <div class="panel-body" >
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>
        </div>
    </section>

</div>
