<?php

namespace common\modules\types\admin;

/**
 * files module definition class
 */
class Types extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\types\admin\controllers';
    public $defaultRoute = 'types';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
