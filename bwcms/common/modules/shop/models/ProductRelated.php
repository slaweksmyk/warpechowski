<?php

namespace common\modules\shop\models;

use Yii;
use common\modules\shop\models\Product;

/**
 * This is the model class for table "{{%xmod_shop_products_related}}".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $related_id
 *
 * @property XmodShopProductsList $product
 * @property XmodShopProductsList $related
 */
class ProductRelated extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_shop_products_related}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'related_id'], 'integer'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['related_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['related_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_shop', 'ID'),
            'product_id' => Yii::t('backend_module_shop', 'Product ID'),
            'related_id' => Yii::t('backend_module_shop', 'Related ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id'])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelated()
    {
        return $this->hasOne(Product::className(), ['id' => 'related_id'])->one();
    }
}
