<?php

namespace common\modules\shop\models;

use Yii;
use common\modules\shop\models\Product;
use common\modules\shop\models\ShopOrders;

/**
 * This is the model class for table "xmod_shop_orders_items".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $product_id
 * @property integer $amount
 * @property string $price_netto
 * @property string $price_brutto
 *
 * @property ShopOrders $order
 * @property Product $product
 */
class ShopOrdersItems extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_shop_orders_items}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'product_id', 'amount'], 'integer'],
            [['name', 'vat', 'currency', 'discount_code'], 'safe'],
            [['price_netto', 'price_brutto', 'org_price_netto', 'org_price_brutto'], 'number'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => ShopOrders::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_shop', 'ID'),
            'order_id' => Yii::t('backend_module_shop', 'Order ID'),
            'product_id' => Yii::t('backend_module_shop', 'Product ID'),
            'amount' => Yii::t('backend_module_shop', 'Amount'),
            'price_netto' => Yii::t('backend_module_shop', 'Price Netto'),
            'price_brutto' => Yii::t('backend_module_shop', 'Price Brutto'),
            'org_price_netto' => Yii::t('backend_module_shop', 'org_price_netto'),
            'org_price_brutto' => Yii::t('backend_module_shop', 'org_price_brutto'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(ShopOrders::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
