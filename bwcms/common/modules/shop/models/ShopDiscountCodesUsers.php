<?php

namespace common\modules\shop\models;

use Yii;
use common\modules\users\models\User;
use common\modules\shop\models\ShopDiscountCodes;


/**
 * This is the model class for table "{{%xmod_shop_discount_codes_users}}".
 *
 * @property int $id
 * @property int $code_id
 * @property int $user_id
 *
 * @property ShopDiscountCodes $code
 * @property User $user
 */
class ShopDiscountCodesUsers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%xmod_shop_discount_codes_users}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code_id', 'user_id'], 'integer'],
            [['code_id'], 'exist', 'skipOnError' => true, 'targetClass' => ShopDiscountCodes::className(), 'targetAttribute' => ['code_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code_id' => 'Code ID',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCode()
    {
        return $this->hasOne(ShopDiscountCodes::className(), ['id' => 'code_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
