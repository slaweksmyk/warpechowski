<?php

namespace common\modules\shop\models;

use Yii;
use common\modules\shop\models\ShopOrders;

/**
 * This is the model class for table "xmod_shop_delivers".
 *
 * @property integer $id
 * @property string $name
 * @property double $price_brutto
 *
 * @property ShopOrders[] $xmodShopOrders
 */
class ShopDelivers extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_shop_delivers}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price_brutto'], 'number'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_shop', 'ID'),
            'name' => Yii::t('backend_module_shop', 'Name'),
            'price_brutto' => Yii::t('backend_module_shop', 'Price Brutto'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopOrders()
    {
        return $this->hasMany(ShopOrders::className(), ['deliver_id' => 'id']);
    }
}
