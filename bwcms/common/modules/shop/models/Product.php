<?php

namespace common\modules\shop\models;

use Yii;

use common\modules\shop\models\Stock;
use common\modules\shop\models\Producer;
use common\modules\shop\models\Category;
use common\modules\shop\models\PriceList;
use common\modules\shop\models\PriceProduct;
use common\modules\shop\models\Product_i18n;
use common\modules\shop\models\ProductRelated;
use common\modules\shop\models\ProductAttribute;

use common\modules\promotion\models\PromotionDaily;
use common\modules\promotion\models\PromotionReduction;
use common\modules\promotion\models\PromotionConnection;

use common\modules\files\models\File;
use common\modules\gallery\models\Gallery;
use common\modules\shop\models\ProductAccessory;

use creocoder\translateable\TranslateableBehavior;
use common\modules\shop\models\ProductComment;

use yii\imagine\Image;
use Imagine\Image\Box;

use yii\helpers\BaseUrl;

/**
 * This is the model class for table "{{%xmod_shop_products_list}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $code
 * @property string $sticker
 * @property integer $category_id
 * @property integer $thumbnail_id
 * @property integer $gallery_id
 * @property integer $vat
 * @property string $price_netto
 * @property string $price_brutto
 * @property string $old_price_netto
 * @property string $old_price_brutto
 * @property string $short_description
 * @property string $description
 * @property integer $is_active
 * @property string $energy_class
 * @property integer $energy_class_img
 *
 * @property XmodShopCategories $category
 * @property XmodGallery $gallery
 * @property Files $thumbnail
 * @property XmodShopProductsRelated[] $xmodShopProductsRelateds
 * @property XmodShopProductsRelated[] $xmodShopProductsRelateds0
 */
class Product extends \common\hooks\yii2\db\ActiveRecord
{
    
    private $aForcedValues = [];
    private $sShortCurrency;
    public $currency = "zł";
    public $isOnPromotion = false;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_shop_products_list}}';
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => ['name', 'short_description', 'description'],
                'translationLanguageAttribute' => 'language',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_INSERT | self::OP_UPDATE,
        ];
    }  

    /**
     * @inheritdoc
     */
    public function getTranslations()
    {
        return $this->hasMany(Product_i18n::className(), ['xmod_shop_products_list_id' => 'id']);
    }    
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_created', 'date_updated', 'sort'], 'safe'],
            [['category_id', 'thumbnail_id', 'gallery_id', 'producer_id', 'vat', 'is_active', 'is_hidden_price', 'is_bestseller', 'is_slider', 'is_recomended', 'low_state'], 'integer'],
            [['price_netto', 'price_brutto', 'old_price_netto', 'old_price_brutto'], 'number'],
            [['short_description', 'description', 'add_category_hash', 'color', 'color_name', 'hidden_price_desc', 'seo_description'], 'string'],
            [['name', 'slug', 'code', 'sticker', 'seo_title', 'seo_keywords'], 'string', 'max' => 1024],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['gallery_id'], 'exist', 'skipOnError' => true, 'targetClass' => Gallery::className(), 'targetAttribute' => ['gallery_id' => 'id']],
            [['producer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Producer::className(), 'targetAttribute' => ['producer_id' => 'id']],
            [['thumbnail_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['thumbnail_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_shop', 'ID'),
            'name' => Yii::t('backend_module_shop', 'Name'),
            'slug' => Yii::t('backend_module_shop', 'Slug'),
            'code' => Yii::t('backend_module_shop', 'Code'),
            'sticker' => Yii::t('backend_module_shop', 'Sticker'),
            'category_id' => Yii::t('backend_module_shop', 'Category ID'),
            'add_category_hash' => Yii::t('backend_module_shop', 'Add Category ID'),
            'thumbnail_id' => Yii::t('backend_module_shop', 'Thumbnail ID'),
            'gallery_id' => Yii::t('backend_module_shop', 'Gallery ID'),
            'vat' => Yii::t('backend_module_shop', 'Vat'),
            'price_netto' => Yii::t('backend_module_shop', 'Price Netto'),
            'price_brutto' => Yii::t('backend_module_shop', 'Price Brutto'),
            'old_price_netto' => Yii::t('backend_module_shop', 'Old Price Netto'),
            'old_price_brutto' => Yii::t('backend_module_shop', 'Old Price Brutto'),
            'short_description' => Yii::t('backend_module_shop', 'Short Description'),
            'description' => Yii::t('backend_module_shop', 'Description'),
            'hidden_price_desc' => Yii::t('backend_module_shop', 'Hidden price desc'),
            'is_hidden_price' => Yii::t('backend_module_shop', 'Is hidden price'),
            'low_state' => "Stan niski",
            'is_bestseller' => "Czy bestseller",
            'is_slider' => "Promowany na sliderze",
            'is_recomended' => "Czy polecany",
            'is_active' => Yii::t('backend_module_shop', 'Is Active'),
            'seo_title' => "Tytuł SEO",
            'seo_description' => "Opis SEO",
            'seo_keywords' => "Słowa kluczowe",
            'Producer Id' => Yii::t('backend_module_shop', 'Producer Id'),
        ];
    }
    
    public function getAbsoluteUrl(){
        return trim(BaseUrl::home(true), "/").$this->getUrl();
    }
   
    public function getUrl(){
        if(!$this->getCategory()){
            return $this->slug;
        }
        
        return $this->getCategory()->getExtendUrl().$this->slug;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id'])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGallery()
    {
        return $this->hasOne(Gallery::className(), ['id' => 'gallery_id']);
    }
    
    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getProducer()
    { 
        return $this->hasOne(Producer::className(), ['id' => 'producer_id']);
    } 
    
    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getAccessories()
    { 
        return $this->hasMany(ProductAccessory::className(), ['product_id' => 'id']);
    }

    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getThumbnail($width = 2000, $height = 2000, $type = "crop", $effect = null, $quality = 80) 
    { 
        return File::getThumbnail($this->thumbnail_id, $height, $width, $type, $effect, $quality);
    } 
    
    
    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getEnergyClassThumbnail($width = 2000, $height = 2000, $type = "crop", $effect = null, $quality = 80) 
    { 
        $oThumbnail = $this->hasOne(File::className(), ['id' => 'energy_class_img'])->one();

        $effect_safe = str_replace("#", "", $effect);
        $sFilePath = Yii::getAlias("@root/upload/{$oThumbnail->filename}");
        $sReworkedPath = Yii::getAlias("@root/upload/reworked/{$width}_{$height}_{$type}_{$effect_safe}_{$oThumbnail->filename}");
        
        if(!file_exists($sReworkedPath)){
            if(file_exists($sFilePath)){
                switch($type){
                    case "crop";
                        Image::thumbnail($sFilePath, $height, $width)->save($sReworkedPath, ['quality' => $quality]);
                        break;

                    case "matched";
                        Image::getImagine()->open($sFilePath)->thumbnail(new Box($width, $height))->save($sReworkedPath , ['quality' => $quality]);
                        break;
                }

                if(!is_null($effect)){
                    $image = yii\imagine\Image::getImagine();
                    $newImage = $image->open($sReworkedPath);

                    switch($effect){
                        case "grayscale":
                            $newImage->effects()->grayscale();
                            break;

                        case "negative":
                            $newImage->effects()->negative();
                            break;

                        default:
                            if(ctype_xdigit($effect_safe)){
                                $color = $newImage->palette()->color($effect);
                                $newImage->effects()->colorize($color);
                            }
                            break;

                    }
                    $newImage->save($sReworkedPath, ['quality' => $quality]);
                }
            }
        }
        
        return "/upload/reworked/{$width}_{$height}_{$type}_{$effect_safe}_{$oThumbnail->filename}";
    } 

    
    
    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function hasThumbnail(){
        return !is_null($this->hasOne(File::className(), ['id' => 'thumbnail_id'])->one());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductsRelated()
    {
        return $this->hasMany(ProductRelated::className(), ['product_id' => 'id'])->all();
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAttributes()
    {
        return $this->hasMany(ProductAttribute::className(), ['product_id' => 'id'])->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttrvals() {
        return $this->hasMany(ProductAttribute::className(), ['product_id' => 'id']);
    }

    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductprice() {
        return $this->hasOne(PriceProduct::className(), ['product_id' => 'id']);
    }

    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getStocks()
    { 
        return $this->hasMany(Stock::className(), ['product_id' => 'id']);
    }
    
    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getComments()
    { 
        return $this->hasMany(ProductComment::className(), ['product_id' => 'id'])->orderBy("ID DESC");
    }
    
    public function getPriceNetto($isCurrency = false, $isOld = false)
    {
        $fPriceNetto = floatval($isOld ? $this->old_price_netto : $this->price_netto);

        foreach($this->getAccessories()->all() as $oAccessory){
            $fPriceNetto += $oAccessory->getAccessory()->one()->getPriceNetto(false, $isOld);
        }

        if($isOld){
            if($fPriceNetto <= $this->getPriceNetto(false, false) || $fPriceNetto == 0.00){ return; }
        }
        
        if($isCurrency){
            return number_format($fPriceNetto, 2, '.', '').$this->currency;
        } else {
            return number_format($fPriceNetto, 2, '.', '');
        }
    }
    
    public function getPriceBrutto($isCurrency = false, $isOld = false)
    {
        $fPriceBrutto = floatval($isOld ? $this->old_price_brutto : $this->price_brutto);
        
        foreach($this->getAccessories()->all() as $oAccessory){
            $fPriceBrutto += $oAccessory->getAccessory()->one()->getPriceBrutto(false, $isOld);
        }

        if($isOld){
            if($fPriceBrutto <= $this->getPriceBrutto(false, false) || $fPriceBrutto == 0.00){ return; }
        }
        
        if($isCurrency){
            return number_format($fPriceBrutto, 2, '.', '').$this->currency;
        } else {
            return number_format($fPriceBrutto, 2, '.', '');
        }
    }
    
    
    public function afterFind()
    {
        $this->checkForPricings();
        $this->checkForPromotion();
        
        parent::afterFind();
    }
    
    protected function checkForPricings()
    {
        if(Yii::$app->session->get('frontend_language') == "pl-PL"){
            $oPriceList = PriceList::find()->where(["=", "short_name", "PL"])->one();
        } else {
            $oPriceList = PriceList::find()->where(["=", "is_default", 1])->one();
        }
        $oPriceProduct = PriceProduct::find()->where(["=","shop_price_id", $oPriceList->id])->andWhere(["=", "product_id", $this->id])->one();

        $this->sShortCurrency = $oPriceList->short_currency;
        
        $this->currency = $oPriceList->symbol_currency;
        if(!is_null($oPriceProduct)){
            $this->aForcedValues["price_netto"] = $oPriceProduct->price_netto;
            $this->aForcedValues["price_brutto"] = $oPriceProduct->price_brutto;
            $this->aForcedValues["old_price_netto"] = $oPriceProduct->old_price_netto;
            $this->aForcedValues["old_price_brutto"] = $oPriceProduct->old_price_brutto;
        }
    }
    
    protected function setPromotionLabels($oPromotion, $sPromotionName, $sPromotionType = 'default'){
        $promotionLabels = Yii::$app->session->get('promotion_labels');
        
        if ($sPromotionType == 'connected') {
            foreach($oPromotion as $oPromo) {
                $promotionLabels[$oPromo->reward_id] = $sPromotionName;
                foreach ($oPromo->decodeElements()->product as $iConnectedProductId) {
                    $promotionLabels[$iConnectedProductId] = $sPromotionName;
                }
            }
        } else {
            $promotionLabels[$oPromotion->product_id] = $sPromotionName;
        }
                
        Yii::$app->session->set('promotion_labels', $promotionLabels);
    }
    
    protected function checkForPromotion(){
        $oUser = Yii::$app->session->get('oUser');
        if($oUser){
            $oClient = $oUser->getDataCompany()->one();

            if($oClient->discount_categories){
                $aDiscounts = json_decode($oClient->discount_categories, true);
                if(isset($aDiscounts[$this->category_id]["perc"]) && $aDiscounts[$this->category_id]["perc"] > 0){
                    $fPerc = floatval($aDiscounts[$this->category_id]["perc"]);

                    if(isset($this->aForcedValues["price_netto"])){
                        $this->aForcedValues["price_netto"] -= $this->aForcedValues["price_netto"]*($fPerc/100);
                    } else {
                        $this->aForcedValues["price_netto"]  = $this->getPriceNetto() - $this->getPriceNetto()*($fPerc/100);
                    }
                    if(isset($this->aForcedValues["price_brutto"])){
                        $this->aForcedValues["price_brutto"] -= $this->aForcedValues["price_brutto"]*($fPerc/100);
                    } else {
                        $this->aForcedValues["price_brutto"]  = $this->getPriceBrutto() - $this->getPriceBrutto()*($fPerc/100);
                    }
                }

                if((isset($aDiscounts[$this->category_id][$this->sShortCurrency]) && $aDiscounts[$this->category_id][$this->sShortCurrency] > 0)){
                    $fDiscountSum = floatval($aDiscounts[$this->category_id][$this->sShortCurrency]);

                    if(isset($this->aForcedValues["price_netto"])){
                        $this->aForcedValues["price_netto"] -= $fDiscountSum;
                    } else {
                        $this->aForcedValues["price_netto"] = $this->getPriceNetto() - $fDiscountSum;
                    }
                    if(isset($this->aForcedValues["price_brutto"])){
                        $this->aForcedValues["price_brutto"] = $this->aForcedValues["price_netto"] + ($this->aForcedValues["price_netto"] * $this->vat/100);
                    } else {
                        $this->aForcedValues["price_brutto"]  = $this->getPriceBrutto() - $fDiscountSum;
                    }
                }
            } else {
                if($oClient->discount_percs > 0){
                    if(isset($this->aForcedValues["price_netto"])){
                        $this->aForcedValues["price_netto"] -= $this->aForcedValues["price_netto"]*($oClient->discount_percs/100);
                    } else {
                        $this->aForcedValues["price_netto"]  = $this->getPriceNetto() - $this->getPriceNetto()*($oClient->discount_percs/100);
                    }
                    if(isset($this->aForcedValues["price_brutto"])){
                        $this->aForcedValues["price_brutto"] -= $this->aForcedValues["price_brutto"]*($oClient->discount_percs/100);
                    } else {
                        $this->aForcedValues["price_brutto"]  = $this->getPriceBrutto() - $this->getPriceBrutto()*($oClient->discount_percs/100);
                    }
                } else {
                    $aDiscountAmount = json_decode($oClient->discount_amounts, true);

                    if((isset($aDiscountAmount[$this->sShortCurrency]) && $aDiscountAmount[$this->sShortCurrency] > 0)){
                        $fDiscountSum = floatval($aDiscountAmount[$this->sShortCurrency]);
                        
                        if(isset($this->aForcedValues["price_netto"])){
                            $this->aForcedValues["price_netto"] -= $fDiscountSum;
                        } else {
                            $this->aForcedValues["price_netto"] = $this->getPriceNetto() - $fDiscountSum;
                        }
                        if(isset($this->aForcedValues["price_brutto"])){
                            $this->aForcedValues["price_brutto"] = $this->aForcedValues["price_netto"] + ($this->aForcedValues["price_netto"] * $this->vat/100);
                        } else {
                            $this->aForcedValues["price_brutto"]  = $this->getPriceBrutto() - $fDiscountSum;
                        }
                    }
                }
            }
        }
        
        // Connection promotion
        $oPromotionConnection = PromotionConnection::find()->where(["=", "reward_id", $this->id])->all();
        if(!empty($oPromotionConnection)){
            $aCart = Yii::$app->session->get('cart');
            
            foreach($oPromotionConnection as $oPromo){
                $bTest = true;
                
                foreach($oPromo->decodeElements()->product as $iProductID){
                    $bTest = isset($aCart[$iProductID]);
                }
                
                if($bTest){
                    $this->isOnPromotion = true;
                    $this->setPromotionLabels($oPromotionConnection, 'Promocja', 'connected');
                    $this->recalculateConnectionProductPromotion($oPromo);
                }
            }
        } else {
            // Daily promotion
            $oPromotionDaily = PromotionDaily::find()->where(["<=", "active_from", date("Y-m-d h:i:s")])->andWhere([">=", "active_to", date("Y-m-d h:i:s")])->andWhere(["=", "product_id", $this->id])->one();
            if(!is_null($oPromotionDaily)){
                $this->isOnPromotion = true;
                $this->setPromotionLabels($oPromotionDaily, 'Promocja dnia');
                $this->recalculateDailyPromotion($oPromotionDaily);
            } else {
                // Reduction promotion
                $oPromotionReduction = PromotionReduction::find()->where(["OR",["=", "product_id", $this->id],["=", "category_id", $this->category_id]])->andWhere(["<=", "date_start", date("Y-m-d h:i:s")])->andWhere([">=", "date_end", date("Y-m-d h:i:s")])->one();
                if(!is_null($oPromotionReduction)){
                    $this->isOnPromotion = true;
                    $this->setPromotionLabels($oPromotionReduction, 'Promocja');
                    $this->recalculateReductionPromotion($oPromotionReduction);
                }
            }
        }
    }
    
    protected function recalculateReductionPromotion($oPromo){
        if(isset($this->aForcedValues["price_netto"])){
            $this->aForcedValues["old_price_netto"] = $this->aForcedValues["price_netto"];   
        } else {
            $this->price_netto;        
        }
        
        if(isset($this->aForcedValues["price_brutto"])){
            $this->aForcedValues["old_price_brutto"] = $this->aForcedValues["price_brutto"];
        } else {
            $this->price_brutto;
        }
        
        if($oPromo->amount_perc > 0){
            if(isset($this->aForcedValues["price_netto"])){
                $this->aForcedValues["price_netto"] -= $this->aForcedValues["price_netto"]*($oPromo->amount_perc/100);
            } else {
                $this->aForcedValues["price_netto"]  = $this->price_netto - $this->price_netto*($oPromo->amount_perc/100);
            }
            if(isset($this->aForcedValues["price_brutto"])){
                $this->aForcedValues["price_brutto"] -= $this->aForcedValues["price_brutto"]*($oPromo->amount_perc/100);
            } else {
                $this->aForcedValues["price_brutto"]  = $this->price_brutto - $this->price_brutto*($oPromo->amount_perc/100);
            }
        }
        
        if($oPromo->amount_sum > 0){
            if(isset($this->aForcedValues["price_netto"])){
                $this->aForcedValues["price_netto"] -= $oPromo->amount_sum;
            } else {
                $this->aForcedValues["price_netto"] = $this->price_netto - $oPromo->amount_sum;
            }
            if(isset($this->aForcedValues["price_brutto"])){
                $this->aForcedValues["price_brutto"] -= $oPromo->amount_sum;
            } else {
                $this->aForcedValues["price_brutto"]  = $this->price_brutto - $oPromo->amount_sum;
            }
        }
        
    }
    
    protected function recalculateConnectionProductPromotion($oPromo){
        if(isset($this->aForcedValues["price_netto"])){
            $this->aForcedValues["old_price_netto"] = $this->aForcedValues["price_netto"];   
        } else {
            $this->price_netto;        
        }
        
        if(isset($this->aForcedValues["price_brutto"])){
            $this->aForcedValues["old_price_brutto"] = $this->aForcedValues["price_brutto"];
        } else {
            $this->price_brutto;
        }
        
        if(isset($this->aForcedValues["price_netto"])){
            $this->aForcedValues["price_netto"]  = $this->aForcedValues["price_netto"] - $this->aForcedValues["price_netto"]*($oPromo->discount_amount/100);
        } else {
            $this->aForcedValues["price_netto"]  = $this->price_netto - $this->price_netto*($oPromo->discount_amount/100);
        }

        if(isset($this->aForcedValues["price_brutto"])){
            $this->aForcedValues["price_brutto"] = $this->aForcedValues["price_brutto"] - $this->aForcedValues["price_brutto"]*($oPromo->discount_amount/100);
        } else {
            $this->aForcedValues["price_brutto"] = $this->price_brutto - $this->price_brutto*($oPromo->discount_amount/100);
        }
    }
    
    protected function recalculateDailyPromotion($oPromotionDaily){
        if(isset($this->aForcedValues["price_netto"])){
            $this->aForcedValues["old_price_netto"] = $this->aForcedValues["price_netto"];   
        } else {
            $this->price_netto;        
        }
        
        if(isset($this->aForcedValues["price_brutto"])){
            $this->aForcedValues["old_price_brutto"] = $this->aForcedValues["price_brutto"];
        } else {
            $this->price_brutto;
        }
        
        switch($oPromotionDaily->promo_type){
            case "0":
                if(isset($this->aForcedValues["price_netto"])){
                    $this->aForcedValues["price_netto"]  = $this->aForcedValues["price_netto"] - $oPromotionDaily->amount;
                } else {
                    $this->aForcedValues["price_netto"]  = $this->price_netto - $oPromotionDaily->amount;
                }            
                
                if(isset($this->aForcedValues["price_brutto"])){
                    $this->aForcedValues["price_brutto"] = $this->aForcedValues["price_brutto"] - $oPromotionDaily->amount;
                } else {
                    $this->aForcedValues["price_brutto"] = $this->price_brutto - $oPromotionDaily->amount;
                }         
                
                break;
                
            case "1":
                if(isset($this->aForcedValues["price_netto"])){
                    $this->aForcedValues["price_netto"]  = $this->aForcedValues["price_netto"] - $this->aForcedValues["price_netto"]*($oPromotionDaily->amount/100);
                } else {
                    $this->aForcedValues["price_netto"]  = $this->price_netto - $this->price_netto*($oPromotionDaily->amount/100);
                }
                
                if(isset($this->aForcedValues["price_brutto"])){
                    $this->aForcedValues["price_brutto"] = $this->aForcedValues["price_brutto"] - $this->aForcedValues["price_brutto"]*($oPromotionDaily->amount/100);
                } else {
                    $this->aForcedValues["price_brutto"] = $this->price_brutto - $this->price_brutto*($oPromotionDaily->amount/100);
                }
                
                break;
        }
    }

    /**
     * @inheritdoc
     */
    public function __set($name, $value) {
        if(in_array($name, ["name", "short_description", "description"])){
            $this->translate(Yii::$app->session->get('backend_language'))->{$name} = $value;
        }
        
        parent::__set($name, $value);
    }
    
    public function __get($name) {
        if(in_array($name, ["name", "short_description", "description"])){
            if(strpos(Yii::$app->request->url, \Yii::$app->request->baseUrl) !== false){
                $translatedAttr = $this->translate(Yii::$app->session->get('backend_language'))->{$name};
            } else {
                $translatedAttr = $this->translate(Yii::$app->session->get('frontend_language'))->{$name};
            }

            if($translatedAttr){
                return $translatedAttr;
            }
        }

        if(isset($this->aForcedValues[$name])){
            return number_format($this->aForcedValues[$name], 2, '.', '');
        }
        
        if(in_array($name, ["price_netto"])){
            return number_format(parent::__get($name), 2, '.', '');
        }

        return parent::__get($name);
    }

}
