<?php

namespace common\modules\shop\models;

use Yii;
use common\modules\shop\models\Product;
use common\modules\shop\models\PriceList;

/**
 * This is the model class for table "xmod_shop_price_products".
 *
 * @property integer $id
 * @property integer $shop_price_id
 * @property integer $product_id
 * @property string $price_brutto
 * @property string $price_netto
 * @property string $old_price_brutto
 * @property string $old_price_netto
 * @property integer $vat
 *
 * @property PriceList $shopPrice
 * @property Product $product
 */
class PriceProduct extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_shop_price_products}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shop_price_id', 'product_id', 'vat'], 'integer'],
            [['price_brutto', 'price_netto', 'old_price_brutto', 'old_price_netto'], 'number'],
            [['shop_price_id'], 'exist', 'skipOnError' => true, 'targetClass' => PriceList::className(), 'targetAttribute' => ['shop_price_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_shop', 'ID'),
            'shop_price_id' => Yii::t('backend_module_shop', 'Shop Price ID'),
            'product_id' => Yii::t('backend_module_shop', 'Product ID'),
            'price_brutto' => Yii::t('backend_module_shop', 'Price Brutto'),
            'price_netto' => Yii::t('backend_module_shop', 'Price Netto'),
            'old_price_brutto' => Yii::t('backend_module_shop', 'Old Price Brutto'),
            'old_price_netto' => Yii::t('backend_module_shop', 'Old Price Netto'),
            'vat' => Yii::t('backend_module_shop', 'Vat'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopPrice()
    {
        return $this->hasOne(PriceList::className(), ['id' => 'shop_price_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
