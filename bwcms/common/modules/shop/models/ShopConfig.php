<?php

namespace common\modules\shop\models;

use Yii;

/**
 * This is the model class for table "xmod_shop_config".
 *
 * @property integer $id
 * @property string $client_notification_email
 * @property string $client_notification_text
 */
class ShopConfig extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_shop_config}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_notification_text', 'client_activation_text'], 'string'],
            [['client_notification_email', 'client_notification_topic', 'client_activation_email', 'client_activation_topic'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_shop', 'ID'),
            'client_notification_email' => Yii::t('backend_module_shop', 'Client Notification Email'),
            'client_notification_text' => Yii::t('backend_module_shop', 'Client Notification Text'),
        ];
    }
}
