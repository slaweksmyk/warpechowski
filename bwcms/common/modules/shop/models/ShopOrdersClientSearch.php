<?php

namespace common\modules\shop\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\shop\models\ShopOrdersClient;

/**
 * ShopOrdersClientSearch represents the model behind the search form about `common\modules\shop\models\ShopOrdersClient`.
 */
class ShopOrdersClientSearch extends ShopOrdersClient
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'order_id', 'user_id'], 'integer'],
            [['firstname', 'surname', 'street', 'city', 'postcode', 'email', 'phone', 'mobile', 'country', 'company', 'nip', 'nip_sulf', 'deliver_email', 'deliver_mobile', 'deliver_phone', 'deliver_street', 'deliver_city', 'deliver_postcode'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ShopOrdersClient::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'order_id' => $this->order_id,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'surname', $this->surname])
            ->andFilterWhere(['like', 'street', $this->street])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'postcode', $this->postcode])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'mobile', $this->mobile])
            ->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'company', $this->company])
            ->andFilterWhere(['like', 'nip', $this->nip])
            ->andFilterWhere(['like', 'nip_sulf', $this->nip_sulf])
            ->andFilterWhere(['like', 'deliver_email', $this->deliver_email])
            ->andFilterWhere(['like', 'deliver_mobile', $this->deliver_mobile])
            ->andFilterWhere(['like', 'deliver_phone', $this->deliver_phone])
            ->andFilterWhere(['like', 'deliver_street', $this->deliver_street])
            ->andFilterWhere(['like', 'deliver_city', $this->deliver_city])
            ->andFilterWhere(['like', 'deliver_postcode', $this->deliver_postcode]);

        return $dataProvider;
    }
}
