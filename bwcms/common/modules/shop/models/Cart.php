<?php

namespace common\modules\shop\models;

use Yii;
use common\modules\users\models\User;
use common\modules\shop\models\CartItem;
use common\modules\shop\models\Product;
use common\modules\shop\models\Stock;

/**
 * This is the model class for table "{{%xmod_shop_cart}}".
 *
 * @property int $id
 * @property int $user_id
 * @property string $session_id
 * @property string $create_date
 * @property string $update_date
 *
 * @property User $user
 * @property CartItem[] $xmodShopCartItems
 */
class Cart extends \yii\db\ActiveRecord
{
    public $aErrors = [];
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%xmod_shop_cart}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['user_id', 'is_active', 'is_reminded'], 'integer'],
            [['name'], 'string'],
            [['create_date', 'update_date'], 'safe'],
            [['session_id'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Nazwa',
            'is_active' => 'Czy aktywny',
            'user_id' => 'Użytkownik',
            'session_id' => 'Session ID',
            'create_date' => 'Data utworzenia',
            'update_date' => 'Data aktualizacji',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCartItems()
    {
        return $this->hasMany(CartItem::className(), ['cart_id' => 'id']);
    }
    
    /**
     * Get sum of all items (brutto)
     */
    public function getSumBrutto(){
        $fCost = 0.00;
        $sCurrency = "zł";
        foreach($this->getCartItems()->all() as $oCartItem){
            $fCost += $oCartItem->getItem()->getPriceBrutto()*$oCartItem->quantity;
            $sCurrency = $oCartItem->getItem()->currency;
        }

        return number_format((float)$fCost, 2, '.', '').$sCurrency;
    }
    
    /**
     * Get sum of all items (netto)
     */
    public function getSumNetto(){
        $fCost = 0.00;
        $sCurrency = "zł";
        foreach($this->getCartItems()->all() as $oCartItem){
            $fCost += $oCartItem->getItem()->getPriceNetto()*$oCartItem->quantity;
            $sCurrency = $oCartItem->getItem()->currency;
        }

        return number_format((float)$fCost, 2, '.', '').$sCurrency;
    }
    
    /**
     * Adds item to cart
     * @param int $iItemID
     * @param float $fQuantity
     * @return bool
     */
    public function addProduct(int $iItemID, float $fQuantity) : bool
    {
        $oProduct = Product::findOne($iItemID);
        if(is_null($oProduct)){ $this->aErrors[] = ["oItemID" => $iItemID, 'sMessage' => "Nie ma takiego produktu."]; return false; }

        if($this->hasProduct($iItemID)){
            $oCartItem = CartItem::find()->where(["=", "cart_id", $this->id])->andWhere(["=", "item_id", $iItemID])->one();
            $oCartItem->quantity += $fQuantity;
            
            if($this->isInStock($iItemID, $oCartItem->quantity)){
                $oCartItem->save();
            } else { return false; }
        } else {
            if($this->isInStock($iItemID, $fQuantity)){
                $oCartItem = new CartItem();
                $oCartItem->cart_id = $this->id;
                $oCartItem->item_id = $iItemID;
                $oCartItem->quantity = $fQuantity;
                $oCartItem->save();
            } else { return false; }
        }

        $this->update_date = date("Y-m-d H:i:s");
        $this->save();
        
        return true;
    }
    
    public function removeProduct(int $iItemID)
    {
        $oProduct = Product::findOne($iItemID);
        if(is_null($oProduct)){ $this->aErrors[] = ["oItemID" => $iItemID, 'sMessage' => "Nie ma takiego produktu."]; return false; }
        
        if($this->hasProduct($iItemID)){
            $oCartItem = CartItem::find()->where(["=", "cart_id", $this->id])->andWhere(["=", "item_id", $iItemID])->one();
            $oCartItem->delete();
        } else { $this->aErrors[] = ["oItemID" => $iItemID, 'sMessage' => "Nie ma takiego produktu w koszyku."]; return false; }
        
        $this->update_date = date("Y-m-d H:i:s");
        $this->save();
        
        return true;
    }
    
    /**
     * Substract product amount
     * @param int $iItemID
     * @param float $fQuantity
     * @return bool
     */
    public function substractProduct(int $iItemID, float $fQuantity) : bool
    {
        $oProduct = Product::findOne($iItemID);
        if(is_null($oProduct)){ $this->aErrors[] = ["oItemID" => $iItemID, 'sMessage' => "Nie ma takiego produktu."]; return false; }
        
        if($this->hasProduct($iItemID)){
            $oCartItem = CartItem::find()->where(["=", "cart_id", $this->id])->andWhere(["=", "item_id", $iItemID])->one();
            $oCartItem->quantity -= $fQuantity;
            
            if($oCartItem->quantity > 0){
                $oCartItem->save();
            } else { return $this->removeProduct($iItemID); }
        } else { $this->aErrors[] = ["oItemID" => $iItemID, 'sMessage' => "Nie ma takiego produktu w koszyku."]; return false; }

        $this->update_date = date("Y-m-d H:i:s");
        $this->save();
        
        return true;
    }

    /**
     * Check if product have requested stock amount
     * @param int $iItemID
     * @param float $fQuantity
     * @return bool
     */
    private function isInStock(int $iItemID, float $fQuantity) : bool
    {
        $oStock = Stock::find()->where(["=", "product_id", $iItemID])->andWhere([">=", "amount", $fQuantity])->one();
        if(is_null($oStock)){ $this->aErrors[] = ["oItemID" => $iItemID, 'sMessage' => "Brak wystarczającej ilości produktu w magazynie"]; return false; }
        
        return true;
    }

    /**
     * Check if product is in cart
     * @param int $iItemID
     * @return boolean
     */
    private function hasProduct(int $iItemID) : bool
    {
        $oCartItem = CartItem::find()->where(["=", "cart_id", $this->id])->andWhere(["=", "item_id", $iItemID])->one();
        return !is_null($oCartItem);
    }
    
}
