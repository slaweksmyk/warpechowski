<?php

namespace common\modules\shop\models;

use Yii;
use common\modules\pages\models\PagesSite;
use common\modules\shop\models\Product;
use common\modules\files\models\File;

use yii\helpers\BaseUrl;

/**
 * This is the model class for table "{{%xmod_shop_categories}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $parent_id
 * @property integer $icon_id
 * @property string $short_description
 * @property string $description
 *
 * @property Files $icon
 * @property Category $parent
 * @property Category[] $categories
 * @property XmodShopProductsList[] $xmodShopProductsLists
 */
class Category extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_shop_categories}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'icon_id', 'is_active', 'extend_page_id', 'sort'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
            [['short_description', 'description'], 'string'],
            [['name', 'slug'], 'string', 'max' => 255],
            [['extend_page_id'], 'exist', 'skipOnError' => true, 'targetClass' => PagesSite::className(), 'targetAttribute' => ['extend_page_id' => 'id']],
            [['icon_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['icon_id' => 'id']],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_shop', 'ID'),
            'name' => Yii::t('backend_module_shop', 'Name'),
            'slug' => Yii::t('backend_module_shop', 'Slug'),
            'parent_id' => Yii::t('backend_module_shop', 'Parent ID'),
            'icon_id' => Yii::t('backend_module_shop', 'Icon ID'),
            'short_description' => Yii::t('backend_module_shop', 'Short Description'),
            'description' => Yii::t('backend_module_shop', 'Description'),
            'sort' => "Kolejność"
        ];
    }

    public function getAbsoluteUrl(){
        return trim(BaseUrl::home(true), "/").$this->getUrl();
    }
   
    public function getUrl(){
        return $this->getExtendUrl()."".$this->slug;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExtendPage()
    {
        return $this->hasOne(PagesSite::className(), ['id' => 'extend_page_id']);
    }
    
    public function getExtendUrl(){
        if(!is_null($this->extend_page_id)){
            $sUrl = "";
            if(Yii::$app->params['oConfig']->default_language != Yii::$app->session->get('frontend_language')){
                $sLang = strtolower(current(explode("-", Yii::$app->session->get('frontend_language'))));
                $sUrl  = "/{$sLang}/{$this->getExtendPage()->one()->url}/";
            } else { $sUrl = "/{$this->getExtendPage()->one()->url}/"; }

            return $sUrl;
        }
        
        return "/";
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getThumbnail($width = 2000, $height = 2000, $type = "crop", $effect = null, $quality = 80) 
    { 
        return File::getThumbnail($this->icon_id, $width, $height, $type, $effect, $quality);
    } 

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Category::className(), ['id' => 'parent_id'])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['parent_id' => 'id'])->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['category_id' => 'id'])->all();
    }
    
}
