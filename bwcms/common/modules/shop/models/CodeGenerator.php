<?php

namespace common\modules\shop\models;

class CodeGenerator extends \yii\base\Model
{
    public $amount = 0;
    public $promotion_id, $promotion_subtype, $restriction_product, $restriction_category;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount', 'promotion_id'], 'integer'],
            [['promotion_subtype'], 'string'],
            [['restriction_product', 'restriction_category'], 'safe'],
        ];
    }
}