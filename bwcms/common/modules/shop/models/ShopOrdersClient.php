<?php

namespace common\modules\shop\models;

use Yii;
use common\modules\users\models\User;
use common\modules\shop\models\ShopOrders;

/**
 * This is the model class for table "xmod_shop_orders_client".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $user_id
 * @property string $firstname
 * @property string $surname
 * @property string $street
 * @property string $city
 * @property string $postcode
 * @property string $email
 * @property string $phone
 *
 * @property ShopOrders $order
 * @property User $user
 */
class ShopOrdersClient extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_shop_orders_client}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'surname', 'city', 'postcode', 'email', 'mobile', 'country', 'deliver_email', 'deliver_mobile', 'deliver_city', 'deliver_postcode'], 'required'],
            [['order_id', 'user_id'], 'integer'],
            [['firstname', 'surname', 'street', 'city', 'postcode', 'email', 'phone', 'mobile', 'country', 'nip', 'nip_sulf', 'deliver_email', 'deliver_mobile', 'deliver_phone', 'deliver_street', 'deliver_city', 'deliver_postcode', 'deliver_street_no', 'deliver_street_loc', 'deliver_country'], 'safe'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => ShopOrders::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_shop', 'ID'),
            'order_id' => Yii::t('backend_module_shop', 'Order ID'),
            'user_id' => Yii::t('backend_module_shop', 'User ID'),
            'firstname' => Yii::t('backend_module_shop', 'Firstname'),
            'surname' => Yii::t('backend_module_shop', 'Surname'),
            'street' => Yii::t('backend_module_shop', 'Street'),
            'city' => Yii::t('backend_module_shop', 'City'),
            'postcode' => Yii::t('backend_module_shop', 'Postcode'),
            'email' => Yii::t('backend_module_shop', 'Email'),
            'phone' => Yii::t('backend_module_shop', 'Phone'),
            'mobile' => Yii::t('backend_module_shop', 'Mobile'),
            'country' => Yii::t('backend_module_shop', 'Country'),
            'nip' => Yii::t('backend_module_shop', 'Nip'),
            'deliver_email' => Yii::t('backend_module_shop', 'deliver_email'),
            'deliver_mobile' => Yii::t('backend_module_shop', 'deliver_mobile'),
            'deliver_phone' => Yii::t('backend_module_shop', 'deliver_phone'),
            'deliver_street' => Yii::t('backend_module_shop', 'deliver_street'),
            'deliver_city' => Yii::t('backend_module_shop', 'deliver_city'),
            'deliver_postcode' => Yii::t('backend_module_shop', 'deliver_postcode'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(ShopOrders::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
