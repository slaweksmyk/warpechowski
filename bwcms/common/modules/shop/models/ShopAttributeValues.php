<?php

namespace common\modules\shop\models;

use Yii;
use common\modules\shop\models\Attribute;


/**
 * This is the model class for table "xmod_shop_attributes_values".
 *
 * @property integer $id
 * @property integer $attribute_id
 * @property string $name
 * @property string $value
 *
 * @property XmodShopAttributes $attribute
 */
class ShopAttributeValues extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_shop_attributes_values}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attribute_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['attribute_id'], 'exist', 'skipOnError' => true, 'targetClass' => Attribute::className(), 'targetAttribute' => ['attribute_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_shop', 'ID'),
            'attribute_id' => Yii::t('backend_module_shop', 'Attribute ID'),
            'name' => Yii::t('backend_module_shop', 'Name')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAttribute()
    {
        return $this->hasOne(Attribute::className(), ['id' => 'attribute_id']);
    }
}
