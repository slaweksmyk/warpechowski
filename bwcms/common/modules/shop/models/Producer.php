<?php

namespace common\modules\shop\models;

use Yii;
use common\modules\files\models\File;

/**
 * This is the model class for table "xmod_shop_producers_list".
 *
 * @property integer $id
 * @property string $name
 * @property integer $thumbnail_id
 *
 * @property Files $thumbnail
 */
class Producer extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_shop_producers_list}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['thumbnail_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['thumbnail_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['thumbnail_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_shop', 'ID'),
            'name' => Yii::t('backend_module_shop', 'Name'),
            'thumbnail_id' => Yii::t('backend_module_shop', 'Thumbnail ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getThumbnail($width = 2000, $height = 2000, $type = "crop", $effect = null, $quality = 80) 
    { 
        return File::getThumbnail($this->thumbnail_id, $width, $height, $type, $effect, $quality);
    } 
}
