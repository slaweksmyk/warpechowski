<?php

namespace common\modules\shop\models;

use Yii;
use common\modules\promotion\models\PromotionsList;

/**
 * This is the model class for table "xmod_shop_discount_codes".
 *
 * @property integer $id
 * @property integer $promotion_id
 * @property string $code
 * @property string $value
 * @property integer $type
 * @property integer $restriction_type
 * @property integer $is_one_time
 * @property integer $is_used
 * @property string $category_hash
 * @property string $product_hash
 * @property string $active_from
 * @property string $active_to
 *
 * @property XmodLoyaltyReferralsConfig[] $xmodLoyaltyReferralsConfigs
 * @property PromotionsList $promotion
 * @property XmodShopOrdersItems[] $xmodShopOrdersItems
 * @property PromotionBday[] $xmodShopPromotionBdays
 * @property PromotionBday[] $xmodShopPromotionBdays0
 * @property PromotionBdayArchive[] $xmodShopPromotionBdayArchives
 * @property PromotionConfig[] $xmodShopPromotionConfigs
 * @property PromotionConfig[] $xmodShopPromotionConfigs0
 * @property PromotionConfig[] $xmodShopPromotionConfigs1
 * @property PromotionConfig[] $xmodShopPromotionConfigs2
 */
class ShopDiscountCodes extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_shop_discount_codes}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['promotion_id', 'type', 'restriction_type', 'is_one_time', 'is_used', 'is_once_per_user'], 'integer'],
            [['category_hash', 'product_hash', 'promotion_subtype'], 'string'],
            [['active_from', 'active_to'], 'safe'],
            [['code', 'value'], 'string', 'max' => 255],
            [['promotion_id'], 'exist', 'skipOnError' => true, 'targetClass' => PromotionsList::className(), 'targetAttribute' => ['promotion_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_shop', 'ID'),
            'promotion_id' => Yii::t('backend_module_shop', 'Promotion ID'),
            'code' => Yii::t('backend_module_shop', 'Code'),
            'value' => Yii::t('backend_module_shop', 'Value'),
            'type' => Yii::t('backend_module_shop', 'Type'),
            'restriction_type' => Yii::t('backend_module_shop', 'Restriction Type'),
            'is_one_time' => Yii::t('backend_module_shop', 'Is One Time'),
            'is_used' => Yii::t('backend_module_shop', 'Is Used'),
            'category_hash' => Yii::t('backend_module_shop', 'Category Hash'),
            'product_hash' => Yii::t('backend_module_shop', 'Product Hash'),
            'active_from' => Yii::t('backend_module_shop', 'Active From'),
            'active_to' => Yii::t('backend_module_shop', 'Active To'),
            'is_once_per_user' => 'Jednorazowy na użytkownika'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromotion()
    {
        return $this->hasOne(PromotionsList::className(), ['id' => 'promotion_id']);
    }
}
