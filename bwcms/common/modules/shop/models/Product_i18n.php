<?php

namespace common\modules\shop\models;

use Yii;

/**
 * This is the model class for table "{{%xmod_shop_products_list_i18n}}".
 *
 * @property integer $id
 * @property string $language
 * @property string $name
 * @property string $short_description
 * @property string $description
 */
class Product_i18n extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_shop_products_list_i18n}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
            [['id', 'xmod_shop_products_list_id'], 'integer'],
            [['short_description', 'description'], 'string'],
            [['language', 'name'], 'string', 'max' => 1024],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'language' => Yii::t('backend', 'Language'),
            'name' => Yii::t('backend', 'Name'),
            'short_description' => Yii::t('backend', 'Short description'),
            'description' => Yii::t('backend', 'Description'),
        ];
    }
}
