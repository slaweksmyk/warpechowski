<?php

namespace common\modules\shop\models;

use Yii;
use common\modules\shop\models\Product;
use common\modules\shop\models\ShopStocksStates;

/**
 * This is the model class for table "{{%xmod_shop_stocks}}".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $type
 * @property integer $amount
 *
 * @property XmodShopProductsList $product
 */
class Stock extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_shop_stocks}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'stock_status_id', 'amount'], 'integer'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['stock_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => ShopStocksStates::className(), 'targetAttribute' => ['stock_status_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_shop', 'ID'),
            'product_id' => Yii::t('backend_module_shop', 'Product ID'),
            'type' => Yii::t('backend_module_shop', 'Type'),
            'amount' => Yii::t('backend_module_shop', 'Amount'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStockStatus()
    {
        return $this->hasOne(ShopStocksStates::className(), ['id' => 'stock_status_id']);
    }
    
    public function substractAmount($iAmount){
        if($this->amount >= $iAmount){
            $this->amount -= $iAmount;

            $oStockState = ShopStocksStates::find()->where(["<=", "amount_from", $this->amount])->andWhere([">=", "amount_to", $this->amount])->one();

            $this->stock_status_id = $oStockState->id;
            $this->save();
        }
    }
    
}
