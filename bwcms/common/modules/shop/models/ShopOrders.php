<?php

namespace common\modules\shop\models;

use Yii;
use common\modules\shop\models\ShopPayment;
use common\modules\shop\models\ShopDelivers;
use common\modules\shop\models\ShopOrdersItems;
use common\modules\shop\models\ShopOrdersClient;


/**
 * This is the model class for table "xmod_shop_orders".
 *
 * @property integer $id
 * @property integer $deliver_id
 * @property integer $status
 * @property string $order_fv
 * @property string $order_date
 *
 * @property ShopDelivers $deliver
 * @property ShopOrdersClient[] $xmodShopOrdersClients
 * @property ShopOrdersItems[] $xmodShopOrdersItems
 */
class ShopOrders extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_shop_orders}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['deliver_id', 'status'], 'integer'],
            [['order_date'], 'safe'],
            [['order_fv', 'company'], 'string', 'max' => 255],
            [['deliver_id'], 'exist', 'skipOnError' => true, 'targetClass' => ShopDelivers::className(), 'targetAttribute' => ['deliver_id' => 'id']],
            [['payment_id'], 'exist', 'skipOnError' => true, 'targetClass' => ShopPayment::className(), 'targetAttribute' => ['payment_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_shop', 'ID'),
            'deliver_id' => Yii::t('backend_module_shop', 'Deliver ID'),
            'status' => Yii::t('backend_module_shop', 'Status'),
            'order_fv' => Yii::t('backend_module_shop', 'Order Fv'),
            'order_date' => Yii::t('backend_module_shop', 'Order Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliver()
    {
        return $this->hasOne(ShopDelivers::className(), ['id' => 'deliver_id']);
    }
    
    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getPayment() 
    { 
        return $this->hasOne(ShopPayment::className(), ['id' => 'payment_id']);
    } 

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopOrdersClients()
    {
        return $this->hasMany(ShopOrdersClient::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopOrdersItems()
    {
        return $this->hasMany(ShopOrdersItems::className(), ['order_id' => 'id']);
    }
    
    public function getFullPrice(){
        $aDiscounts = Yii::$app->session->get('discounts', []);
        
        $fFullCost = 0.0;
        foreach($this->getShopOrdersItems()->all() as $oOrderItem){
            $oProduct = Product::find()->where(["=", "id", $oOrderItem->product_id])->one();

            if(isset($aDiscounts[0][$oOrderItem->product_id])){
                $oDiscountData = $aDiscounts[0][$oOrderItem->product_id];

                switch($oDiscountData["oDiscount"]->type){
                    case 0:
                        $fPriceNetto  = $this->recalculatePriceAmount($oDiscountData["oProduct"]->price_netto, $oDiscountData["oDiscount"]);
                        $fPriceBrutto = $this->recalculatePriceAmount($oDiscountData["oProduct"]->price_brutto, $oDiscountData["oDiscount"]);
                        break;

                    case 1:
                        $fPriceNetto  = $this->recalculatePricePercent($oDiscountData["oProduct"]->price_netto, $oDiscountData["oDiscount"]);
                        $fPriceBrutto = $this->recalculatePricePercent($oDiscountData["oProduct"]->price_brutto, $oDiscountData["oDiscount"]);
                        break;
                }

                $fFullPriceNetto = $fPriceNetto * $oOrderItem->amount;
                $fFullPriceBrutto = $fPriceBrutto * $oOrderItem->amount;

                $fFullCost += $fFullPriceBrutto;
            } else {
                $fFullPriceNetto = $oProduct->price_netto * $oOrderItem->amount;
                $fFullPriceBrutto = $oProduct->price_brutto * $oOrderItem->amount;

                $fFullCost += $fFullPriceBrutto;
            }
        }
        
        return $fFullCost;
    }
    
}
