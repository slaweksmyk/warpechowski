<?php

namespace common\modules\shop\models;

use Yii;
use common\modules\shop\models\Product;
use common\modules\shop\models\ShopAttributeValues;


/**
 * This is the model class for table "xmod_shop_attributes".
 *
 * @property integer $id
 * @property string $name
 * @property double $price
 * @property integer $type
 *
 * @property XmodShopProductsAttributes[] $xmodShopProductsAttributes
 */
class Attribute extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_shop_attributes}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_shop', 'ID'),
            'name' => Yii::t('backend_module_shop', 'Name')
        ];
    }
    
    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getValues() 
    { 
        return $this->hasMany(ShopAttributeValues::className(), ['attribute_id' => 'id']);
    } 

    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['attribute_id' => 'id']);
    }
}
