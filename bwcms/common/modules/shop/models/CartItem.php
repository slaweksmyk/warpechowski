<?php

namespace common\modules\shop\models;

use Yii;
use common\modules\shop\models\Cart;
use common\modules\shop\models\Product;

/**
 * This is the model class for table "{{%xmod_shop_cart_items}}".
 *
 * @property int $id
 * @property int $cart_id
 * @property int $item_id
 * @property int $quantity
 *
 * @property Cart $cart
 * @property Product $item
 */
class CartItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%xmod_shop_cart_items}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cart_id', 'item_id'], 'integer'],
            [['quantity'], 'safe'],
            [['quantity'], 'required'],
            [['cart_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cart::className(), 'targetAttribute' => ['cart_id' => 'id']],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['item_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cart_id' => 'Koszyk',
            'item_id' => 'Przedmiot',
            'quantity' => 'Ilość',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCart()
    {
        return $this->hasOne(Cart::className(), ['id' => 'cart_id'])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Product::className(), ['id' => 'item_id'])->one();
    }
}
