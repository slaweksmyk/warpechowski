<?php

namespace common\modules\shop\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\shop\models\ShopPayment;

/**
 * ShopPaymentSearch represents the model behind the search form about `common\modules\shop\models\ShopPayment`.
 */
class ShopPaymentSearch extends ShopPayment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_local', 'is_active'], 'integer'],
            [['name', 'api', 'thankyou'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ShopPayment::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'is_local' => $this->is_local,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'api', $this->api])
            ->andFilterWhere(['like', 'thankyou', $this->thankyou]);

        return $dataProvider;
    }
}
