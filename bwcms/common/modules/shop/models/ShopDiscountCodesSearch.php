<?php

namespace common\modules\shop\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\shop\models\ShopDiscountCodes;

/**
 * ShopDiscountCodesSearch represents the model behind the search form about `common\modules\shop\models\ShopDiscountCodes`.
 */
class ShopDiscountCodesSearch extends ShopDiscountCodes
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'restriction_type', 'is_one_time', 'is_used'], 'integer'],
            [['code', 'value', 'category_hash', 'product_hash', 'active_from', 'active_to', 'promotion_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ShopDiscountCodes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'value' => $this->value,
            'restriction_type' => $this->restriction_type,
            'active_from' => $this->active_from,
            'active_to' => $this->active_to,
            'is_one_time' => $this->is_one_time,
            'is_used' => $this->is_used,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'category_hash', $this->category_hash])
            ->andFilterWhere(['like', 'product_hash', $this->product_hash]);

        return $dataProvider;
    }
}
