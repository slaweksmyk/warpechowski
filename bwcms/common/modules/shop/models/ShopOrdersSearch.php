<?php

namespace common\modules\shop\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\shop\models\ShopOrders;
use common\modules\shop\models\ShopOrdersClient;

/**
 * ShopOrdersSearch represents the model behind the search form about `common\modules\shop\models\ShopOrders`.
 */
class ShopOrdersSearch extends ShopOrders
{
    public $company;
    public $firstname;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'deliver_id', 'payment_id', 'status'], 'integer'],
            [['company', 'firstname'], 'safe'],
            [['order_fv', 'order_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ShopOrders::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        
        $oClientQuery = ShopOrdersClient::find()->select("order_id as id")->where(["OR", ["LIKE", "firstname", $this->firstname], ["LIKE", "surname", $this->firstname]]);
        $query->andWhere(["IN", "id", $oClientQuery]);
        
        $oClientQuery = ShopOrdersClient::find()->select("order_id as id")->where(["LIKE", "company", $this->company]);
        $query->andWhere(["IN", "id", $oClientQuery]);
        
        $query->andWhere(["LIKE", "order_date", $this->order_date]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'deliver_id' => $this->deliver_id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'order_fv', $this->order_fv]);

        return $dataProvider;
    }
}
