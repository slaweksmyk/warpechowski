<?php

namespace common\modules\shop\models;

use Yii;
use common\modules\shop\models\PriceProduct;

/**
 * This is the model class for table "xmod_shop_price_lists".
 *
 * @property integer $id
 * @property string $name
 * @property string $short_name
 * @property string $short_currency
 * @property string $symbol_currency
 *
 * @property PriceProduct[] $xmodShopPriceProducts
 */
class PriceList extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_shop_price_lists}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'short_name', 'short_currency', 'symbol_currency'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_shop', 'ID'),
            'name' => Yii::t('backend_module_shop', 'Name'),
            'short_name' => Yii::t('backend_module_shop', 'Short Name'),
            'short_currency' => Yii::t('backend_module_shop', 'Short Currency'),
            'symbol_currency' => Yii::t('backend_module_shop', 'Symbol Currency'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceProduct()
    {
        return $this->hasMany(PriceProduct::className(), ['shop_price_id' => 'id']);
    }
}
