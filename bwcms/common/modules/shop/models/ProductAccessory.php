<?php

namespace common\modules\shop\models;

use Yii;
use common\modules\shop\models\Product;

/**
 * This is the model class for table "{{%xmod_shop_products_accessories}}".
 *
 * @property int $id
 * @property int $product_id
 * @property int $accessory_id
 *
 * @property Product $product
 * @property Product $accessory
 */
class ProductAccessory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%xmod_shop_products_accessories}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'accessory_id'], 'integer'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['accessory_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['accessory_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Produkt',
            'accessory_id' => 'Akcesorium',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccessory()
    {
        return $this->hasOne(Product::className(), ['id' => 'accessory_id']);
    }
}
