<?php

namespace common\modules\shop\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\shop\models\ShopStocksStates;

/**
 * ShopStocksStatesSearch represents the model behind the search form about `common\modules\shop\models\ShopStocksStates`.
 */
class ShopStocksStatesSearch extends ShopStocksStates
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'amount_from', 'amount_to'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ShopStocksStates::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'amount_from' => $this->amount_from,
            'amount_to' => $this->amount_to,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
