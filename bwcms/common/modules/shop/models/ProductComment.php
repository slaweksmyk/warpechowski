<?php

namespace common\modules\shop\models;

use Yii;
use common\modules\users\models\User;
use common\modules\shop\models\Product;

/**
 * This is the model class for table "{{%xmod_shop_products_comments}}".
 *
 * @property int $id
 * @property int $product_id
 * @property int $parent_id
 * @property int $user_id
 * @property int $status_id
 * @property string $date_created
 * @property string $author
 * @property string $score
 * @property string $text
 *
 * @property Product $product
 * @property User $user
 * @property ProductComment $parent
 * @property ProductComment[] $productComments
 */
class ProductComment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%xmod_shop_products_comments}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'parent_id', 'user_id', 'status_id'], 'integer'],
            [['date_created'], 'safe'],
            [['score'], 'number'],
            [['text'], 'string'],
            [['author'], 'string', 'max' => 255],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductComment::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Produkt',
            'parent_id' => 'Rodzic',
            'user_id' => 'Użytkownik',
            'status_id' => 'Status',
            'date_created' => 'Data',
            'author' => 'Autor',
            'score' => 'Ocena',
            'text' => 'Treść',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(ProductComment::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductComments()
    {
        return $this->hasMany(ProductComment::className(), ['parent_id' => 'id']);
    }
}
