<?php

namespace common\modules\shop\models;

use Yii;
use common\modules\files\models\File;
use common\modules\shop\models\ShopOrders;

/**
 * This is the model class for table "xmod_shop_payment".
 *
 * @property integer $id
 * @property string $name
 * @property string $api
 * @property integer $is_local
 * @property integer $is_active
 * @property string $thankyou
 *
 * @property ShopOrders[] $xmodShopOrders
 */
class ShopPayment extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_shop_payment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_local', 'is_active'], 'integer'],
            [['name', 'api', 'thankyou'], 'string', 'max' => 255],
            [['config'], 'safe'],
            [['thumbnail_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['thumbnail_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_shop', 'ID'),
            'name' => Yii::t('backend_module_shop', 'Name'),
            'api' => Yii::t('backend_module_shop', 'Api'),
            'is_local' => Yii::t('backend_module_shop', 'Is Local'),
            'is_active' => Yii::t('backend_module_shop', 'Is Active'),
            'thankyou' => Yii::t('backend_module_shop', 'Thankyou'),
            'thumbnail_id' => 'Ikona'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopOrders()
    {
        return $this->hasMany(ShopOrders::className(), ['payment_id' => 'id']);
    }
    
    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getThumbnail($width = 2000, $height = 2000, $type = "crop", $effect = null, $quality = 80) 
    { 
        return File::getThumbnail($this->thumbnail_id, $width, $height, $type, $effect, $quality);
    } 
    
}
