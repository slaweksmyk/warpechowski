<?php

namespace common\modules\shop\models;

use Yii;

/**
 * This is the model class for table "xmod_shop_stocks_states".
 *
 * @property integer $id
 * @property string $name
 * @property integer $amount_from
 * @property integer $amount_to
 */
class ShopStocksStates extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_shop_stocks_states}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['color'], 'string'],
            [['amount_from', 'amount_to'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_shop_stock', 'ID'),
            'name' => Yii::t('backend_module_shop_stock', 'Name'),
            'amount_from' => Yii::t('backend_module_shop_stock', 'Amount From'),
            'amount_to' => Yii::t('backend_module_shop_stock', 'Amount To'),
        ];
    }
}
