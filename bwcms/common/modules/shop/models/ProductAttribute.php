<?php

namespace common\modules\shop\models;

use Yii;
use common\modules\shop\models\Product;
use common\modules\shop\models\Attribute;
use common\modules\shop\models\ShopAttributeValues;

/**
 * This is the model class for table "xmod_shop_products_attributes".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $attribute_id
 *
 * @property XmodShopAttributes $attribute
 * @property XmodShopProductsList $product
 */
class ProductAttribute extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_shop_products_attributes}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'attribute_id'], 'integer'],
            [['attribute_id'], 'exist', 'skipOnError' => true, 'targetClass' => Attribute::className(), 'targetAttribute' => ['attribute_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['value_id'], 'exist', 'skipOnError' => true, 'targetClass' => ShopAttributeValues::className(), 'targetAttribute' => ['value_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_shop', 'ID'),
            'product_id' => Yii::t('backend_module_shop', 'Product ID'),
            'attribute_id' => Yii::t('backend_module_shop', 'Attribute ID'),
            'value_id' => Yii::t('backend_module_shop', 'Value ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAttribute()
    {
        return $this->hasOne(Attribute::className(), ['id' => 'attribute_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
    
    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getValue() 
    { 
        return $this->hasOne(ShopAttributeValues::className(), ['id' => 'value_id']);
    } 
    
}
