<?php

namespace common\modules\shop\admin;

use Yii;
/**
 * Shop module definition class
 */
class Shop extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\shop\admin\controllers';
    public $defaultRoute = 'shop';

    /**
     * @inheritdoc
     */
    public function init()
    {
        $aUrlParts = explode("/", str_replace(\Yii::$app->request->baseUrl, "", Yii::$app->getRequest()->url));
        $this->defaultRoute = current($aUrlParts);

        parent::init();
    }
}
