<?php

namespace common\modules\shop\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\data\ActiveDataProvider;

use common\modules\shop\models\Attribute; 
use common\modules\shop\models\AttributeSearch;

use common\modules\shop\models\ShopAttributeValues; 
use common\modules\shop\models\ShopAttributeValuesSearch;

use common\modules\shop\models\ProductAttribute; 
use common\modules\shop\models\Product;
use common\modules\logs\models\Log;

/**
 * ShopProductController implements the CRUD actions for Product model.
 */
class ShopAttributesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AttributeSearch(); 
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams); 

        return $this->render('index', [ 
            'searchModel' => $searchModel, 
            'dataProvider' => $dataProvider, 
        ]); 
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Attribute();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = Attribute::findOne(["=", "id", $id]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->save();

            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = Attribute::findOne(["id" => $id]);
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionValues($id)
    {
        $searchModel = new ShopAttributeValuesSearch(); 
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams); 

        if(!is_null($id)){
            $dataProvider->query->andWhere(["attribute_id" => $id]);
        }
        
        return $this->render('values_index', [ 
            'searchModel' => $searchModel, 
            'dataProvider' => $dataProvider, 
        ]); 
    }
    
    public function actionValueProducts($id)
    {
        $oProductAttributeRowset = ProductAttribute::find()->where(["=", "value_id", $id])->all();
        
        $aProductIDs = [];
        foreach($oProductAttributeRowset as $oProductAttribute){
            $aProductIDs[] = $oProductAttribute->product_id;
        }
        
        $oProductRowset = Product::find()->where(["IN", "id", $aProductIDs]);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $oProductRowset,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        
        $oShopAttributeValues = ShopAttributeValues::find()->where(["=", "id", $id])->one();
        
        return $this->render('values_products', [ 
            'oShopAttributeValues' => $oShopAttributeValues,
            'dataProvider' => $dataProvider
        ]); 
    }
    
    public function actionValueUpdate($id)
    {
        $model = ShopAttributeValues::findOne(["=", "id", $id]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->save();

            return $this->redirect(['value-update', 'id' => $model->id]);
        } else {
            return $this->render('values_update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionValueCreate($id)
    {
        $model = new ShopAttributeValues();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['value-update', 'id' => $model->id]);
        } else {
            return $this->render('values_create', [
                'model' => $model,
                'attributeID' => $id
            ]);
        }
    }
    
    public function actionValueDelete($id)
    {
        $model = ShopAttributeValues::find()->where(["=", "id", $id])->one();
        $model->delete();
        
        return $this->redirect(['values', "id" => $model->attribute_id]);
    }
    
}
