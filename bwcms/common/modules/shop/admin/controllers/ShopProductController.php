<?php

namespace common\modules\shop\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\modules\shop\models\Category;
use common\modules\shop\models\Product;
use common\modules\shop\models\ProductSearch;
use common\modules\shop\models\ProductAttribute;
use common\modules\shop\models\ProductRelated;
use common\modules\shop\models\ProductAccessory;
use common\modules\shop\models\ProductAccessorySearch;
use common\modules\shop\models\Stock;
use common\modules\shop\models\PriceProduct;
use common\modules\shop\models\ProductComment;
use common\modules\shop\models\ProductCommentSearch;

/**
 * ShopProductController implements the CRUD actions for Product model.
 */
class ShopProductController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->get('per-page')) {
            $dataProvider->setPagination(['pageSize' => Yii::$app->request->get('per-page')]);
        }

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionExport()
    {
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Content-Description: File Transfer');
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename=export.csv;');
        header('Content-Transfer-Encoding: binary');

        $fp = fopen('php://output', 'w');
        fputs($fp, $bom = ( chr(0xEF) . chr(0xBB) . chr(0xBF) ));

        if ($fp) {
            $aHeaders = [];
            $oProduct = Product::find()->one();

            if ($oProduct) {
                $aAttributes = $oProduct->getAttributes();

                foreach ($aAttributes as $sKey => $sValue) {
                    $aHeaders[] = $sKey;
                }
            }

            fputcsv($fp, $aHeaders, ";");

            $oProductRowset = Product::find()->all();
            foreach ($oProductRowset as $oProduct) {
                $aValues = [];
                $aAttributes = $oProduct->getAttributes();

                foreach ($aHeaders as $sKey) {
                    if (isset($aAttributes[$sKey])) {
                        $aValues[] = $aAttributes[$sKey];
                    } else {
                        $aValues[] = "";
                    }
                }

                fputcsv($fp, $aValues, ";");
            }
        }

        fclose($fp);
        exit;
    }

    public function actionImport()
    {
        $aFiles = $_FILES;
        $sRaw = file_get_contents($aFiles["import"]["tmp_name"]);
        $aData = str_getcsv($sRaw, "\n", '"');

        $aHeaders = [];
        foreach ($aData as $index => $sRow) {
            if ($index == 0) {
                $aCols = str_getcsv($sRow, ";", '"');

                foreach ($aCols as $i => $sCol) {
                    if ($i == 0) {
                        $sCol = str_replace(chr(0xEF), "", $sCol);
                        $sCol = str_replace(chr(0xBB), "", $sCol);
                        $sCol = str_replace(chr(0xBF), "", $sCol);
                    }
                    $aHeaders[] = $sCol;
                }
            }
            if ($index > 0) {
                $oProduct = null;
                $aCols = str_getcsv($sRow, ";", '"');

                foreach ($aCols as $i => $sCol) {
                    if ($aHeaders[$i] == "id") {
                        $oProduct = Product::findOne($sCol);
                    }

                    if ($aHeaders[$i] == "code" && !$oProduct) {
                        $oProduct = Product::find()->where(["=", "code", $sCol])->one();
                    }
                }

                if ($oProduct) {
                    foreach ($aCols as $i => $sCol) {
                        $oProduct->{$aHeaders[$i]} = $sCol;
                    }
                }
                $oProduct->save();
            }
        }

        exit;
    }

    public function actionSort()
    {
        $aPost = Yii::$app->request->post();

        $aProdSorts = json_decode($aPost["aArtData"], true);

        foreach ($aProdSorts as $aProdSort) {
            $oProduct = Product::findOne($aProdSort["id"]);
            $oProduct->sort = $aProdSort["sort"];
            $oProduct->save();
        }

        exit;
    }

    public function actionCommentAccept($id)
    {
        $oProductComment = ProductComment::findOne($id);
        $oProductComment->status_id = 1;
        $oProductComment->save();

        return $this->redirect(['update', 'id' => $oProductComment->product_id]);
    }

    public function actionCommentDiscard($id)
    {
        $oProductComment = ProductComment::findOne($id);
        $oProductComment->status_id = -1;
        $oProductComment->save();

        return $this->redirect(['update', 'id' => $oProductComment->product_id]);
    }

    public function actionAddAccessory($id)
    {
        $model = new ProductAccessory();
        $model->product_id = $id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $id]);
        } else {
            return $this->render('add-accessory', [
                        'model' => $model,
            ]);
        }
    }

    public function actionDeleteAccessory($id)
    {
        $oProductAccessory = ProductAccessory::findOne($id);
        $iProduct = $oProductAccessory->product_id;
        $oProductAccessory->delete();

        return $this->redirect(['update', 'id' => $iProduct]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = Product::findOne(["id" => $id]);
        $model->is_active = (int) !$model->is_active;
        $model->save();

        return $this->redirect(["index"]);
    }

    public function actionQuickeditCategory()
    {
        $iID = Yii::$app->request->post('id');
        $sName = Yii::$app->request->post('value');

        $oCategory = Category::find()->where(["=", "id", $iID])->one();
        $oCategory->name = $sName;
        $oCategory->save();
        exit;
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $oStock = new Stock();
            $oStock->product_id = $model->id;
            $oStock->stock_status_id = 1;
            $oStock->amount = 0;
            $oStock->save();

            $model->slug = $this->createUniqueUrl($model->name);
            $model->vat = 23;
            $model->date_created = date("Y-m-d H:i:s");
            $model->save();

            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $oAccessorySearchModel = new ProductAccessorySearch();
        $oAccessoryDataProvider = $oAccessorySearchModel->search(Yii::$app->request->queryParams);
        $oAccessoryDataProvider->query->andWhere(["=", "product_id", $id]);

        $oProductCommentSearchModel = new ProductCommentSearch();
        $oProductCommentDataProvider = $oProductCommentSearchModel->search(Yii::$app->request->queryParams);
        $oProductCommentDataProvider->query->andWhere(["=", "product_id", $id]);

        $model = Product::findOne(["id" => $id]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $aPost = Yii::$app->request->post();

            if (isset($aPost["PriceProduct"])) {
                foreach ($aPost["PriceProduct"] as $iPricingID => $aProductPricing) {
                    $oPricingProduct = PriceProduct::find()->where(["=", "shop_price_id", $iPricingID])->andWhere(["=", "product_id", $model->id])->one();
                    if (!isset($oPricingProduct)) {
                        $oPricingProduct = new PriceProduct();
                    }

                    $oPricingProduct->shop_price_id = $iPricingID;
                    $oPricingProduct->product_id = $model->id;
                    $oPricingProduct->price_brutto = $aProductPricing["price_brutto"];
                    $oPricingProduct->price_netto = $aProductPricing["price_netto"];
                    $oPricingProduct->old_price_brutto = $aProductPricing["old_price_brutto"];
                    $oPricingProduct->old_price_netto = $aProductPricing["old_price_netto"];
                    $oPricingProduct->vat = $aProductPricing["vat"];
                    $oPricingProduct->save();
                }
            }

            $aAddCategories = [];
            foreach ($aPost["Product"] as $key => $val) {
                if (is_int($key) && !is_null($val["add_category_hash"])) {
                    array_push($aAddCategories, $val["add_category_hash"]);
                }
            }
            $model->add_category_hash = implode(",", array_filter($aAddCategories));
            $model->date_updated = date("Y-m-d H:i:s");
            $model->save();

            if (isset($aPost["ProductAttribute"])) {
                foreach ($aPost["ProductAttribute"] as $aProductAttribute) {
                    $oProductAttribute = ProductAttribute::find()->where(["=", "product_id", $aProductAttribute["product_id"]])->andWhere(["=", "attribute_id", $aProductAttribute["attribute_id"]])->one();
                    if (!$oProductAttribute) {
                        $oProductAttribute = new ProductAttribute();
                    }

                    $oProductAttribute->product_id = $aProductAttribute["product_id"];
                    $oProductAttribute->attribute_id = $aProductAttribute["attribute_id"];
                    $oProductAttribute->value_id = $aProductAttribute["value_id"];
                    $oProductAttribute->save();
                }
            }

            ProductRelated::deleteAll(['product_id' => $id]);
            if (isset($aPost["Product"]["related"])) {
                foreach ($aPost["Product"]["related"] as $iProductRelated) {
                    $oProductRelated = new ProductRelated();
                    $oProductRelated->product_id = $id;
                    $oProductRelated->related_id = $iProductRelated;
                    $oProductRelated->save();
                }
            }

            $oStock = Stock::find()->where(["product_id" => $model->id])->one();
            $oStock->stock_status_id = $aPost["Stock"]["stock_status_id"];
            $oStock->amount = $aPost["Stock"]["amount"];
            $oStock->save();

            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            $aRelated = [];
            /* foreach($model->getProductsRelated() as $oRelated){
              array_push($aRelated, $oRelated->related_id);
              } */

            $oStock = Stock::find()->where(["product_id" => $model->id])->one();
            return $this->render('update', [
                        'model' => $model,
                        'aRelated' => $aRelated,
                        'oStock' => $oStock,
                        'oAccessorySearchModel' => $oAccessorySearchModel,
                        'oAccessoryDataProvider' => $oAccessoryDataProvider,
                        'oProductCommentSearchModel' => $oProductCommentSearchModel,
                        'oProductCommentDataProvider' => $oProductCommentDataProvider

                            //'oProductRowset' => Product::find()->where(['<>', 'id', $id])->orderBy('name ASC')->all()
            ]);
        }
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionReplaceThumbnail($id, $image_id)
    {
        $oProduct = $this->findModel($id);
        $oProduct->thumbnail_id = $image_id;
        $oProduct->save();

        return $this->redirect(['update', 'id' => $id]);
    }

    public function actionActionbar()
    {
        $sAction = Yii::$app->request->post('action');
        $sValue = Yii::$app->request->post('value');
        $aIDs = json_decode(Yii::$app->request->post('rows', ""));

        foreach ($aIDs as $iID) {
            $oProduct = Product::findOne(["id" => $iID]);

            switch ($sAction) {
                case "active":
                    $oProduct->is_active = 1;
                    $oProduct->save();
                    break;

                case "deactive":
                    $oProduct->is_active = 0;
                    $oProduct->save();
                    break;

                case "delete":
                    $oProduct->delete();
                    break;

                case "change-category":
                    $oProduct->category_id = $sValue;
                    $oProduct->save();
                    break;

                case "change-attribute-values":
                    $aData = explode("_", $sValue);

                    $oProductAttribute = ProductAttribute::find()->where(["=", "product_id", $oProduct->id])->andWhere(["=", "value_id", $aData[0]])->one();
                    $oProductAttribute->value_id = $aData[1];
                    $oProductAttribute->save();
                    break;
            }
        }

        exit;
    }

    public function actionAjaxgetproducts($query)
    {
        if (Yii::$app->request->isAjax && !empty($query)) {
            $oProductRowset = Product::find()->where(["OR",
                        ["LIKE", "name", $query],
                        ["LIKE", "code", $query]
                    ])->all();

            $aProductItems = [];
            foreach ($oProductRowset as $index => $oProduct) {
                $aProductItems[$index]["id"] = $oProduct->id;
                $aProductItems[$index]["text"] = $oProduct->name;
                $aProductItems[$index]["code"] = $oProduct->code;
            }

            $aReturnArr = [];
            $aReturnArr["total_count"] = count($oProductRowset);
            $aReturnArr["items"] = array_filter($aProductItems);

            echo json_encode(array_filter($aReturnArr));
            exit;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAjaxgetcategories($query)
    {
        if (Yii::$app->request->isAjax && !empty($query)) {
            $oCategoryRowset = Category::find()->where(["LIKE", "name", $query])->all();

            $aCategoryItems = [];
            foreach ($oCategoryRowset as $index => $oCategory) {
                $aCategoryItems[$index]["id"] = $oCategory->id;
                $aCategoryItems[$index]["text"] = $oCategory->name;
            }

            $aReturnArr = [];
            $aReturnArr["total_count"] = count($oCategoryRowset);
            $aReturnArr["items"] = $aCategoryItems;

            echo json_encode($aReturnArr);
            exit;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function createUniqueUrl($name)
    {
        $unique = false;
        $suffix = '';
        while ($unique === false) {
            $slug = $this->prepareURL($name) . $suffix;
            $oProduct = Product::find()->where(["slug" => $slug])->one();
            if (!$oProduct) {
                return $slug;
            } else {
                $suffix = $suffix . '-';
            }
        }
    }

    protected function prepareURL($sText)
    {
        $aReplacePL = array('ą' => 'a', 'ę' => 'e', 'ś' => 's', 'ć' => 'c', 'ó' => 'o', 'ń' => 'n', 'ż' => 'z', 'ź' => 'z', 'ł' => 'l', 'Ą' => 'A', 'Ę' => 'E', 'Ś' => 'S', 'Ć' => 'C', 'Ó' => 'O', 'Ń' => 'N', 'Ż' => 'Z', 'Ź' => 'Z', 'Ł' => 'L');
        $sText = str_replace(array_keys($aReplacePL), array_values($aReplacePL), $sText);
        $sText = str_replace(' ', '-', strtolower($sText));
        $sText = preg_replace('/[\-]+/', '-', $sText);
        return trim($sText, '-');
    }

}
