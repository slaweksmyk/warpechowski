<?php

namespace common\modules\shop\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;

use common\modules\shop\models\Stock;
use common\modules\shop\models\StockSearch;

use common\modules\shop\models\ShopStocksStates;
use common\modules\shop\models\ShopStocksStatesSearch;

use common\modules\logs\models\Log;

/**
 * ShopProductController implements the CRUD actions for Product model.
 */
class ShopStockController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StockSearch(); 
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams); 

        $searchModel2 = new ShopStocksStatesSearch(); 
        $dataProvider2 = $searchModel2->search(Yii::$app->request->queryParams); 
        
        return $this->render('index', [ 
            'searchModel' => $searchModel, 
            'dataProvider' => $dataProvider, 
            'searchModel2' => $searchModel2, 
            'dataProvider2' => $dataProvider2, 
        ]); 
    }
    
    public function actionImport()
    {
        $aFiles = $_FILES;
        $sRaw = file_get_contents($aFiles["import"]["tmp_name"]);
        $aData = str_getcsv($sRaw, "\n", '"');
        
        foreach($aData as $index => $sRow){
            if($index > 0){
                $aCols = str_getcsv($sRow, ";", '"');

                $oStock = Stock::find()->where(["=", "product_id", $aCols[0]])->one();
                $oStock->amount = $aCols[2];
                $oStock->save();
                
                $oStockState = ShopStocksStates::find()->where(["<=", "amount_from", $oStock->amount])->andWhere([">", "amount_to", $oStock->amount])->one();
                $oStock->stock_status_id = $oStockState->id;
                $oStock->save();
            }
        }

        exit;
    }
    
    public function actionExport()
    {
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Content-Description: File Transfer');
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename=export.csv;');
        header('Content-Transfer-Encoding: binary'); 

        $fp = fopen('php://output', 'w');
        fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));

        if ($fp){
            fputcsv($fp, ["ID Produktu", "Nazwa produktu", "Stan magazynowy"], ";");
            foreach(Stock::find()->all() as $oStock){
                fputcsv($fp, [$oStock->product_id, $oStock->getProduct()->one()->name, $oStock->amount], ";");
            }
        }

        fclose($fp);
        exit;
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = Stock::findOne(["id" => $id]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $oStockState = ShopStocksStates::find()->where(["<=", "amount_from", $model->amount])->andWhere([">", "amount_to", $model->amount])->one();

            $model->stock_status_id = $oStockState->id;
            $model->save();
            
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionCreate()
    {
        $model = new ShopStocksStates();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update-status', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionUpdateStatus($id)
    {
        $model = ShopStocksStates::find()->where(["=", "id", $id])->one();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update-status', 'id' => $model->id]);
        } else {
            return $this->render('update-status', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionDeleteStatus($id)
    {
        $model = ShopStocksStates::find()->where(["=", "id", $id])->one();

        $model->delete();
        
        return $this->redirect(['index']);
    }
    
}
