<?php

namespace common\modules\shop\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use common\modules\shop\models\CodeGenerator;

use common\modules\shop\models\ShopDiscountCodes;
use common\modules\shop\models\ShopDiscountCodesSearch;

/**
 * ShopDiscountController implements the CRUD actions for ShopDiscountCodes model.
 */
class ShopDiscountController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ShopDiscountCodes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ShopDiscountCodesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ShopDiscountCodes model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ShopDiscountCodes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ShopDiscountCodes();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    /**
     * Creates a new ShopDiscountCodes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionGenerate()
    {
        $model = new ShopDiscountCodes();
        $generator = new CodeGenerator();
        
        if($model->load(Yii::$app->request->post()) && $generator->load(Yii::$app->request->post())) {

            for($i = 0; $i < $generator->amount; $i++){
                $sCode = strtoupper(substr(md5(rand()), 0, 12));
                
                $oCode = new ShopDiscountCodes();
                $oCode->promotion_id = $generator->promotion_id;
                $oCode->promotion_subtype = $generator->promotion_subtype;
                $oCode->code = $sCode;
                $oCode->value = $model->value;
                $oCode->type = $model->type;
                $oCode->restriction_type = $model->restriction_type;
                if($generator->restriction_category){
                    $oCode->category_hash = implode(",", $generator->restriction_category);
                }
                if($generator->restriction_product){
                    $oCode->product_hash = implode(",", $generator->restriction_product);
                }
                $oCode->active_from = $model->active_from;
                $oCode->active_to = $model->active_to;
                $oCode->is_one_time = 1;
                $oCode->is_used = 0;
                $oCode->save();
            }
            
            return $this->redirect(['index']);
        }

        return $this->render('generate', [
            'generator' => $generator,
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ShopDiscountCodes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $aPost = Yii::$app->request->post();

            $aRestProduct = [];
            if(isset($aPost["aProductRest"])){
                foreach($aPost["aProductRest"] as $key => $val){
                    if(is_int($key) && !is_null($val)) {
                      array_push($aRestProduct, $val);
                    }  
                }
            }
            
            $aRestCategory = [];
            if(isset($aPost["aCategoryRest"])){
                foreach($aPost["aCategoryRest"] as $key => $val){
                    if(is_int($key) && !is_null($val)) {
                      array_push($aRestCategory, $val);
                    }  
                }
            }

            $model->product_hash  = implode(",", array_filter($aRestProduct));
            $model->category_hash = implode(",", array_filter($aRestCategory));
            $model->save();

            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            $aProductRest = [];
            foreach(array_filter(explode(",", $model->product_hash)) as $iProductID){
                array_push($aProductRest, $iProductID);
            }
            
            $aCategoryRest = [];
            foreach(array_filter(explode(",", $model->category_hash)) as $iCategoryID){
                array_push($aCategoryRest, $iCategoryID);
            }
            
            return $this->render('update', [
                'model' => $model,
                'aProductRest' => $aProductRest,
                'aCategoryRest' => $aCategoryRest
            ]);
        }
    }

    /**
     * Deletes an existing ShopDiscountCodes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ShopDiscountCodes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ShopDiscountCodes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ShopDiscountCodes::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
