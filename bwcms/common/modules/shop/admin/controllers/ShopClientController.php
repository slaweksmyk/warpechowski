<?php

namespace common\modules\shop\admin\controllers;

use Yii;
use common\modules\users\models\UserDataCompany;
use common\modules\users\models\UserDataCompanySearch;
use common\modules\files\models\File;
use common\modules\shop\models\ShopConfig;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ShopClientController implements the CRUD actions for UserDataCompany model.
 */
class ShopClientController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserDataCompany models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserDataCompanySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        $oShopConfig = ShopConfig::find()->one();
        
        if ($oShopConfig->load(Yii::$app->request->post()) && $oShopConfig->save()) {
            return $this->redirect(['index']);
        }
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'oShopConfig' => $oShopConfig
        ]);
    }

    /**
     * Displays a single UserDataCompany model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UserDataCompany model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserDataCompany();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing UserDataCompany model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
        $aCatDiscounts = json_decode($model->discount_categories, true);
        $aAmountDiscounts = json_decode($model->discount_amounts, true);

        if ($model->load(Yii::$app->request->post())) {
            $aPost = Yii::$app->request->post();
            $model->discount_amounts = json_encode($aPost["UserDataCompany"]["discount_amounts"]);
            $model->discount_categories = json_encode($aPost["UserDataCompany"]["discountCategories"]);
            $model->save();

            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'aAmountDiscounts' => $aAmountDiscounts,
                'aCatDiscounts' => $aCatDiscounts
            ]);
        }
    }
    
    public function actionDownload($id){
        $oFile = File::find()->where(["=", "id", $id])->one();
        
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.$oFile->filename);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . $oFile->size);
        ob_clean();
        flush();
        readfile(Yii::getAlias("@root/upload/{$oFile->filename}"));
        exit;
    }

    /**
     * Deletes an existing UserDataCompany model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserDataCompany model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserDataCompany the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserDataCompany::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionActive($id){
        $oUserCompanyData = $this->findModel($id);
        $oUser = $oUserCompanyData->getUser()->one();
        
        if(is_null($oUserCompanyData->patron_id)){
            exit("Proszę wybrać opiekuna klienta.");
        } else {
            $oUser->is_accepted = 1;
            $oUser->activated_at = date("Y-m-d H:i:s");
            $oUser->save();
            
            $sBody = "";
            $sBody .= "Klient: {$oUserCompanyData->firstname} {$oUserCompanyData->surname} (ID: $oUser->id)<br/>";
            $sBody .= "Został aktywowany.";

            Yii::$app->mailer->compose()
                ->setTo($oUserCompanyData->getPatron()->one()->getDataCompany()->one()->email)
                ->setSubject('Aktywacja klienta')
                ->setHtmlBody($sBody)
                ->send();
            
            $oShopConfig = ShopConfig::find()->one();
            
            Yii::$app->mailer->compose()
                ->setTo($oUserCompanyData->email)
                ->setSubject($oShopConfig->client_activation_topic)
                ->setHtmlBody($oShopConfig->client_activation_text)
                ->send();
            
            Yii::$app->getResponse()->redirect("/__cms/shop-client/")->send();
        }
        
        exit;
    }
    
}
