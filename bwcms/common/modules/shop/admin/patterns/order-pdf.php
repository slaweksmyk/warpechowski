<?php
    use common\modules\shop\models\Product;

    $oClient = $oOrder->getShopOrdersClients()->one();
?>

Data złożenia zamówienia: <b><?= date("d.m.Y H:i:s", strtotime($oOrder->order_date)) ?></b><br/><br/>

Status zamówienia. <b><?= ($oOrder->status == 1) ? "Nieopłacone" : "Opłacone" ?></b><br/>

Zamówienie nr. <b><?= sprintf('%04d', $oOrder->id); ?></b><br/>
Numer Faktury: <b><?= $oOrder->order_fv ?></b>

<br/><br/><br/>

<table style="width: 100%;" cellpadding="0" cellspacing="0">
    <tr>
        <td></td>
        <td>
            <b>Dane klienta</b><br/><br/>
            <?= $oClient->company ?>
            <?= $oClient->firstname ?> <?= $oClient->surname ?><br/>
            <?= $oClient->street ?> <?= $oClient->street_no ?> <?= $oClient->street_loc != "" ? "lok. ".$oClient->street_loc : "" ?><br/>
            <?= $oClient->postcode ?>, <?= $oClient->city ?><br/>
            <?= $oClient->country ?>
            
            <br/><br/>
            <?php if($oClient->nip != ""){ ?>
                NIP: <?= $oClient->nip ?><br/>
            <?php } ?>
            <?php if($oClient->email != ""){ ?>
                Email: <?= $oClient->email ?><br/>
            <?php } ?>
            <?php if($oClient->phone != ""){ ?>
                Tel. stacjonarny: <?= $oClient->phone ?><br/>
            <?php } ?>
            <?php if($oClient->mobile != ""){ ?>
                Tel. komórkowy: <?= $oClient->mobile ?><br/>
            <?php } ?>
        </td>
        
        <?php
            $oClient->email = $oClient->deliver_email;
            $oClient->mobile = $oClient->deliver_mobile;
            $oClient->phone = $oClient->deliver_phone;
            $oClient->street = $oClient->deliver_street;
            $oClient->city = $oClient->deliver_city;
            $oClient->postcode = $oClient->deliver_postcode;
            $oClient->street_no = $oClient->deliver_street_no;
            $oClient->street_loc = $oClient->deliver_street_loc;
            $oClient->country = $oClient->deliver_country;
        ?>

        <td>
            <b>Dane dostawy</b><br/><br/>
            <?= $oClient->firstname ?> <?= $oClient->surname ?><br/>
            <?= $oClient->street ?> <?= $oClient->street_no ?> <?= $oClient->street_loc != "" ? "lok. ".$oClient->street_loc : "" ?><br/>
            <?= $oClient->postcode ?>, <?= $oClient->city ?><br/>
            <?= $oClient->country ?>
            
            <br/><br/>
            <?php if($oClient->nip != ""){ ?>
                NIP: <?= $oClient->nip ?><br/>
            <?php } ?>
            <?php if($oClient->email != ""){ ?>
                Email: <?= $oClient->email ?><br/>
            <?php } ?>
            <?php if($oClient->phone != ""){ ?>
                Tel. stacjonarny: <?= $oClient->phone ?><br/>
            <?php } ?>
            <?php if($oClient->mobile != ""){ ?>
                Tel. komórkowy: <?= $oClient->mobile ?><br/>
            <?php } ?>
        </td>
    </tr>
</table>

<br/><br/><br/>

<table cellpadding="0" cellspacing="0"> 
    <tr>
        <td>
            <b style="font-size: 18px;">Lista produktów</b>
        </td>
    </tr>
</table>
<table cellpadding="0" cellspacing="0"> 
    <tr>
        <td height="15"></td>
    </tr>
</table>

<table cellpadding="0" cellspacing="0" class="top_table" border="1"> 
    <tr>
        <td width="30">LP.</td>
        <td width="150">Nazwa produktu</td>
        <td width="55">Ilość<br>sztuk</td>
        <td width="50">%<br>VAT</td>
        <td width="85">Cena netto<br>za szt. </td>
        <td width="85">Cena brutto<br>za szt. </td>
        <td width="120">Cena netto za szt.<br>po rabacie </td>
        <td width="125">Cena brutto za szt.<br>po rabacie </td>
        <td width="85">Cena netto</td>
        <td width="85">Cena brutto</td>
    </tr>
    <?php $fSum = 0.00; ?>
    <?php $s ?>
    <?php foreach ($oOrder->getShopOrdersItems()->all() as $index => $oOrderItem) { ?>
        <tr class="main">
            <td width="30"><?= $index+1 ?></td>
            <td width="150"><?= $oOrderItem->name ?></td>
            <td width="55"><?= $oOrderItem->amount ?></td>
            <td width="50"><?= number_format($oOrderItem->vat, 0, '.', '') ?>%</td>
            <td width="85"><?= $oOrderItem->org_price_netto != $oOrderItem->price_netto ? number_format($oOrderItem->org_price_netto, 2, '.', '').$oOrderItem->currency : $oOrderItem->price_netto.$oOrderItem->currency ?></td>
            <td width="85"><?= $oOrderItem->org_price_brutto != $oOrderItem->price_brutto ? number_format($oOrderItem->org_price_brutto, 2, '.', '').$oOrderItem->currency : $oOrderItem->price_brutto.$oOrderItem->currency ?></td>
            <td width="120"><?= $oOrderItem->org_price_netto != $oOrderItem->price_netto ? number_format($oOrderItem->price_netto, 2, '.', '').$oOrderItem->currency : "---" ?></td>
            <td width="125"><?= $oOrderItem->org_price_brutto != $oOrderItem->price_brutto ? number_format($oOrderItem->price_brutto, 2, '.', '').$oOrderItem->currency : "---" ?></td>
            <td width="85"><?= number_format($oOrderItem->price_netto*$oOrderItem->amount, 2, '.', '') ?><?= $oOrderItem->currency ?></td>
            <td width="85"><?= number_format($oOrderItem->price_brutto*$oOrderItem->amount, 2, '.', '') ?><?= $oOrderItem->currency ?></td>
            <?= $fSum += $oOrderItem->price_brutto*$oOrderItem->amount; ?>
        </tr>
    <?php } ?>
    <?php if(\Yii::$app->session->get('discount')){ ?>
        <tr class="sumary" >
            <td width="" colspan="7"></td>
            <td width="" colspan="3" style="color: #fff; border: 1px solid #000; border-top: none; background: #b8b8b8; color:#fff; text-align: right; padding-right: 10px; height: 55px;"><b style="font-size: 13px;">Wartość zamówienia przed rabatem:</b><br><b style="font-size: 17px;"><?= \Yii::$app->session->get('fOryginalFullCost') ?> <?= Yii::$app->session->get('currency') ?></b></td>
        </tr>
    <?php } ?>
    <tr class="sumary">
        <td width="" colspan="7"></td>
        <td width="" colspan="3" style="height: 55px;"><b style="font-size: 17px;">Koszt całkowity: <?= number_format(Yii::$app->session->get('fFullCost', 0.00), 2, '.', '') ?> zł</b></td>
    </tr>
</table>

<br/><br/><br/>

Sposób dostawy: <b><?= $oOrder->getDeliver()->one()->name ?></b><br/>
Rodzaj płatności <b><?= $oOrder->getPayment()->one()->name ?></b>