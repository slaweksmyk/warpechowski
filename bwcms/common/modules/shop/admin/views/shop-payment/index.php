<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;

$this->title = "Zarządzaj płatnościami";
?>
<div class="shop-payment-index">

    <p>
        <?= Html::a(Yii::t('backend_module_shop', 'Create Shop Payment'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                                        <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    'name',
                    [
                        'attribute' => 'api',
                        'format' => 'raw',
                        'contentOptions'=>['style'=>'width: 145px;']
                    ],
                    [
                        'attribute' => 'is_local',
                        'format' => 'raw',
                        'contentOptions'=>['style'=>'width: 145px;'],
                        'value' => function ($data) {
                            return (1 == $data["is_local"]) ?  Yii::t('backend', 'yes') :  Yii::t('backend', 'no');
                        }
                    ],
                    [
                        'attribute' => 'is_active',
                        'format' => 'raw',
                        'contentOptions'=>['style'=>'width: 145px;'],
                        'filter'=> [1 => "Aktywny", 0 => "Nieaktywny"],
                        'value' => function ($data) {
                            return (1 == $data["is_active"]) ?  Yii::t('backend', 'active') :  Yii::t('backend', 'inactive');
                        }
                    ],
                    [
                        'attribute' => 'thankyou',
                        'format' => 'raw',
                        'contentOptions'=>['style'=>'width: 145px;']
                    ],
                    ['class' => 'common\hooks\yii2\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </section>   
</div>
