<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use common\modules\files\models\File;

$this->title = Yii::t('backend_module_shop', 'Update {modelClass}: ', [
    'modelClass' => 'Shop Payment',
]) . $model->name;
?>

<p>
    <?= Html::a("< Wróć do listy", ['index'], ['class' => 'btn btn-success']) ?>
</p>

<div class="shop-payment-update">
    <?php $form = ActiveForm::begin(); ?>
        <section class="panel full" >
            <header>
                <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
            </header>
            <div class="panel-body" >
                <div class="row">
                    <div class="col-3"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
                    <div class="col-2"><?= $form->field($model, 'thumbnail_id')->dropDownList(ArrayHelper::map(File::find()->all(), 'id', 'name'), ["prompt" => "- wybierz miniature -"]) ?></div>
                    <div class="col-2"><?= $form->field($model, 'api')->dropDownList(["PayU" => "PayU", "Przelewy24" => "Przelewy24", "Dotpay" => "Dotpay"]) ?></div>
                    <div class="col-2"><?= $form->field($model, 'thankyou')->textInput(['maxlength' => true]) ?></div>
                    <div class="col-3">
                        <div class="row">
                            <div class="col-5"><?= $form->field($model, 'is_local')->dropDownList([0 => Yii::t('backend_module_articles', 'no'), 1 => Yii::t('backend_module_articles', 'yes')]) ?></div>
                            <div class="col-7"><?= $form->field($model, 'is_active')->dropDownList([0 =>  Yii::t('backend_module_articles', 'inactive'), 1 =>  Yii::t('backend_module_articles', 'active')]) ?></div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_shop', 'Create') : Yii::t('backend', 'save_changes'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </section>   

        <?php if($model->api == "PayU"){ ?>
            <section class="panel full" >
                <header>
                    <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Konfiguracja PayU    
                </header>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-3"><?= $form->field($model, 'config[MerchantPosId]')->textInput(["placeholder" => "300746"])->label("ID punktu płatności (pos_id)") ?></div>
                        <div class="col-3"><?= $form->field($model, 'config[SignatureKey]')->textInput(["placeholder" => "b6ca15b0d1020e8094d9b5f8d163db54"])->label("Drugi klucz (MD5)") ?></div>
                        <div class="col-3"><?= $form->field($model, 'config[OauthClientId]')->textInput(["placeholder" => "300746"])->label("Protokół OAuth (client_id)") ?></div>
                        <div class="col-3"><?= $form->field($model, 'config[OauthClientSecret]')->textInput(["placeholder" => "2ee86a66e5d97e3fadc400c9f19b065d"])->label("Protokół OAuth (client_secret)") ?></div>
                    </div>

                    <div class="row">
                        <div class="col-3"><?= $form->field($model, 'config[Environment]')->dropDownList(["sandbox" => "Testowe", "secure" => "Produkcyjne"])->label("Środowisko") ?></div>
                        <div class="col-3"><?= $form->field($model, 'config[isFv]')->dropDownList([0 => "Nie", 1 => "Tak"])->label("Czy PayU ma wystawić FV?") ?></div>
                        <div class="col-3"><?= $form->field($model, 'config[Description]')->textInput()->label("Opis zamówienia") ?></div>
                        <div class="col-3"><?= $form->field($model, 'config[CountryCode]')->dropDownList(["PL" => "Polska [PL]"])->label("Kod kraju podatkowego") ?></div>
                    </div>

                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_shop', 'Create') : Yii::t('backend', 'save_changes'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                </div>
            </section>
        <?php } ?>
    
        <?php if($model->api == "Przelewy24"){ ?>
            <section class="panel full" >
                <header>
                    <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Konfiguracja Przelewy24    
                </header>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-4"><?= $form->field($model, 'config[MerchantID]')->textInput()->label("ID Sprzedawcy") ?></div>
                        <div class="col-4"><?= $form->field($model, 'config[ShopID]')->textInput()->label("ID Sklepu") ?></div>
                        <div class="col-4"><?= $form->field($model, 'config[Salt]')->textInput()->label("Unikalny identyfikator") ?></div>
                    </div>
                    
                    <div class="row">
                        <div class="col-4"><?= $form->field($model, 'config[CountryCode]')->dropDownList(["PL" => "Polska [PL]"])->label("Kod kraju podatkowego") ?></div>
                        <div class="col-4"><?= $form->field($model, 'config[Description]')->textInput()->label("Opis zamówienia") ?></div>
                        <div class="col-4"><?= $form->field($model, 'config[Environment]')->dropDownList(["sandbox" => "Testowe", "secure" => "Produkcyjne"])->label("Środowisko") ?></div>
                    </div>

                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_shop', 'Create') : Yii::t('backend', 'save_changes'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                </div>
            </section>
        <?php } ?>
    
        <?php if($model->api == "Dotpay"){ ?>
            <section class="panel full" >
                <header>
                    <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Konfiguracja Dotpay    
                </header>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-4"><?= $form->field($model, 'config[MerchantID]')->textInput()->label("ID konta Dotpay") ?></div>
                        <div class="col-4"><?= $form->field($model, 'config[PIN]')->textInput()->label("PIN") ?></div>
                        <div class="col-4"><?= $form->field($model, 'config[Environment]')->dropDownList(["test" => "Testowe", "production" => "Produkcyjne"])->label("Środowisko") ?></div>
                    </div>
                    
                    <div class="row">
                        <div class="col-4"><?= $form->field($model, 'config[Urlc]')->textInput()->label("URLC") ?></div>
                        <div class="col-4"><?= $form->field($model, 'config[Description]')->textInput()->label("Opis zamówienia") ?></div>
                    </div>

                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_shop', 'Create') : Yii::t('backend', 'save_changes'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                </div>
            </section>
        <?php } ?>
    
    
    
    

    <?php ActiveForm::end(); ?>
</div>
