<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('backend_module_shop', 'Create Shop Payment');
?>

<p>
    <?= Html::a("< Wróć do listy", ['index'], ['class' => 'btn btn-success']) ?>
</p>

<div class="shop-payment-create">

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
        </header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(); ?>
            
            <div class="row">
                <div class="col-5"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
                <div class="col-2"><?= $form->field($model, 'api')->textInput(['maxlength' => true]) ?></div>
                <div class="col-2"><?= $form->field($model, 'thankyou')->textInput(['maxlength' => true]) ?></div>
                <div class="col-3">
                    <div class="row">
                        <div class="col-5"><?= $form->field($model, 'is_local')->dropDownList([0 => Yii::t('backend_module_articles', 'no'), 1 => Yii::t('backend_module_articles', 'yes')]) ?></div>
                        <div class="col-7"><?= $form->field($model, 'is_active')->dropDownList([0 =>  Yii::t('backend_module_articles', 'inactive'), 1 =>  Yii::t('backend_module_articles', 'active')]) ?></div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_shop', 'Create') : Yii::t('backend', 'save_changes'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </section>  

</div>
