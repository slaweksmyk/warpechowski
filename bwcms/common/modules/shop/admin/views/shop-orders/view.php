<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->id;
?>

<p>
    <?= Html::a("< Wróć do listy", ['index'], ['class' => 'btn btn-success']) ?>
</p>

<div class="shop-orders-view">

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
        </header>
        <div class="panel-body" >
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
            'deliver_id',
            'status',
            'order_fv',
            'order_date',
                ],
            ]) ?>
        </div>
    </section>  

</div>
