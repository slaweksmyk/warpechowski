<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;

use common\modules\shop\models\ShopOrders;
use common\modules\shop\models\ShopPayment;
use common\modules\shop\models\ShopDelivers;

$this->title = "Zarządzaj zamówieniami";
?>
<div class="shop-orders-index">

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'id',
                        'label' => "ID - Numer zamówienia"
                    ],
                    [
                        'attribute' => "firstname",
                        'label' => Yii::t('backend_module_shop', 'firstname surname'),
                        'format' => 'raw',
                        'value' => function ($data) {
                            $oOrder  = ShopOrders::findOne(["=", "id", $data["id"]]);
                            $oClient = $oOrder->getShopOrdersClients()->one();
                            
                            return "{$oClient->firstname} {$oClient->surname}";
                        }
                    ],
                    [
                        'attribute' => "company",
                        'label' => "Nazwa firmy",
                        'format' => 'raw',
                        'value' => function ($data) {
                            $oOrder  = ShopOrders::findOne(["=", "id", $data["id"]]);
                            $oClient = $oOrder->getShopOrdersClients()->one();
                            
                            return $oClient->company;
                        }
                    ],     
                    [
                        'attribute' => 'deliver_id',
                        'filter' => ArrayHelper::map(ShopDelivers::find()->all(), 'id', 'name'),
                        'label' => "Rodzaj dostawy",
                        'format' => 'raw',
                        'value' => function ($data) {
                            $oDeliver = ShopDelivers::findOne(["=", "id", $data["deliver_id"]]);
                            if(!is_null($oDeliver)) {
                                return $oDeliver->name;
                            }
                        }
                    ],
                    [
                        'attribute' => 'payment_id',
                        'label' => "Rodzaj płatności",
                        'filter' => ArrayHelper::map(ShopPayment::find()->all(), 'id', 'name'),
                        'format' => 'raw',
                        'value' => function ($data) {
                            $oPayment = ShopPayment::findOne(["=", "id", $data["payment_id"]]);
                            if(!is_null($oPayment)) {
                                return $oPayment->name;
                            }
                        }
                    ],
                    [
                        'attribute' => "order_date",
                        'label' => Yii::t('backend_module_shop', 'order_date'),
                        'format' => 'raw',
                        'value' => function ($data) {
                            return $data["order_date"];
                        }
                    ],
                    [
                        'attribute' => 'status',
                        'label' => Yii::t('backend_module_shop', 'status'),
                        'format' => 'raw',
                        'filter'=> [2 => "Za pobraniem", 1 => "Nieopłacone", 0 => "Opłacone"],
                        'value' => function ($data) {
                            switch($data["status"]){
                                case 0: return "Nieopłacone";
                                case 1: return "Opłacone";
                                case 2: return "Za pobraniem";
                            }
                        }
                    ],
                    ['class' => 'common\hooks\yii2\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </section>   
</div>
