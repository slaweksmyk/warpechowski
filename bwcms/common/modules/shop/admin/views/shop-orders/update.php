<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;
use yii\data\ActiveDataProvider;

use common\modules\shop\models\Product;

$this->title = "Szczegóły zamówienia";
?>

<p>
    <?= Html::a("< Wróć do listy", ['index'], ['class' => 'btn btn-success']) ?>
    <?= Html::a("Wygeneruj .PDF", ['generate-pdf', 'id' => \Yii::$app->request->get('id')], ['class' => 'btn btn-primary']) ?>
</p>

<div class="shop-orders-update">

    <section class="panel full">
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
        </header>
        <div class="panel-body" >
            <div class="row">
                <div class="col-3">
                    <div class="form-group">
                        <label class="control-label">Numer zamówienia</label>
                        <input class="form-control" value="<?= $model->id ?>" readonly>
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label class="control-label">Numer FV</label>
                        <input class="form-control" value="<?= $model->order_fv ?>" readonly>
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label class="control-label">Sposób dostawy</label>
                        <input class="form-control" value="<?= $model->getDeliver()->one()->name ?>" readonly>
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label class="control-label">Sposób płatności</label>
                        <input class="form-control" value="<?= $model->getPayment()->one()->name ?>" readonly>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-3">
                    <div class="form-group">
                        <?php
                            $sStatus;
                            switch($model->status){
                                case 0: $sStatus = "Nieopłacone"; break;
                                case 1: $sStatus = "Opłacone"; break;
                            }
                        ?>
                        <label class="control-label">Status</label>
                        <input class="form-control" value="<?= $sStatus ?>" readonly>
                    </div>
                </div>
                <div class="col-9">
                    <div class="form-group">
                        <label class="control-label">Data zamówienia</label>
                        <input class="form-control" value="<?= $model->order_date ?>" readonly>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="panel full">
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Dane klienta            
        </header>
        <div class="panel-body" >
            <div class="row">
                <div class="col-3">
                    <div class="form-group">
                        <label class="control-label">Imię</label>
                        <input class="form-control" value="<?= $model->getShopOrdersClients()->one()->firstname ?>" readonly>
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label class="control-label">Nazwisko</label>
                        <input class="form-control" value="<?= $model->getShopOrdersClients()->one()->surname ?>" readonly>
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        <label class="control-label">Adres E-mail</label>
                        <input class="form-control" value="<?= $model->getShopOrdersClients()->one()->email ?>" readonly>
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        <label class="control-label">Nr telefonu</label>
                        <input class="form-control" value="<?= $model->getShopOrdersClients()->one()->phone ?>" readonly>
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        <label class="control-label">Nr komórkowy</label>
                        <input class="form-control" value="<?= $model->getShopOrdersClients()->one()->mobile ?>" readonly>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <label class="control-label">Ulica</label>
                        <input class="form-control" value="<?= $model->getShopOrdersClients()->one()->street ?>" readonly>
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        <label class="control-label">Kod pocztowy</label>
                        <input class="form-control" value="<?= $model->getShopOrdersClients()->one()->postcode ?>" readonly>
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        <label class="control-label">Miasto</label>
                        <input class="form-control" value="<?= $model->getShopOrdersClients()->one()->city ?>" readonly>
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        <label class="control-label">Kraj</label>
                        <input class="form-control" value="<?= $model->getShopOrdersClients()->one()->country ?>" readonly>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label class="control-label">NIP</label>
                        <input class="form-control" value="<?= $model->getShopOrdersClients()->one()->nip ?>" readonly>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="panel full">
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Dane dostawy           
        </header>
        <div class="panel-body" >
            <div class="row">
                <div class="col-4">
                    <div class="form-group">
                        <label class="control-label">Adres e-mail</label>
                        <input class="form-control" value="<?= $model->getShopOrdersClients()->one()->deliver_email ?>" readonly>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label class="control-label">Nr komórkowy</label>
                        <input class="form-control" value="<?= $model->getShopOrdersClients()->one()->deliver_mobile ?>" readonly>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label class="control-label">Nr telefonu</label>
                        <input class="form-control" value="<?= $model->getShopOrdersClients()->one()->deliver_phone ?>" readonly>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <label class="control-label">Ulica</label>
                        <input class="form-control" value="<?= $model->getShopOrdersClients()->one()->deliver_street ?>" readonly>
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label class="control-label">Kod pocztowy</label>
                        <input class="form-control" value="<?= $model->getShopOrdersClients()->one()->deliver_postcode ?>" readonly>
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label class="control-label">Miasto</label>
                        <input class="form-control" value="<?= $model->getShopOrdersClients()->one()->deliver_city ?>" readonly>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="panel full">
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Zamówione przedmioty           
        </header>
        <div class="panel-body" >
            <?php 
                $dataProvider = new ActiveDataProvider([
                    'query' => $model->getShopOrdersItems(),
                    'pagination' => [
                        'pageSize' => 10,
                    ],
                ]);
                echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        'id',
                        [
                            'label' => Yii::t('backend_module_shop', 'product_id'),
                            'format' => 'raw',
                            'value' => function ($data) {
                                $oProduct  = Product::findOne(["=", "id", $data["product_id"]]);
                                if(!is_null($oProduct)){
                                    return $oProduct->name;
                                }
                            }
                        ],
                        'amount',
                        'price_netto',
                        'price_brutto',
                        [
                            'label' => Yii::t('backend_module_shop', 'final_price'),
                            'format' => 'raw',
                            'value' => function ($data) {
                                return number_format($data["price_brutto"]*$data["amount"], 2, '.', '');
                            }
                        ],
                    ]
                ]);
            ?>
        </div>
    </section>
    
    

</div>
