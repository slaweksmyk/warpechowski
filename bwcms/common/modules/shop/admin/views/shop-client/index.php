<?php

use yii\helpers\Html;
use common\hooks\yii2\grid\GridView;
use yii\widgets\ActiveForm;

$this->title = "Zarządzaj klientami";
?> 
<style>
    .table-bordered > tbody > tr > td:nth-child(2) { width: 20%; }
    
</style>
<div class="user-data-company-index">
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/user.png" alt="logo" style="width: 24px;"/> <?= $this->title ?>          
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'rowOptions'=>function($model){
                    if(!$model->getUser()->one()->is_accepted){
                        return ['class' => 'inactive-row'];
                    }
                },
                'columns' => [
                    'id',
                    [
                        'attribute' => 'company',
                        'label' => "Nazwa firmy",
                        'value' => function ($data) {
                            if($data->company == ""){
                                $data->company = "-";
                            }
                            return $data->company;
                        }
                    ],
                    'firstname',
                    'surname',
                    [
                        'attribute' => 'street',
                        'value' => function ($data) {
                            return "$data->street $data->street_no / $data->street_loc";
                        }
                    ],
                    'city',
                    'email:email',
                    'nip',
                    [
                        'class' => 'common\hooks\yii2\grid\ActionColumn',
                        'template' => '{active} {update} {delete}',
                        'buttons' => [
                            'active' => function ($url, $model) {
                                if(!$model->getUser()->one()->is_accepted){
                                    return Html::a(
                                        '<span class="glyphicon glyphicon-eye-close"></span>',
                                        $url, 
                                        [
                                            'title' => "Aktywuj użytkownika",
                                            'data-pjax' => '0',
                                        ]
                                    );
                                } else {
                                    return Html::a(
                                        '<span class="glyphicon glyphicon-eye-open"></span>',
                                        $url, 
                                        [
                                            'title' => "Aktywny",
                                            'data-pjax' => '0',
                                        ]
                                    );
                                }
                            },
                        ],
                    ],
                ],
            ]); ?>
            <div class="row">
                <div class="col-1" style="font-weight: bold; padding-left: 17px; padding-top: 1px;">*LEGENDA:</div>
                <div class="col-3" style="margin-left: 10px;">
                    <div style="width: 13%; height: 21px; float: left; background: #f9e2e2; text-align: center; padding-top: 3px;">
                        <span class="glyphicon glyphicon-eye-close"></span>
                    </div>
                    <p style="float: right;">- Konto nieaktywne / aktywuj</p>
                </div>
                <div class="col-2">
                    <span class="glyphicon glyphicon-eye-open"></span> 
                    - Konto aktywne
                </div>
                <div class="col-2">
                    <span class="glyphicon glyphicon-pencil"></span> 
                    - Edytuj dane
                </div>
                <div class="col-2">
                    <span class="glyphicon glyphicon-trash"></span> 
                    - Usuń
                </div>
            </div>
        </div>
    </section>
    
</div>
