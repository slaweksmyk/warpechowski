<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\shop\models\Category;
use common\modules\shop\models\PriceList;

$this->title = "Zaktualizuj: {$model->firstname} {$model->surname}";
?>

<p>
    <?= Html::a("< Wróć", ['index'], ['class' => 'btn btn-success']) ?>
    <?php if(!$model->getUser()->one()->is_accepted){ ?>
        <?= Html::a("Aktywuj użytkownika", ['active', "id" => $model->id], ['class' => 'btn btn-success']) ?>
    <?php } ?>
</p>

<div class="user-update">
    <?php $form = ActiveForm::begin(); ?>

        <section class="panel full">
            <header>
                <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
            </header>
            <div class="panel-body" >

                <div class="row">
                    <div class="col-3">
                        <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'nip')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model->getUser()->one(), 'created_at')->textInput(['maxlength' => true])->label("Data rejestracji użytkownika") ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-3">
                        <?= $form->field($model, 'company')->textInput(['maxlength' => true])->label("Nazwa firmy") ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'street')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-2">
                        <?= $form->field($model, 'street_no')->textInput(['maxlength' => true])->label("Numer budynku") ?>
                    </div>
                    <div class="col-2">
                        <?= $form->field($model, 'street_loc')->textInput(['maxlength' => true])->label("Numer lokalu") ?>
                    </div>
                    <div class="col-2">
                        <?= $form->field($model, 'postcode')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-2">
                        <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-2">
                        <?= $form->field($model, 'country')->textInput(['maxlength' => true]) ?>
                    </div>

                </div>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_shop', 'Create') : "Zapisz zmiany", ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
                
            </div>
        </section>

         <section class="panel full">
            <header>
                <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Dane dostawy          
            </header>
            <div class="panel-body" >

                <div class="row">
                    <div class="col-4">
                        <?= $form->field($model, 'deliver_email')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-4">
                        <?= $form->field($model, 'deliver_phone')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-4">
                        <?= $form->field($model, 'deliver_mobile')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <?= $form->field($model, 'deliver_street')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-2">
                        <?= $form->field($model, 'deliver_street_no')->textInput(['maxlength' => true])->label("Numer budynku") ?>
                    </div>
                    <div class="col-2">
                        <?= $form->field($model, 'deliver_street_loc')->textInput(['maxlength' => true])->label("Numer lokalu") ?>
                    </div>
                    <div class="col-2">
                        <?= $form->field($model, 'deliver_postcode')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-2">
                        <?= $form->field($model, 'deliver_city')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
            </div>
        </section>
    
         <section class="panel full">
            <header>
                <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Zniżki        
            </header>
            <div class="panel-body" >
                <div class="row">
                    <div class="col-12">
                        <h4>Zniżka na wszystkie produkty</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <?= $form->field($model, 'discount_percs')->textInput(['maxlength' => true])->label("% zniżki") ?>
                    </div>
                    <?php foreach(PriceList::find()->all() as $oPricing){ ?>
                        <div class="col">
                            <?= $form->field($model, "discount_amounts[{$oPricing->short_currency}]")->textInput(['maxlength' => true, 'value' => isset($aAmountDiscounts[$oPricing->short_currency]) ? number_format((float)$aAmountDiscounts[$oPricing->short_currency], 2, '.', '') : null])->label("zniżka ({$oPricing->symbol_currency})") ?>
                        </div>
                   <?php } ?>
                </div>

                <div class="row">
                    <div class="col-12">
                        <h4>Zniżka na kategorię</h4>
                    </div>
                </div>

                <div class="row">
                    <?php foreach(Category::find()->all() as $oShopCategory){ ?>
                        <div class="col-3" style="padding-top: 30px;">
                            <b><?= $oShopCategory->name ?></b>
                        </div>
                        <div class="col-9">
                            <div class="row">
                                <div class="col">
                                    <?= $form->field($model, "discountCategories[{$oShopCategory->id}][perc]")->textInput(['maxlength' => true, 'placeholder' => '0.00', 'value' => isset($aCatDiscounts[$oShopCategory->id]["perc"]) ? number_format((float)$aCatDiscounts[$oShopCategory->id]["perc"], 2, '.', '') : null])->label("% zniżki") ?>
                                </div>
                                <?php foreach(PriceList::find()->all() as $oPricing){ ?>
                                    <div class="col">
                                        <?= $form->field($model, "discountCategories[{$oShopCategory->id}][{$oPricing->short_currency}]")->textInput(['maxlength' => true, 'placeholder' => '0.00', 'value' => number_format((float)$aCatDiscounts[$oShopCategory->id][$oPricing->short_currency], 2, '.', '')])->label("zniżka ({$oPricing->symbol_currency})") ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_shop', 'Create') : "Zapisz zmiany", ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </section>
    
    
    <?php ActiveForm::end(); ?>

</div>
