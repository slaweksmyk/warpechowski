<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use common\modules\shop\models\Product;

$this->title = 'Dodaj akcesorium';
?> 
<div class="product-accessory-create"> 

    <p> 
        <?= Html::a('< Wróć do produktu', ['update', 'id' => $model->product_id], ['class' => 'btn btn-outline-secondary']) ?> 
    </p> 

    <section class="panel full" > 
        <header> 
            <img src="/__fBFyVd/images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?> 
        </header> 
        <div class="panel-body" > 
            <?php $form = ActiveForm::begin(); ?> 

            <?= $form->field($model, 'accessory_id')->dropDownList(ArrayHelper::map(Product::find()->limit(500)->andWhere(["!=", "id", $model->product_id])->all(), 'id', 'name'), ["prompt" => "- wybierz produkt -"]) ?>

            <div class="form-group"> 
                <?= Html::submitButton($model->isNewRecord ? 'Dodaj' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?> 
            </div> 

            <?php ActiveForm::end(); ?> 
        </div> 
    </section> 

</div> 