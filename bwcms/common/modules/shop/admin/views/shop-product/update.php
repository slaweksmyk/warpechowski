<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = "Edytuj produkt: {$model->name}";

$this->registerJsFile('/bwcms/common/modules/shop/admin/assets/js/shop.js', ['depends' => [yii\web\JqueryAsset::className()]]);
$this->registerCssFile('/bwcms/common/modules/shop/admin/assets/css/shop.css');
?>

<p>
    <?= Html::a("< Wróć do listy", ['index'], ['class' => 'btn btn-outline-secondary']) ?>
</p>

<div class="product-form">
    <?php $form = ActiveForm::begin(); ?>

        <ul class="nav nav-tabs justify-content-end">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#basic">Dane podstawowe</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#price">Cennik</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#attributes">Atrybuty</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#accessory">Akcesoria</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#stock">Stan magazynowy</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#comments">Komentarze</a>
            </li>
        </ul>

        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="basic">
                <?= $this->render('sections/basic.php', [
                    'form' => $form,
                    'model' => $model,
                ]) ?>
            </div>
            <div class="tab-pane fade" id="price">
                <?= $this->render('sections/price.php', [
                    'form' => $form,
                    'model' => $model,
                ]) ?>
            </div>
            <div class="tab-pane fade" id="attributes">
                <?= $this->render('sections/attributes.php', [
                    'form' => $form,
                    'model' => $model,
                ]) ?>
            </div>
            <div class="tab-pane fade" id="accessory">
                <?= $this->render('sections/accessory.php', [
                    'oAccessorySearchModel' => $oAccessorySearchModel,
                    'oAccessoryDataProvider' => $oAccessoryDataProvider
                ]) ?>
            </div>
            <div class="tab-pane fade" id="stock">
                <?= $this->render('sections/stock.php', [
                    'form' => $form,
                    'model' => $model,
                    'oStock' => $oStock
                ]) ?>
            </div>
            <div class="tab-pane fade" id="comments">
                <?= $this->render('sections/comments.php', [
                    'oProductCommentSearchModel' => $oProductCommentSearchModel,
                    'oProductCommentDataProvider' => $oProductCommentDataProvider
                ]) ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>
