<?php
    use yii\helpers\Html;
     use common\hooks\yii2\grid\GridView;
?>
<section class="panel full" >
    <header>
        <div class="row">
            <div class="col-6 align-self-center">
                <img src="/__fBFyVd/images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
            </div>
            <div class="col-6 text-right">
                <?= Html::a("Dodaj akcesorium", ['add-accessory', "id" => Yii::$app->request->get('id')], ['class' => 'btn btn-success btn-sm']) ?>
            </div>
        </div>
    </header>
    <div class="panel-body" >
        <?=
        GridView::widget([
            'dataProvider' => $oAccessoryDataProvider,
            'filterModel' => $oAccessorySearchModel,
            'columns' => [
                'id',
                [
                    'attribute' => 'product_id',
                    'value' => function($model){
                        return $model->getProduct()->one()->name;
                    }
                ],
                [
                    'attribute' => 'accessory_id',
                    'value' => function($model){
                        return $model->getAccessory()->one()->name;
                    }
                ],     
                [
                    'class' => 'common\hooks\yii2\grid\ActionColumn',
                    'template' => '{delete-accessory}',
                    'contentOptions' => ['style' => 'width: 20px;'],
                    'buttons' => [
                        'delete-accessory' => function ($url) {
                            return Html::a(
                                    '<span class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></span>', $url, [
                                    'title' => Yii::t('backend', 'delete'),
                                    'data-toggle' => "tooltip",
                                    'data-placement' => "bottom",
                                    'data-pjax' => 0,
                                    'data-confirm' => 'Czy na pewno usunąć ten element?',
                                    'data-method' => 'post',
                                ]
                            );
                        },
                    ],
                ],
            ],
        ]);
        ?>
    </div>
</section>