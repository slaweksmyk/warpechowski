<?php
    use yii\helpers\Html;
    use yii\helpers\ArrayHelper;
    
    use common\modules\shop\models\ShopStocksStates;
?>

<section class="panel full" >
    <header>
        <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Stan magazynowy
    </header>
    <div class="panel-body">
        <div class="row">
            <div class="col-4"><?= $form->field($oStock, 'stock_status_id')->dropDownList(ArrayHelper::map(ShopStocksStates::find()->all(), 'id', 'name'), ["prompt" => "- wybierz status -"])->label("Nazwa statusu") ?></div>
            <div class="col-4"><?= $form->field($oStock, 'amount')->textInput() ?></div>
            <div class="col-4"><?= $form->field($model, 'low_state')->textInput() ?></div>
        </div>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_shop', 'Create') : "Zapisz zmiany", ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
</section>