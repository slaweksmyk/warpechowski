<?php
    use yii\helpers\Html;
     use common\hooks\yii2\grid\GridView;
?>
<section class="panel full" >
    <header>
        <img src="/__fBFyVd/images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
    </header>
    <div class="panel-body" >
        <?=
        GridView::widget([
            'dataProvider' => $oProductCommentDataProvider,
            'filterModel' => $oProductCommentSearchModel,
            'rowOptions' => function($model) {
                if ($model->status_id == -1) {
                    return ['class' => 'inactive-row'];
                }
                if ($model->status_id == 0) {
                    return ['class' => 'new-row'];
                }

                if ($model->status_id == 1) {
                    return ['class' => 'active-row'];
                }
            },
            'columns' => [
                'id',
                [
                    'attribute' => 'parent_id',
                    'value' => function($model){
                        return $model->getParent()->one() ? $model->getParent()->one()->id : null;
                    }
                ],
                [
                    'attribute' => 'status_id',
                    'value' => function($model){
                        switch($model->status_id){
                            case "-1": return "Odrzucony";
                            case "0":  return "Nowy";
                            case "1":  return "Zaakceptowany";
                        }
                    }
                ],  
                [
                    'attribute' => 'author',
                    'value' => function($model){
                        $oUser = $model->getUser()->one();
                        if($oUser){
                            return $model->getUser()->one()->username;
                        } else {
                            return $model->author;
                        }
                    }
                ],              
                'score',   
                'date_created',
                'text',
                [
                    'class' => 'common\hooks\yii2\grid\ActionColumn',
                    'template' => '{comment-accept} {comment-discard}',
                    'contentOptions' => ['style' => 'width: 100px;'],
                    'buttons' => [
                        'comment-accept' => function ($url) {
                            return Html::a(
                                    '<span class="btn btn-success btn-xs"><span class="glyphicon glyphicon-ok"></span></span>', $url, [
                                    'title' => "Zaakceptuj",
                                    'data-toggle' => "tooltip",
                                    'data-placement' => "bottom",
                                ]
                            );
                        },
                        'comment-discard' => function ($url) {
                            return Html::a(
                                    '<span class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span></span>', $url, [
                                    'title' => "Odrzuć",
                                    'data-toggle' => "tooltip",
                                    'data-placement' => "bottom",
                                ]
                            );
                        },
                    ],
                ],
            ],
        ]);
        ?>
    </div>
</section>