<?php
    use yii\helpers\Html;
    use yii\helpers\ArrayHelper;

    use common\modules\shop\models\Attribute; 
    use common\modules\shop\models\ProductAttribute;  
?>

<section class="panel full" >
    <header>
        <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend_module_shop', 'attribute_data') ?>
    </header>
    <div class="panel-body">
        <div class="row" style="padding: 10px 0px; max-height:200px; overflow: auto;">
            <?php 
                $i = 0;
                foreach(Attribute::find()->all() as $oAttribute){ 
                $oProductAttribute = ProductAttribute::find()->where(["=", "product_id", $model->id])->andWhere(["=", "attribute_id", $oAttribute->id])->one();
                if(!$oProductAttribute) { $oProductAttribute = new ProductAttribute(); }
            ?>
                <div class="col-3">
                    <?= $form->field($oProductAttribute, '['.$i.']value_id')->dropDownList(ArrayHelper::map($oAttribute->getValues()->all(), 'id', 'name'),['prompt'=>'- wybierz atrybut -'])->label($oAttribute->name) ?>
                    <?= $form->field($oProductAttribute, '['.$i.']product_id')->hiddenInput(['value'=> $model->id])->label(false); ?>
                    <?= $form->field($oProductAttribute, '['.$i.']attribute_id')->hiddenInput(['value'=> $oAttribute->id])->label(false); ?>
                </div>
            <?php $i++; } ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_shop', 'Create') : "Zapisz zmiany", ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
</section>