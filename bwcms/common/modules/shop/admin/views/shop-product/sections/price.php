<?php
    use yii\helpers\Html;
    
    use common\modules\shop\models\PriceList;
    use common\modules\shop\models\PriceProduct;
?>

<?php foreach(PriceList::find()->all() as $oPriceList){ ?>
    <?php 
        $oPricingProduct = PriceProduct::find()->where(["=", "shop_price_id", $oPriceList->id])->andWhere(["=", "product_id", $model->id])->one(); 
        if(!isset($oPricingProduct)) { $oPricingProduct = new PriceProduct(); }
    ?>
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend_module_shop', 'price_data') ?>: <?= $oPriceList->name ?> (<?= $oPriceList->symbol_currency ?>)
        </header>
        <div class="panel-body">
            <div class="row">
                <div class="col-11">
                    <div class="row">
                        <div class="col-3"><?= $form->field($oPricingProduct, "[{$oPriceList->id}]price_netto")->textInput(["class" => "form-control pricing_netto"]) ?></div>
                        <div class="col-3"><?= $form->field($oPricingProduct, "[{$oPriceList->id}]price_brutto")->textInput(["class" => "form-control pricing_brutto"]) ?></div>
                        <div class="col-3"><?= $form->field($oPricingProduct, "[{$oPriceList->id}]old_price_netto")->textInput(["class" => "form-control old_pricing_netto"]) ?></div>
                        <div class="col-3"><?= $form->field($oPricingProduct, "[{$oPriceList->id}]old_price_brutto")->textInput(["class" => "form-control old_pricing_brutto"]) ?></div>
                    </div>
                </div>
                <div class="col-1">
                    <div class="row">
                        <div class="col-12"><?= $form->field($oPricingProduct, "[{$oPriceList->id}]vat")->dropDownList([ 0 => 0, 5 => 5, 8 => 8, 23 => 23], ["class" => "form-control vat"]) ?></div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_shop', 'Create') : "Zapisz zmiany", ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    </section>
<?php } ?>