<?php
    use yii\helpers\Html;
    use yii\helpers\ArrayHelper;
    
    use common\modules\files\models\File;
    use common\modules\gallery\models\Gallery;
    use common\modules\shop\models\Producer;
    use common\modules\shop\models\Category;
    
    $aCats = [];
    foreach(Category::find()->where(["IS", "parent_id", null])->all() as $oCat){
        $aCats[$oCat->id] = $oCat->name;

        $oChildRowset = Category::find()->where(["=", "parent_id", $oCat->id])->all();
        if(count($oChildRowset) > 0){
            foreach($oChildRowset as $oChild){
                $aCats[$oChild->id] = "—| {$oChild->name}";
            }
        }
    }
?>

<div class="row">
     <div class="col-3">
         <section class="panel full" >
             <div class="panel-body" style="text-align: center; font-size: 12px; font-weight: bold; color: #797c87;" >
                 <?php if($model->thumbnail_id){ ?>
                     <img src="/upload/<?= File::find()->where(["=", "id", $model->thumbnail_id])->one()->filename  ?>" style="margin: 0 auto; max-height: 203px;" class="img-responsive"/>
                 <?php } else { ?>
                     <img src="<?= \Yii::$app->request->baseUrl ?>images/no-image.png" style="margin: 0 auto;" class="img-responsive"/>
                     <p>Brak zdjęcia</p>
                 <?php } ?>
             </div>
         </section>
         <?php if($model->thumbnail_id){ ?>
             <p>
                 <?= Html::a("Kadruj zdjęcie", \Yii::$app->request->baseUrl."gallery/gallery/image_crop/?id={$model->thumbnail_id}", ['class' => 'btn btn-primary', "style" => "width: 100%"]) ?>
             </p>
         <?php } ?>
     </div>
     <div class="col-9">
         <section class="panel full" >
             <header>
                 <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend_module_shop', 'basic_data') ?>
             </header>
             <div class="panel-body" >

                 <div class="row">
                     <div class="col-4"><?= $form->field($model, 'name')->textInput(['maxlength' => true])->label("Nazwa produktu") ?></div>
                     <div class="col-4"><?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?></div>
                     <div class="col-4"><?= $form->field($model, 'slug')->textInput(['maxlength' => true])->label("Adres URL") ?></div>
                 </div>

                 <div class="row">
                     <div class="col-3"><?= $form->field($model, 'is_hidden_price')->dropDownList([0 =>  Yii::t('backend', 'no'), 1 =>  Yii::t('backend', 'yes')]) ?></div>
                     <div class="col-3"><?= $form->field($model, 'is_active')->dropDownList([0 =>  Yii::t('backend', 'no'), 1 =>  Yii::t('backend', 'yes')]) ?></div>
                     <div class="col-3"><?= $form->field($model, 'hidden_price_desc')->textInput() ?></div>
                     <div class="col-3"><?= $form->field($model, 'sticker')->textInput(['maxlength' => true])->label("Nazwa flagi produktu") ?></div>

                     <div class="col-4"><?= $form->field($model, 'thumbnail_id')->dropDownList(ArrayHelper::map(File::find()->where(["LIKE", "type", "image"])->all(), 'id', 'name'), ["prompt" => "- wybierz miniature -", "class" => "thumbnail-select"])->label("Wybierz miniaturę") ?></div>
                     <div class="col-4"><?= $form->field($model, 'producer_id')->dropDownList(ArrayHelper::map(Producer::find()->all(), 'id', 'name'), ["prompt" => "- wybierz producenta -"])->label("Producent") ?></div>
                     <div class="col-4"><?= $form->field($model, 'gallery_id')->dropDownList(ArrayHelper::map(Gallery::find()->all(), 'id', 'name'), ["prompt" => "- wybierz galerie -"]) ?></div>
                 </div>

                 <div class="form-group">
                     <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_shop', 'Create') : "Zapisz zmiany", ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                 </div>
             </div>
         </section>
     </div>
 </div>

<section class="panel full" >
    <header>
        <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend_module_shop', 'category_data') ?>
    </header>
    <div class="panel-body">
        <div class="row">
            <div class="col-3"><?= $form->field($model, 'category_id')->dropDownList($aCats) ?></div>
        </div>
        <div class="row">
            <?php foreach(explode(",", $model->add_category_hash) as $iAddCat){ ?>
                <div class="col-3"><?= $form->field($model, '[]add_category_hash')->dropDownList($aCats,['prompt'=>'- wybierz kategorie -', 'options' =>[$iAddCat => ['selected' => true]]]) ?></div>
            <?php } ?>
            <div class="col-3 add-category">Dodaj nową kategorię <img src="<?= \Yii::$app->request->baseUrl ?>images/ico/add.jpg" alt="add"/></div>
        </div>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_shop', 'Create') : "Zapisz zmiany", ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
</section>

<section class="panel full" >
    <header>
        <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Dodatkowe parametry
    </header>
    <div class="panel-body">
        <div class="row">
            <div class="col-4"><?= $form->field($model, 'is_bestseller')->dropDownList([0 =>  Yii::t('backend', 'no'), 1 =>  Yii::t('backend', 'yes')]) ?></div>
            <div class="col-4"><?= $form->field($model, 'is_slider')->dropDownList([0 =>  Yii::t('backend', 'no'), 1 =>  Yii::t('backend', 'yes')]) ?></div>
            <div class="col-4"><?= $form->field($model, 'is_recomended')->dropDownList([0 =>  Yii::t('backend', 'no'), 1 =>  Yii::t('backend', 'yes')]) ?></div>
        </div>
        <div class="row">
            <div class="col-1"><?= $form->field($model, 'color')->textInput(["type" => "color"])->label("Kolor") ?></div>
            <div class="col-3"><?= $form->field($model, 'color_name')->textInput()->label("Kolor - słownie") ?></div>
            <div class="col-1"><?= $form->field($model, 'color2')->textInput(["type" => "color"])->label("Kolor") ?></div>
            <div class="col-3"><?= $form->field($model, 'color_name2')->textInput()->label("Kolor - słownie") ?></div>
            <div class="col-4">
                <div class="form-group">
                    <label class="control-label" for="related">Produky powiązane</label>
                    <select id="related" class="form-control ajax-load-products" name="Product[related][]"></select>
                    <div class="help-block"></div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_shop', 'Create') : "Zapisz zmiany", ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
</section>

<section class="panel full" >
    <header>
        <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend_module_shop', 'description_data') ?>
    </header>
    <div class="panel-body">
        <?= $form->field($model, 'short_description')->textarea(['rows' => 6])->label("Opis skrócony produktu") ?>
        <?= $form->field($model, 'description')->textarea(['rows' => 6])->label("Pełny opis produktu")  ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_shop', 'Create') : "Zapisz zmiany", ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
</section>

<section class="panel full" >
    <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Konfiguracja SEO</header>
    <div class="panel-body" >
        <div class="article-form">
            <div class="row">
                <div class="col-6">
                    <?= $form->field($model, 'seo_title')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-6">
                    <?= $form->field($model, 'seo_keywords')->textInput(['maxlength' => true]) ?>
                </div>
            </div>

            <?= $form->field($model, 'seo_description')->textarea(['rows' => 6]) ?>
        </div>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'create') : "Zapisz zmiany", ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
</section>