<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\hooks\yii2\grid\GridView;
use common\modules\shop\models\Category;

$this->title = "Zarządzaj produktami";

$this->registerJsFile('/bwcms/common/modules/shop/admin/assets/js/product.js', ['depends' => [yii\web\JqueryAsset::className()]]);
?>
<div class="product-index">

    <p>
        <?= Html::a(Yii::t('backend_module_shop', 'Create Product'), ['create'], ['class' => 'btn btn-success', 'style' => 'float: left;']) ?>
        <form enctype="multipart/form-data" id="upload-product-file" style="float: right;">
            <?= Html::a("Wyeksportuj produkty (csv)", ['export'], ['class' => 'btn btn-warning']) ?>
            <label>
                <span class="btn btn-primary">Zaimportuj produkty (csv)</span>
                <input type="file" name="import" id="import-file" accept=".csv" style="display: none;"/>
            </label>
        </form>
    </p>
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <?php if(Yii::$app->request->get('per-page')){ echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>"; } ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>

        <div class="panel-body" >
            <?= GridView::widget([
                'id' => 'products-list',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'rowOptions'=>function($model){
                    if(!$model->is_active){
                        return ['class' => 'inactive-row'];
                    }
                },
                'columns' => [
                    [
                        'class' => 'yii\grid\CheckboxColumn',
                    ],
                    'id',
                    [
                        'attribute' => "category_id",
                        'label' => Yii::t('backend_module_shop', 'Category'),
                        'filter'=> ArrayHelper::map(Category::find()->all(), 'id', 'name'),
                        'format' => 'raw',
                        'value' => function ($data) {
                            if($data->getCategory()){
                                return $data->getCategory()->name;
                            } else {
                                return "";
                            }
                        }
                    ],
                    [
                        'attribute' => 'name',
                        'filterInputOptions' => [ 'placeholder' => 'Wyszukaj...', "class" => "form-control"],
                    ],
                    'code',
                    [
                        'attribute' => "is_active",
                        'filter'=> [1 => "Aktywny", 0 => "Nieaktywny"],
                        'label' => "Status",
                        'format' => 'raw',
                        'value' => function ($data) {
                            return ($data->is_active) ? Yii::t('backend', 'active') : Yii::t('backend', 'inactive');
                        }
                    ],     
                    [
                        'class' => 'common\hooks\yii2\grid\ActionColumn',
                        'template' => '{view} {update} {delete}',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                if($model->is_active){
                                    return Html::a(
                                        '<span class="glyphicon glyphicon-eye-open"></span>',
                                        $url, 
                                        [
                                            'title' => Yii::t('backend', 'deactivate'),
                                            'data-pjax' => '0',
                                        ]
                                    );
                                } else {
                                    return Html::a(
                                        '<span class="glyphicon glyphicon-eye-close"></span>',
                                        $url, 
                                        [
                                            'title' => Yii::t('backend', 'activate'),
                                            'data-pjax' => '0',
                                        ]
                                    );
                                }
                            },
                        ],
                    ],
                ],
            ]); ?>
            <div class="row">
                <div class="col-12 action-bar" data-url="<?= \Yii::$app->request->baseUrl ?>shop-product/shop-product/actionbar/">
                    <span class="action-text">Dla wybranych elementów:</span>

                    <select class="form-control action-select action-action">
                        <option disabled selected>Wybierz akcje</option>
                        <option value="active">Aktywuj</option>
                        <option value="deactive">Dezaktywuj</option>
                        <option value="delete">Usuń</option>
                        <option value="change-category" data-secondary="true">Zmień kategorię</option>
                    </select>

                    <span class="action-secondary">
                        <span class="action-text" style="margin-left: 10px;">na</span>

                        <select class="form-control action-select action-value" style="width: auto;">
                            <option disabled selected>Wybierz kategorię</option>
                            <?php foreach(Category::find()->all() as $oCategory){ ?>
                                <option value="<?= $oCategory->id ?>"><?= $oCategory->name ?></option>
                            <?php } ?>
                        </select>
                    </span>

                    <?= Html::a("Zastosuj", "#", ['class' => 'btn btn-success action-button']) ?>
                </div>
            </div>
            
        </div>
    </section>
</div>
