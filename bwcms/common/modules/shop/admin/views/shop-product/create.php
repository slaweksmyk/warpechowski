<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use common\modules\files\models\File;
use common\modules\gallery\models\Gallery;

use common\modules\shop\models\Category;
use common\modules\shop\models\Producer;
use common\modules\shop\models\PriceList;
use common\modules\shop\models\Attribute; 
use common\modules\shop\models\PriceProduct;
use common\modules\shop\models\ProductAttribute;  

$this->title = "Edytuj produkt: {$model->name}";

$this->registerJsFile('/bwcms/common/modules/shop/admin/assets/js/shop.js');
$this->registerCssFile('/bwcms/common/modules/shop/admin/assets/css/shop.css');

$aCats = [];
foreach(Category::find()->where(["IS", "parent_id", null])->all() as $oCat){
    $aCats[$oCat->id] = $oCat->name;

    $oChildRowset = Category::find()->where(["=", "parent_id", $oCat->id])->all();
    if(count($oChildRowset) > 0){
        foreach($oChildRowset as $oChild){
            $aCats[$oChild->id] = "—| {$oChild->name}";
        }
    }
}
?>

<p>
    <?= Html::a("< Wróć do listy", ['index'], ['class' => 'btn btn-success']) ?>
</p>

<div class="product-form">
    <?php $form = ActiveForm::begin(); ?>
        <section class="panel full" >
            <header>
                <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend_module_shop', 'basic_data') ?>
            </header>
            <div class="panel-body" >
                <div class="row">
                    <div class="col-3"><?= $form->field($model, 'name')->textInput(['maxlength' => true])->label("Nazwa produktu") ?></div>
                    <div class="col-2"><?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?></div>
                    <?= $form->field($model, 'is_active')->hiddenInput(['value'=> 0])->label(false); ?>
                </div>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_shop', 'Create') : Yii::t('backend', 'save_changes'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </section>
    <?php ActiveForm::end(); ?>
</div>
