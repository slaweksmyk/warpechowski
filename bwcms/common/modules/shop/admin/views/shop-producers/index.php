<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;

$this->title = "Zarządzaj producentami";
?>
<div class="producer-index">

    <p>
        <?= Html::a(Yii::t('backend_module_shop', 'Create Producer'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                                        <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
                                        <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
        'columns' => [

                        'id',
            'name',

                        ['class' => 'common\hooks\yii2\grid\ActionColumn'],
                    ],
                ]); ?>
                                </div>
    </section>   
</div>
