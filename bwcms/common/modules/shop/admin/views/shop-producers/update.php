<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\shop\models\Producer */

$this->title = Yii::t('backend_module_shop', 'Update {modelClass}: ', [
    'modelClass' => 'Producer',
]) . $model->name;
?>
<div class="producer-update">

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
        </header>
        <div class="panel-body" >
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </section>   

</div>
