<?php

use yii\helpers\Html;

$this->title = Yii::t('backend_module_shop', 'Create Producer');
?>
<div class="producer-create">

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
        </header>
        <div class="panel-body" >
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </section>  

</div>
