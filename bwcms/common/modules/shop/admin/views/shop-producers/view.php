<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->name;
?>

<p>
    <?= Html::a("< Wróć do listy", ['index'], ['class' => 'btn btn-success']) ?>
</p>

<div class="producer-view">

    <p>
        <?= Html::a(Yii::t('backend_module_shop', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend_module_shop', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend_module_shop', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
        </header>
        <div class="panel-body" >
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
            'name',
            'thumbnail_id',
                ],
            ]) ?>
        </div>
    </section>  

</div>
