<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;

$this->title = "Zarządzaj atrybutami";
?>
<div class="attribute-index">

    <p>
        <?= Html::a("< Wróć", ['index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a("Dodaj wartość atrybutu", ['value-create', "id" => $_GET["id"]], ['class' => 'btn btn-success']) ?>
    </p>
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
            <form id="per-page">
                <span>Ilość na stronę:</span><select name="per-page" class="form-control"><option>20</option><option>50</option><option>100</option><option>200</option><option>500</option></select>
            </form>
        </header>
        <div class="panel-body" >
            <?= 
            GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                'id',
                [
                    "attribute" => 'name',
                    "label" => "Jednostka przeliczeniowa / Nazwa wartości atrybutu"
                ],
                [
                    'class' => 'common\hooks\yii2\grid\ActionColumn',
                    'template' => '{value-update} {value-products} {value-delete}',
                    'buttons' => [
                        'value-update' => function ($url) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-pencil"></span>',
                                $url, 
                                [
                                    'title' => "Edytuj",
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                        'value-products' => function ($url) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-list-alt"></span>',
                                $url, 
                                [
                                    'title' => Yii::t('backend_module_shop', 'values'),
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                        'value-delete' => function ($url) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>',
                                $url, 
                                [
                                    'title' => "Edytuj",
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ],
                ],
            ],
            ]); ?>
        </div>
    </section>   
</div>
