<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;
use yii\helpers\ArrayHelper;
use common\modules\shop\models\Category; 
use common\modules\shop\models\ShopAttributeValues; 

$this->title = "Zarządzaj produktami";
?>
<div class="product-index">
    
    <p>
        <?= Html::a("< Wróć", ['values', 'id' => $oShopAttributeValues->attribute_id], ['class' => 'btn btn-success']) ?>
    </p>

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <?php if(Yii::$app->request->get('per-page')){ echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>"; } ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>

        <div class="panel-body" >
            <?= GridView::widget([
                'id' => 'list',
                'dataProvider' => $dataProvider,
                'rowOptions'=>function($model){
                    if(!$model->is_active){
                        return ['class' => 'inactive-row'];
                    }
                },
                'columns' => [
                    [
                        'class' => 'yii\grid\CheckboxColumn',
                    ],
                    'id',
                    [
                        'attribute' => "category_id",
                        'label' => Yii::t('backend_module_shop', 'Category'),
                        'filter'=> ArrayHelper::map(Category::find()->all(), 'id', 'name'),
                        'format' => 'raw',
                        'value' => function ($data) {
                            if($data->getCategory()){
                                return $data->getCategory()->name;
                            } else {
                                return "";
                            }
                        }
                    ],
                    [
                        'attribute' => 'name',
                        'filterInputOptions' => [ 'placeholder' => 'Wyszukaj...', "class" => "form-control"],
                    ],
                    'code',
                    [
                        'attribute' => "is_active",
                        'label' => "Status",
                        'format' => 'raw',
                        'value' => function ($data) {
                            return ($data->is_active) ? Yii::t('backend', 'active') : Yii::t('backend', 'inactive');
                        }
                    ],     
                    [
                        'class' => 'common\hooks\yii2\grid\ActionColumn',
                        'template' => '{view} {update} {delete}',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                if($model->is_active){
                                    return Html::a(
                                        '<span class="glyphicon glyphicon-eye-open"></span>',
                                        \Yii::$app->request->baseUrl."shop-product/shop-product/view/?id={$model->id}", 
                                        [
                                            'title' => Yii::t('backend', 'deactivate'),
                                            'data-pjax' => '0',
                                        ]
                                    );
                                } else {
                                    return Html::a(
                                        '<span class="glyphicon glyphicon-eye-close"></span>',
                                        \Yii::$app->request->baseUrl."shop-product/shop-product/view/?id={$model->id}", 
                                        [
                                            'title' => Yii::t('backend', 'activate'),
                                            'data-pjax' => '0',
                                        ]
                                    );
                                }
                            },
                            'update' => function ($url, $model) {
                                return Html::a(
                                    '<span class="glyphicon glyphicon glyphicon-pencil"></span>',
                                    \Yii::$app->request->baseUrl."shop-product/shop-product/update/?id={$model->id}", 
                                    [
                                        'title' => "Edytuj",
                                        'data-pjax' => '0',
                                    ]
                                );
                            },
                            'delete' => function ($url, $model) {
                                return Html::a(
                                    '<span class="glyphicon glyphicon glyphicon-trash"></span>',
                                    \Yii::$app->request->baseUrl."shop-product/shop-product/delete/?id={$model->id}", 
                                    [
                                        'title' => "Usuń",
                                        'data-pjax' => '0',
                                    ]
                                );
                            }
                        ],
                    ],
                ],
            ]); ?>
            <div class="row">
                <div class="col-12 action-bar" data-url="<?= \Yii::$app->request->baseUrl ?>shop-product/shop-product/actionbar/">
                    <span class="action-text">Dla wybranych elementów:</span>

                    <select class="form-control no-select2 action-select action-action">
                        <option disabled selected>Wybierz akcje</option>
                        <option value="active">Aktywuj</option>
                        <option value="deactive">Dezaktywuj</option>
                        <option value="delete">Usuń</option>
                        <option value="change-attribute-values" data-secondary="true">Zmień wartość atrybutu</option>
                    </select>

                    <span class="action-secondary">
                        <span class="action-text" style="margin-left: 10px;">na</span>

                        <select class="form-control no-select2 action-select action-value" style="width: auto;">
                            <option disabled selected>Wybierz wartość</option>
                            <?php foreach(ShopAttributeValues::find()->where(["=", "attribute_id", $oShopAttributeValues->attribute_id])->orderBy("name")->all() as $oShopAttributeValues){ ?>
                                <option value="<?= $_GET["id"] ?>_<?= $oShopAttributeValues->id ?>"><?= $oShopAttributeValues->name ?></option>
                            <?php } ?>
                        </select>
                    </span>

                    <?= Html::a("Zastosuj", "#", ['class' => 'btn btn-success action-button']) ?>
                </div>
            </div>
            
        </div>
    </section>
</div>
