<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use common\modules\shop\models\Attribute;

$this->title = Yii::t('backend_module_shop', 'Create Shop Attribute Values');
?>

<p>
    <?= Html::a("< Wróć do listy", ['index'], ['class' => 'btn btn-success']) ?>
</p>

<div class="shop-attribute-values-create">

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
        </header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(); ?>
                <?php $model->attribute_id = $attributeID; ?>
            
                <div class="row">
                    <div class="col-6"><?= $form->field($model, 'attribute_id')->dropDownList(ArrayHelper::map(Attribute::find()->all(), 'id', 'name'),['prompt'=>'- wybierz atrybut -']); ?></div>
                    <div class="col-6"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
                </div>
            
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_shop', 'Create') : "Zapisz zmiany", ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </section>  

</div>
