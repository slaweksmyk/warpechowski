<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('backend_module_shop', 'Create Attribute');
?>

<p>
    <?= Html::a("< Wróć do listy", ['index'], ['class' => 'btn btn-success']) ?>
</p>

<div class="attribute-create">

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
        </header>
        <div class="panel-body" >
            <div class="attribute-form">
                <?php $form = ActiveForm::begin(); ?>
                
                <div class="row">
                    <div class="col-12"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
                </div>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_shop', 'Create') : "Zapisz zmiany", ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </section>  

</div>
