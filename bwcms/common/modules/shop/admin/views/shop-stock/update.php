<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = "Określ status ilościowy";
?>

<p>
    <?= Html::a("< Wróć do listy", ['index'], ['class' => 'btn btn-success']) ?>
</p>

<div class="category-update">

    <div class="stock-form">
        
        <section class="panel full" >
            <header>
                <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
            </header>
            <div class="panel-body" >
                <?php $form = ActiveForm::begin(); ?>
                
                <div class="row">
                    <?= $form->field($model, 'product_id')->hiddenInput()->label(false) ?>
                    <div class="col-6"><?= $form->field($model, 'amount')->textInput()->label("Ilość sztuk") ?></div>
                </div>

                <div class="form-group">
                    <?= Html::submitButton("Zapisz zmiany", ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </section>

    </div>

</div>
