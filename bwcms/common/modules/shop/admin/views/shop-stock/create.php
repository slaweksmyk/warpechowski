<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = "Dodaj status";
?>

<p>
    <?= Html::a("< Wróć do listy", ['index'], ['class' => 'btn btn-success']) ?>
</p>

<div class="shop-delivers-create">

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
        </header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(); ?>
                <div class="row">
                    <div class="col-4"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
                    <div class="col-4"><?= $form->field($model, 'amount_from')->textInput() ?></div>
                    <div class="col-4"><?= $form->field($model, 'amount_to')->textInput() ?></div>
                </div>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_shop', 'Create') : Yii::t('backend', 'save_changes'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </section>  

</div>
