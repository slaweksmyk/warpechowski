<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use common\hooks\yii2\grid\GridView;
use common\modules\shop\models\ShopStocksStates;

$this->registerJsFile('/bwcms/common/modules/shop/admin/assets/js/stock.js', ['depends' => [yii\web\JqueryAsset::className()]]);

$this->title = "Zarządzaj stanami magazynowymi";
?>
<div class="stock-index">
    
    <p>
        <form enctype="multipart/form-data" id="upload-stock-file">
            <?= Html::a("Wyeksportuj stany (csv)", ['export'], ['class' => 'btn btn-success']) ?>
            <label>
                <span class="btn btn-primary">Zaimportuj stany (csv)</span>
                <input type="file" name="import" id="import-file" accept=".csv" style="display: none;"/>
            </label>
        </form>
    </p>

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <?php if(Yii::$app->request->get('per-page')){ echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>"; } ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'rowOptions'=>function($model){
                    if($model->getStockStatus()->one()){
                        return ['style' => 'background-color: '.$model->getStockStatus()->one()->color];
                    }
                },
                'columns' => [
                    'id',
                    [
                        'attribute' => "product_id",
                        'label' => Yii::t('backend_module_shop', 'Product'),
                        'filter'=> [],
                        'filterInputOptions' => ['class' => 'ajax-load-products', 'style' => 'width: 100%;'],
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'width:40%'],
                        'value' => function ($data) {
                            if($data->getProduct()->one()){
                                return $data->getProduct()->one()->name;
                            } else {
                                return "";
                            }
                        }
                    ],
                    [
                        'label' => "Status",
                        'format' => 'raw',
                        'attribute' => "stock_status_id",
                        'filter'=> ArrayHelper::map(ShopStocksStates::find()->all(), 'id', 'name'),
                        'value' => function ($data) {
                            if($data->getStockStatus()->one()){
                                return $data->getStockStatus()->one()->name;
                            } else {
                                return "";
                            }
                        }
                    ],
                    [
                        'label' => "Ilość",
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'width:10%'],
                        'attribute' => "amount",
                    ],     
                    [
                        'class' => 'common\hooks\yii2\grid\ActionColumn',
                        'template' => '{update}',
                    ],
                ],
            ]); ?>
            <div class="row">
                <div class="col-5"><?= Html::a("Zapisz zmiany", ["index"], ['class' => 'btn btn-primary']) ?></div>
                <div class="col-4" style="padding-left: 45px;"><input type="checkbox" style="position: relative; top: 2px;"/> Włącz definicje słowną</div>
                <div class="col-3" style="text-align: right;"><input type="checkbox" style="position: relative; top: 2px;"/> Włącz definicje ilościową</div>
            </div>
            
        </div>
    </section>
    
    <p>
        <?= Html::a("Utwórz status", ["create"], ['class' => 'btn btn-success']) ?>
    </p>
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Moduł edycji nazewnictwa statusów ilościowych
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <?php if(Yii::$app->request->get('per-page')){ echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>"; } ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?= GridView::widget([
                'dataProvider' => $dataProvider2,
                'filterModel' => $searchModel2,
                'rowOptions'=>function($model){
                    if($model->color){
                        return ['style' => 'background-color: '.$model->color];
                    }
                },
                'columns' => [
                    'id',
                    [
                        'attribute' => 'name',
                        'label' => 'Nazwa statusu definiującego stan ilościowy',
                        'headerOptions' => ['style' => 'width:50%'],
                    ],
                    'amount_from',
                    'amount_to', 
                    [
                        'class' => 'common\hooks\yii2\grid\ActionColumn',
                        'template' => '{update-status} {delete-status}',
                        'buttons' => [
                            'update-status' => function ($url, $model) {
                                return Html::a(
                                    '<span class="glyphicon glyphicon-pencil"></span>',
                                    $url, 
                                    [
                                        'title' => "Edytuj",
                                        'data-pjax' => '0',
                                    ]
                                );
                            },
                            'delete-status' => function ($url, $model) {
                                return Html::a(
                                    '<span class="glyphicon glyphicon-trash"></span>',
                                    $url, 
                                    [
                                        'title' => "Usuń",
                                        'data-pjax' => '0',
                                    ]
                                );
                            }   
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </section>
    
</div>
