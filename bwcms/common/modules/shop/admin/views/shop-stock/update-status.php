<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = "Dodaj status";
?>

<p>
    <?= Html::a("< Wróć do listy", ['index'], ['class' => 'btn btn-success']) ?>
</p>

<div class="shop-delivers-create">

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
        </header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(); ?>
                <div class="row">
                    <div class="col-3"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
                    <div class="col-3"><?= $form->field($model, 'amount_from')->textInput() ?></div>
                    <div class="col-3"><?= $form->field($model, 'amount_to')->textInput() ?></div>
                    <div class="col-3"><?= $form->field($model, 'color')->textInput(["type" => "color"])->label("Edytuj kolor") ?></div>
                </div>

                <div class="form-group">
                    <?= Html::submitButton("Zapisz zmiany", ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </section>  

</div>
