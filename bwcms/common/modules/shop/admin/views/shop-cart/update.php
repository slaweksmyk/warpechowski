<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\modules\users\models\User;
use common\hooks\yii2\grid\GridView;

$this->title = 'Zaktualizuj koszyk';
?>
<div class="cart-update">

    <p>
        <?= Html::a('< Wróć  do listy', ['index'], ['class' => 'btn btn-outline-secondary']) ?>
    </p>

    <section class="panel full" >
        <header>
            <img src="/__fBFyVd/images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
        </header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(); ?>

            <div class="row">
                <div class="col-8">
                    <?= $form->field($model, 'user_id')->dropDownList(ArrayHelper::map(User::find()->all(), 'id', 'username'), ["prompt" => "- wybierz użytkownika - "]); ?>
                </div>
                <div class="col-2">
                    <label class="control-label">Łączna kwota netto</label>
                    <input type="text" class="form-control" value="<?= $model->getSumNetto() ?>" readonly/>
                </div>
                <div class="col-2">
                    <label class="control-label">Łączna kwota brutto</label>
                    <input type="text" class="form-control" value="<?= $model->getSumBrutto() ?>" readonly/>
                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Dodaj' : 'Zaktualizuj', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </section>
    
    <p>
        <?= Html::a('Dodaj produkt', ['add', 'id' => $model->id], ['class' => 'btn btn-success pull-right']) ?>
        <br/><br/>
    </p>
    
    <section class="panel full" >
        <header>
            <img src="/__fBFyVd/images/login/logo-gray.png" alt="logo"/> Przedmioty w koszyku
        </header>
        <div class="panel-body" >
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'columns'      => [
                    'id',
                    [
                        'label' => "Przedmiot",
                        'value' => function($model){
                            return $model->getItem()->name;
                        }
                    ],
                    'quantity',
                    [
                        'label' => "Cena za sztuke (netto)",
                        'value' => function($model){
                            return $model->getItem()->getPriceNetto()." ".$model->getItem()->currency;
                        }
                    ], 
                    [
                        'label' => "Cena za sztuke (brutto)",
                        'value' => function($model){
                            return $model->getItem()->getPriceBrutto()." ".$model->getItem()->currency;
                        }
                    ],
                    [
                        'label' => "Cena netto",
                        'value' => function($model){
                            return number_format((float)$model->getItem()->getPriceNetto()*$model->quantity, 2, '.', '')." ".$model->getItem()->currency;
                        }
                    ],
                    [
                        'label' => "Cena brutto",
                        'value' => function($model){
                            return number_format((float)$model->getItem()->getPriceBrutto()*$model->quantity, 2, '.', '')." ".$model->getItem()->currency;
                        }
                    ],     
                    [
                        'label' => "VAT",
                        'value' => function($model){
                            return $model->getItem()->vat."%";
                        }
                    ],     
                    [
                        'class' => 'common\hooks\yii2\grid\ActionColumn',
                        'template' => '{delete-item}',
                        'contentOptions' => ['style' => 'width: 100px;'],
                        'buttons' => [
                            'delete-item' => function ($url) {
                                return Html::a(
                                    '<span class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></span>', $url, [
                                        'title' => Yii::t('backend', 'delete'),
                                        'data-toggle' => "tooltip",
                                        'data-placement' => "bottom",
                                        'data-pjax' => 0,
                                        'data-confirm' => 'Czy na pewno usunąć ten element?',
                                        'data-method' => 'post',
                                    ]
                                );
                            },
                        ],
                    ],
                ],
            ]);
            ?>
        </div>
    </section>
    

</div>
