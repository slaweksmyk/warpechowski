<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\modules\shop\models\Product;

$this->title = 'Dodaj produkt';
?>

<p>
    <?= Html::a('< Wróć do koszyka', ['update', 'id' => $oCart->id], ['class' => 'btn btn-outline-secondary']) ?>
</p>

<section class="panel full" >
    <header>
        <img src="/__fBFyVd/images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
    </header>
    <div class="panel-body" >
        <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <div class="col-4">
                <?= $form->field($oCartItem, 'item_id')->dropDownList(ArrayHelper::map(Product::find()->all(), 'id', 'name'), ["prompt" => "- wybierz produkt - "]); ?>
            </div>
            <div class="col-2">
                <?= $form->field($oCartItem, 'quantity')->textInput(['maxlength' => true, 'min' => 1]) ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton($oCartItem->isNewRecord ? 'Dodaj' : 'Zaktualizuj', ['class' => $oCartItem->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</section>