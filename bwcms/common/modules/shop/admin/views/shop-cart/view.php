<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->id;
?>
<div class="cart-view">

    <p>
        <?= Html::a('< Wróć  do listy', ['index'], ['class' => 'btn btn-outline-secondary']) ?>
    </p>

     <section class="panel full" >
        <header>
            <img src="/__fBFyVd/images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
        </header>
        <div class="panel-body" >
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'session_id',
            'create_date',
            'update_date',
        ],
    ]) ?>
        </div>
    </section>

</div>
