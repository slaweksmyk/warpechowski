<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\modules\users\models\User;

$this->title = 'Dodaj koszyk';
?>
<div class="cart-create">

    <p>
        <?= Html::a('< Wróć do listy', ['index'], ['class' => 'btn btn-outline-secondary']) ?>
    </p>

    <section class="panel full" >
        <header>
            <img src="/__fBFyVd/images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
        </header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(); ?>

            <div class="row">
                <div class="col-6">
                    <?= $form->field($model, 'user_id')->dropDownList(ArrayHelper::map(User::find()->all(), 'id', 'username'), ["prompt" => "- wybierz użytkownika - "]); ?>
                </div>
                <div class="col-6">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Dodaj' : 'Zaktualizuj', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </section>

</div>
