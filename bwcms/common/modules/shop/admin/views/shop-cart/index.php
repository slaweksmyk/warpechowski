<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\hooks\yii2\grid\GridView;
use common\modules\users\models\User;

/* @var $searchModel common\modules\shop\models\CartSearch */

$this->title = 'Zarządzaj koszykami';
?>
<div class="cart-index">

    <p>
        <?= Html::a('Dodaj koszyk', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <section class="panel full" >
        <header>
            <img src="/__fBFyVd/images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <?php if (Yii::$app->request->get('per-page')) {
                        echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>";
                    } ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel'  => $searchModel,
                'columns'      => [
                    'id',
                    [
                        'attribute' => 'user_id',
                        'filter' => ArrayHelper::map(User::find()->all(), 'id', 'username'),
                        'value' => function($model){
                            $oUser = $model->getUser()->one();
                            return $oUser ? $oUser->username : "Osoba niezalogowana";
                        }
                    ],
                    'name',
                    [
                        'label' => 'Produkty',
                        'contentOptions' => ['style' => 'width: 90px;'],
                        'value' => function($model){
                            return $model->getCartItems()->count();
                        }
                    ],
                    [
                        'label' => 'Wartość netto',
                        'value' => function($model){
                            return $model->getSumNetto();
                        }
                    ],    
                    [
                        'label' => 'Wartość brutto',
                        'value' => function($model){
                            return $model->getSumBrutto();
                        }
                    ],
                    [
                        'attribute' => 'create_date',
                        'contentOptions' => ['style' => 'width: 150px;'],
                        'value' => function($model){
                            return date("d.m.Y H:i:s", strtotime($model->create_date));
                        }
                    ],
                    [
                        'attribute' => 'update_date',
                        'contentOptions' => ['style' => 'width: 150px;'],
                        'value' => function($model){
                            return date("d.m.Y H:i:s", strtotime($model->update_date));
                        }
                    ],    
                    ['class' => 'common\hooks\yii2\grid\ActionColumn'],
                ],
            ]);
            ?>
        </div>
    </section>
</div>
