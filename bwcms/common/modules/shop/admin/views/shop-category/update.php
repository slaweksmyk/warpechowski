<?php
$this->title = Yii::t('backend_module_shop', 'Update {modelClass}: ', [
    'modelClass' => 'Category',
]) . $model->name;
?>
<div class="category-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
