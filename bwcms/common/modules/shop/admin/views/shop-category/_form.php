<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\modules\shop\models\Category;

use common\modules\pages\models\PagesSite;

$aCats = [];
foreach(Category::find()->where(["IS", "parent_id", null])->all() as $oCat){
    $aCats[$oCat->id] = $oCat->name;

    $oChildRowset = Category::find()->where(["=", "parent_id", $oCat->id])->all();
    if(count($oChildRowset) > 0){
        foreach($oChildRowset as $oChild){
            $aCats[$oChild->id] = "—| {$oChild->name}";
        }
    }
}
?>

<p>
    <?= Html::a("< Wróć do listy", ['index'], ['class' => 'btn btn-success']) ?>
</p>

<div class="category-form">

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
        </header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(); ?>

            <?php if($model->isNewRecord){ ?>
                <div class="row">
                    <div class="col"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
                    <div class="col">
                        <?= $form->field($model, 'parent_id')->dropDownList($aCats, ['prompt'=>Yii::t('backend_module_shop', '- select_parent -')]) ?>
                    </div>
                    <div class="col">
                        <?= $form->field($model, 'extend_page_id')->dropDownList(ArrayHelper::map(PagesSite::find()->all(), 'id', 'name'), ["prompt" => Yii::t('backend_module_articles', '- select extend -')])->label("Podstrona rozwinięcia"); ?>
                    </div>
                    <div class="col">
                        <?= $form->field($model, 'sort')->textInput(['maxlength' => true, 'type' => 'number']) ?>
                    </div>
                </div>
            <?php } else { ?>
                <div class="row">
                    <div class="col"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
                    <div class="col">
                        <?= $form->field($model, 'parent_id')->dropDownList($aCats, ['prompt'=>Yii::t('backend_module_shop', '- select_parent -')]) ?>
                    </div>
                    <div class="col"><?= $form->field($model, 'icon_id')->textInput() ?></div>
                    <div class="col">
                        <?= $form->field($model, 'extend_page_id')->dropDownList(ArrayHelper::map(PagesSite::find()->all(), 'id', 'name'), ["prompt" => Yii::t('backend_module_articles', '- select extend -')])->label("Podstrona rozwinięcia"); ?>
                    </div>
                    <div class="col">
                        <?= $form->field($model, 'sort')->textInput(['maxlength' => true, 'type' => 'number']) ?>
                    </div>
                </div>
            <?php } ?>

            <?php if(!$model->isNewRecord){ ?>
                <?= $form->field($model, 'short_description')->textarea(['rows' => 6]) ?>
                <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
            <?php } ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_shop', 'Create') : Yii::t('backend', 'save_changes'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </section>

</div>
