<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\shop\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend_module_shop', 'Categories');
?>
<div class="category-index">

    <p>
        <?= Html::a(Yii::t('backend_module_shop', 'Create Category'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <?php if(Yii::$app->request->get('per-page')){ echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>"; } ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    [
                        'label' => Yii::t('backend_module_shop', 'Parent'),
                        'format' => 'raw',
                        'value' => function ($data) {
                            if($data->getParent()){
                                return $data->getParent()->name;
                            } else {
                                return "";
                            }
                        }
                    ],
                    'name',
                    [
                        'attribute' => "id",
                        'label' => Yii::t('backend_module_shop', 'product_number'),
                        'format' => 'raw',
                        'value' => function ($data) {
                            return count($data->getProducts());
                        }
                    ],
                    [
                        'class' => 'common\hooks\yii2\grid\ActionColumn',
                        'template' => '{view} {update} {delete}',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                if($model->is_active){
                                    return Html::a(
                                        '<span class="glyphicon glyphicon-eye-open"></span>',
                                        $url, 
                                        [
                                            'title' => Yii::t('backend', 'deactivate'),
                                            'data-pjax' => '0',
                                        ]
                                    );
                                } else {
                                    return Html::a(
                                        '<span class="glyphicon glyphicon-eye-close"></span>',
                                        $url, 
                                        [
                                            'title' => Yii::t('backend', 'activate'),
                                            'data-pjax' => '0',
                                        ]
                                    );
                                }
                            }
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </section>
    
</div>
