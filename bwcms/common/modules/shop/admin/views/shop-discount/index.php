<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;

use common\modules\promotion\models\PromotionsList;

$this->title = "Zarządzaj kodami rabatowymi";
?>
<div class="shop-discount-codes-index">

    <p>
        <?= Html::a(Yii::t('backend_module_shop', 'Create Shop Discount Codes'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a("Generuj kody rabatowe", ['generate'], ['class' => 'btn btn-primary']) ?>
    </p>
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    'code',
                    [
                        'attribute' => 'type',
                        'label' => Yii::t('backend_module_shop', 'type'),
                        'format' => 'raw',
                        'filter'=> [1 => "Procentowy", 0 => "Kwotowy"],
                        'value' => function ($data) {
                            switch($data["type"]){
                                case "0":
                                    return "Kwotowy";
                                case "1":
                                    return "Procentowy";
                            }
                        }
                    ],
                    [
                        'attribute' => 'value',
                        'label' => 'Wartość jednostkowa'
                    ],
                    [
                        'attribute' => "is_used",
                        'format' => 'raw',
                        'filter'=> [1 => Yii::t('backend', 'yes'), 0 => Yii::t('backend', 'no')],
                        'value' => function ($data) {
                            switch($data["is_used"]){
                                case "0":
                                    return Yii::t('backend', 'no');
                                case "1":
                                    return Yii::t('backend', 'yes');
                            }
                        }
                    ],
                    [
                        'attribute' => "promotion_id",
                        'label' => "Przypisana promocja",
                        'format' => 'raw',
                        'value' => function ($data) {
                            $oPromotion = PromotionsList::find()->where(["=", "id", $data->promotion_id])->one();
                            
                            if(!is_null($oPromotion)){
                                return $oPromotion->name;
                            } else {
                                return;
                            }
                        }
                    ],
                    ['class' => 'common\hooks\yii2\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </section>   
</div>
