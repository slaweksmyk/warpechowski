<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->id;
?>

<p>
    <?= Html::a("< Wróć do listy", ['index'], ['class' => 'btn btn-success']) ?>
</p>

<div class="shop-discount-codes-view">

    <p>
        <?= Html::a(Yii::t('backend_module_shop', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend_module_shop', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend_module_shop', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
        </header>
        <div class="panel-body" >
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
            'code',
            'type',
            'restriction_type',
            'category_hash:ntext',
            'product_hash:ntext',
            'active_from',
            'active_to',
            'is_one_time:datetime',
            'is_used',
                ],
            ]) ?>
        </div>
    </section>  

</div>
