<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use common\modules\shop\models\Category;
use common\modules\promotion\models\PromotionsList;

$this->registerJsFile('/bwcms/common/modules/shop/admin/assets/js/generator.js');
$this->registerCssFile('/bwcms/common/modules/shop/admin/assets/css/generator.css');

$this->title = "Generator kodów rabatowych";
?>

<p>
    <?= Html::a("< Wróć do listy", ['index'], ['class' => 'btn btn-success']) ?>
</p>

<div class="shop-discount-codes-update">

    <?php $form = ActiveForm::begin(); ?>
        <section class="panel full" >
            <header>
                <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/>Konfiguracja generatora         
            </header>
            <div class="panel-body" >

                <div class="row">
                    <div class="col-3"><?= $form->field($generator, 'amount')->textInput(["type" => "number", "min" => 0])->label("Ilość kodów") ?></div>
                    <div class="col-3"><?= $form->field($generator, 'promotion_id')->dropDownList(ArrayHelper::map(PromotionsList::find()->all(), 'id', 'name'), ["prompt" => "- wybierz promocję -"])->label("Przypisz do promocji"); ?></div>
                    <div class="col-3 generator-hide"><?= $form->field($generator, 'promotion_subtype')->dropDownList(["first_buy_code" => "Za pierwsze zakupy", "newsletter_sign_code" => "Za zapis do newslettera", "register_code" => "Za rejestracje konta", "facebook_synch_code" => "Za synchronizacje z facebookiem", "order_discount_code" => "Za złożenie zamówienia"], ["prompt" => "- wybierz rodzaj -"])->label("Rodzaj promocji konfiguratora"); ?></div>
                </div>
            </div>
        </section>
    
        <section class="panel full" >
            <header>
                <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Konfiguracja kodów          
            </header>
            <div class="panel-body" >

                <div class="row">
                    <div class="col-3"><?= $form->field($model, 'value')->textInput(['maxlength' => true])->label("Wartość kodu") ?></div>
                    <div class="col-3"><?= $form->field($model, 'type')->dropDownList([0 =>  Yii::t('backend_module_shop', 'amount'), 1 =>  Yii::t('backend_module_shop', 'percent')]) ?></div>
                    <div class="col-2"><?= $form->field($model, 'active_from')->textInput(["class" => "datepicker form-control"]) ?></div>
                    <div class="col-2"><?= $form->field($model, 'active_to')->textInput(["class" => "datepicker form-control"]) ?></div>
                </div>
                
                <div class="row">
                    <div class="col-3"><?= $form->field($model, 'restriction_type')->dropDownList([0 =>  Yii::t('backend_module_shop', 'category'), 1 =>  Yii::t('backend_module_shop', 'product')], ['prompt'=>'- brak -']) ?></div>
                    <div class="col-3 generator-rest-1"><?= $form->field($generator, 'restriction_product[]')->dropDownList([], ["class" => "form-control ajax-load-products", "placeholder" => "Dodaj kategorię"])->label("Lista produktów") ?></div>
                    <div class="col-3 generator-rest-0"><?= $form->field($generator, 'restriction_category[]')->dropDownList([], ["class" => "form-control ajax-load-product-categories"])->label("Lista kategorii") ?></div>
                </div>

                <div class="form-group">
                    <?= Html::submitButton("Generuj", ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </section>


    <?php ActiveForm::end(); ?>
</div>
