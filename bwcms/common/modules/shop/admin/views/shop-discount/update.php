<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use common\modules\shop\models\Product;
use common\modules\shop\models\Category;

$this->registerJsFile('/bwcms/common/modules/shop/admin/assets/js/discounts.js');

$this->title = Yii::t('backend_module_shop', 'Update {modelClass}: ', [
    'modelClass' => 'Shop Discount Codes',
]) . $model->code;
?>

<p>
    <?= Html::a("< Wróć do listy", ['index'], ['class' => 'btn btn-success']) ?>
</p>

<div class="shop-discount-codes-update">

    <?php $form = ActiveForm::begin(); ?>
        <section class="panel full" >
            <header>
                <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
            </header>
            <div class="panel-body" >

                <div class="row">
                    <div class="col-4"><?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?></div>
                    <div class="col-4"><?= $form->field($model, 'value')->textInput(['maxlength' => true]) ?></div>
                    <div class="col-2"><?= $form->field($model, 'active_from')->textInput(["class" => "datepicker form-control"]) ?></div>
                    <div class="col-2"><?= $form->field($model, 'active_to')->textInput(["class" => "datepicker form-control"]) ?></div>
                </div>

                <div class="row">
                    <div class="col-3"><?= $form->field($model, 'type')->dropDownList([0 =>  Yii::t('backend_module_shop', 'amount'), 1 =>  Yii::t('backend_module_shop', 'percent')]) ?></div>
                    <div class="col-3"><?= $form->field($model, 'restriction_type')->dropDownList([0 =>  Yii::t('backend_module_shop', 'category'), 1 =>  Yii::t('backend_module_shop', 'product')], ['prompt'=>'- brak -']) ?></div>
                    <div class="col-3"><?= $form->field($model, 'is_one_time')->dropDownList([0 =>  Yii::t('backend', 'no'), 1 =>  Yii::t('backend', 'yes')]) ?></div>
                    <div class="col-3"><?= $form->field($model, 'is_once_per_user')->dropDownList([0 =>  Yii::t('backend', 'no'), 1 =>  Yii::t('backend', 'yes')]) ?></div>
                </div>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_shop', 'Create') : Yii::t('backend', 'save_changes'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </section> 

        <section class="panel full hidden" id="restrict-category">
            <header>
                <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Ograniczenie na kategorie         
            </header>
                <div class="panel-body">
                    <div class="row" style="padding: 10px 0px; max-height:200px; overflow: auto;">
                        <?php foreach(Category::find()->all() as $oCategory){ ?>
                            <div class="col-3">
                                <label><input type="checkbox" name="aCategoryRest[]" value="<?= $oCategory->id ?>" <?php if(in_array($oCategory->id, $aCategoryRest)){ ?>checked<?php } ?>/> <?= $oCategory->name ?></label>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_shop', 'Create') : Yii::t('backend', 'save_changes'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                </div>
        </section>
    
        <section class="panel full hidden" id="restrict-product">
            <header>
                <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Ograniczenie na produkty         
            </header>
                <div class="panel-body">
                    <div class="row" style="padding: 10px 0px; max-height:200px; overflow: auto;">
                        <?php foreach(Product::find()->all() as $oProduct){ ?>
                            <div class="col-3">
                                <label><input type="checkbox" name="aProductRest[]" value="<?= $oProduct->id ?>" <?php if(in_array($oProduct->id, $aProductRest)){ ?>checked<?php } ?>/> <?= $oProduct->name ?></label>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_shop', 'Create') : Yii::t('backend', 'save_changes'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                </div>
        </section>

    <?php ActiveForm::end(); ?>
</div>
