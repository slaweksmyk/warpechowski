$(document).ready(function(){
   
    $("#shopdiscountcodes-restriction_type").change(function(){
        var val = parseInt($(this).val());

        if(val === 1){
            $(".generator-rest-1").css("opacity", 1).css("position", "initial");
            $(".generator-rest-0").css("opacity", 0).css("position", "absolute");
        } else if(val === 0){
            $(".generator-rest-1").css("opacity", 0).css("position", "absolute");
            $(".generator-rest-0").css("opacity", 1).css("position", "initial");
        } else {
            $(".generator-rest-1").css("opacity", 0).css("position", "absolute");
            $(".generator-rest-0").css("opacity", 0).css("position", "absolute");
        }
    });
    
    $("#codegenerator-promotion_id").change(function(){
        var val = $(this).val();

        if(val == 4){
            $(".generator-hide").css("opacity", 1);
        } else {
            $(".generator-hide").css("opacity", 0);
        }
    });
    
});

