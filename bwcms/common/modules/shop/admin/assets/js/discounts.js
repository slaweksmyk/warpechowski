$(document).ready(function(){

    $("#shopdiscountcodes-restriction_type").change(function(){
        checkActiveSection();
    });
    
    function checkActiveSection(){
        $("#restrict-category").addClass("hidden");
        $("#restrict-product").addClass("hidden");

        switch($("#shopdiscountcodes-restriction_type").val()){
            
            case "0":
                $("#restrict-category").removeClass("hidden");
                break;
                
            case "1":
                $("#restrict-product").removeClass("hidden");
                break;
                
        }
    }
    checkActiveSection();

});