$(document).ready(function(){
   
   $(".add-category").click(function(){
      $(this).prev().clone().insertBefore($(this)).find("label").html("&nbsp;");
   });
   
   $(".pricing_netto").change(function(){
       var parent = $(this).parent().parent().parent().parent().parent();
       
       var netto = parseFloat($(this).val());
       var vat = parseFloat(parent.find(".vat").val());
       var tax = netto*(vat/100);
       
       var brutto = Math.round((netto+tax)* 100) / 100;

       parent.find(".pricing_brutto").val(brutto);
   });
   
   $(".pricing_brutto").change(function(){
       var parent = $(this).parent().parent().parent().parent().parent();
       
       var brutto = parseFloat($(this).val());
       var vat = parseFloat(parent.find(".vat").val());
       var tax = brutto*(vat/100);
       
       var netto = Math.round((brutto-tax)* 100) / 100;

       parent.find(".pricing_netto").val(netto);
   });
   
});