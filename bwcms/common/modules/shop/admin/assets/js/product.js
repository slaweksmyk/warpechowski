$(document).ready(function(){
   
   $("#import-file").change(function(){
        var file = this.files[0];
       
        $.ajax({
            url: $("#homeDir").text()+"/shop-product/shop-product/import/",
            type: 'POST',
            
            data: new FormData($('#upload-product-file')[0]),
            cache: false,
            contentType: false,
            processData: false
        }).done(function() {
            location.reload();
        });
   });
   
});