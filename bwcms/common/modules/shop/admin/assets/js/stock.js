$(document).ready(function(){
   
   $("#import-file").change(function(){
        var file = this.files[0];
       
        $.ajax({
            url: $("#homeDir").text()+"/shop-stock/shop-stock/import/",
            type: 'POST',
            
            data: new FormData($('#upload-stock-file')[0]),
            cache: false,
            contentType: false,
            processData: false
        }).done(function() {
            location.reload();
        });
   });
   
});