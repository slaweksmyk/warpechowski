<?php
namespace common\modules\import\admin\controllers;

use Yii;
use yii\web\Controller;

use common\modules\files\models\File;
use common\modules\import\models\Wordpress;

/**
 * LayoutController implements the CRUD actions for PagesSiteLayout model.
 */
class ImportController extends Controller
{
    public $dbFrom, $oModel = null;
    public $aLog, $aPost = [];
    public $isError = false;
    
    /* Config */
    public $iLimit = 100; // Limit na cykl
    public $iOffset = 0; // Offset
    public $iAmountLast = 9999999; // Ilość ostatnich rekordów
    public $aPlugins = ["ICL"]; // Only for wordpress custom plugins
    // Support for: ICL Translations
    
    public function actionIndex()
    {
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        
        if(Yii::$app->request->isPost){
            $this->aPost = Yii::$app->request->post();

            /* Connect to database */
            $this->establishConnection();

            if(!$this->isError){
                switch($this->aPost["type"]){
                    case "1":
                        $this->oModel = new Wordpress($this);
                }
            }
            
            return $this->render('end', [
                'aLog' => $this->aLog
            ]);
        } else {
            return $this->render('start', []);
        } 
    }
    
    private function establishConnection()
    {
        $this->dbFrom = new \yii\db\Connection([
            'dsn' => "mysql:host={$this->aPost["host"]};dbname={$this->aPost["database"]};port={$this->aPost["port"]}",
            'username' => $this->aPost["login"],
            'password' => $this->aPost["password"],
        ]);
        
        try {
            $this->dbFrom->open();
            $this->dbFrom->createCommand("SET NAMES utf8")->execute();
            $this->displayMessage(true, "success", "Baza danych", "Nawiązano połączenie z: <b>{$this->aPost["host"]}:{$this->aPost["port"]}</b> z bazą: <b>{$this->aPost["database"]}</b>");
        } catch (yii\db\Exception $e) {
            $this->displayMessage(false, "error", $e->getName(), $e->getMessage());
            $this->isError = true;
        }
    }
    
    /* Universal */
    
    public function uploadFile($filename, $name) 
    {
        $aFileData = pathinfo($filename);

        if (isset($aFileData["extension"])) {
            $hash = base_convert(time(), 10, 36);
            $file_name = preg_replace("/[^a-zA-Z0-9.]/", "", $aFileData["filename"]);
            $save_name = $hash . "_" . $file_name . '.' . $aFileData["extension"];

            $path = Yii::getAlias('@root/upload/') . date("Y-m-d") . "/";
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }
            
            $oFile = null;
            if($filename != ""){
                $oFile = File::find()->where(["=", "old_filepath", $filename])->one();
            }
            if(is_null($oFile)){
                copy($filename, $path . $save_name);

                $oFile = new File();
                $oFile->created_at = date("Y-m-d");
                $oFile->category_id = 57;
                $oFile->is_crop = 0;
                $oFile->name = $name;
                $oFile->filename = date("Y-m-d") . "/" . $save_name;
                $oFile->type = mime_content_type($filename);
                $oFile->size = filesize($filename);
                $oFile->old_filepath = $filename;
                $oFile->save();
            }
            return $oFile->id;
        }
    }

    public function displayMessage($isSuccess, $sLabelText, $sName, $sMessage, $iAmount = null)
    {
        $this->aLog[] = [
            "amount" => $iAmount,
            "type" => ["label" => $isSuccess ? "success" : "danger", "message" => $sLabelText],
            "name" => $sName,
            "message" => $sMessage
        ];
    }
    
}
