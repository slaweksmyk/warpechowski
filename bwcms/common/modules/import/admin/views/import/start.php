<?php
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
    
    $this->registerJsFile('/bwcms/common/modules/import/admin/assets/js/script.js');
?>

<section class="panel full">
    <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"> Import danych</header>
    <div class="panel-body">
        <?php $form = ActiveForm::begin() ?>
            <div class="row">
                <div class="col-3">
                    <div class="form-group">
                        <label class="control-label" for="import-type">Rodzaj</label>
                        <?= Html::dropDownList('type', null, [1 => "Wordpress"], ["prompt" => "- wybierz typ -", "id" => "import-type", "class" => "form-control"]) ?>
                    </div>
                </div>
                <div class="col-2">
                    <label class="control-label">Host</label>
                    <?= Html::input("text", "host", null, ["class" => "form-control"]) ?>
                </div>
                <div class="col-1">
                    <label class="control-label">Port</label>
                    <?= Html::input("text", "port", "3306", ["class" => "form-control"]) ?>
                </div>
                <div class="col-3">
                    <label class="control-label">Login</label>
                    <?= Html::input("text", "login", null, ["class" => "form-control"]) ?>
                </div>
                <div class="col-3">
                    <label class="control-label">Hasło</label>
                    <?= Html::input("password", "password", null, ["class" => "form-control"]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    <label class="control-label">Baza danych</label>
                    <?= Html::input("text", "database", null, ["class" => "form-control"]) ?>
                </div>
                <div class="col-8">
                    <label class="control-label">Ścieżka do katalogu z uploadem</label>
                    <?= Html::input("text", "upload_dir", null, ["class" => "form-control"]) ?>
                </div>
            </div>
            <div class="row hidden-inputs on-wordpress" style="display: none; margin-top: 10px;">
                <div class="col-2">
                    <label class="control-label">Prefix bazy danych</label>
                    <?= Html::input("text", "prefix", null, ["class" => "form-control"]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-12" style="margin-top: 20px;">
                    <?= Html::submitInput("Importuj", ["class" => "btn btn-primary"]) ?>
                </div>
            </div>
        <?php ActiveForm::end() ?>
    </div>
</section>