<section class="panel full">
    <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"> Log komunikatów</header>
    <div class="panel-body">
        <?php foreach($aLog as $index => $aValues){ ?>
            <div class="row">
                <div class="col-12" style="border-bottom: 1px solid #f0f0f0; padding: 5px 15px; font-size: 12px;">
                    <?= (isset($aValues["type"]) && is_array($aValues["type"])) ? "<span class='label label-{$aValues["type"]["label"]}'>{$aValues["type"]["message"]}</span>" : '' ?>
                    <span style="display: inline-block; position: relative; top: 2px;">
                        [<?= $aValues["name"] ?>] 
                        <?= $aValues["message"] ?> 
                    </span>
                    <?= isset($aValues["amount"]) ? "<span class='badge' style='font-weight: normal;'>{$aValues["amount"]}</span>" : '' ?>
                </div>
            </div>
        <?php } ?>
    </div>
</section>