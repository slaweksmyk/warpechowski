<?php
namespace common\modules\import\admin;

/**
 * layout module definition class
 */
class Import extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\import\admin\controllers';
    public $defaultRoute = 'import';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
