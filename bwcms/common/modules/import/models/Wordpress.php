<?php
namespace common\modules\import\models;

use Yii;
use common\modules\tags\models\Tag;
use common\modules\authors\models\Author;
use common\modules\tags\models\TagArticle;
use common\modules\gallery\models\Gallery;
use common\modules\articles\models\Article;
use common\modules\gallery\models\GalleryItems;
use common\modules\articles\models\Article_i18n;
use common\modules\categories\models\ArticlesCategories;


class Wordpress extends \yii\base\Model
{
    private $iTempSuccess = 0;
    private $iTempErrors = 0;
    private $iLimit, $iOffset, $iAmountLast;
    
    private $isRunning = true;
    private $oController = null;
    
    private $sOldPath = "/home/web/web-74/WWW/web-393/portal/wp-content/uploads/";
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

        ];
    }
    
    public function __construct(&$oController, $config = array()) {
        $this->oController = $oController;
        $this->iLimit      = $oController->iLimit;
        $this->iOffset     = $oController->iOffset;
        $this->iAmountLast = $oController->iAmountLast;
        
        $this->executeImport();
        
        return parent::__construct($config);
    }
    
    private function executeImport()
    {
        /* Get categories */
        $this->parseCategories();
        
        /* Get articles */
        $this->parseArticles();
        
        /* Get tags */
        $this->parseTags();
        
        /* Asign tags to articles */
        $this->asignTags();
        
        /* Get galleries */
        $this->getGalleries();
        
        /* Get authors */
        $this->getAuthors();
        
        if(in_array("ICL", $this->oController->aPlugins)){
            $this->parseTranslations();
        }
        
    }
    
    private function getAuthors()
    {
        $oArticleRowset = Article::find()->where(["IS NOT", "old_id", NULL])->all();
        foreach($oArticleRowset as $oArticle){
            $aPost = $this->oController->dbFrom->createCommand("SELECT * FROM `mk_posts` WHERE `ID` = {$oArticle->old_id}")->queryOne();
            $this->parseAuthor($oArticle, $aPost["post_author"]);
        }
        
        if($this->iTempSuccess > 0){
            $this->oController->displayMessage(true, "success", "Autorzy", "Pomyślnie dodano autorów", $this->iTempSuccess);
            $this->iTempSuccess = 0;
        }
        
        if($this->iTempErrors) {
            $this->oController->displayMessage(false, "error", "Autorzy", "Problem z dodawaniem autorów", $this->iTempErrors);
            $this->iTempErrors  = 0;
        }
    }
    
    private function parseAuthor($oArticle, $sAuthorID)
    {
        $aAuthor = $this->oController->dbFrom->createCommand("SELECT * FROM `mk_users` WHERE `ID` = {$sAuthorID}")->queryOne();
        
        $sFirstname = $aAuthor["display_name"];
        $sSurname = "";
        
        $aRaw = explode(" ", $aAuthor["display_name"]);
        if(count($aRaw) > 1){
            $sFirstname = $aRaw[0];
            $sSurname = $aRaw[1];
        }
        
        $oAuthor = Author::find()->where(["=", "firstname", $sFirstname])->andWhere(["=", "surname", $sSurname])->one();
        if(is_null($oAuthor)){
            $oAuthor = new Author();
        }
        $oAuthor->firstname = $sFirstname;
        $oAuthor->surname = $sSurname;
        $oAuthor->title = $aAuthor["user_email"];
        $bStatus = $oAuthor->save();
        
        if($bStatus){
            $oArticle->author_id = $oAuthor->id;
            $bStatus = $oArticle->save();
        }
        
        $bStatus ? $this->iTempSuccess++ : $this->iTempErrors++;
    }
    
    private function getGalleries()
    {
        $oArticleRowset = Article::find()->where(["IS NOT", "old_id", NULL])->all();
        foreach($oArticleRowset as $oArticle){
            preg_match('~\[gallery.+ids="\K([^"]*)~', $oArticle->full_description, $matches, PREG_OFFSET_CAPTURE, 0);
            if(count($matches) > 0 && isset($matches[1][0])){
                $this->parseGallery($oArticle, explode(",", $matches[1][0]));
            }
        }
        
        if($this->iTempSuccess > 0){
            $this->oController->displayMessage(true, "success", "Galerie", "Pomyślnie dodano galerie", $this->iTempSuccess);
            $this->iTempSuccess = 0;
        }
        
        if($this->iTempErrors) {
            $this->oController->displayMessage(false, "error", "Galerie", "Problem z dodawaniem galerii", $this->iTempErrors);
            $this->iTempErrors  = 0;
        }
    }
    
    private function parseTranslations()
    {
        $oArticleRowset = Article::find()->where(["IS NOT", "old_id", NULL])->all();
        foreach($oArticleRowset as $oArticle){
            $aLanguageMeta = $this->oController->dbFrom->createCommand("SELECT * FROM `mk_icl_translations` WHERE `element_id` = {$oArticle->old_id}")->queryOne();
            $aLanguageVariants = $this->oController->dbFrom->createCommand("SELECT * FROM `mk_icl_translations` WHERE `trid` = {$aLanguageMeta['trid']} AND `source_language_code` IS NOT NULL")->queryAll();
            foreach($aLanguageVariants as $aLanguageVariant){
                $aPost = $this->oController->dbFrom->createCommand("SELECT * FROM `mk_posts` WHERE `ID` = {$aLanguageVariant['element_id']}")->queryOne();
                $this->addTranslation($oArticle, $aPost, $aLanguageVariant["language_code"]);
            }
        }
        
        if($this->iTempSuccess > 0){
            $this->oController->displayMessage(true, "success", "ICL", "Pomyślnie dodano tłumaczenia", $this->iTempSuccess);
            $this->iTempSuccess = 0;
        }
        
        if($this->iTempErrors) {
            $this->oController->displayMessage(false, "error", "ICL", "Problem z dodawaniem tłumaczeń", $this->iTempErrors);
            $this->iTempErrors  = 0;
        }
    }
    
    private function addTranslation($oArticle, $aPost, $sLangCode)
    {
        if($sLangCode == "en") { $sLangCode = "en-US"; }
        if($sLangCode == "ru") { $sLangCode = "ru-RU"; }
        
        $Articlei18n = Article_i18n::find()->where(["=", "article_id", $oArticle->id])->andWhere(["=", "language", $sLangCode])->one();
        if(is_null($Articlei18n)){
            $Articlei18n = new Article_i18n();
        }
        $Articlei18n->article_id = $oArticle->id;
        $Articlei18n->language = $sLangCode;
        $Articlei18n->title = $aPost["post_title"];
        $Articlei18n->short_description = $this->parseText($aPost["post_excerpt"]);
        $Articlei18n->full_description  = $this->parseText($aPost["post_content"]);
        $Articlei18n->full_description  = preg_replace('/\[gallery[^]]*\]/', '', $Articlei18n->full_description);
        $bStatus = $Articlei18n->save();

        $bStatus ? $this->iTempSuccess++ : $this->iTempErrors++;
    }
    
    private function asignTags()
    {
        $oArticleRowset = Article::find()->where(["IS NOT", "old_id", NULL])->all();
        foreach($oArticleRowset as $oArticle){
            $aTags = $this->oController->dbFrom->createCommand("SELECT * FROM `mk_term_relationships` LEFT JOIN `mk_term_taxonomy` ON `mk_term_taxonomy`.`term_taxonomy_id` = `mk_term_relationships`.term_taxonomy_id WHERE `object_id` = {$oArticle->old_id} AND mk_term_taxonomy.taxonomy = 'post_tag'")->queryAll();
        
            foreach($aTags as $aTag){
                $this->asignTag($oArticle, $aTag);
            }
            
        }
        
        if($this->iTempSuccess > 0){
            $this->oController->displayMessage(true, "success", "Tagi", "Pomyślnie przypisano tagi do artykułów", $this->iTempSuccess);
            $this->iTempSuccess = 0;
        }
        
        if($this->iTempErrors) {
            $this->oController->displayMessage(false, "error", "Tagi", "Problem z przypisaniem tagów do artykułów", $this->iTempErrors);
            $this->iTempErrors  = 0;
        }
    }
    
    private function asignTag($oArticle, $aTag)
    {
        $oTag = Tag::find()->where(["=", "old_id", $aTag["term_id"]])->one();
        if(!is_null($oTag)){
            $oArticleTag = TagArticle::find()->where(["=", "tag_id", $oTag->id])->andWhere(["=", "article_id", $oArticle->id])->one();
            if(is_null($oArticleTag)){
                $oArticleTag = new TagArticle();
            }
            $oArticleTag->article_id = $oArticle->id;
            $oArticleTag->tag_id = $oTag->id;
            $bStatus = $oArticleTag->save();
            
            $bStatus ? $this->iTempSuccess++ : $this->iTempErrors++;
        }
    }
    
    private function parseTags()
    {
        $aTaxonomy = $this->oController->dbFrom->createCommand("SELECT * FROM `mk_term_taxonomy` WHERE `taxonomy` LIKE 'post_tag' ORDER BY `term_id` ASC")->queryAll();
        foreach($aTaxonomy as $aData){
            $aTag = $this->oController->dbFrom->createCommand("SELECT * FROM `mk_terms` WHERE `term_id` = {$aData['term_id']}")->queryOne();
            $this->parseTag($aTag);
        }
        
        if($this->iTempSuccess > 0){
            $this->oController->displayMessage(true, "success", "Tagi", "Pomyślnie zaimportowano tagi", $this->iTempSuccess);
            $this->iTempSuccess = 0;
        }
        
        if($this->iTempErrors) {
            $this->oController->displayMessage(false, "error", "Tagi", "Problem z importem tagów", $this->iTempErrors);
            $this->iTempErrors  = 0;
        }
    }
    
    private function parseTag($aTag)
    {
        $oTag = Tag::find()->where(["=", "old_id", $aTag["term_id"]])->one();
        if(is_null($oTag)){
            $oTag = new Tag();
        }
        
        $oTag->old_id = $aTag["term_id"];
        $oTag->name = $aTag["name"];
        $bStatus = $oTag->save();
        
        $bStatus ? $this->iTempSuccess++ : $this->iTempErrors++;
    }
    
    private function parseCategories()
    {
        $aCategories = $this->oController->dbFrom->createCommand("SELECT * FROM `mk_term_taxonomy` WHERE `taxonomy` LIKE 'category' ORDER BY `term_id` ASC")->queryAll();
        foreach($aCategories as $aData){
            $aCategory = $this->oController->dbFrom->createCommand("SELECT * FROM `mk_terms` WHERE `term_id` = {$aData['term_id']}")->queryOne();
            $this->parseCategory($aData, $aCategory);
        }
        
        if($this->iTempSuccess > 0){
            $this->oController->displayMessage(true, "success", "Kategorie", "Pomyślnie zaimportowano kategorie", $this->iTempSuccess);
            $this->iTempSuccess = 0;
        }
        
        if($this->iTempErrors) {
            $this->oController->displayMessage(false, "error", "Kategorie", "Problem z importem kategorii", $this->iTempErrors);
            $this->iTempErrors  = 0;
        }
    }
    
    private function parseArticles()
    {
        while($this->isRunning) {
            $aArticleRowset = $this->oController->dbFrom->createCommand("SELECT `mk_posts`.* FROM `mk_posts` LEFT JOIN `mk_icl_translations` ON `mk_posts`.ID = `mk_icl_translations`.element_id WHERE `post_status` LIKE 'publish' AND `post_type` LIKE 'post' AND `mk_icl_translations`.language_code = 'pl' ORDER BY `ID` DESC LIMIT {$this->iOffset},{$this->iLimit}")->queryAll();

            if (count($aArticleRowset) > 0 && $this->iAmountLast >= $this->iOffset) {
                foreach ($aArticleRowset as $aArticle) {
                    $this->parseArticle($aArticle);
                }
            } else {
                $this->isRunning = false;
                break;
            }

            $this->iOffset += $this->iLimit;
        }
        
        if($this->iTempSuccess > 0){
            $this->oController->displayMessage(true, "success", "Artykuły", "Pomyślnie zaimportowano artykuły", $this->iTempSuccess);
            $this->iTempSuccess = 0;
        }
        
        if($this->iTempErrors) {
            $this->oController->displayMessage(false, "error", "Artykuły", "Problem z importem artykułów", $this->iTempErrors);
            $this->iTempErrors  = 0;
        }
        
    }
    
    private function parseCategory($aData, $aCategory)
    {
        $oCategory = ArticlesCategories::find()->where(["=", "old_id", $aCategory["term_id"]])->one();
        if(is_null($oCategory)){
            $oCategory = new ArticlesCategories();
        }

        $oCategory->old_id = $aCategory["term_id"];
        $oCategory->name = $aCategory["name"];
        $oCategory->slug = $aCategory["slug"];
        $oCategory->is_active = 1;
        
        if(isset($aData['parent']) && $aData['parent'] != "0"){
            $aParentData = $this->oController->dbFrom->createCommand("SELECT * FROM `mk_term_taxonomy` WHERE `term_id` = {$aData['parent']}")->queryOne();
            $aParent = $this->oController->dbFrom->createCommand("SELECT * FROM `mk_terms` WHERE `term_id` = {$aData['parent']}")->queryOne();
            $oCategory->parent_id = $this->parseCategory($aParentData, $aParent);
        }
        $bStatus = $oCategory->save();
        $bStatus ? $this->iTempSuccess++ : $this->iTempErrors++;
        
        return $oCategory->id;
    }
    
    private function parseArticle($aData)
    {
        $oArticle = Article::find()->where(["=", "old_id", $aData["ID"]])->one();
        if(is_null($oArticle)){
            $oArticle = new Article();
        }

        $oArticle->old_id = $aData["ID"];
        $oArticle->title = $aData["post_title"];
        $oArticle->category_id = $this->getCategory($aData);
        $oArticle->thumbnail_id = $this->getThumbnail($aData);
        $oArticle->slug = $aData["post_name"];
        $oArticle->is_active = ($aData["post_status"] == "publish");
        $oArticle->is_published = ($aData["post_status"] == "publish");
        $oArticle->short_description = $this->parseText($aData["post_excerpt"]);
        $oArticle->full_description = $this->parseText($aData["post_content"]);
        $oArticle->date_created = $aData["post_date"];
        $oArticle->date_updated = $aData["post_modified"];
        $oArticle->date_display = $aData["post_date"];
        $bStatus = $oArticle->save();
        
        $Articlei18n = Article_i18n::find()->where(["=", "article_id", $oArticle->id])->andWhere(["=", "language", "pl-PL"])->one();
        if(is_null($Articlei18n)){
            $Articlei18n = new Article_i18n();
        }
        $Articlei18n->article_id = $oArticle->id;
        $Articlei18n->language = "pl-PL";
        $Articlei18n->title = $aData["post_title"];
        $Articlei18n->short_description = $this->parseText($aData["post_excerpt"]);
        $Articlei18n->full_description = $this->parseText($aData["post_content"]);
        $Articlei18n->save();
        
        $bStatus ? $this->iTempSuccess++ : $this->iTempErrors++;
        
        return $oArticle->id;
    }
    
    private function getCategory($aData)
    {
        $aPostCategory = $this->oController->dbFrom->createCommand("SELECT * FROM `mk_term_relationships` LEFT JOIN `mk_term_taxonomy` ON `mk_term_taxonomy`.`term_taxonomy_id` = `mk_term_relationships`.term_taxonomy_id WHERE `object_id` = {$aData['ID']} AND mk_term_taxonomy.taxonomy = 'category'")->queryOne();
        if(isset($aPostCategory["term_id"])){
            $oCategory = ArticlesCategories::find()->where(["=", "old_id", $aPostCategory["term_id"]])->one();
            if(!is_null($oCategory)){
                return $oCategory->id;
            }
        }
        
        return null;
    }
    
    private function getThumbnail($aData){
        $aPostMetaThumbnail = $this->oController->dbFrom->createCommand("SELECT * FROM `mk_postmeta` WHERE `post_id` = {$aData['ID']} AND `meta_key` = '_thumbnail_id'")->queryOne();

        if(isset($aPostMetaThumbnail['meta_value'])){
            $aPostThumbnail = $this->oController->dbFrom->createCommand("SELECT * FROM `mk_postmeta` WHERE `post_id` = {$aPostMetaThumbnail['meta_value']} AND `meta_key` = '_wp_attached_file'")->queryOne();

            $sDir  = $this->oController->aPost["upload_dir"];
            $sPath = str_replace($this->sOldPath, "", $aPostThumbnail["meta_value"]);
            
            $sFilePath = $sDir.$sPath;

            if(file_exists($sFilePath)){
                return $this->oController->uploadFile($sFilePath, basename($sFilePath));
            } else {
                $this->iTempErrors++;
                $this->oController->displayMessage(false, "error", "Pliki", "Problem z importem pliku: {$sFilePath}");
            }
        }
        
        return null;
    }
    
    private function parseGallery($oArticle, $aIDs){
        $oGallery = Gallery::find()->where(["=", "name", $oArticle->title])->one();
        if(is_null($oGallery)){
            $oGallery = new Gallery();
        }

        $oGallery->created_at = date("Y-m-d H:i:s");
        $oGallery->name = $oArticle->title;
        $bStatus = $oGallery->save();

        if($bStatus){
            $oArticle->gallery_id = $oGallery->id;
            $oArticle->full_description = preg_replace("/\[gallery[^]]*\]/", '', $oArticle->full_description);
            $bStatus = $oArticle->save(); 

            GalleryItems::deleteAll("gallery_id = {$oGallery->id}");
        }
        
        foreach($aIDs as $index => $sID){
            $aMetaImage = $this->oController->dbFrom->createCommand("SELECT * FROM `mk_postmeta` WHERE `post_id` = {$sID} AND `meta_key` = '_wp_attached_file'")->queryOne();
            if($aMetaImage){
                $sDir  = $this->oController->aPost["upload_dir"];
                $sPath = str_replace($this->sOldPath, "", $aMetaImage["meta_value"]);

                $sFilePath = $sDir.$sPath;

                if(file_exists($sFilePath)){
                    $oGalleryItem = new GalleryItems();
                    $oGalleryItem->gallery_id = $oGallery->id;
                    $oGalleryItem->file_id = $this->oController->uploadFile($sFilePath, basename($sFilePath));
                    $oGalleryItem->sort = $index;
                    $oGalleryItem->name = "Image #{$index}";
                    $oGalleryItem->is_active = 1;
                    $oGalleryItem->save();
                } else {
                    $this->iTempErrors++;
                    $this->oController->displayMessage(false, "error", "Pliki", "Problem z importem pliku: {$sFilePath}");
                }
            }
        }
        
        $bStatus ? $this->iTempSuccess++ : $this->iTempErrors++;
    }
    
    private function parseText($sText)
    {
        // Remove Caption tags
        $sText = preg_replace('#\s*\[caption[^]]*\](.*?)\[/caption\]\s*#is', '<div>$1</div>', $sText);

        // Add br's 
        $sText = $this->nl2p($sText);

        return $sText;
    }
	
    private function nl2p($string)
    {
        $paragraphs = '';

        foreach (explode("\n", $string) as $line) {
            if (trim($line)) {
                $paragraphs .= '<p>' . $line . '</p>';
            }
        }

        return $paragraphs;
    }
	
}
