<?php

namespace common\modules\gallery\admin;

/**
 * gallery module definition class
 */
class Gallery extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\gallery\admin\controllers';
    public $defaultRoute = 'gallery';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
