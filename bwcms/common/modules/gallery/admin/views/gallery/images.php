<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\helpers\ArrayHelper;
use common\modules\files\models\FileCategory;

use common\modules\gallery\models\GalleryItems;

$this->title = Yii::t('backend_module_gallery', 'gallery').': '.Yii::t('backend_module_gallery', 'Images');
$this->params['breadcrumbs'][] = $this->title;

//add CSS
$this->registerCssFile('/bwcms/common/modules/gallery/admin/assets/css/gallery.css');

$this->registerJsFile('/bwcms/common/modules/gallery/admin/assets/js/adding.js', ['depends' => [yii\web\JqueryAsset::className()]]);
$activeCat = false;
?>

<div class="file-index">
    
    <p><?= Html::a('< '.Yii::t('backend_module_gallery', 'Return to Gallery'), ['view', 'id' => $gallery], ['class' => 'btn btn-outline-secondary']) ?></p>
    
    <section class="panel full">
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo">
            <?= Yii::t('backend_module_gallery', 'Images'); ?>
        </header>
        <div class="panel-body" >
            <div class="row" >
                <div class="col-12 col-sm-4" >
                    <b>Foldery:</b>
                    <select id="getCategory">
                        <option value="<?= \Yii::$app->request->baseUrl ?>gallery/gallery/image_add/?gallery=<?= Yii::$app->request->get('gallery') ?>">- wybierz folder -</option>
                        <?php foreach(ArrayHelper::map(FileCategory::find()->all(), 'id', 'name') as $iID => $sName){ ?>
                            <option value="<?= \Yii::$app->request->baseUrl ?>gallery/gallery/image_add/?gallery=<?= Yii::$app->request->get('gallery') ?>&cat=<?= $iID ?>&show=<?= Yii::$app->request->get('show', 8) ?>" <?php if(Yii::$app->request->get('cat') == $iID){ echo "selected"; } ?>><?= $sName ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-12 col-sm-4">
                    <div class="form-group">
                        <b>Wyszukaj</b>
                        <input type="text" id="gal-search" class="form-control" name="search" value="<?= \Yii::$app->request->get('search') ?>" placeholder="" aria-invalid="false">
                    </div>
                </div>
                <div class="col-12 col-sm-4 text-right" style="padding-top: 5px;" >
                    <b><?= Yii::t('backend_module_gallery', 'Show on the page'); ?>:</b>
                    <div class="dropdown" style="display: inline-block;" >
                        <button class="btn dropdown-toggle btn-outline-secondary" type="button" data-toggle="dropdown"> 
                        <?php echo $filtr['show']; ?>
                        <span class="caret"></span></button>
                        <ul class="dropdown-menu" style="min-width: 60px;" >
                        <?php for ($i=8; $i<33; $i=$i+4) { ?>
                            <li <?php if($filtr['show'] == $i){ echo 'class="active"'; } ?> >
                                <?php 
                                if( $activeCat ){
                                    echo "<a href='". Url::to([ 'image_add', 'gallery' => $gallery, 'cat' => $activeCat, 'show' => $i ]) ."' >{$i}</a>";
                                } else {
                                    echo "<a href='". Url::to([ 'image_add', 'gallery' => $gallery, 'show' => $i ]) ."' >{$i}</a>";
                                }
                                ?>
                            </li>
                        <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>

            <hr>
            
            <div class="row images-gallery-items add" >
                <?php foreach ($models as $photo) { ?>
                
                    <?php 
                        $item = GalleryItems::findOne(['gallery_id' => $gallery, 'file_id' => $photo->id]);
                        
                        if($item) { 
                            $actionLink = Url::to([ 'image_delete', 'id' => $item->id, 'back' => true ]);
                        } else { 
                            $actionLink = Url::to([ 'image_addnew', 'gallery' => $gallery, 'id' => $photo->id ]);
                        }
                    ?>
                
                   
                            <div class="item <?php if($item){ echo "active"; } ?>" style="background-image: url('/upload/<?= $photo->filename; ?>');">
                                <a style="position: absolute; left: 0px; top: 0px; width: 100%; height: 100%; z-index: 999;" href="<?php echo $actionLink; ?>" data-method="post" data-pjax="0" >
                                <div class="clearfix" >
                                    <div class="sort" >ID: <?php echo $photo->id; ?></div>
                                    <div class="actions" >
                                        <b><?php echo $item ? '+' : '-' ; ?></b>
                                    </div>
                                </div>
                                <div class="name" >
                                    <div><span><b><?php echo $photo->name; ?></b></span></div>
                                    <small>( <?php $oCategory = FileCategory::findOne(['id' => $photo->category_id]); echo $oCategory->name;?> | <?php echo \Yii::$app->formatter->asShortSize($photo->size); ?> )</small>
                                </div>
                                </a>
                            </div>

                <?php } ?>
            </div>
            
            <?php if($pages->totalCount > $pages->defaultPageSize) { ?>
                <hr>
                <div class="text-center" >
                    <?php echo LinkPager::widget([ 'pagination' => $pages ]); ?>
                </div>
            <?php } ?>
                
        </div>
    </section>
</div>
