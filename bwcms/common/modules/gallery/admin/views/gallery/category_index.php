<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\gallery\models\GalleryCatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend_module_gallery', 'module_gallery').': '.Yii::t('backend_module_gallery', 'categories_list');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gallery-cat-index">

    <p>
        <?= Html::a(Yii::t('backend_module_gallery', 'back_to_galleries_list'), ['index'], ['class' => 'btn btn-outline-secondary']) ?> 
        <?= Html::a(Yii::t('backend_module_gallery', 'Create new Category'), ['category_create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend_module_gallery', 'Categories'); ?>
            <form id="per-page">
                <span><?= Yii::t('backend', 'quantity_per_page'); ?>:</span>
                <select name="per-page" class="form-control">
                    <?php if(Yii::$app->request->get('per-page')){ echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>"; } ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'header' => Yii::t('backend', 'LP'),
                        'contentOptions'=>['style'=>'width: 70px']
                    ],

                    [
                        'attribute' => 'id',
                        'contentOptions'=>['style'=>'width: 70px']
                    ],
                    
                    'name',

                    [
                        'class' => 'common\hooks\yii2\grid\ActionColumn',
                        'contentOptions'=>['style'=>'width: 100px; text-align: center'],
                        'template' => '{leadUpdate} {leadView} {leadDelete}',
                        'buttons' => [
                            'leadUpdate' => function ($url, $model) {
                                $url = Url::to(['category_update', 'id' => $model->id]);
                                return Html::a(
                                                '<span class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></span>', $url, [
                                            'title' => Yii::t('backend', 'edit'),
                                            'data-toggle' => "tooltip",
                                            'data-placement' => "bottom"
                                                ]
                                );
                            },
                            'leadView' => function ($url, $model) {
                                $url = Url::to(['category_view', 'id' => $model->id]);
                                return Html::a(
                                                '<span class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-eye-open"></span></span>', $url, [
                                            'title' => Yii::t('backend', 'view'),
                                            'data-toggle' => "tooltip",
                                            'data-placement' => "bottom"
                                                ]
                                );
                            },
                            'leadDelete' => function ($url, $model) {
                                $url = Url::to(['category_delete', 'id' => $model->id]);
                                return Html::a(
                                                '<span class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></span>', $url, [
                                            'title' => Yii::t('backend', 'delete'),
                                            'data-toggle' => "tooltip",
                                            'data-placement' => "bottom",
                                            'data-pjax' => 0,
                                            'data-confirm' => 'Czy na pewno usunąć ten element?',
                                            'data-method' => 'post',
                                                ]
                                );
                            },
                        ],
                    ],
                ],
            ]); ?>
            
         </div>
        
    </section>
</div>