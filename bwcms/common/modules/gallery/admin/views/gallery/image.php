<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = Yii::t('backend_module_gallery', 'Image').': '.$model->name;

//add CSS
$this->registerCssFile('/bwcms/common/modules/gallery/admin/assets/css/gallery.css');
?>

<div class="file-index">
    
    <p><?= Html::a('< '.Yii::t('backend_module_gallery', 'Return to Gallery'), ['view', 'id' => $model->gallery_id], ['class' => 'btn btn-outline-secondary']) ?></p>
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> 
            <?= Yii::t('backend', 'update'); ?> <?= Yii::t('backend', 'data'); ?>
        </header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(['action' => Url::to([ 'image_upadepost', 'id' => $model->id ]), 'method' => 'post',]); ?>
                <div class="row gallery-item-image" >
                    <div class="col-6" >
                        
                    </div>
                    <div class="col-6" >
                        
                    </div>
                    <div class="col-12 col-sm-6" >
                        <div class="img-native" >
                            <span>
                                <?php 
                                    echo Yii::t('backend_module_gallery', 'Native image');
                                    
                                    $filename = Yii::getAlias('@root/upload/'.$model->file->filename);
                                    $fileSize = list($width, $height) = getimagesize($filename);
                                    if($fileSize){ echo ': ('.$fileSize[0].'x'.$fileSize[1].')'; }
                                ?>
                            </span>
                            <br>
                            <img src='/app-upload-files/<?= $model->file->filename; ?>' alt='<?= $model->alt; ?>' />
                        </div>
                        <div class="img-native" >
                            <span>

                            </span>
                            <hr>
                            <p>
                                <?= Html::a(Yii::t('backend_module_gallery', 'Edit image'), ['image_crop', 'id' => $model->id], ['class' => 'btn btn-warning mb10']) ?>
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6" >
                        <div class="xmod-gallery-form">
                            <?php echo $form->field($model, 'is_active')->dropDownList( array( 0 => Yii::t('backend', 'no'), 1 => Yii::t('backend', 'yes') ) )->label( \Yii::t('backend', 'is_active') ); ?>
                            <?php echo $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                            <?php echo $form->field($model, 'alt')->textInput(['maxlength' => true]); ?>
                            <?php echo $form->field($model, 'extlink')->textInput(['maxlength' => true]); ?>
                            <?php echo $form->field($model, 'description')->textarea(['rows' => 6]); ?>
                            <div class="form-group text-right">
                                <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'create') : Yii::t('backend', 'update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </section>

</div>
