<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\gallery\models\GalleryCat */

$this->title = Yii::t('backend_module_gallery', 'Create new Category');

$this->params['breadcrumbs'][] = ['label' => Yii::t('backend_module_gallery', 'Gallery Cats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gallery-cat-create">
    <p>
        <?= Html::a(Yii::t('backend_module_gallery', 'back_to_galleries_category_list'), ['category_index'], ['class' => 'btn btn-outline-secondary']) ?>
    </p>
    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend', 'data'); ?></header>
        <div class="panel-body" >
            <?=
            $this->render('category_form', [
                'model' => $model,
            ])
            ?>
        </div>
    </section>

</div>
