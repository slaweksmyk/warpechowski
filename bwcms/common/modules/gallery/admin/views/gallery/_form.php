<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="xmod-gallery-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-6">
            <?php echo $form->field($model, 'category_id')->dropDownList( $categories ); ?>
        </div>
    </div>

    <?php echo $form->field($model, 'description')->textarea(['rows' => 6]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'create') : Yii::t('backend', 'update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
