<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\widgets\DetailView;

$this->title = Yii::t('backend_module_gallery', 'gallery').': '.$model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend_module_gallery', 'Xmod Galleries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

//add CSS
$this->registerCssFile('/bwcms/common/modules/gallery/admin/assets/css/gallery.css');

$this->registerJsFile('/bwcms/common/modules/gallery/admin/assets/js/editing.js', ['depends' => [yii\web\JqueryAsset::className()]]);
?>

<div class="xmod-gallery-view">

    <p>
        <?= Html::a(Yii::t('backend_module_gallery', 'back_to_galleries_list'), ['index'], ['class' => 'btn btn-outline-secondary']) ?>      
        <?= Html::a(Yii::t('backend', 'update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend', 'delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend_module_gallery', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a("Dodaj grafiki z komputera", ['image_upload', 'gallery' => $model->id], ['class' => 'btn btn-success']) ?>
        <?= Html::a("Dodaj grafiki z menadżera plików", ['image_add', 'gallery' => $model->id], ['class' => 'btn btn-warning']) ?>
    </p>
    
    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend_module_gallery', 'module_pages_admin_gallery_data'); ?></header>
        <div class="panel-body" >
            <?php echo DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    [
                        'attribute' => 'category_id',
                        'label' => Yii::t('backend_module_gallery', 'Category'),
                        'value' => ($model->category_id) ? $model->category->name : ''
                    ],
                ],
            ]) ?>
        </div>
    </section>
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend_module_gallery', 'Images'); ?>
        </header>
        <div class="panel-body" >
            <div class="images-gallery-items">
                <?php foreach ($items as $photo) { ?>
                    <div data-item="<?= $photo->id ?>" class="item <?php if($photo->is_active){ echo 'active'; } ?>" style=" background-image: url('/upload/<?= $photo->file->filename; ?>');" >
                        <div class="clearfix" >
                            <div class="sort" >
                                &nbsp<span class="sort-no"><?php echo $photo->sort; ?></span>&nbsp
                            </div>
                            <div class="actions" >
                                <a href="<?php echo Url::to([ 'image_update', 'id' => $photo->id ]); ?>" title="Edytuj" aria-label="Edytuj" data-pjax="0" >
                                    <span class="glyphicon glyphicon-pencil"></span>
                                </a>
                                <a href="<?php echo Url::to([ 'image_delete', 'id' => $photo->id ]); ?>" title="Usuń" aria-label="Usuń" data-confirm="Czy na pewno usunąć ten element?" data-method="post" data-pjax="0" >
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </div>
                        </div>
                        <div class="name" >
                            <div><span><b><?php echo empty($photo->name) ? $photo->file->name : $photo->name; ?></b></span></div>
                            <small>( <?php echo \Yii::$app->formatter->asShortSize($photo->file->size); ?> )</small>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div style="clear: both;"></div>
            
            <?php if($pages->totalCount > $pages->defaultPageSize) { ?>
                <hr>
                <div class="text-center" >
                    <?php echo LinkPager::widget([ 'pagination' => $pages ]); ?>
                </div>
            <?php } ?>
            
        </div>
    </section>

</div>
