<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\gallery\models\GalleryCat */

$this->title = Yii::t('backend_module_gallery', 'gallery_cats_edit') . ': ' . $model->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('backend_module_gallery', 'Gallery Cats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend_module_gallery', 'Update');
?>
<div class="gallery-cat-update">
    <p>

        <?= Html::a(Yii::t('backend', 'back_to_category'), ['category_index'], ['class' => 'btn btn-outline-secondary']) ?>
    </p>
    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend', 'update'); ?> <?= Yii::t('backend', 'data'); ?></header>
        <div class="panel-body" >
            <?=
            $this->render('category_form', [
                'model' => $model,
            ])
            ?>
        </div>
    </section>

</div>
