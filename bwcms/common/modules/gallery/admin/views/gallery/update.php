<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\gallery\models\XmodGallery */

$this->title = Yii::t('backend_module_gallery', 'update_gallery').': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend_module_gallery', 'Xmod Galleries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend_module_gallery', 'Update');
?>
<div class="xmod-gallery-update">
    <p>
        <?= Html::a(Yii::t('backend_module_gallery', 'back_to_galleries_list'), ['index'], ['class' => 'btn btn-outline-secondary']) ?>
    </p>
    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend', 'update'); ?> <?= Yii::t('backend', 'data'); ?></header>
        <div class="panel-body" >
        <?= $this->render('_form', [
            'model' => $model,
            'categories' => $categories
        ]) ?>
        </div>
    </section>

</div>
