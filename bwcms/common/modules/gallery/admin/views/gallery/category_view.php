<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\modules\gallery\models\GalleryCat */

$this->title = Yii::t('backend_module_gallery', 'Category') . ': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend_module_gallery', 'Gallery Cats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gallery-cat-view">

    <p>

        <?= Html::a(Yii::t('backend', 'back_to_category'), ['category_index'], ['class' => 'btn btn-outline-secondary']) ?>

        <?= Html::a(Yii::t('backend', 'edit'), ['category_update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a(Yii::t('backend', 'delete'), ['category_delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend_module_gallery', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend', 'data'); ?></header>
        <div class="panel-body" >
            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'name',
                    [
                        'attribute' => 'description',
                        'format' => 'raw',
                    ],
                ],
            ])
            ?>
        </div>
    </section>

            <?php if (!empty($model->gallery)) { ?>
        <section class="panel full" >
            <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend_module_gallery', 'Galleries in the Category'); ?></header>
            <div class="panel-body" >
                <?php
                echo '<table class="table table-striped table-bordered detail-view"><tbody>';

                foreach ($model->gallery as $gallery) {
                    $url = Url::to(['view', 'id' => $gallery->id]);
                    echo '<tr><td> - ' . Html::a($gallery->name . ' <span class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-eye-open"></span></span>', $url) . '</td></tr>';
                }

                echo '</tbody></table>';
                ?>
            </div>
        </section>
<?php } ?>

</div>
