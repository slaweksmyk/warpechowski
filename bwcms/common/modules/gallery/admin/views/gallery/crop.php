<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = Yii::t('backend_module_gallery', 'Edit image').': '.$oFile->name;

//add CSS
$this->registerCssFile(Yii::$app->homeUrl.'/library/cropperjs/assets/css/font-awesome.min.css');
$this->registerCssFile(Yii::$app->homeUrl.'/library/cropperjs/dist/cropper.min.css');
$this->registerCssFile('/bwcms/common/modules/gallery/admin/assets/css/from-cropp.css');

//add JS
$this->registerJsFile(Yii::$app->homeUrl.'/library/cropperjs/dist/cropper.min.js');
$this->registerJsFile('/bwcms/common/modules/gallery/admin/assets/js/from-cropp.js');
?>

<div class="file-index">
    
    <p><?= Html::a('< '.Yii::t('backend_module_gallery', 'Return'), "javascript:window.history.back();", ['class' => 'btn btn-primary']) ?></p>
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo" /> 
            <?= Yii::t('backend', 'update'); ?> <?= Yii::t('backend', 'data'); ?>
        </header>
        <div class="panel-body" >
            
            <div class="row gallery-cropp-js">
                <div class="col-12 col-md-8" >
                    <div class="img-container">
                        <img src="/upload/<?= $oFile->filename; ?>" >
                    </div>
                </div>
                <div class="col-12 col-md-4" >
                    <div class="docs-data">
                        <div class="input-group input-group-sm">
                            <label class="input-group-addon" for="dataX">X</label>
                            <input type="text" class="form-control" id="dataX" placeholder="x">
                            <span class="input-group-addon">px</span>
                        </div>
                        <div class="input-group input-group-sm">
                            <label class="input-group-addon" for="dataY">Y</label>
                            <input type="text" class="form-control" id="dataY" placeholder="y">
                            <span class="input-group-addon">px</span>
                        </div>
                        <div class="input-group input-group-sm">
                            <label class="input-group-addon" for="dataWidth">Szerokość</label>
                            <input type="text" class="form-control" id="dataWidth" placeholder="width">
                            <span class="input-group-addon">px</span>
                        </div>
                        <div class="input-group input-group-sm">
                            <label class="input-group-addon" for="dataHeight">Wysokość</label>
                            <input type="text" class="form-control" id="dataHeight" placeholder="height">
                            <span class="input-group-addon">px</span>
                        </div>
                        <div class="input-group input-group-sm">
                            <label class="input-group-addon" for="dataScaleX">ScaleX</label>
                            <input type="text" class="form-control" id="dataScaleX" placeholder="scaleX">
                        </div>
                        <div class="input-group input-group-sm">
                            <label class="input-group-addon" for="dataScaleY">ScaleY</label>
                            <input type="text" class="form-control" id="dataScaleY" placeholder="scaleY">
                        </div>
                    </div>
                    <div id="gallery-crop-actions" class="text-right" >
                        <div class="docs-toggles">
                            <div class="btn-group docs-aspect-ratios" data-toggle="buttons" >
                                <label class="btn btn-primary">
                                    <input type="radio" class="sr-only" name="aspectRatio" value="1.7777777777777777" >
                                    <span class="docs-tooltip" >
                                        16:9
                                    </span>
                                </label>
                                <label class="btn btn-primary">
                                    <input type="radio" class="sr-only" name="aspectRatio" value="1.3333333333333333" >
                                    <span class="docs-tooltip" >
                                        4:3
                                    </span>
                                </label>
                                <label class="btn btn-primary">
                                    <input type="radio" class="sr-only" name="aspectRatio" value="1" >
                                    <span class="docs-tooltip" >
                                        1:1
                                    </span>
                                </label>
                                <label class="btn btn-primary">
                                    <input type="radio" class="sr-only" name="aspectRatio" value="0.6666666666666666" >
                                    <span class="docs-tooltip" >
                                        2:3
                                    </span>
                                </label>
                                <label class="btn btn-primary active">
                                    <input type="radio" class="sr-only" name="aspectRatio" value="NaN" checked>
                                    <span class="fa fa-crop"></span>
                                </label>
                            </div>
                        </div>
                        <div class="docs-buttons"> 
                            <div class="btn-group">
                                <span class="btn btn-primary" data-method="flip" data-value ="horizontal" data-action="-1" >
                                    <span class="fa fa-arrows-h"></span>
                                </span>
                                <span class="btn btn-primary" data-method="flip" data-value ="vertical" data-action="-1" >
                                    <span class="fa fa-arrows-v"></span>
                                </span>
                            </div>
                            <div class="btn-group">
                                <span class="btn btn-warning" data-method="reset" data-value ="crop" >
                                    <span class="fa fa-refresh"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <?php $form = ActiveForm::begin(['action' => Url::to([ 'image_cropsave', 'id' => $oFile->id ]), 'method' => 'post',]); ?>
                        <?= $form->field($oFile, 'data')->hiddenInput([ 'id' => 'inputData' ])->label(false); ?>
                        <div class="form-group" >
                            <?= Html::submitButton(Yii::t('backend', 'save'), ['class' => 'btn btn-success send-form']) ?>
                        </div>
                    <?php ActiveForm::end(); ?>
                    <?= Html::a("Przywróć oryginalne zdjęcie", ["image_restore", 'id' => $oFile->id], ['class' => 'btn btn-danger', "style" => "width: 100%;"]) ?>
                </div>
                <div class="col-12 all-prev" >
                    <hr>
                    <div class="clearfix" >
                        <div class="img-preview preview-lg"></div>
                        <div class="img-preview preview-md"></div>
                        <div class="img-preview preview-sm"></div>
                        <div class="img-preview preview-xs"></div>
                    </div>
                </div>
            </div>
            
        </div>
    </section>
</div>
