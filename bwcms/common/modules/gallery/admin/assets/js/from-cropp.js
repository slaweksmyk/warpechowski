window.addEventListener('DOMContentLoaded', function () {
    
    var container = document.querySelector('.gallery-cropp-js .img-container');
    var image = container.getElementsByTagName('img').item(0);
    var actions = document.getElementById('gallery-crop-actions');
    var dataX = document.getElementById('dataX');
    var dataY = document.getElementById('dataY');
    var dataHeight = document.getElementById('dataHeight');
    var dataWidth = document.getElementById('dataWidth');
    var dataScaleX = document.getElementById('dataScaleX');
    var dataScaleY = document.getElementById('dataScaleY');
    var inputImage = document.getElementById('inputData');
    var firstStart = true; 
    var aspectR = false;
    
    var options = {
        preview: '.gallery-cropp-js .img-preview',
        rotatable: false,
        zoomable: false,
        movable: false,
        crop: function (e) {
            var data = e.detail;
            
            if( firstStart ) { 
                var jsonFirst = inputImage.value;
                
                if(jsonFirst && jsonFirst !== ''){
                    var data = JSON.parse(jsonFirst);
                    croppData(data, 'set');
                } else {
                    aspectR = "NaN";
                    croppData(data, 'get' );
                }
                firstStart = false;
            }
            else { 
                croppData(data, 'get' ); 
            }
        }
    };
    
    
    if( aspectR ) { options['aspectRatio'] = aspectR; }
    
    var cropper = new Cropper(image, options);


    actions.querySelector('.docs-toggles').onclick = function (event) {
        var e = event || window.event;
        var target = e.target || e.srcElement;
        var input = target.previousElementSibling;
        var value = input.getAttribute('value');
        
        cropper.setAspectRatio(value);
    };
    
    
    actions.querySelector('.docs-buttons').onclick = function (event) {
        var e = event || window.event;
        var target = e.target || e.srcElement;
        
        if ( !target.getAttribute('data-method') ) {
            target = target.parentNode;
        }  
        
        var method = target.getAttribute('data-method');
        var value = target.getAttribute('data-value');
        
        switch (method) { 
            case 'flip':
                var action = target.getAttribute('data-action');
                if      (value === 'horizontal'){ cropper.scaleX(action); } 
                else if ( value === 'vertical'){ cropper.scaleY(action); }
                if      (action === '-1'){ target.setAttribute('data-action','1'); } 
                else    { target.setAttribute('data-action','-1'); }
                break;
                
            case 'reset':
                cropper.reset();
                break;
                
        }
    };
    
    
    function croppData(data, action)
    {
        dataX.value = Math.round(data.x);
        dataY.value = Math.round(data.y);
        dataHeight.value = Math.round(data.height);
        dataWidth.value = Math.round(data.width);
        dataScaleX.value = typeof data.scaleX !== 'undefined' ? data.scaleX : '';
        dataScaleY.value = typeof data.scaleY !== 'undefined' ? data.scaleY : '';
        
        if( action === 'get' ){
            var json = JSON.stringify(data, null, '');
            inputImage.value = json;
        } else {
            cropper.setData(data);
        }
    }
    
    
});


// RESIZE ---
var metuToggle = document.getElementById('menu-toggle');
metuToggle.onclick = reloadPage;
window.addEventListener('resize', reloadPage);

function reloadPage(){
    setTimeout(function() { location.reload(); }, 500);
}