$(document).ready(function(){
    
    var parts = window.location.search.substr(1).split("&");
    var $_GET = {};
    for (var i = 0; i < parts.length; i++) {
        var temp = parts[i].split("=");
        $_GET[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
    }
    
    $("#getCategory").change(function(){
        window.location.href = $(this).val();
    });
    
    $("#gal-search").change(function(){
        window.location.href = $("#homeDir").text()+"gallery/gallery/image_add/?gallery="+$_GET['gallery']+"&search=" + $(this).val();
    });
    
});