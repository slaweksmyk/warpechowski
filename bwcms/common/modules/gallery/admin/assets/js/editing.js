$(document).ready(function(){
    
    $(".images-gallery-items").sortable({ 
        stop: function( event, ui ){
            updateList();
            var dataitem = ui.item.attr('data-item');
            $('.images-gallery-items .item[data-item='+dataitem+']').css('left', 'initial').css('right', 'initial').css('top', 'initial');
        }
    });
    
    function updateList(){
        $(".images-gallery-items .item").each(function(){
            var no = $(this).index()+1;
            
            $(this).find(".sort-no").text(no);
            savePosition($(this).data("item"), no);
        });
    }
    
    function savePosition(imageID, sort){
        $.ajax({
          method: "POST",
          url: $("#homeDir").text()+"gallery/gallery/image_sort/",
          data: { imageID: imageID, sort: sort }
        }).done(function( data ) {
            
        });
    }
    
});