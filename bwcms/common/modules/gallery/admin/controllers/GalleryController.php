<?php

/**
 * Images gallery
 * 
 * PHP version 7
 *
 * @author Volodymyr Shelelo <contact@profficode.com>
 * @copyright (c) 2016 Volodymyr Shelelo
 * @version 1.0
 */

namespace common\modules\gallery\admin\controllers;

use Yii;
use common\modules\gallery\models\Gallery;
use common\modules\gallery\models\GallerySearch;
use common\modules\gallery\models\GalleryItems;
use common\modules\gallery\models\GalleryCat;
use common\modules\gallery\models\GalleryCatSearch;
use common\modules\files\models\File;
use common\modules\files\models\FileCategory;
use common\modules\files\models\UploadForm;
use yii\web\UploadedFile;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\Pagination;

/**
 * GalleryController implements the CRUD actions for XmodGallery model.
 */
class GalleryController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'image_delete' => ['POST'],
                    'image_addnew' => ['POST'],
                    'image_sort' => ['POST'],
                    'image_upadepost' => ['POST'],
                    'image_cropsave' => ['POST'],
                    'image_cropdelete' => ['POST'],
                    'category_delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all XmodGallery models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GallerySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->get('per-page')) {
            $dataProvider->setPagination(['pageSize' => Yii::$app->request->get('per-page')]);
        }

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'errorAction' => Yii::$app->request->get('errorAction', null)
        ]);
    }

    public function actionGetGaleriesForMce($query = false)
    {
        if (Yii::$app->request->isAjax && !empty($query)) {
            $key = Yii::$app->request->get('query', null);

            $oGalleryRowset = Gallery::find()->where(['like', 'name', $key])->orderBy("id DESC")->limit(20)->all();

            $aTypeItems = [];
            foreach ($oGalleryRowset as $index => $oType) {
                $aTypeItems[$index]["id"] = $oType->id;
                $aTypeItems[$index]["text"] = $oType->name;
            }

            $aReturnArr = [];
            $aReturnArr["total_count"] = count($oGalleryRowset);
            $aReturnArr["items"] = array_filter($aTypeItems);

            echo json_encode(array_filter($aReturnArr));
            exit;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Displays a single XmodGallery model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $query = GalleryItems::find()->where(['gallery_id' => $id])->orderBy('sort ASC');
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize' => 9999]);

        $items = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->all();

        return $this->render('view', [
                    'model' => $this->findModel($id),
                    'items' => $items,
                    'pages' => $pages,
        ]);
    }

    /**
     * Creates a new XmodGallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Gallery();

        $catModel = GalleryCat::find()->all();
        $categories = [Yii::t('backend_module_gallery', 'none')];
        if ($catModel) {
            foreach ($catModel as $value) {
                $categories[$value->id] = $value->name;
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'categories' => $categories
            ]);
        }
    }

    /**
     * Updates an existing XmodGallery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $catModel = GalleryCat::find()->all();
        $categories = [Yii::t('backend_module_gallery', 'none')];
        if ($catModel) {
            foreach ($catModel as $value) {
                $categories[$value->id] = $value->name;
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
                        'categories' => $categories
            ]);
        }
    }

    /**
     * Deletes an existing XmodGallery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the XmodGallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return XmodGallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Gallery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //--------------------------------------------------------------------------
    // ---- ITEMS --------------------------------------------------------------

    /**
     * Show images list (add images)
     * 
     * @param integer $gallery
     * @return mixed
     */
    public function actionImage_add($gallery, $cat = false, $show = 8)
    {
        $query = File::find()->where(["OR", ['type' => 'image/jpeg'], ['type' => 'image/jpg'], ['type' => 'image/png'], ['type' => 'image/gif']]);

        if ($cat) {
            $query->andWhere(['category_id' => $cat]);
        }

        $sSearch = \Yii::$app->request->get('search');
        if ($sSearch) {
            $query->andWhere(["LIKE", "name", $sSearch]);
        }

        $query->orderBy('created_at DESC');

        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize' => $show]);
        $models = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->all();

        if ($cat) {
            $firtrCat = FileCategory::find()->select(['id', 'name'])->where(['id' => $cat])->one();
        } else {
            $firtrCat = false;
        }

        return $this->render('images', [
                    'models' => $models,
                    'pages' => $pages,
                    'gallery' => $gallery,
                    'categories' => FileCategory::find()->all(),
                    'filtr' => ['cat' => $firtrCat, 'show' => $show]
        ]);
    }

    /**
     * Add image to gallery (action POST)
     * 
     * @param integer $gallery
     * @param integer $id
     * @return mixed
     */
    public function actionImage_addnew($gallery, $id)
    {
        $file = File::findOne(['id' => $id]);

        if ($file) {
            $sortMod = GalleryItems::find()->select(['sort'])->orderBy('sort DESC')->one();

            if ($sortMod) {
                $sort = intval($sortMod['sort']) + 1;
            } else {
                $sort = 1;
            }

            $model = new GalleryItems();
            $model->gallery_id = $gallery;
            $model->file_id = $id;
            $model->name = $file->name;
            $model->is_active = 1;
            $model->sort = $sort;
            $model->save();
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Remove item image in the gallery
     * 
     * @param integer $id
     * @return mixed
     */
    public function actionImage_delete($id, $back = false)
    {
        $item = GalleryItems::findOne(['id' => $id]);

        //if( $item->crop ){ self::deleteCropItem($item->crop); }
        $gallery = $item->gallery_id;
        $delete = $item->delete();

        if ($delete) {
            $items = GalleryItems::find()->where(['gallery_id' => $gallery])->orderBy('sort ASC')->all();

            $i = 1;
            foreach ($items as $image) {
                $image->sort = $i++;
                $image->save();
            }
        }

        if ($back) {
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            return $this->redirect(['view', 'id' => $gallery]);
        }
    }

    /**
     * SORT images in the Gallery
     * 
     * @param integer $gallery
     * @param integer $id
     * @param string $to
     * @return mixed
     */
    public function actionImage_sort()
    {
        $imageID = Yii::$app->request->post("imageID");
        $sort = Yii::$app->request->post("sort");

        $oGalleryItem = GalleryItems::findOne($imageID);
        $oGalleryItem->sort = $sort;
        $oGalleryItem->save();
    }

    /**
     * Gallery item image edit data
     * 
     * @param integer $gallery
     * @param integer $id
     * @return mixed
     */
    public function actionImage_update($id)
    {
        $model = GalleryItems::findOne(['id' => $id]);

        return $this->render('image', [
                    'model' => $model
        ]);
    }

    /**
     * Gallery item image crop
     * 
     * @param integer $gallery
     * @param integer $id
     * @return mixed
     */
    public function actionImage_upadepost($id)
    {
        $model = GalleryItems::findOne(['id' => $id]);
        $model->load(Yii::$app->request->post());
        $model->save();

        return $this->redirect(['image_update', 'id' => $model->id]);
    }

    /**
     * Crop item image
     * 
     * @param integer $id
     * @return mixed
     */
    public function actionImage_crop($id)
    {
        $oFile = File::find()->where(["=", "id", $id])->one();

        if (strpos($oFile->type, "image") === false) {
            throw new NotFoundHttpException('Nie można kadrować tego pliku');
        }

        return $this->render('crop', [
                    'oFile' => $oFile
        ]);
    }

    /**
     * Save crop image
     * 
     * @param integer $id
     * @return mixed
     */
    public function actionImage_cropsave($id)
    {
        $oFile = File::find()->where(["=", "id", $id])->one();
        $oFile->load(Yii::$app->request->post());
        if ($oFile->is_crop == 0) {
            if (!file_exists(Yii::getAlias('@root/upload/original/'))) {
                $oldmask = umask(0);
                mkdir(Yii::getAlias('@root/upload/original/'), 0777, true);
                umask($oldmask);
            }

            copy(Yii::getAlias('@root/upload/' . $oFile->filename), Yii::getAlias('@root/upload/original/' . basename($oFile->filename)));
            $oFile->is_crop = 1;
        }
        $oFile->save();

        Yii::$app->ImageHelper->imageGalleryCrop($oFile->filename, $oFile->data);

        return $this->redirect(['image_crop', 'id' => $id]);
    }

    public function actionImage_upload($gallery)
    {
        $model = new UploadForm();
        if (Yii::$app->request->isAjax && !empty($gallery)) {

            if (Yii::$app->request->isPost) {
                $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
                $filename = $model->upload();

                if ($filename) {
                    $info = pathinfo($model->imageFile->name);

                    $sDesc = "";
                    if (isset(Yii::$app->request->post('UploadForm')["description"])) {
                        $sDesc = Yii::$app->request->post('UploadForm')["description"];
                    }

                    foreach (explode(",", Yii::$app->request->post('UploadForm')["category_id"]) as $iCategory) {
                        if ($iCategory == 0) {
                            $oCategory = FileCategory::find()->where(["LIKE", "name", Yii::$app->user->identity->username])->one();

                            if (is_null($oCategory)) {
                                $oCategory = new FileCategory();
                                $oCategory->parent_id = 54;
                                $oCategory->name = Yii::$app->user->identity->username;
                                $oCategory->save();
                            }
                            $iCategory = $oCategory->id;
                        }

                        $oFile = new File();
                        $oFile->created_at = date("Y-m-d H:i:s");
                        $oFile->category_id = $iCategory;
                        $oFile->name = $info['filename'];
                        $oFile->filename = $filename;
                        $oFile->description = $sDesc;
                        $oFile->type = $model->imageFile->type;
                        $oFile->size = $model->imageFile->size;
                        $oFile->save();

                        $oGalleryItem = new GalleryItems();
                        $oGalleryItem->gallery_id = $gallery;
                        $oGalleryItem->file_id = $oFile->id;
                        $oGalleryItem->sort = 0;
                        $oGalleryItem->name = $info['filename'];
                        $oGalleryItem->is_active = 1;
                        $oGalleryItem->save();
                    }
                }
                echo json_encode(["iFileID" => $oFile->id, "sPath" => $oFile->filename, "iUploadIndex" => Yii::$app->request->post('uploadIndex')]);
                exit;
            } else {
                throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
            }
        }

        return $this->render('image_upload', [
                    'model' => $model,
                    'maxUpload' => $this->maxFileUploadSize(),
                    'maxUploadRaw' => $this->maxFileUploadSize(false),
        ]);
    }

    /**
     * Delete crop item by POST method
     * 
     * @param int $id
     * @return mixed
     */
    public function actionImage_cropdelete($id)
    {
        $item = GalleryItems::findOne(['id' => $id]);

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Restore cropped image
     * 
     * @param int $id
     * @return mixed
     */
    public function actionImage_restore($id)
    {
        $oFile = File::find()->where(["=", "id", $id])->one();
        copy(Yii::getAlias('@root/upload/original/' . basename($oFile->filename)), Yii::getAlias('@root/upload/' . $oFile->filename));

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Delete item crop and cropped file
     * 
     * @param object $crop
     */
    protected function deleteCropItem($crop)
    {
        $model = GalleryItemsCrop::findOne($crop->id);

        if ($model) {
            $filename = Yii::getAlias('@root/upload/' . $model->filename);
            if ($filename && is_file($filename)) {
                unlink($filename);
                $model->delete();
            }
        }
    }

    // -------------------------------------------------------------------------
    // -------- CATEGORY -------------------------------------------------------
    // -------------------------------------------------------------------------

    /**
     * Lists all GalleryCat models.
     * @return mixed
     */
    public function actionCategory_index()
    {
        $searchModel = new GalleryCatSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('category_index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single GalleryCat model.
     * @param integer $id
     * @return mixed
     */
    public function actionCategory_view($id)
    {
        $model = GalleryCat::findOne(['id' => $id]);

        return $this->render('category_view', [
                    'model' => $model
        ]);
    }

    /**
     * Creates a new GalleryCat model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCategory_create()
    {
        $model = new GalleryCat();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['category_view', 'id' => $model->id]);
        } else {
            return $this->render('category_create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing GalleryCat model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionCategory_update($id)
    {
        $model = GalleryCat::findOne(['id' => $id]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['category_view', 'id' => $model->id]);
        } else {
            return $this->render('category_update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing GalleryCat model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionCategory_delete($id)
    {
        $cat = GalleryCat::findOne(['id' => $id]);
        $galeries = false;

        if ($cat) {
            $galeries = $cat->gallery;
            $delete = $cat->delete();
        } else {
            $delete = false;
        }

        if ($delete && !empty($galeries)) {
            foreach ($galeries as $gallery) {
                $gallery->category_id = 0;
                $gallery->save();
            }
        }

        return $this->redirect(['category_index']);
    }

    private function maxFileUploadSize($formated = true)
    {
        static $max_size = -1;

        if ($max_size < 0) {
            $post_max_size = $this->parseSize(ini_get('post_max_size'));
            if ($post_max_size > 0) {
                $max_size = $post_max_size;
            }

            $upload_max = $this->parseSize(ini_get('upload_max_filesize'));
            if ($upload_max > 0 && $upload_max < $max_size) {
                $max_size = $upload_max;
            }
        }
        if ($formated) {
            return $this->formatBytes($max_size);
        } else {
            return $max_size;
        }
    }

    private function parseSize($size)
    {
        $unit = preg_replace('/[^bkmgtpezy]/i', '', $size);
        $size = preg_replace('/[^0-9\.]/', '', $size);
        if ($unit) {
            return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
        } else {
            return round($size);
        }
    }

    private function formatBytes($size, $precision = 2)
    {
        $unit = ['B', 'KB', 'MB', 'GB', 'TB'];

        for ($i = 0; $size >= 1024 && $i < count($unit) - 1; $i++) {
            $size /= 1024;
        }

        return round($size, $precision) . $unit[$i];
    }

}
