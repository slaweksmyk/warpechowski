<?php

namespace common\modules\gallery\models;

use Yii;

/**
 * This is the model class for table "xmod_gallery_cat".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 */
class GalleryCat extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_gallery_cat}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_gallery', 'ID'),
            'name' => Yii::t('backend_module_gallery', 'Name'),
            'description' => Yii::t('backend_module_gallery', 'Description'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGallery()
    {
        return $this->hasMany(Gallery::className(), ['category_id' => 'id']);
    }
    
}
