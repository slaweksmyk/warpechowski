<?php

namespace common\modules\gallery\models;

use Yii;

/**
 * This is the model class for table "xmod_gallery".
 *
 * @property integer $id
 * @property string $created_at
 * @property string $name
 * @property string $description
 * @property integer $category_id
 *
 * @property GalleryItems[] $galleryItems
 * @property Category $category
 */
class Gallery extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_gallery}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description'], 'string', 'max' => 255],
            [['category_id'], 'integer'],
            ['created_at', 'default', 'value' => date('Y-m-d H:i:s')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_gallery', 'ID'),
            'created_at' => Yii::t('backend_module_gallery', 'Created At'),
            'name' => Yii::t('backend_module_gallery', 'Name'),
            'description' => Yii::t('backend_module_gallery', 'Description'),
            'category_id' => Yii::t('backend_module_gallery', 'Category'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGalleryItems()
    {
        return $this->hasMany(GalleryItems::className(), ['gallery_id' => 'id'])->orderBy('sort ASC');
    }
    
    /**
     * @return Gallery category
     */
    public function getCategory()
    {
        return $this->hasOne(GalleryCat::className(), ['id' => 'category_id']);
    }
    
}
