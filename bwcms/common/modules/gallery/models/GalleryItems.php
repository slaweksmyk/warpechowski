<?php

namespace common\modules\gallery\models;

use Yii;
use common\modules\files\models\File;

/**
 * This is the model class for table "xmod_gallery_items".
 *
 * @property integer $id
 * @property integer $gallery_id
 * @property integer $file_id
 * @property integer $sort
 * @property string $name
 * @property string $alt
 * @property string $description
 *
 * @property Gallery $gallery
 * @property File $file
 * @property GalleryItemsCrop[] $galleryItemsCrops
 */
class GalleryItems extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_gallery_items}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gallery_id', 'file_id', 'sort'], 'required'],
            [['gallery_id', 'file_id', 'sort', 'is_active'], 'integer'],
            [['description'], 'string'],
            [['name', 'alt', 'extlink'], 'string', 'max' => 255],
            [['gallery_id'], 'exist', 'skipOnError' => true, 'targetClass' => Gallery::className(), 'targetAttribute' => ['gallery_id' => 'id']],
            [['file_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['file_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_gallery', 'ID'),
            'gallery_id' => Yii::t('backend_module_gallery', 'Gallery ID'),
            'file_id' => Yii::t('backend_module_gallery', 'File ID'),
            'sort' => Yii::t('backend_module_gallery', 'Sort'),
            'name' => Yii::t('backend_module_gallery', 'Name'),
            'alt' => Yii::t('backend_module_gallery', 'Alt'),
            'extlink' => Yii::t('backend_module_gallery', 'extlink'),
            'description' => Yii::t('backend_module_gallery', 'Description'),
            'is_active' => Yii::t('backend_module_gallery', 'Is active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGallery()
    {
        return $this->hasOne(Gallery::className(), ['id' => 'gallery_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(File::className(), ['id' => 'file_id']);
    }
    
    public function getThumbnail($width = 2000, $height = 2000, $type = "crop", $effect = null, $quality = 80) 
    { 
        return File::getThumbnail($this->file_id, $width, $height, $type, $effect, $quality);
    } 

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGalleryItemsCrops()
    {
        return $this->hasMany(GalleryItemsCrop::className(), ['item_id' => 'id']);
    }
    
    /**
     * @return Crop
     */
    public function getCrop(){
        return $this->hasOne(GalleryItemsCrop::className(), ['item_id' => 'id']);
    }
    
}
