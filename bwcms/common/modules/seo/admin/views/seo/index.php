<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\modules\files\models\File;

$this->title = Yii::t('backend_module_seo', "main_title");
?>
<div class="config-update">
    <?php $form = ActiveForm::begin(); ?>

    <section class="panel full">
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend_module_seo', "section_seo") ?>            
        </header>
        <div class="panel-body" >
            <div class="config-form">
                <div class="row">
                    <div class="col-4"><?= $form->field($model, 'seo_static_title')->textInput(['maxlength' => true]) ?></div>
                </div>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_seo', 'Create') : Yii::t('backend_module_seo', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    </section>

    <section class="panel full">
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend_module_seo', "section_seo_google") ?>            
        </header>
        <div class="panel-body" >
            <div class="config-form">
                <div class="row">
                    <div class="col-4"><?= $form->field($model, 'seo_gsv')->textInput(['maxlength' => true]) ?></div>
                    <div class="col-4"><?= $form->field($model, 'seo_ga')->textInput(['maxlength' => true, 'placeholder' => 'UA-XXXXX-Y']) ?></div>
                    <div class="col-4"><?= $form->field($model, 'seo_gtm')->textInput(['maxlength' => true, 'placeholder' => 'GTM-XXXX']) ?></div>
                </div>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_seo', 'Create') : Yii::t('backend_module_seo', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    </section>

    <section class="panel full">
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend_module_seo', "section_seo_fb") ?>            
        </header>
        <div class="panel-body" >
            <div class="config-form">
                <div class="row">
                    <div class="col-3"><?= $form->field($model, 'seo_fb_profile')->textInput(['maxlength' => true]) ?></div>
                    <div class="col-3"><?= $form->field($model, 'seo_fb_pixel')->textInput(['maxlength' => true]) ?></div>
                    <div class="col-3"><?= $form->field($model, 'seo_fb_app')->textInput(['maxlength' => true]) ?></div>
                    <div class="col-3"><?= $form->field($model, 'seo_fb_image')->dropDownList(ArrayHelper::map(File::find()->all(), 'id', 'name'), ["prompt" => "- wybierz miniature -"]) ?></div>
                </div>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_seo', 'Create') : Yii::t('backend_module_seo', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    </section>

    <section class="panel full">
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend_module_seo', "section_seo_tw") ?>            
        </header>
        <div class="panel-body" >
            <div class="config-form">
                <div class="row">
                    <div class="col-6"><?= $form->field($model, 'seo_tw_profile')->textInput(['maxlength' => true]) ?></div>
                    <div class="col-3"><?= $form->field($model, 'seo_tw_image_alt')->textInput(['maxlength' => true]) ?></div>
                    <div class="col-3"><?= $form->field($model, 'seo_tw_image')->dropDownList(ArrayHelper::map(File::find()->all(), 'id', 'name'), ["prompt" => "- wybierz miniature -"]) ?></div>
                </div>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_seo', 'Create') : Yii::t('backend_module_seo', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    </section>

    <section class="panel full">
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend_module_seo', "section_seo_gp") ?>            
        </header>
        <div class="panel-body" >
            <div class="config-form">
                <div class="row">
                    <div class="col-12"><?= $form->field($model, 'seo_gp_profile')->textInput(['maxlength' => true]) ?></div>
                </div>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_seo', 'Create') : Yii::t('backend_module_seo', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    </section>
    <section class="panel full">
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Profil Youtube            
        </header>
        <div class="panel-body" >
            <div class="config-form">
                <div class="row">
                    <div class="col-12"><?= $form->field($model, 'seo_yt_profile')->textInput(['maxlength' => true])->label("Profil Youtube") ?></div>
                </div>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_seo', 'Create') : Yii::t('backend_module_seo', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    </section>
    
    <section class="panel full">
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Dodatkowe kody JS            
        </header>
        <div class="panel-body" >
            <div class="config-form">
                <div class="row">
                    <div class="col-6"><?= $form->field($model, 'custom_scripts_head')->textarea(['rows' => 6, 'class' => 'form-control noTynyMCE']) ?></div>
                    <div class="col-6"><?= $form->field($model, 'custom_scripts_footer')->textarea(['rows' => 6, 'class' => 'form-control noTynyMCE']) ?></div>
                </div>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_seo', 'Create') : Yii::t('backend_module_seo', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    </section>

    <?php ActiveForm::end(); ?>
</div>
