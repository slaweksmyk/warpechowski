<?php

namespace common\modules\seo\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use common\modules\config\models\Config;

/**
 * ConfigController implements the CRUD actions for Config model.
 */
class SeoController extends Controller
{
    
    private $aManifest = [ "name" => "BlackWolf 3.0", "orientation" => "any", "short_name" => "BlackWolf 3.0", "theme_color" => "black", "start_url" => "/", "display" => "browser", "background_color" => "#fff", "description" => "" ];
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Config models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = $this->findModel(1);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->aManifest["name"] = $model->page_name;
            $this->aManifest["lang"] = \Yii::$app->language;
            
            $sPath = str_replace("backend", "frontend", Yii::getAlias('@webroot'));
            file_put_contents("$sPath/manifest.json", json_encode($this->aManifest));

            return $this->redirect(['index']);
        } else {
            return $this->render('index', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the Config model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Config the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Config::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
