<?php

namespace common\modules\seo\admin;

/**
 * files module definition class
 */
class Seo extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\seo\admin\controllers';
    public $defaultRoute = 'seo';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
