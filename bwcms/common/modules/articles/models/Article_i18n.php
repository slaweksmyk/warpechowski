<?php

namespace common\modules\articles\models;

use Yii;

/**
 * This is the model class for table "{{%xmod_articles_i18n}}".
 *
 * @property integer $id
 * @property string $language
 * @property string $title
 * @property string $short_description
 * @property string $full_description
 */
class Article_i18n extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_articles_i18n}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
            [['id', 'is_more_info'], 'integer'],
            [['date_text', 'place', 'city', 'short_description', 'full_description'], 'string'],
            [['language', 'title'], 'string', 'max' => 1024],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'language' => Yii::t('backend', 'Language'),
            'title' => Yii::t('backend', 'Title'),
            'short_description' => Yii::t('backend', 'Short description'),
            'full_description' => Yii::t('backend', 'Full description'),
        ];
    }
}
