<?php

namespace common\modules\articles\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\articles\models\Article;

/**
 * ArticleSearch represents the model behind the search form about `common\modules\articles\models\Article`.
 */
class ArticleSearch extends Article
{
    public $type_id;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'author_id', 'thumbnail_id', 'status_id', 'create_user_id', 'update_user_id', 'category_id', 'type_id'], 'integer'],
            [['title', 'short_description', 'full_description', 'active_from', 'active_to', 'date_created', 'date_updated'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Article::find()->joinWith("types")->joinWith("status")->leftJoin('bwcms_xmod_articles_i18n', '`bwcms_xmod_articles_i18n`.`article_id` = `bwcms_xmod_articles`.`id`');

        if(Yii::$app->request->get("deleted") != 1){
            $query->where(["!=", Yii::$app->getDb()->tablePrefix."xmod_articles_statuses.id", 3]);
        } else {
            $query->where(["=", Yii::$app->getDb()->tablePrefix."xmod_articles_statuses.id", 3]);
        }
        
        if(Yii::$app->request->get("from") && Yii::$app->request->get("to")){
            $query->where(["between", "date_published", Yii::$app->request->get("from"), Yii::$app->request->get("to")]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'sort' => SORT_ASC
                ]
            ],
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'author_id' => $this->author_id,
            'thumbnail_id' => $this->thumbnail_id,
            'category_id' => $this->category_id,
            'bwcms_xmod_articles_i18n.status_id' => $this->status_id,
            'active_from' => $this->active_from,
            'active_to' => $this->active_to,
            'sort' => $this->sort,
            'create_user_id' => $this->create_user_id,
            'date_created' => $this->date_created,
            'update_user_id' => $this->update_user_id,
            'date_updated' => $this->date_updated,
        ]);

        $query->andFilterWhere(['like', 'bwcms_xmod_articles_i18n.title', $this->title])
            ->andFilterWhere(['like', 'bwcms_xmod_articles_i18n.short_description', $this->short_description])
            ->andFilterWhere(['like', 'bwcms_xmod_articles_i18n.full_description', $this->full_description]);

        return $dataProvider;
    }
}
