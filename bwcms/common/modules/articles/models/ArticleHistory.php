<?php

namespace common\modules\articles\models;

use Yii;
use common\modules\articles\models\Article;
use common\modules\users\models\User;

/**
 * This is the model class for table "xmod_articles_history".
 *
 * @property integer $id
 * @property string $article_id
 * @property integer $updated_user_id
 * @property integer $is_autosave
 * @property string $content_data
 * @property string $create_date
 *
 * @property Article $article
 * @property User $updatedUser
 */
class ArticleHistory extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_articles_history}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article_id', 'updated_user_id', 'is_autosave'], 'integer'],
            [['content_data'], 'string'],
            [['create_date'], 'safe'],
            [['article_id'], 'exist', 'skipOnError' => true, 'targetClass' => Article::className(), 'targetAttribute' => ['article_id' => 'id']],
            [['updated_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_articles', 'ID'),
            'article_id' => Yii::t('backend_module_articles', 'Article ID'),
            'updated_user_id' => Yii::t('backend_module_articles', 'Updated User ID'),
            'is_autosave' => Yii::t('backend_module_articles', 'Is Autosave'),
            'content_data' => Yii::t('backend_module_articles', 'Content Data'),
            'create_date' => Yii::t('backend_module_articles', 'Create Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'article_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedUser()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_user_id']);
    }
}
