<?php

namespace common\modules\articles\models;

use Yii;
use common\modules\files\models\File;
use common\modules\articles\models\Article;

/**
 * This is the model class for table "xmod_articles_files".
 *
 * @property integer $id
 * @property string $article_id
 * @property integer $file_id
 *
 * @property Article $article
 * @property File $file
 */
class ArticleFile extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_articles_files}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article_id', 'file_id', 'is_featured'], 'integer'],
            [['article_id'], 'exist', 'skipOnError' => true, 'targetClass' => Article::className(), 'targetAttribute' => ['article_id' => 'id']],
            [['file_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['file_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_article', 'ID'),
            'article_id' => Yii::t('backend_module_article', 'Article ID'),
            'file_id' => Yii::t('backend_module_article', 'File ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'article_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(File::className(), ['id' => 'file_id']);
    }
}
