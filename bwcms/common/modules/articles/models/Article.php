<?php

namespace common\modules\articles\models;

use Yii;
use common\modules\users\models\User;
use common\modules\categories\models\ArticlesCategories;
use common\modules\gallery\models\Gallery;
use common\modules\files\models\File;
use common\modules\types\models\TypeHash;
use common\modules\authors\models\Author;
use common\modules\tags\models\TagArticle;
use common\modules\articles\models\ArticleRelated;
use common\modules\articles\models\ArticleFile;
use common\modules\articles\models\ArticleVideo;
use common\modules\articles\models\ArticleComment;
use common\modules\articles\models\ArticleHistory;
use common\modules\articles\models\ArticleStatus;
use common\modules\articles\models\Article_i18n;

use yii\imagine\Image;
use Imagine\Image\Box;
use yii\helpers\BaseUrl;

use creocoder\translateable\TranslateableBehavior;

/**
 * This is the model class for table "{{%articles}}". 
 * 
 * @property integer $id
 * @property integer $category_id
 * @property string $title
 * @property string $slug
 * @property string $short_description
 * @property string $full_description
 * @property integer $thumbnail_id
 * @property integer $is_active
 * @property string $active_from
 * @property string $active_to
 * @property integer $sort
 * @property integer $create_user_id
 * @property string $date_created
 * @property integer $update_user_id
 * @property string $date_updated
 * @property integer $is_published
 * @property integer $url
 * 
 * @property ArticlesCategories $category
 * @property User $createUser
 * @property User $updateUser
 * @property ArticlesCategories $articlesCategories
 */
class Article extends \common\hooks\yii2\db\ActiveRecord {

    public $aSliders = [];
    public $forced_position = false;

    /**
     * @inheritdoc 
     */
    public static function tableName() {
        return '{{%xmod_articles}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => ['status_id', 'title', 'slug', 'short_description', "full_description", 'date_text', 'place', 'city', "is_more_info"],
                'translationLanguageAttribute' => 'language',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_INSERT | self::OP_UPDATE,
        ];
    }  
    
    /**
     * @inheritdoc
     */
    public function getTranslations()
    {
        return $this->hasMany(Article_i18n::className(), ['article_id' => 'id']);
    }    

    /**
     * @inheritdoc 
     */
    public function rules() {
        return [
            [['title'], 'required'],
            [['number', 'category_id', 'thumbnail_id', 'gallery_id', 'author_id', 'sort', 'create_user_id', 'update_user_id', 'status_id','comment_show','comment_add','is_more_info'], 'integer'],
            [['size', 'technique', 'dimensions', 'date_text', 'place', 'city', 'year', 'short_description', 'full_description', 'seo_description', 'summary_description', 'url'], 'string'],
            [['active_from', 'active_to', 'date_created', 'date_updated', 'date_published', 'date_display', 'add_categories_hash'], 'safe'],
            [['title', 'slug', 'extlink', 'seo_title', 'seo_keywords'], 'string', 'max' => 1024],
            [['thumbnail_description'], 'string', 'max' => 500],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => ArticleStatus::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ArticlesCategories::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['create_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['create_user_id' => 'id']],
            [['gallery_id'], 'exist', 'skipOnError' => true, 'targetClass' => Gallery::className(), 'targetAttribute' => ['gallery_id' => 'id']],
            [['thumbnail_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['thumbnail_id' => 'id']],
            [['update_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['update_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc 
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('backend_module_articles', 'ID'),
            'category_id' => Yii::t('backend_module_articles', 'Category ID'),
            'title' => Yii::t('backend_module_articles', 'Title'),
            'slug' => Yii::t('backend_module_articles', 'Slug'),
            'extlink' => Yii::t('backend_module_articles', 'Extlink'),
            'summary_description' => Yii::t('backend_module_articles', 'Summary Description'),
            'short_description' => Yii::t('backend_module_articles', 'Short Description'),
            'full_description' => Yii::t('backend_module_articles', 'Full Description'),
            'thumbnail_id' => Yii::t('backend_module_articles', 'Thumbnail ID'),
            'gallery_id' => Yii::t('backend_module_articles', 'Gallery'),
            'active_from' => Yii::t('backend_module_articles', 'Active From'),
            'active_to' => Yii::t('backend_module_articles', 'Active To'),
            'sort' => Yii::t('backend_module_articles', 'Sort'),
            'status_id' => Yii::t('backend_module_articles', 'Status'),
            'create_user_id' => Yii::t('backend_module_articles', 'Create User ID'),
            'date_created' => Yii::t('backend_module_articles', 'Date Created'),
            'update_user_id' => Yii::t('backend_module_articles', 'Update User ID'),
            'date_display' => Yii::t('backend_module_articles', 'Display Date'),
            'date_updated' => Yii::t('backend_module_articles', 'Date Updated'),
            'seo_title' => Yii::t('backend_module_articles', 'Seo title'),
            'seo_description' => Yii::t('backend_module_articles', 'Seo descrtiption'),
            'seo_keywords' => Yii::t('backend_module_articles', 'seo_keywords'),
            'url' => Yii::t('backend_module_articles', 'url'),
            'comment_show' => Yii::t('backend_module_articles', 'Comment Show'),
            'comment_add' => Yii::t('backend_module_articles', 'Comment Add'),
        ];
    }

    public function getAbsoluteUrl() {
        return trim(BaseUrl::home(true), "/").$this->getUrl();
    }

    public function getUrl() {
        if(Yii::$app->params['oConfig']->default_language != Yii::$app->session->get('frontend_language')){
            $sLang = strtolower(current(explode("-", Yii::$app->session->get('frontend_language'))));
            $sUrl  = "/{$sLang}/{$this->slug}";
        } else { $sUrl = "/{$this->slug}"; }
        
        if ($this->getCategory()) {
            $sUrl = $this->getCategory()->getExtendUrl() . $this->slug;
        }
        return $sUrl;
    }

    public function hasType($typeID) {
        foreach ($this->getTypes()->all() as $oTypeHash) {
            if ($typeID == $oTypeHash->type_id) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery 
     */
    public function getCategory() {
        return $this->hasOne(ArticlesCategories::className(), ['id' => 'category_id'])->one();
    }

    /**
     * @return \yii\db\ActiveQuery 
     */
    public function getCreateUser() {
        return $this->hasOne(User::className(), ['id' => 'create_user_id'])->one();
    }

    /**
     * @return \yii\db\ActiveQuery 
     */
    public function getGallery() {
        return $this->hasOne(Gallery::className(), ['id' => 'gallery_id'])->one();
    }

    /**
     * @return \yii\db\ActiveQuery 
     */
    public function getAuthor() {
        return $this->hasOne(Author::className(), ['id' => 'author_id'])->one();
    }

    /**
     * @return \yii\db\ActiveQuery 
     */
    public function getVideo() {
        return $this->hasOne(ArticleVideo::className(), ['article_id' => 'id'])->one();
    }

    /**
     * @return \yii\db\ActiveQuery 
     */
    public function getHistory() {
        return $this->hasMany(ArticleHistory::className(), ['article_id' => 'id'])->orderBy("ID DESC")->all();
    }

    /**
     * @return \yii\db\ActiveQuery 
     */
    public function getCommentsCount() {
        return $this->hasMany(ArticleComment::className(), ['article_id' => 'id'])->andWhere(['is_active' => 1])->count();
    }

    /**
     * @return \yii\db\ActiveQuery 
     */
    public function getComments($limit = 20, $offset = 0) {
        return $this->hasMany(ArticleComment::className(), ['article_id' => 'id'])->where(["=", "is_active", 1])->andWhere(["IS", "parent_id", NULL])->limit($limit)->offset($offset)->orderBy("date_created DESC")->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleFiles() {
        return $this->hasMany(ArticleFile::className(), ['article_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags() {
        return $this->hasMany(TagArticle::className(), ['article_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelated() {
        return $this->hasMany(ArticleRelated::className(), ['article_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery 
     */
    public function getThumbnail($height = null, $width = null, $type = "crop", $effect = null, $quality = 80) {
        return File::getThumbnail($this->thumbnail_id, $width, $height, $type, $effect, $quality);
    }

    /**
     * @return \yii\db\ActiveQuery 
     */
    public function hasThumbnail() {
        return !is_null($this->thumbnail_id);
    }

    /**
     * @return \yii\db\ActiveQuery 
     */
    public function getUpdateUser() {
        return $this->hasOne(User::className(), ['id' => 'update_user_id'])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypes() {
        return $this->hasMany(TypeHash::className(), ['article_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories() {
        if (!$this->add_categories_hash) {
            return [];
        }
        return json_decode($this->add_categories_hash);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus() {
        return $this->hasOne(ArticleStatus::className(), ['id' => 'status_id']);
    }
    
    /**
     * @inheritdoc
     */
    public function __set($name, $value) {
        if(in_array($name, ['status_id', 'title', 'slug', 'short_description', "full_description", 'date_text', 'place', 'city', "is_more_info"])){
            $this->translate(Yii::$app->session->get('backend_language'))->{$name} = $value;
        }
        
        parent::__set($name, $value);
    }
    
    public function __get($name) {
        if(in_array($name, ['status_id','title', 'slug', 'short_description', "full_description", 'date_text', 'place', 'city', "is_more_info"])){
            if(isset(Yii::$app->request->url) && strpos(Yii::$app->request->url, \Yii::$app->request->baseUrl) !== false && \Yii::$app->request->baseUrl == "/__fBFyVd/"){
                $translatedAttr = $this->translate(Yii::$app->session->get('backend_language'))->{$name};
            } else {
                $translatedAttr = $this->translate(Yii::$app->session->get('frontend_language'))->{$name};
            }

            if($translatedAttr){
                return $translatedAttr;
            }
        }
        return parent::__get($name);
    }

}
