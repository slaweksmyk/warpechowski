<?php

namespace common\modules\articles\models;

use Yii;
use common\modules\articles\models\Article;

/**
 * This is the model class for table "xmod_articles_video".
 *
 * @property integer $id
 * @property string $article_id
 * @property string $youtube_id
 * @property integer $start_from
 * @property integer $is_fullscreen
 * @property integer $is_controlls
 * @property integer $is_title
 * @property integer $is_featured
 *
 * @property Article $article
 */
class ArticleVideo extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_articles_video}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article_id', 'start_from', 'is_fullscreen', 'is_controlls', 'is_title', 'is_featured'], 'integer'],
            [['youtube_id'], 'string', 'max' => 255],
            [['article_id'], 'exist', 'skipOnError' => true, 'targetClass' => Article::className(), 'targetAttribute' => ['article_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_article', 'ID'),
            'article_id' => Yii::t('backend_module_article', 'Article ID'),
            'youtube_id' => Yii::t('backend_module_article', 'Youtube ID'),
            'start_from' => Yii::t('backend_module_article', 'Start From'),
            'is_fullscreen' => Yii::t('backend_module_article', 'Is Fullscreen'),
            'is_controlls' => Yii::t('backend_module_article', 'Is Controlls'),
            'is_title' => Yii::t('backend_module_article', 'Is Title'),
            'is_featured' => Yii::t('backend_module_article', 'Is Featured'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'article_id']);
    }
    
    public function generateEmbed($height = null, $width = null){

        $sHeight = "";
        if($height){ $sHeight = "height='{$height}'"; }
        
        $sWidth = "";
        if($width){ $sWidth = "width='{$width}'"; }
        
        $sFullScreen = "";
        if($this->is_fullscreen){ $sFullScreen = "allowfullscreen"; }
        
        $aUrlParams = [];
        if($this->start_from > 0){ $aUrlParams["start"] = $this->start_from; }
        if($this->is_controlls == 0){ $aUrlParams["controls"] = 0; }
        if($this->is_title == 0){ $aUrlParams["showinfo"] = 0; }
        if($this->is_featured == 0){ $aUrlParams["rel"] = 0; }
        
        $sAddQuery = http_build_query($aUrlParams);
        $sSrc = "src='https://www.youtube.com/embed/{$this->youtube_id}?{$sAddQuery}' ";
        
        return "<iframe {$sWidth} {$sHeight} {$sSrc} {$sSrc} {$sFullScreen} frameborder='0'></iframe>";
    }
    
}
