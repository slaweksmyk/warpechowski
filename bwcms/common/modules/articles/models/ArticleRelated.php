<?php

namespace common\modules\articles\models;

use Yii;
use common\modules\articles\models\Article;

/**
 * This is the model class for table "xmod_articles_related".
 *
 * @property integer $id
 * @property integer $article_id
 * @property integer $related_id
 *
 * @property Article $related
 * @property Article $article
 */
class ArticleRelated extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_articles_related}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article_id', 'related_id'], 'integer'],
            [['related_id'], 'exist', 'skipOnError' => true, 'targetClass' => Article::className(), 'targetAttribute' => ['related_id' => 'id']],
            [['article_id'], 'exist', 'skipOnError' => true, 'targetClass' => Article::className(), 'targetAttribute' => ['article_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_articles', 'ID'),
            'article_id' => Yii::t('backend_module_articles', 'Article ID'),
            'related_id' => Yii::t('backend_module_articles', 'Related ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelated()
    {
        return $this->hasOne(Article::className(), ['id' => 'related_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'related_id']);
    }
}
