<?php

namespace common\modules\articles\models;

use Yii;
use common\modules\articles\models\Article;

/**
 * This is the model class for table "xmod_articles_comments".
 *
 * @property integer $id
 * @property string $article_id
 * @property string $text
 * @property string $author
 * @property string $date_created
 * @property string $date_updated
 *
 * @property Article $article
 */
class ArticleComment extends \common\hooks\yii2\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%xmod_articles_comments}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['article_id', 'is_active'], 'integer'],
            [['text'], 'string'],
            [['date_created', 'date_updated'], 'safe'],
            [['author'], 'string', 'max' => 255],
            [['article_id'], 'exist', 'skipOnError' => true, 'targetClass' => Article::className(), 'targetAttribute' => ['article_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('backend_module_article', 'ID'),
            'article_id' => Yii::t('backend_module_article', 'Article ID'),
            'text' => Yii::t('backend_module_article', 'Text'),
            'author' => Yii::t('backend_module_article', 'Author'),
            'date_created' => Yii::t('backend_module_article', 'Date Created'),
            'date_updated' => Yii::t('backend_module_article', 'Date Updated'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle() {
        return $this->hasOne(Article::className(), ['id' => 'article_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent() {
        return $this->hasOne(ArticleComment::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments() {
//        if(!\Yii::$app->user->isGuest){
//            return $this->hasMany(ArticleComment::className(), ['parent_id' => 'id']);
//        } else {
        return $this->hasMany(ArticleComment::className(), ['parent_id' => 'id'])->where(["=", "is_active", 1]);
        //     }
    }

}
