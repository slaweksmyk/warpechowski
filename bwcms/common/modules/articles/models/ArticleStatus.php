<?php

namespace common\modules\articles\models;

use Yii;
use common\modules\articles\models\Article;

/**
 * This is the model class for table "xmod_articles_statuses".
 *
 * @property integer $id
 * @property string $name
 * @property integer $is_published
 *
 * @property Article[] $xmodArticles
 */
class ArticleStatus extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_articles_statuses}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_published'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_articles', 'ID'),
            'name' => Yii::t('backend_module_articles', 'Name'),
            'is_published' => Yii::t('backend_module_articles', 'Is Published'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasMany(Article::className(), ['status_id' => 'id']);
    }
}
