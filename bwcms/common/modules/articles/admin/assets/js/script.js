$(document).ready(function(){
    
    var $_GET = {};
    document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
        function decode(s) {
            return decodeURIComponent(s.split("+").join(" "));
        }
        $_GET[decode(arguments[1])] = decode(arguments[2]);
    });

    $(window).on("load",function(){
        $(".article-sliders").each(function(){
            var selectedSliderOption = $(this).find(":selected");
            if(selectedSliderOption.data("subcategory")){}
        });
    });

    /* Article autosave */
    setInterval(function(){ 
        $.post($("#homeDir").text()+"articles/article/autosave/", { id: $_GET["id"] })
        .done(function(data) {
            $(".autosave").fadeIn(2000).fadeOut(800);
        }); 
    }, 480000);

    $("body").on("dragover", ".download-panel", function(e){
        e.preventDefault();
    });
    
    $(".download-panel").bind("drop", function(e) {
        var files = e.originalEvent.dataTransfer.files;
        var formData = new FormData();
        var file = files[0];
        
        formData.append("UploadForm[imageFile]", file);
        formData.append("UploadForm[category_id]", 0);

        $.ajax({
            type: 'POST',
            url: $("#homeDir").text()+"files/manager/create/",
            async: true,
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        }).done(function(data) {
            var obj = jQuery.parseJSON(data);
            $.post($("#homeDir").text()+"articles/article/add-download-file/", { articleID: $_GET["id"], fileID: obj.iFileID})
            .done(function(data) {
                location.reload();
            });
        });

        return false;
    });

    $("body").on("dragover", ".drag-upload", function(e){
        e.preventDefault();
    });
    
    $(".drag-upload").bind("drop", function(e) {
        var files = e.originalEvent.dataTransfer.files;
        
        var error = "";
        var formData = new FormData();
        var file = files[0];
        var extension = file.name.split('.').pop();
        
        formData.append("UploadForm[imageFile]", file);
        formData.append("UploadForm[category_id]", 0);
        
        if (file.size > 1024000) {
            alert("Rozmiar zdjęcia przekracza 1MB!");
            return;
        }

        if(file.type.match('image.*')) {
            $.ajax({
                type: 'POST',
                url: $("#homeDir").text()+"files/anager/create/",
                async: true,
                data: formData,
                cache: false,
                contentType: false,
                processData: false
            }).done(function(data) {
                var obj = jQuery.parseJSON(data);
                replaceThumbnailID($_GET["id"], obj.iFileID);
            });;
        } else {
            alert("Wybrany plik nie jest grafiką.");
            return;
        }

        return false;
    });
    
    $(".drag-upload").bind("dragenter", function(e) {
        $(this).find("p").text("Upuść plik tutaj!");
    });
    
    $(".drag-upload").bind("dragleave", function(e) {
        $(this).find("p").text("Upuść plik w dowolnym miejscu w tym obszarze, aby wysłać je na serwer");
    });
    
    $("body").on("change", "#add-file-upload", function(){
        var formData = new FormData();
        var file = $(this).get(0).files[0];
        
        formData.append("UploadForm[imageFile]", file);
        formData.append("UploadForm[category_id]", 0);
        
        $.ajax({
            type: 'POST',
            url: $("#homeDir").text()+"files/manager/create/",
            async: true,
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        }).done(function(data) {
            var obj = jQuery.parseJSON(data);
            $.post($("#homeDir").text()+"articles/article/add-download-file/", { articleID: $_GET["id"], fileID: obj.iFileID})
            .done(function(data) {
                location.reload();
            });
        });
    });
    
    $("body").on("change", "#upload-thumbnail", function(){
        var error = "";
        var formData = new FormData();
        var file = $(this).get(0).files[0];
        var extension = file.name.split('.').pop();
        
        formData.append("UploadForm[imageFile]", file);
        formData.append("UploadForm[category_id]", 0);
        
        if (file.size > 1024000) {
            alert("Rozmiar zdjęcia przekracza 1MB!");
            return;
        }

        if(file.type.match('image.*')) {
            $.ajax({
                type: 'POST',
                url: $("#homeDir").text()+"files/manager/create/",
                async: true,
                data: formData,
                cache: false,
                contentType: false,
                processData: false
            }).done(function(data) {
                var obj = jQuery.parseJSON(data);
                replaceThumbnailID($_GET["id"], obj.iFileID);
            });;
        } else {
            alert("Wybrany plik nie jest grafiką.");
            return;
        }
        
        return false;
    });
    
    $("body").on("change", "#add-file-download", function(){
        $.post($("#homeDir").text()+"articles/article/add-download-file/", { articleID: $_GET["id"], fileID: $(this).val()})
        .done(function(data) {
            location.reload();
        });
    });

    $("body").on("change", "#change-thumbnail", function(){
        replaceThumbnailID($_GET["id"], $(this).val());
    });

    $("body").on("click", ".slider-more", function(){
        var clone = $(".slider-row:first-child").clone();
        clone.find("select").each(function(){
            $(this).attr("name", $(this).attr("name").replace("[0]", "[1]"));
        });
        $(".slider-body").append(clone);

        return false;
    });
    
    $("body").on("click", ".slider-less", function(){
        $(this).parentsUntil($(".slider-row")).parent().remove();
        
        return false;
    });
    
    function replaceThumbnailID(articleID, imageID){
        $("#article-thumbnail_id").replaceWith("<input type='hidden' name='Article[thumbnail_id]' value='"+imageID+"' />");
        $.post($("#homeDir").text()+"articles/article/replace-thumbnail/", { articleID: articleID, imageID: imageID})
        .done(function(data) {
            $("#w0").submit();
        });
    }
    
});