<?php

namespace common\modules\articles\admin;

/**
 * files module definition class
 */
class Articles extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\articles\admin\controllers';
    public $defaultRoute = 'article';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
}
