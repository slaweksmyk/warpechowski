<?php
use yii\helpers\Html;
use common\modules\categories\models\ArticlesCategories;

use backend\helpers\ArticleCategoryHelper;
?>

<div class="row">
    <div class="col-5"><b>Menu akcji</b></div>
    <div class="col-4"><b>Lista kategorii artykułowych</b></div>
    <div class="col-3"></div>
</div>

<div class="row">
    <div class="col-5">
        <p>
            <?= Html::a('<span class="glyphicon glyphicon-plus"></span> '.Yii::t('backend_module_articles', 'create_article'), ["create"], ['class' => 'btn btn-success btn-sm']) ?>
            <?= Html::a(Yii::t('backend_module_articles', 'create_article_category'), \Yii::$app->request->baseUrl."categories/categories/create/", ['class' => 'btn btn-outline-secondary btn-sm']) ?>
            
            <?php if(Yii::$app->request->get("deleted") != 1){ ?>
                <?= Html::a('<span class="glyphicon glyphicon-trash"></span> Zobacz kosz', ["index", "deleted" => 1], ['class' => 'btn btn-outline-secondary btn-sm', 'title' => "Zobacz usunięte artykuły"]) ?>
            <?php } else { ?>
                <?= Html::a("Wróć do listy", ["index"], ['class' => 'btn btn-outline-secondary btn-sm']) ?>
            <?php } ?>
        </p>
    </div>
    <div class="col-4 cat-list-box">
        <div class="category-list">
            <?php foreach(ArticlesCategories::find()->all() as $oCategory){ ?>
                <?php if($oCategory->id == Yii::$app->request->get("id")){ ?>
                    <div class="btn-group">
                        <button type="button" class="btn <?php if($oCategory->id == \Yii::$app->request->get('id')){ echo "btn-warning btn-sm"; } else { echo "btn-primary"; } ?>" onClick="window.location.href = \Yii::$app->request->baseUrl.'articles/article/articles/?id=<?= $oCategory->id ?>';"><?= $oCategory->name ?></button>
                        <?php if(count($oCategory->getCategories()->all()) > 0){ ?>
                            <button type="button" class="btn <?php if($oCategory->id == \Yii::$app->request->get('id')){ echo "btn-warning btn-sm"; } else { echo "btn-primary"; } ?> dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="caret"></span>
                            </button>
                            <?= ArticleCategoryHelper::generateCategoryTreeAsTable($oCategory->id); ?>
                        <?php } ?>
                    </div>
                <?php } ?>
            <?php } ?>

            <?php foreach(ArticlesCategories::find()->where(["IS", "parent_id", NULL])->all() as $oCategory){ ?>
                <?php if($oCategory->id != Yii::$app->request->get("id")){ ?>
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary btn-sm" onClick="window.location.href = \Yii::$app->request->baseUrl.'articles/article/articles/?id=<?= $oCategory->id ?>';"><?= $oCategory->name ?></button>
                        <?php if(count($oCategory->getCategories()->all()) > 0){ ?>
                            <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="caret"></span>
                            </button>
                            <?= ArticleCategoryHelper::generateCategoryTreeAsTable($oCategory->id); ?>
                        <?php } ?>
                    </div>
                <?php } ?>
            <?php } ?>
            <div class="list-close" title="Pokaż wszystkie kategorie" data-toggle="tooltip" data-placement="bottom">x</div>
        </div>
    </div>
    <div class="col-3 cat-list-button">
        <div class="btn-group" style="float: right;">
            <button type="button" class="btn btn-outline-secondary btn-sm article-qmenu-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Pokaż wszystkie kategorie
                <span class="caret"></span>
            </button>
        </div>
    </div>
</div>