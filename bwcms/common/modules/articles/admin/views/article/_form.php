<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\modules\files\models\File;
use common\modules\slider\models\Slider;
use common\modules\authors\models\Author;
use common\modules\articles\models\ArticleStatus;
use common\modules\categories\models\ArticlesCategories;
use common\modules\gallery\models\Gallery;

$this->registerJsFile('/bwcms/common/modules/articles/admin/assets/js/script.js', ['depends' => [yii\web\JqueryAsset::className()]]);
$this->registerCssFile('/bwcms/common/modules/articles/admin/assets/css/styles.css');
$this->registerJsFile('/bwcms/common/modules/comments/admin/assets/js/script.js', ['depends' => [yii\web\JqueryAsset::className()]]);
$this->registerCssFile('/bwcms/common/modules/comments/admin/assets/css/style.css');
?>

<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-8">
        <section class="panel full" >
            <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend', 'content') ?></header>
            <div class="panel-body" >
                <div class="article-form">
                    <div class="row">
                        <div class="col-4"><?= $form->field($model, 'number')->textInput(["type" => "number", "min" => "0"])->label('Numer') ?></div>
                        <div class="col-4"><?= $form->field($model, 'date_text')->textInput()->label('Data (opis)') ?></div>
                        <div class="col-4"><?= $form->field($model, 'place')->textInput()->label('Miejsce') ?></div>
                        <div class="col-4"><?= $form->field($model, 'city')->textInput()->label('Miasto') ?></div>
                        <div class="col-4"><?= $form->field($model, 'year')->textInput()->label('Rok') ?></div>
                        <div class="col-4"><?= $form->field($model, 'is_more_info')->dropDownList([1 => Yii::t('backend', 'yes'), 0 => Yii::t('backend', 'no')])->label('Więcej informacji') ?></div>
                        <div class="col-4"><?= $form->field($model, 'technique')->textInput()->label('Technika') ?></div>
                        <div class="col-4"><?= $form->field($model, 'size')->textInput()->label('Wymiary') ?></div>
                        <div class="col-4"><?= $form->field($model, 'dimensions')->textInput()->label('Z kolekcji') ?></div>
                    </div>
                    <div class="row">
                        <div class="col-12"><?= $form->field($model, 'short_description')->textarea(['rows' => 6]) ?></div>
                    </div>
                    <div class="row">
                        <div class="col-12"><?= $form->field($model, 'full_description')->textarea(['rows' => 6]) ?></div>
                    </div>
                    <div class="row">
                        <div class="col-12"><?= $form->field($model, 'summary_description')->textarea(['rows' => 6]) ?></div>
                    </div>
                </div>
            </div>
        </section>

        <section class="panel full">
            <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend', 'parameters') ?></header>
            <div class="panel-body">
                <div class="row">
                    <?php if ($model->getCategory() && $model->getCategory()->getExtendPage()->one()) { ?>
                        <div class="col-12">
                            <div class="form-group">
                                <label class="control-label" for="article-url">Adres URL rozwinięcia artykułu:</label>
                                <input type="text" id="article-url" class="form-control" value="<?= Yii::$app->request->getHostInfo() ?>/<?= $model->getCategory()->getExtendPage()->one()->url ?>/<?= $model->slug ?>" aria-invalid="false" readonly>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="col-12">
                        <?= $form->field($model, 'extlink')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-4">
                        <?= $form->field($model, 'sort')->textInput(["type" => "number", "min" => "0"]) ?>
                    </div>
                    <div class="col-4">
                        <?= $form->field($model, 'date_display')->textInput(["class" => "datepicker form-control"])->label("Data wyświetlana") ?>
                    </div>
                    <div class="col-4">
                        <?= $form->field($model, 'date_published')->textInput(["class" => "datepicker form-control"])->label("Data opublikowania") ?>
                    </div>
                    <hr>

                    <?php if ($model->getGallery()) { ?>
                        <div class="col-12">
                            <span class="label-delete">
                                <input type="hidden" name="Article[gallery_id]" value="<?= $model->gallery_id ?>"/>
                                <span title="Usuń galerię">x</span>
                                <?= $model->getGallery()->name ?>
                            </span>
                        </div>
                    <?php } ?>

                    <div class="col-12">
                        <?= $form->field($model, 'gallery_id')->dropDownList(ArrayHelper::map(Gallery::find()->all(), 'id', 'name'), ["prompt" => 'Wyszukaj i dodaj galerię']); ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="panel full">
            <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Oznaczenia artykułu</header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label class="control-label" for="categories">Dodatkowe kategorie artykułu</label>
                            <div class="row">
                                <div class="col-12 article-types">
                                    <?php foreach ($model->getCategories() as $iCategoryID) { ?>
                                        <span>
                                            <input type="hidden" name="Article[add_categories][]" value="<?= $iCategoryID ?>"/>
                                            <span title="Usuń kategorie artykułu">x</span>
                                            <?php $oCat = ArticlesCategories::find()->where(["=", "id", $iCategoryID])->one();
                                            if (!is_null($oCat)) { echo $oCat->name; } ?>
                                        </span>
                                    <?php } ?>
                                </div>
                            </div>
                            <select id="categories" class="form-control ajax-load-categories" name="Article[add_categories][]"></select>
                            <div class="help-block"></div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group">
                            <label class="control-label" for="types">Typy artykułu</label>
                            <div class="row">
                                <div class="col-12 article-types">
                                    <?php foreach ($model->getTypes()->all() as $oTypeHash) { ?>
                                        <span class="label-delete">
                                            <input type="hidden" name="Article[types][]" value="<?= $oTypeHash->type_id ?>"/>
                                            <span title="Usuń typ artykułu">x</span>
                                            <?= $oTypeHash->getType()->one()->name ?>
                                        </span>
                                    <?php } ?>
                                </div>
                            </div>
                            <select id="types" class="form-control ajax-load-article-types" name="Article[types][]"></select>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label class="control-label" for="tags">Tagi artykułu</label>
                            <div class="row">
                                <div class="col-12 article-types">
                                    <?php foreach ($model->getTags()->all() as $oTagsHash) { ?>
                                        <span class="label-delete">
                                            <input type="hidden" name="Article[tags][]" value="<?= $oTagsHash->tag_id ?>"/>
                                            <span title="Usuń tag artykułu">x</span>
                                            <?= $oTagsHash->getTag()->one()->name ?>
                                        </span>
                                    <?php } ?>
                                </div>
                            </div>
                            <select id="types" class="form-control ajax-load-article-tags" name="Article[tags][]"></select>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label class="control-label" for="related">Artykuły powiązane (zobacz także)</label>
                            <div class="row">
                                <div class="col-12 article-types">
                                    <?php foreach ($model->getRelated()->all() as $oRelatedHash) { ?>
                                        <span class="label-delete">
                                            <input type="hidden" name="Article[related][]" value="<?= $oRelatedHash->related_id ?>"/>
                                            <span title="Usuń powiązanie">x</span>
                                            <?= $oRelatedHash->getRelated()->one()->title ?>
                                        </span>
                                    <?php } ?>
                                </div>
                            </div>
                            <select id="types" class="form-control ajax-load-articles" name="Article[related][]"></select>
                            <div class="help-block"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="col-4">
        <div class="autosave">Wykonano autozapis...</div>
        <section class="panel full">
            <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Główne ustawienia</header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-12">
                        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-6">
                        <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(ArticlesCategories::find()->all(), 'id', 'name'), ["prompt" => Yii::t('backend_module_articles', '- select category -')]); ?>
                    </div>
                    <div class="col-6">
                        <?= $form->field($model, 'author_id')->dropDownList(ArrayHelper::map(
                            Author::find()->all(), 'id', function($model) {
                                return $model->firstname . ' ' . $model->surname;
                            }), ["prompt" => "Wybierz autora"])->label("Autor");
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_articles', 'create') : Yii::t('backend', 'save_changes'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'style' => 'display: block; margin: 0 auto;']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="panel full" >
            <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend', 'activity') ?></header>
            <div class="panel-body" >
                <div class="article-form">
                    <div class="row">
                        <div class="col-12"><?= $form->field($model, 'status_id')->dropDownList(ArrayHelper::map(ArticleStatus::find()->all(), 'id', 'name'))->label("Status") ?></div>
                        <div class="col-6"><?= $form->field($model, 'active_from')->textInput(["class" => "datepicker form-control"]) ?></div>
                        <div class="col-6"><?= $form->field($model, 'active_to')->textInput(["class" => "datepicker form-control"]) ?></div>
                    </div>
                </div>
            </div>
        </section>

        <section class="panel full">
            <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Historia zmian</header>
            <div class="panel-body" style="max-height: 250px; overflow: auto;">
                <div class="list-group">
                    <?php foreach ($model->getHistory() as $index => $oHistory) { ?>
                        <a href=<?= \Yii::$app->request->baseUrl ?>."articles/article/diff/?id=<?= $oHistory->id ?>&article_id=<?= $model->id ?>" title="Przywróć wersje artykułu z dn.<?= date("d.m.Y H:i:s", strtotime($oHistory->create_date)) ?>" data-placement="bottom" data-toggle="tooltip" class="list-group-item" <?php
                        if ($oHistory->is_autosave) { echo "style='background: #eee;'"; }
                        ?>>Wersja z dn.<?= date("d.m.Y H:i:s", strtotime($oHistory->create_date)) ?></a>
                       <?php } ?>
                </div>
            </div>
        </section>

        <?php if (!$model->isNewRecord) { ?>
            <ul class="nav nav-tabs nav-justified">
                <li class="active">
                    <a href="#thumbnail" data-toggle="tab">Zdjęcie główne</a>
                </li>
                <li>
                    <a href="#video" data-toggle="tab">Film Youtube</a>
                </li>
            </ul>

            <div class="tab-content ">
                <div class="tab-pane active" id="thumbnail">
                    <section class="panel full">
                        <header>
                            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Zdjęcie główne
                            <?php if ($model->thumbnail_id) { ?>
                                <div class="pull-right" style="margin-right: 5px;">
                                    <a href=<?= \Yii::$app->request->baseUrl ?>."gallery/gallery/image_crop/?id=<?= $model->thumbnail_id ?>" style="position: relative; top: -5px;" title="Kadruj zdjęcie" data-toggle="tooltip" data-placement="bottom">
                                        <span class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-resize-full"></span></span>
                                    </a>

                                    <?=
                                    Html::a('<span class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></span>', ['article/remove-thumbnail', 'id' => $model->id], [
                                        'style' => 'position: relative; top: -5px;',
                                        'title' => 'Usuń zdjęcie główne',
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'bottom',
                                        'data' => [
                                            'confirm' => 'Czy na pewno chcesz usunąć zdjęcie główne?',
                                            'method' => 'post',
                                        ],
                                    ]);
                                    ?>
                                </div>
                            <?php } ?>
                        </header>
                        <div class="panel-body" style="text-align: center; font-size: 12px; font-weight: bold; color: #797c87;">
                            <div class="row">
                                <div class="col-12">
                                    <div class="drag-upload">
                                        <?php if ($model->thumbnail_id) { ?>
                                            <img src="/upload/<?= File::find()->where(["=", "id", $model->thumbnail_id])->one()->filename ?>" style="margin: 0 auto; max-height: 203px;" class="img-responsive"/>
                                        <?php } else { ?>
                                            <img src=<?= \Yii::$app->request->baseUrl ?>."images/no-image.png" style="margin: 0 auto;" class="img-responsive"/>
                                            <p>Brak zdjęcia</p>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <hr/>
                                </div>
                                <div class="col-12">
                                    <?= $form->field($model, 'thumbnail_description')->textInput()->label("Opis zdjęcia") ?>
                                </div>
                                <div class="col-12">
                                    <a href="#" data-init="file-manager" data-target="#change-thumbnail" title="Wybierz zdjęcie z menadżera plików" data-toggle="tooltip" data-placement="bottom">
                                        <span class="btn btn-success btn-md" style="width: 100%;">Wybierz zdjęcie z menadżera plików</span>
                                    </a>
                                    <input type="hidden" id="change-thumbnail"/>
                                </div>   
                                <div class="col-12" style="margin-top: 10px;">

                                </div>
                                <div class="col-12" style="position: relative; top: -6px;">
                                    lub
                                </div>
                                <div class="col-12">
                                    <label style="width: 100%;">
                                        <span class="btn btn-success btn-md" style="width: 100%;" title="Wgraj plik z komputera" data-toggle="tooltip" data-placement="bottom">Wgraj plik ze swojego komputera</span>
                                        <input type="file" id="upload-thumbnail" accept="image/*" style="display: none;" />
                                    </label>
                                    <p style="text-align: center; padding-top: 5px;"><small>*Wgrywając plik z komputera, tworzysz automatycznie folder z plikami o nazwie Twojego użytkownika</small></p>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="tab-pane" id="video">
                    <section class="panel full">
                        <header>
                            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Film Youtube
                            <?php if ($model->getVideo()) { ?>
                                <div class="pull-right" style="margin-right: 5px;">
                                    <a href=<?= \Yii::$app->request->baseUrl ?>."articles/article/remove-video/?id=<?= $model->id ?>" style="position: relative; top: -5px;" title="Usuń film" data-toggle="tooltip" data-placement="bottom">
                                        <span class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></span>
                                    </a>
                                </div>
                            <?php } ?>
                        </header>
                        <div class="panel-body">
                            <input type="hidden" name="ArticleVideo[article_id]" value="<?= $model->id ?>" />
                            <?php if ($video->youtube_id) { ?>
                                <div class="row">
                                    <div class="col-12">
                                        <?= $video->generateEmbed(200, "100%"); ?>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="row">
                                <?php if ($video->youtube_id) { ?>
                                    <div class="col-12">
                                        <hr/>
                                    </div>
                                <?php } ?>
                                <div class="col-8">
                                    <?= $form->field($video, 'youtube_id')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-4">
                                    <?= $form->field($video, 'start_from')->textInput(['maxlength' => true]) ?>
                                </div>   
                                <div class="col-6">
                                    <?= $form->field($video, 'is_fullscreen')->dropDownList([1 => Yii::t('backend', 'yes'), 0 => Yii::t('backend', 'no')]) ?>
                                </div>
                                <div class="col-6">
                                    <?= $form->field($video, 'is_controlls')->dropDownList([1 => Yii::t('backend', 'yes'), 0 => Yii::t('backend', 'no')]) ?>
                                </div>
                                <div class="col-6">
                                    <?= $form->field($video, 'is_title')->dropDownList([1 => Yii::t('backend', 'yes'), 0 => Yii::t('backend', 'no')]) ?>
                                </div>
                                <div class="col-6">
                                    <?= $form->field($video, 'is_featured')->dropDownList([0 => Yii::t('backend', 'no'), 1 => Yii::t('backend', 'yes')]) ?>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        <?php } ?>

        <section class="panel full">
            <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Dodaj artykuł do slidera</header>
            <div class="panel-body slider-body">
                <?php if (count($oSliderItems) > 0) { ?>
                    <?php foreach ($oSliderItems as $index => $oSliderItem) { ?>
                        <div class="row slider-row">
                            <div class="col-9" id="sliders-cnt">
                                <div class="form-group field-article-asliders">
                                    <label class="control-label" for="article-asliders">Wybierz slider</label>
                                    <select class="form-control article-sliders" name="Article[aSliders][<?= $index ?>][slider_id]">
                                        <option value="NULL">- wybierz slider -</option>
                                        <?php $oMultiSlider = null; ?>
                                        <?php foreach (Slider::find()->all() as $oSlider) { ?>
                                            <option value="<?= $oSlider->id ?>" <?php if ($oSlider->id == $oSliderItem->slider_id) { ?><?php
                                            if ($oSlider->type_id == 3) { $oMultiSlider = $oSlider; ?>data-subcategory="true"<?php } ?> selected<?php } ?>><?= $oSlider->title ?></option>
                                        <?php } ?>
                                    </select>
                                    <div class="help-block"></div>
                                </div>
                                <?php if (!is_null($oMultiSlider)) { ?>
                                    <div class="form-group field-article-asliders">
                                        <label class="control-label" for="article-asliders">Wybierz zakładkę</label>
                                        <select class="form-control article-sliders" name="Article[aSliders][<?= $index ?>][parent_id]">
                                            <option value="NULL">- wybierz zakładkę -</option>
                                            <?php foreach ($oMultiSlider->getSliderItemsRaw()->where(["=", "parent_id", 0])->all() as $oSliderTuck) { ?>
                                                <option value="<?= $oSliderTuck->id ?>" <?php if ($oSliderItem->parent_id == $oSliderTuck->id) { ?>selected<?php } ?>><?= $oSliderTuck->title ?></option>
                                            <?php } ?>
                                        </select>
                                        <div class="help-block"></div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="col-3">
                                <div class="row">
                                    <div class="col-12" style="padding-top: 29px;">
                                        <?= Html::a("+", "#", ['class' => 'btn btn-xs slider-more btn-success', 'data-toggle' => "tooltip", "data-placement" => "bottom", "title" => "Dodaj slider"]) ?>
                                        <?= Html::a("-", "#", ['class' => 'btn btn-xs slider-less btn-danger', 'data-toggle' => "tooltip", "data-placement" => "bottom", "title" => "Usuń slider", 'style' => "width: 22px;"]) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } else { ?> 
                    <div class="row slider-row">
                        <div class="col-9">
                            <?= $form->field($model, 'aSliders[0][slider_id]')->dropDownList(ArrayHelper::map(Slider::find()->all(), 'id', 'title'), ["prompt" => "- wybierz slider -", "class" => "form-control no-select2"])->label("Wybierz slider"); ?>
                        </div>
                        <div class="col-3">
                            <div class="row">
                                <div class="col-12" style="padding-top: 29px;">
                                    <?= Html::a("+", "#", ['class' => 'btn btn-xs slider-more btn-success', 'data-toggle' => "tooltip", "data-placement" => "bottom", "title" => "Dodaj do slidera"]) ?>
                                    <?= Html::a("-", "#", ['class' => 'btn btn-xs slider-less btn-danger', 'data-toggle' => "tooltip", "data-placement" => "bottom", "title" => "Usuń ze slidera", 'style' => "width: 22px;"]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </section>
    </div>
</div>

<?php if (!$model->isNewRecord) { ?>
    <ul class="nav nav-tabs">
        <li class="active">
            <a href="#seo" data-toggle="tab">Konfiguracja SEO</a>
        </li>
        <li>
            <a href="#download" data-toggle="tab">Pliki do pobrania</a>
        </li>
        <li>
            <a href="#comment" data-toggle="tab">Komentarze</a>
        </li>
    </ul>

    <div class="tab-content ">
        <div class="tab-pane active" id="seo">
            <section class="panel full" >
                <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend', 'seo_configuration') ?></header>
                <div class="panel-body" >
                    <div class="article-form">
                        <div class="row">
                            <div class="col-6">
                                <?= $form->field($model, 'seo_title')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-6">
                                <?= $form->field($model, 'seo_keywords')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                        <?= $form->field($model, 'seo_description')->textarea(['rows' => 6]) ?>
                    </div>
                </div>
            </section>
        </div>
        <div class="tab-pane" id="download">
            <section class="panel full" >
                <header>
                    <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Pliki do pobrania
                    <span class="btn btn-success btn-sm pull-right" data-init="file-manager" data-target="#add-file-download"  style="margin-top: -5px; margin-right: 10px;" title="Wybierz plik z menadżera plików" data-toggle="tooltip" data-placement="bottom">Wybierz plik z menadżera plików</span>
                    <label class="pull-right">
                        <span class="btn btn-success btn-sm pull-right" style="margin-top: -5px; margin-right: 10px;" title="Wgraj plik z komputera" data-toggle="tooltip" data-placement="bottom">Wgraj plik z komputera</span>
                        <input type="file" id="add-file-upload" style="display: none"/>
                    </label>
                    <input type="hidden" id="add-file-download"/>
                </header>
                <div class="panel-body download-panel">
                    <?php foreach ($model->getArticleFiles()->all() as $oArticleFile) { ?>
                        <?php $oFile = $oArticleFile->getFile()->one(); ?>
                        <div class="col-2">
                            <div class="row">
                                <div class="col-12">
                                    <img src=<?= \Yii::$app->request->baseUrl ?>."images/ico/file.png" style="height: 80px;" class="fileico img-responsive"/>
                                    <a href=<?= \Yii::$app->request->baseUrl ?>."articles/article/remove-file/?id=<?= $oArticleFile->id ?>&article=<?= $model->id ?>" title="Usuń" class="remove-file download-remove">
                                        <span class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></span>
                                    </a>
                                    <a href=<?= \Yii::$app->request->baseUrl ?>."articles/article/feature-file/?id=<?= $oArticleFile->id ?>&article=<?= $model->id ?>" title="Wyróżnij" class="remove-file" style="position: absolute; top: 35px; right: 20px;">
                                        <span class="btn btn-warning btn-xs"><span class="glyphicon <?php if ($oArticleFile->is_featured) { ?>glyphicon-star<?php } else { ?>glyphicon-star-empty<?php } ?>"></span></span>
                                    </a>
                                </div>
                                <div class="col-12 filename"><?= $oFile->filename ?></div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </section>
        </div>
        <div class="tab-pane" id="comment">
            <section class="panel full" >
                <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Komentarze</header>
                <div class="panel-body">
                    <div class="row form-inline">
                        <div class="col-4"><?= $form->field($model, 'comment_show')->dropDownList([0 => Yii::t('backend_module_articles', 'active'), 1 => Yii::t('backend_module_articles', 'inactive')]) ?></div>
                        <div class="col-4"><?= $form->field($model, 'comment_add')->dropDownList([0 => Yii::t('backend_module_articles', 'active'), 1 => Yii::t('backend_module_articles', 'inactive')]) ?></div>
                        <div class="col-3">
                            <div class="form-group"><?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_articles', 'create') : Yii::t('backend', 'save_changes'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'style' => 'display: block; margin: 0 auto;']) ?></div>
                        </div>
                    </div>
                    <?= GridView::widget([
                        'dataProvider' => $dataProviderComments,
                        'filterModel' => $searchModelComments,
                        'rowOptions' => function($model) {
                            if ($model->is_active == 1) {
                                return ['class' => 'inactive-row'];
                            }
                        },
                        'columns' => [
                            'id',
                            [
                                'attribute' => "text",
                                'format' => 'raw',
                                'contentOptions' => ['title' => "Kliknij, aby rozwinąć treść", 'style' => 'width: 500px', 'class' => 'toggled-td'],
                                'value' => function($model) {
                                    return nl2br($model->text);
                                }
                            ],
                            'author',
                            'date_created',
                            [
                                'class' => 'common\hooks\yii2\grid\ActionColumn',
                                'template' => '{accept} {block} {delete}',
                                'contentOptions' => ['style' => 'width: 100px;'],
                                'buttons' => [
                                    'accept' => function ($url, $model) {
                                        return Html::a(
                                            '<span class="btn btn-success btn-xs"><span class="glyphicon glyphicon-ok"></span></span>', \Yii::$app->request->baseUrl."comments/comments/accept/?id={$model->id}", [
                                                'title' => "Zatwierdź",
                                                'data-toggle' => "tooltip",
                                                'data-placement' => "bottom"
                                            ]
                                        );
                                    },
                                    'block' => function ($url, $model) {
                                        return Html::a(
                                            '<span class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-remove"></span></span>', \Yii::$app->request->baseUrl."comments/comments/block/?id={$model->id}", [
                                                'title' => "Odrzuć",
                                                'data-toggle' => "tooltip",
                                                'data-placement' => "bottom"
                                            ]
                                        );
                                    },
                                    'delete' => function ($url, $model) {
                                        return Html::a(
                                            '<span class="btn btn-danger btn-xs glyphicon glyphicon-trash"></span>', \Yii::$app->request->baseUrl."comments/comments/delete/?id={$model->id}", [
                                                'title' => "Usuń",
                                                'data-toggle' => "tooltip",
                                                'data-placement' => "bottom"
                                            ]
                                        );
                                    },
                                ],
                            ],
                        ],
                    ]);
                    ?>
                </div>
            </section>
        </div>
    </div>
<?php } ?>

<?php ActiveForm::end(); ?> 