<?php

 use common\hooks\yii2\grid\GridView;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\modules\authors\models\Author;
use common\modules\articles\models\ArticleStatus;
use common\modules\categories\models\ArticlesCategories;

$this->title = Yii::t('backend_module_articles', 'articles_list');
?>
<div class="article-index">

    <?= $this->render('_categories-qmenu', []) ?>

    <div class="row">
        <div class="col-12">
            <section class="panel full panel-default" >
                <div class="panel-heading" >
                    <?= $this->render('_quickmenu', []) ?>
                </div>
                <div class="panel-body" >

                    <?=
                    GridView::widget([
                        'id' => 'article-list',
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'tableOptions' => [
                            'class' => 'table table-striped table-bordered sortable'
                        ],
                        'rowOptions' => function ($model, $key, $index, $grid) {
                            if ($model->getStatus()->one()->id == 1) {
                                return ['class' => 'row-draft'];
                            }
                            if ($model->getStatus()->one()->id == 2) {
                                return ['class' => 'row-confirm'];
                            }
                            if ($model->getStatus()->one()->id == 3) {
                                return ['class' => 'row-trash'];
                            }
                            if ($model->getStatus()->one()->id == 4) {
                                return ['class' => 'row-waiting'];
                            }
                        },
                        'columns' => [
                            [
                                'class' => 'yii\grid\CheckboxColumn',
                            ],
                            [
                                'attribute' => 'title',
                                'format' => 'raw',
                                'value' => function($data) {
                                    return '<a href="'.\Yii::$app->request->baseUrl.'articles/article/update/?id=' . $data->id . '" data-toggle="tooltip" data-placement="bottom" title="Edytuj">' . $data->title . '</a>';
                                }
                            ],
                            [
                                'attribute' => 'category_id',
                                'format' => 'raw',
                                'contentOptions' => ['style' => 'width: 170px;'],
                                'filter' => ArrayHelper::map(ArticlesCategories::find()->all(), 'id', 'name'),
                                'value' => function ($data) {
                                    $oCategory = ArticlesCategories::find()->where(["=", "id", $data->category_id])->one();

                                    if (!is_null($oCategory)) {
                                        return $oCategory->name;
                                    }
                                }
                            ],
                            [
                                'attribute' => 'author_id',
                                'label' => 'Autor',
                                'format' => 'raw',
                                'contentOptions' => ['style' => 'width: 140px;'],
                                'filter' => ArrayHelper::map(Author::find()->all(), 'id', function($model) {
                                    return $model->firstname . ' ' . $model->surname;
                                }),
                                'value' => function ($model) {
                                    if($model->getAuthor()){
                                        return $model->getAuthor()->firstname . ' ' . $model->getAuthor()->surname;
                                    }
                                }
                            ],
                            [
                                'attribute' => 'date_updated',
                                'label' => 'Aktualizacja',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return date("d.m.y H:i:s", strtotime($model->date_updated)) . '<br>' . ($model->getUpdateUser() ? '<small>' . $model->getUpdateUser()->username . '</small>' : '<small>brak</small>');
                                }
                            ],
                            [
                                'attribute' => 'status_id',
                                'format' => 'raw',
                                'filter' => ArrayHelper::map(ArticleStatus::find()->all(), 'id', 'name'),
                                'contentOptions' => ['style' => 'width: 145px;'],
                                'value' => function ($data) {
                                    return ($data->date_published ? date("d.m.y H:i:s", strtotime($data->date_published))."<br/>" : "").$data->getStatus()->one()->name;
                                }
                            ],
                            [
                                'class' => 'common\hooks\yii2\grid\ActionColumn',
                                'template' => '{update} {duplicate} {delete}',
                                'contentOptions' => ['style' => 'width: 120px;'],
                                'buttons' => [
                                    'update' => function ($url) {
                                        return Html::a(
                                                        '<span class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></span>', $url, [
                                                    'title' => Yii::t('backend', 'edit'),
                                                    'data-toggle' => "tooltip",
                                                    'data-placement' => "bottom"
                                                        ]
                                        );
                                    },
                                    'duplicate' => function ($url) {
                                        return Html::a(
                                                        '<span class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-file"></span></span>', $url, [
                                                    'title' => Yii::t('backend', 'duplicate'),
                                                    'data-toggle' => "tooltip",
                                                    'data-placement' => "bottom"
                                                        ]
                                        );
                                    },
                                    'delete' => function ($url) {
                                        return Html::a(
                                                        '<span class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></span>', Yii::$app->request->get("deleted") ? $url . "&remove=1" : $url, [
                                                    'title' => Yii::t('backend', 'delete'),
                                                    'data-toggle' => "tooltip",
                                                    'data-placement' => "bottom",
                                                    'data-pjax' => 0,
                                                    'data-confirm' => 'Czy na pewno usunąć ten element?',
                                                    'data-method' => 'post',
                                                        ]
                                        );
                                    },
                                ],
                            ],
                        ],
                    ]);
                    ?>

                </div>
                <div class="panel-footer">
                    <?= $this->render('_quickmenu', []) ?>
                </div>
            </section>
        </div>
        <div class="col-12">
            <section class="panel full" >
                <header>
                    <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend', 'category_map') ?>    
                </header>
                <div class="panel-body" >
                    <div class="clt">
                        <ul>
                            <?php foreach (ArticlesCategories::find()->where(["IS", "parent_id", NULL])->all() as $oCategory) { ?>
                                <li>
                                    <div class="dir"></div> 
                                    <a href="<?= \Yii::$app->request->baseUrl ?>articles/article/articles/?id=<?= $oCategory->id ?>"><?= $oCategory->name ?></a>
                                    <a href="<?= \Yii::$app->request->baseUrl ?>categories/categories/update/?id=<?= $oCategory->id ?>"><div class="edit"></div></a>
                                    <?php if (count(ArticlesCategories::find()->where(["=", "parent_id", $oCategory->id])->all()) > 0) { ?>
                                        <ul>
                                            <?php foreach (ArticlesCategories::find()->where(["=", "parent_id", $oCategory->id])->all() as $oChild) { ?>
                                                <li>
                                                    <div class="dir"></div> 
                                                    <a href="<?= \Yii::$app->request->baseUrl ?>articles/article/articles/?id=<?= $oChild->id ?>"><?= $oChild->name ?></a>
                                                    <a href="<?= \Yii::$app->request->baseUrl ?>categories/categories/update/?id=<?= $oChild->id ?>"><div class="edit"></div></a>
                                                    <?php if (count(ArticlesCategories::find()->where(["=", "parent_id", $oChild->id])->all()) > 0) { ?>
                                                        <ul>
                                                            <?php foreach (ArticlesCategories::find()->where(["=", "parent_id", $oChild->id])->all() as $oChild2) { ?>
                                                                <li>
                                                                    <div class="dir"></div> 
                                                                    <a href="<?= \Yii::$app->request->baseUrl ?>articles/article/articles/?id=<?= $oChild2->id ?>"><?= $oChild2->name ?></a>
                                                                    <a href="<?= \Yii::$app->request->baseUrl ?>categories/categories/update/?id=<?= $oChild2->id ?>"><div class="edit"></div></a>
                                                                </li>
                                                            <?php } ?>
                                                        </ul>
                                                    <?php } ?>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    <?php } ?>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </section>
        </div>
    </div>


</div>
