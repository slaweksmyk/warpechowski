<?php
use yii\helpers\Html;   
$this->title = Yii::t('backend_module_articles', 'Create Article');
?>
<div class="article-create">
    <p>
        <?= Html::a(Yii::t('backend_module_articles', 'back_to_articles_list'), ["index"], ['class' => 'btn btn-outline-secondary']) ?>
    </p>
    
    <?= $this->render('_form', [
        'model' => $model,
        'oSliderItems' => []
    ]) ?>
</div>
