<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
 use common\hooks\yii2\grid\GridView;
use yii\widgets\ActiveForm;
use common\modules\users\models\User;
use common\modules\articles\models\ArticleStatus;
use common\modules\categories\models\ArticlesCategories;

use backend\helpers\ArticleCategoryHelper;

$oCategory = ArticlesCategories::find()->where(["=", "id", $_GET["id"]])->one();
$this->title = $oCategory->name." - lista artykułów";
?>
<div class="article-index">

    <?= $this->render('_categories-qmenu', []) ?>
    
    <div id="sortable_data">
        <?php ActiveForm::begin(); ?>
        <?php ActiveForm::end(); ?>
    </div>
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <?php if(Yii::$app->request->get('per-page')){ echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>"; } ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?= GridView::widget([
                'id' => 'list',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'rowOptions'=>function($model){
                    if(!$model->is_active){
                        return ['class' => 'inactive-row'];
                    }
                },
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered sortable'
                ],
                'rowOptions' => function ($model, $key, $index, $grid) {
					if (!$model->getStatus()->one()->is_published) {
						return ['class' => 'inactive-row'];
					}
                },
                'columns' => [
                    [
                        'class' => 'yii\grid\CheckboxColumn',
                    ],
                    [
                        'attribute' => 'id',
                        'label' => "LP"
                    ],
                    [
                        'attribute' => 'title',
                        'value' => function($data){
                            if(strlen($data->title) > 40){
                                return substr($data->title, 0, 40)."...";
                            } else {
                                return $data->title;
                            }
                        }
                    ],
                    [
                        'attribute' => 'category_id',
                        'format' => 'raw',
                        'contentOptions' => ['style' => 'width: 180px;'],
                        'filter'=> ArrayHelper::map(ArticlesCategories::find()->all(), 'id', 'name'),
						'value' => function ($data) {
							$oCategory = ArticlesCategories::find()->where(["=", "id", $data->category_id])->one();

							if (!is_null($oCategory)) {
								return $oCategory->name;
							}
						}
                    ],
                    [
                        'attribute' => 'update_user_id',
                        'format' => 'raw',
                        'contentOptions' => ['style' => 'width: 145px;'],
                        'value' => function ($data) {
                            $oUser = User::find()->where(["=", "id", $data->update_user_id])->one();
                            if (!is_null($oUser)) {
                                return $oUser->username;
                            }
                        }
                    ],
                    [
                        'attribute' => 'date_updated',
                        'label' => 'Data aktualizacji'
                    ],
					[
						'attribute' => 'status_id',
						'format' => 'raw',
						'filter'=> ArrayHelper::map(ArticleStatus::find()->all(), 'id', 'name'),
						'contentOptions' => ['style' => 'width: 145px;'],
						'value' => function ($data) {
							return $data->getStatus()->one()->name;
						}
					],
                    [
                        'attribute' => 'sort',
                        'contentOptions' => ['style' => 'width: 145px;'],
                    ],
                    [
                        'class' => 'common\hooks\yii2\grid\ActionColumn',
                        'template' => '{update} {duplicate} {delete}',
                        'contentOptions' => ['style' => 'width: 100px;'],
                        'buttons' => [
                            'update' => function ($url) {
                                return Html::a(
                                        '<span class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></span>', $url, [
                                        'title' => Yii::t('backend', 'edit'),
                                        'data-toggle' => "tooltip",
                                        'data-placement' => "bottom"
                                    ]
                                );
                            },
                            'duplicate' => function ($url) {
                                return Html::a(
                                        '<span class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-file"></span></span>', $url, [
                                        'title' => Yii::t('backend', 'duplicate'),
                                        'data-toggle' => "tooltip",
                                        'data-placement' => "bottom"
                                    ]
                                );
                            },
                            'delete' => function ($url) {
                                return Html::a(
                                        '<span class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></span>', $url, [
                                        'title' => Yii::t('backend', 'delete'),
                                        'data-toggle' => "tooltip",
                                        'data-placement' => "bottom",
                                        'data-pjax' => 0,
                                        'data-confirm' => 'Czy na pewno usunąć ten element?',
                                        'data-method' => 'post',
                                    ]
                                );
                            },
                        ],
                    ],
                ],
            ]);
            ?>
            <?= $this->render('_quickmenu', []) ?>
        </div>
    </section>   
</div>
