<?php
    use yii\helpers\Html;
    $this->title = Yii::t('backend_module_articles', 'update: ', [
        'modelClass' => 'Article',
    ]) . $model->title." <span style='color: {$model->getStatus()->one()->color}; font-size: 19px;'>({$model->getStatus()->one()->name})</span>";
?>

<p>
    <?= Html::a(Yii::t('backend_module_articles', 'back_to_articles_list'), ['index'], ['class' => 'btn btn-outline-secondary']) ?>

    <?php if($model->getCategory() && $model->getCategory()->getExtendPage()->one()){ ?>
        <?= Html::a(Yii::t('backend', 'preview'), ['preview', 'url' => Yii::$app->request->getHostInfo()."/{$model->getCategory()->getExtendPage()->one()->url}/{$model->slug}"], ['class' => 'btn btn-warning', 'target' => '_blank']) ?>
        <?= Html::a("Przejdź do artykułu", Yii::$app->request->getHostInfo()."/{$model->getCategory()->getExtendPage()->one()->url}/{$model->slug}", ['class' => 'btn btn-primary pull-right', "target" => "_blank"]) ?>
    <?php } ?>
</p>

<?= $this->render('_form', [
    'model' => $model,
    'video' => $video,
    'oSliderItems' => $oSliderItems,
    'searchModelComments' => $searchModelComments,
    'dataProviderComments' => $dataProviderComments
]) ?>