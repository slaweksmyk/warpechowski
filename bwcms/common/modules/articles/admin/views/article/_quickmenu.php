<?php

use yii\helpers\Html;
use common\modules\categories\models\ArticlesCategories;

$request = Yii::$app->request;
?>

<div class="row">
    <div class="col-6 action-bar" data-url="<?= \Yii::$app->request->baseUrl ?>articles/article/actionbar/">
        <span class="action-text"><?= Yii::t('backend', 'for_selected_items'); ?>:</span>

        <select class="form-control action-select action-action input-sm" data-action="btncolor">
            <option disabled selected><?= Yii::t('backend', 'select_actions'); ?></option>
            <option value="delete" data-color="danger"><?= Yii::t('backend', 'delete'); ?></option>
            <option value="change-category" data-secondary="true" data-color="primary"><?= Yii::t('backend', 'change_category'); ?></option>
        </select>

        <span class="action-secondary">
            <span class="action-text" style="margin-left: 10px;">na</span>

            <select class="form-control action-select action-value no-select2" style="width: auto;">
                <option disabled selected><?= Yii::t('backend', 'select_category'); ?></option>
                <?php foreach (ArticlesCategories::find()->all() as $oCategory) { ?>
                    <option value="<?= $oCategory->id ?>"><?= $oCategory->name ?></option>
                <?php } ?>
            </select>
        </span>
        <?= Html::a(Yii::t('backend', 'for_selected_items'), "#", ['class' => 'btn btn-primary action-button btn-sm', 'data-action' => 'btn']) ?>
    </div>
    <div class="col-3 pull-right">
        <form id="per-page" class="per-page-right">
            <span><?= Yii::t('backend', 'quantity_per_page'); ?>:</span>
            <select name="per-page" class="form-control input-sm">
                <?php if (Yii::$app->request->get('per-page')) {
                    echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>";
                } ?>
                <option>50</option><option>100</option><option>200</option><option>500</option>
            </select>
        </form>
    </div>
    <div class="col-2 pull-right">
        <input type="text" name="date-range" id="article-daterange" <?php if($request->get('from') && $request->get('to')){ ?>value="<?= $request->get('from')." - ".$request->get('to') ?>"<?php } ?> class="form-control datepicker-range" placeholder="Wybierz według daty" style="font-size: 12px;"/>
    </div>
</div>