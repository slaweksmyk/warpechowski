<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    
    $this->title = "Przywracanie wersji artykułu: {$oArticle->title}<br/><small>z dnia: {$oArticleHistory->create_date}</small>";
?>

<div class="row">
    <div class="col-6">
        <?php $form = ActiveForm::begin(); ?>
            <p>
                <?= Html::a("< Wróć do artykułu", ['update', 'id' => $oArticle->id], ['class' => 'btn btn-outline-secondary']) ?>
            </p>
            <section class="panel full" >
                <header>
                    <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Wersja aktualna   
                </header>
                <div class="panel-body">
                    <?php $aAttrList = []; ?>
                    <?php foreach($oArticle->attributes() as $sAttribute){ ?>
                        <?php $oAttributeData = $oArticle->getTableSchema()->getColumn($sAttribute); ?>
                        <?php if(strpos($sAttribute, "id") === false && strpos($sAttribute, "hash") === false){ ?>
                            <div class="row">
                                <div class="col-12">

                                    <?php if($oAttributeData->type == "text"){ ?>
                                        <?= $form->field($oArticle, $sAttribute)->textarea() ?>
                                        <?php $aAttrList[$sAttribute] = ["type" => $oAttributeData->type, "value" => $oArticle->{$sAttribute}]; ?>
                                    <?php } ?>

                                    <?php if($oAttributeData->type == "smallint"){ ?>
                                        <?= $form->field($oArticle, $sAttribute)->dropDownList([0 => Yii::t('backend_module_articles', 'no'), 1 => Yii::t('backend_module_articles', 'yes')]) ?>
                                        <?php $aAttrList[$sAttribute] = ["type" => $oAttributeData->type, "value" => $oArticle->{$sAttribute}]; ?>
                                    <?php } ?>

                                    <?php if($oAttributeData->type == "string"){ ?>
                                        <?= $form->field($oArticle, $sAttribute)->textInput() ?>
                                        <?php $aAttrList[$sAttribute] = ["type" => $oAttributeData->type, "value" => $oArticle->{$sAttribute}]; ?>
                                    <?php } ?>

                                    <?php if($oAttributeData->type == "integer"){ ?>
                                        <?= $form->field($oArticle, $sAttribute)->textInput() ?>
                                        <?php $aAttrList[$sAttribute] = ["type" => $oAttributeData->type, "value" => $oArticle->{$sAttribute}]; ?>
                                    <?php } ?>

                                    <?php if($oAttributeData->type == "datetime"){ ?>
                                        <?= $form->field($oArticle, $sAttribute)->textInput(["class" => "datepicker form-control"]) ?>
                                        <?php $aAttrList[$sAttribute] = ["type" => $oAttributeData->type, "value" => $oArticle->{$sAttribute}]; ?>
                                    <?php } ?>

                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </section>
        <?php ActiveForm::end(); ?> 
    </div>
    <div class="col-6">
        <?php $form = ActiveForm::begin(); ?>
            <div style="margin-bottom: 10px; position: relative; top: -10px;">
                <?= Html::submitButton("Przywróć tą wersję", ['class' => 'btn btn-primary', 'style' => 'float: left;']) ?>
                <div style="float: right;">
                    <?= $oArticleHistoryPrev ? Html::a("<", ['diff', 'id' => $oArticleHistoryPrev->id, 'article_id' => $oArticle->id], ['class' => 'btn btn-outline-secondary', 'title' => "Poprzednia wersja", 'data-toggle' => 'tooltip', 'data-placement' => 'bottom']) : "" ?>
                    <?= $oArticleHistoryNext ? Html::a(">", ['diff', 'id' => $oArticleHistoryNext->id, 'article_id' => $oArticle->id], ['class' => 'btn btn-outline-secondary', 'title' => "Następna wersja", 'data-toggle' => 'tooltip', 'data-placement' => 'bottom']) : "" ?>
                </div>
            </div>
            <section class="panel full" >
                <header>
                    <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Wersja z dnia <?= date("d.m.Y H:i:s", strtotime($oArticleHistory->create_date)) ?>   
                </header>
                <div class="panel-body" >
                    <?php foreach(json_decode($oArticleHistory->content_data) as $sAttribute => $sValue){ ?>
                        <?php if(isset($aAttrList[$sAttribute])){ ?>
                            <div class="row">
                                <div class="col-12">
                                    <?php if($aAttrList[$sAttribute]["type"] == "text"){ ?>
                                        <?php $oArticle->{$sAttribute} = $sValue; ?>
                                        <?= $form->field($oArticle, $sAttribute, ($aAttrList[$sAttribute]["value"] != $sValue) ? ['options' => ['class' => 'has-warning']] : [])->textarea() ?>
                                    <?php } ?>

                                    <?php if($aAttrList[$sAttribute]["type"] == "smallint"){ ?>
                                        <?php $oArticle->{$sAttribute} = $sValue; ?>
                                        <?= $form->field($oArticle, $sAttribute, ($aAttrList[$sAttribute]["value"] != $sValue) ? ['options' => ['class' => 'has-warning']] : [])->dropDownList([0 => Yii::t('backend_module_articles', 'no'), 1 => Yii::t('backend_module_articles', 'yes')]) ?>
                                    <?php } ?>

                                    <?php if($aAttrList[$sAttribute]["type"] == "string"){ ?>
                                        <?php $oArticle->{$sAttribute} = $sValue; ?>
                                        <?= $form->field($oArticle, $sAttribute, ($aAttrList[$sAttribute]["value"] != $sValue) ? ['options' => ['class' => 'has-warning']] : [])->textInput() ?>
                                    <?php } ?>

                                    <?php if($aAttrList[$sAttribute]["type"] == "integer"){ ?>
                                        <?php $oArticle->{$sAttribute} = $sValue; ?>
                                        <?= $form->field($oArticle, $sAttribute, ($aAttrList[$sAttribute]["value"] != $sValue) ? ['options' => ['class' => 'has-warning']] : [])->textInput() ?>
                                    <?php } ?>

                                    <?php if($aAttrList[$sAttribute]["type"] == "datetime"){ ?>
                                        <?php $oArticle->{$sAttribute} = $sValue; ?>
                                        <?= $form->field($oArticle, $sAttribute, ($aAttrList[$sAttribute]["value"] != $sValue) ? ['options' => ['class' => 'has-warning']] : [])->textInput(["class" => "datepicker form-control"]) ?>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </section>
        <?php ActiveForm::end(); ?>
    </div>
</div>


