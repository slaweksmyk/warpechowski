<?php

/**
 * Article module
 * 
 * PHP version 7
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @copyright (c) 2016 Krzysztof Borecki
 * @version 1.0
 */

namespace common\modules\articles\admin\controllers;

use Yii;
use common\modules\articles\models\Article;
use common\modules\articles\models\ArticleSearch;
use common\modules\categories\models\ArticlesCategories;
use common\modules\articles\models\ArticleCommentSearch;
use common\modules\slider\models\SliderItems;
use common\modules\files\models\File;
use common\modules\types\models\Type;
use common\modules\types\models\TypeHash;
use common\modules\tags\models\Tag;
use common\modules\tags\models\TagArticle;
use common\modules\articles\models\ArticleRelated;
use common\modules\articles\models\ArticleVideo;
use common\modules\articles\models\ArticleFile;
use common\modules\articles\models\ArticleHistory;
use common\modules\authors\models\Author;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionSort()
    {
        $aPost = Yii::$app->request->post();

        $aArtSorts = json_decode($aPost["aArtData"], true);

        foreach ($aArtSorts as $aArtSort) {
            $oArticle = Article::findOne($aArtSort["id"]);
            $oArticle->sort = $aArtSort["sort"];
            $oArticle->save();
        }

        exit;
    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex($id = null)
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider
        ]);
    }

    public function actionRemoveAtende($id)
    {
        $oArticle = Article::find()->where(["=", "id", $id])->one();
        if (!is_null($oArticle)) {
            $oVideo = $oArticle->getRedGalaxy()->one();
            $oVideo->delete();
        }

        return $this->redirect(['update', 'id' => $id]);
    }

    public function actionRemoveVideo($id)
    {
        $oArticle = Article::find()->where(["=", "id", $id])->one();
        if (!is_null($oArticle)) {
            $oVideo = $oArticle->getVideo();
            $oVideo->delete();
        }

        return $this->redirect(['update', 'id' => $id]);
    }

    public function actionAutosave()
    {
        if (Yii::$app->request->isAjax) {
            $oArticle = Article::find()->where(["=", "id", Yii::$app->request->post('id')])->one();

            $aArticleData = $oArticle->getAttributes();
            unset($aArticleData["id"]);

            $oArticleHistory = new ArticleHistory();
            $oArticleHistory->article_id = $oArticle->id;
            $oArticleHistory->updated_user_id = Yii::$app->user->identity->id;
            $oArticleHistory->is_autosave = 1;
            $oArticleHistory->content_data = json_encode($aArticleData);
            $oArticleHistory->create_date = date("Y-m-d H:i:s");
            $oArticleHistory->save();
        }
    }

    public function actionRevert($id, $articleID)
    {
        $oArticle = Article::find()->where(["=", "id", $articleID])->one();
        $oArticleHistory = ArticleHistory::find()->where(["=", "id", $id])->one();
        $aContentData = json_decode($oArticleHistory->content_data);

        foreach ($aContentData as $key => $value) {
            $oArticle->{$key} = $value;
        }
        $oArticle->save();

        return $this->redirect(['update', 'id' => $articleID]);
    }

    public function actionGetArticlesForMce($key)
    {
        if (Yii::$app->request->isAjax && !empty($key)) {
            $oArticleRowset = Article::find()->where(["=", "status_id", 2])->andWhere(['LIKE', 'title', $key])->orderBy("id DESC")->limit(20)->all();

            $aTypeItems = [];
            foreach ($oArticleRowset as $index => $oArticle) {
                $aTypeItems[$index]["id"] = json_encode([$oArticle->id, $oArticle->title, $oArticle->getUrl()]);
                $aTypeItems[$index]["text"] = $oArticle->title;
                $aTypeItems[$index]["url"] = $oArticle->getUrl();
            }

            $aReturnArr = [];
            $aReturnArr["total_count"] = count($oArticleRowset);
            $aReturnArr["items"] = array_filter($aTypeItems);

            echo json_encode(array_filter($aReturnArr));
            exit;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionFeatureFile($id)
    {
        $oArticleFile = ArticleFile::find()->where(["=", "id", $id])->one();
        $oArticleFile->is_featured = ($oArticleFile->is_featured) ? 0 : 1;
        $oArticleFile->save();

        return $this->redirect(['update', 'id' => Yii::$app->request->get('article')]);
    }

    public function actionRemoveFile($id)
    {
        $oArticleFile = ArticleFile::find()->where(["=", "id", $id])->one();
        $oArticleFile->delete();
        return $this->redirect(['update', 'id' => Yii::$app->request->get('article')]);
    }

    public function actionAddDownloadFile()
    {
        $oArticleFile = new ArticleFile();
        $oArticleFile->article_id = Yii::$app->request->post('articleID');
        $oArticleFile->file_id = Yii::$app->request->post('fileID');
        $oArticleFile->save();

        exit;
    }

    public function actionRemoveThumbnail($id)
    {
        $oArticle = Article::find()->where(["=", "id", $id])->one();
        if (!is_null($oArticle)) {
            $oArticle->thumbnail_id = null;
            $oArticle->save();
        }

        return $this->redirect(['update', 'id' => $id]);
    }

    public function actionReplaceThumbnail()
    {
        $iArticleID = Yii::$app->request->post('articleID');
        $iThumbnailID = Yii::$app->request->post('imageID');

        $oArticle = Article::find()->where(["=", "id", $iArticleID])->one();
        if (!is_null($oArticle)) {
            $oArticle->thumbnail_id = $iThumbnailID;
            $oArticle->save();
        }

        exit;
    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionArticles($id)
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (!is_null($id)) {
            $oCategory = ArticlesCategories::find()->where(["=", "id", $id])->one();

            if (count($oCategory->getCategories()->all()) > 0) {
                $aCats = [];
                foreach ($oCategory->getCategories()->all() as $oChild) {
                    array_push($aCats, $oChild->id);
                }

                $dataProvider->query->andWhere(["OR", ["=", "category_id", $id], ["IN", "category_id", $aCats]]);
            } else {
                $dataProvider->query->andWhere(["category_id" => $id]);
            }
        }

        if (Yii::$app->request->get('per-page')) {
            $dataProvider->setPagination(['pageSize' => Yii::$app->request->get('per-page')]);
        }

        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post();

            $oArticle = $this->findModel($post["modelID"]);
            $oArticle->sort = $post["index"];
            $oArticle->save();

            exit;
        }

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Enable preview mode
     * @return mixed
     */
    public function actionPreview($url = null)
    {
        Yii::$app->session->set('preview_mode', true);
        Yii::$app->response->redirect(Url::to($url ?? "/", true));
    }

    public function actionList()
    {


        echo "<script>
    var args = top.tinymce.activeEditor.windowManager.getParams();
console.log(args.arg1, args.arg2);
</script>
";
    }

    /**
     * Displays a single Article model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Duplicate a single Article model.
     * @param integer $id
     * @return mixed
     */
    public function actionDuplicate($id)
    {
        $obj = Article::find()->where(["=", "id", $id])->one();
        $clone = new Article;
        $clone->attributes = $obj->attributes;
        $clone->slug = $obj->slug . "-";
        $clone->save();

        return $this->redirect(['index']);
    }

    public function actionDiff($id, $article_id)
    {
        $oArticle = Article::findOne($article_id);

        if ($oArticle->load(Yii::$app->request->post()) && $oArticle->save()) {
            return $this->redirect(['update', 'id' => $article_id]);
        }

        $iArticleHistoryIndex = 0;
        $oArticleHistoryRowset = ArticleHistory::find()->where(["=", "article_id", $article_id])->all();
        foreach ($oArticleHistoryRowset as $index => $oArticleHis) {
            if ($oArticleHis->id == $id) {
                $iArticleHistoryIndex = $index;
            }
        }

        return $this->render('diff', [
                    'oArticle' => $oArticle,
                    'oArticleHistoryPrev' => $iArticleHistoryIndex > 0 ? $oArticleHistoryRowset[$iArticleHistoryIndex - 1] : null,
                    'oArticleHistoryNext' => $iArticleHistoryIndex + 1 < count($oArticleHistoryRowset) ? $oArticleHistoryRowset[$iArticleHistoryIndex + 1] : null,
                    'oArticleHistory' => $oArticleHistoryRowset[$iArticleHistoryIndex],
                    'oArticleHistoryRowset' => $oArticleHistoryRowset
        ]);
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Article();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->slug = $this->createUniqueUrl($model->title);
            $model->date_created = Yii::$app->formatter->asTime('now', 'yyyy-MM-dd HH:mm:ss');
            // $model->date_display = $model->date_created;
            $model->create_user_id = Yii::$app->user->identity->id;
            $model->status_id = 1;
            $model->save();

            if (isset(Yii::$app->request->post("Article")["aSliders"])) {
                foreach (Yii::$app->request->post("Article")["aSliders"] as $iSliderID) {
                    if ($iSliderID) {
                        $oSliderItem = new SliderItems();
                        $oSliderItem->slider_id = $iSliderID;
                        $oSliderItem->title = $model->title;
                        $oSliderItem->description = trim(strip_tags($model->full_description));
                        $oSliderItem->extlink = $model->extlink;
                        $oSliderItem->thumbnail_id = $model->thumbnail_id;
                        $oSliderItem->create_user_id = Yii::$app->user->identity->id;
                        $oSliderItem->is_active = 1;
                        $oSliderItem->date_created = date("Y-m-d H:i:s");
                        $oSliderItem->date_display = $model->date_display;
                        $oSliderItem->data_id = $model->id;
                        $oSliderItem->settings_show_title = 1;
                        $oSliderItem->settings_show_description = 1;
                        $oSliderItem->settings_target_blank = 0;
                        $oSliderItem->settings_thumbnail_show = 1;
                        $oSliderItem->save();
                    }
                }
            }

            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('article_create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $searchModelComments = new ArticleCommentSearch();
        $dataProviderComments = $searchModelComments->search(Yii::$app->request->queryParams);
        $dataProviderComments->query->andWhere(["=", "article_id", $model->id]);

        $video = ArticleVideo::find()->where(["=", "article_id", $id])->one();
        if (is_null($video)) {
            $video = new ArticleVideo();
        }

        $oSliderItems = SliderItems::find()->where(["=", "data_id", $id])->all();
        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
            if (isset($post["thumb_file_id"]) && $post["thumb_file_id"]) {
                $oFile = File::find()->where(["=", "id", $post["thumb_file_id"]])->one();

                echo "/upload/{$oFile->filename}";
                exit;
            }
        }

        if ($model->load(Yii::$app->request->post())) {

            $video->load(Yii::$app->request->post());
            $video->youtube_id = str_replace("https://www.youtube.com/watch?v=", "", $video->youtube_id);
            $video->youtube_id = str_replace("http://www.youtube.com/watch?v=", "", $video->youtube_id);
            $video->youtube_id = str_replace("https://youtu.be/", "", $video->youtube_id);
            $video->youtube_id = str_replace("http://youtu.be/", "", $video->youtube_id);
            $video->save();

            if ($model->status_id == 2 && $model->date_published == NULL) {
                $model->date_published = Yii::$app->formatter->asTime('now', 'yyyy-MM-dd HH:mm:ss');
                if ($model->date_display == NULL) {
                    $model->date_display = Yii::$app->formatter->asTime('now', 'yyyy-MM-dd HH:mm:ss');
                }
            }

            $model->slug = $this->createUniqueUrl($model->title, $id);
            $model->url = $model->slug;
            $model->update_user_id = Yii::$app->user->identity->id;
            $model->date_updated = Yii::$app->formatter->asTime('now', 'yyyy-MM-dd HH:mm:ss');
            $model->save();

            $aArticleData = $model->getAttributes();
            unset($aArticleData["id"]);

            $oArticleHistory = new ArticleHistory();
            $oArticleHistory->article_id = $model->id;
            $oArticleHistory->updated_user_id = Yii::$app->user->identity->id;
            $oArticleHistory->is_autosave = 0;
            $oArticleHistory->content_data = json_encode($aArticleData);
            $oArticleHistory->create_date = date("Y-m-d H:i:s");
            $oArticleHistory->save();

            SliderItems::deleteAll("data_id = {$id}");
            if (isset(Yii::$app->request->post("Article")["aSliders"])) {
                foreach (Yii::$app->request->post("Article")["aSliders"] as $aSliderData) {
                    if ($aSliderData) {
                        $oSliderItem = new SliderItems();
                        if (isset($aSliderData["parent_id"])) {
                            $oSliderItem->parent_id = $aSliderData["parent_id"];
                        }
                        $oSliderItem->slider_id = $aSliderData["slider_id"];
                        $oSliderItem->title = $model->title;
                        $oSliderItem->description = trim(strip_tags($model->full_description));
                        $oSliderItem->extlink = $model->extlink;
                        $oSliderItem->thumbnail_id = $model->thumbnail_id;
                        $oSliderItem->create_user_id = Yii::$app->user->identity->id;
                        $oSliderItem->is_active = 1;
                        $oSliderItem->date_created = date("Y-m-d H:i:s");
                        $oSliderItem->date_display = $model->date_display;
                        $oSliderItem->data_id = $model->id;
                        $oSliderItem->settings_show_title = 1;
                        $oSliderItem->settings_show_description = 1;
                        $oSliderItem->settings_target_blank = 0;
                        $oSliderItem->settings_thumbnail_show = 1;
                        $oSliderItem->save();
                    }
                }
            }

            $aPost = Yii::$app->request->post();

            if (!isset($aPost["Article"]["gallery_id"])) {
                $model->gallery_id = null;
            }

            if (isset($aPost["Article"]["add_categories"])) {
                $model->add_categories_hash = json_encode($aPost["Article"]["add_categories"]);
            } else {
                $model->add_categories_hash = null;
            }
            $model->save();

            TypeHash::deleteAll(['article_id' => $id]);
            if (isset($aPost["Article"]["types"])) {
                foreach ($aPost["Article"]["types"] as $iArticleType) {
                    $oArticleTagHash = new TypeHash();
                    $oArticleTagHash->article_id = $id;
                    $oArticleTagHash->type_id = $iArticleType;
                    $oArticleTagHash->save();
                }
            }

            TagArticle::deleteAll(['article_id' => $id]);
            if (isset($aPost["Article"]["tags"])) {
                foreach ($aPost["Article"]["tags"] as $iArticleTags) {
                    $oArticleTagHash = new TagArticle();
                    $oArticleTagHash->article_id = $id;
                    $oArticleTagHash->tag_id = $iArticleTags;
                    $oArticleTagHash->save();
                }
            }

            ArticleRelated::deleteAll(['article_id' => $id]);
            if (isset($aPost["Article"]["related"])) {
                foreach ($aPost["Article"]["related"] as $iArticleID) {
                    $oArticleRelatedHash = new ArticleRelated();
                    $oArticleRelatedHash->article_id = $id;
                    $oArticleRelatedHash->related_id = $iArticleID;
                    $oArticleRelatedHash->save();
                }
            }

            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('article_update', [
                        'model' => $model,
                        'video' => $video,
                        'oSliderItems' => $oSliderItems,
                        'searchModelComments' => $searchModelComments,
                        'dataProviderComments' => $dataProviderComments
            ]);
        }
    }

    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (Yii::$app->request->get("remove") == 1) {
            SliderItems::deleteAll("data_id = {$id}");
            $model = $this->findModel($id)->delete();

            return $this->redirect(['index', 'deleted' => 1]);
        } else {
            SliderItems::deleteAll("data_id = {$id}");
            $model = $this->findModel($id);
            $model->status_id = 3;
            $model->save();

            return $this->redirect(['index', 'id' => $model->category_id]);
        }
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionActionbar()
    {
        $sAction = Yii::$app->request->post('action');
        $sValue = Yii::$app->request->post('value');
        $aIDs = json_decode(Yii::$app->request->post('rows', ""));

        foreach ($aIDs as $iID) {
            $oArticle = Article::findOne(["id" => $iID]);
            switch ($sAction) {

                case "delete":
                    $oArticle->status_id = 3;
                    $oArticle->save();
                    break;

                case "change-category":
                    $oArticle->category_id = $sValue;
                    $oArticle->save();
                    break;
            }
        }

        exit;
    }

    public function actionGetById($id)
    {
        if (Yii::$app->request->isAjax && !empty($id)) {
            $find = Article::find()->where(["id" => $id])->asArray()->one();

            $src = File::findOne($find['thumbnail_id'])->filename ?? 'default-article.jpg';
            $find['src'] = '/upload/' . $src;

            $array = preg_replace("/<.+>/sU", "", $find);

            echo json_encode($array);
            exit;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAjaxgettypes($query)
    {
        if (Yii::$app->request->isAjax && !empty($query)) {
            $oTypeRowset = Type::find()->where(["LIKE", "name", $query])->all();

            $aTypeItems = [];
            foreach ($oTypeRowset as $index => $oType) {
                $aTypeItems[$index]["id"] = $oType->id;
                $aTypeItems[$index]["text"] = $oType->name;
            }

            $aReturnArr = [];
            $aReturnArr["total_count"] = count($oTypeRowset);
            $aReturnArr["items"] = array_filter($aTypeItems);

            echo json_encode(array_filter($aReturnArr));
            exit;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAjaxgettags($query)
    {
        if (Yii::$app->request->isAjax && !empty($query)) {
            $oTagRowset = Tag::find()->where(["LIKE", "name", $query])->all();

            $aTagItems = [];
            foreach ($oTagRowset as $index => $oTag) {
                $aTagItems[$index]["id"] = $oTag->id;
                $aTagItems[$index]["text"] = $oTag->name;
            }

            $aReturnArr = [];
            $aReturnArr["total_count"] = count($oTagRowset);
            $aReturnArr["items"] = array_filter($aTagItems);

            echo json_encode(array_filter($aReturnArr));
            exit;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAjaxgetarticles($query)
    {
        if (Yii::$app->request->isAjax && !empty($query)) {
            $oArticleRowset = Article::find()->where(["LIKE", "title", $query])->limit(30)->all();

            $aArticleItems = [];
            foreach ($oArticleRowset as $index => $oArticle) {
                $aArticleItems[$index]["id"] = $oArticle->id;
                $aArticleItems[$index]["text"] = $oArticle->title;
            }

            $aReturnArr = [];
            $aReturnArr["total_count"] = count($oArticleRowset);
            $aReturnArr["items"] = array_filter($aArticleItems);

            echo json_encode(array_filter($aReturnArr));
            exit;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAjaxgetcategories($query)
    {
        if (Yii::$app->request->isAjax && !empty($query)) {
            $oRowset = ArticlesCategories::find()->where(["LIKE", "name", $query])->all();

            $aItems = [];
            foreach ($oRowset as $index => $oItem) {
                $aItems[$index]["id"] = $oItem->id;
                $aItems[$index]["text"] = $oItem->name;
            }

            $aReturnArr = [];
            $aReturnArr["total_count"] = count($oRowset);
            $aReturnArr["items"] = array_filter($aItems);

            echo json_encode(array_filter($aReturnArr));
            exit;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAjaxgetauthors($query)
    {
        if (Yii::$app->request->isAjax && !empty($query)) {
            $oRowset = Author::find()->where(["OR", ["LIKE", "firstname", $query], ["LIKE", "surname", $query]])->all();

            $aItems = [];
            foreach ($oRowset as $index => $oItem) {
                $aItems[$index]["id"] = $oItem->id;
                $aItems[$index]["text"] = "{$oItem->firstname} {$oItem->surname}";
            }

            $aReturnArr = [];
            $aReturnArr["total_count"] = count($oRowset);
            $aReturnArr["items"] = array_filter($aItems);

            echo json_encode(array_filter($aReturnArr));
            exit;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function createUniqueUrl($name, $iArticleID = null)
    {
        $unique = false;
        $suffix = '';
        while ($unique === false) {
            $slug = $this->prepareURL($name) . $suffix;

            if ($iArticleID) {
                $oArticle = Article::find()->where(["slug" => $slug])->andWhere(["!=", "id", $iArticleID])->one();
            } else {
                $oArticle = Article::find()->where(["slug" => $slug])->one();
            }

            if (!$oArticle) {
                return $slug;
            } else {
                $suffix = $suffix . '-';
            }
        }
    }

    protected function prepareURL($sText)
    {
        $aReplacePL = array('ą' => 'a', 'ę' => 'e', 'ś' => 's', 'ć' => 'c', 'ó' => 'o', 'ń' => 'n', 'ż' => 'z', 'ź' => 'z', 'ł' => 'l', 'Ą' => 'A', 'Ę' => 'E', 'Ś' => 'S', 'Ć' => 'C', 'Ó' => 'O', 'Ń' => 'N', 'Ż' => 'Z', 'Ź' => 'Z', 'Ł' => 'L');
        $sText = str_replace(array_keys($aReplacePL), array_values($aReplacePL), $sText);
        $sText = str_replace("?", "", $sText);
        $sText = str_replace("„", "", $sText);
        $sText = str_replace("”", "", $sText);
        $sText = str_replace(' ', '-', strtolower($sText));
        $sText = preg_replace('/[\-]+/', '-', $sText);
        $sText = preg_replace('/[^A-Za-z0-9 -]/u', '', $sText);

        return trim($sText, '-');
    }

}
