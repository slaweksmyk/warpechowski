<?php

namespace common\modules\authors\admin;

/**
 * files module definition class
 */
class Authors extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\authors\admin\controllers';
    public $defaultRoute = 'authors';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
}
