<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use backend\helpers\ArticleCategoryHelper;

use common\modules\files\models\File;
use common\modules\users\models\User;
use common\modules\pages\models\PagesSite;
use common\modules\categories\models\ArticlesCategories;

$this->title = "Zaktualizuj autora: {$model->firstname} {$model->surname}";
?>
<div class="author-update">

    <p>
        <?= Html::a('< Wróć  do listy', ['index'], ['class' => 'btn btn-outline-secondary']) ?>
    </p>
    
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-4">
            <section class="panel full">
                <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Zdjęcie</header>
                <div class="panel-body" style="text-align: center; font-size: 12px; font-weight: bold; color: #797c87;">
                    <div class="row">
                        <div class="col-12">
                            <?php if($model->photo_id){ ?>
                                <img src="/upload/<?= File::find()->where(["=", "id", $model->photo_id])->one()->filename  ?>" style="margin: 0 auto; max-height: 203px;" class="img-responsive"/>
                            <?php } else { ?>
                                <img src="<?= \Yii::$app->request->baseUrl ?>images/no-image.png" style="margin: 0 auto;" class="img-responsive"/>
                                <p>Brak zdjęcia</p>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-12">
                            <a href="#" data-init="file-manager" data-target="#change-thumbnail" title="Wybierz zdjęcie z menadżera plików" data-toggle="tooltip" data-placement="bottom">
                                <span class="btn btn-success btn-md" style="width: 100%; margin-top: 20px;">Wybierz główne zdjęcie</span>
                            </a>
                            <?= $form->field($model, 'photo_id')->hiddenInput(["id" => "change-thumbnail"])->label(false) ?>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-8">
            <section class="panel full" >
                <header>
                    <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Dane osobowe
                </header>
                <div class="panel-body" >
                    <div class="row">
                        <div class="col-2">
                            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-5">
                            <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-5">
                            <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-6">
                            <?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-3">
                            <?= $form->field($model, 'facebook_url')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-3">
                            <?= $form->field($model, 'twitter_url')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-3">
                            <?= $form->field($model, 'google_url')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-3">
                            <?= $form->field($model, 'linkedin_url')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-6">
                            <?= $form->field($model, 'extend_page_id')->dropDownList(ArrayHelper::map(PagesSite::find()->all(), 'id', 'name'), ["prompt" => Yii::t('backend_module_articles', '- select extend -')])->label("Podstrona rozwinięcia"); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_types', 'Create') : Yii::t('backend_module_types', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                </div>
            </section>
        </div>
    </div>
    
    <div class="row">
        <div class="col-12">
            <section class="panel full">
                <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Opis</header>
                <div class="panel-body">
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-12">
                            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
                        </div>
                    </div>
                    <div class="form-group" style="text-align: left;">
                        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_types', 'Create') : Yii::t('backend_module_types', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                </div>
            </section>
        </div>
    </div>
    
    <div class="row">
        <div class="col-12">
            <section class="panel full">
                <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Artykuły autora</header>
                <div class="panel-body">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            'id',
                            [
                                'attribute' => 'title',
                                'format' => 'raw',
                                'contentOptions' => ['style' => 'width: 250px;'],
                                'value' => function($data){
                                    if(strlen($data->title) > 20){
                                        $title = substr($data->title, 0, 20)."...";
                                        return "<div class='quick-edit' title='{$data->title}'>{$title} <div style='float: right;' title='Edytuj'>✎</div></div>";
                                    } else {
                                        return "<div class='quick-edit'>{$data->title} <div style='float: right;' title='Edytuj'>✎</div></div>";
                                    }
                                }
                            ],
                            [
                                'attribute' => 'category_id',
                                'format' => 'raw',
                                'contentOptions' => ['style' => 'width: 180px;'],
                                'filter'=> ArrayHelper::map(ArticlesCategories::find()->all(), 'id', 'name'),
                                'value' => function ($data) {
                                    $oCategory = ArticlesCategories::find()->where(["=", "id", $data->category_id])->one();

                                    if (!is_null($oCategory)) {
                                        $sReturn = "<select style='border: none; width: 100%;' class='art-catsel'>";

                                        foreach (ArticleCategoryHelper::generateCategoryTree() as $iCatID => $sCatName) {
                                            $sSelected = "";
                                            if ($iCatID == $data->category_id) {
                                                $sSelected = "selected";
                                            }

                                            $sReturn .= "<option {$sSelected}>{$sCatName}</option>";
                                        }

                                        $sReturn .= "<select>";
                                        return $sReturn;
                                    }
                                }
                            ],
                            [
                                'attribute' => 'update_user_id',
                                'format' => 'raw',
                                'contentOptions' => ['style' => 'width: 145px;'],
                                'value' => function ($data) {
                                    $oUser = User::find()->where(["=", "id", $data->update_user_id])->one();
                                    if (!is_null($oUser)) {
                                        return $oUser->username;
                                    }
                                }
                            ],
                            [
                                'attribute' => 'date_updated',
                                'label' => 'Data aktualizacji'
                            ],
                            [
                                'class' => 'common\hooks\yii2\grid\ActionColumn',
                                'template' => '{update} {duplicate} {delete}',
                                'contentOptions' => ['style' => 'width: 100px;'],
                                'buttons' => [
                                    'update' => function ($url) {
                                        return Html::a(
                                                '<span class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></span>', $url, [
                                                'title' => Yii::t('backend', 'edit'),
                                                'data-toggle' => "tooltip",
                                                'data-placement' => "bottom"
                                            ]
                                        );
                                    },
                                    'duplicate' => function ($url) {
                                        return Html::a(
                                                '<span class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-file"></span></span>', $url, [
                                                'title' => Yii::t('backend', 'duplicate'),
                                                'data-toggle' => "tooltip",
                                                'data-placement' => "bottom"
                                            ]
                                        );
                                    },
                                    'delete' => function ($url) {
                                        return Html::a(
                                                '<span class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></span>', $url, [
                                                'title' => Yii::t('backend', 'delete'),
                                                'data-toggle' => "tooltip",
                                                'data-placement' => "bottom",
                                                'data-pjax' => 0,
                                                'data-confirm' => 'Czy na pewno usunąć ten element?',
                                                'data-method' => 'post',
                                            ]
                                        );
                                    },
                                ],
                            ],
                         ],
                     ]); ?>
                </div>
            </section>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
