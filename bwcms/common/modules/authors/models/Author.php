<?php

namespace common\modules\authors\models;

use Yii;
use common\modules\files\models\File;
use common\modules\pages\models\PagesSite;

use yii\helpers\BaseUrl;

/**
 * This is the model class for table "xmod_author".
 *
 * @property integer $id
 * @property integer $photo_id
 * @property string $firstname
 * @property string $surname
 * @property string $title
 * @property string $position
 * @property string $facebook_url
 * @property string $twitter_url
 * @property string $google_url
 * @property string $linkedin_url
 * @property string $description
 *
 * @property File $photo
 */
class Author extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_authors}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['photo_id'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
            [['description', 'slug', 'email'], 'string'],
            [['firstname', 'surname', 'title', 'position', 'facebook_url', 'twitter_url', 'google_url', 'linkedin_url'], 'string', 'max' => 255],
            [['photo_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['photo_id' => 'id']],
            [['extend_page_id'], 'exist', 'skipOnError' => true, 'targetClass' => PagesSite::className(), 'targetAttribute' => ['extend_page_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_authors', 'ID'),
            'photo_id' => Yii::t('backend_module_authors', 'Photo ID'),
            'firstname' => Yii::t('backend_module_authors', 'Firstname'),
            'surname' => Yii::t('backend_module_authors', 'Surname'),
            'title' => Yii::t('backend_module_authors', 'Title'),
            'position' => Yii::t('backend_module_authors', 'Position'),
            'facebook_url' => Yii::t('backend_module_authors', 'Facebook Url'),
            'twitter_url' => Yii::t('backend_module_authors', 'Twitter Url'),
            'google_url' => Yii::t('backend_module_authors', 'Google Url'),
            'linkedin_url' => Yii::t('backend_module_authors', 'Linkedin Url'),
            'description' => Yii::t('backend_module_authors', 'Description'),
            'email' => "E-mail",
        ];
    }

    public function getAbsoluteUrl(){
        return trim(BaseUrl::home(true), "/").$this->getUrl();
    }
   
    public function getUrl(){
        return $this->getExtendUrl()."".$this->slug;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExtendPage()
    {
        return $this->hasOne(PagesSite::className(), ['id' => 'extend_page_id']);
    }
    
    public function getExtendUrl(){
        if(!is_null($this->extend_page_id)){
            return "/{$this->getExtendPage()->one()->url}/";
        }
        
        return "/";
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhoto($width = 2000, $height = 2000, $type = "crop", $effect = null, $quality = 80)
    {
        return File::getThumbnail($this->file_id, $width, $height, $type, $effect, $quality);
    }
}
