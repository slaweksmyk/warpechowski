<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\settings\models\Settings */

$this->title = Yii::t('backend', 'module_translation_create');
$this->params['breadcrumbs'][] = ['label' => 'Translataion', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-create">

    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?></header>
        <div class="panel-body" >
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </section>     

</div>
