<?php

use yii\helpers\Html;

$this->title = Yii::t('backend', 'module_translation_update').": ". $model->translation;
?>
<div class="settings-update">

    <p>
        <?= Html::a(Yii::t('backend', 'module_translation_create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?></header>
        <div class="panel-body" >
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </section>

</div>
