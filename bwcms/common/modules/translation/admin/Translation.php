<?php

namespace common\modules\translation\admin;

/**
 * Settings module definition class
 */
class Translation extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\translation\admin\controllers';
    public $defaultRoute = 'translation';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
