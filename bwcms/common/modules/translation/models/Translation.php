<?php

namespace common\modules\translation\models;

use Yii;
use creocoder\translateable\TranslateableBehavior;

/**
 * This is the model class for table "{{%translation}}".
 *
 * @property integer $id
 * @property string $code
 * @property string $translation
 */
class Translation extends \common\hooks\yii2\db\ActiveRecord
{
    private $params = [];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%translation}}';
    }
	
    public function __construct(){
            $this->params = require(Yii::getAlias('@backend/config/main.php'));
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => ['translation'],
                'translationLanguageAttribute' => 'language',
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_INSERT | self::OP_UPDATE,
        ];
    }

    /**
     * @inheritdoc
     */
    public function getTranslations()
    {
        return $this->hasMany(Translation_i18n::className(), ['translation_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'translation'], 'string', 'max' => 1024],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => Yii::t('backend', 'code'),
            'translation' => Yii::t('backend', 'translation'),
            
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function __set($name, $value) {
        if(in_array($name, ["translation"])){
            $this->translate(Yii::$app->session->get('backend_language'))->{$name} = $value;
        }
        
        parent::__set($name, $value);
    }
    
    public function __get($name) {
        if(in_array($name, ["translation"])){
            if(strpos(Yii::$app->request->url, $this->params["homeUrl"]) !== false){
                $translatedAttr = $this->translate(Yii::$app->session->get('backend_language'))->{$name};
            } else {
                $translatedAttr = $this->translate(Yii::$app->session->get('frontend_language'))->{$name};
            }

            if($translatedAttr){
                return $translatedAttr;
            }
        }

        return parent::__get($name);
    }
    
  
    
}
