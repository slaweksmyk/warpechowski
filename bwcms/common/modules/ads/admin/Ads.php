<?php

namespace common\modules\ads\admin;

/**
 * files module definition class
 */
class Ads extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\ads\admin\controllers';
    public $defaultRoute = 'ads';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
}
