<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\modules\files\models\File;

$this->title = Yii::t('backend_module_ads', 'Update {modelClass}: ', [
            'modelClass' => 'Ad',
        ]) . $model->name;
?>
<div class="ad-update">

    <p>
        <?= Html::a('< Wróć  do listy', ['index'], ['class' => 'btn btn-outline-secondary']) ?>
    </p>

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-4">
            <section class="panel full" >
                <header>
                    <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Grafika
                </header>
                <div class="panel-body" style="text-align: center;" >
                    <?php if ($model->thumbnail_id) { ?>
                        <img src="/upload/<?= File::find()->where(["=", "id", $model->thumbnail_id])->one()->filename ?>" style="margin: 0 auto; max-height: 203px;" class="img-responsive"/>
                    <?php } else { ?>
                        <img src="<?= \Yii::$app->request->baseUrl ?>images/no-image.png" style="margin: 0 auto;" class="img-responsive"/>
                        <p>Brak zdjęcia</p>
                    <?php } ?>

                    <div class="col-12" style="margin-top: 10px;">
                        <a href="#" data-init="file-manager" data-target="#change-thumbnail" title="Wybierz miniaturę z menadżera plików" data-toggle="tooltip" data-placement="bottom">
                            <span class="btn btn-success btn-md" style="width: 100%;">Wybierz miniaturę</span>
                        </a>
                    </div>   

                    <div class="col-12" style="margin-top: 10px;">
                        <?= $form->field($model, 'thumbnail_id')->hiddenInput(["id" => "change-thumbnail"])->label(false); ?>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-8">
            <section class="panel full" >
                <header>
                    <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
                </header>
                <div class="panel-body" >
                    <div class="row">
                        <div class="col-6">
                            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-6">
                            <?= $form->field($model, 'url')->textInput(['maxlength' => true])->label("Adres Url") ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4">
                            <?= $form->field($model, 'width')->textInput(['maxlength' => true])->label("Szerokość (px)") ?>
                        </div>
                        <div class="col-4">
                            <?= $form->field($model, 'height')->textInput(['maxlength' => true])->label("Wysokość (px)") ?>
                        </div>
                        <div class="col-4">
                            <?= $form->field($model, 'is_active')->dropDownList([0 => Yii::t('backend_module_articles', 'inactive'), 1 => Yii::t('backend_module_articles', 'active')]) ?>
                        </div>
                    </div>

                    <?= $form->field($model, 'code')->textarea(['rows' => 6, 'class' => "noTynyMCE", "style" => "width: 100%;"]) ?>

                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_ads', 'Create') : Yii::t('backend_module_ads', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
