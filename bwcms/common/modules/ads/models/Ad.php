<?php

namespace common\modules\ads\models;

use Yii;
use common\modules\files\models\File;

use yii\imagine\Image;
use Imagine\Image\Box;

/**
 * This is the model class for table "xmod_ads".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 */
class Ad extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_ads}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['thumbnail_id', 'is_active'], 'integer'],
            [['code', 'height', 'width'], 'string'],
            [['name', 'url'], 'string', 'max' => 255],
            [['thumbnail_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['thumbnail_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_ads', 'ID'),
            'name' => Yii::t('backend_module_ads', 'Name'),
            'code' => Yii::t('backend_module_ads', 'Code'),
            'is_active' => Yii::t('backend_module_articles', 'Is Active'),
        ];
    }
    
    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getThumbnail($width = 2000, $height = 2000, $type = "crop", $effect = null, $quality = 80) 
    { 
        return File::getThumbnail($this->file_id, $width, $height, $type, $effect, $quality);
    } 
    
    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function hasThumbnail(){
        return !is_null($this->hasOne(File::className(), ['id' => 'thumbnail_id'])->one());
    }
    
}
