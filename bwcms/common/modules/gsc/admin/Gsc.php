<?php
namespace common\modules\gsc\admin;

/**
 * layout module definition class
 */
class Gsc extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\gsc\admin\controllers';
    public $defaultRoute = 'gsc';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
