<?php

/**
 * Article module
 * 
 * PHP version 7
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @copyright (c) 2016 Krzysztof Borecki
 * @version 1.0
 */

namespace common\modules\gsc\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class GscController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
        //putenv('GOOGLE_APPLICATION_CREDENTIALS=/path/to/service-account.json');
        
        
        $oClient = new \Google_Client();
        //$oClient->useApplicationDefaultCredentials();
        
        $oClient->setDeveloperKey("AIzaSyByHaNoepqCozysHlu1PiJ1SaWT7ki7bVE");
        $oClient->addScope("https://www.googleapis.com/auth/webmasters");
        
        $oGsc = new \Google_Service_SearchConsole($oClient);
        
        $httpClient = $oClient->authorize();
        $response = $httpClient->get('https://www.googleapis.com/webmasters/v3/sites?fields=siteEntry&key=AIzaSyByHaNoepqCozysHlu1PiJ1SaWT7ki7bVE');
        

        var_dump($response);
        exit;
        return $this->render('index', [

        ]);
    }
    
    
}
