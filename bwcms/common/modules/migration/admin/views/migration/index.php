<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = "Migracja danych";
?>

<?php $form = ActiveForm::begin(); ?>
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?> - pozyskiwanie danych
        </header>
        <div class="panel-body" >

            <div class="row">
                <div class="col-12"><?= $form->field($model, 'main_url')->textInput() ?></div>
            </div>

            <div class="row">
                <div class="col-10"><?= $form->field($model, 'db_host')->textInput() ?></div>
                <div class="col-2"><?= $form->field($model, 'db_port')->textInput() ?></div>
            </div>

            <div class="row">
                <div class="col-4"><?= $form->field($model, 'db_username')->textInput() ?></div>
                <div class="col-4"><?= $form->field($model, 'db_password')->textInput() ?></div>
                <div class="col-4"><?= $form->field($model, 'db_database')->textInput() ?></div>
            </div>

            <div class="form-group">
                <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
            </div>


        </div>
    </section>

<style>
    label { display: block; }
</style>
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?> - elementy do przeniesienia
        </header>
        <div class="panel-body" >
            <?php
                $aElements = [0 => 'Artykuły', 1 => 'Użytkownicy'];
            ?>
            
            <?= Html::checkboxList('aElements',[],$aElements); ?>

            <div class="form-group">
                <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </section>
<?php ActiveForm::end(); ?>