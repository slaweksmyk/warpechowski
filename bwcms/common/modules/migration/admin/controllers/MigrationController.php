<?php

namespace common\modules\migration\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;

use common\modules\migration\models\Migration;

/**
 * RssController implements the CRUD actions for Rss model.
 */
class MigrationController extends Controller
{
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Rss models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new Migration();
        
        if ($model->load(Yii::$app->request->post())) {
            $model->connect();
            
            $model->migrateElements();
            
            return $this->render('index', [
                "model" => $model
            ]);
        } else {
            return $this->render('index', [
                "model" => $model
            ]);
        }

    }

}
