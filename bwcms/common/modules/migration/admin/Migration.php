<?php

namespace common\modules\migration\admin;

/**
 * files module definition class
 */
class Migration extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\migration\admin\controllers';
    public $defaultRoute = 'migration';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
