<?php

namespace common\modules\migration\models;

use Yii;
use yii\base\Model;
use yii\db\Connection;

use common\modules\files\models\File;
use common\modules\files\models\FileCategory;

use common\modules\articles\models\Article;
use common\modules\articles\models\ArticlesCategories;

class Migration extends Model
{

    public $db_host;
    public $db_port = 3306;
    public $db_username;
    public $db_password;
    public $db_database;
    
    public $main_url;
    
    protected $connection;
    
    private $migrateCategoryID;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['main_url', 'db_host', 'db_port', 'db_username', 'db_database'], 'required', 'message' => 'To pole jest wymagane'],
        ];
    }

    public function connect(){
        $this->connection = new Connection([
            'dsn' => "mysql:host={$this->db_host};dbname={$this->db_database}",
            'username' => $this->db_username,
            'password' => $this->db_password,
        ]);
        $this->connection->open();
        $this->connection->createCommand('SET NAMES utf8')->execute();
    }
    
    public function migrateElements(){
        $request = Yii::$app->request;
        
        foreach($request->post('aElements') as $element){
            if($element === "0"){ $this->migrateArticles(); }
            if($element === "1"){ /* Users */ }
        }
    }
    
    /*
     * Migrate Articles
     */
    private function migrateArticles(){
        set_time_limit(5000);
        
        $oFileCategoryModel = FileCategory::findOne(["name" => "Migracja - miniatury"]);
        if(is_null($oFileCategoryModel)){
            $oFileCategoryModel = new FileCategory();
            $oFileCategoryModel->parent_id = 0;
            $oFileCategoryModel->name = "Migracja - miniatury";
            $oFileCategoryModel->sort = 0;
            $oFileCategoryModel->save();
        }
        
        $this->migrateCategoryID = $oFileCategoryModel->id;

        Article::deleteAll();
        ArticlesCategories::deleteAll();
        
        $aCategoryRowset = $this->connection->createCommand('SELECT * FROM `article_category`')->queryAll();
        foreach($aCategoryRowset as $aCategory){
            $oCategoryModel = new ArticlesCategories();
            $oCategoryModel->id = $aCategory["id"];
            $oCategoryModel->parent_id = $aCategory["parent_id"];
            $oCategoryModel->name = $aCategory["name"];
            $oCategoryModel->slug = $this->prepareURL($aCategory["name"]);
            $oCategoryModel->description = $aCategory["description"];
            $oCategoryModel->sort = $aCategory["sort"];
            $oCategoryModel->is_active = $aCategory["is_active"];
            $oCategoryModel->save();

            $this->getArticles($aCategory["id"]);
        }
    }
    
    private function getArticles($iOldCategoryID){
        $aArticleRowset = $this->connection->createCommand("SELECT * FROM `article` WHERE `article_category_id` = {$iOldCategoryID}")->queryAll();
        
        foreach($aArticleRowset as $aArticle){
            $oArticleModel = new Article();
            $oArticleModel->category_id = $aArticle["article_category_id"];
            $oArticleModel->title = $aArticle["name"];
            $oArticleModel->slug = $this->createUniqueUrl($aArticle["name"]);
            $oArticleModel->extlink = $aArticle["extlink"];
            $oArticleModel->short_description = $aArticle["short_description"];
            $oArticleModel->full_description = $aArticle["description"];
            $oArticleModel->is_active = $aArticle["is_active"];
            $oArticleModel->active_from = $aArticle["start_publish_at"];
            $oArticleModel->active_to = $aArticle["finish_publish_at"];
            $oArticleModel->sort = $aArticle["sort"];
            $oArticleModel->create_user_id = Yii::$app->user->id;
            $oArticleModel->date_created = $aArticle["created_at"];
            $oArticleModel->update_user_id = Yii::$app->user->id;
            $oArticleModel->date_updated = $aArticle["updated_at"];
            $oArticleModel->date_display = $aArticle["add_at"];
            $oArticleModel->published = $aArticle["is_active"];
            $oArticleModel->seo_keywords = $aArticle["meta_keywords"];
            $oArticleModel->seo_description = $aArticle["meta_description"];

            if(!is_null($aArticle["files_item_id"])){
                $oArticleModel->thumbnail_id = $this->getThumbnail($aArticle["files_item_id"]);
            }
            
            $oArticleModel->save();
        }
        
    }
    
    private function getThumbnail($iFileID){
        $aFile = $this->connection->createCommand("SELECT * FROM `files_item` WHERE `id` = {$iFileID}")->queryOne();
        file_put_contents(Yii::getAlias("@root/upload/{$aFile['filename']}"), file_get_contents("{$this->main_url}/files/manager/{$aFile['filename']}"));
        
        $oFileModel = new File();
        $oFileModel->category_id = $this->migrateCategoryID;
        $oFileModel->name = $aFile["name"];
        $oFileModel->filename = $aFile["filename"];
        $oFileModel->thumbnail = 1;
        $oFileModel->type = "image/{$aFile["fileextension"]}";
        $oFileModel->size = $aFile["filesize"];
        $oFileModel->save();
        
        return $oFileModel->id;
    }
    
    protected function createUniqueUrl($name){
        $unique = false;
        $suffix = '';
        while ($unique === false) {
            $slug = $this->prepareURL($name).$suffix;
            $oArticle = Article::find()->where(["slug" => $slug])->one();
            if(!$oArticle){
                return $slug;
            } else {
                $suffix = $suffix . '-';
            }
        }
    }
    
    protected function prepareURL($sText){
        $aReplacePL = array( 'ą' => 'a', 'ę' => 'e', 'ś' => 's', 'ć' => 'c', 'ó' => 'o', 'ń' => 'n', 'ż' => 'z', 'ź' => 'z', 'ł' => 'l', 'Ą' => 'A', 'Ę' => 'E', 'Ś' => 'S', 'Ć' => 'C', 'Ó' => 'O', 'Ń' => 'N', 'Ż' => 'Z', 'Ź' => 'Z', 'Ł' => 'L');
        $sText = str_replace(array_keys($aReplacePL), array_values($aReplacePL), $sText);
        $sText = str_replace(' ', '-', strtolower($sText));
        $sText = preg_replace('/[\-]+/', '-', $sText);
        return trim($sText, '-');
    }
    
    
}
