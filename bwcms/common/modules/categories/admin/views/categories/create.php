<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\categories\models\ArticlesCategories;

$this->title = Yii::t('backend_module_articles', 'Create Articles Categories');
?>
<div class="articles-categories-create">

    <p>
        <?= Html::a(Yii::t('backend', 'back_to_category'), ['index'], ['class' => 'btn btn-outline-secondary']) ?>
    </p>
    
    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?></header>
        <div class="panel-body" >
            <div class="article-form">
                <?php $form = ActiveForm::begin(); ?>
                
                <div class="row">
                    <div class="col-4"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
                    <div class="col-4"><?= $form->field($model, 'is_active')->dropDownList([0 =>  Yii::t('backend_module_articles', 'inactive'), 1 =>  Yii::t('backend_module_articles', 'active')]) ?></div>
                    <div class="col-4">
                        <?php
                            $aCategories = [];
                            $aCategories[null] = "- brak -";
                            foreach(ArticlesCategories::find()->all() as $oCategory){
                                $aCategories[$oCategory->id] = $oCategory->name;
                            }
                            echo $form->field($model, 'parent_id')->dropDownList($aCategories)
                        ?>
                    </div>
                </div>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('backend',  'create') : Yii::t('backend',  'update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </section>

</div>
