<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use common\modules\files\models\File;
use common\modules\pages\models\PagesSite;
use common\modules\categories\models\ArticlesCategories;

$this->title = Yii::t('backend_module_articles', 'editCategory');
?>

<p>
    <?= Html::a(Yii::t('backend', 'back_to_category'), ['index'], ['class' => 'btn btn-outline-secondary']) ?>
</p>
<div class="articles-categories-update">

    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?></header>
        <div class="panel-body" >
            <div class="article-form">
                <?php $form = ActiveForm::begin(); ?>
                
                <div class="row">
                    <div class="col-9"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
                    <div class="col-3"><?= $form->field($model, 'sort')->textInput() ?></div>
                </div>
                
                <div class="row">
                    <div class="col-3">
                        <?= $form->field($model, 'is_active')->dropDownList([0 =>  Yii::t('backend_module_articles', 'inactive'), 1 =>  Yii::t('backend_module_articles', 'active')]) ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'parent_id')->dropDownList(ArrayHelper::map(ArticlesCategories::find()->all(), 'id', 'name'), ["prompt" => Yii::t('backend_module_articles', '- select parent -')]) ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'extend_page_id')->dropDownList(ArrayHelper::map(PagesSite::find()->all(), 'id', 'name'), ["prompt" => Yii::t('backend_module_articles', '- select extend -')]); ?>
                    </div>
                    <div class="col-3">
                        <a href="#" data-init="file-manager" data-target="#change-thumbnail" title="Wybierz zdjęcie z menadżera plików" data-toggle="tooltip" data-placement="bottom">
                            <span class="btn btn-success btn-md" style="width: 100%; margin-top: 20px;">Wybierz główne zdjęcie</span>
                        </a>
                        <?= $form->field($model, 'thumbnail_id')->hiddenInput(["id" => "change-thumbnail"])->label(false) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-9">
                        <div class="form-group">
                            <label class="control-label" for="types">Typy kategorii</label>
                            <div class="row">
                                <div class="col-12 article-types">
                                    <?php foreach($model->getTypes()->all() as $oTypeHash){ ?>
                                        <span>
                                            <input type="hidden" name="ArticlesCategories[types][]" value="<?= $oTypeHash->type_id ?>"/>
                                            <span title="Usuń typ kategorii">x</span>
                                            <?= $oTypeHash->getType()->one()->name ?>
                                        </span>
                                    <?php } ?>
                                </div>
                            </div>
                            <select id="types" class="form-control ajax-load-article-types" name="ArticlesCategories[types][]"></select>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-3">
                        <a href="#" data-init="file-manager" data-target="#add-change-thumbnail" title="Wybierz zdjęcie z menadżera plików" data-toggle="tooltip" data-placement="bottom">
                            <span class="btn btn-success btn-md" style="width: 100%; margin-top: 20px;">Wybierz dodatkowe zdjęcie</span>
                        </a>
                        <?= $form->field($model, 'add_thumbnail_id')->hiddenInput(["id" => "add-change-thumbnail"])->label(false) ?>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-9"><?= $form->field($model, 'description')->textarea(['rows' => 6]) ?></div>
                    <div class="col-3 seo-tips"></div>
                </div>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_articles',  'create') : Yii::t('backend_module_articles',  'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </section>      

</div>
