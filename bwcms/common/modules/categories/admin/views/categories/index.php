<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;
use common\modules\categories\models\ArticlesCategories;

$this->title = Yii::t('backend_module_articles', 'Articles Categories');
?>
<div class="articles-categories-index">

    <p>
        <?= Html::a(Yii::t('backend_module_articles', 'Create Articles Categories'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <div class="row">
        <div class="col-12">
            <section class="panel full" >
                <header>
                    <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            <form id="per-page">
                        <span><?= Yii::t('backend', 'quantity_per_page'); ?>:</span>
                        <select name="per-page" class="form-control">
                            <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                        </select>
                    </form>
                </header>
                <div class="panel-body" >
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                         'columns' => [
                            'id',
                            [
                                'attribute' => 'parent_id',
                                'format' => 'raw',
                                'contentOptions'=>['style'=>'width: 145px;'],
                                'value' => function ($data) {
                                    return ArticlesCategories::find()->where(["=", "id", $data->id])->one()->name;
                                }
                            ],
                            'name',
                            [
                                'attribute' => 'is_active',
                                'format' => 'raw',
                                'contentOptions'=>['style'=>'width: 145px;'],
                                'value' => function ($data) {
                                    return (1 == $data["is_active"]) ?  Yii::t('backend', 'active') :  Yii::t('backend', 'inactive');
                                }
                            ],
                            [
                                'attribute' => 'sort',
                                'contentOptions'=>['style'=>'width: 145px;'],
                            ],   
                            [
                                'class' => 'common\hooks\yii2\grid\ActionColumn',
                                'template' => '{update} {view} {delete}',
                                'contentOptions' => ['style' => 'width: 100px;'],
                                'buttons' => [
                                    'update' => function ($url) {
                                        return Html::a(
                                                        '<span class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></span>', $url, [
                                                    'title' => Yii::t('backend', 'edit'),
                                                    'data-toggle' => "tooltip",
                                                    'data-placement' => "bottom"
                                                        ]
                                        );
                                    },
                                    'view' => function ($url) {
                                        return Html::a(
                                                        '<span class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-eye-open"></span></span>', $url, [
                                                    'title' => Yii::t('backend', 'view'),
                                                    'data-toggle' => "tooltip",
                                                    'data-placement' => "bottom"
                                                        ]
                                        );
                                    },
                                    'delete' => function ($url) {
                                        return Html::a(
                                                        '<span class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></span>', $url, [
                                                    'title' => Yii::t('backend', 'delete'),
                                                    'data-toggle' => "tooltip",
                                                    'data-placement' => "bottom",
                                                    'data-pjax' => 0,
                                                    'data-confirm' => 'Czy na pewno usunąć ten element?',
                                                    'data-method' => 'post',
                                                        ]
                                        );
                                    },
                                ],
                            ],
                        ],
                    ]); ?>
                </div>
            </section>   
        </div>
        <div class="col-12">
            <section class="panel full" >
                <header>
                    <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend', 'category_map') ?>    
                </header>
                <div class="panel-body" >
                    <div class="clt">
                        <ul>
                            <?php foreach(ArticlesCategories::find()->where(["=", "parent_id", 0])->all() as $oCategory){ ?>
                                <li>
                                    <div class="dir"></div> 
                                    <a href="<?= \Yii::$app->request->baseUrl ?>articles/article/articles/?id=<?= $oCategory->id ?>"><?= $oCategory->name ?></a>
                                    <a href="<?= \Yii::$app->request->baseUrl ?>categories/categories/update/?id=<?= $oCategory->id ?>"><span class="btn btn-primary btn-xs edit"><span class="glyphicon glyphicon-pencil"></span></span></a>
                                    <?php if(count(ArticlesCategories::find()->where(["=", "parent_id", $oCategory->id])->all()) > 0){ ?>
                                        <ul>
                                            <?php foreach(ArticlesCategories::find()->where(["=", "parent_id", $oCategory->id])->all() as $oChild){ ?>
                                                <li>
                                                    <div class="dir"></div> 
                                                    <a href="<?= \Yii::$app->request->baseUrl ?>articles/article/articles/?id=<?= $oChild->id ?>"><?= $oChild->name ?></a>
                                                    <a href="<?= \Yii::$app->request->baseUrl ?>categories/categories/update/?id=<?= $oChild->id ?>"><span class="btn btn-primary btn-xs edit"><span class="glyphicon glyphicon-pencil"></span></a>
                                                    <?php if(count(ArticlesCategories::find()->where(["=", "parent_id", $oChild->id])->all()) > 0){ ?>
                                                        <ul>
                                                            <?php foreach(ArticlesCategories::find()->where(["=", "parent_id", $oChild->id])->all() as $oChild2){ ?>
                                                                <li>
                                                                    <div class="dir"></div> 
                                                                    <a href="<?= \Yii::$app->request->baseUrl ?>articles/article/articles/?id=<?= $oChild2->id ?>"><?= $oChild2->name ?></a>
                                                                    <a href="<?= \Yii::$app->request->baseUrl ?>categories/categories/update/?id=<?= $oChild2->id ?>"><span class="btn btn-primary btn-xs edit"><span class="glyphicon glyphicon-pencil"></span></a>
                                                                </li>
                                                            <?php } ?>
                                                        </ul>
                                                    <?php } ?>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    <?php } ?>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
