<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->name;
?>
<div class="articles-categories-view">

    <p>
          <?= Html::a(Yii::t('backend', 'back_to_category'), ['index'], ['class' => 'btn btn-outline-secondary']) ?>
        <?= Html::a(Yii::t('backend_module_articles', 'update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('backend_module_articles', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend_module_articles', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
        </header>
        <div class="panel-body" >
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
            'parent_id',
            'name',
            [
                'attribute' => 'slug',
                'label' => Yii::t('backend', 'Slug'),
            ],
            'sort',
            'description:ntext',
            'is_active',
                ],
            ]) ?>
        </div>
    </section>  

</div>
