<?php

namespace common\modules\categories\admin;

/**
 * files module definition class
 */
class Categories extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\categories\admin\controllers';
    public $defaultRoute = 'categories';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
