<?php

namespace common\modules\categories\admin\controllers;

use Yii;
use common\modules\categories\models\ArticlesCategories;
use common\modules\categories\models\ArticlesCategoriesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\helpers\UrlHelper;
use common\modules\types\models\CategoryTypesHash;

/**
 * CategoryController implements the CRUD actions for ArticlesCategories model.
 */
class CategoriesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ArticlesCategories models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticlesCategoriesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ArticlesCategories model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ArticlesCategories model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ArticlesCategories();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->date_created = date("Y-m-d H:i:s");
            $model->slug = UrlHelper::prepareURL($model->name);
            $model->save();
            
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ArticlesCategories model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->date_updated = date("Y-m-d H:i:s");
            $model->slug = UrlHelper::prepareURL($model->name);
            $model->save();
            
            $aPost = Yii::$app->request->post();
            CategoryTypesHash::deleteAll(['category_id' => $id]);
            if(isset($aPost["ArticlesCategories"]["types"])){
                foreach($aPost["ArticlesCategories"]["types"] as $iCategoryType){
                    $oCategoryTagHash = new CategoryTypesHash();
                    $oCategoryTagHash->category_id = $id;
                    $oCategoryTagHash->type_id = $iCategoryType;
                    $oCategoryTagHash->save();
                }
            }
            
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ArticlesCategories model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ArticlesCategories model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ArticlesCategories the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ArticlesCategories::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
