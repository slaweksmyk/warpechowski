<?php

namespace common\modules\categories\models;

use Yii;

/**
 * This is the model class for table "{{%xmod_articles_i18n}}".
 *
 * @property integer $id
 * @property string $language
 * @property string $name
 * @property string $description
 */
class ArticlesCategories_i18n extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_articles_categories_i18n}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['description'], 'string'],
            [['language', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'language' => Yii::t('backend', 'Language'),
            'name' => Yii::t('backend', 'Name'),
            'description' => Yii::t('backend', 'Description'),
        ];
    }
}
