<?php

namespace common\modules\categories\models;

use Yii;
use common\modules\files\models\File;
use common\modules\pages\models\PagesSite;
use common\modules\articles\models\Article;
use creocoder\translateable\TranslateableBehavior;
use common\modules\categories\models\ArticlesCategories_i18n;

use yii\helpers\BaseUrl;
use common\modules\types\models\CategoryTypesHash;

use yii\imagine\Image;
use Imagine\Image\Box;

/**
 * This is the model class for table "xmod_articles_categories".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $extend_page_id
 * @property string $name
 * @property string $slug
 * @property integer $sort
 * @property string $description
 * @property integer $is_active
 *
 * @property Article[] $xmodArticles
 * @property PagesSite $extendPage
 * @property ArticlesCategories $parent
 * @property ArticlesCategories[] $articlesCategories
 * @property ArticleCategoriesI18n[] $xmodArticlesCategoriesI18ns
 */
class ArticlesCategories extends \common\hooks\yii2\db\ActiveRecord
{
    protected $aTranslateColumns = ['name', 'description'];
    private $params = [];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_articles_categories}}';
    }
	
    public function __construct(){
            $this->params = require(Yii::getAlias('@backend/config/main.php'));
    }
    
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => $this->aTranslateColumns,
                'translationLanguageAttribute' => 'language',
            ],
        ];
    }  
    
    /**
     * @inheritdoc
     */
    public function getTranslations()
    {
        return $this->hasMany(ArticlesCategories_i18n::className(), ['category_id' => 'id']);
    }     

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'extend_page_id', 'thumbnail_id', 'add_thumbnail_id', 'sort', 'is_active'], 'integer'],
            [['description'], 'string'],
            [['date_created', 'date_updated', 'cache_file', 'old_id'], 'safe'],
            [['name', 'slug', 'url'], 'string', 'max' => 255],
            [['extend_page_id'], 'exist', 'skipOnError' => true, 'targetClass' => PagesSite::className(), 'targetAttribute' => ['extend_page_id' => 'id']],
            [['thumbnail_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['thumbnail_id' => 'id']],
            [['parent_id'], 'exist', 'skipOnError' => true, 'skipOnEmpty' => true, 'targetClass' => ArticlesCategories::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_articles', 'ID'),
            'parent_id' => Yii::t('backend_module_articles', 'Parent ID'),
            'extend_page_id' => Yii::t('backend_module_articles', 'Extend Page ID'),
            'name' => Yii::t('backend_module_articles', 'Name'),
            'slug' => Yii::t('backend_module_articles', 'Slug'),
            'sort' => Yii::t('backend_module_articles', 'Sort'),
            'description' => Yii::t('backend_module_articles', 'Description'),
            'is_active' => Yii::t('backend_module_articles', 'Is Active'),
        ];
    }
    
    public function getAddThumbnail($height = null, $width = null, $type = "crop", $effect = null, $quality = 80) {
        return File::getThumbnail($this->add_thumbnail_id, $width, $height, $type, $effect, $quality);
    }
    
    public function getAbsoluteUrl(){
        return trim(BaseUrl::home(true), "/").$this->getUrl();
    }
   
    public function getUrl(){
        //return $this->getExtendUrl()."".$this->slug;
        return $this->getExtendUrl();
    }

    public function hasType($typeID){
        foreach($this->getTypes()->all() as $oTypeHash){
            if($typeID == $oTypeHash->type_id){
                return true;
            }
        }
        return false;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypes()
    {
        return $this->hasMany(CategoryTypesHash::className(), ['category_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasMany(Article::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(PagesSite::className(), ['article_category_id' => 'id']);
    }

    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getThumbnail($width, $height, $type = "crop", $effect = null, $quality = 80) 
    { 
        if(is_null($this->cache_file)){
            $oThumbnail = $this->hasOne(File::className(), ['id' => 'thumbnail_id'])->one();
            if ($oThumbnail->type == 'image/svg+xml') {
                $this->cache_file = "/upload/{$oThumbnail->filename}";
                $this->save();
            } else {
                $effect_safe = str_replace("#", "", $effect);
                $sFilePath = Yii::getAlias("@root/upload/{$oThumbnail->filename}");

                $aTmp = explode("/", $oThumbnail->filename);
                $oThumbnail->filename = end($aTmp);
                $sReworkedPath = Yii::getAlias("@root/cache/img/{$width}_{$height}_{$type}_{$effect_safe}_{$oThumbnail->filename}");
                if (file_exists($sFilePath)) {
                    switch ($type) {
                        case "crop";
                            Image::thumbnail($sFilePath, $height, $width)->save($sReworkedPath, ['quality' => $quality]);
                            break;

                        case "matched";
                            Image::getImagine()->open($sFilePath)->thumbnail(new Box($width, $height))->save($sReworkedPath, ['quality' => $quality]);
                            break;
                    }

                    if (!is_null($effect)) {
                        $image = yii\imagine\Image::getImagine();
                        $newImage = $image->open($sReworkedPath);

                        switch ($effect) {
                            case "grayscale":
                                $newImage->effects()->grayscale();
                                break;

                            case "negative":
                                $newImage->effects()->negative();
                                break;

                            default:
                                if (ctype_xdigit($effect_safe)) {
                                    $color = $newImage->palette()->color($effect);
                                    $newImage->effects()->colorize($color);
                                }
                                break;
                        }
                        $newImage->save($sReworkedPath, ['quality' => $quality]);
                    }
                }
                
                $this->cache_file = "/cache/img/{$width}_{$height}_{$type}_{$effect_safe}_{$oThumbnail->filename}";
                $this->save();
            }
        }
        return $this->cache_file;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExtendPage()
    {
        return $this->hasOne(PagesSite::className(), ['id' => 'extend_page_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(ArticlesCategories::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(ArticlesCategories::className(), ['parent_id' => 'id']);
    }
    
    public function getExtendUrl(){
        if(!is_null($this->extend_page_id)){
            $sUrl = "";
            if(Yii::$app->params['oConfig']->default_language != Yii::$app->session->get('frontend_language')){
                $sLang = strtolower(current(explode("-", Yii::$app->session->get('frontend_language'))));
                $sUrl  = "/{$sLang}/{$this->getExtendPage()->one()->url}/";
            } else { $sUrl = "/{$this->getExtendPage()->one()->url}/"; }

            return $sUrl;
        }
        
        return "/";
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::className(), ['category_id' => 'id'])->where(["=", "status_id", 2]);
    }
    
    public function __set($name, $value) {
        if(in_array($name, $this->aTranslateColumns)){
            $this->translate(Yii::$app->session->get('backend_language'))->{$name} = $value;
        }
        
        parent::__set($name, $value);
    }

    public function __get($name) {
        if(in_array($name, $this->aTranslateColumns)){
            if(strpos(Yii::$app->request->url, $this->params["homeUrl"]) !== false){
                $translatedAttr = $this->translate(Yii::$app->session->get('backend_language'))->{$name};
            } else {
                $translatedAttr = $this->translate(Yii::$app->session->get('frontend_language'))->{$name};
            }

            if($translatedAttr){
                return $translatedAttr;
            }
        }
        
        return parent::__get($name);
    }
}
