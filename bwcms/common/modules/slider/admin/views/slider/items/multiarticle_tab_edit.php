<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Countries */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
$this->registerJs(
        '$("document").ready(function(){ 
        $("#new_item").on("pjax:end", function() {
            $.pjax.reload({container:"#' . $model->id . ' #items"});
        
        });
       
    });'
);
?>

<div class="edit-item-form-tab">
    <div class="row">
        <div class="col-8">
       
        <?php yii\widgets\Pjax::begin(['id' => 'edit_item']) ?>

        <?php
        $form = ActiveForm::begin([
                    'action' => ['slider/update-items/?id=' . $model->id],
                    'options' => ['data-pjax' => true, 'class' => 'form-inline']
                        ]
        );
        ?>

        <?= $form->field($model, 'id')->hiddenInput(['value' => $model->id])->label(false) ?>
        <?= $form->field($model, 'updated_user_id')->hiddenInput(['value' => Yii::$app->user->identity->id])->label(false) ?>


        <?= $form->field($model, 'title')->textInput(['maxlength' => 200, 'class' => 'input-sm'])->label(false); ?>

        <?= Html::submitButton(Yii::t('backend_module_slider', 'Update Title'), ['class' => 'btn btn-outline-secondary btn-sw']) ?>


        <?php ActiveForm::end(); ?>
        <?php yii\widgets\Pjax::end() ?>
             </div>
         <div class="col-4 text-right">
             
          <?= Html::button(Yii::t('backend_module_slider', 'Delete'), ['class' => 'btn btn-danger', 'data-target' => '.modal-delete-slide', 'data-toggle' => 'modal', 'data-action' => 'items-delete-slide', 'data-slide' => $model->id, 'data-item' => $model->slider_id]) ?>
        
         </div>
    </div>
</div>