<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Countries */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
$this->registerJs(
        '$("document").ready(function(){ 
        $("#new-item-'.$model->id.'").on("pjax:end", function() {
          $.pjax.reload({container:"#items-'. $model->id .'"});
        
        });
       
    });'
);
?>

<div class="new-item-form-multiarticle align-right">

    <?php yii\widgets\Pjax::begin(['id' => 'new-item-'.$model->id]) ?>
    <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true, 'class' => 'form-inline']]); ?>


    <?= $form->field($model, 'create_user_id')->hiddenInput(['value' => Yii::$app->user->identity->id])->label(false) ?>
    <?= $form->field($model, 'slider_id')->hiddenInput(['value' => $model->slider_id])->label(false) ?>
    <?= $form->field($model, 'parent_id')->hiddenInput(['value' => $model->id])->label(false) ?>
    <?= $form->field($model, 'type_id')->hiddenInput(['value' => $model->type_id])->label(false) ?>
    <?= $form->field($model, 'categories_id')->hiddenInput(['value' => $model->categories_id])->label(false) ?>

    <?= Html::submitButton(Yii::t('backend_module_slider', 'Create Next'), ['class' => 'btn btn-success']) ?>


    <?php ActiveForm::end(); ?>
    <?php yii\widgets\Pjax::end() ?>
</div>