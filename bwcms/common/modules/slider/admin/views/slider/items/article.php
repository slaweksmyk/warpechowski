<div class="post">
    <?=
    $this->render('article_form', [
        'model' => $model,
        'count' => $count,
        'multiarticle' => $multiarticle
    ])
    ?>
</div>