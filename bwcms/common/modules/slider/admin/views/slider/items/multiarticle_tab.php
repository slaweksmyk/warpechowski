<?php

use common\modules\slider\models\{
    SliderItemsSearch,
    SliderType
};
use yii\widgets\Pjax;
use yii\widgets\ListView;

$searchTab = new SliderItemsSearch();
$dataProviderTab = $searchTab->search(Yii::$app->request->queryParams);

$dataProviderTab->query->where(["parent_id" => $model->id]);
$dataProviderTab->query->orderBy('sort ASC, date_updated DESC');
?>
<!-- Tab panels -->
<div role="tabpanel" class="tab-pane <?= $index == 0 ? 'active' : ''; ?>" id="<?= $model->id ?>">
    <?=
    $this->render('multiarticle_tab_edit', [
        'model' => $model,
    ])
    ?>
    <?php Pjax::begin(['id' => 'items-' . $model->id]) ?>
    <?=
    ListView::widget([
        'dataProvider' => $dataProviderTab,
        // 'itemView' => SliderType::findOne($model->type_id)->code,
        'itemView' => 'article',
        'viewParams' => [
            'count' => $dataProviderTab->getCount(),
            'multiarticle' => true
        ],
    ]);
    ?>
    <?php Pjax::end() ?>




    <div class="new_item-form">
        <form class="form-inline" data-slider="<?= $model->slider_id ?>" data-parent="<?= $model->id ?>" data-type="article">


            <div class="form-group">
                <label for="items-create"><?= Yii::t('backend_module_slider', 'Title') ?></label>
                <input type="text" class="form-control" id="items-create" name="title" placeholder="<?= Yii::t('backend_module_slider', 'Title') ?>">
                <span class="error-block"><?= Yii::t('backend_module_slider', 'Title Required') ?></span>
            </div>

            <span class="btn btn-success" data-action="items-create"><?= Yii::t('backend_module_slider', 'Create Next') ?></span>
        </form>
    </div>

</div>
