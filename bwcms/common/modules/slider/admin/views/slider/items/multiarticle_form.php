<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\files\models\File;
use common\modules\articles\models\Article;
use yii\helpers\ArrayHelper;

?>



<div class="item-form">

    <?php yii\widgets\Pjax::begin(['id' => 'edit_item']) ?>
    <?php $form = ActiveForm::begin([
                'action' => ['slider/update-items/?id=' . $model->id],
                'options' => ['data-pjax' => true]
                    ]
    );
    ?>


    <?= $form->field($model, 'id')->hiddenInput(['value' => $model->id])->label(false) ?>
    <?= $form->field($model, 'thumbnail_id')->hiddenInput(['value' => $model->thumbnail_id])->label(false) ?>
    <?= $form->field($model, 'update_user_id')->hiddenInput(['value' => Yii::$app->user->identity->id])->label(false) ?>

    <div class="row">
        <div class="col-4">

            <div class="row">
                <div class="col-6"> 

                    <img alt="" data-itemfiles="<?= $model->id ?>"  class="img-thumbnail" src="<?= $model->thumbnail_id ? File::getThumbnail($model->thumbnail_id, 140, 140) : '/upload/brak.png'; ?>" data-holder-rendered="true">
                    <div class="dropdown">
                        <button id="dLabel" class="btn btn-primary btn-xs" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?= Yii::t('backend_module_slider', 'Change photo') ?>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">


                            <li  data-toggle="modal" data-action="open-manager" data-target=".modal-open" data-slide="<?= $model->id ?>"> <?= Yii::t('backend_module_slider', 'Add From Manager') ?></li>
                            <li  data-action="image-article" data-slide="<?= $model->id ?>" data-article="<?= $model->data_id ?>">  <?= Yii::t('backend_module_slider', 'Add From Article') ?></li>
                        </ul>
                    </div>
                </div>
                <div class="col-6">
                    <?= $form->field($model, 'thumbnail_title')->textInput(['maxlength' => true]) ?>
                </div>
            </div>


            <div class="row slider-sort">
                <div class="col-4">
                    <?= $form->field($model, 'sort')->textInput(['maxlength' => true]) ?>
                    <span class="label label-primary mainslide " data-action="mainSlide" data-slide="<?= $model->id ?>">Ustaw jako główny</span>
                </div>
            </div>



        </div>
        <div class="col-8">
            <div class="row">
                <div class="col-8">
                    <?= $form->field($model, 'data_id')->dropDownList(ArrayHelper::map(Article::find()->all(), 'id', 'title'), ["prompt" => Yii::t('backend_module_articles', '- select type -'), 'data-key' => $model->id]); ?>

                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'settings_show_title')->checkbox(['checked' => 'checked']) ?>



                    <?= $form->field($model, 'description')->textarea(['class' => 'noTynyMCE form-control', 'rows' => 4]) ?>


                    <?= $form->field($model, 'settings_show_description')->checkbox(['checked' => 'checked']) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-4"><?= $form->field($model, 'active_from')->textInput(["class" => "datepicker form-control"]) ?></div>
                <div class="col-4"><?= $form->field($model, 'active_to')->textInput(["class" => "datepicker form-control"]) ?></div>
                <div class="col-4"><?= $form->field($model, 'is_active')->dropDownList([0 => Yii::t('backend_module_slider', 'inactive'), 1 => Yii::t('backend_module_slider', 'active')]) ?></div>
            </div>
            <div class="row">
                <div class="col-8"> <?= $form->field($model, 'extlink')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'settings_target_blank')->checkbox(['checked' => 'checked']) ?>
                </div>

            </div>

            <div class="row">
                <div class="col-4">
                    <?= $form->field($model, 'text_1')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-4">
                    <?= $form->field($model, 'text_2')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-4">
                    <?= $form->field($model, 'text_3')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-4">
                    <?= $form->field($model, 'text_4')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-4">
                    <?= $form->field($model, 'text_5')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-4">
                    <?= $form->field($model, 'text_6')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-4">
                    <?= $form->field($model, 'text_button')->textInput(['maxlength' => true]) ?>
                </div>

            </div>

        </div>
    </div>










    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'data-action' => 'items-save']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php yii\widgets\Pjax::end() ?>
</div>
<hr>