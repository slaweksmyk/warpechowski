<div class="new_item-form" data-spy="affix" data-offset-top="300" data-offset-bottom="0">
    <form class="form-inline" data-slider="<?= $slider ?>" data-parent="<?= $parent ?>" data-type="<?= $type ?>">
        <div class="form-group">
            <label for="items-create"><?= Yii::t('backend_module_slider', 'Title')?></label>
            <input type="text" class="form-control" id="items-create" name="title" placeholder="<?= Yii::t('backend_module_slider', 'Title')?>">
            <span class="error-block"><?= Yii::t('backend_module_slider', 'Title Required')?></span>
        </div>
        <span class="btn btn-success" data-action="items-create"><?= $type == "multiarticle" ? Yii::t('backend_module_slider', 'Create Overlap') : Yii::t('backend_module_slider', 'Create Slide')?></span>
    </form>
</div>