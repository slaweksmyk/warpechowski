<div class="files-default-index">
    <?php
        use yii\helpers\Html;
        use yii\widgets\ActiveForm;
        use yii\helpers\ArrayHelper;
        use \common\modules\files\models\FileCategory;

        $this->registerJsFile('/bwcms/common/modules/files/admin/assets/js/fileUpload.js');
    ?>
    
    


        <div class="panel-body upload-manager" >
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

                <div class="add-files" >
                    <?= $form->field($model, 'thumbnail_id')->fileInput(['multiple' => true, 'id' => 'add-files-input'])->label('Dodaj plik +'); ?>
                </div>
                <select id="uploadform-category_id" class="form-control" name="UploadForm[category_id]" aria-invalid="false">
<option value="1">Test</option>
</select>

                <div id="imagesAddedBox" class="row" ></div>
                <div id="uploadStatus"></div>

                <?= Html::submitButton(Yii::t('backend_module_slider', 'submit'), ['class' => 'btn btn-success']) ?>

            <?php ActiveForm::end() ?>
        </div>
</div>