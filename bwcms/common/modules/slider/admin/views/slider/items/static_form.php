<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\files\models\File;

/* @var $this yii\web\View */
/* @var $model app\models\Countries */
/* @var $form yii\widgets\ActiveForm */
?>



<div class="item-form">
    <div class="item-sort  <?= $model->sort == 1 ? 'bg-success' : '' ?>">
        <?php if ($model->sort > 1) { ?>
            <span class="sort-arrow glyphicon glyphicon-arrow-up" data-action="sort-change" data-type="up" data-slide="<?= $model->id ?>" data-item="<?= $model->slider_id ?>"></span>
        <?php } ?>
        <span class="badge badge-default"><?= $model->sort ?></span>



        <?php if ($count != $model->sort) { ?>
            <span class="sort-arrow glyphicon glyphicon-arrow-down" data-action="sort-change" data-type="down" data-slide="<?= $model->id ?>" data-item="<?= $model->slider_id ?>"></span>
        <?php } ?>
    </div>
    <?php yii\widgets\Pjax::begin(['id' => 'edit_item-' . $model->id]) ?>
    <?php
    $form = ActiveForm::begin([
                'action' => ['slider/update-items/?id=' . $model->id],
                'options' => ['data-pjax' => true, 'data-id' => $model->id]
                    ]
    );
    ?>


    <?= $form->field($model, 'id')->hiddenInput(['value' => $model->id])->label(false) ?>
    <?= $form->field($model, 'thumbnail_id')->hiddenInput(['id' => "thumbnail_{$model->id}", 'data-id' => $model->id, 'class' => 'slider-thumb', 'value' => $model->thumbnail_id])->label(false) ?>
    <?= $form->field($model, 'update_user_id')->hiddenInput(['value' => Yii::$app->user->identity->id])->label(false) ?>

    <div class="row">
        <div class="col-12"> <div class="text-right color-light">#<?= $model->id ?></div></div>
        <div class="col-4">

            <div class="row">
                <div class="col-6"> 
                    <?=
                    $form->field($model, 'format')->dropDownList(
                            [1 => Yii::t('backend_module_slider', 'Format Photo'), 2 => Yii::t('backend_module_slider', 'Format Video')], [
                        "prompt" => Yii::t('backend_module_slider', '- select format -'),
                        "data-slide" => $model->id
                            ]
                    )
                    ?>
 
                    <img alt="" data-itemfiles="<?= $model->id ?>"  class="img-thumbnail" src="<?= $model->thumbnail_id ? File::getThumbnail($model->thumbnail_id, 140, 140) : '/upload/brak.png'; ?>" data-holder-rendered="true">
                    
                    <div class="dropdown">
                        <button id="dLabel" class="btn btn-primary btn-xs" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?= Yii::t('backend_module_slider', 'Change Photo') ?>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li>
                                <span data-init="file-manager" data-target="#thumbnail_<?= $model->id ?>" data-slide="<?= $model->id ?>"> <?= Yii::t('backend_module_slider', 'Add From Manager') ?></span>
                            </li>  

                        </ul>
                    </div>
                </div>
                <div class="col-6">
                    <?= $form->field($model, 'thumbnail_title')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'settings_thumbnail_show')->checkbox(['checked' => 'checked']) ?>
                </div>
            </div>

            <div class="row slider-sort">
                <div class="col-4">
                    <?= $form->field($model, 'sort')->textInput(['maxlength' => true]) ?>

                    <span class="label label-primary mainslide" data-action="sort-change" data-type="first" data-slide="<?= $model->id ?>" data-item="<?= $model->slider_id ?>">Ustaw jako główny</span>

                </div>
            </div>




        </div>
        <div class="col-8">
            <div class="row">
                <div class="col-8"> <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'settings_show_title')->checkbox(['checked' => 'checked']) ?>
                </div>
                <div class="col-8"></div>
            </div>
            <div class="row">
                <div class="col-4"><?= $form->field($model, 'active_from')->textInput(["class" => "datepicker form-control"]) ?></div>

                <div class="col-4"><?= $form->field($model, 'active_to')->textInput(["class" => "datepicker form-control"]) ?></div>
                <div class="col-4"><?= $form->field($model, 'is_active')->dropDownList([0 => Yii::t('backend_module_slider', 'inactive'), 1 => Yii::t('backend_module_slider', 'active')]) ?></div>
            </div>

            <div class="row">
                <div class="col-8"> <?= $form->field($model, 'extlink')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'settings_target_blank')->checkbox(['checked' => 'checked']) ?>
                </div>

            </div>

            <div class="panel <?= $model->format == 2 ? 'panel-primary' : 'panel-default' ?>" data-action="video">
                <div class="panel-heading"><?= Yii::t('backend_module_slider', 'Video Panel') ?></div>
                <div class="panel-body">
                     <div class="col-3">
                        
                         <iframe data-action="video-iframe" width="150" height="150" src="<?= $model->video ?>" frameborder="0" allowfullscreen></iframe>
                         
                         
                       
                         
                         
                    </div>
                    <div class="col-4"> <?= $form->field($model, 'video')->textInput(['maxlength' => true, 'data-action' => 'video-src',  "data-slide" => $model->id]) ?></div>
                    <div class="col-2"><button type="button" class="btn btn-primary btn-xs" id="launch-atende" data-toggle="modal" data-target="#atendeModal" style="margin-top: 29px;">Wybierz film</button></div>
                    <div class="col-3"> <?=
                        $form->field($model, 'video_type')->dropDownList(
                                [
                            'youtube' => Yii::t('backend_module_slider', 'Video Youtube'),
                            'vimeo' => Yii::t('backend_module_slider', 'Video Vimeo'),
                            'facebook' => Yii::t('backend_module_slider', 'Video Facebook'),
                            'atende' => "Atende",
                                ], ["prompt" => Yii::t('backend_module_slider', '- select video -')]
                        )
                        ?>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading"><?= Yii::t('backend_module_slider', 'Text Panel') ?></div>
                <div class="panel-body">
                    <div class="col-4">
                        <?= $form->field($model, 'text_1')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-4">
                        <?= $form->field($model, 'text_2')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-4">
                        <?= $form->field($model, 'text_3')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-4">
                        <?= $form->field($model, 'text_4')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-4">
                        <?= $form->field($model, 'text_5')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-4">
                        <?= $form->field($model, 'text_6')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-4">
                        <?= $form->field($model, 'text_button')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
            </div>






        </div>
    </div>








    <div class="row">
        <div class="col-6">
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_slider', 'Create') : Yii::t('backend_module_slider', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'data-action' => 'items-save']) ?>
            </div>
        </div>
        <div class="col-6 align-right">
            <div class="form-group">

                <?= Html::button(Yii::t('backend_module_slider', 'Delete'), ['class' => 'btn btn-danger', 'data-target' => '.modal-delete', 'data-toggle' => 'modal', 'data-action' => 'items-delete', 'data-slide' => $model->id, 'data-item' => $model->slider_id]) ?>
            </div>
        </div>
    </div>



    <?php ActiveForm::end(); ?>
    <?php yii\widgets\Pjax::end() ?>
</div>
<hr>