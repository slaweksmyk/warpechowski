<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;
use common\modules\slider\models\{
    SliderType,
    SliderEffect
};
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\slider\models\SliderCategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend_module_slider', 'Slider List'). $model->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-categories-index">




    <p>
        <?= Html::a(Yii::t('backend_module_slider', 'Return List Slider Categories'), ['index'], ['class' => 'btn btn-outline-secondary']) ?>

        <?= Html::a(Yii::t('backend_module_slider', 'Create Slider'), ['create', 'id' => Yii::$app->getRequest()->getQueryParam('id')], ['class' => 'btn btn-success']) ?>


    </p>

    <div class="row">
        <div class="col-12">
            <section class="panel full" >
                <header>
                    <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
                    <form id="per-page">
                        <span>Ilość na stronę:</span>
                        <select name="per-page" class="form-control">
                            <?php
                            if (Yii::$app->request->get('per-page')) {
                                echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>";
                            }
                            ?>
                            <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                        </select>
                    </form>
                </header>
                <div class="panel-body" >
                    <?=
                    GridView::widget([
                        'id' => 'list',
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'tableOptions' => [
                            'class' => 'table table-striped table-bordered sortable'
                        ],
                        'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                            //'id',
                            //'parent_id',
                            [
                                'attribute' => 'title',
                                'contentOptions' => ['style' => 'width: 345px;'],
                            ],
                                [
                                'label' => Yii::t('backend_module_slider', 'Slide Quantity'),
                                'format' => 'raw',
                                'value' => function ($data) {
                                    return "<span class='badge'>".count($data->getSliderItems())."</span>";
                                }
                            ],
                                [
                                'attribute' => 'type_id',
                                'format' => 'raw',
                                'value' => function ($data) {
                                    $type = SliderType::find()->where(["=", "id", $data->type_id])->one();

                                    if (!is_null($type)) {
                                        return Yii::t('backend_module_slider', $type->title);
                                    }
                                }
                            ],
//                                [
//                                'attribute' => 'settings_effect',
//                                'filter'=> ArrayHelper::map(SliderEffect::find()->where(["=", "active", 1])->all(), 'id', 'title'),
//                                'format' => 'raw',
//                                'value' => function ($data) {
//                                    $type = SliderEffect::find()->where(["=", "id", $data->settings_effect])->one();
//
//                                    if (!is_null($type)) {
//                                        return $type->title;
//                                    }
//                                }
//                            ],
                            [
                                'attribute' => 'date_created',
                                'filter' => false
                            ],
                                [
                                'attribute' => "is_active",
                                'filter' => [1 => "Aktywny", 0 => "Nieaktywny"],
                                'label' => "Status",
                                'format' => 'raw',
                                'value' => function ($data) {
                                    return ($data->is_active) ? Yii::t('backend', 'active') : Yii::t('backend', 'inactive');
                                }
                            ],
                            //'description:ntext',
                            //'sort',
                            // 'is_active',
                            // 'create_user_id',
                            // 'update_user_id',
                            // 'date_created',
                            // 'date_updated',
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update} {delete}',
                                'contentOptions' => ['style' => 'width: 100px;'],
                                'buttons' => [
                                    'update' => function ($url) {
                                   
                                         return Html::a(
                                                '<span class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></span>', $url, [
                                            'title' => Yii::t('backend_module_slider', 'Update Slider Settings Tooltip'),
                                            'data-toggle' => "tooltip",
                                            'data-placement' => "bottom",
                                                    'data-pjax' => '0',
                                                ]
                                );
                                    },
                                    'view' => function ($url) {
                                      
                                        
                                         return Html::a(
                                                '<span class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-list-alt"></span></span>', $url, [
                                               'title' => Yii::t('backend_module_slider', 'Slider List'),
                                            'data-toggle' => "tooltip",
                                            'data-placement' => "bottom"
                                                ]
                                );
                                         
                                         
                                    },
                                             'delete' => function ($url) {
                                return Html::a(
                                                '<span class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></span>', $url, [
                                            'title' => Yii::t('backend', 'delete'),
                                            'data-toggle' => "tooltip",
                                            'data-placement' => "bottom",
                                            'data-pjax' => 0,
                                            'data-confirm' => 'Czy na pewno usunąć ten element?',
                                            'data-method' => 'post',
                                                ]
                                );
                            },
                                ],
                            ],
                        ],
                    ]);
                    ?>
                </div>


            </section>
        </div>
    </div>
</div>
