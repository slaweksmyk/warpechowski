<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\modules\slider\models\{
    SliderCategories,
    SliderEffect,
    SliderType
};
use yii\widgets\Pjax;
use yii\widgets\ListView;

$category = SliderCategories::findOne($model->categories_id);

$this->title = Yii::t('backend_module_slider', 'Update Slider Settings') . $model->title;

$this->registerJsFile('/bwcms/common/modules/slider/admin/assets/js/slider.js');
$this->registerCssFile('/bwcms/common/modules/slider/admin/assets/css/slider.css');
?>
<div class="slider-slider-update">
     
    <p>
        <?= Html::a(Yii::t('backend_module_slider', 'Return Slider List'), ["slider-list", 'id' => $model->categories_id], ['class' => 'btn btn-outline-secondary']) ?>
    </p>

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend_module_slider', 'Items List') ?>
        </header>
        <div class="panel-body" >

            <!-- Render create form -->    
            <?= $this->render('items/_form', [
                'model' => $modelItems,
                'slider' => $model->id,
                'parent' => 0,
                'type'  => SliderType::findOne($model->type_id)->code
            ]) ?>
            <hr>
            <?php if (SliderType::findOne($model->type_id)->code == 'multiarticle') { ?>

                <!-- Nav tabs -->
                <?php Pjax::begin(['id' => 'multiarticle']) ?>
                <?= ListView::widget([
                    'dataProvider' => $parentItem,
                    'itemView' => 'items/multiarticle_nav',
                    'summary' => false,
                    'itemOptions' => [
                        'tag' => false
                    ],
                    'options' => [
                        'class' => 'nav nav-tabs',
                        'role' => 'tablist',
                        'id' => false,
                        'tag' => 'ul'
                    ]
                ]); ?>
                <?php Pjax::end() ?>

                <?php Pjax::begin(['id' => 'multiarticleItems']) ?>
                <?= ListView::widget([
                    'dataProvider' => $parentItem,
                    'itemView' => 'items/multiarticle_tab',
                    'summary' => false,
                    'itemOptions' => [
                        'tag' => false
                    ],
                    'options' => [
                        'class' => 'tab-content',
                        'id' => false,
                        'tag' => 'div'
                    ]
                ]); ?>
                <?php Pjax::end() ?>
            <?php } else { ?>   
                <?php Pjax::begin(['id' => 'items-'.$model->id]) ?>
                <?=
                ListView::widget([
                    'dataProvider' => $dataProvider,
                    'viewParams' => [
                        'count' => $dataProvider->getCount(),
                         'multiarticle' => false
                    ],
                    'itemView' => 'items/' . SliderType::findOne($model->type_id)->code,
                ]);
                ?>
                <?php Pjax::end() ?>
            <?php } ?>   
        </div>
    </section>
    <?php $form = ActiveForm::begin(); ?>
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
        </header>
        <div class="panel-body" >
            <?= $form->field($model, 'update_user_id')->hiddenInput(['value' => Yii::$app->user->identity->id])->label(false) ?>
            <div class="row">
                <div class="col-12">
                    <p class="alert alert-info">
                        <?= Yii::t('backend_module_slider', 'Update Slider Categories') ?>:  
                        <b><?= $category->title ?></b>
                        o typie: 
                        <b><?= Yii::t('backend_module_slider', SliderCategories::getType($category->type_id)->title) ?></b>
                    </p>
                </div>
                <div class="col-4"> <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?></div>
                <div class="col-2"><?= $form->field($model, 'is_active')->dropDownList([0 => Yii::t('backend_module_articles', 'inactive'), 1 => Yii::t('backend_module_articles', 'active')]) ?></div>
                <div class="col-2"><?= $form->field($model, 'active_from')->textInput(["class" => "datepicker form-control"]) ?></div>
                <div class="col-2"><?= $form->field($model, 'active_to')->textInput(["class" => "datepicker form-control"]) ?></div>
                <div class="col-2"><?= $form->field($model, 'settings_show_text')->dropDownList([0 => Yii::t('backend_module_slider', 'no'), 1 => Yii::t('backend_module_slider', 'yes')]) ?></div>
            </div>

            <div class="row">    
                <div class="col-2"><?= $form->field($model, 'settings_effect')->dropDownList(ArrayHelper::map(SliderEffect::find()->where(['type' => 'effect'])->all(), 'id', 'title'), ["prompt" => Yii::t('backend_module_slider', '- select effect -')]); ?></div>
                <div class="col-2"><?= $form->field($model, 'settings_effect_time')->dropDownList(ArrayHelper::map(SliderEffect::find()->where(['type' => 'time'])->all(), 'id', 'title'), ["prompt" => Yii::t('backend_module_slider', '- select effect time -')]); ?></div>
                <div class="col-2"><?= $form->field($model, 'settings_arrow')->dropDownList([0 => Yii::t('backend_module_slider', 'no'), 1 => Yii::t('backend_module_slider', 'yes')]) ?></div>
                <div class="col-2"><?= $form->field($model, 'settings_dots')->dropDownList([0 => Yii::t('backend_module_slider', 'no'), 1 => Yii::t('backend_module_slider', 'yes')]) ?></div>
                <div class="col-2"> <?= $form->field($model, 'settings_quantity_slide')->textInput(['maxlength' => true]) ?></div>
                <div class="col-2"> <?= $form->field($model, 'settings_quantity_slide_max')->textInput(['maxlength' => true]) ?></div>
            </div>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_slider', 'Create') : Yii::t('backend_module_slider', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    </section>

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/>  <?= Yii::t('backend_module_slider', 'Update Slider Show Settings') ?>
        </header>
        <div class="panel-body" >
            <?= $form->field($model, 'update_user_id')->hiddenInput(['value' => Yii::$app->user->identity->id])->label(false) ?>
            
            <div class="row">    
                <div class="col-2">  </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_slider', 'Create') : Yii::t('backend_module_slider', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    </section>
    <?php ActiveForm::end(); ?>
</div>

<div class="modal fade modal-delete" tabindex="-1" role="dialog" aria-labelledby="deleteModal">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4>Czy na pewno chcesz usunąć?</h4>
                </div>
                <div class="panel-footer">
                    <div class="row">    
                        <div class="col-6"><button class="btn btn-danger" data-action="item-delete-confirm" data-slide="">Tak</button></div>
                        <div class="col-6 align-right"><button class="btn btn-outline-secondary" data-dismiss="modal" aria-label="Close">Anuluj</button></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade modal-delete-slide" tabindex="-1" role="dialog" aria-labelledby="deleteSlideModal">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4>Czy na pewno chcesz usunąć zakładkę? Wszystkie slide dodsne to tej zakładki zostana usunięte.</h4>
                </div>
                <div class="panel-footer">
                    <div class="row">    
                        <div class="col-6"><button class="btn btn-danger" data-action="item-delete-confirm-slide" data-slide="">Tak</button></div>
                        <div class="col-6 align-right"><button class="btn btn-outline-secondary" data-dismiss="modal" aria-label="Close">Anuluj</button></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>