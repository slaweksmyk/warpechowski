<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\modules\slider\models\SliderType;
use common\components\Common;
/* @var $this yii\web\View */
/* @var $model common\modules\slider\models\SliderCategories */

$this->title = Yii::t('backend_module_slider', 'Create Slider Categories');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend_module_slider', 'Slider Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-categories-create">


    <p>
        <?= Html::a(Yii::t('backend_module_slider', 'Return List Slider Categories'), ["index"], ['class' => 'btn btn-outline-secondary']) ?>
    </p>
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
        </header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'create_user_id')->hiddenInput(['value' => Yii::$app->user->identity->id])->label(false) ?>
            <?= $form->field($model, 'sort')->hiddenInput(['value' => 0])->label(false) ?>



            <div class="row">
                <div class="col-4"> <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?></div>
                <div class="col-4"><?= $form->field($model, 'is_active')->dropDownList([1 => Yii::t('backend_module_slider', 'active'),0 => Yii::t('backend_module_slider', 'inactive')]) ?></div>
                <div class="col-4">
                    <?php
                    echo $form->field($model, 'type_id')->dropDownList(Common::mapTranslate('backend_module_slider',SliderType::find()->all(), 'id', 'title'), ["prompt" => Yii::t('backend_module_slider', '- select type -')]);
                    ?>
                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_slider', 'Create') : Yii::t('backend_module_slider', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </section>


</div>
