$(document).ready(function () {

    $("body").on("change", ".slider-thumb", function () {
        var id = $(this).data("id");

        $("form[data-id='" + id + "']").submit();
    });


    $("#launch-atende").click(function () {
        callAtende();
    });

    $("body").on("click", "#atende-tree div", function () {
        var name = $(this).data("name");
        var file = $(this).data("file");

        if (typeof name != 'undefined') {
            callAtende(name);
        } else if (typeof file != 'undefined') {
            $("#slideritems-video").val(file);
            $("[data-dismiss='modal']").trigger("click");
        }
    });

    function callAtende(name) {
        $.post($("#homeDir").text()+"articles/article/get-atende/", {name: name})
                .done(function (data) {
                    $("#atende-tree").html(data);
                });
    }

});



$(document).on('ready pjax:success', function () {

$(".ajax-load-articles-single").select2({
        width: "100%",

        placeholder: "Wyszukaj i dodaj artykuł",
        ajax: {
            url: $("#homeDir").text()+"articles/article/ajaxgetarticles/",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    query: params.term,
                };
            },
            processResults: function (data, params) {
                return {
                    results: data.items
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        minimumInputLength: 1
    });
    //Button create slide
    $("[data-action='items-save']").click(function (event) {

        var btn = $(this);
        var text = btn.html();
        //btn.html('Zmieniam..').attr("disabled", true);

        btn.parent().parent().css('opacity', '0.6');
        setTimeout(function () {
            //btn.html(text).attr("disabled", false);
            btn.parent().parent().css('opacity', '1');
        }, 1000);
    });



    $('.view').click(function (e) {
        e.preventDefault();
        $('#pModal').modal('show')
                .find('.modal-content')
                .load($(this).attr('href'));
    });
    // Open modal manager files
    $("[data-action='open-manager']").on("click", function () {
        var id = $(this).data("slide");
        $("#pModal").data("slide", id);
    });

    //When cliced on image in modal file manager
    $("[data-action='items-files']").click(function () {


        var img = $(this);
        var srcImg = img.attr('src');
        var idImg = img.data('file');


        var id = $(".modal").data('slide');

        var array = {

            id: id,
            idImg: idImg,
            srcImg: srcImg


        };

        // Change src value on main image on slide
        $("[data-itemfiles='" + id + "']").attr('src', srcImg);
        //Change value in input thumbnail_id
        $("[data-key='" + id + "']").find("#slideritems-thumbnail_id").val(idImg);
        // Hidden modal
        $('#pModal').modal('hide');
    });
//
    //When cliced on option - from article in dropdown menu
    $("[data-action='image-article']").click(function () {

        var id = $(this).data("slide");
        var idArticle = $("select[data-key='" + id + "']").find(":selected").val();
        // console.log(idArticle);
        // Get article data with one record - thumnail_id
        $.get($("#homeDir").text()+"slider/slider/get-article/", {id: idArticle, record: 'thumbnail_id'}).done(function (data) {

            // Get name of file from thumnail_id
            $.get($("#homeDir").text()+"slider/slider/get-file/", {id: data}).done(function (image) {

                $("[data-itemfiles='" + id + "']").attr('src', '/upload/' + image);
                $("[data-key='" + id + "']").find('#thumbnail_' + id).val(data);

            });


        });

    });


    $("[data-action='get-data']").click(function () {

        var get = $(this).data("get");
        var key = $(this).data("slide");
        var row = $(this).data("row");
        var distInput = $(this).data("dist");

        var idArticle = $("select[data-key='" + key + "']").find(":selected").val();

        // Get article data
        $.get($("#homeDir").text()+"slider/slider/get-article/", {id: idArticle, record: get}).done(function (data) {

            if (row == "input") {
                var input = $("[data-key='" + key + "'] #slideritems-" + distInput);
                if (input.val() == data) {
                    //console.log('takie samo') 
                } else {
                    input.val(data);
                }


            }

            if (row == "textarea") {

                valueDesc = jQuery(data).text();

                $("[data-key='" + key + "'] #slideritems-" + distInput).text(valueDesc);
            }



        });

    });

    $("[data-action='items-create']").click(function () {

        var form = $(this).parent();
        var slider = form.data("slider");
        var parent = form.data("parent");
        var title = form.find("input[name=title]");
        var titleCopy = title.val();
        var type = form.data("type");

        // Hidden error block
        form.find(".error-block").hide()


        if (title.val().trim().length > 0) {
            title.val('');
            // Create Article
            $.get($("#homeDir").text()+"slider/slider/create-slide/", {id: slider, parent: parent, title: titleCopy}).done(function (data) {

                if (type === "article" || type === 'static') {

                    $.pjax.reload({container: "#items-" + (parent > 0 ? parent : slider)});
                    //console.log('fdf')
                }

                if (type === "multiarticle") {
                    $.pjax.reload({container: "#multiarticle"});
                    setTimeout(function () {
                        $.pjax.reload({container: "#multiarticleItems"});
                    }, 500);


                }

                title.val('');
            });

        } else {


            form.find(".error-block").show();
            // reset title val / white spaces
            title.val('');
        }


    });

    // Get data from select article
//    $("select[name='SliderItems[data_id]']").change(function () {
//
//        var key = $(this).data("key");
//
//        var value = $(this).find(":selected");
//          
//          // get short description and put to textarea description
//        $.get("/__cms/slider/slider/get-article/", {id: value.val(), record: 'short_description'})
//                .done(function (data) {
//                    valueDesc = jQuery(data).text();
//                    $("[data-key='" + key + "'] #slideritems-description").text(valueDesc);
//                });
//       // Put article title in input title
//       //  $("[data-key='" + key + "'] #slideritems-title").val(value.text());
//
//        // Put article short description in textarea
//       // $("[data-key='" + key + "'] [data-action='image-article']").attr("data-article", value.val());
//
//
//
//    });

    $("[data-action='mainSlide']").click(function () {
        var key = $(this).data('slide');
        $("[data-key='" + key + "'] #slideritems-sort").val(1);
    });

    $('#multiarticle a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    })



    /*
     * -------------------------------------------
     */


    // Open modal manager files
    $("[data-action='items-delete']").on("click", function (e) {

        var id = $(this).data("slide");
        var idItem = $(this).data("item");

        $("[data-action='item-delete-confirm']").attr('data-slide', id).attr('data-item', idItem);

    });

    $("[data-action='item-delete-confirm']").on("click", function (e) {

        var id = $(this).attr("data-slide");
        var idItem = $(this).attr("data-item");

        console.log(idItem);
        $.get($("#homeDir").text()+"slider/slider/items-delete/", {id: id})
                .done(function (data) {
                    $('.modal-delete').modal('hide');
                    $("[data-action='item-delete-confirm']").removeAttr('data-slide');
                    $.pjax.reload({container: "#items-" + idItem});
                });
    });

    /*
     * -------------------------------------------
     */
    // Open modal manager files
    $("[data-action='items-delete-slide']").on("click", function (e) {

        var id = $(this).data("slide");
        var idItem = $(this).data("item");

        $("[data-action='item-delete-confirm-slide']").attr('data-slide', id).attr('data-item', idItem);

    });

    $("[data-action='item-delete-confirm-slide']").on("click", function (e) {

        var id = $(this).attr("data-slide");
        var idItem = $(this).attr("data-item");

        console.log(idItem);
        $.get($("#homeDir").text()+"slider/slider/items-delete-slide/", {id: id})
                .done(function (data) {
                    $('.modal-delete-slide').modal('hide');
                    $("[data-action='item-delete-confirm-slide']").removeAttr('data-slide');
                    $.pjax.reload({container: "#multiarticle"});
                    $.pjax.reload({container: "#multiarticleItems"});
                });
    });


    /*
     * -------------------------------------------
     */




    $("[data-action='sort-change']").on("click", function (e) {
        e.preventDefault();
        var id = $(this).data("slide");
        var type = $(this).data("type");
        var idItem = $(this).attr("data-item");

        $(this).removeClass('label-primary ').addClass('label-success');
        // console.log(idItem);

        // console.log(id + ' ' + type);
        $.get($("#homeDir").text()+"slider/slider/sort-change/", {id: id, type: type})
                .done(function (data) {
                    // console.log(data);
                    $.pjax.reload({container: "#items-" + idItem});

                });
    });


    // Get data from select article
    $("select[name='SliderItems[format]']").change(function () {



        var value = $(this).find(":selected");
        var key = $(this).data('slide');

        if (value.val() == 2) {
            $("[data-key='" + key + "'] [data-action='video']").removeClass('panel-default').addClass('panel-primary');


            $("[data-key='" + key + "'] input[name='SliderItems[video]']").focus();
        }
        if (value.val() == 1) {
            $('[data-action="video"]').removeClass('panel-primary').addClass('panel-default');
        }
        // console.log(value.val());



    });



    $("[data-action='video-src']").change(function () {
        var src = $(this).val();
        var key = $(this).data('slide');
        $("[data-key='" + key + "'] [data-action='video-iframe']").attr('src', src);
    });
});