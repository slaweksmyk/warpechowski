<?php

namespace common\modules\slider\admin;

use Yii;
/**
 * Shop module definition class
 */
class Slider extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\slider\admin\controllers';
    public $defaultRoute = 'slider';

    /**
     * @inheritdoc
     */
    public function init()
    {
        $aUrlParts = explode("/", str_replace(\Yii::$app->request->baseUrl, "", Yii::$app->getRequest()->url));
        $this->defaultRoute = current($aUrlParts);

        parent::init();
    }
}
