<?php

namespace common\modules\slider\admin\controllers;

use Yii;
use common\modules\slider\models\{
    SliderCategories,
    SliderCategoriesSearch,
    Slider,
    SliderSearch,
    SliderItems,
    SliderItemsSearch,
    SliderType
};
use common\modules\articles\models\Article;
use common\modules\files\models\FileSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SliderController implements the CRUD actions for SliderCategories model.
 */
class SliderController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SliderCategories models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new SliderCategoriesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->get('per-page')) {
            $dataProvider->setPagination(['pageSize' => Yii::$app->request->get('per-page')]);
        }

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSliderList($id) {
        $searchModel = new SliderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where(["categories_id" => $id]);

        if (Yii::$app->request->get('per-page')) {
            $dataProvider->setPagination(['pageSize' => Yii::$app->request->get('per-page')]);
        }


        $model = SliderCategoriesSearch::findOne(["id" => $id]);

        return $this->render("slider_list", [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'model' => $model
        ]);
    }

    /**
     * Displays a single SliderCategories model.
     * @param integer $id
     * @return mixed
     */
    public function actionCategoriesView($id) {
        return $this->render('categories_view', [
                    'model' => $this->findCategoriesModel($id),
        ]);
    }

    /**
     * Creates a new SliderCategories model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCategoriesCreate() {
        $model = new SliderCategories();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->date_created = Yii::$app->formatter->asTime('now', 'yyyy-MM-dd HH:mm:ss');

            $model->save();
            return $this->redirect(['categories-update', 'id' => $model->id]);
        } else {
            return $this->render('categories_create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new Slider model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Slider();




        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->date_created = Yii::$app->formatter->asTime('now', 'yyyy-MM-dd HH:mm:ss');

            $model->save();
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('slider_create', [
                        'model' => $model,
            ]);
        }
    }

    public function actionCreateSlide(int $id, int $parent = null, string $title) {

        $model = new SliderItems();
        $model->slider_id = $id;
        $model->title = trim(htmlentities(strip_tags($title)));

        $model->settings_thumbnail_show = 1;
        $model->settings_show_title = 1;
        $model->settings_show_description = 1;
        $model->is_active = 1;
        $model->date_created = Yii::$app->formatter->asTime('now', 'yyyy-MM-dd HH:mm:ss');
        $model->create_user_id = Yii::$app->user->identity->id;

        if ($parent > 0) {
            $model->parent_id = $parent;
            $lastSort = SliderItems::find()->where(['slider_id' => $id, "parent_id" => $parent])->orderBy(['sort' => SORT_DESC])->one();
        } else {
            $lastSort = SliderItems::find()->where(['slider_id' => $id])->orderBy(['sort' => SORT_DESC])->one();
        }

        if ($lastSort) {
            // $lastSort = $lastSort->sort;
            $model->sort = $lastSort->sort + 1;
        } else {
            $model->sort = 1;
        }
        $model->save();

        return true;
    }

    /**
     * Updates an existing SliderCategories model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionCategoriesUpdate($id) {
        $model = $this->findCategoriesModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['categories-update', 'id' => $model->id]);
        } else {
            return $this->render('categories_update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SliderCategories model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {





        // Slider models
        $model = $this->findModel($id);

        $modelItems = new SliderItems();


        if ($modelItems->load(Yii::$app->request->post()) && $modelItems->save()) {

            if ($modelItems->parent_id) {
                $lastSort = SliderItems::find()->where(['slider_id' => $modelItems->slider_id, "parent_id" => $modelItems->parent_id])->orderBy(['sort' => SORT_DESC])->one();
            } else {
                $lastSort = SliderItems::find()->where(['slider_id' => $modelItems->slider_id])->orderBy(['sort' => SORT_DESC])->one();
            }



            if ($lastSort->sort) {
                // $lastSort = $lastSort->sort;
                $modelItems->sort = $lastSort->sort + 1;
            } else {
                $modelItems->sort = 1;
            }
            //$modelItems->save();



            $modelItems = new SliderItems(); //reset model
        }

        // SLider Items models - with Pjax - WHERE SLIDER ID
        $searchModel = new SliderItemsSearch();
        // Slider parent
        $searchModel->slider_id = $id;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort = [
            'defaultOrder' => [
                'sort' => SORT_ASC,
            ]
        ];


        // Files from manager

        $searchModelFiles = new FileSearch();
        $dataProviderFiles = $searchModelFiles->search(Yii::$app->request->queryParams);


        // Slider multiarticle without parents
        if (SliderType::findOne($model->type_id)->code == 'multiarticle') {



            $searchModelParent = new SliderItemsSearch();
            $dataProviderParent = $searchModelParent->search(Yii::$app->request->queryParams);
            $dataProviderParent->query->where(["slider_id" => $id]);
            $dataProviderParent->query->andWhere(["parent_id" => 0]);
        } else {
            $dataProviderParent = false;
        }




        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort = [
            'defaultOrder' => [
                'sort' => SORT_ASC,
            ]
        ];


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('slider_update', [
                        'model' => $model,
                        'modelItems' => $modelItems,
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                        'searchModelFiles' => $searchModelFiles,
                        'dataProviderFiles' => $dataProviderFiles,
                        'parentItem' => $dataProviderParent,
            ]);
        }
    }

    public function actionSortChange($id, $type) {

        if ($type == "down") {

            $model = SliderItems::findOne($id);

            // Actual sort position
            $sort = $model->sort;
            // Slider parent
            $slider_id = $model->slider_id;
            $parent_id = $model->parent_id;



            // Next Slider
            $modelNeighbor = SliderItems::findOne(['sort' => $sort + 1, 'slider_id' => $slider_id, 'parent_id' => $parent_id]);
            $modelNeighbor->sort = $modelNeighbor->sort - 1;
            $modelNeighbor->update();

            // Next Slider
            $model->sort = $model->sort + 1;
            $model->update();

            return "success down";
        }

        if ($type == "up") {

            $model = SliderItems::findOne($id);

            // Actual sort position
            $sort = $model->sort; //2
            $slider_id = $model->slider_id;
            $parent_id = $model->parent_id;



            // Next Slider
            $modelNeighbor = SliderItems::findOne(['sort' => $sort - 1, 'slider_id' => $slider_id, 'parent_id' => $parent_id]); //1
            $modelNeighbor->sort = $modelNeighbor->sort + 1; //2
            $modelNeighbor->update(); // 2
            // Next Slider
            $model->sort = $model->sort - 1; //1
            $model->update(); //1


            return "old-sort:" . $sort;
        }

        if ($type == "first") {

            $model = $this->findItemsModel($id);
            $model->date_updated = Yii::$app->formatter->asTime('now', 'yyyy-MM-dd HH:mm:ss');

            //We set sort position on Zero
            $model->sort = 0;

            $model->save();
            ///////////////////////////////////////////////
            //
            
            // We check type of slider
            if ($model->parent_id) {
                // Multiarticle
                $models = SliderItems::find()->where(['slider_id' => $model->slider_id, 'parent_id' => $model->parent_id])->orderBy('sort ASC, date_updated DESC')->all();
            } else {
                // Single
                $models = SliderItems::find()->where(['slider_id' => $model->slider_id])->orderBy('sort ASC, date_updated DESC')->all();
            }

            foreach ($models as $key => $model) {
                $model->sort = $key + 1;
                $model->update(false); // skipping validation as no user input is involved
            }

//            // Actual sort position
//            $sort = $model->sort; //2
//            $slider_id = $model->slider_id;
//            $parent_id = $model->parent_id;
//
//
//
//            // Next Slider
//            $modelNeighbor = SliderItems::findOne(['sort' => $sort - 1, 'slider_id' => $slider_id, 'parent_id' => $parent_id]); //1
//            $modelNeighbor->sort = $modelNeighbor->sort + 1; //2
//            $modelNeighbor->update(); // 2
//            // Next Slider
//            $model->sort = $model->sort - 1; //1
//            $model->update(); //1


            return true;
        }
        return false;
    }

    public function actionUpdateItems($id) {
        if (Yii::$app->request->isPjax) {
            $modelItems = $this->findItemsModel($id);

            if ($modelItems->load(Yii::$app->request->post()) && $modelItems->save()) {
                $modelItems->date_updated = Yii::$app->formatter->asTime('now', 'yyyy-MM-dd HH:mm:ss');

                if ($modelItems->sort == 1) {
                    $modelItems->sort = 0;
                }
                $modelItems->save();

                if ($modelItems->parent_id) {
                    $models = SliderItems::find()->where(['slider_id' => $modelItems->slider_id, 'parent_id' => $modelItems->parent_id])->orderBy('sort ASC, date_updated DESC')->all();
                } else {
                    $models = SliderItems::find()->where(['slider_id' => $modelItems->slider_id])->orderBy('sort ASC, date_updated DESC')->all();
                }

                foreach ($models as $key => $model) {
                    $model->sort = $key + 1;
                    $model->update(false); // skipping validation as no user input is involved
                }

                if ($modelItems->sort == 1) {
                    SliderItems::updateAll(['status' => status + 1], 'slider_id = {$modelItems->slider_id} AND id != {$modelItems->id}');
                } else {
                    
                }

                $modelItems = new SliderItems(); //reset model
            }



            // SLider Items models - with Pjax - WHERE SLIDER ID
            $searchModel = new SliderItemsSearch();
            // Slider parent
            $searchModel->slider_id = $modelItems->id;


            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->sort = [
                'defaultOrder' => [
                    'sort' => SORT_ASC,
                ]
            ];




            /* return $this->render('slider_update', [
              'dataProvider' => $dataProvider,
              ]); */
        } else {

            return $this->redirect(['update', 'id' => $this->findItemsModel($id)->slider_id]);
        }
    }

    /**
     * Deletes an existing SliderCategories model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionCategoriesDelete($id) {
        $this->findCategoriesModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['slider-list']);
    }

    public function actionItemsDelete($id) {
        $this->findItemsModel($id)->delete();

        return true;
    }

    public function actionItemsDeleteSlide($id) {

        //SliderItems::find()->where(['id' => $id])->one()->delete();

        if (($model = SliderItems::findOne($id)) !== null) {
            $model->delete();


            $models = SliderItems::find()->where(['parent_id' => $id])->all();

            if ($models) {
                foreach ($models as $model) {
                    $model->delete();
                }
            }
        } else {
            // throw new NotFoundHttpException('The requested page does not exist.');
        }





        return true;
    }

    public function actionGetArticle(int $id, string $record = 'id') {

        $article = Article::findOne($id);

        if ($article) {
            $return = $article->$record;
        } else {
            $return = false;
        }
        return $return;
    }

    public function actionGetFile(int $id) {

        $file = \common\modules\files\models\File::findOne($id);


        if ($file) {
            $return = $file->filename;
        } else {
            $return = false;
        }
        return $return;
    }

    /**
     * Finds the SliderCategories model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SliderCategories the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findCategoriesModel($id) {
        if (($model = SliderCategories::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModel($id) {
        if (($model = Slider::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findItemsModel($id) {
        if (($model = SliderItems::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
