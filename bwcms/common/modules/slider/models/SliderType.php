<?php

namespace common\modules\slider\models;

use Yii;

/**
 * This is the model class for table "xmod_slider_type".
 *
 * @property integer $id
 * @property string $title
 * @property string $code
 */
class SliderType extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_slider_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'code'], 'required'],
            [['title', 'code'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_slider', 'ID'),
            'title' => Yii::t('backend_module_slider', 'Title'),
            'code' => Yii::t('backend_module_slider', 'Code'),
        ];
    }
    public function valueLabels()
    {   
        /*
         * code
         */
        return [
            'static' => Yii::t('backend_module_slider', 'Type Article'),
            'article' => Yii::t('backend_module_slider', 'Type Multi Article'),
            'multiarticle' => Yii::t('backend_module_slider', 'Type Static'),
        ];
    }
    /**
     * @inheritdoc
     * @return SliderTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SliderTypeQuery(get_called_class());
    }
}
