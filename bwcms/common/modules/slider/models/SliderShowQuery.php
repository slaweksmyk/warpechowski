<?php

namespace common\modules\slider\models;

/**
 * This is the ActiveQuery class for [[SliderShow]].
 *
 * @see SliderShow
 */
class SliderShowQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return SliderShow[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SliderShow|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
