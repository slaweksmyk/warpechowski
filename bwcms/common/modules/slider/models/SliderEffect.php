<?php

namespace common\modules\slider\models;

use Yii;

/**
 * This is the model class for table "xmod_slider_effect".
 *
 * @property integer $id
 * @property string $title
 *
 * @property XmodSlider[] $xmodSliders
 */
class SliderEffect extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_slider_effect}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_slider', 'ID'),
            'title' => Yii::t('backend_module_slider', 'Title'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSliders()
    {
        return $this->hasMany(Slider::className(), ['settings_effect' => 'id']);
    }
 

    /**
     * @inheritdoc
     * @return SliderEffectQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SliderEffectQuery(get_called_class());
    }
}
