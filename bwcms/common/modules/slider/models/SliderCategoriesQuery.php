<?php

namespace common\modules\slider\models;

/**
 * This is the ActiveQuery class for [[SliderCategories]].
 *
 * @see SliderCategories
 */
class SliderCategoriesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return SliderCategories[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SliderCategories|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
