<?php

namespace common\modules\slider\models;

/**
 * This is the ActiveQuery class for [[SliderType]].
 *
 * @see SliderType
 */
class SliderTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return SliderType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SliderType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
