<?php

namespace common\modules\slider\models;

/**
 * This is the ActiveQuery class for [[SliderItems]].
 *
 * @see SliderItems
 */
class SliderItemsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return SliderItems[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SliderItems|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
