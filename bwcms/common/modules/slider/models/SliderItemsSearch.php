<?php

namespace common\modules\slider\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\slider\models\SliderItems;

/**
 * SliderItemsSearch represents the model behind the search form about `common\modules\slider\models\SliderItems`.
 */
class SliderItemsSearch extends SliderItems
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'slider_id', 'categories_id', 'thumbnail_id', 'create_user_id', 'update_user_id', 'is_active', 'sort', 'settings_show_title', 'settings_show_description', 'settings_target_blank','format'], 'integer'],
            [['title', 'description', 'extlink', 'video', 'thumbnail_title', 'active_from', 'active_to', 'date_created', 'date_updated', 'date_display', 'text_1', 'text_2', 'text_3', 'text_4', 'text_5', 'text_6', 'text_button'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SliderItems::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'slider_id' => $this->slider_id,
            'categories_id' => $this->categories_id,
            'thumbnail_id' => $this->thumbnail_id,
            'create_user_id' => $this->create_user_id,
            'update_user_id' => $this->update_user_id,
            'is_active' => $this->is_active,
            'active_from' => $this->active_from,
            'active_to' => $this->active_to,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
            'date_display' => $this->date_display,
            'sort' => $this->sort,
            'settings_show_title' => $this->settings_show_title,
            'settings_show_description' => $this->settings_show_description,
            'settings_target_blank' => $this->settings_target_blank,
            'format' => $this->format,
           
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'extlink', $this->extlink])
            ->andFilterWhere(['like', 'video', $this->video])
            ->andFilterWhere(['like', 'video_type', $this->video_type])
            ->andFilterWhere(['like', 'thumbnail_title', $this->thumbnail_title])
            ->andFilterWhere(['like', 'text_1', $this->text_1])
            ->andFilterWhere(['like', 'text_2', $this->text_2])
            ->andFilterWhere(['like', 'text_3', $this->text_3])
            ->andFilterWhere(['like', 'text_4', $this->text_4])
            ->andFilterWhere(['like', 'text_5', $this->text_5])
            ->andFilterWhere(['like', 'text_6', $this->text_6])
            ->andFilterWhere(['like', 'text_button', $this->text_button]);

        return $dataProvider;
    }
}
