<?php

namespace common\modules\slider\models;

use Yii;
use common\modules\files\models\File;
use common\modules\users\models\User;

/**
 * This is the model class for table "xmod_slider_items".
 *
 * @property integer $id
 * @property integer $slider_id
 * @property integer $categories_id
 * @property string $title
 * @property string $description
 * @property string $extlink
 * @property string $video
 * @property integer $thumbnail_id
 * @property string $thumbnail_title
 * @property integer $create_user_id
 * @property integer $update_user_id
 * @property integer $is_active
 * @property string $active_from
 * @property string $active_to
 * @property string $date_created
 * @property string $date_updated
 * @property string $date_display
 * @property integer $sort
 * @property string $text_1
 * @property string $text_2
 * @property string $text_3
 * @property string $text_4
 * @property string $text_5
 * @property string $text_6
 * @property string $text_button
 * @property integer $settings_show_title
 * @property integer $settings_show_description
 * @property integer $settings_target_blank
 * @property integer $settings_thumbnail_show
 * @property integer $type_id
 * @property integer $data_id
 * @property integer $format
 * 
 * @property XmodSliderCategories $categories
 * @property User $createUser
 * @property XmodSlider $slider
 * @property Files $thumbnail
 * @property User $updateUser
 */
class SliderItems extends \common\hooks\yii2\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%xmod_slider_items}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['slider_id', 'categories_id', 'thumbnail_id', 'create_user_id', 'update_user_id', 'is_active', 'sort', 'settings_show_title', 'settings_show_description', 'settings_target_blank', 'type_id', 'data_id', 'parent_id', 'settings_thumbnail_show', 'format'], 'integer'],
            [['description', 'text_1', 'text_2', 'text_3', 'text_4', 'text_5', 'text_6', 'video_type'], 'string'],
            [['active_from', 'active_to', 'date_created', 'date_updated', 'date_display'], 'safe'],
            [['title', 'extlink', 'video', 'thumbnail_title', 'text_button'], 'string', 'max' => 255],
            [['categories_id'], 'exist', 'skipOnError' => true, 'targetClass' => SliderCategories::className(), 'targetAttribute' => ['categories_id' => 'id']],
            [['create_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['create_user_id' => 'id']],
            [['slider_id'], 'exist', 'skipOnError' => true, 'targetClass' => Slider::className(), 'targetAttribute' => ['slider_id' => 'id']],
            [['thumbnail_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['thumbnail_id' => 'id']],
            [['update_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['update_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('backend_module_slider', 'ID'),
            'slider_id' => Yii::t('backend_module_slider', 'Slider ID'),
            'categories_id' => Yii::t('backend_module_slider', 'Categories ID'),
            'title' => Yii::t('backend_module_slider', 'Title'),
            'description' => Yii::t('backend_module_slider', 'Description'),
            'extlink' => Yii::t('backend_module_slider', 'Extlink'),
            'video' => Yii::t('backend_module_slider', 'Video'),
            'thumbnail_id' => Yii::t('backend_module_slider', 'Thumbnail ID'),
            'thumbnail_title' => Yii::t('backend_module_slider', 'Thumbnail Title'),
            'create_user_id' => Yii::t('backend_module_slider', 'Create User ID'),
            'update_user_id' => Yii::t('backend_module_slider', 'Update User ID'),
            'is_active' => Yii::t('backend_module_slider', 'Is Active'),
            'active_from' => Yii::t('backend_module_slider', 'Active From'),
            'active_to' => Yii::t('backend_module_slider', 'Active To'),
            'date_created' => Yii::t('backend_module_slider', 'Date Created'),
            'date_updated' => Yii::t('backend_module_slider', 'Date Updated'),
            'date_display' => Yii::t('backend_module_slider', 'Date Display'),
            'sort' => Yii::t('backend_module_slider', 'Sort'),
            'text_1' => Yii::t('backend_module_slider', 'Text 1'),
            'text_2' => Yii::t('backend_module_slider', 'Text 2'),
            'text_3' => Yii::t('backend_module_slider', 'Text 3'),
            'text_4' => Yii::t('backend_module_slider', 'Text 4'),
            'text_5' => Yii::t('backend_module_slider', 'Text 5'),
            'text_6' => Yii::t('backend_module_slider', 'Text 6'),
            'text_button' => Yii::t('backend_module_slider', 'Text Button'),
            'settings_show_title' => Yii::t('backend_module_slider', 'Settings Show Title'),
            'settings_show_description' => Yii::t('backend_module_slider', 'Settings Show Description'),
            'settings_target_blank' => Yii::t('backend_module_slider', 'Settings Target Blank'),
            'type_id' => Yii::t('backend_module_slider', 'Type Id'),
            'data_id' => Yii::t('backend_module_slider', 'Data Id'),
            'parent_id' => Yii::t('backend_module_slider', 'Parent Id'),
            'settings_thumbnail_show' => Yii::t('backend_module_slider', 'Settings Thumbnail Show'),
            'format' => Yii::t('backend_module_slider', 'Format'),
            'video_type' => Yii::t('backend_module_slider', 'Video Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories() {
        return $this->hasOne(SliderCategories::className(), ['id' => 'categories_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser() {
        return $this->hasOne(User::className(), ['id' => 'create_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlider() {
        return $this->hasOne(Slider::className(), ['id' => 'slider_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getThumbnail($width = 2000, $height = 2000, $type = "crop", $effect = null, $quality = 80) 
    { 
        return File::getThumbnail($this->thumbnail_id, $width, $height, $type, $effect, $quality);
    } 

    public function hasThumbnail() {
        return !is_null($this->thumbnail_id);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdateUser() {
        return $this->hasOne(User::className(), ['id' => 'update_user_id']);
    }

    /**
     * @inheritdoc
     * @return XmodSliderItemsQuery the active query used by this AR class.
     */
    public static function find() {
        return new SliderItemsQuery(get_called_class());
    }

}
