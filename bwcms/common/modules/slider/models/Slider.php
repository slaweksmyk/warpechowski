<?php

namespace common\modules\slider\models;

use Yii;
use common\modules\users\models\User;
/**
 * This is the model class for table "xmod_slider".
 *
 * @property integer $id
 * @property integer $categories_id
 * @property integer $parent_id
 * @property integer $create_user_id
 * @property integer $update_user_id
 * @property integer $is_active
 * @property string $description
 * @property string $title
 * @property string $active_from
 * @property string $active_to
 * @property string $date_created
 * @property string $date_updated
 * @property string $date_display
 * @property integer $settings_arrow
 * @property integer $settings_dots
 * @property integer $settings_show_text
 * @property integer $settings_effect
 * @property integer $settings_effect_time
 * @property integer $settings_quantity_slide
 * @property integer $settings_quantity_slide_max
 * @property integer $type_id
 *
 * @property XmodSliderCategories $categories
 * @property User $createUser
 * @property XmodSliderEffect $settingsEffect
 * @property User $updateUser
 * @property XmodSliderItems[] $xmodSliderItems
 */
class Slider extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_slider}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
      
            [['categories_id', 'parent_id', 'create_user_id', 'update_user_id', 'is_active', 'settings_arrow', 'settings_dots', 'settings_show_text', 'settings_effect', 'settings_effect_time', 'settings_quantity_slide', 'settings_quantity_slide_max','type_id'], 'integer'],
            [['description'], 'string'],
            [['active_from', 'active_to', 'date_created', 'date_updated', 'date_display'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['categories_id'], 'exist', 'skipOnError' => true, 'targetClass' => SliderCategories::className(), 'targetAttribute' => ['categories_id' => 'id']],
            [['create_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['create_user_id' => 'id']],
            [['settings_effect'], 'exist', 'skipOnError' => true, 'targetClass' => SliderEffect::className(), 'targetAttribute' => ['settings_effect' => 'id']],
            [['update_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['update_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_slider', 'ID'),
            'categories_id' => Yii::t('backend_module_slider', 'Categories ID'),
            'parent_id' => Yii::t('backend_module_slider', 'Parent ID'),
            'create_user_id' => Yii::t('backend_module_slider', 'Create User ID'),
            'update_user_id' => Yii::t('backend_module_slider', 'Update User ID'),
            'is_active' => Yii::t('backend_module_slider', 'Is Active'),
            'description' => Yii::t('backend_module_slider', 'Description'),
            'title' => Yii::t('backend_module_slider', 'Title'),
            'active_from' => Yii::t('backend_module_slider', 'Active From'),
            'active_to' => Yii::t('backend_module_slider', 'Active To'),
            'date_created' => Yii::t('backend_module_slider', 'Date Created'),
            'date_updated' => Yii::t('backend_module_slider', 'Date Updated'),
            'date_display' => Yii::t('backend_module_slider', 'Date Display'),
            'settings_arrow' => Yii::t('backend_module_slider', 'Settings Arrow'),
            'settings_dots' => Yii::t('backend_module_slider', 'Settings Dots'),
            'settings_show_text' => Yii::t('backend_module_slider', 'Settings Show Text'),
            'settings_effect' => Yii::t('backend_module_slider', 'Settings Effect'),
            'settings_effect_time' => Yii::t('backend_module_slider', 'Settings Effect Time'),
            'settings_quantity_slide' => Yii::t('backend_module_slider', 'Settings Quantity Slide'),
            'settings_quantity_slide_max' => Yii::t('backend_module_slider', 'Settings Quantity Slide Max'),
            'type_id' => Yii::t('backend_module_slider', 'Type ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasOne(SliderCategories::className(), ['id' => 'categories_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(User::className(), ['id' => 'create_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSettingsEffect()
    {
        return $this->hasOne(SliderEffect::className(), ['id' => 'settings_effect']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdateUser()
    {
        return $this->hasOne(User::className(), ['id' => 'update_user_id']);
    }
 /**
     * @return \yii\db\ActiveQuery
     */
    public function getType($id)
    {
        return SliderType::findOne($id);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSliderItems()
    {
        return $this->hasMany(SliderItems::className(), ['slider_id' => 'id'])->all();
    }
    
    public function getSliderItemsRaw()
    {
        return $this->hasMany(SliderItems::className(), ['slider_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return SliderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SliderQuery(get_called_class());
    }
}
