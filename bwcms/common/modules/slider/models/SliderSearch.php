<?php

namespace common\modules\slider\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\slider\models\Slider;

/**
 * SliderSearch represents the model behind the search form about `common\modules\slider\models\Slider`.
 */
class SliderSearch extends Slider
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'categories_id', 'parent_id', 'create_user_id', 'update_user_id', 'is_active', 'settings_arrow', 'settings_dots', 'settings_show_text', 'settings_effect', 'settings_effect_time', 'settings_quantity_slide', 'settings_quantity_slide_max'], 'integer'],
            [['description', 'title', 'active_from', 'active_to', 'date_created', 'date_updated', 'date_display'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Slider::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'categories_id' => $this->categories_id,
            'parent_id' => $this->parent_id,
            'create_user_id' => $this->create_user_id,
            'update_user_id' => $this->update_user_id,
            'is_active' => $this->is_active,
            'active_from' => $this->active_from,
            'active_to' => $this->active_to,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
            'date_display' => $this->date_display,
            'settings_arrow' => $this->settings_arrow,
            'settings_dots' => $this->settings_dots,
            'settings_show_text' => $this->settings_show_text,
            'settings_effect' => $this->settings_effect,
            'settings_effect_time' => $this->settings_effect_time,
            'settings_quantity_slide' => $this->settings_quantity_slide,
            'settings_quantity_slide_max' => $this->settings_quantity_slide_max,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}
