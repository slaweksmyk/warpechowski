<?php

namespace common\modules\slider\models;

use Yii;
use common\modules\users\models\User;
/**
 * This is the model class for table "xmod_slider_categories".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $title
 * @property string $description
 * @property integer $sort
 * @property integer $is_active
 * @property integer $create_user_id
 * @property integer $update_user_id
 * @property string $date_created
 * @property string $date_updated
 * @property integer $type_id 
 *
 * @property Slider[] $Sliders
 * @property User $createUser
 * @property User $updateUser
 * @property SliderType $type 
 * @property SliderItems[] $SliderItems
 */
class SliderCategories extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_slider_categories}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'sort', 'is_active', 'create_user_id', 'update_user_id', 'type_id'], 'integer'],
            [['description'], 'string'],
            [['title','type_id'], 'required'],
            [['date_created', 'date_updated'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['create_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['create_user_id' => 'id']],
            [['update_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['update_user_id' => 'id']],
             [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => SliderType::className(), 'targetAttribute' => ['type_id' => 'id']], 
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_slider', 'ID'),
            'parent_id' => Yii::t('backend_module_slider', 'Parent ID'),
            'title' => Yii::t('backend_module_slider', 'Title'),
            'description' => Yii::t('backend_module_slider', 'Description'),
            'sort' => Yii::t('backend_module_slider', 'Sort'),
            'is_active' => Yii::t('backend_module_slider', 'Is Active'),
            'create_user_id' => Yii::t('backend_module_slider', 'Create User ID'),
            'update_user_id' => Yii::t('backend_module_slider', 'Update User ID'),
            'date_created' => Yii::t('backend_module_slider', 'Date Created'),
            'date_updated' => Yii::t('backend_module_slider', 'Date Updated'),
            'type_id' => Yii::t('backend_module_slider', 'Type ID')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSliders()
    {
        return $this->hasMany(Slider::className(), ['categories_id' => 'id'])->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateUser()
    {
        return $this->hasOne(User::className(), ['id' => 'create_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdateUser()
    {
        return $this->hasOne(User::className(), ['id' => 'update_user_id']);
    }

    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSliderItems()
    {
        return $this->hasMany(SliderItems::className(), ['categories_id' => 'id']);
    }
    
 /**
     * @return \yii\db\ActiveQuery
     */
    public static function getType($id)
    {
        return SliderType::findOne($id);
    }

    /**
     * @inheritdoc
     * @return SliderCategoriesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SliderCategoriesQuery(get_called_class());
    }
}
