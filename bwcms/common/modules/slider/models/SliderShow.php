<?php

namespace common\modules\slider\models;

use Yii;

/**
 * This is the model class for table "xmod_slider_show".
 *
 * @property integer $id
 * @property integer $slider_id
 * @property integer $page_id
 * @property string $controller
 * @property string $action
 */
class SliderShow extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_slider_show}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slider_id', 'page_id'], 'integer'],
            [['controller', 'action'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_slider', 'ID'),
            'slider_id' => Yii::t('backend_module_slider', 'Slider ID'),
            'page_id' => Yii::t('backend_module_slider', 'Page ID'),
            'controller' => Yii::t('backend_module_slider', 'Controller'),
            'action' => Yii::t('backend_module_slider', 'Action'),
        ];
    }

    /**
     * @inheritdoc
     * @return SliderShowQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SliderShowQuery(get_called_class());
    }
}
