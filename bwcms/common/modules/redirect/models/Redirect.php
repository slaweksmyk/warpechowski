<?php

namespace common\modules\redirect\models;

use Yii;

/**
 * This is the model class for table "{{%xmod_redirect}}".
 *
 * @property integer $id
 * @property string $url
 * @property string $target
 * @property integer $is_active
 */
class Redirect extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_redirect}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url', 'target'], 'url', 'message' => "Wartość musi być prawidłowym adresem url"],
            [['url', 'target'], 'required', 'message' => "Powyższe pole jest wymagane"],
            [['is_active'], 'integer'],
            [['url', 'target'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_redirect', 'ID'),
            'url' => Yii::t('backend_module_redirect', 'Url'),
            'target' => Yii::t('backend_module_redirect', 'Target'),
            'is_active' => Yii::t('backend_module_redirect', 'Is Active'),
        ];
    }
}
