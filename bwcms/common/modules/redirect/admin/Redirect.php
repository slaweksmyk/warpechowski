<?php

namespace common\modules\redirect\admin;

/**
 * files module definition class
 */
class Redirect extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\redirect\admin\controllers';
    public $defaultRoute = 'redirect';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
