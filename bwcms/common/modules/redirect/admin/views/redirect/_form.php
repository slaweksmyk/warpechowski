<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\redirect\models\Redirect */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="redirect-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="row">
        <div class="col-4"><?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?></div>
        <div class="col-4"><?= $form->field($model, 'target')->textInput(['maxlength' => true]) ?></div>
        <div class="col-4"><?= $form->field($model, 'is_active')->dropDownList([0 =>  Yii::t('backend', 'inactive'), 1 =>  Yii::t('backend', 'active')]) ?></div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_redirect', 'Create') : Yii::t('backend_module_redirect', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
