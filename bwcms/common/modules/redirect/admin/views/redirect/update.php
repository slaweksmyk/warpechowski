<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\redirect\models\Redirect */

$this->title = Yii::t('backend_module_redirect', 'Update {modelClass}: ', [
    'modelClass' => 'Redirect',
]) . $model->id;
?>
<div class="redirect-update">

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
        </header>
        <div class="panel-body" >
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </section>   

</div>
