<?php

namespace common\modules\logs\models;

use Yii;
use yii\helpers\Url;
use common\modules\modules\models\Module;
use common\modules\users\models\User;
use common\modules\pages\models\PagesAdmin;

/**
 * This is the model class for table "{{%logs}}".
 *
 * @property integer $id
 * @property integer $module_id
 * @property integer $user_id
 * @property string $action
 * @property string $value
 * @property string $date
 * @property string $url
 * @property string $ip_adress
 *
 * @property Modules $module
 * @property User $user
 */
class Log extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_logs}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['module_id', 'user_id'], 'integer'],
            [['date'], 'safe'],
            [['action', 'value', 'url', 'ip_adress'], 'string', 'max' => 255],
            [['module_id'], 'exist', 'skipOnError' => true, 'targetClass' => Module::className(), 'targetAttribute' => ['module_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'module_id' => Yii::t('backend', 'Module ID'),
            'user_id' => Yii::t('backend', 'User ID'),
            'action' => Yii::t('backend', 'Action'),
            'value' => Yii::t('backend', 'Value'),
            'date' => Yii::t('backend', 'DateAdd'),
            'url' => Yii::t('backend', 'Url'),
            'ip_adress' => Yii::t('backend', 'Ip Adress'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModule()
    {
        return $this->hasOne(Modules::className(), ['id' => 'module_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    /**
     * Save a single Log model record.
     * @param integer $moduleID
     * @param string $action
     * @param string $value
     * @return mixed
     */
    public static function addRecord($moduleID, $action, $value, $param = null){
        $log = new self();
        $log->module_id = PagesAdmin::findOne(["url" => $moduleID])->module_id;
        $log->user_id = Yii::$app->user->identity->id;
        $log->action = $action;
        $log->value = $value;
        $log->date = date('Y-m-d H:i:s');
        $log->ip_adress = Yii::$app->getRequest()->getUserIP();
        if($param){
            $log->url = Url::to(['view', 'id' => $param]);
        }
        $log->save();
    }
    
}
