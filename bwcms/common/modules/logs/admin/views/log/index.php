<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;
use common\modules\modules\models\Module;
use common\modules\users\models\User;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\logs\models\LogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'module_logs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-index">
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <?php if(Yii::$app->request->get('per-page')){ echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>"; } ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'date',
                        'value' => function ($data) {
                            return date("d.m.Y H:i", strtotime($data["date"]));
                        }
                    ],
                    [
                        'attribute' => 'user_id',
                        'value' => function ($data) {
                            return User::findOne($data["user_id"])->username;
                        }
                    ],
                    [
                        'attribute' => 'module_id',
                        'value' => function ($data) {
                            return Module::findOne($data)->name;
                        }
                    ],
                    [
                        'attribute' => 'action',
                        'value' => function ($data) {
                            return Yii::t('backend', $data["action"]);
                        }
                    ],
                    'value',
                    /*[
                        'attribute' => 'url',
                        'format' => 'raw',
                        'value' => function ($data) {
                            if($data["url"]){
                                return "<a href='{$data["url"]}'>Podgląd</a>";
                            }
                        }
                    ], */
                    'ip_adress',

                    [
                        'class' => 'common\hooks\yii2\grid\ActionColumn', 
                        'template' => '{view}'
                    ],
                ],
            ]); ?>
        </div>
    </section>
</div>

