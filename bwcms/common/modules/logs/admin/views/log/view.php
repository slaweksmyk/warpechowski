<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\modules\modules\models\Module;
use common\modules\users\models\User;

/* @var $this yii\web\View */
/* @var $model common\modules\logs\models\Log */

$this->title = Yii::t('backend', 'module_logs_edit')." #".$model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'module_logs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-view">

    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?></header>
        <div class="panel-body" >
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    [
                        'attribute' => 'module_id',
                        'value' => Module::findOne($model["module_id"])->name
                    ],
                    [
                        'attribute' => 'user_id',
                        'value' => User::findOne($model["user_id"])->username
                    ],
                    'action',
                    'value',
                    'date',
                    /*'url:url',*/
                    'ip_adress',
                ],
            ]) ?>
        </div>
    </section>
</div>
