<?php
/**
 * Module: SEO Meta (backend)
 * 
 * Module init
 * 
 * PHP version 7
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @copyright (c) 2016 Krzysztof Borecki
 * @version 1.0
 */

namespace common\modules\logs\admin;

/**
 * files module definition class
 */
class Logs extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\logs\admin\controllers';
    public $defaultRoute = 'log';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
