<?php
/**
 * Logs module
 * 
 * PHP version 7
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @copyright (c) 2016 Krzysztof Borecki
 * @version 1.0
 */

namespace common\modules\logs\admin\controllers;

use Yii;
use common\modules\logs\models\Log;
use common\modules\logs\models\LogSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LogController implements the CRUD actions for Log model.
 */
class LogController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    /**
     * Before load controller actions
     * 
     * @param string $action
     * @return boolean
     */
    public function beforeAction($action)
    {
        if ( parent::beforeAction($action) ) {
            $sActionType = strtolower(str_replace("action", "", $action->actionMethod));

            if($sActionType == "index") $sActionType = "view";
            if(!Yii::$app->user->can($this->module->id."_{$sActionType}") ){
                return $this->redirect(Yii::$app->homeUrl);
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Lists all Log models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy("`id` DESC");
        
        if(Yii::$app->request->get('per-page')){
            $dataProvider->setPagination(['pageSize' => Yii::$app->request->get('per-page')]);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Log model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Log model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Log the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Log::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
       
}
