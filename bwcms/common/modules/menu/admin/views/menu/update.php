<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\menu\models\Menu */

$this->title = Yii::t('backend_module_menu', 'module_menu_update') .': '. $model->name;

//add CSS
$this->registerCssFile('/bwcms/common/modules/menu/admin/assets/css/menu_builder.css');

//add script
$this->registerJsFile('/bwcms/common/modules/menu/admin/assets/js/menu_builder_ui.js', ['depends' => [yii\web\JqueryAsset::className()]]);
?>

<p>
    <?= Html::a(Yii::t('backend_module_menu', 'back_to_menu_list'), ['index'], ['class' => 'btn btn-outline-secondary']) ?>
</p>

<div class="menu-update">

    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend_module_menu', 'module_menu_update') ?></header>
        <div class="panel-body" >

            <div class="menu-form row" >
                <?php $form = ActiveForm::begin(); ?>
                    <div class="col-12 col-sm-4 col-md-3 col-lg-2 pull-right" >
                        <div class="form-group text-right" >
                            <?php echo Html::submitButton($model->isNewRecord ? Yii::t('backend', 'create') : Yii::t('backend', 'update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']); ?>
                        </div>
                    </div>
                    <div class="col-12 col-sm-8 col-md-9 col-lg-10" >
                        <?php echo $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Name'])->label(false); ?>
                    </div>  
                    <?php echo $form->field($model, 'data')->textarea(['rows' => 6, 'class' => 'form-control noTynyMCE hidden', 'id' => 'menu-itemsResult', 'readonly' => true])->label(false); ?>  
                <?php ActiveForm::end(); ?>
            </div>

            <div class="row menu-generator" >
                <div class="col-12 col-sm-5 col-md-4" >
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4><?= Yii::t('backend', 'module_menu_page_list') ?>:</h4>
                        </div>
                        <div class="panel-body" >
                            <div class="space first-space" id="menu-pagesList" >
                                <?php $controller->pagesListGenerator($model['id']); ?>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-12 col-sm-7 col-md-8" >
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4><?= Yii::t('backend', 'module_menu_page_elements') ?>:</h4>
                        </div>
                        <div class="panel-body" >
                            <div class="space first-space" id="menu-itemsList" >
                                <?php $controller->updateMenuGenerator( $model['data'] ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

</div>
