<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\menu\models\Menu */

$this->title = Yii::t('backend_module_menu', 'menu').': '.$model->name;

//add CSS
$this->registerCssFile('/bwcms/common/modules/menu/admin/assets/css/menu_builder.css');
?>

 

<div class="menu-view">

    <p>
         <?= Html::a(Yii::t('backend_module_menu', 'back_to_menu_list'), ['index'], ['class' => 'btn btn-outline-secondary']) ?>
        <?php echo Html::a(Yii::t('backend', 'edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']); ?>
        <?php echo Html::a(Yii::t('backend', 'delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'menu_confirm_delete'),
                'method' => 'post',
            ],
        ]); ?>
    </p>

    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend_module_menu', 'menu_data') ?></header>
        <div class="panel-body" >
        <?php echo DetailView::widget([
            'model' => $model,
            'attributes' => [ 
                'id', 
                [
                    'attribute' => 'name',
                    'label' => Yii::t('backend', 'Name'),
                ] 
            ],
        ]); ?>
        </div>
    </section>
    
    <div class="menu-generator" >
        <section class="panel full" >
            <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend', 'items') ?></header>
            <div class="panel-body" >
                <div class="space first-space view" id="menu-itemsList" >
                    <?php $controller->updateMenuGenerator( $model['data'] ); ?>
                </div>
            </div>
        </section>
    </div>

</div>
