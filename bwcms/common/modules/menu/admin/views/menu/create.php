<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\menu\models\Menu */

$this->title = Yii::t('backend_module_menu', 'menu_create');
$this->params['breadcrumbs'][] = $this->title;
?>

<p>
    <?= Html::a(Yii::t('backend_module_menu', 'back_to_menu_list'), ['index'], ['class' => 'btn btn-outline-secondary']) ?>
</p>


<div class="menu-create">
    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?></header>
        <div class="panel-body" >
            <div class="menu-form">
                <?php $form = ActiveForm::begin(); ?>

                <?php echo $form->field($model, 'name')->textInput(['maxlength' => true]); ?>

                <div class="form-group">
                    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('backend', 'create') : Yii::t('backend', 'update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']); ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>

        </div>
    </section>
</div>
