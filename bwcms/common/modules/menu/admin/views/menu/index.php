<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\menu\models\MenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend_module_menu', 'menu_list');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-index">

    <p><?php echo Html::a(Yii::t('backend_module_menu', 'menu_create'), ['create'], ['class' => 'btn btn-success']); ?></p>
    
    <?php if( $errorAction ) { ?>
        <div style="padding: 15px; background: #ffd4d4; border-radius: 7px; margin-bottom: 10px;" >
            <h5>
                <?= Yii::t('backend', 'module_menu_error') ?>: 
                <b><?php echo $errorAction['name']; ?></b> <small>(ID <?php echo $errorAction['id']; ?>)</small>
            </h5>
        </div>
    <?php } ?>
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/><?= Yii::t('backend', 'module_menu_list') ?>
            <form id="per-page">
                <span><?= Yii::t('backend', 'quantity_per_page'); ?>:</span>
                <select name="per-page" class="form-control">
                    <?php if(Yii::$app->request->get('per-page')){ echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>"; } ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?php echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [   
                        'class' => 'yii\grid\SerialColumn',
                        'header' => Yii::t('backend', 'LP'),
                        'contentOptions'=>['style'=>'width: 70px']
                    ],

                    [
                        'attribute' => 'id',
                        'contentOptions'=>['style'=>'width: 70px']
                    ],
                    'name',

                    //'data:ntext',

                    [
                        'class' => 'common\hooks\yii2\grid\ActionColumn',
                        'template' => '{update} {view} {delete}',
                        'contentOptions' => ['style' => 'width: 100px;'],
                        'buttons' => [
                            'update' => function ($url) {
                                return Html::a(
                                                '<span class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></span>', $url, [
                                            'title' => Yii::t('backend', 'edit'),
                                            'data-toggle' => "tooltip",
                                            'data-placement' => "bottom"
                                                ]
                                );
                            },
                            'view' => function ($url) {
                                return Html::a(
                                                '<span class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-eye-open"></span></span>', $url, [
                                            'title' => Yii::t('backend', 'view'),
                                            'data-toggle' => "tooltip",
                                            'data-placement' => "bottom"
                                                ]
                                );
                            },
                            'delete' => function ($url) {
                                return Html::a(
                                                '<span class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></span>', $url, [
                                            'title' => Yii::t('backend', 'delete'),
                                            'data-toggle' => "tooltip",
                                            'data-placement' => "bottom",
                                            'data-pjax' => 0,
                                            'data-confirm' => 'Czy na pewno usunąć ten element?',
                                            'data-method' => 'post',
                                                ]
                                );
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </section>
    
</div>
