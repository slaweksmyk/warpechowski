$(document).ready(function(){
    
    /**
     *  UI Sortable init
     */
    $('.menu-generator .space').sortable({
        connectWith:'.menu-generator .space',
        tolerance:'intersect',
        scroll:false,
        start: function(event, ui) {
            beforeDrop(ui);
        },
        stop: function(event, ui) {
            afterDrop(ui);
        }
    });
    $('.menu-generator .space').disableSelection();
    
    /**
     * Before drop function
     * @param {object} ui
     */
    function beforeDrop(ui)
    {
        $('#menu-itemsList .item').addClass('show');

        var $item = $(ui.item.context);
        $item.addClass('active');
    }
    
    /**
     * After drop function
     * 
     * @param {object} ui
     */
    function afterDrop(ui)
    {
        $('#menu-itemsList .item.show').removeClass('show');

        var $item = $(ui.item.context); $item.removeClass('active');
		
        if( $item.parent().attr('id') === 'menu-pagesList' ){
            pagesListLine($item, false);
        }
        
        var $menuJsonString = JSON.stringify( childsToArray(false) );
        $('#menu-itemsResult').val( $menuJsonString );
    }
    
    /**
     * Page structure to line
     * 
     * @param {object} $item
     * @param {object} $parent
     */
    function pagesListLine( $item, $parent )
    {
        if( $parent ){ $parent.after( $item ); }
        var $childrens = $item.children('.space');
        if ( $childrens.children('.item').length > 0 ) {
            $childrens.children('.item').each(function(){
                pagesListLine( $(this), $item );
            });
        }
    }
    
    /**
     * Create menu items Array
     * 
     * @param {object} $parent
     * @returns {Array}
     */
    function childsToArray( $parent ){
        var $array = [],
            $items = $('#menu-itemsList');
        if( $parent ){ $items = $parent.children('.space'); }
        
        $items.children('.item').each(function(){
            if ( $(this).children('.space').children('.item').length > 0 ) {
                var $childrens_items = childsToArray( $(this) );
                $array.push({
                    'id' : $(this).data('id'),
                    'children' : $childrens_items
                });
            } else {
                $array.push({ 'id' : $(this).data('id') });
            }
        });
        
        return $array;
    }
    
    
    // EDIT MENU by Click to arrows
    $('#menu-itemsList .item .up').on('click', function(){
        buttonMoveItem( $(this).parent(), 'up' );
    });
    $('#menu-itemsList .item .down').on('click', function(){
        buttonMoveItem( $(this).parent(), 'down' );
    });
    
    function buttonMoveItem( $item, $move )
    {
        var menu_edit = false;
        
        if( $move === 'up' ){
            var prevItem = $item.prev();
            if( prevItem.hasClass('item') ){
                prevItem.before($item);
                menu_edit = true;
            }
        }else if( $move === 'down' ){
            var nextItem = $item.next();
            if( nextItem.hasClass('item') ){
                nextItem.after($item);
                menu_edit = true;
            }
        }
        
        if( menu_edit ){
            var $menuJsonString = JSON.stringify( childsToArray(false) );
            $('#menu-itemsResult').val( $menuJsonString );
        }
    }
    
});