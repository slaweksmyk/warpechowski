<?php

/**
 * Module Website MENU - main BACKEND controller
 * 
 * PHP version 7
 *
 * @author Volodymyr Shelelo <contact@profficode.com>
 * @copyright (c) 2016 Volodymyr Shelelo
 * @version 1.0
 */

namespace common\modules\menu\admin\controllers;

use Yii;
use common\modules\menu\models\Menu;
use common\modules\menu\models\MenuSearch;
use common\modules\menu\models\MenuItems;
use common\modules\pages\models\PagesSite;
use common\modules\pages\models\PagesAdmin;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MenuController implements the CRUD actions for Menu model.
 */
class MenuController extends Controller {

    /**
     * Protected menu id
     * 
     * @var array
     */
    protected $protected_id = [1];

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Menu models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new MenuSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->get('per-page')) {
            $dataProvider->setPagination(['pageSize' => Yii::$app->request->get('per-page')]);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'errorAction' => Yii::$app->request->get('errorAction', null)
        ]);
    }

    /**
     * Displays a single Menu model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'controller' => $this
        ]);
    }

    /**
     * Creates a new Menu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Menu();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Menu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $update = $this->menuUpdate($model['id']);

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'controller' => $this
            ]);
        }
    }

    /**
     * Deletes an existing Menu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $thisMenu = $this->findModel($id);

        if (in_array($id, $this->protected_id)) {
            return $this->redirect([
                'index',
                'errorAction' => ['id' => $id, 'name' => $thisMenu['name']]
            ]);
        } else {
            $delete = $thisMenu->delete();
            if ($delete) {
                self::clearMenuItems($id);
            }

            return $this->redirect(['index']);
        }
    }

//-------------------- METHODS -------------------------------------------------
//------------------------------------------------------------------------------

    /**
     * Menu generator (in the view)
     * 
     * @param string $modelMenu
     */
    public static function updateMenuGenerator($modelMenu) {
        if ($modelMenu) {
            $menu_array = json_decode($modelMenu);
            if (!empty($menu_array)) {
                echo self::updateMenuItemsGenerator($menu_array);
            }
        }
    }

    /**
     * Echo menu items on admin panel
     * 
     * @param array $array
     */
    protected static function updateMenuItemsGenerator($array, $result = false) {
        $pagesModel = new PagesSite();
        $PagesAdmin = new PagesAdmin();

        $edit_page_url = $PagesAdmin->find()->select(['url'])->where(['id' => 5])->one();
        $page_edit = Yii::$app->request->baseUrl . '/' . $edit_page_url['url'];

        foreach ($array as $row) {
            $page = $pagesModel->find()->select(['id', 'name'])->where(['id' => $row->id])->one();
            if (isset($row->children)) {
                $result .= "<div class='item btn btn-outline-secondary' data-id='{$page['id']}' >"
                        . "<span class='move' ></span><span class='up'></span><span class='down'></span>"
                        . "<a href='{$page_edit}/page/view/?id={$page['id']}' target='_blank'><span class='view'></span></a>"
                        . "<h5>{$page['name']}</h5>"
                        . "<div class='space' >" . self::updateMenuItemsGenerator($row->children) . "</div>"
                        . "</div>";
            } else {
                $result .= "<div class='item btn btn-outline-secondary' data-id='{$page['id']}' >"
                        . "<span class='move' ></span><span class='up'></span><span class='down'></span>"
                        . "<a href='{$page_edit}/page/view/?id={$page['id']}' target='_blank'><span class='view'></span></a>"
                        . "<h5>{$page['name']}</h5><div class='space' ></div>"
                        . "</div>";
            }
        }

        return $result;
    }

    /**
     * Update all menu by ID
     * 
     * @param int $menu_id
     */
    public static function menuUpdate($menu_id) {
        $modulMenu = new Menu();
        $menu = $modulMenu->findOne(['id' => $menu_id]);

        if ($menu) {
            self::clearMenuItems($menu_id);

            if ($menu['data'] === '[]') {
                $menu_data = false;
            } else {
                $menu_data = json_decode($menu['data']);
            }

            if (!empty($menu_data)) {
                $menuRowsetJson = self::menuUpdateStep2($menu_id, $menu_data);
                $menu->rowset = json_encode($menuRowsetJson);
                $menu->update();
            } else {
                $menu->rowset = null;
            }

            return true;
        }
    }

    /**
     * Remove menu items and reset page url
     * 
     * @param int $menu_id
     */
    protected static function clearMenuItems($menu_id) {
        $menuItems = new MenuItems();
        $PagesSite = new PagesSite();

        if ($menu_id == 1) {
            $pages_item = $menuItems->findAll(['menu_id' => $menu_id]);
        } else {
            $pages_item = array();
        }

        foreach ($pages_item as $item) {
            $page = $PagesSite->findOne(['id' => $item['page_id']]);
            if ($page) {
                $page->url = $page['slug'];
                $page->update();
            }
        }

        $menuItems->deleteAll(['menu_id' => $menu_id]);
    }

    /**
     * Generate menu rowset and save Items
     * 
     * @param int $menu_id
     * @param array $menu_data
     * @param string $parentUrl
     * @return array
     */
    protected static function menuUpdateStep2($menu_id, $menu_data, $parentUrl = false) {
        $PagesSite = new PagesSite();
        $menu_rowset = array();
        $i = 0;

        foreach ($menu_data as $item) {
            $page = $PagesSite->find()->select(['id', 'name', 'slug', 'url', 'is_home'])->where(['id' => $item->id])->one();

            if ($menu_id  && $parentUrl && $page['is_home'] == 0) {
                $pageUrl = $parentUrl . '/' . $page['slug'];
            } elseif ($menu_id  && $page['is_home'] == 1) {
                $pageUrl = false;
            } elseif ($menu_id) {
                $pageUrl = $page['slug'];
            } else {
                $pageUrl = $page['url'];
            }

            if ($menu_id  && $pageUrl) {
                $url_math = $pageUrl;
                $menu_rowset[$i]['url'] = '/' . $url_math . '/';
            } elseif ($menu_id ) {
                $url_math = '/';
                $menu_rowset[$i]['url'] = $url_math;
            } else {
                $url_math = false;
                $menu_rowset[$i]['url'] = '/' . $pageUrl . '/';
            }
            
            
            $menu_rowset[$i]['id'] = $page['id'];
            self::updateSetMenuItem(array('page_id' => $item->id, 'page_url' => $url_math, 'menu_id' => $menu_id, 'is_home' => $page->is_home));

            if (isset($item->children)) {
                $menu_rowset[$i]['children'] = self::menuUpdateStep2($menu_id, $item->children, $pageUrl);
            }
            $i++;
        }

        return $menu_rowset;
    }

    /**
     * Update Menu ITEMS table
     * 
     * @param array $item
     */
    protected static function updateSetMenuItem($item = array()) {
        if ($item['page_id'] && $item['menu_id']) {
            $menu_item = new MenuItems();
            $menu_item->page_id = $item['page_id'];
            $menu_item->menu_id = $item['menu_id'];
            $menu_item->save();
        }
        if ($item['page_id'] && $item['page_url']) {
            $pageModel = new PagesSite();
            $page = $pageModel->findOne(['id' => $item['page_id']]);
            if ($item['is_home']) {
                $page->url = '/';
            } else {
                $page->url = $item['page_url'];
            }
            $page->update();
        }
    }

    /**
     * Generate pages list about UPDATE MANU in admin panel
     * 
     * @param int $menu_id
     * @param array $modelMenu
     */
    public static function pagesListGenerator($menu_id) {
        $menuItems = new MenuItems();
        $active_items = $menuItems->findAll(['menu_id' => $menu_id]);

        $pagesModel = new PagesSite();
        $select = $pagesModel->find()->select(['id', 'name']);
        $select->where(['is_active' => 1]);

        foreach ($active_items as $item) {
            $select->andWhere(['not', ['id' => $item['page_id']]]);
        }
        $pages = $select->all();

        if (!empty($pages)) {
            $PagesAdmin = new PagesAdmin();
            $edit_page_url = $PagesAdmin->find()->select(['url'])->where(['id' => 5])->one();
            $page_edit = Yii::$app->request->baseUrl . '/' . $edit_page_url['url'];
            $result = '';

            foreach ($pages as $page) {
                $result .= "<div class='item btn btn-outline-secondary' data-id='{$page['id']}' >"
                        . "<span class='move' ></span><span class='up'></span><span class='down'></span>"
                        . "<a href='{$page_edit}/page/view/?id={$page['id']}' target='_blank'><span class='view'></span></a>"
                        . "<h5>{$page['name']}</h5><div class='space' ></div>"
                        . "</div>";
            }

            echo $result;
        }
    }

    /**
     * Finds the Menu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Menu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Menu::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
