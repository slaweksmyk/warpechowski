<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = "Aktywacja strony";

function checkPermissions($sPath){
    $sPerms  = substr(sprintf('%o', fileperms($sPath)), -4);
    return [$sPerms, ($sPerms == "0777") ? $sStatus = "✓" : $sStatus = "✗"];
}

?>
<?php $form = ActiveForm::begin(); ?>

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend_module_config', "section_smtp") ?>            
        </header>
        <div class="panel-body" >
            <div class="config-form">
                <div class="row">
                    <div class="col-12"><?= $form->field($config, 'default_email')->textInput(['maxlength' => true]) ?></div>
                </div>
                <div class="row">
                    <div class="col"><?= $form->field($config, 'smtp_host')->textInput(['maxlength' => true]) ?></div>
                    <div class="col"><?= $form->field($config, 'smtp_username')->textInput(['maxlength' => true]) ?></div>
                    <div class="col"><?= $form->field($config, 'smtp_password')->passwordInput(['maxlength' => true]) ?></div>
                    <div class="col"><?= $form->field($config, 'smtp_port')->textInput() ?></div>
                    <div class="col"><?= $form->field($config, 'smtp_encryption')->dropDownList([null => "Brak", "SSL" => "SSL", "TLS" => "TLS"]) ?></div>
                </div>
                <div class="form-group">
                    <?= Html::submitButton($config->isNewRecord ? Yii::t('backend_module_config', 'Create') : "Zapisz zmiany", ['class' => $config->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    </section>

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Test ustawień SMTP           
        </header>
        <div class="panel-body" >
            <div class="config-form">
                <div class="row">
                    <div class="col-6"><?= $form->field($model, 'test_email')->textInput()->label("Testowy adres e-mail") ?></div>
                </div>
                <div class="form-group">
                    <?= Html::submitButton("wyślij", ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    </section>

    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Uprawnienia</header>
        <div class="panel-body" >
            <div class="article-form">
                <div class="row">
                    <div class="col-6">
                        Upload: <b><?= checkPermissions(\Yii::getAlias('@root')."/upload/")[1] ?></b> (<?= checkPermissions(\Yii::getAlias('@root')."/upload/")[0] ?>)<br/>
                        Cache: <b><?= checkPermissions(\Yii::getAlias('@root')."/cache/")[1] ?></b> (<?= checkPermissions(\Yii::getAlias('@root')."/cache/")[0] ?>)<br/>
                    </div>
                    <div class="col-6">
                        Front-end (assets): <b><?= checkPermissions(\Yii::getAlias('@frontend')."/assets/")[1] ?></b> (<?= checkPermissions(\Yii::getAlias('@frontend')."/assets/")[0] ?>)<br/>
                        Back-end (assets): <b><?= checkPermissions(\Yii::getAlias('@backend')."/assets/")[1] ?></b> (<?= checkPermissions(\Yii::getAlias('@backend')."/assets/")[0] ?>)<br/>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Robots.txt</header>
        <div class="panel-body" >
            <div class="article-form">
                <div class="row">
                    <div class="col-12"><?= $form->field($model, 'robots')->textarea(["class" => "noTynyMCE", "style" => "height: 150px; width: 100%", "value" => file_get_contents(\Yii::getAlias('@frontend')."/web/robots.txt")])->label(false) ?></div>
                </div>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('backend_module_articles',  'update'), ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    </section>

<?php ActiveForm::end(); ?>
