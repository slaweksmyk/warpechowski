<?php
namespace common\modules\initialize\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;

use common\modules\initialize\models\Initialize;

/**
 * LayoutController implements the CRUD actions for PagesSiteLayout model.
 */
class InitializeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PagesSiteLayout models.
     * @return mixed
     */
    public function actionIndex()
    {
        
        $model  = new Initialize();
        $config = Yii::$app->params['oConfig'];
        
        if(Yii::$app->request->isPost){
            $config->load(Yii::$app->request->post());
            $config->save();

            if(Yii::$app->request->post("Initialize")["test_email"]){
                \Yii::$app->mailer->compose()
                    ->setTo(Yii::$app->request->post("Initialize")["test_email"])
                    ->setSubject("Testowa wiadomość")
                    ->setTextBody("Test wysyłki SMTP zakończony poprawnie.")
                    ->send();
            }
            
            if(Yii::$app->request->post("Initialize")["robots"]){
                file_put_contents(\Yii::getAlias('@frontend')."/web/robots.txt", Yii::$app->request->post("Initialize")["robots"]);
            }
            
        }

        return $this->render('index', [
            'model' => $model,
            'config' => $config
        ]);
    }

}
