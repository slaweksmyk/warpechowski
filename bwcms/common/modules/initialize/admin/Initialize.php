<?php
namespace common\modules\initialize\admin;

/**
 * layout module definition class
 */
class Initialize extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\initialize\admin\controllers';
    public $defaultRoute = 'initialize';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
