<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\permissions\models\AuthItem */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Auth Item',
]) . $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Auth Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->username]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="auth-item-update">

    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?></header>
        <div class="panel-body" >
            <div class="auth-item-form">
                <?php 
                    $form = ActiveForm::begin();
                    $aRoles = [];
                    foreach($oRolesRowset as $oRole){ $aRoles[$oRole->name] = $oRole->name; }
                ?>
                
                <?= $form->field($oUserRole, 'item_name')->dropDownList($aRoles)->label("Grupa") ?>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </section>

</div>
