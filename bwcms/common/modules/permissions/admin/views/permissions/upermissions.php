<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\permissions\models\AuthItemChild;

/* @var $this yii\web\View */
/* @var $model common\modules\permissions\models\AuthItem */

$this->title = "Edytuj uprawnienia: ".$model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'permissions_role'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile('/app-module-assets/permissions/admin/assets/js/permissionChange.js');
?>
<div class="auth-item-view">
    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?></header>
        <div class="panel-body" >
        <?php $form = ActiveForm::begin();  ?>
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Nazwa modułu</th>
                        <th style="text-align: center;">Wyświetlanie</th>
                        <th style="text-align: center;">Edytowanie</th>
                        <th style="text-align: center;">Kasowanie</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $auth = Yii::$app->authManager;
                        foreach($moduleModel as $mModel){
                            $module_name = strtolower($mModel->name);
                    ?>
                            <tr>
                                <td><?= $mModel->name ?></td>
                                <td style="text-align: center;">
                                    <?php $can_view = (int) !is_null(AuthItemChild::find()->where("parent = '{$id}' AND child = '{$module_name}_view'")->one()); ?>
                                    <input type="hidden" name="view_<?= $mModel->id ?>" value="<?= $can_view ?>"/>
                                    <div class="choice_selector <?php if($can_view) { echo "active"; } ?>"></div>
                                </td>
                                <td style="text-align: center;">
                                    <?php $can_edit = (int) !is_null(AuthItemChild::find()->where("parent = '{$id}' AND child = '{$module_name}_edit'")->one()); ?>
                                    <input type="hidden" name="edit_<?= $mModel->id ?>" value="<?= $can_edit ?>"/>
                                    <div class="choice_selector <?php if($can_edit) { echo "active"; } ?>"></div>
                                </td>
                                <td style="text-align: center;">
                                    <?php $can_delete = (int) !is_null(AuthItemChild::find()->where("parent = '{$id}' AND child = '{$module_name}_delete'")->one()); ?>
                                    <input type="hidden" name="delete_<?= $mModel->id ?>" value="<?= $can_delete ?>"/>
                                    <div class="choice_selector <?php if($can_delete) { echo "active"; } ?>"></div>
                                </td>
                            </tr>
                        <?php } ?>
                </tbody>
            </table>
            <?php ActiveForm::end(); ?>
        </div>
    </section>

</div>
