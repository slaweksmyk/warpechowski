<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;
use yii\helpers\Url;
use common\modules\users\models\User;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\permissions\models\AuthItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'module_permissions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-item-index">
    <p>
        <?= Html::a(Yii::t('backend', 'Create Role'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <?php if(Yii::$app->request->get('per-page')){ echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>"; } ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'Uźytkownik',
                        'format' => 'raw',
                        'value' => function ($data) {
                            $oUser = User::findIdentity($data["user_id"]);
                            return $oUser ? $oUser->username : null;
                        }
                    ],
                    [
                        'attribute' => 'Data dodania',
                        'value' => function ($data) {
                            return date("d.m.Y H:i", $data["created_at"]);
                        }
                    ],     
                    [
                        'class' => 'common\hooks\yii2\grid\ActionColumn',
                        'template' => '{uedit} {udelete}',
                        'buttons' => [
                            'userpermissions' => function ($url) {
                                return Html::a(
                                    '<span class="glyphicon glyphicon-list-alt"></span>',
                                    $url, 
                                    [
                                        'title' => 'Change permissions',
                                        'data-pjax' => '0',
                                    ]
                                );
                            },
                            'uedit' => function ($url) {
                                return Html::a(
                                    '<span class="glyphicon glyphicon-pencil"></span>',
                                    $url, 
                                    [
                                        'title' => 'Edytuj',
                                        'data-pjax' => '0',
                                    ]
                                );
                            },
                            'udelete' => function ($url) {
                                return Html::a(
                                    '<span class="glyphicon glyphicon-trash"></span>',
                                    $url, 
                                    [
                                        'title' => 'Skasuj',
                                        'data-confirm' => 'Czy na pewno usunąć ten element?',
                                        'data-pjax' => '0',
                                    ]
                                );
                            },    
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            if ($action === 'uedit') {
                                return Url::to(['permissions/uedit', 'id' => $model->user_id]);
                            }
                        }
                    ],
                ],
            ]); ?>
        </div>
    </section>   
</div>
