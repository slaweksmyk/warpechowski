<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\permissions\models\AuthItemChild;

/* @var $this yii\web\View */
/* @var $model common\modules\permissions\models\AuthItem */

$this->title = Yii::t('backend', 'update') . ": " . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Auth Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile('/bwcms/common/modules/permissions/admin/assets/js/permissionChange.js');
?>
<div class="auth-item-view">
    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?></header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(); ?>
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th><?= Yii::t('backend', 'module_name') ?></th>
                        <th style="text-align: center;"><?= Yii::t('backend', 'creating') ?></th>
                        <th style="text-align: center;"><?= Yii::t('backend', 'viewing') ?></th>
                        <th style="text-align: center;"><?= Yii::t('backend', 'editing') ?></th>
                        <th style="text-align: center;"><?= Yii::t('backend', 'deleting') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Panel administracyjny</td>
                        <td style="text-align: center;" colspan="4">
                            <?php $can = (int) !is_null(AuthItemChild::find()->where("parent = '{$id}' AND child = 'adminPanel'")->one()); ?>
                            <input type="hidden" name="adminPanel" value="<?= $can ?>"/>
                            <div class="choice_selector <?php
                            if ($can) {
                                echo "active";
                            }
                            ?>"></div>
                        </td>
                    </tr>
                    <?php
                    $auth = Yii::$app->authManager;
                    foreach ($moduleModel as $mModel) {
                        $module_name = strtolower($mModel->name);
                        ?>
                        <tr>
                            <td><?= $mModel->display_name ?></td>
                            <td style="text-align: center;">
                                <?php $can_create = (int) !is_null(AuthItemChild::find()->where("parent = '{$id}' AND child = '{$module_name}_create'")->one()); ?>
                                <input type="hidden" name="create_<?= $mModel->id ?>" value="<?= $can_create ?>"/>
                                <div class="choice_selector <?php
                            if ($can_create) {
                                echo "active";
                            }
                                ?>"></div>
                            </td>
                            <td style="text-align: center;">
                                     <?php $can_view = (int) !is_null(AuthItemChild::find()->where("parent = '{$id}' AND child = '{$module_name}_view'")->one()); ?>
                                <input type="hidden" name="view_<?= $mModel->id ?>" value="<?= $can_view ?>"/>
                                <div class="choice_selector <?php
                                if ($can_view) {
                                    echo "active";
                                }
                                ?>"></div>
                            </td>
                            <td style="text-align: center;">
                                <?php $can_update = (int) !is_null(AuthItemChild::find()->where("parent = '{$id}' AND child = '{$module_name}_update'")->one()); ?>
                                <input type="hidden" name="update_<?= $mModel->id ?>" value="<?= $can_update ?>"/>
                                <div class="choice_selector <?php
                            if ($can_update) {
                                echo "active";
                            }
                                ?>"></div>
                            </td>
                            <td style="text-align: center;">
    <?php $can_delete = (int) !is_null(AuthItemChild::find()->where("parent = '{$id}' AND child = '{$module_name}_delete'")->one()); ?>
                                <input type="hidden" name="delete_<?= $mModel->id ?>" value="<?= $can_delete ?>"/>
                                <div class="choice_selector <?php
    if ($can_delete) {
        echo "active";
    }
    ?>"></div>
                            </td>
                        </tr>
<?php } ?>
                </tbody>
            </table>
<?php ActiveForm::end(); ?>
        </div>
    </section>
</div>
