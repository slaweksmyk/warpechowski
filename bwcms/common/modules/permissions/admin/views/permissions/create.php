<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\permissions\models\AuthItem */

$this->title = Yii::t('backend', 'module_permissions_create_role');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Auth Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-item-create">

    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?></header>
        <div class="panel-body" >
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </section>

</div>
