<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\permissions\models\AuthItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'module_permissions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-item-index">
    <p>
        <?= Html::a(Yii::t('backend', 'Create Role'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <?php if(Yii::$app->request->get('per-page')){ echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>"; } ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'name',
                    [
                        'label' => 'Liczba użytkowników',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return count(Yii::$app->authManager->getUserIdsByRole($data["name"]));
                        }
                    ],

                    [
                        'class' => 'common\hooks\yii2\grid\ActionColumn',
                        'template' => '{update} {users} {gpermissions} {delete}',
                        'buttons' => [
                            'gpermissions' => function ($url) {
                                return Html::a(
                                    '<span class="glyphicon glyphicon-list-alt"></span>',
                                    $url, 
                                    [
                                        'title' => 'Zmień uprawnienia',
                                        'data-pjax' => '0',
                                    ]
                                );
                            },
                            'users' => function ($url) {
                                return Html::a(
                                    '<span class="glyphicon glyphicon-user"></span>',
                                    $url, 
                                    [
                                        'title' => 'Lista użytkowników',
                                        'data-pjax' => '0',
                                    ]
                                );
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </section>   
</div>
