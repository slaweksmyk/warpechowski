<?php

/**
 * Permissions module
 * 
 * PHP version 7
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @copyright (c) 2016 Krzysztof Borecki
 * @version 1.0
 */

namespace common\modules\permissions\admin\controllers;

use Yii;
use common\modules\permissions\models\AuthItem;
use common\modules\permissions\models\AuthItemSearch;
use common\modules\permissions\models\AuthAssignment;
use common\modules\permissions\models\AuthAssignmentSearch;
use common\modules\modules\models\Module;
use common\modules\users\models\User;
use common\modules\logs\models\Log;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PermissionsController implements the CRUD actions for AuthItem model.
 */
class PermissionsController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Before load controller actions
     * 
     * @param string $action
     * @return boolean
     */
    public function beforeAction($action) {
        if (parent::beforeAction($action)) {
            $sActionType = strtolower(str_replace("action", "", $action->actionMethod));

            if ($sActionType == "index" OR $sActionType == "users")
                $sActionType = "view";
            if ($sActionType == "uedit" OR $sActionType == "gpermissions")
                $sActionType = "update";
            if ($sActionType == "udelete")
                $sActionType = "delete";

            if (!Yii::$app->user->can($this->module->id . "_{$sActionType}")) {
                return $this->redirect(Yii::$app->homeUrl);
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Lists all AuthItem models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new AuthItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->get('per-page')) {
            $dataProvider->setPagination(['pageSize' => Yii::$app->request->get('per-page')]);
        }

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all AuthItem models.
     * @return mixed
     */
    public function actionUsers($id) {
        $searchModel = new AuthAssignmentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where("item_name = '{$id}'");

        if (Yii::$app->request->get('per-page')) {
            $dataProvider->setPagination(['pageSize' => Yii::$app->request->get('per-page')]);
        }

        return $this->render('users', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AuthItem model.
     * @param string $id
     * @return mixed
     */
    public function actionUserpermissions($id) {
        $moduleModel = Module::find()->all();
        $model = User::find()->where("id = {$id}")->one();

        if (Yii::$app->request->post()) {
            $auth = Yii::$app->authManager;

            $type = explode("_", Yii::$app->request->post()["name"])[0];
            $moduleID = explode("_", Yii::$app->request->post()["name"])[1];

            $oModule = Module::getById($moduleID);

            $permission = $auth->getPermission(strtolower($oModule->name) . '_' . $type);

            if (is_null($permission)) {
                $permission = $auth->createPermission(strtolower($oModule->name) . "_" . $type);
                $permission->description = "Uprawnienia modułu: {$oModule->name}";
                $auth->add($permission);
            }

            if (Yii::$app->request->post()["val"] == 1) {
                $auth->assign($permission, $id);
            } else {
                $auth->revoke($permission, $id);
            }
        }

        return $this->render('upermissions', [
                    'id' => $id,
                    'moduleModel' => $moduleModel,
                    'model' => $model
        ]);
    }

    /**
     * Displays a single AuthItem model.
     * @param string $id
     * @return mixed
     */
    public function actionGpermissions($id) {
        $moduleModel = Module::find()->all();

        if (Yii::$app->request->post()) {
            $auth = Yii::$app->authManager;
            $role = $auth->getRole($id);

            if (Yii::$app->request->post()["name"] == "adminPanel") {
                $permission = $auth->getPermission('adminPanel');
            } else {
                $type = explode("_", Yii::$app->request->post()["name"])[0];
                $moduleID = explode("_", Yii::$app->request->post()["name"])[1];
                $oModule = Module::getById($moduleID);
                $permission = $auth->getPermission(strtolower($oModule->name) . '_' . $type);
            }

            if (is_null($permission)) {
                $permission = $auth->createPermission(strtolower($oModule->name) . "_" . $type);
                $permission->description = "Uprawnienia modułu: {$oModule->name}";
                $auth->add($permission);
            }

            if (Yii::$app->request->post()["val"] == 1) {
                $auth->addChild($role, $permission);
            } else {
                $auth->removeChild($role, $permission);
            }

            exit;
        }

        // Dopisanie uprawnień do usera
        /* $auth = Yii::$app->authManager;
          $permission = $auth->getPermission('permissions_view');
          $auth->assign($permission, 5); */

        // Dopisanie uprawnień do grupy
        /* $auth = Yii::$app->authManager;
          $role = $auth->getRole($id);
          $permission = $auth->getPermission('permissions_view');
          $auth->addChild($role, $permission);
         */

        /*
          $searchModel = new AuthAssignmentSearch();
          $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
          $dataProvider->query->where("item_name = '{$id}'");
         */


        return $this->render('gpermissions', [
                    'id' => $id,
                    'moduleModel' => $moduleModel,
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AuthItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new AuthItem();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['gpermissions', 'id' => $model->name]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AuthItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AuthItem model.
     * @param string $id
     * @return mixed
     */
    public function actionUedit($id) {
        $model = User::find()->where("id = {$id}")->one();
        $oUserRole = AuthAssignment::find()->where("user_id = {$id}")->one();

        if (Yii::$app->request->post()) {
            $newRole = Yii::$app->request->post()["AuthAssignment"]["item_name"];
            $oUserRole->item_name = $newRole;
            $oUserRole->save();
        }

        $oRolesRowset = AuthItem::find()->where("type = 1")->all();

        return $this->render('uedit', [
                    'model' => $model,
                    'oUserRole' => $oUserRole,
                    'oRolesRowset' => $oRolesRowset
        ]);
    }

    /**
     * Deletes an existing AuthItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing AuthItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionUdelete($id) {
        $model = AuthAssignment::find()->where(['id' => $id])->one();
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AuthItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return AuthItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = AuthItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
