<?php

namespace common\modules\permissions\admin;

/**
 * files module definition class
 */
class Permissions extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\permissions\admin\controllers';
    public $defaultRoute = 'permissions';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
