<?php
/**
 * Module Admin Pages - main model
 * 
 * PHP version 7
 *
 * @author Volodymyr Shelelo <contact@profficode.com>
 * @copyright (c) 2016 Volodymyr Shelelo
 * @version 1.0
 */

namespace common\modules\pages\models;

use Yii;
use common\modules\modules\models\Module;

/**
 * This is the model class for table "pages_admin".
 *
 * @property integer $id
 * @property string $created_at
 * @property string $updated_at
 * @property string $name
 * @property string $description
 * @property string $slug
 * @property string $url
 * @property integer $module_id
 * @property integer $is_active
 *
 * @property Modules $module
 */
class PagesAdmin extends \common\hooks\yii2\db\ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pages_admin}}';
    }

    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // db validation
            [['module_id', 'is_active','list_id'], 'integer'],
            [['description', 'url'], 'string'],
            [['name', 'slug'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            [['module_id'], 'exist', 'skipOnError' => true, 'targetClass' => Module::className(), 'targetAttribute' => ['module_id' => 'id']],
        
            // custom validation
            [['name', 'slug', 'description'], 'trim'],
            [['name', 'slug', 'is_active'], 'required'],
            
            // default values
            ['is_active', 'default', 'value' => 1],
            ['created_at', 'default', 'value' => date('Y-m-d H:i:s')],
        ];
    }

    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'name' => 'Name',
            'description' => 'Description',
            'slug' => 'Slug',
            'url' => 'Url',
            'module_id' => 'Module ID',
            'is_active' => 'Is Active',
        ];
    }

    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModule()
    {
        return $this->hasOne(Module::className(), ['id' => 'module_id']);
    }
    
    
    /**
     * Get Page by slug
     * 
     * @param string $slug
     * @return array
     */
    public function getBySlug($slug) 
    {
        return self::find()->where(['slug' => $slug])->one();
    }
    
    
    /**
     * Get Page by ID
     * 
     * @param int $id
     * @return array
     */
    public function getById($id)
    {
        return self::find()->where(['id' => $id])->one();
    }
    
    
    /**
     * Get last Page in DB
     * 
     * @return array
     */
    public function getLastRecord()
    {
        return self::find()->orderBy(['id' => SORT_DESC])->limit(1)->one();
    }
    
    
    /**
     * Page slug generation
     * 
     * @param string $name
     */
    public function slugCreate($name) 
    {
        $prepareURL =  \common\helpers\UrlHelper::prepareURL($name);
        
        if( self::getBySlug($prepareURL) ) {
            return $prepareURL.'1';
        } else {
            return $prepareURL;
        }
    }
    
    
    /**
     * Update page data
     * 
     * @return bool
     */
    public function pageUpdate()
    {
        $this->updated_at = date('Y-m-d H:i:s');
        $this->url = $this->slug; //new url
        return $this->save();
    }
    
    
    /**
     * Delete page in admin panel
     * 
     * @return boolean
     */
    public function pageDelete($protected_id)
    {
        if( in_array($this->id, $protected_id) ) {
            return false;
        } else {
            return $this->delete();
        }
    }
    
    public function isModuleEnabled(){
        return Module::findOne($this->module_id)->is_active;
    }
    
    
    /**
     * Get all modules list
     * 
     * @return array
     */
    public static function getModulesConfig()
    {

        $pages = self::getDb()->cache(function () { 
            return self::find()->select(['url', 'module_id'])->where(['is_active' => 1])->all();
        });
        $Module = new Module();
        $modules = array();
        
        foreach($pages as $page) {
            if( $page->module_id && $page->url !== '/' ) {
                $module = $Module->getById($page->module_id, array('admin'));
                $modules[$page->url] = [ 'class' => $module['admin'] ];
            }
        }
        
        return $modules;
    }
    
}
