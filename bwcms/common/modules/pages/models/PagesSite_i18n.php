<?php

namespace common\modules\pages\models;

use Yii;

/**
 * This is the model class for table "{{%pages_site_i18n}}".
 *
 * @property integer $id
 * @property string $page_site_id
 * @property string $language
 * @property string $name
 * @property string $description
 * @property string $seo_title
 * @property string $seo_keywords
 * @property string $seo_description
 *
 * @property PagesSite $pageSite
 */
class PagesSite_i18n extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pages_site_i18n}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_site_id'], 'integer'],
            [['description'], 'string'],
            [['language', 'name', 'seo_title', 'seo_keywords', 'seo_description'], 'string', 'max' => 255],
            [['page_site_id'], 'exist', 'skipOnError' => true, 'targetClass' => PagesSite::className(), 'targetAttribute' => ['page_site_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_site_id' => 'Page Site ID',
            'language' => 'Language',
            'name' => 'Name',
            'description' => 'Description',
            'seo_title' => 'Seo Title',
            'seo_keywords' => 'Seo Keywords',
            'seo_description' => 'Seo Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageSite()
    {
        return $this->hasOne(PagesSite::className(), ['id' => 'page_site_id']);
    }
}
