<?php
/**
 * Module Website Pages - main model
 * 
 * PHP version 7
 *
 * @author Volodymyr Shelelo <contact@profficode.com>
 * @copyright (c) 2016 Volodymyr Shelelo
 * @version 1.0
 */

namespace common\modules\pages\models;

use Yii;
use common\modules\modules\models\Module;
use common\modules\modules\models\ModuleRules;
use common\modules\pages\models\PagesSite_i18n;
use common\modules\categories\models\ArticlesCategories;

use creocoder\translateable\TranslateableBehavior;

/**
 * This is the model class for table "pages".
 *
 * @property integer $id
 * @property string $created_at
 * @property string $updated_at
 * @property string $name
 * @property string $description
 * @property string $slug
 * @property string $url
 * @property integer $module_id
 * @property integer $layout_id
 * @property integer $is_home
 * @property integer $is_active
 *
 * @property Modules $module
 */
class PagesSite extends \common\hooks\yii2\db\ActiveRecord
{
    
    protected $childs;
    private static $oConfig;
	private $params = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pages_site}}';
    }
    
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => ['name', 'description', 'slug', 'url', 'seo_title', 'seo_keywords', 'seo_description'],
                'translationLanguageAttribute' => 'language',
            ],
        ];
    }
	
	public function __construct(){
		$this->params = require(Yii::getAlias('@backend/config/main.php'));
	}
    
    /**
     * @inheritdoc
     */
    public function getTranslations()
    {
        return $this->hasMany(PagesSite_i18n::className(), ['page_site_id' => 'id']);
    }    

    /**
     * @inheritdoc
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_INSERT | self::OP_UPDATE,
        ];
    }  
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // db validation
            [['module_id', 'layout_id', 'is_home', 'is_active', 'article_category_id'], 'integer'],
            [['created_at', 'updated_at', 'description', 'url', 'seo_description'], 'string'],
            [['name', 'slug', 'seo_title', 'seo_keywords'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            [['module_id'], 'exist', 'skipOnError' => true, 'targetClass' => Module::className(), 'targetAttribute' => ['module_id' => 'id']],
            [['article_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ArticlesCategories::className(), 'targetAttribute' => ['article_category_id' => 'id']],
            
            // custom validation
            [['name', 'slug', 'description'], 'trim'],
            [['name', 'slug', 'layout_id'], 'required'],
            
            // default values
            ['is_active', 'default', 'value' => 1],
            ['is_home', 'default', 'value' => 0],
            ['created_at', 'default', 'value' => date('Y-m-d H:i:s')],
        ];
    }

    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'name' => 'Nazwa',
            'description' => 'Description',
            'slug' => 'Slug',
            'url' => 'Url',
            'module_id' => 'Module ID',
            'layout_id' => 'Layout ID',
            'is_home' => 'This is homepage',
            'is_active' => 'Is Active',
            'seo_title' => Yii::t('backend_module_articles', 'Seo title'),
            'seo_description' => Yii::t('backend_module_articles', 'Seo descrtiption'),
            'seo_keywords' => Yii::t('backend_module_articles', 'seo_keywords'),
        ];
    }

    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModule()
    {
        return $this->hasOne(Module::className(), ['id' => 'module_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleCategory()
    {
        return ArticlesCategories::getDb()->cache(function () { 
            return $this->hasOne(ArticlesCategories::className(), ['id' => 'article_category_id']);
        });
    }
    
    /**
     * Get Page by slug
     * 
     * @param string $slug
     * @return array
     */
    public static function getBySlug($slug) 
    {
        return self::find()->where(['slug' => $slug])->one();
    }

    
    /**
     * Get Page by ID
     * 
     * @param int $id
     * @return array
     */
    public function getById($id)
    {
        return self::find()->where(['id' => $id])->one();
    }

    
    /**
     * Get last Page in DB
     * 
     * @return array
     */
    public function getLastRecord()
    {
        return self::find()->orderBy(['id' => SORT_DESC])->limit(1)->one();
    }
    
    
    /**
     * Page slug generation
     * 
     * @param string $name
     */
    public static function slugCreate($name) 
    {
        $prepareURL =  \common\helpers\UrlHelper::prepareURL($name);
        
        if( self::getBySlug($prepareURL) ) {
            return $prepareURL.'1';
        } else {
            return $prepareURL;
        }
    }
    
    
    /**
     * Update page data
     * 
     * @return bool
     */
    public function pageUpdate()
    {
        $old_home = PagesSite::findOne([ 'is_home' => 1 ]);
        
         if( $old_home ){
            if( $old_home->id == $this->id ){
                $this->is_home = 1;
            } 
            elseif( $this->is_home == 1 ){
                $old_home->slug = PagesSite::slugCreate($old_home->name);
                $old_home->url = $old_home->slug;
                $old_home->is_home = 0;
                $old_home->update();
            }  
        } else { $this->is_home = 1; }
        
        if( $this->is_home == 1 ){ $this->slug = '/'; }
        
        $this->updated_at = date('Y-m-d H:i:s');
        $this->updated_user_id = \Yii::$app->user->identity->id;
        $this->url = $this->slug;
        return $this->save();
    }
    
    
    /**
     * Get Page by URL
     * 
     * @param array $parametrs
     * @return array
     */
    public static function getByUrl( $parametrs = array(), $url = false )
    {
        if( !$url ) { $url = Yii::$app->request->getUrl(); }

        // Check for add parameters
        $tCheck = explode("?", $url);
        if(count($tCheck) > 0){
            $url = $tCheck[0];
        }

        // Check for slug
        if(substr($url, -1) != "/"){
            $tmp = explode("/", $url);

            $slug = end($tmp);
            if(strpos($slug, ".html") === false){
                Yii::$app->params['slug'] = $slug;  
            }

            array_pop($tmp);
            $url = implode("/", $tmp);
        }

        // Remove first and last "/" in the Page URL
        if( strlen($url) > 1 ){
            if( $url{0} == '/' ){ $url = \substr($url, 1); }
            if( $url{strlen($url)-1} == '/' ) { $url = \substr($url,0,-1); }
        } else {
            $url = '/';
        }
        
        $page = null;
        self::$oConfig = Yii::$app->params['oConfig'];
        if(self::$oConfig->default_language != Yii::$app->session->get('frontend_language')){
            $sLang = strtolower(current(explode("-", Yii::$app->session->get('frontend_language'))));
            $url = str_replace("{$sLang}/", "", $url);
            
            if(Yii::$app->request->url == "/{$sLang}/"){
                $url = str_replace($sLang, "/", $url);
            }
        } else {
            if(!Yii::$app->request->referrer){
                $aUr = explode("/", $url);
                $sUrlLang = strtolower(current($aUr));
                $sCurrUrl = trim(str_replace(current($aUr)."/", "/", $url), "/");

                $config = yii\helpers\ArrayHelper::merge(
                    [], require Yii::getAlias('@common')."/config/i18n.php"
                );
                foreach($config["languages"] as $sLang){
                    $sShort = strtolower(current(explode("-", $sLang)));

                    if($sUrlLang == $sShort){
                        Yii::$app->language = $sLang;
                        Yii::$app->session->set('frontend_language', $sLang);
                        $page_i18n = PagesSite_i18n::find()->where(["=", "url", $sCurrUrl])->one();

                        $page = self::find()->leftJoin(Yii::$app->getDb()->tablePrefix.'pages_site_i18n', '`'.Yii::$app->getDb()->tablePrefix.'pages_site_i18n`.`page_site_id` = `'.Yii::$app->getDb()->tablePrefix.'pages_site`.`id`')->where([Yii::$app->getDb()->tablePrefix.'pages_site.id' => $page_i18n->page_site_id, 'is_active' => 1])->select($parametrs)->one();
                    }
                }
            }
        }

        if(is_null($page)){
            if( empty($parametrs) ) {
                $page = self::find()->where(['url' => $url, 'is_active' => 1])->one();
            } else {
                $page = self::find()->leftJoin(Yii::$app->getDb()->tablePrefix.'pages_site_i18n', '`'.Yii::$app->getDb()->tablePrefix.'pages_site_i18n`.`page_site_id` = `'.Yii::$app->getDb()->tablePrefix.'pages_site`.`id`')->where([Yii::$app->getDb()->tablePrefix.'pages_site_i18n.url' => $url, 'is_active' => 1])->select($parametrs)->one();
            }
        }

        $layout = \common\modules\layouts\models\PagesSiteLayout::findOne([ 'id' => $page['layout_id'] ]);

        return array( 'page' => $page, 'layout' => $layout );
    }
    

    /**
     * Get all URL config: rules and modules
     * 
     * @return array
     */
    public static function getUrlConfig()
    {
        $pages = self::find()->leftJoin(Yii::$app->getDb()->tablePrefix.'pages_site_i18n', '`'.Yii::$app->getDb()->tablePrefix.'pages_site_i18n`.`page_site_id` = `'.Yii::$app->getDb()->tablePrefix.'pages_site`.`id`')->select([''.Yii::$app->getDb()->tablePrefix.'pages_site_i18n.url', 'module_id'])->where(['is_active' => 1])->all();
        $Module = new Module(); 
        $ModuleRules = new ModuleRules();
        $modules = array(); $rules = array();

        self::$oConfig = Yii::$app->params['oConfig'];
        foreach($pages as $page) {
            if( $page->module_id && $page->url !== '/' ) {
                $one_module = $Module->getById($page->module_id, array('site'));
                $one_module_rules = $ModuleRules->getByModuleId($page->module_id);
            } else {
                $config = yii\helpers\ArrayHelper::merge(
                    [], require Yii::getAlias('@common')."/config/i18n.php"
                );
                
                $one_module = false; $one_module_rules = false;
                foreach($config["languages"] as $sLang){
                    $url = $page->url;
                    $sShort = strtolower(current(explode("-", $sLang)));
                    
                    if($sLang != self::$oConfig->default_language){
                        $url = "{$sShort}/".$url;
                        $url = str_replace("//", "/", $url);
                    }

                    $rules[$url] = 'site/index';
                }
            }
            if( $one_module ) {
                $modules[ 'module_id_'.$page->module_id ] = [ 'class' => $one_module['site'] ]; 
            }
            if( $one_module_rules ) {
                $rules = array_merge ($rules, self::generateModuleRules( 'module_id_'.$page->module_id, $one_module_rules, $page->url ));
            }
        }

        return array( 'rules' => $rules, 'modules' => $modules );
    }
    
    
    /**
     * Module rules generatin from actions
     * 
     * @param array $module_class
     * @param string $module_rules
     * @param string $page_url
     * @return array
     */
    static function generateModuleRules($module_class, $module_rules, $page_url)
    {
        $rules = array();
        foreach($module_rules as $row) {
            $rules[$module_class.'/'.$row['controller'].'/'.$row['action']] = 'error';
            
            if( $row['url_regexp'] ) {
                $rules[$page_url.'/'.$row['url_regexp']] = $module_class.'/'.$row['controller'].'/'.$row['action'];
            } else {
                $rules[$page_url] = $module_class.'/'.$row['controller'].'/'.$row['action'];
            }
        }
        
        return $rules;
    }

    public function __set($name, $value) {
        if(in_array($name, ['name', 'description', 'slug', 'url', 'seo_title', 'seo_keywords', 'seo_description'])){
            $this->translate(Yii::$app->session->get('backend_language'))->{$name} = $value;
        }
        
        parent::__set($name, $value);
    }
    
    public function __get($name) {
        if(in_array($name, ['name', 'description', 'slug', 'url', 'seo_title', 'seo_keywords', 'seo_description'])){
            if(strpos(Yii::$app->request->url, $this->params["homeUrl"]) !== false){
                $translatedAttr = $this->translate(Yii::$app->session->get('backend_language'))->{$name};
            } else {
                $translatedAttr = $this->translate(Yii::$app->session->get('frontend_language'))->{$name};
            }

            if($translatedAttr){
                return $translatedAttr;
            }
        }

        return parent::__get($name);
    }
    
}
