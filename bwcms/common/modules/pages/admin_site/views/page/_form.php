<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\modules\categories\models\ArticlesCategories;
use common\modules\lists\models\ArticlesList;
?>

<div class="pages-admin-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    $items = array();
    $params = ['prompt' => \Yii::t('backend', 'default')];
    foreach ($modules as $module) {
        $items[$module['id']] = $module['name'];
    }

    $la_items = array();
    $la_params = ['prompt' => '- ' . \Yii::t('backend', 'select') . ' -'];
    foreach ($layouts as $layout) {
        $la_items[$layout['id']] = $layout['name'];
    }
    ?>

    <div class="row" >
        <div class="col-12 col-sm-6 col-lg-3" >
            <?php echo $form->field($model, 'is_active')->dropDownList(array(0 => Yii::t('backend', 'no'), 1 => Yii::t('backend', 'yes')))->label(\Yii::t('backend', 'is_active')); ?>
        </div>
        <div class="col-12 col-sm-6 col-lg-3" >
            <?php echo $form->field($model, 'is_home')->dropDownList(array(0 => Yii::t('backend', 'no'), 1 => Yii::t('backend', 'yes')))->label(\Yii::t('backend_module_pages_site', 'module_pages_is_home')); ?>
        </div>
        <div class="col-12 col-sm-6 col-lg-3" >
            <?php echo $form->field($model, 'module_id')->dropDownList($items, $params)->label(\Yii::t('backend_module_pages_site', 'module_pages_type')); ?>
        </div>
        <div class="col-12 col-sm-6 col-lg-3" >
<?php echo $form->field($model, 'layout_id')->dropDownList($la_items, $la_params)->label(\Yii::t('backend_module_pages_site', 'module_pages_template')); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-6">
            <?php echo $form->field($model, 'name')->textInput(['maxlength' => true])->label(Yii::t('backend', 'Name')); ?>
        </div>
        <div class="col-3">
            <?php echo $form->field($model, 'slug')->textInput(['maxlength' => true])->label(Yii::t('backend', 'Slug')); ?>
        </div>
    </div>

        <?php echo $form->field($model, 'description')->textarea(['rows' => 6])->label(Yii::t('backend', 'Description')); ?>

    <div class="form-group">
    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']); ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
