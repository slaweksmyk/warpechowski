<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\pages\models\PagesSite */
/* @var $modules common\modules\modules\models\Module */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pages-admin-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'name')->textInput(['maxlength' => true])->label('Nazwa'); ?>

    <?php 
        $items = array();
        $params = [ 'prompt' => \Yii::t('backend', 'default') ];
        foreach ($modules as $module) { $items[$module['id']] = $module['name']; }
        
        $la_items = array();
        $la_params = [ 'prompt' => '- '.\Yii::t('backend', 'select').' -' ];
        foreach ($layouts as $layout) { $la_items[$layout['id']] = $layout['name']; }
    ?>
    <div class="row" >
        <div class="col-12 col-md-6" >
            <?php echo $form->field($model, 'module_id')->dropDownList($items, $params)->label(\Yii::t('backend_module_pages_site', 'module_pages_type')); ?>
        </div>
        <div class="col-12 col-md-6" >
            <?php echo $form->field($model, 'layout_id')->dropDownList($la_items, $la_params)->label(\Yii::t('backend_module_pages_site', 'module_pages_template')); ?>
        </div>
    </div>
    
    <?php $model->is_active = 1;  ?>
    <?php echo $form->field($model, 'is_active')->radioList( array( 1 => Yii::t('backend', 'yes'), 0 => Yii::t('backend', 'no') ) )->label(Yii::t('backend', 'is_active')); ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('backend', 'create') : Yii::t('backend', 'update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
