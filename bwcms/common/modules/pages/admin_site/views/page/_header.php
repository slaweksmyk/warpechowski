<?php
    use yii\helpers\Html;
    use common\modules\users\models\UserData;
?>
<style>
    #m_title { display: none; }
    section.title-area { padding-top: 0px; }
</style>

<section class="title-area" style="padding-top: 0px;">
    <h1><?php if($this->title){ echo $this->title; } else { echo Yii::t('backend', 'home_title'); } ?> <a href="#" title="przypnij moduł"></a></h1>
    <?php if( $model->url == "/" ){ $pageUrl = $baseUrl; } else { $pageUrl = $baseUrl.'/'.$model->url.'/'; } ?>
    <?= Html::a("Podgląd podstrony", $pageUrl, ['class' => 'btn btn-success pull-right', "target" => "_blank", "style" => "margin-top: 8px;"]) ?>
    <p><img src="<?= \Yii::$app->request->baseUrl ?>images/ico/time-ico.png" alt="time"/> <?= Yii::t('backend', 'last_edit') ?>: <?= date("H:i:s, d.m.Y", strtotime($model->updated_at)) ?> <?php if($model->updated_user_id){ ?> przez: <?= UserData::find()->where(["=", "user_id", $model->updated_user_id])->one()->firstname ?> <?= UserData::find()->where(["=", "user_id", $model->updated_user_id])->one()->surname ?><?php } ?></p>
    <p><b>Adres URL: <a href="<?= $pageUrl ?>" target="_blank"><?= $pageUrl ?></a></b></p>
</section>

<div class="row" style="margin-top: 10px;">
    <div class="col-5"><b>Zarządzanie stroną</b></div>
    <div class="col-3"></div>
</div>

<div class="row">
    <div class="col-12">
        <?php if(!Yii::$app->request->get("mid")){ $sClass = "btn-warning"; } else { $sClass = "btn-primary"; } ?>
        <?= Html::a("Zarządzaj ustawieniami głównymi", ['update', 'id' => $model->id], ['class' => "btn {$sClass}", 'style' => "margin-bottom: 10px;"]) ?>
        <?php foreach($aModuleRowset as $aModuleGroup){ ?>
            <?php if($aModuleGroup["module"]->id == Yii::$app->request->get("mid")){ $sClass = "btn-warning"; } else { $sClass = "btn-primary"; } ?>
            <?= Html::a($aModuleGroup["page_admin"]->name, \Yii::$app->request->baseUrl."pages-site/page/module-list/?pid=".$model->id."&mid=".$aModuleGroup["module"]->id."&".$aModuleGroup["params"], ['class' => "btn {$sClass}", 'style' => "margin-bottom: 10px;"]) ?>
        <?php } ?>
    </div>
</div>