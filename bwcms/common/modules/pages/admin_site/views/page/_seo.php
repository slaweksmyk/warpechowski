<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\pages\models\PagesSite */
/* @var $modules common\modules\modules\models\Module */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pages-admin-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="row">
        <div class="col-6">
            <?php echo $form->field($model, 'seo_title')->textInput(['maxlength' => true])->label(Yii::t('backend', 'Title')); ?>
        </div>
        <div class="col-6">
            <?php echo $form->field($model, 'seo_keywords')->textInput(['maxlength' => true])->label(Yii::t('backend', 'kaywords')); ?>
        </div>
        <div class="col-12">
            <?php echo $form->field($model, 'seo_description')->textarea()->label(Yii::t('backend', 'Description')); ?>
        </div>
    </div>
    
    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
