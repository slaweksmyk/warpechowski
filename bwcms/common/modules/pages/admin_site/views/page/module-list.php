<?php
 use common\hooks\yii2\grid\GridView;

    $sTitle = "";
    foreach($aModuleRowset as $aModuleGroup){
        if($aModuleGroup["module"]->id == Yii::$app->request->get("mid")){
            $sTitle = $aModuleGroup["page_admin"]->name;
        }
    }

    $this->title = "{$sTitle} strony - {$model->name}";
?>
<div class="pages-admin-update">
    
    <?= $this->render('_header', [
        'model' => $model,
        'baseUrl' => Yii::$app->request->getHostInfo(),
        'aModuleRowset' => $aModuleRowset
    ]) ?>

    <?php
        if(Yii::$app->request->get('mid') == 41){
            echo $this->render('_categories-qmenu', [

            ]);
        } else {
            echo '<h2 style="color: #797c87; font-size: 12px; font-weight: bold; padding-left: 20px;">'.$sTitle.'</h2>';
        }
    ?>
    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend_module_pages_site', 'module_pages_site_pages'); ?></header>
        <div class="panel-body" >
            <?php echo GridView::widget([
                'dataProvider' => $oActiveDataProvider,
                'columns' => $aColumns,
            ]); ?>
        </div>
    </section>

</div>