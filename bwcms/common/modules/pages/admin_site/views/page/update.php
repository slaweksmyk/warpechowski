<?php

$this->title = "Zarządzaj modułami strony - {$model->name}";

//add CSS
$this->registerCssFile('/bwcms/common/modules/layouts/admin/assets/css/template.css');

//add JS
$this->registerJsFile('/bwcms/common/modules/layouts/admin/assets/js/widgets.js', ['depends' => [yii\web\JqueryAsset::className()]]);

?>
<div class="pages-admin-update">

    <?= $this->render('_header', [
        'model' => $model,
        'baseUrl' => Yii::$app->request->getHostInfo(),
        'aModuleRowset' => $aModuleRowset
    ]) ?>
    
    
    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Edytuj dane</header>
        <div class="panel-body" >
            <?php echo $this->render('_form', [
                'model' => $model,
                'modules' => $modules,
                'layouts' => $layouts,
                'baseUrl' => $baseUrl
            ]); ?>
        </div>
    </section>
    
    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Dane SEO</header>
        <div class="panel-body" >
            <?php echo $this->render('_seo', [
                'model' => $model,
            ]); ?>
        </div>
    </section>
    
    <h1>Zarządzaj widgetami strony - <?= $model->name ?></h1>
    
    <section class="panel full" style="margin-top: 20px;">
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?php echo \Yii::t('backend', 'data'); ?></header>
        <div class="panel-body" >
            <?php echo $structure; ?>
        </div>
    </section>

</div>
