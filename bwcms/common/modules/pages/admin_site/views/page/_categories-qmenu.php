<?php
use yii\helpers\Html;
use common\modules\categories\models\ArticlesCategories;

use backend\helpers\ArticleCategoryHelper;
?>

<div class="row" style="margin-top: 10px;">
    <div class="col-5"><b>Lista kategorii artykułowych</b></div>
    <div class="col-3"></div>
</div>

<div class="row" style="margin-bottom: 10px;">
    <div class="col-9 cat-list-box">
        <div class="category-list">
            <?php
                $oMainCats = ArticlesCategories::find();
                $oMainCatsRest = ArticlesCategories::find()->where(["IS", "parent_id", NULL]);
                
                $aIDs = Yii::$app->request->get('id');
                if(is_array($aIDs)){
                    $oMainCats->where(["IN", "id", $aIDs]);
                    $oMainCatsRest->andWhere(["IN", "id", $aIDs]);
                }
                $oMainCats = $oMainCats->all();
                $oMainCatsRest = $oMainCatsRest->all();
            ?>
            
            <?php foreach($oMainCats as $oCategory){ ?>
                <?php if($oCategory->id == Yii::$app->request->get("id")){ ?>
                    <div class="btn-group">
                        <button type="button" class="btn <?php if($oCategory->id == \Yii::$app->request->get('id')){ echo "btn-warning"; } else { echo "btn-primary"; } ?>" onClick="window.location.href = \Yii::$app->request->baseUrl.'articles/article/articles/?id=<?= $oCategory->id ?>';"><?= $oCategory->name ?></button>
                        <?php if(count($oCategory->getCategories()->all()) > 0){ ?>
                            <button type="button" class="btn <?php if($oCategory->id == \Yii::$app->request->get('id')){ echo "btn-warning"; } else { echo "btn-primary"; } ?> dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="caret"></span>
                            </button>
                            <?= ArticleCategoryHelper::generateCategoryTreeAsTable($oCategory->id); ?>
                        <?php } ?>
                    </div>
                <?php } ?>
            <?php } ?>

            <?php foreach($oMainCatsRest as $oCategory){ ?>
                <?php if($oCategory->id != Yii::$app->request->get("id")){ ?>
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary" onClick="window.location.href = \Yii::$app->request->baseUrl.'articles/article/articles/?id=<?= $oCategory->id ?>';"><?= $oCategory->name ?></button>
                        <?php if(count($oCategory->getCategories()->all()) > 0){ ?>
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="caret"></span>
                            </button>
                            <?= ArticleCategoryHelper::generateCategoryTreeAsTable($oCategory->id); ?>
                        <?php } ?>
                    </div>
                <?php } ?>
            <?php } ?>
            <div class="list-close" title="Pokaż wszystkie kategorie" data-toggle="tooltip" data-placement="bottom">x</div>
        </div>
    </div>
    <div class="col-3 cat-list-button">
        <div class="btn-group" style="float: right;">
            <button type="button" class="btn btn-outline-secondary article-qmenu-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Pokaż wszystkie kategorie
                <span class="caret"></span>
            </button>
        </div>
    </div>
</div>