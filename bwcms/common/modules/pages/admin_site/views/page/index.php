<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel commom\modules\pages\models\PagesSiteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend_module_pages_site', 'module_pages_site_pages');
?>
<div class="pages-admin-index">
    
    <p>
        <?php echo Html::a(Yii::t('backend_module_pages_site', 'module_pages_site_add'), ['create'], ['class' => 'btn btn-success']); ?>
    </p>
    
    <?php if( $errorAction ) { ?>
        <div style="padding: 15px; background: #ffd4d4; border-radius: 7px; margin-bottom: 10px;" >
            <?php echo $errorAction['message']; ?>
        </div>
    <?php } ?>
    
    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend_module_pages_site', 'module_pages_site_pages'); ?></header>
        <div class="panel-body" >
            <?php echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'header' => Yii::t('backend', 'LP')
                    ],

                    [
                        'attribute' => 'id',
                        'contentOptions'=>['style'=>'width: 70px']
                    ],
                    'name',
                    [
                        'attribute' => 'url',
                        'label' => "Adres URL",
                        'format' => 'raw',
                        'value' => function ($data) {
                            $baseUrl = \common\helpers\UrlHelper::getBaseUrl();
                            if( $data->url == "/" ) { 
                                return "<a href='{$baseUrl}' target='_blank' >{$baseUrl}</a>"; 
                            } else { 
                                return "<a href='{$baseUrl}/{$data->url}/' target='_blank' >{$baseUrl}/{$data->url}/</a>";
                            } 
                        }
                    ],
                    [
                        'attribute' => 'is_active',
                        'label' => Yii::t('backend', 'is_active'),
                        'value' => function ($data) {
                            if( $data->is_active ){
                                return Yii::t('backend', 'yes');
                            } else {
                                return Yii::t('backend', 'no');
                            }
                        },
                        'contentOptions'=>['style'=>'width: 100px; text-align: center']
                    ],

                    // 'created_at',
                    // 'updated_at',
                    // 'description:ntext',
                    // 'slug',
                    [
                        'class' => 'common\hooks\yii2\grid\ActionColumn',
                        'template' => '{update} {view} {widgets} {delete}',
                        'contentOptions' => ['style' => 'width: 130px;'],
                        'buttons' => [
                            'widgets' => function ($url) {
                                return Html::a(
                                                '<span class="btn btn-warning-yellow btn-xs"><span class="glyphicon glyphicon-pushpin"></span></span>', $url, [
                                            'title' => Yii::t('backend', 'menu_widgets'),
                                            'data-toggle' => "tooltip",
                                            'data-placement' => "bottom"
                                                ]
                                );
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </section>
    
</div>
