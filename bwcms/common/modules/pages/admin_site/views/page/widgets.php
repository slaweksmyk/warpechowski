<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\layouts\models\PagesSiteLayout */

$this->title = \Yii::t('backend', 'menu_widgets').': '.$model->name;

//add CSS
$this->registerCssFile('/bwcms/common/modules/layouts/admin/assets/css/template.css');

//add JS
$this->registerJsFile('/bwcms/common/modules/layouts/admin/assets/js/widgets.js');
?>

<div class="pages-site-layout-view">

    <p>
        <?php echo Html::a('< '.$model->name, ['view', 'id' => $model->id], ['class' => 'btn btn-outline-secondary']) ?>
    </p>

    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?php echo \Yii::t('backend', 'data'); ?></header>
        <div class="panel-body" >
            <?php echo $structure; ?>
        </div>
    </section>

</div>
