<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\pages\models\PagesSite */
/* @var $modules common\modules\modules\models\Module */

$this->title = Yii::t('backend_module_pages_site', 'module_pages_admin_create');

?>

<p>
    <?= Html::a(Yii::t('backend_module_pages_site', 'back_to_pages_list'), ['index'], ['class' => 'btn btn-outline-secondary']) ?>
</p>

<div class="pages-admin-create">

    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend', 'update'); ?>  <?= Yii::t('backend', 'data'); ?></header>
        <div class="panel-body" >
            <?php echo $this->render('_form_create', [
                'model' => $model,
                'modules' => $modules,
                'layouts' => $layouts
            ]) ?>
        </div>
    </section>

</div>
