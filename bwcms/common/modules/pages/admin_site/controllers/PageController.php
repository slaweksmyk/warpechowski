<?php
/**
 * Module Website Pages - main BACKEND controller
 * 
 * PHP version 7
 *
 * @author Volodymyr Shelelo <contact@profficode.com>
 * @copyright (c) 2016 Volodymyr Shelelo
 * @version 1.0
 */

namespace common\modules\pages\admin_site\controllers;

use Yii;

use common\modules\pages\models\PagesAdmin;

use common\modules\pages\models\PagesSite;
use common\modules\pages\models\PagesSiteSearch;

use common\modules\layouts\models\PagesSiteLayout;
use common\modules\modules\models\Module;
use common\modules\menu\models\MenuItems;
use common\modules\widget\models\WidgetItems;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

/**
 * PagesAdminController implements the CRUD actions for PagesAdmin model.
 */
class PageController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    
    /**
     * Lists all PagesAdmin models.
     * 
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PagesSiteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
 
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'errorAction' => Yii::$app->request->get('errorAction', null)
        ]);
    }

    
    /**
     * Creates a new PagesAdmin model.
     * 
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PagesSite();
        $modules = new Module();
        $is_post = $model->load(Yii::$app->request->post());
        
        if ($is_post) {
            $is_have_page = $model->find()->select(['id'])->one();
            
            if( $is_have_page ){ $model->is_home = 0; } 
            else { $model->is_home = 1; }
            
            $model->slug = $model->slugCreate($model->name);
            $model->url = $model->slug;
            
            if ($model->validate()) { $save = $model->save(); } 
            else { $save = false; }
            
            $model->save();
            
            if ($save) { return $this->redirect(['update', 'id' => $model->id]); }
            
        }
        
        return $this->render('create', [
            'model' => $model,
            'modules' => $modules->getActiveModules(),
            'layouts' => \common\modules\layouts\models\PagesSiteLayout::find()->select(['id', 'name'])->all(),
        ]);
    }

    
    /**
     * Updates an existing PagesAdmin model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * 
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modules = new Module();
        $layout = PagesSiteLayout::findOne([ 'id' => $model->layout_id ]);
        
        $aModuleRowset = $this->getUsedModules($model);
  
        if($model->load(Yii::$app->request->post())) {
            if ($model->validate()) { $save = $model->pageUpdate(); } 
            else { $save = false; }
            if ($save) { 
                $this->updateMenu( $model->id );
                return $this->redirect(['update', 'id' => $model->id]); 
            }
        }

        return $this->render('update', [
            'model' => $model,
            'aModuleRowset' => $aModuleRowset,
            'modules' => $modules->getActiveModules(),
            'layouts' => \common\modules\layouts\models\PagesSiteLayout::find()->select(['id', 'name'])->all(),
            'baseUrl' => Yii::$app->request->getHostInfo(),
            'structure' => \Yii::$app->LayoutsHelper->getLayoutStructure($layout->layout, 'page', $layout->id, $id)
        ]);    
    }

    
    /**
     * Deletes an existing PagesAdmin model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * 
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        
        if( $model->is_home == 1 ){
            return $this->redirect(['index',
                'errorAction' => [ 'message' => '<h5>'.\Yii::t('backend_module_pages_site', 'module_pages_no_main_delete').'</h5>' ]
            ]);
        } else {
            $model->delete();
            return $this->redirect(['index']);
        }
    }
    
    
    /**
     * Widgets on the Page
     * 
     * @param int $id
     * @return mixed
     */
    public function actionWidgets($id)
    {
        $model = $this->findModel($id);
        $layout = PagesSiteLayout::findOne([ 'id' => $model->layout_id ]);
        
        return $this->render('widgets', [
            'model' => $model,
            'structure' => \Yii::$app->LayoutsHelper->getLayoutStructure($layout->layout, 'page', $layout->id, $id)
        ]);
    }
    
    public function actionModuleList($pid){
        $oPage = $this->findModel($pid);
        $oModule = Module::find()->where(["=", "id", Yii::$app->request->get("mid")])->one();
        
        $oObject = new $oModule->model();
        $aColumns = [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => Yii::t('backend', 'LP')
            ]
        ];
        
        $aLabels = $oObject->attributeLabels();
        foreach($oObject->getTableSchema()->columns as $oColumn){
            if($oColumn->type == "string" && strpos($oColumn->name, 'id') === false && isset($aLabels[$oColumn->name])){
                $aColumns[] = $oColumn->name;
            }
            
            if($oColumn->type == "smallint" && isset($aLabels[$oColumn->name])){
                $aColumns[] = [
                    "attribute" => $oColumn->name,
                    "value" => function($model, $key, $index, $column){
                        return ($model->{$column->attribute} > 0) ? Yii::t("backend", "Yes") : Yii::t("backend", "No");
                    }
                ];
            }
        }

        $aColumns[] = [
            'class' => 'common\hooks\yii2\grid\ActionColumn',
            'headerOptions' => ["width" => "100px"],
            'buttonOptions' => ["target" => "_blank"],
            'urlCreator' => function ($action, $model, $key, $index) {
                $oModule = Module::find()->where(["LIKE", "model", get_class($model)])->one();
                $oPageAdmin = PagesAdmin::find()->where(["=", "module_id", $oModule->id])->one();
                $oMod = new $oModule->admin(99);
                
                if($oModule->id == 55 && $action == "update"){
                    $action = "update-list";
                }
                
                if($oModule->id == 11 && $action == "view"){
                    $action = "update";
                }
                
                return "".\Yii::$app->request->baseUrl."{$oPageAdmin->url}/{$oMod->defaultRoute}/{$action}/?id={$key}";
            }
        ];
        
        $oObject = $oObject->find();
        $aIDs = [];
        $aGet = Yii::$app->request->get();
        foreach($aGet as $key => $val){
            if($aGet["mid"] == 55){
                if($key == "article_list_id"){
                    foreach($val as $id){
                        $aIDs[] = $id;
                    }
                }
            } else {
                if($key != "mid" && $key != "pid"){
                    if(is_array($val)){
                        foreach($val as $id){
                            $aIDs[] = $id;
                        }
                    } else if(strpos($key, "id") !== null){
                        $aIDs[] = $val;
                    }
                }
            }
        }

        if(is_array($aIDs)){
            $oObject->where(["IN", "id", $aIDs]);
        }
        
        $oActiveDataProvider = new ActiveDataProvider([
            'query' => $oObject,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('module-list', [
            'model' => $oPage,
            'aColumns' => $aColumns,
            'baseUrl' => Yii::$app->request->getHostInfo(),
            'aModuleRowset' => $this->getUsedModules($oPage),
            'oActiveDataProvider' => $oActiveDataProvider
        ]);
    }

    
    /**
     * Update website menu after - update page
     * 
     * @param int $page_id
     */
    protected static function updateMenu($page_id)
    {
        $menuItems = new MenuItems();
        $items = $menuItems->findAll([ 'page_id' => $page_id ]);

        if( !empty($items) ){
            foreach ($items as $item) {
                \common\modules\menu\admin\controllers\MenuController::menuUpdate( $item['menu_id'] );
            }
        }
    }

    /**
     * Finds the PagesAdmin model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * 
     * @param integer $id
     * @return PagesAdmin the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PagesSite::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    private function getUsedModules($model){
        $aModuleRowset = [];
        $oWidgetItemsRowset = WidgetItems::find()->where(["=", "page_id", $model->id])->all();
        foreach($oWidgetItemsRowset as $oWidgetItem){
            $oWidget = $oWidgetItem->getWidget()->one();
            $oModule = $oWidget->getModule()->one();
            if(!is_null($oModule)){
                $oPageAdmin = PagesAdmin::find()->where(["=", "module_id", $oModule->id])->one();
                $aData = unserialize($oWidgetItem->data);
                
                $aUnsetKeys = ["layout", "order", "limit", "limit_pc", "limit_tablet", "limit_mobile", "layout_enabled", "layout_tablet_enabled", "layout_mobile", "layout_mobile_enabled", "layout_tablet"];
                foreach($aUnsetKeys as $sKey){
                    unset($aData[$sKey]);
                }
                

                if(isset($aModuleRowset[$oModule->id]["params"])){
                    $aData = array_merge_recursive($aData, $aModuleRowset[$oModule->id]["params"]);
                } 
                
                if(is_array($aData)){
                    if(current($aData)){
                        if(is_array(current($aData))){
                            $aData[key($aData)] = array_unique(current($aData), SORT_REGULAR);
                        }
                    }

                    $aModuleRowset[$oModule->id] = [
                        "module" => $oModule,
                        "page_admin" => $oPageAdmin,
                        "page_site" => $model,
                        "params" => $aData
                    ];
                }
            }
        }
        
        foreach($aModuleRowset as $key => $aMod){
            $mParm = $aMod["params"];

            if(is_array($mParm)){
                $mParm = $mParm;
            } else {
                $mParm = [$mParm];
            }
            $aModuleRowset[$key]["params"] = urldecode(http_build_query($mParm));
        }
        
        return $aModuleRowset;
    }
    
}
