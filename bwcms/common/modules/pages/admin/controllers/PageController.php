<?php
/**
 * Module Admin Pages - main BACKEND controller
 * 
 * PHP version 7
 *
 * @author Volodymyr Shelelo <contact@profficode.com>
 * @copyright (c) 2016 Volodymyr Shelelo
 * @version 1.0
 */

namespace common\modules\pages\admin\controllers;

use Yii;

use common\modules\pages\models\PagesAdmin;
use common\modules\pages\models\PagesAdminSearch;
use common\modules\modules\models\Module;
use common\modules\logs\models\Log;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PagesAdminController implements the CRUD actions for PagesAdmin model.
 */
class PageController extends Controller
{
    
    /**
     * Protected pages id
     * 
     * @var array
     */
    protected $protected_id = [1, 2, 3, 4, 5, 6, 7];
    
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    
    /**
     * Lists all PagesAdmin models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PagesAdminSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        if(Yii::$app->request->get('per-page')){
            $dataProvider->setPagination(['pageSize' => Yii::$app->request->get('per-page')]);
        }
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'errorAction' => Yii::$app->request->get('errorAction', null)
        ]);
    }

    
    /**
     * Displays a single PagesAdmin model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    
    /**
     * Creates a new PagesAdmin model.
     * 
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PagesAdmin();
        $modules = new Module();
        $is_post = $model->load(Yii::$app->request->post());
        
        if ($is_post) {
            $model->slug = $model->slugCreate($model->name);
            $model->url = $model->slug;
            
            if ($model->validate()) { $save = $model->save(); } 
            else { $save = false; /* var_dump($model->errors); */ }

            if ($save) { return $this->redirect(['update', 'id' => $model->id]); }
        }
        
        return $this->render('create', [
            'model' => $model,
            'modules' => $modules->getActiveModules(true)
        ]);
    }

    
    /**
     * Updates an existing PagesAdmin model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * 
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modules = new Module();
        $is_post = $model->load(Yii::$app->request->post());
        
        if($is_post) {
            if ($model->validate()) {
                $save = $model->pageUpdate();
            } 
            else { $save = false; }

            if ($save) { return $this->redirect(['view', 'id' => $model->id]); }
        }
        
        if( in_array($id, $this->protected_id) ) { $protectedVal = true; } 
        else { $protectedVal = false; }
        
        return $this->render('update', [
            'model' => $model,
            'modules' => $modules->getActiveModules(true),
            'baseUrl' => Yii::$app->request->getHostInfo(),
            'protectedVal' => $protectedVal
        ]);    
    }
    

    /**
     * Deletes an existing PagesAdmin model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $thisPage = $this->findModel($id);
        
        if( $thisPage->pageDelete($this->protected_id) ){
            return $this->redirect(['index']);
        } else {
            return $this->redirect(['index',
                'errorAction' => ['id' => $id, 'name' => $thisPage['name']]
            ]);
        }
    }

    
    /**
     * Finds the PagesAdmin model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PagesAdmin the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PagesAdmin::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
}
