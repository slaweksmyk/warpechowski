<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\pages\models\PagesAdmin */
/* @var $modules common\modules\modules\models\Module */

$this->title = Yii::t('backend', 'module_pages_admin_update').$model->name;

?>
<div class="pages-admin-update">
    
    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend', 'update'); ?> <?= Yii::t('backend', 'data'); ?></header>
        <div class="panel-body" >
            <?php echo $this->render('_form', [
                'model' => $model,
                'modules' => $modules,
                'baseUrl' => $baseUrl,
                'protectedVal' => $protectedVal
            ]); ?>
        </div>
    </section>

</div>
