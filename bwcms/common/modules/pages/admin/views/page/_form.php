<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\pages\models\PagesAdmin */
/* @var $modules common\modules\modules\models\Module */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pages-admin-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php // echo $form->field($model, 'created_at')->textInput(['class' => 'datepicker']); ?>
    
    <p>
        <?php 
            echo "<b>ID</b>: {$model->id}";
            if( $model->created_at ){ echo "<b style='margin-left: 25px' >Dodano</b>: {$model->created_at}"; } 
            if( $model->updated_at ){ echo "<b style='margin-left: 25px' >Zaktualizowano</b>: {$model->updated_at}"; } 
        ?>
    </p>

    <?php if( $model->url == "/" ){ $pageUrl = $baseUrl.\Yii::$app->request->baseUrl; } else { $pageUrl = $baseUrl.\Yii::$app->request->baseUrl.$model->url.'/'; } ?>
    <p>
        <?php echo "<b>Adres URL:</b> <a href='{$pageUrl}' target='_blank' >{$pageUrl}</a>"; ?>
    </p>
  
    <?php 
        if( $protectedVal ) { 
            echo '<p><b>Aktywny</b>: Tak</p>';
        } else {
            echo $form->field($model, 'is_active')->radioList( array( 1 => Yii::t('backend', 'yes'), 0 => Yii::t('backend', 'no') ) )->label('Aktywny');
        } 
    ?>
    
    <?php
        if( $protectedVal ) {
            echo '<p><b>Rodzaj:</b> ';
            foreach ($modules as $module) { 
                if( $module['id'] == $model['module_id'] ) {
                    echo "{$module['name']}";
                }
            }
            echo '</p><br>';
        } else {
            $items = array();
            $params = [ 'prompt' => 'Domyślny' ];
            foreach ($modules as $module) { 
                $items[$module['id']] = $module['name']; 
            }
            echo $form->field($model, 'module_id')->dropDownList($items, $params)->label('Rodzaj');
        }
    ?>
    
    <?php echo $form->field($model, 'name')->textInput(['maxlength' => true])->label('Nazwa'); ?>
    
    <?php echo $form->field($model, 'slug')->textInput(['maxlength' => true])->label('Adres URL'); ?>

    <?php echo $form->field($model, 'description')->textarea(['rows' => 6])->label('Opis'); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'create') : Yii::t('backend', 'update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
