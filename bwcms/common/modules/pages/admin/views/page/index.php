<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\pages\models\PagesAdminSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'module_pages_admin');
?>
<div class="pages-admin-index">

    <p>
        <?php echo Html::a(Yii::t('backend', 'module_pages_admin_create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <?php if( $errorAction ) { ?>
        <div style="padding: 15px; background: #ffd4d4; border-radius: 7px; margin-bottom: 10px;" >
            <h5>
                <?= Yii::t('backend', 'module_pages_admin_error'); ?>:
                <b><?php echo $errorAction['name']; ?></b> <small>(ID <?php echo $errorAction['id']; ?>)</small>
            </h5>
        </div>
    <?php } ?>
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Pages list
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <?php if(Yii::$app->request->get('per-page')){ echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>"; } ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?php echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'header' => Yii::t('backend', 'LP')
                    ],

                    [
                        'attribute' => 'id',
                        'contentOptions'=>['style'=>'width: 70px']
                    ],
                    'name',
                    [
                        'attribute' => 'url',
                        'label' => "Adres URL", 
                        'format' => 'raw',
                        'value' => function ($data) {
                            $baseUrl = \common\helpers\UrlHelper::getBaseUrl();
                            if( $data->url == "/" ) { 
                                return "<a href='{$baseUrl}".\Yii::$app->request->baseUrl."' target='_blank' >{$baseUrl}</a>"; 
                            } else { 
                                return "<a href='{$baseUrl}".\Yii::$app->request->baseUrl."{$data->url}/' target='_blank' >{$baseUrl}".\Yii::$app->request->baseUrl."{$data->url}/</a>";
                            } 
                        }
                    ],
                    [
                        'attribute' => 'is_active',
                        'label' => Yii::t('backend', 'is_active').'?',
                        'value' => function ($data) {
                            if( $data->is_active ){
                                return Yii::t('backend', 'yes');
                            } else {
                                return Yii::t('backend', 'no');
                            }
                        },
                        'contentOptions'=>['style'=>'width: 100px; text-align: center']
                    ],

                    // 'created_at',
                    // 'updated_at',
                    // 'slug',
                    // 'module_id',
                    // 'description:ntext',

                    [
                        'class' => 'common\hooks\yii2\grid\ActionColumn',
                        'contentOptions'=>['style'=>'width: 74px; text-align: center']
                    ],
                ],
            ]); ?>
        </div>
    </section>
    
</div>
