<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\pages\models\PagesSite */
/* @var $modules common\modules\modules\models\Module */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pages-admin-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'name')->textInput(['maxlength' => true])->label('Nazwa'); ?>

    <?php 
        $items = array();
        $params = [ 'prompt' => 'Default' ];
        foreach ($modules as $module) { $items[$module['id']] = $module['name']; }
        echo $form->field($model, 'module_id')->dropDownList($items, $params)->label('Page type');
    ?>
    
    <?php $model->is_active = 1;  ?>
    <?php echo $form->field($model, 'is_active')->radioList( array( 1 => Yii::t('backend', 'yes'), 0 => Yii::t('backend', 'no') ) )->label(Yii::t('backend', 'is_active').'?'); ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('backend', 'create') : Yii::t('backend', 'update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
