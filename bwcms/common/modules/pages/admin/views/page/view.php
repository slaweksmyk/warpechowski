<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\modules\modules\models\Module;

/* @var $this yii\web\View */
/* @var $model common\modules\pages\models\PagesAdmin */

$this->title = Yii::t('backend', 'module_pages_admin_view').$model->name;

// Get module and action NAMES
$module_name = Yii::t('backend', 'default');
$action_name = "- non -";
if($model->module_id) {
    $modules = new Module();
    $module_page = $modules->getById($model->module_id);
    if( $module_page ) {
        $module_name = $module_page->name;
    }
}

// Get page active status
if( $model->is_active ){ $active_page = Yii::t('backend', 'yes'); } 
else { $active_page = Yii::t('backend', 'no'); }

// Page link generation
$baseUrl = \common\helpers\UrlHelper::getBaseUrl();
if( $model->url == "/" ) { 
    $page_link = "<a href='{$baseUrl}".\Yii::$app->request->baseUrl."' target='_blank' >{$baseUrl}".\Yii::$app->request->baseUrl."</a>"; 
} else { 
    $page_link = "<a href='{$baseUrl}".\Yii::$app->request->baseUrl."{$model->url}/' target='_blank' >{$baseUrl}".\Yii::$app->request->baseUrl."{$model->url}/</a>";
}
?>

<div class="pages-admin-view">

    <p>
        <?php echo Html::a(Yii::t('backend', 'update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php echo Html::a(Yii::t('backend', 'delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'delete_confirm'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend', 'module_pages_admin_page_data') ?></header>
        <div class="panel-body" >
            <?php echo DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'name',
                    'created_at',
                    'updated_at',
                    'slug',
                    [
                        'attribute' => 'url',
                        'label' => 'Page link',
                        'format' => 'raw',
                        'value' => $page_link
                    ],
                    [
                        'attribute' => 'module_id',
                        'label' => 'Page type',
                        'value' => $module_name
                    ],
                    [
                        'attribute' => 'is_active',
                        'label' => 'Page is active?',
                        'value' => $active_page
                    ],
                    [
                        'attribute' => 'description',
                        'format' => 'raw',
                    ],
                ],
            ]) ?>
        </div>
    </section>

</div>
