<?php

namespace common\modules\files\models;

use Yii;

/**
 * This is the model class for table "{{%files_category}}".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $name
 * @property integer $sort
 */
class FileCategory extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%files_category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parent_id', 'sort', 'is_new'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'name' => 'Name',
            'sort' => 'Sort',
        ];
    }
}
