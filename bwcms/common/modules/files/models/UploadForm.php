<?php
namespace common\modules\files\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;
    public $category_id;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, gif, jpeg, doc, docx, xls, xlsx, pdf, svg', "maxFiles" => 99],
            [['category_id', 'size'], 'integer'],
        ];
    }
    
    public function upload()
    {
        $sFilename = $this->generateUniqueName();
        $save = $this->imageFile->saveAs(Yii::getAlias("@root/upload/".date("Y-m-d")."/".$sFilename));

        if($save) {
            return $sFilename;
        } else {
            return false;
        }
    }
    
    protected function generateUniqueName() {
        $unique = false;
        $suffix = '';
        $i = 0;
        while ($unique === false) {
            $file_name = preg_replace("/[^a-zA-Z0-9.]/", "", $this->imageFile->baseName);
            $save_name = $file_name . $suffix . '.' . $this->imageFile->extension;

            $path = Yii::getAlias('@root/upload/').date("Y-m-d")."/";
            $directory = date("Y-m-d")."/";
            if(!file_exists($path)){
                $oldmask = umask(0);
                mkdir($path, 0777, true);
                umask($oldmask);
            }

            $sUniqueName = $this->prepareName($save_name);
            $sTest = $path.$sUniqueName;

            if($i == 10){ exit; }
            if(!file_exists($sTest)){
                return $sUniqueName;
            } else {
                $suffix = $suffix . '-';
            }
            $i++;
        }
    }

    protected function prepareName($sText) {
        $aReplacePL = array('ą' => 'a', 'ę' => 'e', 'ś' => 's', 'ć' => 'c', 'ó' => 'o', 'ń' => 'n', 'ż' => 'z', 'ź' => 'z', 'ł' => 'l', 'Ą' => 'A', 'Ę' => 'E', 'Ś' => 'S', 'Ć' => 'C', 'Ó' => 'O', 'Ń' => 'N', 'Ż' => 'Z', 'Ź' => 'Z', 'Ł' => 'L');
        $sText = str_replace(array_keys($aReplacePL), array_values($aReplacePL), $sText);
        $sText = str_replace("?", "", $sText);
        $sText = str_replace("„", "", $sText);
        $sText = str_replace("”", "", $sText);
        $sText = str_replace(' ', '-', strtolower($sText));
        //$sText = preg_replace('/[\-]+/', '-', $sText);

        return $sText;
    }
    
}