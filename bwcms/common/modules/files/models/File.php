<?php

namespace common\modules\files\models;

use Yii;



use yii\imagine\Image;
use Imagine\Image\Box;
/**
 * This is the model class for table "{{%files}}".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $name
 * @property string $filename
 * @property tinyint thumbnail
 * @property string $type
 * @property integer $size
 */
class File extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%files}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'size', 'is_crop'], 'integer'],
            [['name', 'filename', 'type'], 'string', 'max' => 255],
            [['data', 'description'], 'safe'],
            ['created_at', 'default', 'value' => date('Y-m-d H:i:s')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'category_id' => 'Kategoria',
            'name' => Yii::t('backend', 'name'),
            'filename' => Yii::t('backend', 'filename'),
            'type' => Yii::t('backend', 'type'),
            'size' => Yii::t('backend', 'size'),
        ];
    }
    
    public function getExtension(){
        $aTypes = explode("/", $this->type);
        return $aTypes[1];
    }
    
    
    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public static function getThumbnail($id, $width, $height, $type = "crop", $effect = null, $quality = 80) 
    { 
        $oThumbnail = parent::find()->where(['id' => $id])->one();
                    
        if(!$oThumbnail){
            return false;
        }
                  
        if( $oThumbnail->type == 'image/svg+xml'){
             return "/upload/{$oThumbnail->filename}";
        }
        $effect_safe = str_replace("#", "", $effect);
        $sFilePath = Yii::getAlias("@root/upload/{$oThumbnail->filename}");
        $aTmp = explode("/", $oThumbnail->filename);
        $oThumbnail->filename = end($aTmp);
        
        $sHashName = substr(md5("{$width}_{$height}_{$type}_{$effect_safe}_{$quality}"), -6);
        $aFileData = pathinfo($oThumbnail->filename);
        
        $sCachedFile = "/cache/img/{$aFileData["filename"]}-{$sHashName}.{$aFileData["extension"]}";

        $sReworkedPath = Yii::getAlias("@root{$sCachedFile}");

        if(!file_exists($sReworkedPath)){
            if(file_exists($sFilePath)){
                switch($type){
                    case "crop";
                        Image::thumbnail($sFilePath, $height, $width)->save($sReworkedPath, ['quality' => $quality]);
                        break;

                    case "matched";
                        Image::getImagine()->open($sFilePath)->thumbnail(new Box($height, $width))->save($sReworkedPath , ['quality' => $quality]);
                        break;
                }

                if(!is_null($effect)){
                    $image = yii\imagine\Image::getImagine();
                    $newImage = $image->open($sReworkedPath);

                    switch($effect){
                        case "grayscale":
                            $newImage->effects()->grayscale();
                            break;

                        case "negative":
                            $newImage->effects()->negative();
                            break;

                        default:
                            if(ctype_xdigit($effect_safe)){
                                $color = $newImage->palette()->color($effect);
                                $newImage->effects()->colorize($color);
                            }
                            break;

                    }
                    $newImage->save($sReworkedPath, ['quality' => $quality]);
                }
            }
        }
        
        return $sCachedFile;
    } 
    
}
