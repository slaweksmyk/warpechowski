$(document).ready(function(){
    var currentCategory;
    var target;

    $(document).keydown(function(e) {
        if(e.ctrlKey && e.keyCode === 65 || e.keyCode === 97) {
            $(".folder-element").addClass("open");
            e.preventDefault();
        }
        
        if(e.keyCode === 46){
            if(confirm("Czy chcesz usunąć wybrane elementy?")){
                $(".folder-element.open").each(function(){
                    if($(this).hasClass("single-file")){
                        deleteFile($(this).data("id"), $(this));
                    }
                    if($(this).hasClass("single-folder")){
                        deleteFolder($(this).data("id"), $(this));
                    }
                });
            }
            e.preventDefault();
        }
        
        if(e.keyCode === 13) {
            if($(':focus').hasClass("new-category-name")){
                changeCategoryName();
            }
        }
    });
    
    $("body").on("click", ".remove-category", function(){
        if(confirm("Czy chcesz usunąć wybrany folder wraz ze znajdującymi się w nim plikami?")){
            deleteFolder($(this).data("delete"), $(this).parent().parent());
            return false;
        }
    });
    
    $("body").on("click", ".remove-file", function(){
        if(confirm("Czy chcesz usunąć wybrany element?")){
            deleteFile($(this).data("delete"), $(this).parent().parent());
            return false;
        }
    });

    $("body").on("change", "select[name='order']", function(){
        getItems({
            category: currentCategory,
            order: $("select[name='order']").val(),
            search_category: $("input[name='search_category']").val(),
            search_file: $("input[name='search_file']").val()
        });
    });

    $("body").on("focusout", ".search-row input", function(){
        getItems({
            category: currentCategory,
            order: $("select[name='order']").val(),
            search_category: $("input[name='search_category']").val(),
            search_file: $("input[name='search_file']").val()
        });
    });
    
    $("body").on("click", "#remove-all", function(){
        $(".folder-element.open").each(function(){
            if($(this).hasClass("single-file")){
                deleteFile($(this).data("id"), $(this));
            }
            if($(this).hasClass("single-folder")){
                deleteFolder($(this).data("id"), $(this));
            }
        });
    });
    
    $("body").on("focusout", ".new-category-name", function(){
        changeCategoryName();
    });
    
    $("body").on("click", ".go-back", function(){
        getItems({
            category: $(this).data("back")
        });
        
        return false;
    });
    
    $("body").on("click", ".breadcrumb a", function(){
        getItems({
            category: $(this).data("category")
        });

        return false;
    });

    $("body").on("click", ".folder-element", function(){
        $(this).toggleClass("open");
    });

    $("body").on("dblclick", ".single-folder", function(){
        getItems({
            category: $(this).data("id")
        });
    });
    
    $("body").on("dblclick", ".single-file", function(){
        if(typeof target !== 'undefined'){
            $(target).val($(this).data("id")).change();
            $('#modal').modal('toggle');
        }
    });
    
    /* Folder drag&drop */
    var draggedFolder;
    $("body").on("dragstart", ".single-folder", function(e){
        draggedFolder = $(this);
    });
    
    $("body").on("dragover", ".single-folder", function(e){
        e.preventDefault();
    });

    $("body").on("drop", ".single-folder", function(e){
        if(draggedFolder.data("id") !== $(this).data("id")){
            changeParent(draggedFolder.data("id"), $(this).data("id"), $(this));
        }
        e.stopPropagation(); 
        e.preventDefault();
        return false;
    });
    
    $("body").on("click", "#create-files", function(){
        window.location.href = $("#homeDir").text()+"files/manager/create/?category="+currentCategory;
    });
    
    $("body").on("click", "#create-folder", function(){
        $.post($("#homeDir").text()+"files/manager/create-category-ajax/", { parentID: $(".folders-area").data("category") })
        .done(function(iCategoryID) {
            var returnHTML = "";
            returnHTML +='<div class="col-xs-2 folder-element single-folder new-folder open" data-url="./?category='+iCategoryID+'" data-id="'+iCategoryID+'">';
                returnHTML +='<img src="'+$("#homeDir").text()+'images/ico/folder.png" alt="Nowy folder" class="img-responsive"/>';
                returnHTML +='<p><input type="text" class="new-category-name" value="Nowy folder"/></p>';
                returnHTML +='<div class="row single-tools">';
                    returnHTML +='<a href="'+$("#homeDir").text()+'files/manager/update-category/?id='+iCategoryID+'" title="Edytuj" data-toggle="tooltip" data-placement="bottom">';
                        returnHTML +='<span class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></span>';
                    returnHTML +='</a>';
                    returnHTML +='<a href="'+$("#homeDir").text()+'files/manager/delete-category/?id='+iCategoryID+'" title="Usuń" data-toggle="tooltip" data-placement="bottom" data-pjax="0" data-confirm="Czy na pewno usunąć ten element?" data-method="post">';
                        returnHTML +='<span class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></span>';
                    returnHTML +='</a>';
                returnHTML +='</div>';
            returnHTML +='</div>';

            if($(".single-folder").length > 0){
                $(".single-folder").last().after(returnHTML);
            } else {
                $(".folders-desctop").prepend(returnHTML);
            }

            $(".new-category-name").focus();
            $(".new-category-name")[0].setSelectionRange(0, $(".new-category-name").val().length);
        });

        return false;
    });

    function initManager(){
        target = $("#target").data("element");
        $("#target").remove();
        
        getItems({
            category: $(".folders-area").data("category")
        });
    }

    function getItems(params){
        if(typeof params !== 'undefined'){
            if(typeof params.category !== 'undefined'){
                currentCategory = params.category;
            }
        }

        $.get($("#homeDir").text()+"files/manager/index/", params)
        .done(function( data ) {
            $(".folders-area").html(data);
        });
    }
    
    function changeParent(iCategoryID, iNewParentID, element){
        $.post($("#homeDir").text()+"files/manager/change-parent-category-ajax/", { cID: iCategoryID, parentID: iNewParentID })
        .done(function(data) {
            element.remove();
            location.reload();
        });
    }
    
    function changeCategoryName(){
        var newName = $(".new-category-name").val();
        var cID = $(".new-folder").data("id");
        
        $.post($("#homeDir").text()+"files/manager/rename-category-ajax/", { cID: cID, newName: newName })
        .done(function(data) {
            $(".new-folder").find("input").replaceWith("<p>"+newName+"</p>");
            $(".new-folder").removeClass("open").removeClass("new-folder");
        });
    }
    
    function deleteFile(id, element){
        $.post($("#homeDir").text()+"files/manager/delete-file-ajax/", { id: id })
        .done(function(data) {
            element.fadeOut();
        });
    }
    
    function deleteFolder(id, element){
        $.post($("#homeDir").text()+"files/manager/delete-category-ajax/", { id: id })
        .done(function(data) {
            element.fadeOut();
        });
    }
    
    initManager();

});