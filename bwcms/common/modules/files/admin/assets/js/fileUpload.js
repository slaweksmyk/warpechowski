$(document).ready(function(){
    
    var $filesInput = $('#add-files-input'),
        validFiles = [],
        valid_count = 0;
    
    var docsArray   = ["doc", "docx", "odt"];
    var excellArray = ["xls", "xlsx"];
    var zipArray    = ["rar", "zip"];
    
    $filesInput.on('change', function(){
        var error = "";
        
        validFiles = [];
        valid_count = 0;

        for (var i = 0; i <  $filesInput.get(0).files.length; i++){
            var file = $filesInput.get(0).files[i];
            var extension = file.name.split('.').pop();
            
            error = "";
            processFile(file, extension, error);
        }
    });
	
	
    $('#w0').submit(function(event){
        var $this = $(this);
        var scriptURI = $(this).attr("action");
        var inc = 0;
		
        $.each(validFiles, function(i, file){
            var formData = new FormData();
            var progressObject = $("<progress></progress>").addClass("file-"+inc);
            var element  = $("#imagesAddedBox").find(".add-file-"+inc);

            formData.append("UploadForm[imageFile]", file);
            formData.append("UploadForm[category_id]", $("#categories").val());
            formData.append("uploadIndex", inc);
            
            element.children(".content").append(progressObject);
            inc++;

            $.ajax({
                type: 'POST',
                url: scriptURI,
                xhr: function() {
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){
                        myXhr.upload.addEventListener('progress',function(e){
                            progressObject.attr({value:e.loaded,max:e.total});
                                       
                            if(e.loaded  === e.total){
                                progressObject.css("backgroundColor", "green");
                            }
                        }, false);
                    }
                    return myXhr;
                },
                async: true,
                success: completeHandler,
                data: formData,
                cache: false,
                contentType: false,
                processData: false
            });
        });
        event.stopImmediatePropagation();
        return false;
    });
    
    function processFile(file, extension, error){

        if(file.type.match('image.*')) {
            var path = (window.URL || window.webkitURL).createObjectURL(file);

            if (file.size > maxImageSize) {
                error = "Przekroczony rozmiar zdjęcia!";
            }

             if(error === ""){ 
                 validFiles.push(file);
                 addFileHTML(file.name, file.size, valid_count++, path, error);
             } else {
                 addFileHTML(file.name, file.size, "none", path, error);
             }	
        } else {
            if (file.size > maxOtherSize) {
                error = "Przekroczony rozmiar pliku!";
            }
            
            if($.inArray(extension, docsArray) !== -1){
                if(error === ""){ 
                    validFiles.push(file); 
                    addFileHTML(file.name, file.size, valid_count++, '/bwcms/common/modules/files/admin/assets/images/docx.png', error);
                } else {
                    addFileHTML(file.name, file.size, "none", '/bwcms/common/modules/files/admin/assets/images/docx.png', error);
                }
            }

            if($.inArray(extension, excellArray) !== -1){
                if(error === ""){ 
                    validFiles.push(file); 
                    addFileHTML(file.name, file.size, valid_count++, '/bwcms/common/modules/files/admin/assets/images/xlsx.png', error);
                } else {
                    addFileHTML(file.name, file.size, "none", '/bwcms/common/modules/files/admin/assets/images/xlsx.png', error);
                }
            }

            if($.inArray(extension, zipArray) !== -1){
                if(error === ""){ 
                    validFiles.push(file);
                    addFileHTML(file.name, file.size, valid_count++, '/bwcms/common/modules/files/admin/assets/images/rar.png', error);
                } else {
                    addFileHTML(file.name, file.size, "none", '/bwcms/common/modules/files/admin/assets/images/rar.png', error);
                }
            }

            if(extension === "pdf"){
                if(error === ""){ 
                    validFiles.push(file); 
                    addFileHTML(file.name, file.size, valid_count++, '/bwcms/common/modules/files/admin/assets/images/pdf.png', error);
                } else {
                    addFileHTML(file.name, file.size, "none", '/bwcms/common/modules/files/admin/assets/images/pdf.png', error);
                }
            }
        }

    }

    function completeHandler(data){
        var obj = jQuery.parseJSON(data);
        
        $(".file-"+obj.iUploadIndex).after("<a href='"+$("#homeDir").text()+"files/manager/update/?category=&id="+obj.iFileID+"' target='_blank' style='font-size: 12px;'>Przejdź do pliku</a>");
    }

    // Get file ext
    function getFileType($name, $pos){
        return $name.substr($name.length - $pos);
    }
	
    //File bytes to KB, MB, GB, TB
    function bytesToSize(bytes) {
        var sizes = ['B', 'KB', 'MB', 'GB', 'TB'];
        if (bytes === 0) return '0 Byte';
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
    };
	   
    function addFileHTML(file_name, file_size, valid_count, path, error){
        var imgHtml  = '<div class="col-xs-12 col-sm-6 col-lg-4 upload-cnt add-file-'+ valid_count +'" >';
                imgHtml += '<div class="img-body" ><img src="'+ path +'" alt="img" /></div>';
                    imgHtml += '<div class="content">';
                    imgHtml += '<div class="img-name" >'+ file_name +'</div>';
                    imgHtml += '<div class="img-size" >'+ bytesToSize(file_size) +'</div>';
                    imgHtml += '<div class="img-name" style="color: red; font-size: 10px;" >'+ error +'</div>';
                imgHtml += '</div>';
                imgHtml += '<div class="upload-remove"><span class="btn btn-danger btn-xs" title="Usuń z listy"><span class="glyphicon glyphicon-trash"></span></span></div>';
            imgHtml += '</div>';

        $('#imagesAddedBox').append(imgHtml);
    }
    
    $("body").on('click', ".upload-remove", function(){
        var el = $(this).parent();
        $(this).parent().fadeOut(function(){
            var newValidFiles = [];
            $.each(validFiles, function(i, file){
                if(file.name != el.find(".img-name").text()){
                    newValidFiles.push(file);
                }
            });
            
            var currentNo = parseInt(el.attr("class").substr(el.attr("class").length - 1));
            $(".upload-cnt").each(function(){
                var thisNo = parseInt($(this).attr("class").substr($(this).attr("class").length - 1));
                if(thisNo > currentNo){
                    $(this).removeClass("add-file-"+thisNo);
                    $(this).addClass("add-file-"+(thisNo-1));
                }
            });
            
            validFiles = newValidFiles;
            el.remove();
        });
    });
	
    $(".drag-upload").bind("drop", function(e) {
        var files = e.originalEvent.dataTransfer.files;
        
        var error = "";
        $.each(files, function(i, file){
            var extension = file.name.split('.').pop();
            error = "";
            processFile(file, extension, error);
        });

        $(this).find("p").text("Upuść pliki w dowolnym miejscu w tym obszarze, aby wysłać je na serwer");
        return false;
    });
    
    $(".drag-upload").bind("dragenter", function(e) {
        $(this).find("p").text("Upuść pliki tutaj!");
    });
    
    $(".drag-upload").bind("dragleave", function(e) {
        $(this).find("p").text("Upuść pliki w dowolnym miejscu w tym obszarze, aby wysłać je na serwer");
    });
    
});