<?php

namespace common\modules\files\admin;

/**
 * files module definition class
 */
class Files extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\files\admin\controllers';
    public $defaultRoute = 'manager';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
