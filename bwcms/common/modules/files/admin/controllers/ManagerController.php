<?php

namespace common\modules\files\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use common\modules\files\models\File;
use common\modules\files\models\FileSearch;
use common\modules\files\models\FileCategory;
use common\modules\files\models\FileCategorySearch;
use common\modules\files\models\UploadForm;
use common\modules\logs\models\Log;
use common\modules\articles\models\ArticleImage;

/**
 * ManagerController implements the CRUD actions for File model.
 */
class ManagerController extends Controller
{

    private $aBreadcrump = [];

    public function actionGetImageFilesForMce($key)
    {
        if (Yii::$app->request->isAjax && !empty($key)) {
            $oFileRowset = File::find()->where(["!=", "type", "application/pdf"])->andWhere(['LIKE', 'name', $key])->limit(20)->all();

            $aTypeItems = [];
            foreach ($oFileRowset as $index => $oFile) {
                $aTypeItems[$index]["id"] = json_encode([$oFile->filename, $oFile->name]);
                $aTypeItems[$index]["text"] = '<img src="' . File::getThumbnail($oFile->id, 40, 40, "matched") . '" style="width: 40px;"> ' . $oFile->name;
            }

            $aReturnArr = [];
            $aReturnArr["total_count"] = count($oFileRowset);
            $aReturnArr["items"] = array_filter($aTypeItems);

            echo json_encode(array_filter($aReturnArr));
            exit;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetPdfFilesForMce($key)
    {
        if (Yii::$app->request->isAjax && !empty($key)) {
            $oFileRowset = File::find()->where(["=", "type", "application/pdf"])->andWhere(['LIKE', 'name', $key])->limit(20)->all();

            $aTypeItems = [];
            foreach ($oFileRowset as $index => $oFile) {
                $aTypeItems[$index]["id"] = json_encode([$oFile->filename, $oFile->name]);
                $aTypeItems[$index]["text"] = $oFile->name;
            }

            $aReturnArr = [];
            $aReturnArr["total_count"] = count($oFileRowset);
            $aReturnArr["items"] = array_filter($aTypeItems);

            echo json_encode(array_filter($aReturnArr));
            exit;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetCategoriesAjax($query)
    {
        if (Yii::$app->request->isAjax && !empty($query)) {
            $oCategoryRowset = FileCategory::find()->where(["LIKE", "name", $query])->all();

            $aCategoryItems = [];
            foreach ($oCategoryRowset as $index => $oCategory) {
                $aCategoryItems[$index]["id"] = $oCategory->id;
                $aCategoryItems[$index]["text"] = $oCategory->name;
            }

            $aReturnArr = [];
            $aReturnArr["total_count"] = count($oCategoryRowset);
            $aReturnArr["items"] = array_filter($aCategoryItems);

            echo json_encode(array_filter($aReturnArr));
            exit;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionChangeParentCategoryAjax()
    {
        if (Yii::$app->request->isAjax) {
            $oCategory = FileCategory::find()->where(["=", "id", \Yii::$app->request->post('cID', null)])->one();
            $oCategory->parent_id = \Yii::$app->request->post('parentID', null);
            $oCategory->save();

            exit;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionRenameCategoryAjax()
    {
        if (Yii::$app->request->isAjax) {
            $oCategory = FileCategory::find()->where(["=", "id", \Yii::$app->request->post('cID', null)])->one();
            $oCategory->name = \Yii::$app->request->post('newName', null);
            $oCategory->save();

            Log::addRecord($this->module->id, "create", "Folder: " . $oCategory->name, null);
            exit;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionCreateCategoryAjax()
    {
        if (Yii::$app->request->isAjax) {
            $oCategory = new FileCategory();
            $oCategory->parent_id = \Yii::$app->request->post('parentID', null);
            $oCategory->name = "Nowy folder";
            $oCategory->save();

            echo $oCategory->id;
            exit;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDeleteFileAjax()
    {
        if (Yii::$app->request->isAjax) {
            $oFile = File::find()->where(["=", "id", \Yii::$app->request->post('id', null)])->one();

            if (file_exists(Yii::getAlias("@root/upload/{$oFile->filename}"))) {
                unlink(Yii::getAlias("@root/upload/{$oFile->filename}"));
            }
            Log::addRecord($this->module->id, "delete", "Plik: " . $oFile->name, null);
            $oFile->delete();

            exit;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDeleteCategoryAjax()
    {
        if (Yii::$app->request->isAjax) {
            $oCategory = FileCategory::find()->where(["=", "id", \Yii::$app->request->post('id', null)])->one();

            $oFileRowset = File::find()->where(["=", "category_id", $oCategory->id])->all();
            foreach ($oFileRowset as $oFile) {
                if (file_exists(Yii::getAlias("@root/upload/{$oFile->filename}"))) {
                    unlink(Yii::getAlias("@root/upload/{$oFile->filename}"));
                }
                Log::addRecord($this->module->id, "delete", "Plik: " . $oFile->name, null);
                $oFile->delete();
            }

            Log::addRecord($this->module->id, "delete", "Folder: " . $oCategory->name, null);
            $oCategory->delete();

            exit;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Lists all File models.
     * @return mixed
     */
    public function actionIndex()
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $iCategoryLimit = 1000;
        $iFileLimit = 50;

        $searchModel = new FileCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        $oCategory = null;
        $iCurrentCategory = null;
        $sSearchCategory = null;
        $sSearchFile = null;

        if (Yii::$app->request->isAjax && Yii::$app->request->isGet) {
            $iCurrentCategory = Yii::$app->request->get('category', null);
            $sSearchCategory = Yii::$app->request->get('search_category', null);
            $sSearchFile = Yii::$app->request->get('search_file', null);

            if (!is_null($iCurrentCategory) && $iCurrentCategory !== "") {
                $oCategory = FileCategory::find()->where(["=", "id", $iCurrentCategory])->one();

                if ($sSearchCategory) {
                    $oCategoryRowset = FileCategory::find()->where(["LIKE", "name", $sSearchCategory])->limit($iCategoryLimit)->orderBy("sort")->all();
                } else {
                    $oCategoryRowset = FileCategory::find()->where(["=", "parent_id", $iCurrentCategory])->limit($iCategoryLimit)->orderBy("sort")->all();
                }

                if ($sSearchFile) {
                    $oFileRowset = File::find()->where(["OR",
                                ["LIKE", "name", $sSearchFile],
                                ["LIKE", "filename", $sSearchFile]
                            ])->orderBy(Yii::$app->request->get('order', "id DESC"))->limit($iFileLimit)->all();
                } else {
                    $oFileRowset = File::find()->where(["=", "category_id", $iCurrentCategory])->limit($iFileLimit)->orderBy(Yii::$app->request->get('order', "id DESC"))->all();
                }
            } else {
                if ($sSearchCategory) {
                    $oCategoryRowset = FileCategory::find()->where(["LIKE", "name", $sSearchCategory])->limit($iCategoryLimit)->orderBy("sort")->all();
                } else {
                    $oCategoryRowset = FileCategory::find()->where(["IS", "parent_id", NULL])->limit($iCategoryLimit)->orderBy("sort")->all();
                }

                if ($sSearchFile) {
                    $oFileRowset = File::find()->where(["OR",
                                ["LIKE", "name", $sSearchFile],
                                ["LIKE", "filename", $sSearchFile]
                            ])->limit($iFileLimit)->all();
                } else {
                    $oFileRowset = File::find()->where(["IS", "category_id", NULL])->limit($iFileLimit)->all();
                }
            }
        } else {
            $oCategoryRowset = FileCategory::find()->where(["IS", "parent_id", NULL])->limit($iCategoryLimit)->orderBy("sort")->all();
            $oFileRowset = File::find()->where(["IS", "category_id", NULL])->limit($iFileLimit)->all();
        }

        if ($sSearchCategory) {
            $oFileRowset = [];
        } else if ($sSearchFile) {
            $oCategoryRowset = [];
        }

        $aParams = [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'oCategoryRowset' => $oCategoryRowset,
            'oFileRowset' => $oFileRowset,
            'iCurrentCategory' => $iCurrentCategory,
            'oCategory' => $oCategory,
            'aBreadcrumps' => $this->prepareBreadCrupms($oCategory),
            'sTarget' => Yii::$app->request->post('target')
        ];

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('/folders/content', $aParams);
        } else {
            return $this->render('/folders/index', $aParams);
        }
    }

    private function prepareBreadCrupms($oCategory)
    {
        if (!is_null($oCategory)) {
            $this->generateBreadCrump($oCategory);
        }

        $k = array_keys($this->aBreadcrump);
        $v = array_values($this->aBreadcrump);
        $rk = array_reverse($k);
        $rv = array_reverse($v);

        $this->aBreadcrump = [null => "Ekran startowy"] + array_combine($rk, $rv);

        return $this->aBreadcrump;
    }

    private function generateBreadCrump($oCategory)
    {
        if (!is_null($oCategory)) {
            $this->aBreadcrump[$oCategory->id] = $oCategory->name;

            if (!is_null($oCategory->parent_id)) {
                $oParentCategory = FileCategory::find()->where(["=", "id", $oCategory->parent_id])->one();

                $this->generateBreadCrump($oParentCategory);
            }
        }
    }

    /**
     * Widget Module for AJAX load in another controller etc. Example is in main.js - fileManager()
     * @return mixed
     */
    public function actionWidget()
    {
        $searchModel = new FileCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->renderAjax('/widget/index', [
        ]);

//         $model = File::find()->all();
//         
//            return $this->render('_form', [
//                        'model' => $model
//            ]);
    }

    /**
     * Lists all File models.
     * @return mixed
     */
    public function actionFiles($id = null)
    {

        $searchModel = new FileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (!is_null($id)) {
            $dataProvider->query->where(["category_id" => $id]);
        }


        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('/widget/items/file_list', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
            ]);
        } else {
            return $this->render('files', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single File model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new File model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UploadForm();

        if (Yii::$app->request->isPost) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            $filename = $model->upload();

            if ($filename) {
                $info = pathinfo($model->imageFile->name);

                foreach (explode(",", Yii::$app->request->post('UploadForm')["category_id"]) as $iCategory) {
                    if ($iCategory == 0) {
                        $oCategory = FileCategory::find()->where(["LIKE", "name", Yii::$app->user->identity->username])->one();

                        if (is_null($oCategory)) {
                            $oCategory = new FileCategory();
                            $oCategory->parent_id = 54;
                            $oCategory->name = Yii::$app->user->identity->username;
                            $oCategory->save();
                        }
                        $iCategory = $oCategory->id;
                    }

                    $oFile = new File();
                    $oFile->created_at = date("Y-m-d H:i:s");
                    $oFile->category_id = $iCategory;
                    $oFile->name = $info['filename'];
                    $oFile->filename = date("Y-m-d") . "/" . $filename;
                    $oFile->type = $model->imageFile->type;
                    $oFile->size = $model->imageFile->size;
                    $oFile->save();

                    Log::addRecord($this->module->id, "create", "Plik: " . $info['filename'], null);
                }
            }
            echo json_encode(["iFileID" => $oFile->id, "iUploadIndex" => Yii::$app->request->post('uploadIndex')]);
            exit;
        }

        return $this->render('file_create', [
                    'model' => $model,
                    'maxUpload' => $this->maxFileUploadSize(),
                    'maxUploadRaw' => $this->maxFileUploadSize(false),
        ]);
    }

    public function actionCreateCategory()
    {
        $model = new FileCategory();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('category_create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing File model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateCategory($id)
    {
        $model = FileCategory::find()->where(["=", "id", $id])->one();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('category_update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing File model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->updated_at = date("Y-m-d H:i:s");
            $model->save();

            return $this->redirect(['files', 'id' => $model->category_id]);
        } else {
            return $this->render('file_update', [
                        'model' => $model,
            ]);
        }
    }

    public function actionDeleteCategory($id)
    {
        $oCategory = FileCategory::find()->where(["=", "id", $id])->one();

        $oFileRowset = File::find()->where(["=", "category_id", $oCategory->id])->all();
        foreach ($oFileRowset as $oFile) {
            if (file_exists(Yii::getAlias("@root/upload/{$oFile->filename}"))) {
                unlink(Yii::getAlias("@root/upload/{$oFile->filename}"));
            }
            $oFile->delete();
        }

        $oCategory->delete();

        return $this->redirect(['index', 'category' => \Yii::$app->request->get('category', null)]);
    }

    /**
     * Deletes an existing File model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $oFile = File::find()->where(["=", "id", $id])->one();

        if (file_exists(Yii::getAlias("@root/upload/{$oFile->filename}"))) {
            unlink(Yii::getAlias("@root/upload/{$oFile->filename}"));
        }
        $oFile->delete();

        return $this->redirect(['index', 'category' => \Yii::$app->request->get('category', null)]);
    }

    /**
     * Finds the File model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return File the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = File::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function maxFileUploadSize($formated = true)
    {
        static $max_size = -1;

        if ($max_size < 0) {
            $post_max_size = $this->parseSize(ini_get('post_max_size'));
            if ($post_max_size > 0) {
                $max_size = $post_max_size;
            }

            $upload_max = $this->parseSize(ini_get('upload_max_filesize'));
            if ($upload_max > 0 && $upload_max < $max_size) {
                $max_size = $upload_max;
            }
        }
        if ($formated) {
            return $this->formatBytes($max_size);
        } else {
            return $max_size;
        }
    }

    private function parseSize($size)
    {
        $unit = preg_replace('/[^bkmgtpezy]/i', '', $size);
        $size = preg_replace('/[^0-9\.]/', '', $size);
        if ($unit) {
            return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
        } else {
            return round($size);
        }
    }

    private function formatBytes($size, $precision = 2)
    {
        $unit = ['B', 'KB', 'MB', 'GB', 'TB'];

        for ($i = 0; $size >= 1024 && $i < count($unit) - 1; $i++) {
            $size /= 1024;
        }

        return round($size, $precision) . $unit[$i];
    }

}
