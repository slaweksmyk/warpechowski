<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;

use common\modules\files\models\FileCategory;

$this->title = "Menadżer plików";
?>
<div class="file-index">
    <p>
        <?= Html::a("< Wróć do listy kategorii", ['index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a("Dodaj plik", ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <?php if(Yii::$app->request->get('per-page')){ echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>"; } ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'format' => 'raw',
                        'contentOptions'=>['style'=>'width: 100px;'],
                        'value' => function ($data) {
                            return "<div style='height: 60px; width: 60px; display: block; margin: 0 auto; background: url(/upload/".$data["filename"].") center; background-size: cover;'></div>";
                        }
                    ],
                    'id',
                    'name',
                    'filename',
                    [
                        'attribute' => 'size',
                        'format' => 'raw',
                        'contentOptions'=>['style'=>'width: 145px;'],
                        'value' => function ($data) {
                            return \Yii::$app->formatter->asShortSize($data["size"]);
                        }
                    ],

                    [
                        'class' => 'common\hooks\yii2\grid\ActionColumn',
                        'template' => '{update} {crop} {delete}',
                        'buttons' => [
                            'crop' => function ($url, $model) {
                                if(strpos($model->type, "image") !== false){
                                    return Html::a(
                                        '<span class="glyphicon glyphicon-scissors"></span>',
                                        \Yii::$app->request->baseUrl."gallery/gallery/image_crop/?id={$model->id}", 
                                        [
                                            'title' => "Kadruj",
                                            'data-pjax' => '0',
                                        ]
                                    );
                                }
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </section>   
            
</div>
