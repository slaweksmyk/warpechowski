<div class="files-default-index">
    <?php
        use yii\helpers\Html;
        use yii\widgets\ActiveForm;
        use \common\modules\files\models\FileCategory;

        $oActiveCategory = FileCategory::find()->where(["=", "id", Yii::$app->request->get('category')])->one();

        $this->title = "Menadżer plików - upload plików";
        $this->registerCssFile('/bwcms/common/modules/files/admin/assets/css/fileUpload.css');
        $this->registerJsFile('/bwcms/common/modules/files/admin/assets/js/fileUpload.js', ['depends' => [yii\web\JqueryAsset::className()]]);
    ?>
    
    <p>
        <?= Html::a("< Wróć do listy", ['index', 'category' => \Yii::$app->request->get('category', null)], ['class' => 'btn btn-outline-secondary']) ?>
    </p>
    
    <script>
        var maxImageSize = 2048000;
        var maxOtherSize = <?= $maxUploadRaw ?>;
    </script>

    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Upload plików</header>
        <div class="panel-body upload-manager" >
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

                <div class="row">
                    <div class="col-3">
                        <div class="drag-upload" ondragover="return false">
                            <div>
                                <p>Upuść pliki w dowolnym miejscu w tym obszarze, aby wysłać je na serwer</p>
                                <img src="<?= \Yii::$app->request->baseUrl ?>images/ico/download.png" alt="download"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="add-files" >
                            <?= $form->field($model, 'imageFile')->fileInput(['multiple' => true, 'id' => 'add-files-input'])->label('Dodaj plik +'); ?>
                            
                            <table style="font-size: 12px;">
                                <tr>
                                    <th colspan="2" style="padding-bottom: 5px;">Limity rozmiaru plików</th>
                                </tr>
                                <tr>
                                    <th style="padding-top: 5px; font-size: 11px;">PNG, JPG, GIF, JPEG, SVG:</th>
                                    <td style="padding-left: 15px; padding-top: 5px;">2MB</td>
                                </tr>
                                <tr style="padding-top: 5px;">
                                    <th style="padding-top: 5px; font-size: 11px;">PDF, DOC, DOCX, XLS, XLSX:</th>
                                    <td style="padding-left: 15px; padding-top: 5px;"><?= $maxUpload ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label class="control-label" for="categories">Foldery</label>
                            <select id="categories" class="form-control ajax-load-file-categories" name="UploadForm[category_id][]">
                                <?php if(!is_null($oActiveCategory)){ ?>
                                    <option value="<?= $oActiveCategory->id ?>" selected="selected"><?= $oActiveCategory->name ?></option>
                                <?php } ?>
                            </select>
                            <div class="help-block"></div>
                        </div>
                    </div>
                </div>

                <div id="imagesAddedBox" class="row"></div>
                <div id="uploadStatus"></div>

                <?= Html::submitButton(Yii::t('backend', 'submit'), ['class' => 'btn btn-success']) ?>

            <?php ActiveForm::end() ?>
        </div>
    </section>  
</div> 
