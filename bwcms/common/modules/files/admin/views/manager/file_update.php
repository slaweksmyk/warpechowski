<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = "Menadżer plików" . $model->name;

?>

<div class="file-update">
    
    <p>
        <?= Html::a("< Wróć do listy", ['index', 'category' => \Yii::$app->request->get('category', null)], ['class' => 'btn btn-outline-secondary']) ?>
    </p>

    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?></header>
        <div class="panel-body" >
            <div class="file-form">

                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <div class="row">
                    <div class="col-6">
                        <p>Adres URL:</p>
                        <input type="text" class="form-control" value="<?= Url::to("/", true); ?>upload/<?= $model->filename; ?>" readonly/>
                    
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-12"><?= $form->field($model, 'description')->textarea(['rows' => 6])->label("Opis") ?></div>
                        </div>
                    
                    </div>
                    <div class="col-6">
                        <?php
                            $testType = explode("/", $model->type);
                            if($testType[0] == "image"){
                                echo '<p>Podgląd</p>';
                                echo '<img src="/upload/'.$model->filename.'" class="img-responsive"/>';
                            }
                        ?>
                    </div>
                </div>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </section>

</div>
