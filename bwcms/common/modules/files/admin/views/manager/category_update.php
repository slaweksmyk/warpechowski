<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use common\modules\files\models\FileCategory;
$this->title = "Menadżer plików" . $model->name;

?>

<p>
    <?= Html::a("< Wróć do listy", ['index', 'category' => \Yii::$app->request->get('category', null)], ['class' => 'btn btn-outline-secondary']) ?>
</p>

<div class="file-update">

    <section class="panel full" >
        <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?></header>
        <div class="panel-body" >
            <div class="file-form">
                <?php $form = ActiveForm::begin(); ?>
                
                <div class="row">
                    <div class="col-6">
                        <?= $form->field($model, 'parent_id')->dropDownList(ArrayHelper::map(FileCategory::find()->asArray()->all(), 'id', 'name'), ['prompt'=>'- wybierz -']) ?>
                    </div>
                    <div class="col-6">
                        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Utwórz' : 'Edytuj', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </section>

</div>
