<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;

use common\modules\files\models\File;
use common\modules\files\models\FileCategory;

use yii\widgets\Pjax;
$this->title = "Menadżer plików";
?>
<div class="file-index">
    <p>
        <?= Html::a("Dodaj kategorię", ['create-category'], ['class' => 'btn btn-success']) ?>
        <?= Html::a("Dodaj plik", ['create'], ['class' => 'btn btn-primary']) ?>
    </p>
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <?php if(Yii::$app->request->get('per-page')){ echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>"; } ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?php Pjax::begin(['id' => 'files-categories']) ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    [
                        'label' => "Rodzic",
                        'format' => 'html',
                        'contentOptions'=>['style'=>'width: 145px; text-align: center;'],
                        'value' => function ($data) {
                            $oParent = FileCategory::find()->where(["=", "id", $data["parent_id"]])->one();
                            if(!is_null($oParent)){
                                return $oParent->name;
                            } else {
                                return "- brak -";
                            }
                        }
                    ], 
                    'name',
                    [
                        'label' => "Ilość plików",
                        'format' => 'html',
                        'contentOptions'=>['style'=>'width: 145px; text-align: center;'],
                        'value' => function ($data) {
                            return count(File::find()->where(["=", "category_id", $data["id"]])->all());
                        }
                    ],   
                    [
                        'class' => 'common\hooks\yii2\grid\ActionColumn',
                        'template' => '{update-category} {files} {delete-category}',
                        'buttons' => [
                            'update-category' => function ($url) {
                                return Html::a(
                                    '<span class="glyphicon glyphicon-pencil"></span>',
                                    $url, 
                                    [
                                        'title' => Yii::t('backend', 'edit'),
                                        'data-pjax' => '0',
                                    ]
                                );
                            },
                            'files' => function ($url) {
                                return Html::a(
                                    '<span class="glyphicon glyphicon-list-alt"></span>',
                                    $url, 
                                    [
                                        'title' => "Pliki",
                                        'data-pjax' => '0',
                                    ]
                                );
                            },
                            'delete-category' => function ($url) {
                                return Html::a(
                                    '<span class="glyphicon glyphicon-trash"></span>',
                                    $url, 
                                    [
                                        'title' => Yii::t('backend', 'delete'),
                                        'data-confirm' => 'Czy na pewno usunąć ten element?',
                                        'data-pjax' => '0',
                                    ]
                                );
                            },       
                        ]
                    ],
                ],
            ]); ?>
             <?php Pjax::end() ?>
        </div>
    </section>   
            
</div>
