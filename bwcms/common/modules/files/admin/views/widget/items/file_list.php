<?php

use yii\widgets\ListView;
?>
<?=

ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => 'file_one',
    'summary' => false,
    'itemOptions' => [
        'tag' => false
    ],
    'options' => [
        'class' => 'manager-nav',
        'role' => 'tablist',
        'id' => false,
        'tag' => 'ul'
    ]
]);
?>






