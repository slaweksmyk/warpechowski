<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use common\modules\files\models\File;
use common\modules\files\models\FileCategory;

$this->registerJsFile('/bwcms/common/modules/files/admin/assets/js/files.js');
$this->registerCssFile('/bwcms/common/modules/files/admin/assets/css/files.css');

$this->title = "Menadżer plików";
?>
<div class="row">
    <p>
        <?= Html::a("Dodaj kategorię", ['create-category'], ['class' => 'btn btn-success']) ?>
        <?= Html::a("Dodaj plik", ['create'], ['class' => 'btn btn-primary']) ?>
    </p>



    <div class="col-3 no-padding-right" data-item="categories">


        <?=
        ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => 'items/nav',
            'summary' => false,
            'itemOptions' => [
                'tag' => false
            ],
            'options' => [
                'class' => 'manager-nav',
                'role' => 'tablist',
                'id' => false,
                'tag' => 'ul'
            ]
        ]);
        ?>




    </div>
    <div class="col-9" data-item="file-list">
        <?=
        ListView::widget([
            'dataProvider' => $dataFile,
            'itemView' => 'items/file_one',
            'summary' => false,
            'itemOptions' => [
                'tag' => false
            ],
            'options' => [
                'class' => 'manager-nav',
                'role' => 'tablist',
                'id' => false,
                'tag' => 'ul'
            ]
        ]);
        ?> 
    </div>
</div>