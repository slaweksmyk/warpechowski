<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ListView;
?>


<?php Pjax::begin(['id' => 'files']) ?>
<?=
ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => 'items/file',
    'itemOptions' => [
                            'tag' => false
                        ],
]);
?>
<?php Pjax::end() ?>



</ div>
