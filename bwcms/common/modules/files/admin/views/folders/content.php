<?php

use common\modules\files\models\File;

$this->registerJsFile('/bwcms/common/modules/files/admin/assets/js/folders.js');
$this->registerCssFile('/bwcms/common/modules/files/admin/assets/css/folders.css');

$this->title = "Menadżer plików";
?>
<div style="height: 0px; overflow: hidden;" id="target" data-element="<?= $sTarget ?>"></div>
<div class="row">
    <div class="col-12">
        <ol class="breadcrumb">
            <?php foreach($aBreadcrumps as $iCategoryID => $sName){ ?>
                <?php if(end($aBreadcrumps) != $sName){ ?>
                    <li class="breadcrumb-item"><a href="#" data-category="<?= $iCategoryID ?>"><?= $sName ?></a></li>
                <?php } else { ?>
                    <li class="breadcrumb-item active"><?= $sName ?></li>
                <?php } ?>
            <?php } ?>
        </ol>
    </div>
</div>
<div class="row quick-action">
    <div class="col-8">
        <b>Akcja dla zaznaczonych elementów:</b>
        <span class="btn btn-danger btn-xs" id="remove-all">Usuń <span class="glyphicon glyphicon-trash"></span></span>
    </div>
    <div class="col-4">
        <a href="#" id="create-files">
            <span class="btn btn-success btn-xs pull-right">Załaduj pliki <span class="glyphicon glyphicon-plus"></span></span>
        </a>
        <a href="#" id="create-folder">
            <span class="btn btn-success btn-xs pull-right" style="margin-right: 7px;">Dodaj nowy folder <span class="glyphicon glyphicon-plus"></span></span>
        </a>
    </div>
</div>
<div class="row search-row">
    <div class="col-1">
        <?php if(Yii::$app->request->get('category')){ ?>
            <?php if(!is_null($oCategory) && !is_null($oCategory->parent_id)){ ?>
                <a href="#" data-back="<?= $oCategory->parent_id ?>" class="go-back">
                    <img src="<?= \Yii::$app->request->baseUrl ?>images/ico/back-arrow.png" alt="back" data-toggle="tooltip" data-placement="bottom" title="Wróć" class="img-responsive folder-back"/>
                </a>
            <?php } else { ?>
                <a href="#" class="go-back">
                    <img src="<?= \Yii::$app->request->baseUrl ?>images/ico/back-arrow.png" alt="back" data-toggle="tooltip" data-placement="bottom" title="Wróć" class="img-responsive folder-back"/>
                </a>
            <?php } ?>
        <?php } ?>
    </div>
    <div class="col-1 text-center" style="padding-top: 7px; font-weight: bold;">Wyszukaj:</div>
    <div class="col-3"><input type="text" name="search_category" class="form-control" placeholder="Wpisz nazwę folderu..." value="<?= Yii::$app->request->get('search_category') ?>"/></div>
    <div class="col-3"><input type="text" name="search_file" class="form-control" placeholder="Wpisz nazwę pliku..." value="<?= Yii::$app->request->get('search_file') ?>"/></div>
    <div class="col-2 text-right"><b>Sortuj według:</b></div>
    <div class="col-2">
        <select name="order" class="form-control">
            <option value="id ASC" <?php if(Yii::$app->request->get('order', "id DESC") == "id ASC"){ echo "selected"; } ?>>Data rosnąco</option>
            <option value="id DESC" <?php if(Yii::$app->request->get('order', "id DESC") == "id DESC"){ echo "selected"; } ?>>Data malejąco</option>

            <option value="name ASC" <?php if(Yii::$app->request->get('order', "id DESC") == "name ASC"){ echo "selected"; } ?>>Nazwa rosnąco</option>
            <option value="name DESC" <?php if(Yii::$app->request->get('order', "id DESC") == "name DESC"){ echo "selected"; } ?>>Nazwa malejąco</option>

            <option value="type ASC" <?php if(Yii::$app->request->get('order', "id DESC") == "type ASC"){ echo "selected"; } ?>>Rodzaj rosnąco</option>
            <option value="type DESC" <?php if(Yii::$app->request->get('order', "id DESC") == "type DESC"){ echo "selected"; } ?>>Rodzaj malejąco</option>

            <option value="updated_at ASC" <?php if(Yii::$app->request->get('order', "id DESC") == "updated_at ASC"){ echo "selected"; } ?>>Data ostatniej modyfikacji rosnąco</option>
            <option value="updated_at DESC" <?php if(Yii::$app->request->get('order', "id DESC") == "updated_at DESC"){ echo "selected"; } ?>>Data ostatniej modyfikacji malejąco</option>
        </select>
    </div>
</div>
<div class="row">
    <div class="col-12 folders-desctop">
        <div class="row">
            <?php foreach($oCategoryRowset as $oCategory){ ?>
                <div class="col-2 folder-element single-folder" draggable="true" data-id="<?= $oCategory->id ?>">
                    <img src="<?= \Yii::$app->request->baseUrl ?>images/ico/folder.png" alt="<?= $oCategory->name ?>" class="img-responsive"/>
                    <p><?= $oCategory->name ?></p>
                    <div class="row single-tools">
                        <a href="<?= \Yii::$app->request->baseUrl ?>files/manager/update-category/?category=<?= Yii::$app->request->get('category', null) ?>&id=<?= $oCategory->id ?>" title="Edytuj" data-toggle="tooltip" data-placement="bottom">
                            <span class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></span>
                        </a>
                        <a href="#" class="remove-category" data-delete="<?= $oCategory->id ?>">
                            <span class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></span>
                        </a>
                    </div>
                </div>
            <?php } ?>
            <?php foreach($oFileRowset as $oFile){ ?>
                <?php 
                    $sThumbnail = \Yii::$app->request->baseUrl."images/ico/file.png";
                    if(strpos($oFile->type, "image" ) !== false && strpos($oFile->type, "svg") == false){
                        $sThumbnail = File::getThumbnail($oFile->id, 80, 80, "crop");
                    }
                    if(strpos($oFile->type, "pdf") !== false){
    //                    $sThumbnail = "/__cms/images/ico/pdf.png";
                    }
                    if(strpos($oFile->type, "svg") !== false){
                        $sThumbnail = "/upload/".$oFile->filename;
                    }
                ?>
                <div class="col-2 folder-element single-file" data-id="<?= $oFile->id ?>">
                    <img src="<?= $sThumbnail ?>" alt="<?= $oFile->name ?>" class="img-responsive"/>
                    <p title="<?= $oFile->name ?>"><?= $oFile->name ?></p>
                    <div class="row single-tools">
                        <a href="<?= \Yii::$app->request->baseUrl ?>files/manager/update/?category=<?= Yii::$app->request->get('category', null) ?>&id=<?= $oFile->id ?>" title="Edytuj" data-toggle="tooltip" data-placement="bottom">
                            <span class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></span>
                        </a>
                        <?php if(strpos($oFile->type, "image") !== false){ ?>
                            <a href="<?= \Yii::$app->request->baseUrl ?>gallery/gallery/image_crop/?id=<?= $oFile->id ?>" title="Kadruj" data-toggle="tooltip" data-placement="bottom">
                                <span class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-resize-full"></span></span>
                            </a>
                        <?php } ?>
                        <a href="#" title="Usuń" class="remove-file" data-delete="<?= $oFile->id ?>">
                            <span class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></span>
                        </a>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>