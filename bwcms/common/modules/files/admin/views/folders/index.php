<?php
use yii\helpers\Html;

$this->registerJsFile('/bwcms/common/modules/files/admin/assets/js/folders.js', ['depends' => [yii\web\JqueryAsset::className()]]);
$this->registerCssFile('/bwcms/common/modules/files/admin/assets/css/folders.css', ['depends' => [yii\bootstrap4\BootstrapAsset::className()]]);

$this->title = "Menadżer plików";
?>

<div class="file-index">
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
        </header>
        <div class="panel-body folders-area" data-category="<?= Yii::$app->request->get('category', null) ?>"></div>
    </section>   
</div>
