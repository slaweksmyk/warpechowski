<?php

namespace common\modules\map\models;

use Yii;
use common\modules\map\models\MapMarker;

/**
 * This is the model class for table "xmod_maps".
 *
 * @property integer $id
 * @property string $name
 * @property string $map_key
 * @property string $map_version
 * @property string $default_map_type
 * @property string $center_position_lat
 * @property string $center_position_long
 * @property string $map_style
 * @property integer $default_zoom_level
 * @property integer $is_cluster
 * @property integer $is_geolocalisation
 * @property integer $is_double_click_zoom
 * @property integer $is_zoom_control
 * @property integer $is_map_type_control
 * @property integer $is_scale_control
 * @property integer $is_street_view_control
 * @property integer $is_rotate_control
 * @property integer $is_full_screen_control
 * @property integer $is_disable_default_ui
 *
 * @property MapMarker[] $xmodMapsMarkers
 */
class Map extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_maps}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'map_key'], 'required'],
            [['map_style', 'style_height', 'style_width'], 'string'],
            [['default_zoom_level', 'map_type', 'is_cluster', 'is_geolocalisation', 'icon_id', 'is_double_click_zoom', 'is_zoom_control', 'is_map_type_control', 'is_scale_control', 'is_street_view_control', 'is_rotate_control', 'is_full_screen_control', 'is_disable_default_ui'], 'integer'],
            [['name', 'map_key', 'map_version', 'default_map_type', 'center_position_lat', 'center_position_long'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Nazwa',
            'map_type' => 'Rodzaj mapy',
            'icon_id' => 'Domyślna ikona markera',
            'map_key' => 'Klucz API',
            'map_version' => 'Wersja mapy',
            'style_height' => "Wysokość mapy",
            'style_width' => "Szerokość mapy",
            'default_map_type' => 'Rodzaj mapy',
            'center_position_lat' => 'Centrowanie mapy - długość',
            'center_position_long' => 'Centrowanie mapy - szerokość',
            'map_style' => 'Styl mapy',
            'default_zoom_level' => 'Domyślny poziom przybliżenia',
            'is_cluster' => 'Czy grupować punkty',
            'is_geolocalisation' => 'Czy użyć geolokalizacji',
            'is_double_click_zoom' => 'Czy przybliżać dwuklikiem',
            'is_zoom_control' => 'Czy dodać opcje przybliżania',
            'is_map_type_control' => 'Czy dodać zmianę rodzaju mapy',
            'is_scale_control' => 'Czy dać opcje skalowania mapy',
            'is_street_view_control' => 'Czy umożliwić widok street view',
            'is_rotate_control' => 'Czy umożliwić rotacje mapy',
            'is_full_screen_control' => 'Czy aktywować tryb pełnoekranowy',
            'is_disable_default_ui' => 'Czy całkowicie wyłączyć interfejs',
        ];
    }
    
    public function generateMap()
    {
        return Yii::$app->view->renderFile(Yii::getAlias("@common/modules/map/admin/views/map/map_{$this->map_type}.php"),["oMap" => $this]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMapMarkers()
    {
        return $this->hasMany(MapMarker::className(), ['map_id' => 'id']);
    }
}
