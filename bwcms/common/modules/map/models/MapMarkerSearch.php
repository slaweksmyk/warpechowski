<?php

namespace common\modules\map\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\map\models\MapMarker;

/**
 * MapMarkerSearch represents the model behind the search form about `common\modules\map\models\MapMarker`.
 */
class MapMarkerSearch extends MapMarker
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'map_id', 'icon_id', 'is_active'], 'integer'],
            [['name', 'slug', 'type', 'province', 'city', 'street', 'postcode', 'phone', 'email', 'hours', 'map_loc', 'map_long', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MapMarker::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'map_id' => $this->map_id,
            'icon_id' => $this->icon_id,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'province', $this->province])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'street', $this->street])
            ->andFilterWhere(['like', 'postcode', $this->postcode])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'hours', $this->hours])
            ->andFilterWhere(['like', 'map_loc', $this->map_loc])
            ->andFilterWhere(['like', 'map_long', $this->map_long])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
