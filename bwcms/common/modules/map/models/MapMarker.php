<?php

namespace common\modules\map\models;

use Yii;
use common\modules\map\models\Map;
use common\modules\files\models\File;

/**
 * This is the model class for table "xmod_maps_markers".
 *
 * @property integer $id
 * @property integer $map_id
 * @property integer $icon_id
 * @property integer $is_active
 * @property string $name
 * @property string $slug
 * @property string $type
 * @property string $province
 * @property string $city
 * @property string $street
 * @property string $postcode
 * @property string $phone
 * @property string $email
 * @property string $hours
 * @property string $map_loc
 * @property string $map_long
 * @property string $description
 *
 * @property Map $map
 * @property File $icon
 */
class MapMarker extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_maps_markers}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['map_id', 'name'], 'required'],
            [['map_id', 'icon_id', 'is_active'], 'integer'],
            [['description'], 'string'],
            [['name', 'slug', 'type', 'province', 'city', 'street', 'postcode', 'phone', 'email', 'hours', 'map_loc', 'map_long'], 'string', 'max' => 255],
            [['map_id'], 'exist', 'skipOnError' => true, 'targetClass' => Map::className(), 'targetAttribute' => ['map_id' => 'id']],
            [['icon_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['icon_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'map_id' => 'Mapa',
            'icon_id' => 'Ikona',
            'is_active' => 'Czy aktywny',
            'name' => 'Nazwa',
            'slug' => 'Slug',
            'type' => 'Rodzaj',
            'province' => 'Województwo',
            'city' => 'Miasto',
            'street' => 'Ulica',
            'postcode' => 'Kod pocztowy',
            'phone' => 'Numer telefonu',
            'email' => 'E-mail',
            'hours' => 'Godziny otwarcia',
            'map_loc' => 'Współrzędne - szerokość',
            'map_long' => 'Współrzędne - długość',
            'description' => 'Opis',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMap()
    {
        return $this->hasOne(Map::className(), ['id' => 'map_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIcon()
    {
        return $this->hasOne(File::className(), ['id' => 'icon_id']);
    }
}
