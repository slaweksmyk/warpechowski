<?php

namespace common\modules\map\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\map\models\Map;

/**
 * MapSearch represents the model behind the search form about `common\modules\map\models\Map`.
 */
class MapSearch extends Map
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'default_zoom_level', 'is_cluster', 'is_geolocalisation', 'is_double_click_zoom', 'is_zoom_control', 'is_map_type_control', 'is_scale_control', 'is_street_view_control', 'is_rotate_control', 'is_full_screen_control', 'is_disable_default_ui'], 'integer'],
            [['name', 'map_key', 'map_version', 'default_map_type', 'center_position_lat', 'center_position_long', 'map_style'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Map::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'default_zoom_level' => $this->default_zoom_level,
            'is_cluster' => $this->is_cluster,
            'is_geolocalisation' => $this->is_geolocalisation,
            'is_double_click_zoom' => $this->is_double_click_zoom,
            'is_zoom_control' => $this->is_zoom_control,
            'is_map_type_control' => $this->is_map_type_control,
            'is_scale_control' => $this->is_scale_control,
            'is_street_view_control' => $this->is_street_view_control,
            'is_rotate_control' => $this->is_rotate_control,
            'is_full_screen_control' => $this->is_full_screen_control,
            'is_disable_default_ui' => $this->is_disable_default_ui,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'map_key', $this->map_key])
            ->andFilterWhere(['like', 'map_version', $this->map_version])
            ->andFilterWhere(['like', 'default_map_type', $this->default_map_type])
            ->andFilterWhere(['like', 'center_position_lat', $this->center_position_lat])
            ->andFilterWhere(['like', 'center_position_long', $this->center_position_long])
            ->andFilterWhere(['like', 'map_style', $this->map_style]);

        return $dataProvider;
    }
}
