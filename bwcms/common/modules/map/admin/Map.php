<?php

namespace common\modules\map\admin;

/**
 * files module definition class
 */
class Map extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\map\admin\controllers';
    public $defaultRoute = 'map';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
