<?php

namespace common\modules\map\admin\controllers;

use Yii;
use common\modules\map\models\Map;
use common\modules\map\models\MapSearch;
use common\modules\map\models\MapMarker;
use common\modules\map\models\MapMarkerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MapController implements the CRUD actions for Map model.
 */
class MapController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Map models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MapSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * Lists all Map models.
     * @return mixed
     */
    public function actionMarkers($id)
    {
        $searchModel = new MapMarkerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(["map_id" => $id]);
        
        $oMap = Map::findOne($id);

        return $this->render('markers', [
            'oMap' => $oMap,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Map model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Map();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->style_height = "400px";
            $model->style_width  = "100%";
            $model->save();
            
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Map model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $marker = new MapMarker();

        if ($marker->load(Yii::$app->request->post())) {
            $marker->slug = $this->prepareURL($marker->name);
            $marker->map_id = $model->id;
            $marker->is_active = 1;
            $marker->description = nl2br($marker->description);
            $marker->save();
            
            return $this->redirect(['update', 'id' => $model->id]);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render("update_{$model->map_type}", [
                'model' => $model,
                'marker' => $marker,
            ]);
        }
    }
    
    public function actionUpdateMarker($id)
    {
        $model = MapMarker::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update-marker', 'id' => $model->id]);
        } else {
            return $this->render('marker', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Map model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionDeleteMarker($id)
    {
        $oMarker = MapMarker::findOne($id);
        $oMarker->delete();

        return $this->redirect(['markers', 'id' => $oMarker->map_id]);
    }

    /**
     * Finds the Map model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Map the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Map::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function prepareURL($sText) {
        $aReplacePL = array('ą' => 'a', 'ę' => 'e', 'ś' => 's', 'ć' => 'c', 'ó' => 'o', 'ń' => 'n', 'ż' => 'z', 'ź' => 'z', 'ł' => 'l', 'Ą' => 'A', 'Ę' => 'E', 'Ś' => 'S', 'Ć' => 'C', 'Ó' => 'O', 'Ń' => 'N', 'Ż' => 'Z', 'Ź' => 'Z', 'Ł' => 'L');
        $sText = str_replace(array_keys($aReplacePL), array_values($aReplacePL), $sText);
        $sText = str_replace("?", "", $sText);
        $sText = str_replace("„", "", $sText);
        $sText = str_replace("”", "", $sText);
        $sText = str_replace(' ', '-', strtolower($sText));
        $sText = preg_replace('/[\-]+/', '-', $sText);
        $sText = preg_replace('/[^A-Za-z0-9 -]/u', '', $sText);

        return trim($sText, '-');
    }
    
}
