<?php 
    use common\modules\files\models\File;
?>

<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.47.0/mapbox-gl.js'></script>
<script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-language/v0.10.0/mapbox-gl-language.js'></script>
<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.47.0/mapbox-gl.css' rel='stylesheet' />

<div id="map" style="height: <?= $oMap->style_height ?>; width: <?= $oMap->style_width ?>;"></div>

<script>
    (function() {
        mapboxgl.accessToken = '<?= $oMap->map_key ?>';
        var map = new mapboxgl.Map({
            container: 'map',
            <?php if($oMap->center_position_lat && $oMap->center_position_long){ ?>
                center: [<?= $oMap->center_position_lat ?>, <?= $oMap->center_position_long ?>],
            <?php } ?>

            zoom: <?= $oMap->default_zoom_level ?>,
            style: 'mapbox://styles/mapbox/<?= $oMap->map_style ? $oMap->map_style : "basic" ?>-v9?optimize=true'
        });

        var geojson = {
            type: 'FeatureCollection',
            features: [
            <?php foreach($oMap->getMapMarkers()->all() as $oMapMarker){ ?>
                {
                    type: 'Feature',
                    properties: {
                        popup: "<b><?= $oMapMarker->name ?></b><br/><br/><?= str_replace('"', "", $oMapMarker->description) ?>",
                        icon: "<?= $oMapMarker->icon_id ? File::getThumbnail($oMapMarker->icon_id, 50, 50, "matched") : $oMap->icon_id ? File::getThumbnail($oMap->icon_id, 50, 50, "matched") : 'https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi2.png'; ?>"
                    },
                    geometry: {
                        type: 'Point',
                        coordinates: [<?= $oMapMarker->map_long ?>, <?= $oMapMarker->map_loc ?>]
                    },
                },
            <?php } ?>
            ]
        };

        geojson.features.forEach(function(marker) {
            var el = document.createElement('img');
            el.src = marker.properties.icon;

            new mapboxgl.Marker(el)
                .setLngLat(marker.geometry.coordinates)
                .setPopup(new mapboxgl.Popup({offset: 25})
                .setHTML(marker.properties.popup))
                .addTo(map);
        });

        map.addControl(new MapboxLanguage({
            defaultLanguage: 'mul'
        }));
        
        <?php if($oMap->is_full_screen_control){ ?>
            map.addControl(new mapboxgl.FullscreenControl());
        <?php } ?>
        
        <?php if($oMap->is_geolocalisation){ ?>
            map.addControl(new mapboxgl.GeolocateControl({
                positionOptions: {
                    enableHighAccuracy: true
                },
                trackUserLocation: true
            }));
        <?php } ?>
            
        <?php if($oMap->is_map_type_control){ ?>
            map.addControl(new mapboxgl.NavigationControl());
        <?php } ?>
            
        <?php if(!$oMap->is_double_click_zoom){ ?>
            map.doubleClickZoom.disable();
        <?php } ?>
            
        var tempMarker;
        map.on('click', function (e) {
            if(!tempMarker){
                var el = document.createElement('img');
                el.src = '<?= $oMap->icon_id ? File::getThumbnail($oMap->icon_id, 50, 50, "matched") : "https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi2.png"; ?>';

                tempMarker = new mapboxgl.Marker(el)
                    .setLngLat([e.lngLat.lng, e.lngLat.lat])
                    .setPopup(new mapboxgl.Popup({offset: 25})
                    .setHTML("aaa"))
                    .addTo(map);
            } else {
                tempMarker.setLngLat([e.lngLat.lng, e.lngLat.lat]);
            }
            
            $("input[name='MapMarker[map_loc]']").val(e.lngLat.lat);
            $("input[name='MapMarker[map_long]']").val(e.lngLat.lng);
        });

    })();
</script>

