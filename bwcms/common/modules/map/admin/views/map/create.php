<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Dodaj mapę';
?>
<div class="map-create">

    <p>
        <?= Html::a('< Wróć do listy', ['index'], ['class' => 'btn btn-outline-secondary']) ?>
    </p>

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
        </header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(); ?>
            
            <div class="row">
                <div class="col-4">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-4">
                    <?= $form->field($model, 'map_key')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-2">
                    <?= $form->field($model, 'map_type')->dropDownList(["0" => "Google maps", "1" => "Mapbox (OSM)"])?>
                </div>
                <div class="col-2">
                    <?= $form->field($model, 'map_version')->dropDownList(["3" => "Wersja stabilna", "3.exp" => "Wersja eksperymentalna", "3.32" => "Wersja najnowsza"])?>
                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Dodaj' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </section>

</div>
