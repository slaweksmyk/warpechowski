<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\files\models\File;

$this->title = 'Zaktualizuj mapę: ' . $model->name;
?>
<div class="map-update">

    <p>
        <?= Html::a('< Wróć  do listy', ['index'], ['class' => 'btn btn-outline-secondary']) ?>
        <?= Html::a('Lista markerów', ['markers', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>
    
    <div class="row">
        <div class="col-7">
            <section class="panel full" >
                <header>
                    <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Podgląd mapy
                </header>
                <div class="panel-body" >
                    <?= $model->generateMap() ?>
                </div>
            </section>
            
            <section class="panel full" >
                <header>
                    <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Instrukcja
                </header>
                <div class="panel-body" >
                    <p>Aby dodać nowy marker, kliknij w wybrany punkt na mapie. Podstawowe dane adresowe zostaną pobrane automatycznie, pozostałe trzeba uzupełnić ręcznie.</p>
                </div>
            </section>
        </div>
        <div class="col-5">
            <section class="panel full" >
                <header>
                    <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Dodaj marker
                </header>
                <div class="panel-body" >
                    <?php $form = ActiveForm::begin(); ?>

                    <?= $form->field($marker, 'name')->textInput(['maxlength' => true]) ?>

                    <div class="row">
                        <div class="col-4">
                            <?= $form->field($marker, 'province')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-8">
                            <?= $form->field($marker, 'street')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-4">
                            <?= $form->field($marker, 'postcode')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-8">
                            <?= $form->field($marker, 'city')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-6">
                            <?= $form->field($marker, 'map_loc')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-6">
                            <?= $form->field($marker, 'map_long')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-4">
                            <?= $form->field($marker, 'phone')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-4">
                            <?= $form->field($marker, 'email')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-4">
                            <?= $form->field($marker, 'hours')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    
                    <?= $form->field($marker, 'description')->textarea(['rows' => 5, 'class' => 'noTynyMCE form-control']) ?>
                    
                    <div class="form-group">
                        <?= Html::submitButton($marker->isNewRecord ? 'Dodaj marker' : 'Zapisz zmiany', ['class' => $marker->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </section>
        </div>
    </div>
    
    <section class="panel full" >
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(); ?>
            
            <hr/>
                <h3 style="text-align: center;">Dane podstawowe</h3>
            <hr/>
            
            <div class="row">
                <div class="col-4">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-5">
                    <?= $form->field($model, 'map_key')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-3">
                    <?= $form->field($model, 'map_version')->dropDownList(["3" => "Wersja stabilna", "3.exp" => "Wersja eksperymentalna", "3.32" => "Wersja najnowsza"])?>
                </div>
            </div>
            
            <div class="row">
                <div class="col-3">
                    <?= $form->field($model, 'style_height')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-3">
                    <?= $form->field($model, 'style_width')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-3">
                    <?= $form->field($model, 'default_zoom_level')->textInput(['type' => "number"]) ?>
                </div>
                <div class="col-3">
                    <?= $form->field($model, 'default_map_type')->dropDownList(["roadmap" => "Drogowa", "satellite" => "Satelitarna", "hybrid" => "Hybrydowa", "terrain" => "Terenowa"]) ?>
                </div>
            </div>
            
            <div class="row">
                <div class="col-6">
                    <?= $form->field($model, 'center_position_lat')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-6">
                    <?= $form->field($model, 'center_position_long')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            
            <hr/>
                <h3 style="text-align: center;">Konfiguracja interfejsu</h3>
            <hr/>
            
            <div class="row">
                <div class="col-3">
                    <?= $form->field($model, 'is_cluster')->dropDownList([1 => Yii::t('backend', 'yes'), 0 => Yii::t('backend', 'no')]) ?>
                </div>
                <div class="col-3">
                    <?= $form->field($model, 'is_geolocalisation')->dropDownList([1 => Yii::t('backend', 'yes'), 0 => Yii::t('backend', 'no')]) ?>
                </div>
                <div class="col-3">
                    <?= $form->field($model, 'is_double_click_zoom')->dropDownList([1 => Yii::t('backend', 'yes'), 0 => Yii::t('backend', 'no')]) ?>
                </div>
                <div class="col-3">
                    <?= $form->field($model, 'is_zoom_control')->dropDownList([1 => Yii::t('backend', 'yes'), 0 => Yii::t('backend', 'no')]) ?>
                </div>
            </div>
            
            <div class="row">
                <div class="col-3">
                    <?= $form->field($model, 'is_map_type_control')->dropDownList([1 => Yii::t('backend', 'yes'), 0 => Yii::t('backend', 'no')]) ?>
                </div>
                <div class="col-3">
                    <?= $form->field($model, 'is_scale_control')->dropDownList([1 => Yii::t('backend', 'yes'), 0 => Yii::t('backend', 'no')]) ?>
                </div>
                <div class="col-3">
                    <?= $form->field($model, 'is_street_view_control')->dropDownList([1 => Yii::t('backend', 'yes'), 0 => Yii::t('backend', 'no')]) ?>
                </div>
                <div class="col-3">
                    <?= $form->field($model, 'is_full_screen_control')->dropDownList([1 => Yii::t('backend', 'yes'), 0 => Yii::t('backend', 'no')]) ?>
                </div>
            </div>
            
            <div class="row">
                <div class="col-4">
                    <?= $form->field($model, 'is_rotate_control')->dropDownList([1 => Yii::t('backend', 'yes'), 0 => Yii::t('backend', 'no')]) ?>
                </div>
                <div class="col-4">
                    <?= $form->field($model, 'is_disable_default_ui')->dropDownList([1 => Yii::t('backend', 'yes'), 0 => Yii::t('backend', 'no')]) ?>
                </div>
                <div class="col-4">
                    <?php if($model->icon_id){ ?>
                        <div class="row">
                            <div class="col-9">
                                <b>Domyślna ikona dla markerów</b>
                                <a href="#" data-init="file-manager" data-target="#map-icon_id">
                                    <span class="btn btn-success btn-md" style="width: 100%; margin-top: 5px;">Wybierz ikonę z menadżera plików</span>
                                </a>
                                <?= $form->field($model, 'icon_id')->hiddenInput()->label(false) ?>
                            </div>
                            <div class="col-3">
                                <img src="<?= File::getThumbnail($model->icon_id, 50, 50, "matched") ?>" class="img-responsive" style="margin-top: 10px;"/>
                            </div>
                        </div>
                    <?php } else { ?>
                        <b>Domyślna ikona dla markerów</b>
                        <a href="#" data-init="file-manager" data-target="#map-icon_id">
                            <span class="btn btn-success btn-md" style="width: 100%; margin-top: 5px;">Wybierz ikonę z menadżera plików</span>
                        </a>
                        <?= $form->field($model, 'icon_id')->hiddenInput()->label(false) ?>
                    <?php } ?>
                </div>
            </div>
            

            <?= $form->field($model, 'map_style')->textarea(['rows' => 6, 'class' => 'noTynyMCE form-control']) ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Zapisz zmiany', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </section>
   

</div>
