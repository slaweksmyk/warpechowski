<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\files\models\File;

$this->title = 'Zaktualizuj marker: ' . $model->name;
?>

<p>
    <?= Html::a('< Wróć  do listy', ['markers', 'id' => $model->getMap()->one()->id], ['class' => 'btn btn-outline-secondary']) ?>
</p>

<section class="panel full" >
    <header>
        <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Zaktualizuj marker
    </header>
    <div class="panel-body" >
        <?php $form = ActiveForm::begin(); ?>
        
        <div class="row">
            <div class="col-8">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-4">
                <?php if($model->icon_id){ ?>
                    <div class="row">
                        <div class="col-9">
                            <b>Ikona dla markera</b>
                            <a href="#" data-init="file-manager" data-target="#mapmarker-icon_id">
                                <span class="btn btn-success btn-md" style="width: 100%; margin-top: 5px;">Wybierz ikonę z menadżera plików</span>
                            </a>
                            <?= $form->field($model, 'icon_id')->hiddenInput()->label(false) ?>
                        </div>
                        <div class="col-3">
                            <img src="<?= File::getThumbnail($model->icon_id, 50, 50, "matched") ?>" class="img-responsive" style="margin-top: 10px;"/>
                        </div>
                    </div>
                <?php } else { ?>
                    <b>Ikona dla markera</b>
                    <a href="#" data-init="file-manager" data-target="#mapmarker-icon_id">
                        <span class="btn btn-success btn-md" style="width: 100%; margin-top: 5px;">Wybierz ikonę z menadżera plików</span>
                    </a>
                    <?= $form->field($model, 'icon_id')->hiddenInput()->label(false) ?>
                <?php } ?>
            </div>
        </div> 

        <div class="row">
            <div class="col-3">
                <?= $form->field($model, 'province')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'street')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-2">
                <?= $form->field($model, 'postcode')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-4">
                <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-3">
                <?= $form->field($model, 'map_loc')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'map_long')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-2">
                <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-2">
                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-2">
                <?= $form->field($model, 'hours')->textInput(['maxlength' => true]) ?>
            </div>
        </div>


        <?= $form->field($model, 'description')->textarea(['rows' => 5, 'class' => 'noTynyMCE form-control']) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Dodaj marker' : 'Zapisz zmiany', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</section>