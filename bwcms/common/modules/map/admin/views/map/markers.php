<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;

$this->title = 'Zarządzaj markerami mapy: '.$oMap->name;
?>
<div class="map-index">

    <p>
        <?= Html::a('< Wróć do mapy', ['update', 'id' => $oMap->id], ['class' => 'btn btn-outline-secondary']) ?>
    </p>

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <?php if (Yii::$app->request->get('per-page')) {
                        echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>";
                    } ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    'name',
                    'province',
                    'city',
                    'street',
                    [
                        'class' => 'common\hooks\yii2\grid\ActionColumn',
                        'template' => '{update-marker} {delete-marker}',
                        'contentOptions' => ['style' => 'width: 100px;'],
                        'buttons' => [
                            'update-marker' => function ($url) {
                                return Html::a(
                                        '<span class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></span>', $url, [
                                        'title' => Yii::t('backend', 'edit'),
                                        'data-toggle' => "tooltip",
                                        'data-placement' => "bottom"
                                    ]
                                );
                            },
                            'delete-marker' => function ($url) {
                                return Html::a(
                                        '<span class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></span>', $url, [
                                        'title' => Yii::t('backend', 'delete'),
                                        'data-toggle' => "tooltip",
                                        'data-placement' => "bottom",
                                        'data-pjax' => 0,
                                        'data-confirm' => 'Czy na pewno usunąć ten element?',
                                        'data-method' => 'post',
                                    ]
                                );
                            },
                        ],
                    ],
                ],
            ]);
            ?>
        </div>
    </section>
</div>
