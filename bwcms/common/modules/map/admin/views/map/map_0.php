<?php 
    use common\modules\files\models\File;
?>

<script>
var map;
var tempMarker;
var markers = [];
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        <?php if($oMap->center_position_lat && $oMap->center_position_long){ ?>
            center: {lat: <?= $oMap->center_position_lat ?>, lng: <?= $oMap->center_position_long ?>},
        <?php } ?>

        zoom: <?= $oMap->default_zoom_level ?>,
        disableDefaultUI: <?= $oMap->is_disable_default_ui ? "true" : "false" ?>,
        zoomControl: <?= $oMap->is_zoom_control ? "true" : "false" ?>,
        mapTypeControl: <?= $oMap->is_map_type_control ? "true" : "false" ?>,
        scaleControl: <?= $oMap->is_scale_control ? "true" : "false" ?>,
        streetViewControl: <?= $oMap->is_street_view_control ? "true" : "false" ?>,
        rotateControl: <?= $oMap->is_rotate_control ? "true" : "false" ?>,
        fullscreenControl: <?= $oMap->is_full_screen_control ? "true" : "false" ?>,
        mapTypeId: '<?= $oMap->default_map_type ?>',

        <?php if($oMap->map_style){ ?>
            styles: <?= $oMap->map_style ?>
        <?php } ?>
    });
    
    var markersData = [
        <?php foreach($oMap->getMapMarkers()->all() as $oMapMarker){ ?>
            {
                lat: <?= $oMapMarker->map_loc ?>, 
                lng: <?= $oMapMarker->map_long ?>,
                title: "<?= $oMapMarker->name ?>",
                description: "<?= $oMapMarker->description ?>",
                icon: "<?= $oMapMarker->icon_id ? File::getThumbnail($oMapMarker->icon_id, 50, 50, "matched") : File::getThumbnail($oMap->icon_id, 50, 50, "matched"); ?>"
            },
        <?php } ?>
    ];
    

    for (var i = 0; i < markersData.length; i++) {
        var marker;
        data = markersData[i];

        marker = new google.maps.Marker({
            position: {lat: data.lat, lng: data.lng},
            map: map,
            title: data.title,
            icon: data.icon,
            animation: google.maps.Animation.DROP,
            customInfo: "<b>" +data.title + "</b><br/><br/>" + data.description
        });
    
        var infowindow = new google.maps.InfoWindow();  
        google.maps.event.addListener(marker, 'click', (function(marker) {  
            return function() {  
                infowindow.setContent(marker.customInfo);  
                infowindow.open(map, marker);  
            }  
        })(marker));  

        markers.push(marker);
    }

    <?php if($oMap->is_cluster){ ?>
        var markerCluster = new MarkerClusterer(map, markers, {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
    <?php } ?>
    
    
    /* GeoCoder */
    var geocoder = new google.maps.Geocoder;
    
    google.maps.event.addListener(map, 'click', function(event) {
        placeMarker(event.latLng);
    });
    
    function placeMarker(location) {
        geocoder.geocode({'location': location}, function(results, status) {
            if (status === 'OK') {
                var aData = results[0]["address_components"];
                
                if(aData.length == 9){
                    $("input[name='MapMarker[street]']").val(aData[1].long_name + " " + aData[0].long_name);
                    $("input[name='MapMarker[city]']").val(aData[3].long_name);
                    $("input[name='MapMarker[province]']").val(aData[5].long_name);
                    $("input[name='MapMarker[postcode]']").val(aData[7].long_name);
                } else if(aData.length == 8){
                    $("input[name='MapMarker[street]']").val(aData[1].long_name + " " + aData[0].long_name);
                    $("input[name='MapMarker[city]']").val(aData[3].long_name);
                    $("input[name='MapMarker[province]']").val(aData[5].long_name);
                    $("input[name='MapMarker[postcode]']").val(aData[7].long_name);
                } else if(aData.length == 7) {
                    $("input[name='MapMarker[street]']").val(aData[1].long_name + " " + aData[0].long_name);
                    $("input[name='MapMarker[city]']").val(aData[2].long_name);
                    $("input[name='MapMarker[province]']").val(aData[4].long_name);
                    $("input[name='MapMarker[postcode]']").val(aData[6].long_name);
                } else if(aData.length == 6) {
                    $("input[name='MapMarker[street]']").val(aData[0].long_name);
                    $("input[name='MapMarker[city]']").val(aData[1].long_name);
                    $("input[name='MapMarker[province]']").val(aData[3].long_name);
                    $("input[name='MapMarker[postcode]']").val(aData[5].long_name);
                } else if(aData.length == 5) {
                    $("input[name='MapMarker[street]']").val(aData[0].long_name);
                    $("input[name='MapMarker[city]']").val(aData[1].long_name);
                    $("input[name='MapMarker[province]']").val(aData[3].long_name);
                }
            }
            
            $("input[name='MapMarker[map_loc]']").val(location.lat());
            $("input[name='MapMarker[map_long]']").val(location.lng());
            
            if(!tempMarker){
                tempMarker = new google.maps.Marker({
                    position: location, 
                    map: map,
                    icon: "<?= File::getThumbnail($oMap->icon_id, 50, 50, "matched"); ?>"
                });
            } else {
                tempMarker.setPosition(location);
            }
        });

    }

}
</script>

<?php if($oMap->is_cluster){ ?>
    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<?php } ?>
<script src='https://maps.googleapis.com/maps/api/js?v=<?= $oMap->map_version ?>&key=<?= $oMap->map_key ?>&callback=initMap' async defer></script>
<div id="map" style="height: <?= $oMap->style_height ?>; width: <?= $oMap->style_width ?>;"></div>