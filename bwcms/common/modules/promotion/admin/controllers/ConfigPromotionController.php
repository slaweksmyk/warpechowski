<?php

namespace common\modules\promotion\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;

use common\modules\promotion\models\PromotionConfig;

/**
 * PromotionslistController implements the CRUD actions for PromotionsList model.
 */
class ConfigPromotionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Updates an existing PromotionsList model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = PromotionConfig::find()->where(["=", "id", 1])->one();

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            $model->save();
        }

        return $this->render('update', [
            'model' => $model
        ]);
    }

}
