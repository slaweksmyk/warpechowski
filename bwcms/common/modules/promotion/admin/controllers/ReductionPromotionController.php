<?php

namespace common\modules\promotion\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;

use common\modules\promotion\models\PromotionReduction;

/**
 * PromotionslistController implements the CRUD actions for PromotionsList model.
 */
class ReductionPromotionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Updates an existing PromotionsList model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {
        return $this->render('update', [
            'oPromotionReductionRowset' => PromotionReduction::find()->all()
        ]);
    }
    
    public function actionWholesale()
    {
        $request = Yii::$app->request;
        $oPromotionConnection = new PromotionReduction();
        
        if($request->isPost){
            $oPromotionConnection->product_id = $request->post('product_id');
            $oPromotionConnection->amount_perc = str_replace("%", "", $request->post('rev_perc'));
            $oPromotionConnection->amount_sum = str_replace("zł", "", $request->post('rev_cash'));
            $oPromotionConnection->date_start = $request->post('date_start');
            $oPromotionConnection->date_end = $request->post('date_end');
            $oPromotionConnection->save();
        }
        
        
        return $this->render('wholesale', [
            'oPromotionConnection' => $oPromotionConnection,
            'oPromotionReductionRowset' => PromotionReduction::find()->all()
        ]);
    }
    
    public function actionDelete()
    {
        $oPromotionReduction = PromotionReduction::findOne(Yii::$app->request->get('id'));
        $oPromotionReduction->delete();
        
        return $this->redirect(['wholesale']);
    }
    
    public function actionSave(){
        $aData = json_decode(Yii::$app->request->post('data', "{}"));

        PromotionReduction::deleteAll();
        foreach($aData as $oDat){
            $oPromotionConnection = new PromotionReduction();
            
            if($oDat->isCat){
                $oPromotionConnection->category_id = $oDat->element;
            } else {
                $oPromotionConnection->product_id = $oDat->element;
            }

            $oPromotionConnection->amount_perc = str_replace("%", "", $oDat->rev_perc);
            $oPromotionConnection->amount_sum = str_replace("zł", "", $oDat->rev_cash);
            $oPromotionConnection->date_start = $oDat->date_start;
            $oPromotionConnection->date_end = $oDat->date_end;
            $oPromotionConnection->save();
        }

        exit;
    }

}
