<?php

namespace common\modules\promotion\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use common\modules\shop\models\Product;
use common\modules\shop\models\Category;
use common\modules\promotion\models\PromotionConnection;

/**
 * PromotionslistController implements the CRUD actions for PromotionsList model.
 */
class ConnectionPromotionController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Updates an existing PromotionsList model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {
        return $this->render('update', [
                    'oPromotionConnection' => PromotionConnection::find()->all()
        ]);
    }

    public function actionSave()
    {
        $aData = json_decode(Yii::$app->request->get('data', "{}"));



        PromotionConnection::deleteAll();
        foreach ($aData as $oDat) {
            if (isset($oDat->reward_id)) {
                $iRewardID = $oDat->reward_id;
                $iType = $oDat->type;
                $aElementHash = $oDat->element_hash;
                $sDiscount = str_replace("%", "", $oDat->discount_amount);



                $oPromotionConnection = new PromotionConnection();
                $oPromotionConnection->reward_id = $iRewardID;
                $oPromotionConnection->type = $iType;
                $oPromotionConnection->element_hash = $aElementHash;
                $oPromotionConnection->discount_amount = $sDiscount;
                $oPromotionConnection->date_start = $oDat->date_start;
                $oPromotionConnection->date_end = $oDat->date_end;
                $oPromotionConnection->save();


                echo '<pre>';
                var_dump($oPromotionConnection);
            } else {
                echo 'no reward id';
            }
        }

        exit;
    }

    public function actionGet()
    {
        if (Yii::$app->request->isAjax) {
            $sCategoryName = "- Wszystkie -";
            $oProductRowset = Product::find()->where("1 = 1");
            $oCategoryRowset = Category::find()->where("1 = 1");

            $iCid = Yii::$app->request->get('cid', null);
            $sSearch = Yii::$app->request->get('search', null);
            $aIDin = json_decode(Yii::$app->request->get('ids', "[]"));

            if ($aIDin) {
                $oProductRowset->andWhere(["IN", "id", $aIDin]);
            }

            if ($iCid) {
                $sCategoryName = Category::find()->where(["=", "id", $iCid])->one()->name;

                $oProductRowset->andWhere(["=", "category_id", $iCid]);
                $oCategoryRowset->andWhere(["=", "parent_id", $iCid]);
            }

            if ($sSearch) {
                $oCategoryRowset->andWhere(["<", "id", 0]);
                $oProductRowset->andWhere(["LIKE", "name", $sSearch]);
            }

            $oTmpRowset = $oProductRowset->limit(100)->all();

            $aReturnProducts = $oProductRowset->asArray()->all();
            $aReturnCategory = $oCategoryRowset->asArray()->all();

            foreach ($oTmpRowset as $index => $oTmpProd) {
                if ($oTmpProd->hasThumbnail() && isset($aReturnProducts[$index])) {
                    $aReturnProducts[$index]["thumbnail"] = $oTmpProd->getThumbnail(2000, 2000, "matched");
                }
            }

            echo json_encode([
                "sCategoryName" => $sCategoryName,
                "oProductRowset" => $aReturnProducts,
                "oCategoryRowset" => $aReturnCategory
            ]);

            Yii::$app->end();
            exit;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

}
