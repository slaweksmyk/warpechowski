<?php

namespace common\modules\promotion\admin\controllers;

use Yii;
use yii\db\Expression;
use yii\web\Controller;
use yii\filters\VerbFilter;

use common\modules\users\models\UserData;
use common\modules\promotion\models\PromotionBday;

/**
 * PromotionslistController implements the CRUD actions for PromotionsList model.
 */
class BdayPromotionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Updates an existing PromotionsList model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {
        $oBday = PromotionBday::find()->where(["=", "id", 1])->one();
        if (Yii::$app->request->isPost) {
            $oBday->load(Yii::$app->request->post());
            $oBday->save();
        }
            
        $oBdayRowset = UserData::find()->where("DATE_ADD(birth_date, INTERVAL YEAR(CURDATE())-YEAR(birth_date) + IF(DAYOFYEAR(CURDATE()) > DAYOFYEAR(birth_date),1,0)YEAR)")->andWhere(["IS NOT", "birth_date", NULL])->having(["BETWEEN", "birth_date", new Expression("CURDATE()"), new Expression("DATE_ADD(CURDATE(), INTERVAL 7 DAY)")]);

        $aNdataRowset = [];
        for($i=0;$i<10;$i++){
            array_push($aNdataRowset, 
                [
                    "id" => $i+1,
                    "name" => $oBday->nDayData()[date('m-d', strtotime("+{$i} days"))],
                    "date" => date('d.m.Y', strtotime("+{$i} days"))
                ]
            );
        }

        return $this->render('update', [
            'oBday' => $oBday,
            'oBdayRowset' => $oBdayRowset,
            'aNdataRowset' => $aNdataRowset
        ]);
    }

}
