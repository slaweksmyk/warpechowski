<?php

namespace common\modules\promotion\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use common\modules\promotion\models\PromotionThresholds;

/**
 * PromotionslistController implements the CRUD actions for PromotionsList model.
 */
class ThresholdsPromotionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [

                ],
            ],
        ];
    }

    /**
     * Updates an existing PromotionsList model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = new PromotionThresholds();
        
        if ($model->load(Yii::$app->request->post())) {
            PromotionThresholds::deleteAll();
            
            for($i=0;$i<count($model->order_amount_low);$i++){
                $oPromotionThresholds = new PromotionThresholds();
                $oPromotionThresholds->order_amount_low  = $model->order_amount_low[$i];
                $oPromotionThresholds->order_amount_high = $model->order_amount_high[$i];
                $oPromotionThresholds->discount_amount   = str_replace("%", "", $model->discount_amount[$i]);
                $oPromotionThresholds->save();
            }
        }

        return $this->render('update', [
            'model' => $model,
            'oPromotionThresholdsRowset' => PromotionThresholds::find()->all()
        ]);
    }
    
    /**
     * Deletes an existing model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = PromotionThresholds::find()->where(["=", "id", $id])->one();
        $model->delete();

        return $this->redirect(['update']);
    }


}
