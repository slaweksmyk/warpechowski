<?php

namespace common\modules\promotion\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use common\modules\logs\models\Log;
use common\modules\shop\models\Product;
use common\modules\promotion\models\PromotionDaily;
use common\modules\promotion\models\ShopPromotionDailySave;

/**
 * PromotionslistController implements the CRUD actions for PromotionsList model.
 */
class DailyPromotionController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionAjaxSave()
    {
        $oShopPromotionDailySave = ShopPromotionDailySave::find()->where(["=", "id", Yii::$app->request->post('id')])->one();

        if (is_null($oShopPromotionDailySave)) {
            $oShopPromotionDailySave = new ShopPromotionDailySave();
            $oShopPromotionDailySave->product_id = Yii::$app->request->post('id');
            $oShopPromotionDailySave->product_name = Yii::$app->request->post('name');
            $oShopPromotionDailySave->save();
        }
    }

    public function actionAjaxDelete()
    {
        $oShopPromotionDailySave = ShopPromotionDailySave::find()->where(["=", "product_id", Yii::$app->request->post('id')])->one();
        $oShopPromotionDailySave->delete();
    }

    public function actionDelete()
    {
        if (Yii::$app->request->isAjax) {
            $aPost = Yii::$app->request->post();

            $oPromotionDaily = PromotionDaily::find()->where(["=", "hash", $aPost["hash"]])->one();

            $aNewPromoLabels = [];
            if (count(Yii::$app->session->get('promotion_labels', [])) > 0) {
                foreach (Yii::$app->session->get('promotion_labels', []) as $iProductID => $sName) {
                    if ($oPromotionDaily->product_id != $iProductID) {
                        $aNewPromoLabels[$iProductID] = $sName;
                    }
                }
                Yii::$app->session->set('promotion_labels', $aNewPromoLabels);
            }

            $oPromotionDaily->delete();

            exit;
        }
    }

    public function actionGet()
    {
        if (Yii::$app->request->isAjax) {
            $aPost = Yii::$app->request->get();

            $oPromotionDaily = PromotionDaily::find()->where(["=", "hash", $aPost["hash"]])->one();
            echo json_encode([
                "promo_type" => $oPromotionDaily->promo_type,
                "amount" => $oPromotionDaily->amount,
                "active_from" => date("Y-m-d", strtotime($oPromotionDaily->active_from)),
                "active_to" => date("Y-m-d", strtotime($oPromotionDaily->active_to)),
            ]);

            exit;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSearch()
    {
        if (Yii::$app->request->isAjax) {
            $aPost = Yii::$app->request->post();

            $oProductRowset = Product::find()->where(["OR",
                        ["LIKE", "name", $aPost["value"]],
                        ["LIKE", "code", $aPost["value"]]
                    ])->asArray()->all();

            echo json_encode($oProductRowset);

            exit;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSave()
    {
        if (Yii::$app->request->isAjax) {
            $aPost = Yii::$app->request->post();

            if ($aPost["date_start"] == $aPost["date_end"]) {
                $aPost["date_end"] = date("Y-m-d H:i:s", strtotime('+23 hours', strtotime($aPost["date_end"])));
            }

            $oPromotionDaily = PromotionDaily::find()->where(["=", "hash", $aPost["hash"]])->one();
            $oPromotionDaily->promo_type = $aPost["promo_type"];
            $oPromotionDaily->amount = $aPost["amount"];
            $oPromotionDaily->active_from = $aPost["date_start"];
            $oPromotionDaily->active_to = $aPost["date_end"];
            $oPromotionDaily->save();

            exit;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAjax()
    {
        if (Yii::$app->request->isAjax) {
            $aPost = Yii::$app->request->post();

            if (isset($aPost["hash"]) && $aPost["hash"] != "") {
                $oPromotionDaily = PromotionDaily::find()->where(["=", "hash", $aPost["hash"]])->one();
                $oPromotionDaily->active_from = $aPost["activeFrom"];
                $oPromotionDaily->active_to = $aPost["activeTo"];
                $oPromotionDaily->save();
            } else {
                $oPromotionDaily = new PromotionDaily();
                $oPromotionDaily->product_id = $aPost["productID"];
                $oPromotionDaily->active_from = $aPost["activeFrom"];
                $oPromotionDaily->active_to = $aPost["activeTo"];
                $oPromotionDaily->hash = uniqid();
                $oPromotionDaily->save();
            }

            echo $oPromotionDaily->hash;
            exit;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Updates an existing PromotionsList model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {
        return $this->render('update', [
        ]);
    }

}
