<?php

namespace common\modules\promotion\admin;

use Yii;
/**
 * Promotions module definition class
 */
class Promotion extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\promotion\admin\controllers';
    public $defaultRoute = 'promotions-list';

    /**
     * @inheritdoc
     */
    public function init()
    {
        $aUrlParts = explode("/", str_replace("/__cms/", "", Yii::$app->getRequest()->url));
        $this->defaultRoute = current($aUrlParts);

        parent::init();
    }
}
