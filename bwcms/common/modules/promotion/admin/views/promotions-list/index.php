<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;

$this->title = "Zarządzaj listą promocji";
?>
<div class="promotions-list-index">

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                'id',
                [
                    'attribute' => "cat_name",
                    'headerOptions' => ['style' => 'width:20%'],
                ],
                'name',
                [
                    'class' => 'common\hooks\yii2\grid\ActionColumn',
                    'template' => '{update}',
                    'buttons' => [
                        'update' => function ($url, $model, $key) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-pencil"></span>',
                                \Yii::$app->request->baseUrl."{$model->sys_name}-promotion/{$model->sys_name}-promotion/update/", 
                                [
                                    'title' => "Zaktualizuj",
                                    'data-pjax' => '0',
                                ]
                            );
                        }
                    ],
                ]
            ],
        ]); ?>
        </div>
    </section>   
</div>
