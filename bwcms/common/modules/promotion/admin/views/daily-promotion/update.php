<?php
    use yii\helpers\Html;
    use common\modules\shop\models\Product;
    use common\modules\promotion\models\PromotionDaily;
    use common\modules\promotion\models\ShopPromotionDailySave;

    $this->title = "Produkt dnia";
    
    $this->registerCssFile('/bwcms/common/modules/promotion/admin/assets/css/fullcalendar.css');
    $this->registerCssFile('/bwcms/common/modules/promotion/admin/assets/css/style.css');
    
    $this->registerJsFile('/bwcms/common/modules/promotion/admin/assets/js/moment.min.js', ['depends' => [yii\web\JqueryAsset::className()]]);
    $this->registerJsFile('/bwcms/common/modules/promotion/admin/assets/js/jquery-ui.min.js', ['depends' => [yii\web\JqueryAsset::className()]]);
    $this->registerJsFile('/bwcms/common/modules/promotion/admin/assets/js/fullcalendar.js', ['depends' => [yii\web\JqueryAsset::className()]]);
    $this->registerJsFile('/bwcms/common/modules/promotion/admin/assets/js/locale/pl.js', ['depends' => [yii\web\JqueryAsset::className()]]);
    $this->registerJsFile('/bwcms/common/modules/promotion/admin/assets/js/init.js', ['depends' => [yii\web\JqueryAsset::className()]]);
?>

<script>
    var events = [
        <?php foreach(PromotionDaily::find()->all() as $oDailyEvent){ ?>
                <?php $sClassName = ""; ?>
                <?php if(date("Y-m-d") > date("Y-m-d", strtotime($oDailyEvent->active_to))){ ?>
                    <?php $sColor = "#cacaca"; ?>
                <?php } else { ?>
                    <?php $sColor = "#ffae00"; ?>
                <?php } ?>
                    
                <?php if(date("Y-m-d", strtotime($oDailyEvent->active_from)) == date("Y-m-d", strtotime($oDailyEvent->active_to))){ ?>
                    <?php $sAllDay = "true"; ?>
                    <?php $sClassName = "all-day"; ?>
                <?php } else { ?>
                    <?php $sAllDay = "false"; ?>
                <?php } ?>
                    {
                        product_id: '<?= $oDailyEvent->product_id ?>',
                        title: '<?= $oDailyEvent->getProduct()->one()->name ?>',
                        start: '<?= date("Y-m-d", strtotime($oDailyEvent->active_from)) ?>',
                        end: '<?= date("Y-m-d", strtotime($oDailyEvent->active_to)) ?> 23:59:59',
                        hash: '<?= $oDailyEvent->hash ?>',
                        backgroundColor: '<?= $sColor; ?>',
                        borderColor: '<?= $sColor; ?>',
                        allDay: <?= $sAllDay ?>,
                        className: '<?= $sClassName ?>'
                    },
        <?php } ?>
    ];
</script>

<div id="cal-popup">
    <div class="pop_cnt">
        <section class="panel full">
            <header>
                <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> 
                <span>
                    Edytuj i zapisz promocję produktu dla:<br/><br/>
                    <b class="cal-pop-name" style="text-transform: uppercase; color: black;"></b>
                </span>
                <div class="cal-popup-close" title="Zamknij okno">x</div>
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label" for="product-gallery_id">Rodzaj promocji</label>
                            <select id="promo_type" class="form-control" name="Product[gallery_id]" aria-invalid="false">
                                <option value="">Brak</option>
                                <option value="1">Procentowa</option>
                                <option value="0">Kwotowa</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label" for="pop2">Wartość promocji</label>
                            <input type="text" id="pop2" class="form-control" name="pop">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label" for="pop3">Data rozpoczęcia</label>
                            <input type="text" id="pop3" class="form-control datepicker" name="pop">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label class="control-label" for="pop4">Data zakończenia</label>
                            <input type="text" id="pop4" class="form-control datepicker" name="pop">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-danger popup-delete" style="float: left;">Usuń z kalendarza</button> 
                    
                    <button class="btn btn-warning popup-clear" style="float: right; margin-left: 10px;">Wyczyść</button> 
                    <button class="btn btn-danger popup-close" style="float: right; margin-left: 10px;">Zamknij</button>     
                    <button type="submit" class="btn btn-primary" id="pop-submit" style="float: right;">Zapisz</button>                    
                </div>
            </div>
        </section>
    </div>
</div>

<p>
    <?= Html::a("< Wroć do listy promocji", \Yii::$app->request->baseUrl."promotions-list/", ['class' => 'btn btn-primary']) ?>
</p>

<div style="width: 30%; float: left;">
    <section class="panel full">
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Wyszukaj produkt 
        </header>
        <div class="panel-body" id="search">
            <select id="related" class="form-control ajax-load-products" name="Product[related][]"></select>
        </div>
    </section>
    <section class="panel full">
        <div class="panel-body">
            <div id='external-events'>
                <h2>Lista produktów</h2>
                <div id="events-list">
                    <?php 
                        $oShopPromotionDailySaveRowset = ShopPromotionDailySave::find()->all();
                        foreach($oShopPromotionDailySaveRowset as $oShopPromotionDailySave){
                    ?>
                        <div class='fc-event' data-id="<?= $oShopPromotionDailySave->product_id ?>"><?= $oShopPromotionDailySave->product_name ?><div class="promo-remove-b" data-id="<?= $oShopPromotionDailySave->product_id ?>"><img src="<?= \Yii::$app->request->baseUrl ?>images/ico/minus.png" title="Usuń z listy promocji"/></div></div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
</div>

<section class="panel" style="width: 67%; float: right;">
    <div class="panel-body">
        <div id='calendar'></div>
    </div>
</section>

<br style="clear: both"/>

<section class="panel full" style="min-height: 1px;">
    <div class="panel-body">
        <div class="row">
            <div class="col-1" style="font-weight: bold;">LEGENDA:</div>
            <div class="col-4">
                <div style="height: 10px; width: 10px; background: rgb(76, 189, 76); display: inline-block;"></div> - Aktywuj/usuń produkt na liście promocji produktu dnia
            </div>
            <div class="col-4">
                <div style="height: 10px; width: 10px; background: #ffae00; display: inline-block;"></div> - Dodaj/usuń produkt z listy promocji dnia do kalendarza
            </div>
            <div class="col-3">
                <div style="height: 10px; width: 10px; background: #cacaca; display: inline-block;"></div> - Promocje wygasłe
            </div>
        </div>
    </div>
</section>