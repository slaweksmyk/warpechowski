<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use yii\helpers\ArrayHelper;
    
    use common\modules\shop\models\ShopDiscountCodes;

    $this->title = "Konfigurator promocji";
?>
<div class="user-data-update">
    <?php $form = ActiveForm::begin(); ?>
        <section class="panel full">
            <header>
                <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Konfigurator darmowej dostawy
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-3">
                        <?= $form->field($model, 'free_shipment_amount')->textInput() ?>
                    </div>
                </div>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('backend_module_articles',  'update'), ['class' => 'btn btn-primary', "onclick" => "$('#w0').submit()"]) ?>
                </div>
            </div>
        </section>
        
        <section class="panel full">
            <header>
                <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Kod rabatowy za zakupy
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-3" style="clear: both;">
                        <?= $form->field($model, 'order_cost')->textInput() ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'order_discount_code')->dropDownList(ArrayHelper::map(ShopDiscountCodes::find()->all(), 'id', 'code'), ['prompt' => "- wybierz kod rabatowy -"]) ?>
                    </div>
                    <div class="col-6">
                        <?= $form->field($model, 'order_title')->textInput() ?>
                    </div>
                    <div class="col-12">
                        <?= $form->field($model, 'order_content')->textarea() ?>
                        <small>*W miejscu {CODE} podstawiony zostanie wybrany kod rabatowy.</small><br/><br/>
                    </div>
                </div>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('backend_module_articles',  'update'), ['class' => 'btn btn-primary', "onclick" => "$('#w0').submit()"]) ?>
                </div>
            </div>
        </section>

        <section class="panel full">
            <header>
                <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Pierwsze zakupy
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-3">
                        <?= $form->field($model, 'first_buy_code')->dropDownList(ArrayHelper::map(ShopDiscountCodes::find()->all(), 'id', 'code'), ['prompt' => "- wybierz kod rabatowy -"]) ?>
                    </div>
                    <div class="col-9">
                        <?= $form->field($model, 'first_buy_title')->textInput() ?>
                    </div>
                    <div class="col-12">
                        <?= $form->field($model, 'first_buy_content')->textarea() ?>
                        <small>*W miejscu {CODE} podstawiony zostanie wybrany kod rabatowy.</small><br/><br/>
                    </div>
                </div>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('backend_module_articles',  'update'), ['class' => 'btn btn-primary', "onclick" => "$('#w0').submit()"]) ?>
                </div>
            </div>
        </section>
        
        <section class="panel full">
            <header>
                <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Zapis do newslettera
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-3">
                        <?= $form->field($model, 'newsletter_sign_code')->dropDownList(ArrayHelper::map(ShopDiscountCodes::find()->all(), 'id', 'code'), ['prompt' => "- wybierz kod rabatowy -"]) ?>
                    </div>
                    <div class="col-9">
                        <?= $form->field($model, 'newsletter_sign_title')->textInput() ?>
                    </div>
                    <div class="col-12">
                        <?= $form->field($model, 'newsletter_sign_content')->textarea() ?>
                        <small>*W miejscu {CODE} podstawiony zostanie wybrany kod rabatowy.</small><br/><br/>
                    </div>
                </div>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('backend_module_articles',  'update'), ['class' => 'btn btn-primary', "onclick" => "$('#w0').submit()"]) ?>
                </div>
            </div>
        </section>
        
        <section class="panel full">
            <header>
                <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Nagroda za rejestracje
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-3">
                        <?= $form->field($model, 'register_code')->dropDownList(ArrayHelper::map(ShopDiscountCodes::find()->all(), 'id', 'code'), ['prompt' => "- wybierz kod rabatowy -"]) ?>
                    </div>
                    <div class="col-9">
                        <?= $form->field($model, 'register_title')->textInput() ?>
                    </div>
                    <div class="col-12">
                        <?= $form->field($model, 'register_content')->textarea() ?>
                        <small>*W miejscu {CODE} podstawiony zostanie wybrany kod rabatowy.</small><br/><br/>
                    </div>
                </div>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('backend_module_articles',  'update'), ['class' => 'btn btn-primary', "onclick" => "$('#w0').submit()"]) ?>
                </div>
            </div>
        </section>
        
        <section class="panel full">
            <header>
                <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Nagroda za synchronizacje danych z facebookiem
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-3">
                        <?= $form->field($model, 'facebook_synch_code')->dropDownList(ArrayHelper::map(ShopDiscountCodes::find()->all(), 'id', 'code'), ['prompt' => "- wybierz kod rabatowy -"]) ?>
                    </div>
                    <div class="col-9">
                        <?= $form->field($model, 'facebook_synch_title')->textInput() ?>
                    </div>
                    <div class="col-12">
                        <?= $form->field($model, 'facebook_synch_content')->textarea() ?>
                        <small>*W miejscu {CODE} podstawiony zostanie wybrany kod rabatowy.</small><br/><br/>
                    </div>
                </div>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('backend_module_articles',  'update'), ['class' => 'btn btn-primary', "onclick" => "$('#w0').submit()"]) ?>
                </div>
            </div>
        </section>
        
        
        
    <?php ActiveForm::end(); ?>
</div>