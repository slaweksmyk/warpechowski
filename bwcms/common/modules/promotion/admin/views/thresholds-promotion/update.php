<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    $this->title = "Progi rabatowe";
    
    $this->registerCssFile('/bwcms/common/modules/promotion/admin/assets/css/thresholds.css');

    $this->registerJsFile('/bwcms/common/modules/promotion/admin/assets/js/thresholds.js', ['depends' => [yii\web\JqueryAsset::className()]]);
?>

<section class="panel full">
    <header>
        <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Progi rabatowe
    </header>
    <div class="panel-body">
        <?php $form = ActiveForm::begin(); ?>
            <?php foreach($oPromotionThresholdsRowset as $index => $oPromotionThreshold){ ?>
                <div class="row t-el">
                    <div class="col-1 t-number"><?= $index+1 ?>.</div>
                    <div class="col-10">
                        <div class="row">
                            <div class="col-8">
                                <div class="row">
                                    <div class="col-5">
                                        <?= $form->field($model, 'order_amount_low[]')->textInput(["value" => $oPromotionThreshold->order_amount_low, "style" => "text-align: center;"]) ?>
                                    </div>
                                    <div class="col-1 t-text">-</div>
                                    <div class="col-5">
                                        <?= $form->field($model, 'order_amount_high[]')->textInput(["value" => $oPromotionThreshold->order_amount_high, "style" => "text-align: center;"]) ?>
                                    </div>
                                    <div class="col-1 t-text">→</div>
                                </div>
                            </div>
                            <div class="col-4">
                                <?= $form->field($model, 'discount_amount[]')->textInput(["value" => $oPromotionThreshold->discount_amount."%"]) ?>
                            </div>
                        </div>
                    </div>
                    <?php if($index > 1){ ?>
                        <div class="col-1">
                            <a href="<?= Url::to(['delete', 'id' => $oPromotionThreshold->id]); ?>"><img src="<?= \Yii::$app->request->baseUrl ?>images/ico/delete.png" class="img-responsive t-delete"/></a>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
        
            <div class="form-group">
                <button class="btn btn-success t-add">dodaj próg</button> 
                <button type="submit" class="btn btn-primary">zapisz</button>         
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</section>