<?php
    use yii\helpers\Html;
    use common\modules\shop\models\Product;
    use common\modules\shop\models\Category;
    
    $this->title = "Łączenie produktów";
    
    $this->registerJsFile('/bwcms/common/modules/promotion/admin/assets/js/connection.js', ['depends' => [yii\web\JqueryAsset::className()]]);
    $this->registerJsFile('/bwcms/common/modules/promotion/admin/assets/js/jquery-ui.min.js', ['depends' => [yii\web\JqueryAsset::className()]]);

    $this->registerCssFile('/bwcms/common/modules/promotion/admin/assets/css/connection.css');
?>

<div class="bw-popup">
    <section class="pop-cnt panel">
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Lista produktów
        </header>
        <div class="panel-body">
            <div class="row" id="pop-elements"></div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary pop-close">zaktualizuj</button>                    
            </div>
        </div>
    </section>
</div>

<div class="row">
    <div class="col-4">
        <section class="panel full">
            <header>
                <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Wyszukaj produkt 
            </header>
            <div class="panel-body" id="search">
                <input type="text" placeholder="Nazwa produktu"/>
            </div>
        </section>
        
        <section class="panel full" id="product-list">
            <div class="panel-body">
                <div class="row">
                    <div class="col-12">
                        <h2>Lista produktów</h2>
                        <h3>Wszystkie</h3>
                        <img src="<?= \Yii::$app->request->baseUrl ?>images/ico/cat-back.png" alt="back" class="cat-up"/>
                    </div>
                </div>
                <div class="row list"></div>
            </div>
        </section>
    </div>
    <div class="col-8">
        <section class="panel full" id="drop-area">
            <div class="panel-body">
                <div class="row">
                    <div class="col-12 drop-head">
                        <h2>Stwórz własną promocje</h2>
                        <p>Przeciągnij przedmiot z listy produktów <img src="<?= \Yii::$app->request->baseUrl ?>images/ico/swipe-ico.png" alt="swipe"/></p>
                    </div>
                </div>
                
                <?php $iIndex = 1; ?>
                <?php foreach($oPromotionConnection as $oConnection){ ?>
                    <div class="row promotion-el">
                        <div class="col-2 promo-options">
                            <div class="row">
                                <div class="col-6"><?= $iIndex ?>.</div>
                                <div class="col-6">
                                    <div class="prod-add"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-10">
                            <div class="row">
                                <?php 
                                $i = 0;
                                foreach($oConnection->decodeElements() as $sType => $aElements){ 
                                    $i++;
                                    $j = 0;
                                    foreach($aElements as $key => $value){ $j++; ?>
                                        <?php
                                            $sID = $sStyles = $sName = $sClass = null;
                                            $isCat = "false";
                                            switch($sType){
                                                case "category":
                                                    $oCategory = Category::find()->where(["=", "id", $value])->one();
                                                    
                                                    $sID = $value;
                                                    $sStyles = "background: #ffae00 url(".\Yii::$app->request->baseUrl."images/ico/dir-ico.png) no-repeat center;";
                                                    $sName = $oCategory->name;
                                                    $isCat = "true";
                                                    break;
                                                
                                                case "product":
                                                    $oProduct = Product::find()->where(["=", "id", $value])->one();
                                                    
                                                    $sID = $value;
                                                    $sStyles = "background: url(".$oProduct->getThumbnail(2000,2000, "matched").") no-repeat center; background-size: cover;";
                                                    $sName = $oProduct->name;
                                                    break;
                                                
                                                case "group":
                                                    $sID = "[".implode(",", $value)."]";
                                                    $sClass = "category";
                                                    $sName = "Grupa";
                                                    break;
                                                
                                            }
                                        ?>
                                        <div class="col-4">
                                            <div class="drop-box">
                                                <div class="row">
                                                    <div class="col-8">
                                                        <div class="el-drop <?= $sClass ?>" data-id="<?= $sID ?>" style="<?= $sStyles ?>" data-iscat="<?= $isCat ?>">
                                                            <div class="g-edit"></div>
                                                        </div>
                                                        <p><?= $sName ?></p>
                                                    </div>
                                                    <div class="col-4">
                                                        <?php if($j == count($aElements) && $i == count((array) $oConnection->decodeElements()) ){ ?>
                                                        <?php } else { ?>+<?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                                
                                <div class="col-11 finish-prod">
                                    <div class="row">
                                        <div class="col-1">=</div>
                                        <div class="col-3">
                                            <?php if($oConnection->type == 0){ ?>
                                                <div class="el-drop no-remove" data-id="<?= $oConnection->reward_id ?>" data-iscat="false" style="background-image: url(<?= $oConnection->getRewardProduct()->one()->getThumbnail(2000,2000,"matched") ?>); background-size: cover;"></div>
                                                <p><?= $oConnection->getRewardProduct()->one()->name ?></p>
                                            <?php } else { ?>
                                                <div class="el-drop no-remove" data-id="<?= $oConnection->reward_id ?>" data-iscat="false" style="background: #ffae00 url(<?= \Yii::$app->request->baseUrl ?>images/ico/dir-ico.png) no-repeat center;"></div>
                                                <p><?= $oConnection->getRewardCategory()->one()->name ?></p>
                                            <?php } ?>
                                        </div>
                                        <div class="col-3" style="line-height: 1;">
                                            <h3>Wysokość promocji</h3>
                                            <input type="text" value="<?= $oConnection->discount_amount ?>%"/>
                                            <span>Usuń promocję<img src="<?= \Yii::$app->request->baseUrl ?>images/ico/ico-delete.png" alt="delete" style="position: relative; left: 6px;"/></span>
                                        </div>
                                        <div class="col-4 col-offset-1" style="line-height: 1;">
                                            <h3>Data rozpoczęcia</h3>
                                            <input type="text" name="date_start" value="<?= date("d.m.Y", strtotime($oConnection->date_start)) ?>" class="datepicker"/>

                                            <h3>Data zakończenia</h3>
                                            <input type="text" name="date_end" value="<?= date("d.m.Y", strtotime($oConnection->date_end)) ?>" class="datepicker"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php $iIndex++; } ?>
                
                <div class="row promotion-el">
                    <div class="col-2 promo-options">
                        <div class="row">
                            <div class="col-6"><?= $iIndex ?>.</div>
                            <div class="col-6">
                                <div class="prod-add"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-10">
                        <div class="row">
                            <?php for($i=1;$i<=2;$i++){ ?>
                                <div class="col-4">
                                    <div class="drop-box">
                                        <div class="row">
                                            <div class="col-8">
                                                <div class="el-drop">
                                                    <div class="g-edit"></div>
                                                </div>
                                                <p></p>
                                            </div>
                                            <div class="col-4">
                                                <?php if($i != 2){ ?>+<?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                            <div class="col-11 finish-prod">
                                <div class="row">
                                    <div class="col-1">=</div>
                                    <div class="col-3">
                                        <div class="el-drop no-remove"></div>
                                        <p></p>
                                    </div>
                                    <div class="col-3" style="line-height: 1;">
                                        <h3>Wysokość promocji</h3>
                                        <input type="text" name="amount" value="0%"/>
                                        <span>Usuń promocję<img src="<?= \Yii::$app->request->baseUrl ?>images/ico/ico-delete.png" alt="delete" style="position: relative; left: 6px;"/></span>
                                    </div>
                                    <div class="col-4 col-offset-1" style="line-height: 1;">
                                        <h3>Data rozpoczęcia</h3>
                                        <input type="text" name="date_start" value="" class="datepicker"/>

                                        <h3>Data zakończenia</h3>
                                        <input type="text" name="date_end" value="" class="datepicker"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="add-promo-box"><img src="<?= \Yii::$app->request->baseUrl ?>images/add-promo.png"/> Dodaj nową promocję</div>
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary pop-close pull-right save-promo">zaktualizuj</button>                    
                </div>
            </div>
        </section>
    </div>
</div>