<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use common\modules\shop\models\Product;
    
    $this->title = "Obniżka cen produktów";

    $this->registerCssFile('/bwcms/common/modules/promotion/admin/assets/css/reduction.css');
?>

<div class="row">
    <div class="col-12 text-right" style="margin-bottom: 20px;">
        <?= Html::a("Przejdź w tryb detaliczny", ["update"], ['class' => 'btn btn-outline-secondary']) ?>
    </div>
</div>

<section class="panel full" id="drop-area">
    <div class="panel-body">
        <div class="table-responsive" id="ws-table">
            <h4>Dodaj produkt</h4>
            <?php $form = ActiveForm::begin(); ?>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Produkt</th>
                            <th scope="col">Obniżka kwotowa</th>
                            <th scope="col">Obniżka procentowa</th>
                            <th scope="col">Data rozpoczęcia</th>
                            <th scope="col">Data zakończenia</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="prod"><select class="ajax-load-product" name="product_id"></select></td>
                            <td><input type="text" class="form-control" name="rev_cash" value="<?= $oPromotionConnection->amount_sum ?>"/></td>
                            <td><input type="text" class="form-control" name="rev_perc" value="<?= $oPromotionConnection->amount_perc ?>"/></td>
                            <td><input type="text" class="form-control datepicker" name="date_start" value="<?= $oPromotionConnection->date_start ?>"/></td>
                            <td><input type="text" class="form-control datepicker" name="date_end" value="<?= $oPromotionConnection->date_end ?>"/></td>
                        </tr>
                    </tbody>
                </table>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary pull-right save-promo">dodaj</button>                    
                </div>
            <?php ActiveForm::end(); ?> 
            
            <?php $form = ActiveForm::begin(); ?>
                <h4 style="clear: both;">Lista obniżek</h4>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Produkt</th>
                            <th scope="col">Obniżka kwotowa</th>
                            <th scope="col">Obniżka procentowa</th>
                            <th scope="col">Data rozpoczęcia</th>
                            <th scope="col">Data zakończenia</th>
                            <th scope="col">Akcje</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($oPromotionReductionRowset as $oPromotionReduction){  ?>
                            <?php $oProduct = Product::find()->where(["=", "id", $oPromotionReduction->product_id])->one(); ?>
                            <tr>
                                <td class="prod"><?= $oProduct->name ?></td>
                                <td><input type="text" class="form-control" name="promotion[<?= $oPromotionReduction->id ?>][rev_cash]" value="<?= $oPromotionReduction->amount_sum ?>"/></td>
                                <td><input type="text" class="form-control" name="promotion[<?= $oPromotionReduction->id ?>][rev_perc]" value="<?= $oPromotionReduction->amount_perc ?>"/></td>
                                <td><input type="text" class="form-control datepicker" name="promotion[<?= $oPromotionReduction->id ?>][date_start]" value="<?= date("Y-m-d", strtotime($oPromotionReduction->date_start)) ?>"/></td>
                                <td><input type="text" class="form-control datepicker" name="promotion[<?= $oPromotionReduction->id ?>][date_end]" value="<?= date("Y-m-d", strtotime($oPromotionReduction->date_end)) ?>"/></td>
                                <td><a href="<?= \Yii::$app->request->baseUrl ?>promotions-list/reduction-promotion/delete/?id=<?= $oPromotionReduction->id ?>" title="<?= Yii::t('backend', 'delete') ?>" data-toggle="tooltip" data-placement="bottom" data-confirm="Czy na pewno usunąć ten element?" data-method="POST"><span class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></span></a></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary pop-close pull-right save-promo">zapisz</button>                    
                </div>
            <?php ActiveForm::end(); ?> 
            
        </div>
    </div>
</section>