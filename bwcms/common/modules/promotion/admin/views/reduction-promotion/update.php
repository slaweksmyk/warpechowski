<?php
    use yii\helpers\Html;
    use common\modules\shop\models\Product;
    use common\modules\shop\models\Category;
    
    $this->title = "Obniżka cen produktów";
    
    $this->registerJsFile('/bwcms/common/modules/promotion/admin/assets/js/reduction.js', ['depends' => [yii\web\JqueryAsset::className()]]);
    $this->registerJsFile('/bwcms/common/modules/promotion/admin/assets/js/jquery-ui.min.js', ['depends' => [yii\web\JqueryAsset::className()]]);

    $this->registerCssFile('/bwcms/common/modules/promotion/admin/assets/css/reduction.css');
?>

<div class="row">
    <div class="col-12 text-right" style="margin-bottom: 20px;">
        <?= Html::a("Przejdź w tryb hurtowy", ["wholesale"], ['class' => 'btn btn-outline-secondary']) ?>
    </div>
</div>


<div class="row">
    <div class="col-4">
        <section class="panel full" style="padding-bottom: 10px;">
            <header>
                <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Wyszukaj produkt 
            </header>
            <div class="panel-body" id="search">
                <input type="text" placeholder="Nazwa produktu"/>
            </div>
        </section>
        
        <section class="panel full" id="product-list">
            <div class="panel-body">
                <div class="row">
                    <div class="col-12">
                        <h2>Lista produktów</h2>
                        <h3>Wszystkie</h3>
                        <img src="<?= \Yii::$app->request->baseUrl ?>images/ico/cat-back.png" alt="back" class="cat-up"/>
                    </div>
                </div>
                <div class="row list"></div>
            </div>
        </section>
    </div>
    <div class="col-8">
        <section class="panel full" id="drop-area">
            <div class="panel-body">
                <div class="row">
                    <div class="col-12 drop-head">
                        <h2>Stwórz własną promocje</h2>
                        <p>Przeciągnij przedmiot z listy produktów <img src="<?= \Yii::$app->request->baseUrl ?>images/ico/swipe-ico.png" alt="swipe"/></p>
                    </div>
                </div>
                <?php $i=1; ?>
                <?php foreach($oPromotionReductionRowset as $iIndex => $oPromotionReduction){  ?>
                    <div class="row promotion-el">
                        <div class="col-1 promo-options">
                            <?= $i ?>.
                        </div>
                        <div class="col-11">
                            <div class="row">
                                <?php 
                                    if(!is_null($oPromotionReduction->product_id)){ 
                                        $oProduct = Product::find()->where(["=", "id", $oPromotionReduction->product_id])->one();
                                ?>
                                    <div class="col-3">
                                        <div class="drop-box">
                                            <div class="el-drop" data-id="<?= $oProduct->id ?>" data-iscat="0" style="<?php echo "background: url(".$oProduct->getThumbnail(2000,2000, "matched").") no-repeat center; background-size: cover;"; ?>">
                                                <div class="g-edit"></div>
                                            </div>
                                            <p><?= $oProduct->name ?></p>
                                        </div>
                                    </div>
                                <?php  } else { 
                                    $oCategory = Category::find()->where(["=", "id", $oPromotionReduction->category_id])->one();
                                ?>
                                    <div class="col-3">
                                        <div class="drop-box">
                                            <div class="el-drop" data-id="<?= $oCategory->id ?>" data-iscat="1" style="background: #ffae00 url(<?= \Yii::$app->request->baseUrl ?>images/ico/dir-ico.png) no-repeat center;">
                                                <div class="g-edit"></div>
                                            </div>
                                            <p><?= $oCategory->name ?></p>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="col-3" style="line-height: 1;">
                                    <h3>Obniżka<br/>kwotowa</h3>
                                    <input type="text" name="rev_cash" value="<?= $oPromotionReduction->amount_sum ?>zł"/>
                                    
                                    <div>
                                        <span>Usuń promocję<img src="<?= \Yii::$app->request->baseUrl ?>images/ico/ico-delete.png" alt="delete" style="position: relative; left: 6px;"/></span>
                                    </div>
                                </div>
                                <div class="col-3" style="line-height: 1;">
                                    <h3>Obniżka<br/>procentowa</h3>
                                    <input type="text" name="rev_perc" value="<?= $oPromotionReduction->amount_perc ?>%"/>
                                </div>
                                <div class="col-3" style="line-height: 1;">
                                    <h3>Data rozpoczęcia</h3>
                                    <input type="text" name="date_start" value="<?= date("Y-m-d", strtotime($oPromotionReduction->date_start)) ?>" class="datepicker"/>

                                    <h3>Data zakończenia</h3>
                                    <input type="text" name="date_end" value="<?= date("Y-m-d", strtotime($oPromotionReduction->date_end)) ?>" class="datepicker"/>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php $i++; } ?>

                <div class="row promotion-el">
                    <div class="col-1 promo-options">
                        <?= $i ?>.
                    </div>
                    <div class="col-11">
                        <div class="row">
                            <div class="col-3">
                                <div class="drop-box">
                                    <div class="el-drop">
                                        <div class="g-edit"></div>
                                    </div>
                                    <p></p>
                                </div>
                            </div>
                            <div class="col-3">
                                <h3>Obniżka<br/>kwotowa</h3>
                                <input type="text" name="rev_cash" value="0zł"/>
                            </div>
                            <div class="col-3">
                                <h3>Obniżka<br/>procentowa</h3>
                                <input type="text" name="rev_perc" value="0%"/>
                            </div>
                            <div class="col-3" style="line-height: 1;">
                                <h3>Data rozpoczęcia</h3>
                                <input type="text" name="date_start" value="" class="datepicker"/>

                                <h3>Data zakończenia</h3>
                                <input type="text" name="date_end" value="" class="datepicker"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="add-promo-box"><img src="<?= \Yii::$app->request->baseUrl ?>images/add-promo.png"/> Dodaj nową promocję</div>
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary pop-close pull-right save-promo">zaktualizuj</button>                    
                </div>
            </div>
        </section>
    </div>
</div>









