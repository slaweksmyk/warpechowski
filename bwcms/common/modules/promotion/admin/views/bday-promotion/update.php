<?php
    use yii\helpers\Html;
     use common\hooks\yii2\grid\GridView;
    use yii\widgets\ActiveForm;
    use yii\helpers\ArrayHelper;
    use yii\data\ArrayDataProvider;
    use yii\data\ActiveDataProvider;
    
    use common\modules\users\models\User;
    use common\modules\users\models\UserData;
    use common\modules\shop\models\ShopDiscountCodes;
    
    $this->title = "Promocja urodzinowa/imienionowa";
    
    $this->registerCssFile('/bwcms/common/modules/promotion/admin/assets/css/fullcalendar.css');
    $this->registerCssFile('/bwcms/common/modules/promotion/admin/assets/css/bday.css');
    
    $this->registerJsFile('/bwcms/common/modules/promotion/admin/assets/js/moment.min.js', ['depends' => [yii\web\JqueryAsset::className()]]);
    $this->registerJsFile('/bwcms/common/modules/promotion/admin/assets/js/jquery-ui.min.js', ['depends' => [yii\web\JqueryAsset::className()]]);
    $this->registerJsFile('/bwcms/common/modules/promotion/admin/assets/js/fullcalendar.js', ['depends' => [yii\web\JqueryAsset::className()]]);
    $this->registerJsFile('/bwcms/common/modules/promotion/admin/assets/js/locale/pl.js', ['depends' => [yii\web\JqueryAsset::className()]]);
    $this->registerJsFile('/bwcms/common/modules/promotion/admin/assets/js/bday.js', ['depends' => [yii\web\JqueryAsset::className()]]);
    
?>
<div class="user-data-update">

    <p>
        <?= Html::a("< Wroć do listy promocji", \Yii::$app->request->baseUrl."promotions-list/", ['class' => 'btn btn-primary']) ?>
    </p>
    
    <section class="panel full switch-cal" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Konfiguracja promocji
            <img src="<?= \Yii::$app->request->baseUrl ?>images/ico/cal-ico.png" alt="calendar" class="switch-calendar" style="float: right; position: relative; cursor: pointer; top: -3px;"/>
        </header>
        <div class="panel-body">
            <div class="row">
                <div class="col-12">
                    <?php $form = ActiveForm::begin(); ?>
                    <div class="row">
                        <div class="col-6">
                            <?= $form->field($oBday, 'bday_discount_id')->dropDownList(ArrayHelper::map(ShopDiscountCodes::find()->all(), 'id', 'code'), ['prompt' => "- wybierz kod rabatowy -"])->label("Kod dla osoby mającej urodziny") ?>
                        </div>
                        <div class="col-6">
                            <?= $form->field($oBday, 'nday_discount_id')->dropDownList(ArrayHelper::map(ShopDiscountCodes::find()->all(), 'id', 'code'), ['prompt' => "- wybierz kod rabatowy -"])->label("Kod dla osoby mającej imieniny") ?>
                        </div>
                    </div>
                    
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <?= GridView::widget([
                            'dataProvider' => new ActiveDataProvider([
                                'query' => $oBdayRowset,
                                'pagination' => [
                                    'pageSize' => 10,
                                ],
                            ]),
                            'summary' => "<b>Nadchodzące urodziny</b>",
                            'columns' => [
                                'id',
                                [
                                    'attribute' => 'user_id',
                                    'label' => "Imię i nazwisko",
                                    'format' => 'raw',
                                    'value' => function ($data) {
                                        $oUser = User::findOne(["=", "id", $data["user_id"]]);
                                        if(!is_null($oUser)){
                                            return "{$oUser->getData()->one()->firstname} {$oUser->getData()->one()->surname}";
                                        }
                                    }
                                ],
                                [
                                    'attribute' => 'birth_date',
                                    'format' => 'raw',
                                    'value' => function ($data) {
                                        return date("d.m.Y", strtotime($data["birth_date"]));
                                    }
                                ],
                                [
                                    'attribute' => 'user_id',
                                    'label' => "uID",
                                    'format' => 'raw'
                                ],   
                            ]
                        ]);
                    ?>
                </div>
                <div class="col-6">
                    <?= GridView::widget([
                            'dataProvider' => new ArrayDataProvider([
                                'allModels' => $aNdataRowset,
                                'pagination' => [
                                    'pageSize' => 10,
                                ],
                            ]),
                            'summary' => "<b>Nadchodzące imieniny</b>",
                            'columns' => [
                                'id',
                                [
                                    'attribute' => 'name',
                                    'label' => "Imieniny"
                                ],
                                [
                                    'attribute' => 'date',
                                    'label' => "Data"
                                ]
                            ]
                        ]);
                    ?>
                </div>
            </div>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('backend_module_articles',  'update'), ['class' => 'btn btn-primary', "onclick" => "$('#w0').submit()"]) ?>
            </div>
        </div>
    </section> 
    
    <section class="panel full switch-cal" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Konfiguracja powiadomień
        </header>
        <div class="panel-body">
            <?php $form = ActiveForm::begin(); ?>
                <div class="row">
                    <div class="col-6">
                        <?= $form->field($oBday, 'bday_email_title')->textInput()->label("Tytuł wysyłanej wiadomości") ?>
                    </div>
                    <div class="col-6">
                        <?= $form->field($oBday, 'nday_email_title')->textInput()->label("Tytuł wysyłanej wiadomości") ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <?= $form->field($oBday, 'bday_email_body')->textArea()->label("Treść wysyłanej wiadomości") ?>
                        <small>*W miejscu {CODE} podstawiony zostanie wybrany kod rabatowy.</small>
                    </div>
                    <div class="col-6">
                        <?= $form->field($oBday, 'nday_email_body')->textArea()->label("Treść wysyłanej wiadomości") ?>
                        <small>*W miejscu {CODE} podstawiony zostanie wybrany kod rabatowy.</small>
                    </div>
                </div>
                <div class="form-group">
                    <br/>
                    <?= Html::submitButton(Yii::t('backend_module_articles',  'update'), ['class' => 'btn btn-primary', "onclick" => "$('#w0').submit()"]) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </section>
    
    <section class="panel full switch-cal cal-hidden">
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Kalendarz
            <img src="<?= \Yii::$app->request->baseUrl ?>images/ico/cal-ico.png" alt="calendar" class="switch-calendar" style="float: right; position: relative; cursor: pointer; top: -3px;"/>
        </header>
        <div class="panel-body">
            <div id="calendar"></div>
        </div>
    </section>
    
    <script>
        var events = [
            <?php foreach(UserData::find()->where(["IS NOT", "birth_date", NULL])->all() as $oUserData){ ?>
                {
                    title: '<?= $oUserData->firstname ?> <?= $oUserData->surname ?>',
                    start: '<?= date("Y-m-d", strtotime($oUserData->birth_date)) ?>',
                    end: '<?= date("Y-m-d", strtotime($oUserData->birth_date)) ?>',
                    allDay: true,
                    durationEditable: false,
                    backgroundColor: '#ffae00',
                    borderColor: '#ffae00',
                },
            <?php } ?>
        ];
        var events2 = [
            <?php foreach($oBday->nDayData() as $date => $names){ ?>
                {
                    title: '<?= $names ?>',
                    start: '<?= date("Y-").$date ?>',
                    end: '<?= date("Y-").$date ?>',
                    allDay: true,
                    durationEditable: false,
                    editable: false,
                },
            <?php } ?>
        ];
    </script>

</div>
