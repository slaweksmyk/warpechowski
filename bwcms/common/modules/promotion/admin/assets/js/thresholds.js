$(document).ready(function(){
    
    $(".t-add").click(function(){
        var el = $(".t-el").first().clone();
        
        el.find("input").val("");
        el.find(".t-number").text($(".t-el").length+1);
        el.find(".t-delete").remove();
        
        $(this).parent().before(el);
        return false;
    });
    
});