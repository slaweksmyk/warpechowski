$(document).ready(function(){
    var typingTimer;
    var cid = null;
    var search = null;
    
    var dropableOptions = {
        tolerance: "intersect",
        accept: ".draggable",
        drop: function(event, ui) {    
            var el = $(ui.draggable).clone();
            var img =  el.find(".product-thumb").css("background-image");

            if(typeof img === 'undefined'){
                img = el.find(".category-thumb").css("background-image");
                $(this).css("background-color", "#ffae00");
                $(this).css("background-image", img).css("background-size", "auto");
                $(this).data("iscat", true);
            } else {
                $(this).css("background-image", img).css("background-size", "cover");
                $(this).css("background-color", "white");
                $(this).data("iscat", false);
            }

            $(this).data("id", ui.draggable.data("id"));
            $(this).next().text(el.find("p").text());
        }
    };
    
    var dragableOptions = {
        cursor: "move",
        helper: 'clone',
        revert: "invalid",
        start: function(event, ui) {
            $(ui.helper).css("z-index", 60).find(".prod-add").remove();

            $("#product-list .list").css("overflow", "initial");

            if($('.product-thumb .prod-add.checked').length > 0){
                $(this).draggable().draggable("option", {revert: false});
                $(ui.helper).css("opacity", 0);
            } else {
                $(this).draggable().draggable("option", {revert: "invalid"});
            }
        },
        stop: function(event, ui) {
            $("#product-list .list").css("overflow", "auto");
        }
    };
    
    /*******************/
    
    $(".el-drop").droppable(dropableOptions);
    
    $(".save-promo").click(function(){
        $isOk = true;
        $aSave = {};
        
        $i = 0;
        $(".promotion-el").each(function(){
            if(typeof $(this).find(".el-drop").data("id") != 'undefined' && $(this).find(".el-drop").data("id")){
                $aSave[$i] = {};

                $isCat = 0;
                $dateStart = $(this).find("input[name='date_start']").val();
                $dateEnd   = $(this).find("input[name='date_end']").val();

                if($dateStart == ""){
                    $(this).find("input[name='date_start']").css("border", "1px solid red");
                    $isOk = false;
                }

                if($dateEnd == ""){
                    $(this).find("input[name='date_end']").css("border", "1px solid red");
                    $isOk = false;
                }


                $aSave[$i]["date_start"] = $dateStart;
                $aSave[$i]["date_end"] = $dateEnd;
                $aSave[$i]["isCat"] = $(this).find(".el-drop").data("iscat");
                $aSave[$i]["element"] = $(this).find(".el-drop").data("id");
                $aSave[$i]["rev_cash"] = $(this).find("input[name='rev_cash']").val();
                $aSave[$i]["rev_perc"] = $(this).find("input[name='rev_perc']").val();

                $i++;
            }
        });

        if(!$isOk){
            alert("Proszę poprawić błędy");
            return;
        }

        $.ajax({
            url: $("#homeDir").text()+"promotions-list/reduction-promotion/save/",
            method: "POST",
            data: {
                data: JSON.stringify($aSave)
            },
            cache: false
        })
        .done(function(data) {
            location.reload();
        });
        
    });
    
    $("body").on("click", ".promotion-el span", function() {
        if($(".promotion-el").length > 1){
            var $this = $(this);
            $(this).closest(".promotion-el").fadeOut("slow", function(){
                $this.closest(".promotion-el").remove();
            });
        }
    });
    
    $(".add-promo-box").click(function(){
        var schema = $(".promotion-el").first().clone();
        schema.find(".el-drop").css("background-image", "url("+$("#homeDir").text()+"images/ico/drag-ico.png)").css("background-size", "auto").css("background-color", "white").data("id", null);
        schema.find("p").text("");
        schema.find(".promo-options").text(($(".promotion-el").length + 1) + ".");
        schema.find(".el-drop:not(.no-remove)").each(function(index, value){
            if(index >= 2){
                $(this).parent().parent().parent().remove();
            }
            if(index === 1){
                $(this).parent().next().remove();
            }
        });
        
        schema.find(".el-drop").droppable(dropableOptions);
        
        $(".add-promo-box").parent().parent().before(schema);
    });
    
    $(".cat-up").click(function(){
        cid = null;
        getProductSectionData();
    });
    
    $(document).bind('keyup', function(event) {
        if(event.which === 65 && event.shiftKey ) {
            $(".product-thumb .prod-add").addClass("checked");
        }
        
        if(event.which === 27 && cid > 0) {
            cid = null;
            getProductSectionData();
        }
    });
    
    $("body").on("dblclick", ".category-el", function() {
        cid = $(this).data("id");
        getProductSectionData();
    });
    
    $("#search input").keyup(function(){
        var el = $(this);
        
        clearTimeout(typingTimer);
        typingTimer = setTimeout(function(){
            search = el.val();
            getProductSectionData();
        }, 500);
    });
    
    function getProductSectionData(){
        $(".list").html("");

        $.ajax({
            url: $("#homeDir").text()+"promotions-list/connection-promotion/get/",
            method: "GET",
            data: { 
                cid: cid,
                search: search
            },
            cache: false,
            dataType: "json"
        })
        .done(function(data) {
            $("#product-list h3").text(data.sCategoryName);
            
            if(cid !== null){ $(".cat-up").show(); } 
            else { $(".cat-up").hide(); }
            
            prepareCategoryData(data.oCategoryRowset);
            prepareProductData(data.oProductRowset);
        });
    }
    
    function prepareCategoryData(oCategoryRowset){
        var sCatStr = "";

        $.each(oCategoryRowset, function( index, value ) {
            sCatStr += '<div class="col-4">';
               sCatStr += '<div class="category-el draggable" data-id="'+value.id+'">';
                    sCatStr += '<div class="category-thumb" style="background-image: url('+$("#homeDir").text()+'images/ico/dir-ico.png)"></div>';
                    sCatStr += '<p>'+value.name+'</p>';
                sCatStr += '</div>';
            sCatStr += '</div>';
        });

        $(".list").prepend(sCatStr);
        $(".category-el").width($(".category-el").outerWidth());
    }
    
    function prepareProductData(oProductRowset){
        var sProdStr = "";

        $.each(oProductRowset, function( index, value ) {
            sProdStr += '<div class="col-4">';
               sProdStr += '<div class="product-el draggable" data-id="'+value.id+'">';
                    sProdStr += '<div class="product-thumb" style="background-image: url('+value.thumbnail+'); background-size: cover;"></div>';
                    sProdStr += '<p>'+value.name+'</p>';
                sProdStr += '</div>';
            sProdStr += '</div>';
        });

        $(".list").append(sProdStr);
        $(".product-el").width($(".product-el").outerWidth());
        
        $('.draggable').draggable(dragableOptions);
    }
    
    getProductSectionData();
    
});