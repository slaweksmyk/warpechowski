$(document).ready(function() {
    var clickedHash;
    var clickedEvent;
    var isNew = false;

    var isBlocked = false;
    /* initialize the external events
    -----------------------------------------------------------------*/
    
    $(".popup-clear").click(function(){
        $(".pop_cnt").find("input").val("");
        
        $("#pop-submit").removeClass("btn-primary");
        isBlocked = true;
    });
    
    $(".cal-popup-close").click(function(){
        $("#cal-popup").fadeOut();
    });
    
    $(".popup-close").click(function(){
        $("#cal-popup").fadeOut();
    });

    $('#external-events .fc-event').each(function() {

        // store data so the calendar knows to render an event upon drop
        $(this).data('event', {
            product_id: $(this).data("id"),
            title: $.trim($(this).text()),
            stick: true,
            hash: '',
            backgroundColor: '#ffae00',
            borderColor: '#ffae00',
        });

        // make the event draggable using jQuery UI
        $(this).draggable({
            zIndex: 999,
            revert: true,
            revertDuration: 0,
            start: function() {
                $("#external-events").css("overflow", "initial");
            },
            stop: function() {
                $("#external-events").css("overflow", "auto");
            }
        });

    });
    
    $(window).on("load", function(){
		if(!localStorage.aTempElements) { localStorage.aTempElements = "[]"; }
        var aTempElements = jQuery.parseJSON(localStorage.aTempElements);

        if(aTempElements.length > 0){
            $.each(aTempElements, function( index, value ) {
                var newEl = $(document.createElement('div'));
                newEl.addClass("fc-event");
                newEl.css("background", "#4cbd4c");
                newEl.css("border-color", "#4cbd4c");
                newEl.attr("data-id", value.ID);
                newEl.text(value.Name);
                newEl.append('<div class="promo-remove-b"><img src=$("#homeDir").text()+"images/ico/minus.png" title="Usuń z listy promocji"/></div><div class="promo-remove-b" data-id="'+value.ID+'"><img src=$("#homeDir").text()+"images/ico/minus.png" title="Usuń z listy promocji"/></div><div class="promo-add-b" data-id="'+value.ID+'" data-name="'+value.Name+'"><img src=$("#homeDir").text()+"images/ico/plus.png" title="Dodaj do listy promocji"/></div>');

                $("#events-list").prepend(newEl);
            });
        }
        
    });

    var aTempElements = [];
    $("body").on("change", "#search select", function() {
        $(this).children().each(function(){
            var newEl = $(document.createElement('div'));
            newEl.addClass("fc-event");
            newEl.css("background", "#4cbd4c");
            newEl.css("border-color", "#4cbd4c");
            newEl.attr("data-id", $(this).attr("value"));
            newEl.text($(this).text());
            newEl.append('<div class="promo-remove-b"><img src=$("#homeDir").text()+"images/ico/minus.png" title="Usuń z listy promocji"/></div><div class="promo-remove-b" data-id="'+$(this).attr("value")+'"><img src=$("#homeDir").text()+"images/ico/minus.png" title="Usuń z listy promocji"/></div><div class="promo-add-b" data-id="'+$(this).attr("value")+'" data-name="'+$(this).text()+'"><img src=$("#homeDir").text()+"images/ico/plus.png" title="Dodaj do listy promocji"/></div>');
            
            $("#events-list").prepend(newEl);
            
            var aElement = { "ID": $(this).attr("value"), "Name": $(this).text() };

            aTempElements.push(aElement);
            if (typeof(Storage) !== "undefined") {
                localStorage.setItem("aTempElements", JSON.stringify(aTempElements));
            }
            
            $(this).remove();
        });
        return false;
    });
    
    var savedProducts = [];
    $("body").on("click", ".promo-add-b", function() {
        var el = $(this);

        $.post($("#homeDir").text()+"promotions-list/daily-promotion/ajax-save/", { id: $(this).data("id"), name: $(this).data("name") })
        .done(function( data ) {
            el.parent().attr("style", "");
            el.parent().find(".promo-add-b").remove();
            
            var aNewTempElements = [];
            var aTempElements = jQuery.parseJSON(localStorage.aTempElements);
            if(aTempElements.length > 0){
                $.each(aTempElements, function( index, value ) {
                    if(value.ID != el.data("id")){
                        aNewTempElements.push({ "ID": value.ID, "Name": value.Name });
                    }
                });
                if (typeof(Storage) !== "undefined") {
                    localStorage.setItem("aTempElements", JSON.stringify(aNewTempElements));
                }
            }
            
            location.reload();
        });
    });
    
    $("body").on("click", ".promo-remove-b", function() {
        
        var el = $(this).parent();
        if($(this).data("id")){
            $.post($("#homeDir").text()+"promotions-list/daily-promotion/ajax-delete/", { id: $(this).data("id") })
            .done(function( data ) {
                el.remove();
            });
        } else {
            $(this).remove();
        }
    });
    
    $("#cal-popup input").change(function(){
        var isOK = true;
        $("#cal-popup input").each(function(){
            if($(this).val() == ""){
                isOK = false;
            }
        });
        
        if(isOK){
            $("#pop-submit").addClass("btn-primary");
            isBlocked = false;
        }
    });
    
    $("#pop-submit").click(function(){
        if(isBlocked){
            return;
        }
        var promo_type = $("#promo_type").val();
        var amount = $("#pop2").val();
        var date_start = $("#pop3").val();
        var date_end = $("#pop4").val();
        var hash = clickedHash;
        
        $.ajax({
            method: "POST",
            url: $("#homeDir").text()+"promotions-list/daily-promotion/save/",
            data: { 
                promo_type: promo_type,
                amount: amount,
                date_start: date_start,
                date_end: date_end,
                hash: hash
            }
        }).done(function( hash ) {
            location.reload();
        });
    });

    /* initialize the calendar
    -----------------------------------------------------------------*/
    $('#calendar').fullCalendar({
        events: events,
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay,listMonth'
        },
        locale: 'pl',
        editable: true,
        droppable: true,
        allDayDefault: true,
        drop: function(date, jsEvent, ui, resourceId) {
            saveModyfication($(this).data("id"), date.format(), date.format(), '', $(this).data("event"));
        },
        eventDrop: function(event, jsEvent, ui, view) {
            if(event.end !== null){
                saveModyfication(event.product_id, event.start.format(), event.end.format(), event.hash, event);
            } else {
                saveModyfication(event.product_id, event.start.format(), event.start.format(), event.hash, event);
            }
        },
        eventResize: function(event, jsEvent, ui, view) {
            if(event.end !== null){
                saveModyfication(event.product_id, event.start.format(), event.end.format(), event.hash, event);
            } else {
                saveModyfication(event.product_id, event.start.format(), event.start.format(), event.hash, event);
            }
        },
        eventClick: function(event, jsEvent, view) {
            clickedEvent = event;
            clickedHash  = event.hash;
            
            $(".cal-pop-name").text(event.title);

            if($(jsEvent.target).hasClass("fc-delete")){
                $.ajax({
                    method: "POST",
                    url: $("#homeDir").text()+"promotions-list/daily-promotion/delete/",
                    data: { 
                        hash: clickedHash
                    }
                }).done(function( data ) {
                    $('#calendar').fullCalendar( 'removeEvents', clickedEvent._id);
                });
            } else {
                $.ajax({
                    method: "GET",
                    url: $("#homeDir").text()+"promotions-list/daily-promotion/get/",
                    dataType: "json",
                    data: { 
                        hash: clickedHash
                    }
                }).done(function( data ) {
                    $("#pop2").val(data.amount);
                    $("#pop3").val(data.active_from);
                    $("#pop4").val(data.active_to);
                    $("#promo_type").val(data.promo_type);

                    $("#cal-popup").fadeIn();
                });
            }
        },
        eventRender: function(event, eventElement) { 
            eventElement.find("div.fc-content").prepend("<div class='fc-delete' title='Usuń promocje'>x</div>");
            eventElement.find("div.fc-content").prepend("<div class='fc-resized' title='Kliknij i przytrzymaj LPM, aby wydłużyć czas promocji'><img src='"+$("#homeDir").text()+"images/exchange.png'/></div>");
            eventElement.attr("title", "Kliknij, aby ustawić wartość i rodzaj promocji.");
        },
    });
    
    $(".popup-delete").click(function(){
        $.ajax({
            method: "POST",
            url: $("#homeDir").text()+"promotions-list/daily-promotion/delete/",
            data: { 
                hash: clickedHash
            }
        }).done(function( data ) {
            $('#calendar').fullCalendar( 'removeEvents', clickedEvent._id);
            location.reload();
        });
    });

    function saveModyfication(productID, activeFrom, activeTo, hash, event){
        
        if (typeof(event.source) == "undefined"){
            isNew = true;
        }
        $.ajax({
            method: "POST",
            url: $("#homeDir").text()+"promotions-list/daily-promotion/ajax/",
            data: { 
                productID: productID,
                activeFrom: activeFrom,
                activeTo: activeTo,
                hash: hash
            }
        })
        .done(function( hash ) {
            if(isNew){
                location.reload();
            }
        });
    }

});