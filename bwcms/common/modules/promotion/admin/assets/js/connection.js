$(document).ready(function(){
    var typingTimer, popedElement;
    var selectedObjs = [];
    
    var cid = null;
    var search = null;
    
    var dropableOptions = {
        tolerance: "intersect",
        accept: ".draggable",
        drop: function(event, ui) {    
            if(selectedObjs.length > 1){
                var ids = [];
                $(selectedObjs).each(function(){
                    ids.push($(this).data("id"));
                });
                
                $(this).data("id", ids).attr("style", "").addClass("category");
                $(this).next().text("Grupa");
                $(".product-thumb .prod-add").removeClass("checked");
            } else {
                var el = $(ui.draggable).clone();
                var img =  el.find(".product-thumb").css("background-image");
                
                if(typeof img === 'undefined'){
                    img = el.find(".category-thumb").css("background-image");
                    $(this).css("background-color", "#ffae00");
                    $(this).css("background-image", img).css("background-size", "auto");
                    $(this).data("iscat", true);
                } else {
                    $(this).css("background-image", img).css("background-size", "cover");
                    $(this).css("background-color", "white");
                    $(this).data("iscat", false);
                }

                $(this).data("id", ui.draggable.data("id"));
                $(this).next().text(el.find("p").text());
            }
        }
    };
    
    var dragableOptions = {
        cursor: "move",
        helper: 'clone',
        revert: "invalid",
        start: function(event, ui) {
            $(ui.helper).css("z-index", 60).find(".prod-add").remove();

            $("#product-list .list").css("overflow", "initial");

            if($('.product-thumb .prod-add.checked').length > 0){
                $(this).draggable().draggable("option", {revert: false});
                $(ui.helper).css("opacity", 0);

                selectedObjs = [];
                selectedObjs = $('.product-thumb .prod-add.checked').parent().parent();
                selectedObjs.each(function(){
                    $(this).css("position", "relative");
                });
            } else {
                $(this).draggable().draggable("option", {revert: "invalid"});
            }
        },
        stop: function(event, ui) {
            $("#product-list .list").css("overflow", "auto");

            if(selectedObjs.length > 0){
                selectedObjs.each(function(){
                    $(this).css("position", "initial");
                });
            }
            selectedObjs = [];
        },
        drag: function(event, ui) {
            if(selectedObjs.length > 0){
                var currentLoc = $(ui.helper).position();
                var orig = ui.originalPosition;

                var offsetLeft = currentLoc.left-orig.left;
                var offsetTop = currentLoc.top-orig.top;

                moveSelected(offsetLeft, offsetTop);
            }
        }    
    };
    
    /****************************************************/

    $(".el-drop").droppable(dropableOptions);
    
    $(".save-promo").click(function(){
        $isOk = true;
        $aSave = {};
        
        $i = 0;
        $(".promotion-el").each(function(){
            var aPromoElements = {};
            aPromoElements["category"] = [];
            aPromoElements["product"] = [];
            aPromoElements["group"] = [];
            
            $aSave[$i] = {};
            $revID = 0;
            $isCat = 0;
            
            $dateStart = $(this).find("input[name='date_start']").val();
            $dateEnd   = $(this).find("input[name='date_end']").val();
            
            if($dateStart == ""){
                $(this).find("input[name='date_start']").css("border", "1px solid red");
                $isOk = false;
            }
            
            if($dateEnd == ""){
                $(this).find("input[name='date_end']").css("border", "1px solid red");
                $isOk = false;
            }

            $(this).find(".el-drop").each(function(){
                if(!$(this).hasClass("no-remove")){
                    var id = $(this).data("id");

                    if($(this).data("iscat")){
                        aPromoElements["category"].push(id);
                    } else {
                        if(Array.isArray(id)){
                            aPromoElements["group"].push(id);
                        } else {
                            aPromoElements["product"].push(id);
                        }
                    }
                } else {
                    $isCat = $(this).data("iscat") ? 1 : 0;
                    $revID = $(this).data("id");
                }
            });

            if (typeof $revID !== 'undefined' && $revID != ""){
                $aSave[$i]["reward_id"] = $revID;
                $aSave[$i]["type"] = $isCat;
                $aSave[$i]["element_hash"] = JSON.stringify(aPromoElements);
                $aSave[$i]["discount_amount"] = $(this).find("input[name='amount']").val();
                $aSave[$i]["date_start"] = $dateStart;
                $aSave[$i]["date_end"] = $dateEnd;
            }
            $i++;
        });

        if(!$isOk){
            alert("Proszę poprawić błędy");
            return;
        }
        
        $.ajax({
            url: $("#homeDir").text()+"promotions-list/connection-promotion/save/",
            method: "POST",
            data: {
                data: JSON.stringify($aSave)
            },
            cache: false
        })
        .done(function(data) {
            location.reload();
        });
        
    });
    
    $(".pop-close").click(function(){
        $(".bw-popup").fadeOut();
    });
    
    $("body").on("click", ".prod-add", function() {
        $(this).toggleClass("checked"); 
    });
    
    $("body").on("click", ".category", function() {
        populatePopup($(this).data("id"));
        popedElement = $(this);
        
        $(".bw-popup").fadeIn();
    });
    
    $("body").on("click", ".p-edit", function() {
        var el = $(this).parent().parent();
        var id = el.data("id");
        
        arr = popedElement.data("id");
        arr.remove(id);
        
        popedElement.data("id", arr);

        popedElement = null;
        el.fadeOut();
    });

    $(".add-promo-box").click(function(){
        var schema = $(".promotion-el").first().clone();
        schema.find(".el-drop").css("background-image", "url("+$("#homeDir").text()+"images/ico/drag-ico.png)").css("background-size", "auto").css("background-color", "white").data("id", null);
        schema.find("p").text("");
        schema.find(".promo-options").children().children().first().text(($(".promotion-el").length + 1) + ".");
        schema.find(".el-drop:not(.no-remove)").each(function(index, value){
            if(index >= 2){
                $(this).parent().parent().parent().remove();
            }
            if(index === 1){
                $(this).parent().next().remove();
            }
        });
        
        schema.find(".el-drop").droppable(dropableOptions);
        
        $(".add-promo-box").parent().parent().before(schema);
    });
    
    $(".cat-up").click(function(){
        cid = null;
        getProductSectionData();
    });
    
    $(document).bind('keyup', function(event) {
        if(event.which === 65 && event.shiftKey ) {
            $(".product-thumb .prod-add").addClass("checked");
        }
        
        if(event.which === 27 && cid > 0) {
            cid = null;
            getProductSectionData();
        }
    });
    
    $("body").on("click", "#drop-area .promotion-el span", function() {
        if($(".promotion-el").length > 1){
            var $this = $(this);
            $(this).closest(".promotion-el").fadeOut("slow", function(){
                $this.closest(".promotion-el").remove();
            });
        }
    });
    
    $("body").on("click", "#drop-area .prod-add", function() {
        var el = "";
        el += '<div class="col-4">';
            el += '<div class="drop-box">';
                el += '<div class="row">';
                    el += '<div class="col-8">';
                        el += '<div class="el-drop"></div>';
                        el += '<p></p>';
                    el += '</div>';
                    el += '<div class="col-4">+</div>';
                el += '</div>';
            el += '</div>';
        el += '</div>';
        
        el = $(el);

        $(this).closest(".promo-options").next().children().prepend(el);
        el.find(".el-drop").droppable(dropableOptions);
    });
    
    $("body").on("dblclick", ".category-el", function() {
        cid = $(this).data("id");
        getProductSectionData();
    });
    
    $("#search input").keyup(function(){
        var el = $(this);
        
        clearTimeout(typingTimer);
        typingTimer = setTimeout(function(){
            search = el.val();
            getProductSectionData();
        }, 500);
    });
    
    function populatePopup(data){
        $.ajax({
            url: $("#homeDir").text()+"promotions-list/connection-promotion/get/",
            method: "GET",
            data: { 
                ids: data+""
            },
            cache: false,
            dataType: "json"
        })
        .done(function(data) {
            $("#pop-elements").html("");
            var element = "";
            $.each(data.oProductRowset, function( index, value ) {
                element += '<div class="col-3" data-id="'+value["id"]+'">';
                    element += '<div class="el-drop" style="background-image: url('+value["thumbnail"]+'); background-size: cover;">';
                        element += '<div class="p-edit"></div>';
                    element += '</div>';
                    element += '<p>'+value["name"]+'</p>';
                element += '</div>';
            });
            $("#pop-elements").prepend(element);
        });
    }
    
    function getProductSectionData(){
        $(".list").html("");

        $.ajax({
            url: $("#homeDir").text()+"promotions-list/connection-promotion/get/",
            method: "GET",
            data: { 
                cid: cid,
                search: search
            },
            cache: false,
            dataType: "json"
        })
        .done(function(data) {
            $("#product-list h3").text(data.sCategoryName);
            
            if(cid !== null){ $(".cat-up").show(); } 
            else { $(".cat-up").hide(); }
            
            prepareCategoryData(data.oCategoryRowset);
            prepareProductData(data.oProductRowset);
        });
    }
    
    function prepareCategoryData(oCategoryRowset){
        var sCatStr = "";

        $.each(oCategoryRowset, function( index, value ) {
            sCatStr += '<div class="col-4">';
               sCatStr += '<div class="category-el draggable" data-id="'+value.id+'">';
                    sCatStr += '<div class="category-thumb" style="background-image: url('+$("#homeDir").text()+'images/ico/dir-ico.png)"></div>';
                    sCatStr += '<p>'+value.name+'</p>';
                sCatStr += '</div>';
            sCatStr += '</div>';
        });

        $(".list").prepend(sCatStr);
        $(".category-el").width($(".category-el").outerWidth());
        
    }
    
    function prepareProductData(oProductRowset){
        var sProdStr = "";

        $.each(oProductRowset, function( index, value ) {
            sProdStr += '<div class="col-4">';
               sProdStr += '<div class="product-el draggable" data-id="'+value.id+'">';
                    sProdStr += '<div class="product-thumb" style="background-image: url('+value.thumbnail+'); background-size: cover;">';
                        sProdStr += '<div class="prod-add"></div>';
                    sProdStr += '</div>';
                    sProdStr += '<p>'+value.name+'</p>';
                sProdStr += '</div>';
            sProdStr += '</div>';
        });

        $(".list").append(sProdStr);
        $(".product-el").width($(".product-el").outerWidth());
        
        $('.draggable').draggable(dragableOptions);
    }

    getProductSectionData();
    
    function moveSelected(ol, ot){
        selectedObjs.each(function(){
            $this = $(this);
            var pos = $this.position();

            var l = $this.context.clientLeft;
            var t = $this.context.clientTop;

            $this.css('left', l+ol);
            $this.css('top', t+ot);
        });
    }
    
    Array.prototype.remove = function() {
        var what, a = arguments, L = a.length, ax;
        while (L && this.length) {
            what = a[--L];
            while ((ax = this.indexOf(what)) !== -1) {
                this.splice(ax, 1);
            }
        }
        return this;
    };
    
});