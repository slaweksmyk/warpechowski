$(document).ready(function(){
    
    $('#calendar').fullCalendar({
        events: events.concat(events2),
        defaultView: "listMonth",
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay,listMonth'
        },
        locale: 'pl',
    });
    
    $(".switch-calendar").click(function(){
       $(".switch-cal").each(function(){
            $(this).toggleClass("cal-hidden");
       });
    });
    
});