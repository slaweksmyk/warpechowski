<?php

namespace common\modules\promotion\models;

use Yii;
use common\modules\shop\models\Product;

/**
 * This is the model class for table "xmod_shop_promotion_daily".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $active_from
 * @property string $active_to
 * @property integer $promo_type
 * @property string $amount
 *
 * @property Product $product
 */
class PromotionDaily extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_shop_promotion_daily}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'promo_type'], 'integer'],
            [['active_from', 'active_to'], 'safe'],
            [['amount'], 'number'],
            [['hash'], 'string', 'max' => 255],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_promotion', 'ID'),
            'product_id' => Yii::t('backend_module_promotion', 'Product ID'),
            'active_from' => Yii::t('backend_module_promotion', 'Active From'),
            'active_to' => Yii::t('backend_module_promotion', 'Active To'),
            'promo_type' => Yii::t('backend_module_promotion', 'Promo Type'),
            'amount' => Yii::t('backend_module_promotion', 'Amount'),
            'hash' => Yii::t('backend_module_promotion', 'Hash'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
