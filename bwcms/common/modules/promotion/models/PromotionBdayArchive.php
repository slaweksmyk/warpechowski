<?php

namespace common\modules\promotion\models;

use Yii;
use common\modules\users\models\User;
use common\modules\shop\models\ShopDiscountCodes;

/**
 * This is the model class for table "xmod_shop_promotion_bday_archive".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $code_id
 * @property string $date
 *
 * @property ShopDiscountCodes $code
 * @property User $user
 */
class PromotionBdayArchive extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_shop_promotion_bday_archive}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'code_id', 'type'], 'integer'],
            [['date'], 'safe'],
            [['code_id'], 'exist', 'skipOnError' => true, 'targetClass' => ShopDiscountCodes::className(), 'targetAttribute' => ['code_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_promotion', 'ID'),
            'user_id' => Yii::t('backend_module_promotion', 'User ID'),
            'code_id' => Yii::t('backend_module_promotion', 'Code ID'),
            'date' => Yii::t('backend_module_promotion', 'Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCode()
    {
        return $this->hasOne(ShopDiscountCodes::className(), ['id' => 'code_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
