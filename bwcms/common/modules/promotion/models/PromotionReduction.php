<?php

namespace common\modules\promotion\models;

use Yii;
use common\modules\shop\models\Product;
use common\modules\shop\models\Category;

/**
 * This is the model class for table "xmod_shop_promotion_reduction".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $category_id
 * @property string $amount_perc
 * @property string $amount_sum
 * @property string $date_start
 * @property string $date_end
 *
 * @property Product $product
 * @property Category $category
 */
class PromotionReduction extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_shop_promotion_reduction}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'category_id'], 'integer'],
            [['amount_perc', 'amount_sum'], 'number'],
            [['date_start', 'date_end'], 'safe'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_promotion', 'ID'),
            'product_id' => Yii::t('backend_module_promotion', 'Product ID'),
            'category_id' => Yii::t('backend_module_promotion', 'Category ID'),
            'amount_perc' => Yii::t('backend_module_promotion', 'Amount Perc'),
            'amount_sum' => Yii::t('backend_module_promotion', 'Amount Sum'),
            'date_start' => Yii::t('backend_module_promotion', 'Date Start'),
            'date_end' => Yii::t('backend_module_promotion', 'Date End'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
}
