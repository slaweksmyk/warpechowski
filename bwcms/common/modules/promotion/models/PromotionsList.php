<?php

namespace common\modules\promotion\models;

use Yii;

/**
 * This is the model class for table "xmod_shop_promotion_list".
 *
 * @property integer $id
 * @property string $name
 * @property string $cat_name
 * @property string $sys_name
 */
class PromotionsList extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_shop_promotion_list}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'cat_name', 'sys_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_promotion', 'ID'),
            'name' => Yii::t('backend_module_promotion', 'Name'),
            'cat_name' => Yii::t('backend_module_promotion', 'Cat Name'),
            'sys_name' => Yii::t('backend_module_promotion', 'Sys Name'),
        ];
    }
}
