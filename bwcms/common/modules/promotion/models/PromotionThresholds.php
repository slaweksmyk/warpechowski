<?php

namespace common\modules\promotion\models;

use Yii;

/**
 * This is the model class for table "xmod_shop_promotion_thresholds".
 *
 * @property integer $id
 * @property string $order_amount_low
 * @property string $order_amount_high
 * @property string $discount_amount
 */
class PromotionThresholds extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_shop_promotion_thresholds}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_amount_low', 'order_amount_high', 'discount_amount'], 'each', 'rule' => ['number'], 'when' => function($model){
                return is_array($model);
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_promotion', 'ID'),
            'order_amount_low' => Yii::t('backend_module_promotion', 'Order Amount Low'),
            'order_amount_high' => Yii::t('backend_module_promotion', 'Order Amount High'),
            'discount_amount' => Yii::t('backend_module_promotion', 'Discount Amount'),
        ];
    }
}
