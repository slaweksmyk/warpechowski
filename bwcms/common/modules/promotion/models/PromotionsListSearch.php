<?php

namespace common\modules\promotion\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\promotion\models\PromotionsList;

/**
 * PromotionsListSearch represents the model behind the search form about `common\modules\promotion\models\PromotionsList`.
 */
class PromotionsListSearch extends PromotionsList
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'cat_name', 'sys_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PromotionsList::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['cat_name'=>SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'cat_name', $this->cat_name])
            ->andFilterWhere(['like', 'sys_name', $this->sys_name]);

        return $dataProvider;
    }
}
