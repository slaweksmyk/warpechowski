<?php

namespace common\modules\promotion\models;

use Yii;
use common\modules\shop\models\ShopDiscountCodes;

/**
 * This is the model class for table "xmod_shop_promotion_config".
 *
 * @property integer $id
 * @property string $free_shipment_amount
 * @property integer $first_buy_code
 * @property integer $newsletter_sign_code
 * @property integer $register_code
 * @property integer $facebook_synch_code
 *
 * @property ShopDiscountCodes $firstBuyCode
 * @property ShopDiscountCodes $newsletterSignCode
 * @property ShopDiscountCodes $registerCode
 * @property ShopDiscountCodes $facebookSynchCode
 */
class PromotionConfig extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_shop_promotion_config}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['free_shipment_amount', 'order_cost'], 'number'],
            [['first_buy_title', 'first_buy_content', 'newsletter_sign_title', 'newsletter_sign_content', 'register_title', 'register_content', 'facebook_synch_title', 'facebook_synch_content', 'order_title', 'order_content'], 'string'],
            [['first_buy_code', 'newsletter_sign_code', 'register_code', 'facebook_synch_code', 'order_discount_code'], 'integer'],
            [['first_buy_code'], 'exist', 'skipOnError' => true, 'targetClass' => ShopDiscountCodes::className(), 'targetAttribute' => ['first_buy_code' => 'id']],
            [['newsletter_sign_code'], 'exist', 'skipOnError' => true, 'targetClass' => ShopDiscountCodes::className(), 'targetAttribute' => ['newsletter_sign_code' => 'id']],
            [['register_code'], 'exist', 'skipOnError' => true, 'targetClass' => ShopDiscountCodes::className(), 'targetAttribute' => ['register_code' => 'id']],
            [['facebook_synch_code'], 'exist', 'skipOnError' => true, 'targetClass' => ShopDiscountCodes::className(), 'targetAttribute' => ['facebook_synch_code' => 'id']],
            [['order_discount_code'], 'exist', 'skipOnError' => true, 'targetClass' => ShopDiscountCodes::className(), 'targetAttribute' => ['order_discount_code' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_promotion', 'ID'),
            'free_shipment_amount' => Yii::t('backend_module_promotion', 'Free Shipment Amount'),
            'first_buy_code' => Yii::t('backend_module_promotion', 'First Buy Code'),
            'first_buy_title' => Yii::t('backend_module_promotion', 'First Buy Title'),
            'first_buy_content' => Yii::t('backend_module_promotion', 'First Buy Content'),
            'newsletter_sign_code' => Yii::t('backend_module_promotion', 'Newsletter Sign Code'),
            'newsletter_sign_title' => Yii::t('backend_module_promotion', 'First Buy Title'),
            'newsletter_sign_content' => Yii::t('backend_module_promotion', 'First Buy Content'),
            'register_code' => Yii::t('backend_module_promotion', 'Register Code'),
            'register_title' => Yii::t('backend_module_promotion', 'First Buy Title'),
            'register_content' => Yii::t('backend_module_promotion', 'First Buy Content'),
            'facebook_synch_code' => Yii::t('backend_module_promotion', 'Facebook Synch Code'),
            'facebook_synch_title' => Yii::t('backend_module_promotion', 'First Buy Title'),
            'facebook_synch_content' => Yii::t('backend_module_promotion', 'First Buy Content'),
            'order_cost' => Yii::t('backend_module_promotion', 'order_cost'),
            'order_discount_code' => Yii::t('backend_module_promotion', 'order_discount_code'),
            'order_title' => Yii::t('backend_module_promotion', 'order_title'),
            'order_content' => Yii::t('backend_module_promotion', 'order_content'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFirstBuyCode()
    {
        return $this->hasOne(ShopDiscountCodes::className(), ['id' => 'first_buy_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewsletterSignCode()
    {
        return $this->hasOne(ShopDiscountCodes::className(), ['id' => 'newsletter_sign_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegisterCode()
    {
        return $this->hasOne(ShopDiscountCodes::className(), ['id' => 'register_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacebookSynchCode()
    {
        return $this->hasOne(ShopDiscountCodes::className(), ['id' => 'facebook_synch_code']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderDiscountCode()
    {
        return $this->hasOne(ShopDiscountCodes::className(), ['id' => 'order_discount_code']);
    }
    
    
}
