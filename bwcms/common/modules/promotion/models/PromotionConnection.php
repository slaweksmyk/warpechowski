<?php

namespace common\modules\promotion\models;

use Yii;
use common\modules\shop\models\Product;
use common\modules\shop\models\Category;

/**
 * This is the model class for table "xmod_shop_promotion_connection".
 *
 * @property integer $id
 * @property integer $reward_id
 * @property integer $type
 * @property string $element_hash
 * @property string $discount_amount
 *
 * @property Product $reward
 * @property Category $reward0
 */
class PromotionConnection extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_shop_promotion_connection}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reward_id', 'type'], 'integer'],
            [['element_hash'], 'string'],
            [['date_start', 'date_end'], 'safe'],
            [['discount_amount'], 'number'],
            [['reward_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['reward_id' => 'id']],
            //[['reward_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['reward_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_promotion', 'ID'),
            'reward_id' => Yii::t('backend_module_promotion', 'Reward ID'),
            'type' => Yii::t('backend_module_promotion', 'Type'),
            'element_hash' => Yii::t('backend_module_promotion', 'Element Hash'),
            'discount_amount' => Yii::t('backend_module_promotion', 'Discount Amount'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRewardProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'reward_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRewardCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'reward_id']);
    }
    
    public function decodeElements(){
        return json_decode($this->element_hash);
    }
    
}
