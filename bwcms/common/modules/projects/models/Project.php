<?php

namespace common\modules\projects\models;

use Yii;
use common\modules\files\models\File;
use common\modules\gallery\models\Gallery;
use common\modules\projects\models\ProjectData;
use common\modules\projects\models\ProjectSignup;
use common\modules\projects\models\ProjectCategory;
use common\modules\projects\models\ProjectTimeline;
use common\modules\projects\models\ProjectScoreIdea;
use common\modules\projects\models\ProjectOrganizers;
use common\modules\projects\models\ProjectScoreRealisation;

use yii\imagine\Image;
use Imagine\Image\Box;

/**
 * This is the model class for table "xmod_projects_list".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $thumbnail_id
 * @property string $gallery_id
 * @property string $date_start
 * @property string $date_end
 *
 * @property ProjectData[] $xmodProjectsDatas
 * @property ProjectCategory $category
 * @property Gallery $gallery
 * @property File $thumbnail
 * @property ProjectOrganizers[] $xmodProjectsOrganizers
 * @property ProjectScore[] $xmodProjectsScores
 * @property ProjectSignup[] $xmodProjectsSignups
 */
class Project extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_projects_list}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'thumbnail_id', 'gallery_id', 'is_active', 'is_promoted'], 'integer'],
            [['type'], 'string'],
            [['date_start', 'date_end'], 'safe'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProjectCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['gallery_id'], 'exist', 'skipOnError' => true, 'targetClass' => Gallery::className(), 'targetAttribute' => ['gallery_id' => 'id']],
            [['thumbnail_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['thumbnail_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_projects', 'ID'),
            'category_id' => Yii::t('backend_module_projects', 'Category ID'),
            'thumbnail_id' => Yii::t('backend_module_projects', 'Thumbnail ID'),
            'gallery_id' => Yii::t('backend_module_projects', 'Gallery ID'),
            'type' => Yii::t('backend_module_projects', 'Type'),
            'date_start' => Yii::t('backend_module_projects', 'Date Start'),
            'date_end' => Yii::t('backend_module_projects', 'Date End'),
            'is_active' => Yii::t('backend_module_projects', 'Is Active'),
            'is_promoted' => Yii::t('backend_module_projects', 'Is Promoted'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectData()
    {
        return $this->hasOne(ProjectData::className(), ['project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ProjectCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGallery()
    {
        return $this->hasOne(Gallery::className(), ['id' => 'gallery_id']);
    }

    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getThumbnail($width, $height, $type = "crop", $effect = null, $quality = 80) 
    { 
        $oThumbnail = $this->hasOne(File::className(), ['id' => 'thumbnail_id'])->one();

        $effect_safe = str_replace("#", "", $effect);
        $sFilePath = Yii::getAlias("@root/upload/{$oThumbnail->filename}");
        $sReworkedPath = Yii::getAlias("@root/upload/reworked/{$width}_{$height}_{$type}_{$effect_safe}_{$oThumbnail->filename}");

        if(!file_exists($sReworkedPath)){
            if(file_exists($sFilePath)){
                switch($type){
                    case "crop";
                        Image::thumbnail($sFilePath, $width,$height)->save($sReworkedPath, ['quality' => $quality]);
                        break;

                    case "matched";
                        Image::getImagine()->open($sFilePath)->thumbnail(new Box($height, $width))->save($sReworkedPath , ['quality' => $quality]);
                        break;
                }

                if(!is_null($effect)){
                    $image = yii\imagine\Image::getImagine();
                    $newImage = $image->open($sReworkedPath);

                    switch($effect){
                        case "grayscale":
                            $newImage->effects()->grayscale();
                            break;

                        case "negative":
                            $newImage->effects()->negative();
                            break;

                        default:
                            if(ctype_xdigit($effect_safe)){
                                $color = $newImage->palette()->color($effect);
                                $newImage->effects()->colorize($color);
                            }
                            break;

                    }
                    $newImage->save($sReworkedPath, ['quality' => $quality]);
                }
            }
        }
        
        return "/upload/reworked/{$width}_{$height}_{$type}_{$effect_safe}_{$oThumbnail->filename}";
    } 
    
    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function hasThumbnail(){
        return !is_null($this->hasOne(File::className(), ['id' => 'thumbnail_id'])->one());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectOrganizers()
    {
        return $this->hasMany(ProjectOrganizers::className(), ['project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectScoreIdea()
    {
        return $this->hasMany(ProjectScoreIdea::className(), ['project_id' => 'id']);
    }
    
    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getProjectsScoreRealisations() 
    { 
        return $this->hasMany(ProjectScoreRealisation::className(), ['project_id' => 'id']);
    } 

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectSignups()
    {
        return $this->hasMany(ProjectSignup::className(), ['project_id' => 'id']);
    }
    
    public function getAverageIdeaScore(){
        $score = 0;
        
        $oScoreRowset = $this->getProjectScoreIdea()->all();
        if(count($oScoreRowset) == 0) { return $score; }  
        
        foreach($oScoreRowset as $oScore){ 
            $score += $oScore->score;
        }

        return round($score/count($oScoreRowset)); 
    }
    
    public function getAmount(){
        return count($this->getProjectScoreIdea()->all());
    }
    
    public function getAverageRealisationScore(){
        $score = 0;
        
        $oScoreRowset = $this->getProjectsScoreRealisations()->all();
        if(count($oScoreRowset) == 0) { return $score; }  
        
        foreach($oScoreRowset as $oScore){ 
            $score += $oScore->score;
        }
        
        return round($score/count($oScoreRowset)); 
    }
    
    public function getRealisationAmount(){
        return count($this->getProjectsScoreRealisations()->all());
    }
    
    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getTimeline() 
    { 
        return $this->hasMany(ProjectTimeline::className(), ['project_id' => 'id']);
    } 
    
}
