<?php

namespace common\modules\projects\models;

use Yii;
use common\modules\files\models\File;
use common\modules\projects\models\Project;

/**
 * This is the model class for table "xmod_projects_categories".
 *
 * @property integer $id
 * @property integer $thumbnail_id
 * @property string $name
 * @property string $color
 *
 * @property File $thumbnail
 * @property Project[] $xmodProjectsLists
 */
class ProjectCategory extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_projects_categories}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'thumbnail_id'], 'integer'],
            [['name', 'color'], 'string', 'max' => 255],
            [['thumbnail_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['thumbnail_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_projects', 'ID'),
            'parent_id' => Yii::t('backend_module_projects', 'Parent ID'),
            'thumbnail_id' => Yii::t('backend_module_projects', 'Thumbnail ID'),
            'name' => Yii::t('backend_module_projects', 'Name'),
            'color' => Yii::t('backend_module_projects', 'Color'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getThumbnail()
    {
        return $this->hasOne(File::className(), ['id' => 'thumbnail_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['category_id' => 'id']);
    }
    
    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getParent() 
    { 
        return $this->hasOne(ProjectCategory::className(), ['id' => 'parent_id']);
    } 

    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getChildrens() 
    { 
        return $this->hasMany(ProjectCategory::className(), ['parent_id' => 'id']);
    } 
}
