<?php

namespace common\modules\projects\models;

use Yii;
use common\modules\gallery\models\Gallery;
use common\modules\projects\models\Project;
use common\modules\projects\models\ProjectOrganizers;

/**
 * This is the model class for table "xmod_projects_timeline".
 *
 * @property integer $id
 * @property integer $project_id
 * @property integer $organizer_id
 * @property string $gallery_id
 * @property string $date
 * @property string $description
 *
 * @property ProjectOrganizers $organizer
 * @property Project $project
 */
class ProjectTimeline extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_projects_timeline}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'organizer_id', 'gallery_id'], 'integer'],
            [['date'], 'safe'],
            [['description'], 'string'],
            [['gallery_id'], 'exist', 'skipOnError' => true, 'targetClass' => XmodGallery::className(), 'targetAttribute' => ['gallery_id' => 'id']],
            [['organizer_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProjectOrganizers::className(), 'targetAttribute' => ['organizer_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_projects', 'ID'),
            'project_id' => Yii::t('backend_module_projects', 'Project ID'),
            'organizer_id' => Yii::t('backend_module_projects', 'Organizer ID'),
            'gallery_id' => Yii::t('backend_module_projects', 'Gallery ID'),
            'date' => Yii::t('backend_module_projects', 'Date'),
            'description' => Yii::t('backend_module_projects', 'Description'),
        ];
    }
    
    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getGallery() 
    { 
        return $this->hasOne(Gallery::className(), ['id' => 'gallery_id']);
    } 

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrganizer()
    {
        return $this->hasOne(ProjectOrganizers::className(), ['id' => 'organizer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }
}
