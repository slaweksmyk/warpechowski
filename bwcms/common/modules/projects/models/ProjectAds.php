<?php

namespace common\modules\projects\models;

use Yii;
use common\modules\files\models\File;
use common\modules\projects\models\Project;

/**
 * This is the model class for table "xmod_projects_ads".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $project_id
 * @property integer $banner_id
 * @property integer $points
 * @property string $city
 * @property string $date_created
 * @property string $date_expire
 *
 * @property File $banner
 * @property Project $project
 */
class ProjectAds extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_projects_ads}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'project_id', 'banner_id', 'points'], 'integer'],
            [['date_created', 'date_expire'], 'safe'],
            [['city'], 'string', 'max' => 255],
            [['banner_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['banner_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_project', 'ID'),
            'type' => Yii::t('backend_module_project', 'Type'),
            'project_id' => Yii::t('backend_module_project', 'Project ID'),
            'banner_id' => Yii::t('backend_module_project', 'Banner ID'),
            'points' => Yii::t('backend_module_project', 'Points'),
            'city' => Yii::t('backend_module_project', 'City'),
            'date_created' => Yii::t('backend_module_project', 'Date Created'),
            'date_expire' => Yii::t('backend_module_project', 'Date Expire'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanner()
    {
        return $this->hasOne(File::className(), ['id' => 'banner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }
}
