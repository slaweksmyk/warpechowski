<?php

namespace common\modules\projects\models;

use Yii;
use common\modules\users\models\User;
use common\modules\projects\models\Project;

/**
 * This is the model class for table "xmod_projects_score_realisation".
 *
 * @property integer $id
 * @property integer $project_id
 * @property integer $user_id
 * @property integer $score
 *
 * @property Project $project
 * @property User $user
 */
class ProjectScoreRealisation extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_projects_score_realisation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'user_id', 'score'], 'integer'],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_projects', 'ID'),
            'project_id' => Yii::t('backend_module_projects', 'Project ID'),
            'user_id' => Yii::t('backend_module_projects', 'User ID'),
            'score' => Yii::t('backend_module_projects', 'Score'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
