<?php

namespace common\modules\projects\models;

use Yii;
use common\modules\projects\models\Project;

/**
 * This is the model class for table "xmod_projects_data".
 *
 * @property integer $id
 * @property integer $project_id
 * @property string $name
 * @property string $slug
 * @property string $city
 * @property string $district
 * @property string $street
 * @property string $map_lat
 * @property string $map_long
 * @property string $short_description
 * @property string $full_description
 *
 * @property Project $project
 */
class ProjectData extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_projects_data}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id'], 'integer'],
            [['short_description', 'full_description'], 'string'],
            [['name', 'slug', 'city', 'district', 'street', 'website', 'map_lat', 'map_long'], 'string', 'max' => 255],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_projects', 'ID'),
            'project_id' => Yii::t('backend_module_projects', 'Project ID'),
            'name' => Yii::t('backend_module_projects', 'Name'),
            'slug' => Yii::t('backend_module_projects', 'Slug'),
            'city' => Yii::t('backend_module_projects', 'City'),
            'district' => Yii::t('backend_module_projects', 'District'),
            'street' => Yii::t('backend_module_projects', 'Street'),
            'map_lat' => Yii::t('backend_module_projects', 'Map Lat'),
            'map_long' => Yii::t('backend_module_projects', 'Map Long'),
            'short_description' => Yii::t('backend_module_projects', 'Short Description'),
            'full_description' => Yii::t('backend_module_projects', 'Full Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }
}
