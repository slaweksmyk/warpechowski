<?php

namespace common\modules\projects\models;

use Yii;
use common\modules\users\models\User;
use common\modules\projects\models\Project;

/**
 * This is the model class for table "xmod_projects_abuse".
 *
 * @property integer $id
 * @property integer $project_id
 * @property integer $user_id
 * @property string $topic
 * @property string $description
 *
 * @property Project $project
 * @property User $user
 */
class ProjectAbuse extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_projects_abuse}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id'], 'integer'],
            [['description'], 'string'],
            [['topic'], 'string', 'max' => 255],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_project', 'ID'),
            'project_id' => Yii::t('backend_module_project', 'Project ID'),
            'topic' => Yii::t('backend_module_project', 'Topic'),
            'description' => Yii::t('backend_module_project', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

}
