<?php

namespace common\modules\projects\admin;

use Yii;
/**
 * files module definition class
 */
class Projects extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\projects\admin\controllers';
    public $defaultRoute = 'project';

    /**
     * @inheritdoc
     */
    public function init()
    {
        $aUrlParts = explode("/", str_replace(\Yii::$app->request->baseUrl, "", Yii::$app->getRequest()->url));
        $this->defaultRoute = current($aUrlParts);
        parent::init();
    }
}
