<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;

use common\modules\projects\models\ProjectData;
use common\modules\projects\models\ProjectCategory;

$this->title = Yii::t('backend_module_projects', 'Projects');
?>
<div class="project-index">

    <p>
        <?= Html::a(Yii::t('backend_module_projects', 'Create Project'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control"><option>20</option><option>50</option><option>100</option><option>200</option><option>500</option></select>
            </form>
        </header>
        <div class="panel-body" >
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    [
                        'attribute' => 'category_id',
                        'contentOptions'=>['style'=>'width: 150px;'],
                        'value' => function($data){
                            return ProjectCategory::find()->where(["=", "id", $data["category_id"]])->one()->name;
                        }
                    ],
                    [
                        'attribute' => 'projectData.name',
                        'contentOptions'=>['style'=>'width: 150px;'],
                    ],
                    [
                        'attribute' => 'Lokalizacja',
                        'format' => 'raw',
                        'contentOptions'=>['style'=>'width: 250px;'],
                        'value' => function ($data) {
                            $oProjectData = ProjectData::find()->where(["=", "project_id", $data["id"]])->one();
                            
                            $sReturn = "";
                            if($oProjectData->city){ $sReturn .= $oProjectData->city; }
                            if($oProjectData->city && $oProjectData->district) { $sReturn .= "<br/>"; }
                            if($oProjectData->district){ $sReturn .= $oProjectData->district; }
                            if($oProjectData->district && $oProjectData->street) { $sReturn .= "<br/>"; }
                            if($oProjectData->street){ $sReturn .= $oProjectData->street; }
                            
                            return $sReturn;
                        }
                    ],
                    'date_start',
                    'date_end',
                    [
                        'class' => 'common\hooks\yii2\grid\ActionColumn',
                        'template' => '{update} {delete}',
                    ],
                ],
            ]); ?>
        </div>
    </section>   
</div>
