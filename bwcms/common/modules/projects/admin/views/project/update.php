<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use common\modules\files\models\File;
use common\modules\gallery\models\Gallery;

use common\modules\projects\models\ProjectCategory;

$this->title = Yii::t('backend_module_projects', 'Update {modelClass}: ', [
    'modelClass' => 'Project',
]) . $modelData->name;
?>
<div class="project-update">

    <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($modelData, 'project_id')->hiddenInput(["value" => $model->id])->label(false) ?>

        <section class="panel full" >
            <header>
                <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
            </header>
            <div class="panel-body" >
                <div class="project-form">

                    <div class="row">
                        <div class="col-6"><?= $form->field($modelData, 'name')->textInput() ?></div>
                        <div class="col-6">
                            <div class="row">
                                <div class="col-4"><?= $form->field($model, 'type')->dropDownList(["normal" => "Standardowy", "auction" => "Aukcja", "collection" => "Zbiórka"]) ?></div>
                                <div class="col-4"><?= $form->field($model, 'date_start')->textInput(["class" => "datepicker form-control"]) ?></div>
                                <div class="col-4"><?= $form->field($model, 'date_end')->textInput(["class" => "datepicker form-control"]) ?></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-4"><?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(ProjectCategory::find()->all(), 'id', 'name')) ?></div>
                        <div class="col-4"><?= $form->field($model, 'thumbnail_id')->dropDownList(ArrayHelper::map(File::find()->all(), 'id', 'name'), ["prompt" => "- wybierz miniature -"]) ?></div>
                        <div class="col-4"><?= $form->field($model, 'gallery_id')->dropDownList(ArrayHelper::map(Gallery::find()->all(), 'id', 'name'), ["prompt" => "- wybierz galerie -"]) ?></div>
                    </div>

                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_projects', 'Create') : Yii::t('backend_module_projects', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>

                </div>
            </div>
        </section>

        <section class="panel full" >
            <header>
                <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Dane dodatkowe          
            </header>
            <div class="panel-body" >
                <div class="project-form">

                    <div class="row">
                        <div class="col-4"><?= $form->field($modelData, 'city')->textInput() ?></div>
                        <div class="col-4"><?= $form->field($modelData, 'district')->textInput() ?></div>
                        <div class="col-4"><?= $form->field($modelData, 'street')->textInput() ?></div>
                    </div>

                    <div class="row">
                        <div class="col-4"><?= $form->field($modelData, 'website')->textInput() ?></div>
                        <div class="col-4">
                            <div class="row">
                                <div class="col-6"><?= $form->field($modelData, 'map_lat')->textInput() ?></div>
                                <div class="col-6"><?= $form->field($modelData, 'map_long')->textInput() ?></div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="row">
                                <div class="col-6"><?= $form->field($model, 'is_active')->dropDownList([0 => Yii::t('backend', 'no'), 1 => Yii::t('backend', 'yes')]) ?></div>
                                <div class="col-6"><?= $form->field($model, 'is_promoted')->dropDownList([0 => Yii::t('backend', 'no'), 1 => Yii::t('backend', 'yes')]) ?></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-10"><?= $form->field($modelData, 'short_description')->textarea(['rows' => 6]) ?></div>
                        <div class="col-2 seo-tips"></div>
                    </div>

                    <div class="row">
                        <div class="col-10"><?= $form->field($modelData, 'full_description')->textarea(['rows' => 6]) ?></div>
                        <div class="col-2 seo-tips"></div>
                    </div>
                    
                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_projects', 'Create') : Yii::t('backend_module_projects', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>

                </div>
            </div>
        </section>

    <?php ActiveForm::end(); ?>

</div>
