<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;

use common\modules\projects\models\ProjectCategory;

$this->title = Yii::t('backend_module_projects', 'Project Categories');
?>
<div class="project-category-index">

    <p>
        <?= Html::a(Yii::t('backend_module_projects', 'Create Project Category'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    [
                        'attribute' => 'parent_id',
                        'contentOptions'=>['style'=>'width: 150px;'],
                        'value' => function($data){
                            $oProject = ProjectCategory::find()->where(["=", "id", $data["parent_id"]])->one();
                            if(!$oProject) { return null; }
                            return $oProject->name;
                        }
                    ],
                    'name',
                    [
                        'attribute' => 'color',
                        'contentOptions'=>['style'=>'width: 150px;'],
                    ],
                    [
                        'class' => 'common\hooks\yii2\grid\ActionColumn',
                        'template' => '{update} {delete}',
                    ],
                ],
            ]); ?>
        </div>
    </section>   
</div>
