<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use common\modules\projects\models\ProjectCategory;

$this->title = Yii::t('backend_module_projects', 'Create Project Category');
?>
<div class="project-category-create">

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
        </header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(); ?>
            
            <div class="row">
                <div class="col-4"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
                <div class="col-4"><?= $form->field($model, 'parent_id')->dropDownList(ArrayHelper::map(ProjectCategory::find()->all(), 'id', 'name'), ["prompt" => "- wybierz rodzica -"]) ?></div>
                <div class="col-4"><?= $form->field($model, 'color')->textInput(['maxlength' => true, 'type' => "color"]) ?></div>
            </div>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_projects', 'Create') : Yii::t('backend_module_projects', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </section>  

</div>
