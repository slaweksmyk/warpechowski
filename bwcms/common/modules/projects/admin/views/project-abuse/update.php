<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\projects\models\ProjectAbuse */

$this->title = Yii::t('backend_module_project', 'Update {modelClass}: ', [
    'modelClass' => 'Project Abuse',
]) . $model->id;
?>
<div class="project-abuse-update">

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
        </header>
        <div class="panel-body" >
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </section>   

</div>
