<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;

use common\modules\projects\models\Project;

$this->title = Yii::t('backend_module_project', 'Project Abuses');
?>
<div class="project-abuse-index">
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $this->title ?>            
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option></select>
            </form>
        </header>
        <div class="panel-body" >
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    [
                        'attribute' => 'project_id',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return Project::find()->where(["=", "id", $data["project_id"]])->one()->getProjectData()->one()->name;
                        }
                    ],
                    'topic',
                    'description:ntext',
                    
                    ['class' => 'common\hooks\yii2\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </section>   
</div>
