<?php
/**
 * Module: SEO Meta (backend)
 * 
 * Module init
 * 
 * PHP version 7
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @copyright (c) 2016 Krzysztof Borecki
 * @version 1.0
 */

namespace common\modules\meta\admin;

/**
 * menu module definition class
 */
class MetaTags extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\meta\admin\controllers';
    public $defaultRoute = 'meta';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
