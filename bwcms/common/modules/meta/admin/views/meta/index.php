<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;
use yii\widgets\ActiveForm;

$this->title = "Meta tagi";
$this->params['breadcrumbs'][] = $this->title; 
?>
<div class="article-index">
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Meta tagi - Podstrony
        </header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(); ?>
                <?= GridView::widget([
                    'dataProvider' => $pageDataProvider,
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered'
                    ],
                    'columns' => [
                        'id',
                        'name',
                        [
                            'attribute' => 'seo_title',
                            'format' => 'raw',
                            'label' => 'SEO Tytuł',
                            'headerOptions' => ['style' => 'width:30%'],
                            'value' => function ($data) {
                                return '<textarea class="transparent-input noTynyMCE" style="min-height: 100px; width: 100%;" name="page['.$data["id"].'][seo_title]">'.$data["seo_title"].'</textarea> ';
                            }
                        ],
                        [
                            'attribute' => 'seo_keywords',
                            'format' => 'raw',
                            'label' => 'Słowa kluczowe',
                            'headerOptions' => ['style' => 'width:30%'],
                            'value' => function ($data) {
                                return '<textarea class="transparent-input noTynyMCE" style="min-height: 100px; width: 100%;" name="page['.$data["id"].'][seo_keywords]">'.$data["seo_keywords"].'</textarea> ';
                            }
                        ],
                        [
                            'attribute' => 'seo_description',
                            'format' => 'raw',
                            'label' => 'Opis',
                            'headerOptions' => ['style' => 'width:30%'],
                            'value' => function ($data) {
                                return '<textarea class="transparent-input noTynyMCE" style="min-height: 100px; width: 100%;" name="page['.$data["id"].'][seo_description]">'.$data["seo_description"].'</textarea>';
                            }
                        ],   
                    ]
                ]); ?>
                <div class="form-group text-right">
                    <?= Html::a("Generuj meta dane", ['generate', 'action' => 'pages'], ['class' => 'btn btn-success']) ?>
                    <?= Html::submitButton(Yii::t('backend',  'update'), ['class' => 'btn btn-primary']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </section>
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Meta tagi - Artykuły
        </header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(); ?>
                <?= GridView::widget([
                    'dataProvider' => $articleDataProvider,
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered'
                    ],
                    'columns' => [
                        'id',
                        'title',
                        [
                            'attribute' => 'seo_title',
                            'label' => 'SEO Tytuł',
                            'format' => 'raw',
                            'headerOptions' => ['style' => 'width:30%'],
                            'value' => function ($data) {
                                return '<textarea class="transparent-input noTynyMCE" style="min-height: 100px; width: 100%;" name="article['.$data["id"].'][seo_title]">'.$data["seo_title"].'</textarea>';
                            }
                        ],
                        [
                            'attribute' => 'seo_keywords',
                            'format' => 'raw',
                            'label' => 'Słowa kluczowe',
                            'headerOptions' => ['style' => 'width:30%'],
                            'value' => function ($data) {
                                return '<textarea class="transparent-input noTynyMCE" style="min-height: 100px; width: 100%;" name="article['.$data["id"].'][seo_keywords]">'.$data["seo_keywords"].'</textarea>';
                            }
                        ],
                        [
                            'attribute' => 'seo_description',
                            'format' => 'raw',
                            'label' => 'Opis',
                            'headerOptions' => ['style' => 'width:30%'],
                            'value' => function ($data) {
                                return '<textarea class="transparent-input noTynyMCE" style="min-height: 100px; width: 100%;" name="article['.$data["id"].'][seo_description]">'.$data["seo_description"].'</textarea>';
                            }
                        ],   
                    ]
                ]); ?>
                <div class="form-group text-right">
                    <?= Html::a("Generuj meta dane", ['generate', 'action' => 'articles'], ['class' => 'btn btn-success']) ?>
                    <?= Html::submitButton(Yii::t('backend',  'update'), ['class' => 'btn btn-primary']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </section>
    
    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Meta tagi - Produkty
        </header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(); ?>
                <?= GridView::widget([
                    'dataProvider' => $productDataProvider,
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered'
                    ],
                    'columns' => [
                        'id',
                        'name',
                        [
                            'attribute' => 'seo_title',
                            'label' => 'SEO Tytuł',
                            'format' => 'raw',
                            'headerOptions' => ['style' => 'width:30%'],
                            'value' => function ($data) {
                                return '<textarea class="transparent-input noTynyMCE" style="min-height: 100px; width: 100%;" name="product['.$data["id"].'][seo_title]">'.$data["seo_title"].'</textarea>';
                            }
                        ],
                        [
                            'attribute' => 'seo_keywords',
                            'format' => 'raw',
                            'label' => 'Słowa kluczowe',
                            'headerOptions' => ['style' => 'width:30%'],
                            'value' => function ($data) {
                                return '<textarea class="transparent-input noTynyMCE" style="min-height: 100px; width: 100%;" name="product['.$data["id"].'][seo_keywords]">'.$data["seo_keywords"].'</textarea>';
                            }
                        ],
                        [
                            'attribute' => 'seo_description',
                            'format' => 'raw',
                            'label' => 'Opis',
                            'headerOptions' => ['style' => 'width:30%'],
                            'value' => function ($data) {
                                return '<textarea class="transparent-input noTynyMCE" style="min-height: 100px; width: 100%;" name="product['.$data["id"].'][seo_description]">'.$data["seo_description"].'</textarea>';
                            }
                        ],   
                    ]
                ]); ?>
                <div class="form-group text-right">
                    <?= Html::a("Generuj meta dane", ['generate', 'action' => 'products'], ['class' => 'btn btn-success']) ?>
                    <?= Html::submitButton(Yii::t('backend',  'update'), ['class' => 'btn btn-primary']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </section>
    
</div>