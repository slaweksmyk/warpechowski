<?php
/**
 * Meta module
 * 
 * PHP version 7
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @copyright (c) 2016 Krzysztof Borecki
 * @version 1.0
 */

namespace common\modules\meta\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\data\ArrayDataProvider;

use common\modules\pages\models\PagesSite;
use common\modules\articles\models\Article;
use common\modules\shop\models\Product;
use common\modules\tags\models\TagArticle;

/**
 * LogController implements the CRUD actions for Log model.
 */
class MetaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Log models.
     * @return mixed
     */
    public function actionIndex()
    {
        $aPost = Yii::$app->request->post();

        if($aPost){
            if(isset($aPost["page"])){
                foreach($aPost["page"] as $iPageID => $aData){
                    $oPage = PagesSite::findOne($iPageID);

                    $oPage->seo_title = $aData["seo_title"];
                    $oPage->seo_keywords = $aData["seo_keywords"];
                    $oPage->seo_description = $aData["seo_description"];

                    $oPage->save();
                }
            }
            
            if(isset($aPost["article"])){
                foreach($aPost["article"] as $iArticleID => $aData){
                    $oArticle = Article::findOne($iArticleID);

                    $oArticle->seo_title = $aData["seo_title"];
                    $oArticle->seo_keywords = $aData["seo_keywords"];
                    $oArticle->seo_description = $aData["seo_description"];

                    $oArticle->save();
                }
            }
            
            if(isset($aPost["product"])){
                foreach($aPost["product"] as $iProductID => $aData){
                    $oProduct = Product::findOne($iProductID);

                    $oProduct->seo_title = $aData["seo_title"];
                    $oProduct->seo_keywords = $aData["seo_keywords"];
                    $oProduct->seo_description = $aData["seo_description"];

                    $oProduct->save();
                }
            }
        }
        
        $oPagesRowset = PagesSite::find()->select(['id', 'name', 'seo_title', 'seo_keywords', 'seo_description'])->asArray()->all();
        $oArticlesRowset = Article::find()->select(['id', 'title', 'seo_title', 'seo_keywords', 'seo_description'])->asArray()->all();
        $oProductRowset = Product::find()->select(['id', 'name', 'seo_title', 'seo_keywords', 'seo_description'])->asArray()->all();
        
        $pageDataProvider = new ArrayDataProvider([
            'key'=>'id',
            'allModels' => $oPagesRowset,
        ]);
        
        $articleDataProvider = new ArrayDataProvider([
            'key'=>'id',
            'allModels' => $oArticlesRowset,
        ]);
        
        $productDataProvider = new ArrayDataProvider([
            'key'=>'id',
            'allModels' => $oProductRowset,
        ]);

        return $this->render('index', [
            'pageDataProvider' => $pageDataProvider,
            'articleDataProvider' => $articleDataProvider,
            'productDataProvider' => $productDataProvider
        ]);
    }
    
    /*
     * Generates meta-headers contents for articles/pages/products
     */
    public function actionGenerate()
    {
        $aGet  = Yii::$app->request->get();
        
        switch($aGet["action"]){
            
            case "articles":
                foreach(Article::find()->all() as $oArticle){
                    $sDescription = $oArticle->short_description != "" ? $oArticle->short_description : $oArticle->full_description;
                
                    $aTags = [];
                    foreach(TagArticle::find()->where(["=", "article_id", $oArticle->id])->all() as $oArticleTag){
                        $oTag = $oArticleTag->getTag()->one();
                        $aTags[] = $oTag->name;
                    }
                    
                    $oArticle->seo_title = $oArticle->title;
                    $oArticle->seo_description = mb_substr(strip_tags($sDescription), 0, 150);
                    $oArticle->seo_keywords = implode(", ", $aTags);
                    $oArticle->save();
                }
                break;
            
            case "pages":
                foreach(PagesSite::find()->all() as $oPage){
                    $oPage->seo_title = $oPage->name;
                    $oPage->seo_description = mb_substr(strip_tags($oPage->description), 0, 150);
                    $oPage->save();
                }
                break;
            
            case "products":
                foreach(Product::find()->all() as $oProduct){
                    $oProduct->seo_title = $oProduct->name;
                    $oProduct->seo_description = mb_substr(strip_tags($oProduct->description), 0, 150);
                    $oProduct->save();
                }
                break;
        }
        
        return $this->redirect(['index']);
    }
       
}
