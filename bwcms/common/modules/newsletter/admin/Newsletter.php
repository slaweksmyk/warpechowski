<?php

namespace common\modules\newsletter\admin;

/**
 * newsletter module definition class
 */
class Newsletter extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\newsletter\admin\controllers';
    public $defaultRoute = 'newsletter';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
