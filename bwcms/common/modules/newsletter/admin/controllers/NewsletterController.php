<?php

namespace common\modules\newsletter\admin\controllers;

use Yii;
use common\modules\newsletter\models\NewsletterGroup;
use common\modules\newsletter\models\NewsletterGroupSearch;
use common\modules\newsletter\models\NewsletterEmail;
use common\modules\newsletter\models\NewsletterEmailSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * NewsletterController implements the CRUD actions for NewsletterGroup model.
 */
class NewsletterController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all NewsletterGroup models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewsletterGroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single NewsletterGroup model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new NewsletterGroup model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NewsletterGroup();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing NewsletterGroup model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing NewsletterGroup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    /**
     * @param integer $id
     * @return mixed
     */
    public function actionListEmails($id)
    {
        $searchModel = new NewsletterEmailSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = NewsletterGroup::findOne(["id" => $id]);

        return $this->render("email-list", [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model
        ]);
    }
    
    /**
     * Creates a new NewsletterEmail model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateEmail($id)
    {
        $model = new NewsletterEmail();
        $groupModel = NewsletterGroup::findOne(["id" => $id]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['list-emails', "id" => $groupModel->id]);
        } else {
            return $this->render('create-email', [
                'model' => $model,
                'groupModel' => $groupModel
            ]);
        }
    }
    
    /**
     * Deletes an existing NewsletterEmail model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteEmail($id)
    {
        $oNewsletterEmail = NewsletterEmail::findOne(["id" => $id]);
        $oNewsletterEmail->delete();
        
        return $this->redirect(['list-emails', "id" => $oNewsletterEmail->group_id]);
    }
    
    /**
     * Updates an existing NewsletterEmail model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateEmail($id)
    {
        $model = NewsletterEmail::findOne(["id" => $id]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['list-emails', 'id' => $model->group_id]);
        } else {
            return $this->render('update-email', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the NewsletterGroup model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NewsletterGroup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NewsletterGroup::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
