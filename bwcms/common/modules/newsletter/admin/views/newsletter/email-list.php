<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;

$this->title = "Grupa: {$model->name}";
?>
<div class="newsletter-group-index">

    <p>
        <?= Html::a(Yii::t('backend_module_newsletter', 'back_to_group_newsletter_list'), ['index'], ['class' => 'btn btn-outline-secondary']) ?>
        <?= Html::a(Yii::t('backend_module_newsletter', 'create_email'), ['create-email', 'id' => $model->id], ['class' => 'btn btn-success']) ?>

    </p>

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
            <form id="per-page">
                <span><?= Yii::t('backend', 'quantity_per_page'); ?>:</span>
                <select name="per-page" class="form-control">
                    <?php if (Yii::$app->request->get('per-page')) {
                        echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>";
                    } ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                 'rowOptions' => function($model) {
                    if ($model->is_accept_rules2!=1) {
                        return ['class' => 'inactive-row'];
                    }
                },
                'columns' => [
                    'id',
                    [
                        'attribute' => "email",
                        'headerOptions' => ['style' => 'width:300px'],
                    ],
                    [
                        'attribute' => "is_active",
                        'filter' => [1 => "Aktywny", 0 => "Nieaktywny"],
                         
                        'format' => 'raw',
                        'value' => function ($data) {
                            return ($data->is_active) ? Yii::t('backend_module_newsletter', 'Is Active Yes') : Yii::t('backend_module_newsletter', 'Is Active No');
                        }
                    ],
                    [
                        'attribute' => "is_accept_rules",
                        'filter' => [1 => "Tak", 0 => "Nie"],
                       
                        'format' => 'raw',
                        'value' => function ($data) {
                            return ($data->is_accept_rules) ? Yii::t('backend_module_newsletter', 'Is Accept Rules Yes') : Yii::t('backend_module_newsletter', 'Is Accept Rules No');
                        }
                    ],
                    [
                        'attribute' => "is_accept_rules2",
                        'filter' => [1 => "Tak", 0 => "Nie"],
                         
                        'format' => 'raw',
                        'value' => function ($data) {
                            return ($data->is_accept_rules2) ? Yii::t('backend_module_newsletter', 'Is Accept Rules Yes') : Yii::t('backend_module_newsletter', 'Is Accept Rules No');
                        }
                    ],
                    [
                        'class' => 'common\hooks\yii2\grid\ActionColumn',
                        'template' => '{update-email} {delete-email}',
                        'buttons' => [
                            'update-email' => function ($url) {
                                return Html::a(
                                                '<span class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></span>', $url, [
                                            'title' => Yii::t('backend', 'list-emails-edit'),
                                            'data-pjax' => '0',
                                                ]
                                );
                            },
                            'delete-email' => function ($url) {
                                return Html::a(
                                                '<span class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></span>', $url, [
                                            'title' => Yii::t('backend', 'list-emails-delete'),
                                            'data-confirm' => 'Czy na pewno usunąć ten element?',
                                            'data-pjax' => '0',
                                                ]
                                );
                            },
                        ],
                    ],
                ],
            ]);
            ?>
        </div>
    </section>

</div>
