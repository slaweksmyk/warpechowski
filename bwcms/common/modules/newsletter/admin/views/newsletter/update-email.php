<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('backend_module_newsletter', 'edit_email') .': '. $model->email;
?>
<div class="newsletter-group-update">

    <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
        </header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(); ?>

            <div class="row">
                <div class="col-12"><?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
                <div class="col-4"><?= $form->field($model, 'is_active')->checkBox(['maxlength' => true]) ?></div>
                <div class="col-4"><?= $form->field($model, 'is_accept_rules')->checkBox(['maxlength' => true]) ?></div>
            </div>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_newsletter', 'Create') : Yii::t('backend_module_newsletter', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </section>

</div>
