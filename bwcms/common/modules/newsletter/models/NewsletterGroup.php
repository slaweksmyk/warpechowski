<?php

namespace common\modules\newsletter\models;

use Yii;
use common\modules\newsletter\models\NewsletterEmail;

/**
 * This is the model class for table "{{%xmod_newsletter_group}}".
 *
 * @property integer $id
 * @property string $name
 *
 * @property XmodNewsletterEmail[] $xmodNewsletterEmails
 */
class NewsletterGroup extends \common\hooks\yii2\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_newsletter_group}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_newsletter', 'ID'),
            'name' => Yii::t('backend_module_newsletter', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewsletterEmails()
    {
        return $this->hasMany(NewsletterEmail::className(), ['group_id' => 'id'])->all();
    }
}
