<?php

namespace common\modules\newsletter\models;

use Yii;
use common\modules\newsletter\models\NewsletterGroup;

/**
 * This is the model class for table "{{%xmod_newsletter_email}}".
 *
 * @property integer $id
 * @property integer $group_id
 * @property string $email
 * @property string $hash
 * @property integer $is_active
 * @property integer $is_accept_rules
 *
 * @property XmodNewsletterGroup $group
 */
class NewsletterEmail extends \common\hooks\yii2\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%xmod_newsletter_email}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['email'], 'required'],
            [['id', 'group_id', 'is_active', 'is_accept_rules'], 'integer'],
            [['email', 'hash'], 'string', 'max' => 255],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => NewsletterGroup::className(), 'targetAttribute' => ['group_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('backend_module_newsletter', 'ID'),
            'group_id' => Yii::t('backend_module_newsletter', 'Group ID'),
            'email' => Yii::t('backend_module_newsletter', 'Email'),
            'hash' => Yii::t('backend_module_newsletter', 'Hash'),
            'is_active' => Yii::t('backend_module_newsletter', 'Is Active'),
            'is_accept_rules' => Yii::t('backend_module_newsletter', 'Is Accept Rules'),
            'is_accept_rules2' => Yii::t('backend_module_newsletter', 'Is Accept Rules2'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup() {
        return $this->hasOne(NewsletterGroup::className(), ['id' => 'group_id']);
    }

}
