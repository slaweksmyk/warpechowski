<?php

namespace common\modules\newsletter\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\newsletter\models\NewsletterEmail;

/**
 * NewsletterEmailSearch represents the model behind the search form about `common\modules\newsletter\models\NewsletterEmail`.
 */
class NewsletterEmailSearch extends NewsletterEmail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'group_id', 'is_active', 'is_accept_rules'], 'integer'],
            [['email', 'hash'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NewsletterEmail::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'group_id' => $this->group_id,
            'is_active' => $this->is_active,
            'is_accept_rules' => $this->is_accept_rules

        ]);

        $query->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'hash', $this->hash]);

        return $dataProvider;
    }
}
