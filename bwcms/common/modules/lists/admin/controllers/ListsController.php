<?php

namespace common\modules\lists\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use common\modules\lists\models\ArticlesListCategory;
use common\modules\lists\models\ArticlesListCategorySearch;

use common\modules\articles\models\Article;
use common\modules\types\models\TypeHash;
use common\modules\lists\models\ArticlesList; 
use common\modules\lists\models\ArticlesListSearch; 

use common\modules\lists\models\ArticlesListElements;
 
/**
 * ListsController implements the CRUD actions for ArticlesListCategory model.
 */
class ListsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ArticlesListCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticlesListCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionAddArticle(){
        $oArticle = Article::find()->where(["=", "id", Yii::$app->request->post("articleID")])->one();
        $oArticleList = ArticlesList::find()->where(["=", "id", Yii::$app->request->post("listID")])->one();
        
        $oCategory = $oArticleList->getCategories()->one();
        $oType = $oArticleList->getTypes()->one();
        
        if(!is_null($oCategory)){
            $aAddCategories = json_decode($oArticle->add_categories_hash);
            if(is_array($aAddCategories)){
                if(!in_array($oCategory->id, $aAddCategories)){
                    array_push($aAddCategories, $oCategory->id);
                }
            } else {
                $aAddCategories = [$oCategory->id];
            }
            
            $oArticle->add_categories_hash = json_encode($aAddCategories);
            $oArticle->save();
        }

        $oTypeHash = new TypeHash();
        $oTypeHash->article_id = $oArticle->id;
        $oTypeHash->type_id = $oType->id;
        $oTypeHash->save();
        
        $oArticleListTest = ArticlesListElements::find()->where(["=", "list_id", Yii::$app->request->post("listID")])->orderBy("position DESC")->one();
        if(!is_null($oArticleListTest)){
            $position = $oArticleListTest->position+1;
        } else { $position = 0; }
        $oArticlesListElement = ArticlesListElements::find()->where(["=", "list_id", Yii::$app->request->post("listID")])->andWhere(["=", "article_id", Yii::$app->request->post("articleID")])->one();
        if(is_null($oArticlesListElement)){
            $oArticlesListElement = new ArticlesListElements();
        }
        $oArticlesListElement->list_id = Yii::$app->request->post("listID");
        $oArticlesListElement->article_id = Yii::$app->request->post("articleID");
        $oArticlesListElement->position = $position;
        $oArticlesListElement->save();
    }
    
    public function actionRemoveElementFromList(){
        $oArticle = Article::find()->where(["=", "id", Yii::$app->request->post("articleID")])->one();
        $oArticleList = ArticlesList::find()->where(["=", "id", Yii::$app->request->post("listID")])->one();
        
        $oCategory = $oArticleList->getCategories()->one();
        $oType = $oArticleList->getTypes()->one();
        
        if(!is_null($oCategory)){
            $aAddCategories = json_decode($oArticle->add_categories_hash);
            if(is_array($aAddCategories)){
                $aAddCategories = array_diff($aAddCategories, [$oCategory->id]);
            } else {
                $aAddCategories = [];
            }
            
            $oArticle->add_categories_hash = json_encode($aAddCategories);
            $oArticle->save();
        }
        
        $oTypeHash = TypeHash::find()->where(["=", "article_id", $oArticle->id])->andWhere(["=", "type_id", $oType->id])->one();
        $oTypeHash->delete();
        
        $oArticlesListElements = ArticlesListElements::find()->where(["=", "list_id", Yii::$app->request->post("listID")])->andWhere(["=", "article_id", Yii::$app->request->post("articleID")])->one();
        $oArticlesListElements->delete();
    }
    
    public function actionRemoveElement(){
        $oArticlesListElements = ArticlesListElements::find()->where(["=", "list_id", Yii::$app->request->post("listID")])->andWhere(["=", "article_id", Yii::$app->request->post("articleID")])->one();
        $oArticlesListElements->delete();
    }
    
    public function actionSaveElement(){
        $oArticlesListElement = ArticlesListElements::find()->where(["=", "list_id", Yii::$app->request->post("listID")])->andWhere(["=", "article_id", Yii::$app->request->post("articleID")])->one();
        if(is_null($oArticlesListElement)){
            $oArticlesListElement = new ArticlesListElements();
        }
        $oArticlesListElement->list_id = Yii::$app->request->post("listID");
        $oArticlesListElement->article_id = Yii::$app->request->post("articleID");
        $oArticlesListElement->position = Yii::$app->request->post("index");
        $oArticlesListElement->save();
    }

    public function actionLists($id)
    {
        $searchModel = new ArticlesListSearch(); 
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams); 
        
        $dataProvider->query->andWhere(["=", "list_category_id", $id]);

        return $this->render('lists', [ 
            'id' => $id,
            'searchModel' => $searchModel, 
            'dataProvider' => $dataProvider, 
        ]); 
    }

    /**
     * Displays a single ArticlesListCategory model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    /**
     * Creates a new ArticlesListCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new ArticlesList();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['lists', 'id' => $id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new ArticlesListCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateCategory()
    {
        $model = new ArticlesListCategory();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create-category', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ArticlesListCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    /**
     * Updates an existing ArticlesListCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateList($id)
    {
        $model = ArticlesList::find()->where(["=", "id", $id])->one();
        
        if ($model->load(Yii::$app->request->post())) {
            $aPost = Yii::$app->request->post()["ArticlesList"];

            $model->category_id_hash = isset($aPost["category_id_hash"]) ? json_encode($aPost["category_id_hash"]) : null;
            $model->type_id_hash     = isset($aPost["type_id_hash"]) ? json_encode($aPost["type_id_hash"]) : null;
            $model->tag_id_hash      = isset($aPost["tag_id_hash"]) ? json_encode($aPost["tag_id_hash"]) : null;
            $model->author_id_hash   = isset($aPost["author_id_hash"]) ? json_encode($aPost["author_id_hash"]) : null;
            $model->save();

            return $this->redirect(['update-list', 'id' => $model->id]);
        } else {
            return $this->render('update-list', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ArticlesListCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    /**
     * Deletes an existing ArticlesListCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteList($id)
    {
        $oModel = ArticlesList::find()->where(["=", "id", $id])->one();
        $oModel->delete();

        return $this->redirect(['lists', "id" => $oModel->list_category_id]);
    }
    /**
     * Finds the ArticlesListCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ArticlesListCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ArticlesListCategory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
