<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;

/* @var $searchModel common\modules\lists\models\ArticlesListCategorySearch */

$this->title = Yii::t('backend_module_lists', 'Articles List Categories');
?>
<div class="articles-list-category-index">

    <p>
        <?= Html::a(Yii::t('backend_module_lists', 'Create Articles List Category'), ['create-category'], ['class' => 'btn btn-success']) ?>
    </p>
    
     <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <?php
 if(Yii::$app->request->get('per-page')){ echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>"; } ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?= GridView::widget([
                 'dataProvider' => $dataProvider,
                 'filterModel' => $searchModel,
                 'columns' => [
                        'id',
                        'name',
                        [
                            'class' => 'common\hooks\yii2\grid\ActionColumn',
                            'template' => '{update} {lists} {delete}',
                            'contentOptions' => ['style' => 'width: 100px;'],
                            'buttons' => [
                                'update' => function ($url) {
                                    return Html::a(
                                            '<span class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></span>', $url, [
                                            'title' => Yii::t('backend', 'edit'),
                                            'data-toggle' => "tooltip",
                                            'data-placement' => "bottom"
                                        ]
                                    );
                                },
                                'lists' => function ($url) {
                                    return Html::a(
                                            '<span class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-list-alt"></span></span>', $url, [
                                            'title' => "Wyświetl listy",
                                            'data-toggle' => "tooltip",
                                            'data-placement' => "bottom"
                                        ]
                                    );
                                },
                                'delete' => function ($url) {
                                    return Html::a(
                                            '<span class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></span>', Yii::$app->request->get("deleted") ? $url."&remove=1" : $url , [
                                            'title' => Yii::t('backend', 'delete'),
                                            'data-toggle' => "tooltip",
                                            'data-placement' => "bottom",
                                            'data-pjax' => 0,
                                            'data-confirm' => 'Czy na pewno usunąć ten element?',
                                            'data-method' => 'post',
                                        ]
                                    );
                                },
                            ],
                        ],
                 ],
             ]); ?>
        </div>
    </section>
</div>
