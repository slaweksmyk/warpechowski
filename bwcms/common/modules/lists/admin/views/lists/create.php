<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use common\modules\lists\models\ArticlesListCategory;

$this->title = Yii::t('backend_module_lists', 'Create Articles List');
?>
<div class="articles-list-create">
    
    <p>
        <?= Html::a('< Wróć do listy', ['lists'], ['class' => 'btn btn-outline-secondary']) ?>
    </p>

     <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
        </header>
        <div class="panel-body" >
            <?php $form = ActiveForm::begin(); ?>
                <div class="row">
                    <div class="col-4">
                        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-4">
                        <?= $form->field($model, 'list_category_id')->dropDownList(ArrayHelper::map(ArticlesListCategory::find()->all(), 'id', 'name'), ["prompt" => Yii::t('backend_module_lists', '- select category -')]); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_lists', 'Create') : Yii::t('backend_module_lists', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

           <?php ActiveForm::end(); ?>
        </div>
    </section>

</div>
