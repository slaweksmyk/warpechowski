<?php

use yii\helpers\Html;
 use common\hooks\yii2\grid\GridView;

/* @var $searchModel common\modules\lists\models\ArticlesListSearch */

$this->title = Yii::t('backend_module_lists', 'Articles Lists');
?>
<div class="articles-list-index">
    
    <p>
        <?= Html::a('< Wróć', ['index'], ['class' => 'btn btn-outline-secondary']) ?>
        <?= Html::a(Yii::t('backend_module_lists', 'Create Articles List'), ['create', "id" => $id], ['class' => 'btn btn-success']) ?>
    </p>
    
     <section class="panel full" >
        <header>
            <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Html::encode($this->title) ?>
            <form id="per-page">
                <span>Ilość na stronę:</span>
                <select name="per-page" class="form-control">
                    <?php if(Yii::$app->request->get('per-page')){ echo "<option selected disabled>".Yii::$app->request->get('per-page')."</option>"; } ?>
                    <option>20</option><option>50</option><option>100</option><option>200</option><option>500</option>
                </select>
            </form>
        </header>
        <div class="panel-body" >
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    [
                        'attribute' => 'list_category_id',
                        'value' => function($model){
                            return $model->getListCategory()->one()->name;
                        }
                    ],
                    'name',
                    [
                        'class' => 'common\hooks\yii2\grid\ActionColumn',
                        'template' => '{update-list} {delete-list}',
                        'contentOptions' => ['style' => 'width: 100px;'],
                        'buttons' => [
                            'update-list' => function ($url) {
                                return Html::a(
                                        '<span class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></span>', $url, [
                                        'title' => Yii::t('backend', 'edit'),
                                        'data-toggle' => "tooltip",
                                        'data-placement' => "bottom"
                                    ]
                                );
                            },
                            'delete-list' => function ($url) {
                                return Html::a(
                                        '<span class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></span>', Yii::$app->request->get("deleted") ? $url."&remove=1" : $url , [
                                        'title' => Yii::t('backend', 'delete'),
                                        'data-toggle' => "tooltip",
                                        'data-placement' => "bottom",
                                        'data-pjax' => 0,
                                        'data-confirm' => 'Czy na pewno usunąć ten element?',
                                        'data-method' => 'post',
                                    ]
                                );
                            },
                        ],
                    ],
                 ],
             ]); ?>
        </div>
    </section>
</div>
