<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->registerJsFile('/bwcms/common/modules/lists/admin/assets/js/script.js');
$this->registerCssFile('/bwcms/common/modules/lists/admin/assets/css/style.css');

$this->title = Yii::t('backend_module_lists', 'Update {modelClass}: ', [
    'modelClass' => 'Articles List',
]) . $model->name;
?>


<div class="articles-list-category-update">
    <p>
        <?= Html::a('< Wróć', ['lists', "id" => $model->list_category_id], ['class' => 'btn btn-outline-secondary']) ?>
    </p>

    <div class="row">
        <div class="col-8">
            <section class="panel full" >
                <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> 
                    Lista artykułów
                    <div class="list-search-area">
                        Wyszukaj artykuł:
                        <input type="search" name="search" placeholder="Wpisz nazwę artykułu" class="form-control" id="search-articles"/>
                    </div>
                </header>
                <div class="panel-body list-articles" data-list="<?= $model->id ?>" >
                    <div class="row list-header">
                        <div class="col-2">Lp.</div>
                        <div class="col-10">Nazwa artykułu</div>
                    </div>

                    <div class="list-sortable">
                        <?php foreach($model->getArticles(100) as $index => $oArticle){ ?>
                            <?php $no = $index+1; ?>
                            <div class="row single-row" data-index="<?= $no ?>" data-id="<?= $oArticle->id ?>">
                                <div class="col-2">
                                    <input type="number" value="<?= $no ?>" min="1" data-id="<?= $oArticle->id ?>" class="form-control list-article-index" readonly="readonly"/>
                                    <i class="list-article-edit" data-index="<?= $no ?>" title="Edytuj kolejność">✎</i>
                                </div>
                                <div class="<?= $oArticle->forced_position ? "col-9" : "col-10" ?>">
                                    <div class="list-article-handle" title="<?= $oArticle->title ?>">
                                        <?php
                                            if(strlen($oArticle->title) > 50){
                                                echo mb_substr($oArticle->title, 0, 50)."...";
                                            } else { echo $oArticle->title; }
                                        ?>
                                        <span><?= date("d.m.y H:i", strtotime($oArticle->date_display)) ?></span>
                                    </div>
                                </div>
                                <?php if($oArticle->forced_position){ ?>
                                    <div class="col-1">
                                        <span class="glyphicon glyphicon-pushpin remove-element" data-toggle="tooltip" data-placement="bottom" title="Usuń wymuszoną pozycję" data-id="<?= $oArticle->id ?>"></span>
                                        <?php if(count($model->getTypes()->all()) > 0){ ?>
                                            <span class="glyphicon glyphicon-trash delete-article" data-toggle="tooltip" data-placement="bottom" data-confirm="Czy na pewno usunąć ten element?" title="Usuń artykuł z listy" data-id="<?= $oArticle->id ?>"></span>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-4">
            <?php $form = ActiveForm::begin(); ?>
                <section class="panel full" >
                   <header>
                       <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Dane podstawowe
                   </header>
                   <div class="panel-body" >
                        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'display_name')->textInput(['maxlength' => true])->label("Nazwa wyświetlana") ?>
                        <?= $form->field($model, 'more_url')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'default_sort')->dropDownList(["sort ASC" => "Kolejności rosnąco", "sort DESC" => "Kolejności malejąco", "date_display ASC" => "Data rosnąco", "date_display DESC" => "Data malejąco", "title ASC" => "Alfabetycznie A-Z", "title DESC" => "Alfabetycznie Z-A", "id ASC" => "Kolejności dodania - rosnąco", "id DESC" => "Kolejności dodania - malejąco", "RAND()" => "Losowo"])->label("Domyślne sortowanie"); ?>

                        <div class="form-group">
                            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_lists', 'Create') : Yii::t('backend_module_lists', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>
                   </div>
               </section>
            
                <?php if(count($model->getTypes()->all()) > 0){ ?>
                    <section class="panel full" >
                       <header>
                           <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Dodaj do listy
                       </header>
                       <div class="panel-body" >
                            <?php echo $form->field($model, 'data[article_id]')->dropDownList([], ["id" => "ajax-load-article", "class" => "ajax-load-articles-single"])->label(false); ?>
                       </div>
                    </section>
                <?php } ?>

                <section class="panel full" >
                   <header>
                       <img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Konfiguracja listy
                   </header>
                   <div class="panel-body" >

                        <div class="form-group">
                            <label class="control-label" for="categories">Kategorie</label>
                            <div class="row">
                                <div class="col-12 article-types">
                                    <?php foreach($model->getCategories()->all() as $oCategory){ ?>
                                        <span>
                                            <input type="hidden" name="ArticlesList[category_id_hash][]" value="<?= $oCategory->id ?>"/>
                                            <span title="Usuń kategorię">x</span>
                                            <?= $oCategory->name ?>
                                        </span>
                                    <?php } ?>
                                </div>
                            </div>
                            <select id="categories" class="form-control ajax-load-categories" name="ArticlesList[category_id_hash][]"></select>
                            <div class="help-block"></div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="types">Typy</label>
                            <div class="row">
                                <div class="col-12 article-types">
                                    <?php foreach($model->getTypes()->all() as $oType){ ?>
                                        <span>
                                            <input type="hidden" name="ArticlesList[type_id_hash][]" value="<?= $oType->id ?>"/>
                                            <span title="Usuń typ artykułu">x</span>
                                            <?= $oType->name ?>
                                        </span>
                                    <?php } ?>
                                </div>
                            </div>
                            <select id="types" class="form-control ajax-load-article-types" name="ArticlesList[type_id_hash][]"></select>
                            <div class="help-block"></div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="tags">Tagi</label>
                            <div class="row">
                                <div class="col-12 article-types">
                                    <?php foreach($model->getTags()->all() as $oTag){ ?>
                                        <span>
                                            <input type="hidden" name="ArticlesList[tag_id_hash][]" value="<?= $oTag->id ?>"/>
                                            <span title="Usuń tag artykułu">x</span>
                                            <?= $oTag->name ?>
                                        </span>
                                    <?php } ?>
                                </div>
                            </div>
                            <select id="types" class="form-control ajax-load-article-tags" name="ArticlesList[tag_id_hash][]"></select>
                            <div class="help-block"></div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="authors">Autorzy</label>
                            <div class="row">
                                <div class="col-12 article-types">
                                    <?php foreach($model->getAuthors()->all() as $oAuthor){ ?>
                                        <span>
                                            <input type="hidden" name="ArticlesList[author_id_hash][]" value="<?= $oAuthor->id ?>"/>
                                            <span title="Usuń kategorię">x</span>
                                            <?= $oAuthor->firstname ?> <?= $oAuthor->surname ?>
                                        </span>
                                    <?php } ?>
                                </div>
                            </div>
                            <select id="authors" class="form-control ajax-load-authors" name="ArticlesList[author_id_hash][]"></select>
                            <div class="help-block"></div>
                        </div>

                        <div class="form-group">
                            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend_module_lists', 'Create') : Yii::t('backend_module_lists', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>
                   </div>
               </section>
    
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
