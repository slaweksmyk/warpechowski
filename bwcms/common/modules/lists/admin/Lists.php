<?php
namespace common\modules\lists\admin;

/**
 * files module definition class
 */
class Lists extends \common\hooks\yii2\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\lists\admin\controllers';
    public $defaultRoute = 'lists';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
