$(document).ready(function(){
    
    $("#ajax-load-article").change(function(){
        var listID = $(".list-articles").data("list");
        var articleID = $(this).val();
        
        $.post($("#homeDir").text()+"lists/lists/add-article/", { articleID: articleID, listID: listID })
        .done(function( data ) {
            location.reload();
        });
    });
    
    $(".list-sortable").sortable({ 
        handle: ".list-article-handle",
        containment: ".list-articles",
        stop: function( event, ui ){
            updateList();
            
            saveItem(ui.item);
        }
    });
    
    $("#search-articles").keyup(function(){
        var search = $(this).val().toLowerCase().trim();
        
        if(search === ""){
            $(".list-article-handle").removeClass("highlight");
        } else {
            $(".list-article-handle").each(function(index, value){
                if($(this).attr("title").toLowerCase().indexOf(search) > -1) {
                   $(this).addClass("highlight");
                } else {
                    $(this).removeClass("highlight");
                }
            });
        }
    });
    
    
    $("body").on("click", ".delete-article", function(){
        var listID = $(".list-articles").data("list");
        var articleID = $(this).data("id");
        
        $.post($("#homeDir").text()+"lists/lists/remove-element-from-list/", { articleID: articleID, listID: listID })
        .done(function( data ) {
            location.reload();
        });
    });
   
    $("body").on("click", ".remove-element", function(){
        var listID = $(".list-articles").data("list");
        var articleID = $(this).data("id");
        
        $.post($("#homeDir").text()+"lists/lists/remove-element/", { articleID: articleID, listID: listID })
        .done(function( data ) {
            location.reload();
        });
    });
   
    $("body").on("click", ".list-article-edit", function(){
        var index = $(this).data("index");
        
        if(!$(this).hasClass("save")){
            $(this).attr("title", "Zapisz zmiany").addClass("save").html("✓");
            $(".single-row[data-index='"+index+"']").find(".list-article-index").removeAttr("readonly");
        } else {
            $(this).attr("title", "Edytuj kolejność").removeClass("save").html("✎");
            
            var input = $(".single-row[data-index='"+index+"']").find(".list-article-index");
            input.attr("readonly", "readonly");
            
            var index = input.val()-1;
            var listID = $(".list-articles").data("list");
            var articleID = input.data("id");
            
            $.post($("#homeDir").text()+"lists/lists/save-element/", { articleID: articleID, index: index, listID: listID })
            .done(function( data ) {
                location.reload();
            });
        }
    });
    
    function updateList(){
        $(".single-row").each(function(index, value){
            $(this).find(".list-article-index").val((index+1));
        });
    }
    
    function saveItem(item){
        var index = item.find(".list-article-index").val()-1;
        var listID = $(".list-articles").data("list");
        var articleID = item.data("id");

        $.post($("#homeDir").text()+"lists/lists/save-element/", { articleID: articleID, index: index, listID: listID })
        .done(function( data ) {
            location.reload();
        });
    }
    
});