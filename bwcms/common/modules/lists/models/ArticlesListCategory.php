<?php

namespace common\modules\lists\models;

use Yii;
use common\modules\lists\models\ArticlesList;

/**
 * This is the model class for table "xmod_articles_lists_categories".
 *
 * @property integer $id
 * @property string $name
 *
 * @property ArticlesList[] $xmodArticlesLists
 */
class ArticlesListCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_articles_lists_categories}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'display_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_lists', 'ID'),
            'name' => Yii::t('backend_module_lists', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticlesList()
    {
        return $this->hasMany(ArticlesList::className(), ['list_category_id' => 'id']);
    }
    
    public function __get($name) {
        if (isset(Yii::$app->request->url) && strpos(Yii::$app->request->url, \Yii::$app->request->baseUrl) !== false) {
            return parent::__get($name);
        } else {
            if($name == "name" && $this->display_name != ""){
                return parent::__get("display_name");
            }
        }

        return parent::__get($name);
    }
    
}
