<?php

namespace common\modules\lists\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\lists\models\ArticlesList;

/**
 * ArticlesListSearch represents the model behind the search form about `common\modules\lists\models\ArticlesList`.
 */
class ArticlesListSearch extends ArticlesList
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'list_category_id', 'is_subcategories', 'limit_pc', 'limit_tablet', 'limit_mobile'], 'integer'],
            [['name', 'category_id_hash', 'type_id_hash', 'tag_id_hash', 'author_id_hash', 'more_url', 'default_sort'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ArticlesList::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'list_category_id' => $this->list_category_id,
            'is_subcategories' => $this->is_subcategories,
            'limit_pc' => $this->limit_pc,
            'limit_tablet' => $this->limit_tablet,
            'limit_mobile' => $this->limit_mobile,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'category_id_hash', $this->category_id_hash])
            ->andFilterWhere(['like', 'type_id_hash', $this->type_id_hash])
            ->andFilterWhere(['like', 'tag_id_hash', $this->tag_id_hash])
            ->andFilterWhere(['like', 'author_id_hash', $this->author_id_hash])
            ->andFilterWhere(['like', 'more_url', $this->more_url])
            ->andFilterWhere(['like', 'default_sort', $this->default_sort]);

        return $dataProvider;
    }
}
