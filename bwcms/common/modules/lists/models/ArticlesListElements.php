<?php

namespace common\modules\lists\models;

use Yii;
use common\modules\articles\models\Article;
use common\modules\lists\models\ArticlesList;

/**
 * This is the model class for table "xmod_articles_lists_elements".
 *
 * @property integer $id
 * @property integer $list_id
 * @property integer $article_id
 * @property integer $position
 *
 * @property ArticlesList $list
 * @property Article $article
 */
class ArticlesListElements extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_articles_lists_elements}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['list_id', 'article_id', 'position'], 'integer'],
            [['list_id'], 'exist', 'skipOnError' => true, 'targetClass' => ArticlesList::className(), 'targetAttribute' => ['list_id' => 'id']],
            [['article_id'], 'exist', 'skipOnError' => true, 'targetClass' => Article::className(), 'targetAttribute' => ['article_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_lists', 'ID'),
            'list_id' => Yii::t('backend_module_lists', 'List ID'),
            'article_id' => Yii::t('backend_module_lists', 'Article ID'),
            'position' => Yii::t('backend_module_lists', 'Position'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getList()
    {
        return $this->hasOne(ArticlesList::className(), ['id' => 'list_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'article_id']);
    }
}
