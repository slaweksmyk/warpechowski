<?php

namespace common\modules\lists\models;

use Yii;
use common\modules\tags\models\Tag;
use common\modules\types\models\Type;
use common\modules\types\models\TypeHash;
use common\modules\authors\models\Author;
use common\modules\articles\models\Article;
use common\modules\lists\models\ArticlesListCategory;
use common\modules\categories\models\ArticlesCategories;

/**
 * This is the model class for table "xmod_articles_lists".
 *
 * @property integer $id
 * @property integer $list_category_id
 * @property string $name
 * @property string $category_id_hash
 * @property integer $is_subcategories
 * @property string $type_id_hash
 * @property string $tag_id_hash
 * @property string $author_id_hash
 * @property string $more_url
 * @property string $default_sort
 * @property integer $limit_pc
 * @property integer $limit_tablet
 * @property integer $limit_mobile
 *
 * @property ArticlesListCategory $listCategory
 * @property XmodArticlesListsElements[] $xmodArticlesListsElements
 */
class ArticlesList extends \yii\db\ActiveRecord
{
    public $data;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%xmod_articles_lists}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['list_category_id'], 'required'],
            [['list_category_id', 'is_subcategories', 'limit_pc', 'limit_tablet', 'limit_mobile'], 'integer'],
            [['category_id_hash', 'type_id_hash', 'tag_id_hash', 'author_id_hash'], 'string'],
            [['name', 'display_name', 'more_url', 'default_sort'], 'string', 'max' => 255],
            [['list_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ArticlesListCategory::className(), 'targetAttribute' => ['list_category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend_module_lists', 'ID'),
            'list_category_id' => Yii::t('backend_module_lists', 'List Category ID'),
            'name' => Yii::t('backend_module_lists', 'Name'),
            'category_id_hash' => Yii::t('backend_module_lists', 'Category Id Hash'),
            'is_subcategories' => Yii::t('backend_module_lists', 'Is Subcategories'),
            'type_id_hash' => Yii::t('backend_module_lists', 'Type Id Hash'),
            'tag_id_hash' => Yii::t('backend_module_lists', 'Tag Id Hash'),
            'author_id_hash' => Yii::t('backend_module_lists', 'Author Id Hash'),
            'more_url' => Yii::t('backend_module_lists', 'More Url'),
            'default_sort' => Yii::t('backend_module_lists', 'Default Sort'),
            'limit_pc' => Yii::t('backend_module_lists', 'Limit Pc'),
            'limit_tablet' => Yii::t('backend_module_lists', 'Limit Tablet'),
            'limit_mobile' => Yii::t('backend_module_lists', 'Limit Mobile'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getListCategory()
    {
        return $this->hasOne(ArticlesListCategory::className(), ['id' => 'list_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getXmodArticlesListsElements()
    {
        return $this->hasMany(XmodArticlesListsElements::className(), ['list_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypes() {
        $aTypes = [];
        if($this->type_id_hash != "" && $this->type_id_hash != "null"){
            $aTypes = json_decode($this->type_id_hash);
        }
        
        return Type::find()->where(["IN", "id", $aTypes]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags() {
        $aTags = [];
        if($this->tag_id_hash != "" && $this->tag_id_hash != "null"){
            $aTags = json_decode($this->tag_id_hash);
        }

        return Tag::find()->where(["IN", "id", $aTags]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories() {
        $aCategories = [];
        if($this->category_id_hash != "" && $this->category_id_hash != "null"){
            $aCategories = json_decode($this->category_id_hash);
        }
        
        return ArticlesCategories::find()->where(["IN", "id", $aCategories]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthors() {
        $aAuthors = [];
        if($this->author_id_hash != "" && $this->author_id_hash != "null"){
            $aAuthors = json_decode($this->author_id_hash);
        }
        
        return Author::find()->where(["IN", "id", $aAuthors]);
    }
    
    public function getArticles($limit = 10, $onlyQuery = false, $offset = 0){
        $oArticleQuery = Article::find()->where(['=', "status_id", 2])->orderBy($this->default_sort)->limit($limit);
        
        if($this->category_id_hash != "" && $this->category_id_hash != "null"){
            $aCategories = json_decode($this->category_id_hash);

            $aAddCats = ["OR"];
            foreach($aCategories as $iCatID){
                $aAddCats[] = ["LIKE", "add_categories_hash", $iCatID];
            }

            $oArticleQuery->andWhere(["OR",
                ["IN", "category_id", $aCategories],
                $aAddCats
            ]);
        }

        if($this->author_id_hash != "" && $this->author_id_hash != "null"){
            $oArticleQuery->andWhere(["IN", "author_id", json_decode($this->author_id_hash)]);
        }
        
        if($this->type_id_hash != "" && $this->type_id_hash != "null"){
            $oTypeHash = TypeHash::find()->select("article_id as ID")->where(["IN", "type_id", json_decode($this->type_id_hash)]);
            $oArticleQuery->andWhere(['IN', 'ID', $oTypeHash]);
            //$oArticleQuery->joinWith("types")->andWhere(["IN", "type_id", json_decode($this->type_id_hash)]);
        }
        
        $oArticlesListElements = [];
        if(!isset($_GET["page"])){
            $oArticlesListElements = ArticlesListElements::find()->where(["=", "list_id", $this->id])->all();
        }
        
        $aElements = [];
        foreach($oArticlesListElements as $oArticlesListElement){
            $aElements[$oArticlesListElement->position] = $oArticlesListElement->article_id;
        }
        
        if(count($aElements) > 0){
            $oArticleQuery->andWhere(["NOT IN", "xmod_articles.id", $aElements]);
        }
		
        if($onlyQuery){
            return $oArticleQuery->limit($limit);
        }

        $oArticlesRowset = $oArticleQuery->limit($limit)->offset($offset)->all();

        if(count($oArticlesRowset) > 0){
            foreach($oArticlesRowset as $index => $oArticle){
                if(isset($aElements[$index])){
                    $oArticle = Article::find()->where(["=", "id", $aElements[$index]])->one();
                    $oArticle->forced_position = true;

                    array_splice($oArticlesRowset, $index, 0, [$oArticle]); 
                    unset($aElements[$index]);
                }
            }
            foreach($aElements as $index => $id){
                $oArticle = Article::find()->where(["=", "id", $id])->one();
                $oArticle->forced_position = true;
                $oArticlesRowset[$index] = $oArticle;
            }
        } else if(count($aElements) > 0) {
            foreach($aElements as $index => $id){
                $oArticle = Article::find()->where(["=", "id", $id])->one();
                $oArticle->forced_position = true;
                $oArticlesRowset[] = $oArticle;
            }
        }
        
        $oArticlesRowset = array_slice($oArticlesRowset, 0, $limit);

        return $oArticlesRowset;
    }
    
    public function __get($name) {
        if (isset(Yii::$app->request->url) && strpos(Yii::$app->request->url, \Yii::$app->request->baseUrl) !== false) {
            return parent::__get($name);
        } else {
            if($name == "name" && $this->display_name != ""){
                return parent::__get("display_name");
            }
        }

        return parent::__get($name);
    }
    
    
}
