<?php
return [
    'language' => 'pl-PL',
    'sourceLanguage' => 'PHP 7.0',
    'timeZone' => 'Europe/Warsaw',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'bootstrap' => ['settingsLoader', 'logPanel', 'devicedetect'],
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'feed' => [
            'class' => 'yii\feed\BaseListView',
        ],
        'i18n' => [
            'translations' => [
                'frontend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
                'backend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
            ],
        ],
        'settingsLoader' => [
           'class' => 'common\components\SettingsLoader',
        ],
        'logPanel' => [
            'class' => 'common\components\LogPanel',
        ],
        'LayoutsHelper' => [
           'class' => 'common\components\LayoutsHelper',
        ],
        'WidgetHelper' => [
           'class' => 'common\components\WidgetHelper',
        ],
        'ImageHelper' => [
           'class' => 'common\components\image\ImageHelper',
        ],
	'devicedetect' => [
            'class' => 'alexandernst\devicedetect\DeviceDetect'
	],
    ],
];