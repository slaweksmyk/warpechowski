<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message/extract' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'menu_config' => '@@Overall Configuration@@',
    'menu_label' => '@@Customer Loyalty Scheme@@',
    'menu_products' => '@@Products List@@',
    'menu_referral_config' => '@@Recommended Users Configuration@@',
    'menu_referrals' => '@@Recommended Users List@@',
    'menu_users' => '@@Users@@',
    'Are you sure you want to delete this item?' => 'Are you sure you want to delete this item?',
    'Auto Calc' => 'Auto Calculating',
    'Auto Calc Perc' => 'Used Price Percent',
    'Create' => 'Add',
    'Delete' => 'Delete',
    'Discount Code' => 'Discount Code',
    'ID' => 'ID',
    'Is Enabled' => 'Is Active',
    'Loyalty Products' => 'Products List',
    'Loyalty Referrals' => 'Recommended Users List',
    'Loyalty Users' => 'Users List',
    'Points' => 'Points',
    'Product ID' => 'Product',
    'Ref User ID' => 'Recommended',
    'Type' => 'Type',
    'Update' => 'Save',
    'User ID' => 'User',
];
