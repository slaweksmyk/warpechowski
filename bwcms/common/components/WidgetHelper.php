<?php
/**
 * Layouts Helper
 * 
 * PHP version 7
 *
 * @author Volodymyr Shelelo <contact@profficode.com>
 * @copyright (c) 2016 Volodymyr Shelelo
 * @version 1.0
 */

namespace common\components;
 
use Yii;
use yii\base\Component;
use common\modules\widget\models\WidgetMain;
use common\modules\widget\models\WidgetItems;
 
class WidgetHelper extends Component
{
    
    /**
     * Get ITEM WIDGET HTML
     * 
     * @param array $item
     * @param string $type
     * @return mixed
     */
    public static function getWidgetItem( $item, $type ){
        $view = Yii::getAlias('@common/modules/widget/admin/views/template/item.php');
        
        $widget = WidgetMain::getDb()->cache(function () use ($item) { 
            return WidgetMain::findOne( $item['widget_id'] );
        });
  
        return Yii::$app->controller->renderFile($view, [
            'id' => $item['id'],
            'group' => $widget->group,
            'dafault_name' => $widget->name,
            'custom_name' => $item['custom_name'],
            'is_active' => $item['is_active'],
            'page_id' => $item['page_id'],
            'type' => $type
        ]);
    }
    
    
    /**
     * Get layouts list from web-site
     * 
     * @return array
     */
    public static function getWidgetLayouts($widget) 
    {
        $dir = Yii::getAlias('@common/widgets/'.$widget->folder.'/views');
        $scan = scandir($dir); $layouts = array();
        
        foreach($scan as $value){
            if($value !== '.' && $value !== '..' ){
                $value = str_replace('.php', '', $value);
                $layouts[$value] = $value;
            }
        }
        
        return $layouts;
    }
    
    
    /**
     * Get all widgets on position
     * 
     * @param array $oPage
     * @param string $position
     */
    public static function getWidgets($oPage, $position)
    {
        $itemsModel = new WidgetItems();
        $widgetModel = new WidgetMain();
        
        $wtable = $widgetModel->tableName();
        $itable = $itemsModel->tableName();
        
        $allItems = WidgetItems::getDb()->cache(function () use ($itemsModel, $wtable, $itable, $position, $oPage) { 
            $query = $itemsModel->find();
            $query->leftJoin($wtable, "{$wtable}.`id` = {$itable}.`widget_id`");
            $query->where([ $itable.'.page_id' => $oPage['id'] ]);
            $query->orWhere([ $itable.'.layout_id' => $oPage['layout_id'], $itable.'.page_id' => null]);
            $query->andWhere([ $itable.'.position' => $position, $itable.'.is_active' => 1,  ]);
            $query->orderBy($itable.'.sort ASC');
            return $query->all();
        });

        foreach($allItems as $item){
            self::showWidget($item);
        }
    }
    
    
    /**
     * Show one item widget
     * 
     * @param array $item
     */
    protected static function showWidget($item){
        $settings = null;
        if( $item['data'] && is_string($item['data']) ){ $settings = unserialize($item['data']); }
        
        if( $settings ){
            $settings['widget_item_id'] = $item['id'];
            
            $widgetClass = $item['widget']['action'];
            echo $widgetClass::widget($settings);
        }
    }
    
}