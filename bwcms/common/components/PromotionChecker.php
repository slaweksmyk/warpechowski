<?php
/**
 * Settings Loader
 * 
 * Chceck for promotion
 * 
 * PHP version 7
 *
 * @author Throk <k.borecki@throk.pl>
 * @copyright (c) 2016 Throk
 * @version 1.0
 */
namespace common\components;

use Yii;
use yii\base\Component;

use common\modules\users\models\UserData;
use common\modules\promotion\models\PromotionBday;
use common\modules\promotion\models\PromotionBdayArchive;

class PromotionChecker extends Component
{
    private $oBday;
    
    /**
     * The constructor.
     * Is setting language into default Yii language app
     */
    public function init(){
        parent::init();
        
        if(is_a(Yii::$app,'yii\web\Application')){
            $this->oBday = PromotionBday::find()->where(["=", "id", 1])->one();
            
            $this->checkBday();
            $this->checkNday();
        }
    }
    
    private function checkBday(){
        if($this->oBday->getBdayDiscount()->one()){
            $oBdayUserRowset = UserData::find()->where(["LIKE", "birth_date", date("m-d")])->all();
            foreach($oBdayUserRowset as $oBdayUser){
                $oArchive = PromotionBdayArchive::find()->where(["=", "user_id", $oBdayUser->user_id])->andWhere(["=", "code_id", $this->oBday->getBdayDiscount()->one()->id])->andWhere(["=", "type", 1])->one();
                if(is_null($oArchive)){
                    $oArchive = new PromotionBdayArchive();
                    $oArchive->user_id = $oBdayUser->user_id;
                    $oArchive->code_id = $this->oBday->getBdayDiscount()->one()->id;
                    $oArchive->date = date("Y-m-d H:i:s");
                    $oArchive->type = 1;
                    $oArchive->save();
                    
                    Yii::$app->mailer->compose()
                        ->setTo($oBdayUser->getUser()->one()->email)
                        ->setSubject($this->oBday->bday_email_title)
                        ->setHtmlBody(str_replace("{CODE}", $this->oBday->getBdayDiscount()->one()->code, $this->oBday->bday_email_body))
                        ->send();
                }
            }
        }
    }

    private function checkNday(){
        if($this->oBday->getNdayDiscount()->one()){
            foreach(explode(",", $this->oBday->nDayData()[date("m-d")]) as $sName){
                $oUser = UserData::find()->where(["LIKE", "firstname", substr(trim($sName), 0, -1)])->one();
                if(!is_null($oUser)){
                    $oArchive = PromotionBdayArchive::find()->where(["=", "user_id", $oUser->user_id])->andWhere(["=", "code_id", $this->oBday->getBdayDiscount()->one()->id])->andWhere(["=", "type", 0])->one();
                    if(is_null($oArchive)){
                        $oArchive = new PromotionBdayArchive();
                        $oArchive->user_id = $oUser->user_id;
                        $oArchive->code_id = $this->oBday->getBdayDiscount()->one()->id;
                        $oArchive->date = date("Y-m-d H:i:s");
                        $oArchive->type = 0;
                        $oArchive->save();

                        Yii::$app->mailer->compose()
                            ->setTo($oUser->getUser()->one()->email)
                            ->setSubject($this->oBday->nday_email_title)
                            ->setHtmlBody(str_replace("{CODE}", $this->oBday->getNdayDiscount()->one()->code, $this->oBday->nday_email_body))
                            ->send();
                    }
                }
            }
        }
    }
}



