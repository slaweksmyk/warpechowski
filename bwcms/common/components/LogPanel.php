<?php
/**
 * LogPanel
 * 
 * Display module-only log panel
 * 
 * PHP version 7
 *
 * @author Throk <k.borecki@throk.pl>
 * @copyright (c) 2016 Throk
 * @version 1.0
 */
namespace common\components;

use Yii;
use yii\base\Component;
use common\modules\pages\models\PagesAdmin;
use common\modules\logs\models\LogSearch;
 
class LogPanel extends Component
{
    /**
     * The constructor.
     * Is setting language into default Yii language app
     */
    public function init(){
        parent::init();
    }  
    
    public static function displayPanel($moduleID){
        $model = PagesAdmin::findOne(["url" => $moduleID]);
        if($model){
            $iModuleID = $model->module_id;
            if(!$iModuleID OR $iModuleID == 10){ return; }

            $searchModel = new LogSearch();
            $dataProvider = $searchModel->search([]);
            $dataProvider->query->where("module_id = '{$iModuleID}'");
            $dataProvider->query->orderBy("ID DESC");
            $dataProvider->pagination->pageSize = 5;

            return \Yii::$app->view->renderFile('@app/components/view/log/panel.php', [
                'dataProvider' => $dataProvider
            ]);
        }
    }
    
}



