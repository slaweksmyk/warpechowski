<?php
/**
 * Layouts Helper
 * 
 * PHP version 7
 *
 * @author Volodymyr Shelelo <contact@profficode.com>
 * @copyright (c) 2016 Volodymyr Shelelo
 * @version 1.0
 */

namespace common\components\image;
 
use Yii;
use yii\base\Component;
use common\components\image\ImageResize;
use common\components\image\SimpleImage;
use common\modules\files\models\File;
 
class ImageHelper extends Component
{
    
    /**
     * Crop Thumbnail
     * 
     * @param integer $id
     * @param string $filename
     * @param array $size
     * @return boolean
     */
    public static function createBaseThumbnail($id = false, $filename = false, $size = ['width' => 300, 'height' => 300])
    {
        if ( !$filename && $id ) {
            $model = File::findOne([ 'id' => $id ]);
            if($model) { $filename = $model->filename; }
        }
        
        if( $filename ){
            $file = Yii::getAlias('@root/upload/'.$filename);
            
            $image = new ImageResize($file);
            $image->crop($size['width'], $size['height']);
            
            $crop = Yii::getAlias('@root/upload/thumbnail/'.$filename);
            $save = $image->save($crop);
            
            if($save){ return 1; } else { return 0; } 
        } else {
            return 0;
        }
    }
    
    
    /**
     * Crop images in the gallery
     * 
     * @param string $filename
     * @param string $data
     * @param integer $id
     * @return boolean|string
     */
    public static function imageGalleryCrop(string $filename, string $data)
    {
        $file = Yii::getAlias('@root/upload/'.$filename);
        $cropData = json_decode($data);
        
        $image = new ImageResize($file);
        $image->freecrop($cropData->width, $cropData->height, $cropData->x, $cropData->y);
        $image->flip($cropData->scaleX, $cropData->scaleY);
        $save = $image->save($file);

        if($save) { return $filename; } 
        else { return false; } 
    }
    
}