<?php
/**
 * Display chart
 * 
 * PHP version 7
 *
 * @author Throk <k.borecki@throk.pl>
 * @copyright (c) 2018 Throk
 * @version 1.0
 */
namespace common\components;

use yii\base\Component;
 
class Chart extends Component
{
    /**
     * The constructor.
     * Is setting language into default Yii language app
     */
    public function init(){
        parent::init();
    }  
    
    public static function display($sTitle, $sType, $sStyle){
        
        
        return \Yii::$app->view->renderFile('@common/components/views/chart/default.php', [
            'sTitle' => $sTitle
        ]);
    }
    
}



