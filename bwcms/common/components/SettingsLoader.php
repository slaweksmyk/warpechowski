<?php
/**
 * Settings Loader
 * 
 * Loads settings created by user in Settings Controller
 * 
 * PHP version 7
 *
 * @author Throk <k.borecki@throk.pl>
 * @copyright (c) 2016 Throk
 * @version 1.0
 */
namespace common\components;

use Yii;
use yii\base\Component;
use common\modules\files\models\File;
use common\modules\config\models\Config;
 
class SettingsLoader extends Component
{
    /**
     * The constructor.
     * Is setting language into default Yii language app
     */
    public function init(){
        parent::init();

        if(is_a(Yii::$app,'yii\web\Application')){
            Yii::$app->params['oConfig'] = Config::find()->one();
            $oConfig = Yii::$app->params['oConfig'];

            Yii::$app->name = $oConfig->page_name;
            Yii::$app->mailer->setTransport(
                Yii::$app->mailer->createTransport([
                    'class' => 'Swift_SmtpTransport',
                    'host' => $oConfig->smtp_host,
                    'username' => $oConfig->smtp_username,
                    'password' => $oConfig->smtp_password,
                    'port' => $oConfig->smtp_port,
                    'encryption' => $oConfig->smtp_encryption
                ])
            );

            Yii::$app->mailer->messageConfig = [
                "from" => $oConfig->default_email
            ];
            
            if($oConfig->is_ssl && $this->getProtocol() == "http"){
                $sNewUrl = str_replace("http", "https", Yii::$app->request->absoluteUrl);

                Yii::$app->response->redirect($sNewUrl, 301);
                Yii::$app->end();
            }

           if(!$oConfig->is_ssl && $this->getProtocol() == "https"){
                $sNewUrl = str_replace("https", "http", Yii::$app->request->absoluteUrl);

                Yii::$app->response->redirect($sNewUrl, 301);
                Yii::$app->end();
            }

            $aParsedUrl = parse_url(Yii::$app->request->absoluteUrl);
            if($oConfig->is_url_www && strpos($aParsedUrl["host"], "www") === false){
                $sNewUrl = str_replace($aParsedUrl["host"], "www.".$aParsedUrl["host"], Yii::$app->request->absoluteUrl);

                Yii::$app->response->redirect($sNewUrl, 301);
                Yii::$app->end();
            }

            if(!$oConfig->is_url_www && strpos($aParsedUrl["host"], "www") !== false){
                $sNewUrl = str_replace("www.", "", Yii::$app->request->absoluteUrl);

                Yii::$app->response->redirect($sNewUrl, 301);
                Yii::$app->end();
            }

            if(is_numeric($oConfig->seo_fb_image)){
                $oThumbnail = File::find()->where(["=", "id", $oConfig->seo_fb_image])->one();
                Yii::$app->params['seo_fb_image'] = Yii::$app->request->hostInfo."/upload/{$oThumbnail->filename}";
            }

            Yii::$app->db->enableSchemaCache = $oConfig->cache_database;
            Yii::$app->db->schemaCacheDuration = $oConfig->cache_database_duration;
            Yii::$app->params["pageCacheSystem"] = [
                'enabled' => $oConfig->cache_full_page,
                'cacheTime' => $oConfig->cache_full_page_time,
                'cacheDir' => "/cache/pages/"
            ];
        }
    }
    
    private function getProtocol() {
      $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || 
        $_SERVER['SERVER_PORT'] == 443) ? "https" : "http";
      return $protocol;
    }
    
}



