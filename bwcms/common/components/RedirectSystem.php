<?php
/**
 * Settings Loader
 * 
 * Loads settings created by user in Settings Controller
 * 
 * PHP version 7
 *
 * @author Throk <k.borecki@throk.pl>
 * @copyright (c) 2016 Throk
 * @version 1.0
 */
namespace common\components;

use Yii;
use yii\base\Component;
use common\modules\redirect\models\Redirect;
 
class RedirectSystem extends Component
{
    /**
     * The constructor.
     */
    public function init(){
        parent::init();

        $oRedirect = Redirect::find()->where(["=", "url", Yii::$app->request->absoluteUrl])->andWhere(["=", "is_active", 1])->one();
        if(!is_null($oRedirect)){
            Yii::$app->response->redirect($oRedirect->target, 301);
            Yii::$app->end();
            exit;
        }
    }  
}



