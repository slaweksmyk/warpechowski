<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['corechart', 'line']});
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {
    var data = new google.visualization.DataTable();
    data.addColumn('number', 'Dzień');
    data.addColumn('number', 'Zyskowność');
    data.addColumn('number', 'Kwota zamówień');

    data.addRows([
        [1,  37.8, 80.8],
        [2,  30.9, 69.5],
        [3,  25.4,   57],
        [4,  11.7, 18.8],
        [5,  11.9, 17.6],
        [6,   8.8, 13.6],
        [7,   7.6, 12.3],
        [8,  12.3, 29.2],
        [9,  16.9, 42.9],
        [10, 12.8, 30.9],
        [11,  5.3,  7.9],
        [12,  6.6,  8.4],
        [13,  4.8,  6.3],
        [14,  4.2,  6.2]
    ]);

    var options = {
        height: 400,
        colors: [
            '#dc3545', '#ffae00'
        ],
        legend: {
            position: 'bottom',
            textStyle: {
                fontSize: "14px"
            }
        }
    };

    var chart = new google.charts.Line(document.getElementById('chart_div'));

    chart.draw(data, options);
  }
</script>

<section class="panel full">
    <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= $sTitle ?></header>
    <div class="panel-cnt">
       <div id="chart_div"></div>
    </div>
</section>