<?php
/**
 * Layouts Helper
 * 
 * PHP version 7
 *
 * @author Volodymyr Shelelo <contact@profficode.com>
 * @copyright (c) 2016 Volodymyr Shelelo
 * @version 1.0
 */

namespace common\components;
 
use Yii;
use yii\base\Component;
use common\modules\widget\models\WidgetMain;
use common\modules\widget\models\WidgetItems;
 
class LayoutsHelper extends Component
{
    
    /**
     * Get layouts list from web-site
     * 
     * @return array
     */
    public static function getLayoutsList() 
    {
        $dir = Yii::getAlias('@frontend/views/layouts');
        $scan = scandir($dir); $layouts = array();
        
        foreach($scan as $value){
            if($value !== '.' && $value !== '..' ){
                $layouts[$value] = $value;
            }
        }
        
        return $layouts;
    }
    
    
    /**
     * Get POSITIONS in Layout (by name)
     * 
     * @param string $name
     * @return array
     */
    public static function getLayoutPositions($name) {
        require_once Yii::getAlias('@frontend/views/layouts/'.$name.'/positions.php');
        return get_layout_positions();
    }
    
    
    /**
     * Get Layout structure view (by name)
     * 
     * @param string $name
     * @param bool $edit
     * @return mixed
     */
    public static function getLayoutStructure($name, $type, $layout_id, $page_id = null) {
        $positions = self::getLayoutPositions($name);
        
        $itemsModel = new WidgetItems();
        $query = $itemsModel->find();
        $query->where([ 'page_id' => $page_id ]);
        $query->orWhere([ 'layout_id' => $layout_id, 'page_id' => null]);
        $query->orderBy('sort ASC');
        $addItems = $query->all();
        //$addItems = $itemsModel->find()->where([ 'layout_id' => $layout_id ])->orderBy('sort ASC')->all();
        
        $widgetsItems = self::getWidgetItems();
        $widgetsList = Yii::getAlias('@common/modules/widget/admin/views/template/widgets.php');
        $widgetsAll = Yii::$app->controller->renderFile($widgetsList, ['widgets' => $widgetsItems]);
        
        $view = Yii::getAlias('@common/modules/widget/admin/views/template/template.php');
        $result = Yii::$app->controller->renderFile($view, [
            'positions' => $positions, 
            'type' => $type, 
            'layout_id' => $layout_id,
            'page_id' => $page_id,
            'widget_list' => $widgetsAll,
            'add_items' => $addItems,
        ]);
        
        return $result;
    }
    
    
    /**
     * Get all widgets list
     * 
     * @return array
     */
    protected static function getWidgetItems() {
        $model = new WidgetMain();
        return $model->find()->orderBy('group ASC')->all();
    }
    
}