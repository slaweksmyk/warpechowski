<?php
/**
 * Base class for text modeling
 *
 * @since  1.0
 * @author Tomasz Załucki <z4lucki@gmail.com>
 */
namespace common\helpers;

//use Yii;

class TextHelper {
    
    /**
     * Extension for substr native function. 
     * We added dots on the end text when will he have $lentgth > $length
     * 
     * @param string $message
     * @param integer $start
     * @param integer $length
     *
     * @return string
     */
    public static function substr(string $message, int $start, int $length) {
        // Helper - Strip html tags
        
        /*
         * We not added htmlspecialchars because this is added in getter & setter in Article Model 
         * \common\modules\articles\models\Article.php
         */
        $message = strip_tags($message);
        
        if (mb_strlen($message) > $length) {
            $string = mb_substr($message, $start, $length) . "...";
        } else {
            $string = $message;
        }
        return $string;
    }
}
