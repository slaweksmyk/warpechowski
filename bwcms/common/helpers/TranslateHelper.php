<?php

namespace common\helpers;

use Yii;
use common\modules\translation\models\Translation;

class TranslateHelper
{
    
     /**
     * Translation
     * 
     * @return string
     */
    public static function t($sCode = null) 
    {
        
        $oTranslation = Translation::find()->where(["=", "code", $sCode])->one();
        if(is_null($oTranslation)){ return; }  
        
        return $oTranslation->translation;
    }

    
}