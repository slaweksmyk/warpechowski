<?php

namespace common\helpers;

use Yii;
use yii\helpers\FileHelper;
use common\modules\pages\models\PagesSite;

class CacheHelper
{
    
     /**
     * Cache
     * 
     * @return string
     */
    public static function cachePage()
    {
        self::proceedCache();
    }

    public static function recachePage($iPageID){
        $cacheDir   = Yii::$app->params['pageCacheSystem']['cacheDir'];
        if(file_exists(Yii::getAlias("@root{$cacheDir}{$iPageID}/"))){
            $sCacheFile = null;

            foreach(scandir(Yii::getAlias("@root{$cacheDir}{$iPageID}/")) as $sFile){
                if(strpos($sFile, ".html") !== false){
                    $sCacheFile = $sFile; break;
                }
            }

            if($sCacheFile){ unlink(Yii::getAlias("@root{$cacheDir}{$iPageID}/{$sCacheFile}/")); }
        }
    }
  
    protected static function proceedCache(){
        Yii::$app->response->on(\yii\web\Response::EVENT_BEFORE_SEND, function (\yii\base\Event $event){
            $sCacheFile = null;
            $diff       = null;
            $page       = PagesSite::getByUrl(['id']);
            $cacheDir   = Yii::$app->params['pageCacheSystem']['cacheDir'];
            
            if(file_exists(Yii::getAlias("@root{$cacheDir}{$page["page"]->id}/"))){
                $aFiles = scandir(Yii::getAlias("@root{$cacheDir}{$page["page"]->id}/"));

                if($aFiles !== false){
                    foreach($aFiles as $sFile){
                        if(strpos($sFile, ".html") !== false){
                            $sCacheFile = $sFile; break;
                        }
                    }

                    $cachedTime = intval(pathinfo($sCacheFile)["filename"]);
                    $currentTime = time();

                    $diff = $currentTime - $cachedTime;
                }
            }
            
            if($diff >= Yii::$app->params['pageCacheSystem']['cacheTime'] OR $sCacheFile === null){
                if($sCacheFile){ unlink(Yii::getAlias("@root{$cacheDir}{$page["page"]->id}/{$sCacheFile}")); }
                $filename = time().".html";
                
                FileHelper::createDirectory(Yii::getAlias("@root{$cacheDir}{$page["page"]->id}/"));
                file_put_contents(Yii::getAlias("@root{$cacheDir}{$page["page"]->id}/{$filename}"), $event->sender->data);
            } else {
                echo file_get_contents(Yii::getAlias("@root{$cacheDir}{$page["page"]->id}/{$sCacheFile}"));
                exit;
            }
        });
    }
    
}