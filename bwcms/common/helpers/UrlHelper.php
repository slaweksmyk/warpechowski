<?php

namespace common\helpers;

use Yii;

class UrlHelper
{
    
    /**
     * Change Lang Diacritics
     * 
     * @param string $sText
     * @param string $lang
     * @return string
     */
    public static function clearDiacritics($sText, $lang = 'pl') {
        switch ($lang) {
            case 'pl':
                $aReplace = array( // PL
                    'ą' => 'a', 'ę' => 'e', 'ś' => 's', 'ć' => 'c',
                    'ó' => 'o', 'ń' => 'n', 'ż' => 'z', 'ź' => 'z', 'ł' => 'l',
                    'Ą' => 'A', 'Ę' => 'E', 'Ś' => 'S', 'Ć' => 'C',
                    'Ó' => 'O', 'Ń' => 'N', 'Ż' => 'Z', 'Ź' => 'Z', 'Ł' => 'L'
                );
                break;
        }
        
        return str_replace(array_keys($aReplace), array_values($aReplace), $sText);
    }
    
    
    /**
     * Prepare URL (string to url)
     * 
     * @param string $sText
     * @return string
     */
    public static function prepareURL($sText)
    {
        // special chart of lang
        $sText = self::clearDiacritics($sText);

        $sText = strtolower($sText);
        $sText = str_replace(' ', '-', $sText);
        $sText = preg_replace('/[^0-9a-z\-]+/', '', $sText);
        $sText = preg_replace('/[\-]+/', '-', $sText);
        $sText = trim($sText, '-');

        return $sText;
    }
    
    
    /**
     * Get Base site URL
     * 
     * @return string
     */
    public static function getBaseUrl() 
    {
        return Yii::$app->request->getHostInfo();
    }

}