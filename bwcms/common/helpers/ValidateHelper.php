<?php

namespace common\helpers;

use Yii;
use yii\validators\UrlValidator;
use yii\validators\EmailValidator;
use yii\validators\NumberValidator;

class ValidateHelper
{
    private static $aErrors = [];
    
    /*
     * Check if value is a valid e-mail adress
     * @return mixed value
     */
    public static function isEmail($value){
        $validator = new EmailValidator();
        if ($validator->validate($value, $error)) {
            return true;
        } else {
            return $error;
        }
    }
    
    /*
     * Check if value is a valid number
     * @return mixed value
     */
    public static function isNumber($value){
        $validator = new NumberValidator();
        if ($validator->validate($value, $error)) {
            return true;
        } else {
            return $error;
        }
    }
    
    /*
     * Check if value is a valid url
     * @return mixed value
     */
    public static function isUrl($value){
        $validator = new UrlValidator();
        if ($validator->validate($value, $error)) {
            return true;
        } else {
            return $error;
        }
    }
    
    /*
     * Init validation
     */
    public static function validate($post){
        foreach($post as $key => $value){
            $tmp = explode("-", $key);

            if(count($tmp) == 5){
                if(isset($tmp[0])){ $type = $tmp[0]; }
                if(isset($tmp[1])){ $minLength = $tmp[1]; }
                if(isset($tmp[2])){ $maxLength = $tmp[2]; }
                if(isset($tmp[3])){ $isRequired = $tmp[3]; }
                if(isset($tmp[4])){ $fieldName = $tmp[4]; }

                $aErrors = [];
                if($isRequired){
                    if(strlen($value) < $minLength) { array_push($aErrors, Yii::t('validation', 'err_too_short')); }
                    if(strlen($value) > $maxLength) { array_push($aErrors, Yii::t('validation', 'err_too_long')); }
                    
                    switch($type){

                        case "int":
                            $valid = self::isNumber($value);
                            if($valid !== true){ array_push($aErrors, Yii::t('validation', 'err_not_number')); }
                            break;
                            break;
                        
                        case "email":
                            $valid = self::isEmail($value);
                            if($valid !== true){ array_push($aErrors, Yii::t('validation', 'err_not_email')); }
                            break;
                        
                        case "url":
                            $valid = self::isUrl($value);
                            if($valid !== true){ array_push($aErrors, Yii::t('validation', 'err_not_url')); }
                            break;
                    }
                    
                    if(!empty($aErrors)){
                       self::$aErrors[$fieldName] = $aErrors; 
                    }
                }
            }
        }
        
        return self::$aErrors;
    }
    
    /*
     * Reparse $_POST request
     */
    public static function parsePost($post){
        
        $returnPOST = [];
        foreach($post as $key => $value){
            $tmp = explode("-", $key);
            $fieldName = $key;
            
            if(count($tmp) == 5){
                if(isset($tmp[4])){ $fieldName = $tmp[4]; }
            }
            
            $returnPOST[$fieldName] = $value;
        }
        
        return $returnPOST;
    }
    
    
}