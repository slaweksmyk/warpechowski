<?php

/**
 * Base class for text xss filtering
 *
 * @since  1.0
 * @author Tomasz Załucki <z4lucki@gmail.com>
 */

namespace common\helpers;

class XssHelper
{

    /**
     * Catch all get and Xss filter
     *  
     * @param array $string
     * @param integer $intval
     *
     * @return array
     */
    public static function get($string, $intval)
    {
        if (is_array($string)) {
            array_walk_recursive($string, array(__CLASS__, 'filterGet'));
        } else {
            $string = self::strong($string, $intval, 'get');
        }
        return $string;
    }

    /**
     * Bridget function for @see XssHelper::get         Multi Get Xss Haleper
     *  
     * @param array $string
     * @param integer $intval
     *
     * @return array
     */
    public static function filterGet(&$string, $intval)
    {
        return self::strong($string, $intval, 'get');
    }

    /**
     * Catch all post and Xss filter
     *  
     * @param array $string
     * @param integer $intval
     *
     * @return array
     */
    public static function post($string, $intval)
    {
        if (is_array($string)) {
            array_walk_recursive($string, array(__CLASS__, 'filterPost'));
        } else {
            $string = self::strong($string, $intval, 'post');
        }
        return $string;
    }

    /**
     * Bridget function for @see XssHelper::post         Multi :post Xss Haleper
     *  
     * @param array $string
     * @param integer $intval
     *
     * @return array
     */
    public static function filterPost(&$string, $intval)
    {
        return self::strong($string, $intval, 'post');
    }

    /**
     * Strong function delete all xss tags and html tags with their content.
     * 
     * @param string $string
     * @param int $intval
     * @param void $sql

     * @param string $name
     *
     * @return string
     */
    public static function strong($string, $intval = false, $sql = false, $name = false): string
    {

        if ($sql && $sql === "get") {

            $string = str_replace(self::sqlGet(), "", $string);
            $string = trim(htmlspecialchars(self::strip_tags_content($string)));
        } elseif ($sql && $sql === "post") {

            $string = str_replace(self::sqlPost(), "", $string);
            $string = trim(self::strip_tags_content($string));
        } else {
            $string = trim(self::strip_tags_content($string));
        }

        if ($intval == "intval") {
            $string = intval($string);
        }

        return $string;
    }

    public static function filterColumn(array $column, array $filter): array
    {

        /*
         * filterColumn
         * Delete model's column from @array $filter
         */
        return array_diff($column, $filter);
    }

    /**
     * Strip tags around word with html tag
     *
     * @return array
     */
    private static function strip_tags_content($text, $tags = '', $invert = FALSE)
    {

        preg_match_all('/<(.+?)[\s]*\/?[\s]*>/si', trim($tags), $tags);
        $tags = array_unique($tags[1]);

        if (is_array($tags) AND count($tags) > 0) {
            if ($invert == FALSE) {
                return preg_replace('@<(?!(?:' . implode('|', $tags) . ')\b)(\w+)\b.*?>.*?</\1>@si', '', $text);
            } else {
                return preg_replace('@<(' . implode('|', $tags) . ')\b.*?>.*?</\1>@si', '', $text);
            }
        } elseif ($invert == FALSE) {
            return preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text);
        }
        return $text;
    }

    /**
     * Dictionary sql injection
     *
     * @return array
     */
    private static function sqlGet(): array
    {
        $words = [
            'NULL',
            'UNION ALL select',
            'UNION ALL',
            'UNION',
            'SELECT *',
            'SELECT',
            '(SELECT',
            '( SELECT',
            '(select',
            '( select',
            '^',
            '%',
            '&amp;',
            '<',
            '&lt;',
            '>',
            '&gt;',
            '"',
            '&quot;',
            "'",
            '&#x27;',
            '/',
            '&#x2F;',
            '$',
            '1=1',
            '1=0',
            '0=0',
            '1/0',
            '1/1',
            '0/0',
            '0=1',
            "'1'='1'",
            "'1'='1",
            "'1'=1",
            '=',
            '--',
            'AND',
            '&&',
            'OR',
            '||',
            '|',
            "'",
            '"',
        ];
        return $words;
    }

    /**
     * Dictionary sql injection
     *
     * @return array
     */
    private static function sqlPost(): array
    {
        $words = [
            '(SELECT',
            '( SELECT',
            '(select',
            '( select',
            '^',
            '$',
            '1=1',
            '1=0',
            '0=0',
            '1/0',
            '1/1',
            '0/0',
            '0=1',
            "'1'='1'",
            "'1'='1",
            "'1'=1",
            '=',
            '--',
            'AND',
            '&&',
            'OR',
            '||',
            '|',
            'NULL',
            'UNION ALL select',
            'UNION ALL',
            'UNION',
        ];
        return $words;
    }

}
