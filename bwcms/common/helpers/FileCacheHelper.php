<?php
 
namespace common\helpers;
 
use Yii;
 
class FileCacheHelper
{
    private static $sCacheDirectory = "@root/cache/data/";
    private static $refleshTime = "-15 minutes";
   
    public static function getFromCache($name){
        $sFilePath = Yii::getAlias("@root/cache/data/{$name}.data");

        if(file_exists($sFilePath)){
            if(Yii::$app->session->get("cache_{$name}") > date("Y-m-d H:i:s", strtotime(self::$refleshTime))){
                $cacheFile = fopen($sFilePath, "r");
                $data = fread($cacheFile,filesize($sFilePath));
                fclose($cacheFile);

                return $data;
            }
            unlink($sFilePath);
            return null;
        }
        return null;
    }
   
    public static function saveToCache($name, $data)
    {
        $cacheFile = fopen(Yii::getAlias("@root/cache/data/{$name}.data"), "w");
        fwrite($cacheFile , $data);
        fclose($cacheFile);
       
        Yii::$app->session->set("cache_{$name}", date("Y-m-d H:i:s"));
    }
   
}