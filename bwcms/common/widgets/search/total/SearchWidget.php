<?php

/**
 * Search Panel
 * 
 *  Version 2
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @author Tomasz Załucki <z4lucki@gmail.com>
 * @copyright (c) 2016 Krzysztof Borecki
 * @copyright (c) 2017 Tomasz Załucki
 * @version 2.0
 */

namespace common\widgets\search\total;

use Yii;
use common\modules\articles\models\ArticleSearch;
use common\helpers\FileCacheHelper;

class SearchWidget extends \common\hooks\yii2\bootstrap\Widget {

    public $widget_item_id;
    public $limit;
    private $phrase;
    private $typeID;
    private $tag;
    private $sessionBlock = true;
    
    private static $sessionTime = "-3 second";
      
      
    // Id flagi / xmod_articles_types_hash
    private $includedTypes = [
        ['id' => 26, 'name' => 'Analizy'],
        ['id' => 27, 'name' => 'Wywiady'],
        ['id' => 28, 'name' => 'Opinie'],
        ['id' => 29, 'name' => 'Komentarze'],
        ['id' => 30, 'name' => 'Wiadomości']
    ];

    /**
     * Build widget
     */
    public function init() {
        parent::init();

        $this->phrase = Yii::$app->request->get('search', false) ? Yii::$app->request->get('search', false) : null;
        if(!$this->phrase){
            $this->phrase = Yii::$app->request->get('tag', false) ? Yii::$app->request->get('tag', false) : null;
        }
        $this->typeID = Yii::$app->request->get('type');

        if (Yii::$app->request->get('search', false)) {
            $this->tag = false;
        }

        if (Yii::$app->request->get('tag', false)) {
            $this->tag = \common\modules\tags\models\Tag::find()->where(['slug' => $this->phrase])->one();
            $this->tag = is_null($this->tag) ? false : $this->tag;
        }
    }

    /**
     * Run widget
     */
    public function run() {

        $session = Yii::$app->session;
        $sessionSearch = $session->get('search');
 
        if ($sessionSearch['date']  > date("Y-m-d H:i:s", strtotime(self::$sessionTime))) {
           $this->sessionBlock = false;
        } else {
           $this->sessionBlock = true;
           Yii::$app->session->set("search", [
                "date" => date("Y-m-d H:i:s")
              
            ]);
        }


      






        if (strlen($this->phrase) > 2 && $this->sessionBlock) {
            return $this->search();
        } else {
            return $this->searchEmpty();
        }
    }

    public function search() {

        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // $dataProvider->query->joinWith("status s")->where(['=', "s.is_published", 1]);
        $dataProvider->query->joinWith([
            'status s' => function ($query) {
                $query->andWhere(['=', 's.is_published', 1]);
            }
        ]);






        if ($this->phrase) {
            if ($this->tag) {
                $dataProvider->query->joinWith(['tags t'])->where(['t.tag_id' => $this->tag->id]);
                $this->phrase = $this->tag->name;
            } else {
                $dataProvider->query->andWhere(["OR",
                    ['LIKE', "title", $this->phrase],
                    ['LIKE', "full_description", $this->phrase],
                    ['LIKE', "short_description", $this->phrase],
                    ['LIKE', "summary_description", $this->phrase]
                ]);
            }
        }

        if (!$this->tag && $dataProvider->getTotalCount() > 0) {
            if ($this->typeID) {
                $dataProvider->query->joinWith(['types typ'])->where(['typ.type_id' => $this->typeID]);
            } else {
                //$dataProvider->query->joinWith(['types typ'])->where(['>', 'typ.type_id', 1]);
            }
        }
        $dataProvider->setSort([
            'defaultOrder' => ['date_created' => SORT_DESC],
        ]);

        $dataProvider->pagination = array(
            'pagesize' => $this->md_limit,
            'route' => 'wyszukiwanie',
        );

        //var_dump($dataProvider->query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);

        return $this->render($this->layout, [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'phrase' => $this->phrase,
                    'aSearchedTypes' => $this->includedTypes,
                    'limit' => $this->md_limit,
                    'tag' => $this->tag,
                    'count' => $dataProvider->getTotalCount()
        ]);
    }

    public function searchEmpty() {
        return $this->render('empty', [
                    'phrase' => $this->phrase,
                    'tag' => $this->tag,
        ]);
    }

}
