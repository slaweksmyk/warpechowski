<section id="search" data-item="search" class=" block block-title block-subtitle search ">
    <div class="search-breadcrumb">
        <div class="container ">
            <div class="section"> <h1>Szukaj: <?= $sPhrase ?></h1></div>
        </div>
    </div>
    <div class="container mt-30">
        <div class="dropdown slider-dropdown mt-20">
            <button class="btn btn-dark dark dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Wiadomości</button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <ul class="nav-slider-logo nav nav-pills  justify-content-start" id="nav-1" role="tablist"><!-- !!!!!!!!! nav ID -->
                    <li class="nav-item">
                        <?php foreach($aSearchedTypes as $oArticleType){ ?>
                            <a href="/wyszukiwanie/?search=<?= $sPhrase ?>&type=<?= $oArticleType->type_id ?>" class="nav-link <?php if($oArticleType->type_id == \Yii::$app->request->get('type')){ echo "active"; } ?>"><span><?= $oArticleType->getType()->one()->name ?></span></a>
                        <?php } ?>
                    </li>
                </ul>
            </div>
        </div>
        <ul class="block-list-image list-light mt-20">
            <?php foreach($oArticleFiltred as $index => $oArticle){ ?>
                <?php if($index < 3){ ?>
                    <li>
                        <a href="<?= $oArticle->getUrl() ?>" title="<?= $oArticle->title ?>">
                            <div class="row">
                                <div class="col-4">
                                    <?php if($oArticle->hasThumbnail()){ ?>
                                        <div class="block-card-image img-res">
                                            <img src="<?= $oArticle->getThumbnail(80,80,"crop") ?>" alt="<?= $oArticle->title ?>">
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="col-8 no-p-left">
                                    <h4><?= $oArticle->title ?></h4>
                                </div>
                            </div>
                        </a>
                    </li>
                <?php } ?>
            <?php } ?>
         </ul>
        <div class="row">
            <div class="col-12">
                <ul class="list search background">
                    <?php foreach($oArticleFiltred as $index => $oArticle){ ?>
                        <?php if($index > 3){ ?>
                            <li>
                                <a href="<?= $oArticle->getUrl() ?>" title="<?= $oArticle->title ?>"><strong><?= date("d F Y", strtotime($oArticle->date_display)) ?></strong> <?= $oArticle->title ?></a>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</section>