<?php

use yii\widgets\ListView;
?>

<section id="search" data-item="search" class=" block block-title block-subtitle search ">
    <div class="search-breadcrumb">
        <div class="container section">
           
            <h1>Wynik wyszukiwania dla <?= $tag ? 'tagu' : 'frazy'?>: <?= $phrase ?></h1>
           
            
        </div>
    </div>
    <div class="container mt-30">
        <?php if(!$tag){ ?>
        <div class="row">
            <div class="col-12">
                <div class="search-area">
                    <a href="/wyszukiwanie/?search=<?= $phrase ?>" 
                       title="Wyszukiwarka" 
                       class="item <?php
                       if (!\Yii::$app->request->get('type')) {
                           echo "active";
                       }
                       ?>"
                       >Wszystko
                    </a>
                    <?php foreach ($aSearchedTypes as $oArticleType) { ?>

                        <a href="/wyszukiwanie/?search=<?= $phrase ?>&type=<?= $oArticleType['id'] ?>" 
                           title="<?= $oArticleType['name'] ?>" 
                           class="item <?php
                           if ($oArticleType['id'] == \Yii::$app->request->get('type')) {
                               echo "active";
                           }
                           ?>"
                           ><?= $oArticleType['name'] ?>
                        </a>


                    <?php } ?>
                    <div class="line"></div>
                </div>
            </div>
        </div>
        <?php } ?>
       
        <?=
        ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => 'item/item',
            // 'summary'=>'', 
             'emptyTextOptions' => ['class' => 'empty'],
             'emptyText' => '<i class="empty-icon"></i> Brak wyników wyszkiwania dla frazy: <strong>'.$phrase.'</strong>',
            'layout' => "<div class='list-items'>{items}</div>\n<div class='mt-30 col-12 d-flex justify-content-center'>{pager}</div>",
            'options' => [
                'tag' => false,
            ],
            'itemOptions' => [
                'tag' => false,
            ],
            'viewParams' => ['limit' => $limit],
            'pager' => [
                'firstPageLabel' => 'pierwsza',
                'lastPageLabel' => 'ostatnia',
             //   'prevPageLabel' => '<i class="icon i-page-left"></i>',
             //   'nextPageLabel' => '<i class="icon i-page-right"></i>',
                'prevPageLabel' => false,
                'nextPageLabel' => false,
                'maxButtonCount' => 5,
                 'class' => 'common\hooks\yii2\widgets\LinkPager'
            ],
        ]);
        ?>
    </div>
</section>