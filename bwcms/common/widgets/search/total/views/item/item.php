<?php 
$formatter = \Yii::$app->formatter;
Yii::$app->formatter->locale = 'pl-PL';

if ($index == 0) { ?>
    <div class="row mt-30">
    <?php } ?>
    <?php if ($index <= 2) { ?>
        <!-- block card shadow-->
        <div class="col-4 block-card shadow h-200">
            <div class="card-wrapper">
                <a href="<?= $model->getAbsoluteUrl() ?>" title="<?= $model->title ?>">
                    <?php if ($model->hasThumbnail()) { ?>
                        <div class="image img-res">
                            <img src="<?= $model->getThumbnail(370, 200, "crop") ?>" alt="<?= $model->title ?>">
                        </div>
                    <?php } ?>
                    <div class="title">
                        <div class="txt">
                            <strong><?= $model->getCategory()->name ?? "" ?></strong>
                            <h4><?= $model->title ?></h4>
                        </div>
                    </div>
                </a>
                <div class="social d-flex justify-content-end align-content-end text-right">
                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?= $model->getAbsoluteUrl() ?>" title="Udostępnij na facebooku"><i class="icon icon-icon-facebook-small"></i></a> 
                    <a href="https://twitter.com/home?status=<?= $model->getAbsoluteUrl() ?>" title="Udostępnij na twitterze"><i class="icon icon-icon-twitter-small"></i></a>
                </div>
            </div>
        </div>
    <?php } ?>
    <?php if ($index == 2) { ?>
    </div>
<?php } ?>
<?php if ($index == 3) { ?>   
    <div class="row">
        <div class="col-12">
            <ul class="list search mt-30">
            <?php } ?>

            <?php if ($index >= 3) { ?>  
                <li>
                    <a href="<?= $model->getAbsoluteUrl() ?>" title="<?= $model->title ?>"><strong><?= $formatter->asDate($model->date_display, 'long');?></strong> <?= $model->title ?></a>
                </li>
            <?php } ?>

            <?php if ($index == ($limit - 1)) { ?>    
            </ul>
        </div>
    </div>
<?php } ?>