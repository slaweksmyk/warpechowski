<section id="search" data-item="search" class=" block block-title block-subtitle search ">
    <div class="search-breadcrumb">
        <div class="container section">
            <h1>Wynik wyszukiwania dla <?= $tag ? 'tagu' : 'frazy' ?>: <?= $phrase ?></h1>
        </div>
    </div>
    <div class="container mt-30">
        <div class="empty">
            <i class="empty-icon"></i> Brak wyników wyszkiwania dla <?=$phrase?>. Wpisz conajmniej 3 znaki.
        </div>
    </div>
</section>