<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\modules\map\models\Map;

?>

<div class="widget-main-form" >
    
    <!-- ADD WIDGET CUSTOM DATA -->
    <div class="row" style="background: #f8f8f8; padding-top: 15px; padding-bottom: 15px; " >
        <div class="col-4" >
            <?php echo $form->field($model, 'data[map_id]')->dropDownList(ArrayHelper::map(Map::find()->all(), 'id', 'name'), ["prompt" => "- wybierz mapę -"])->label("Mapa"); ?>
        </div>
    </div>
    <!-- END CUSTOM DATA -->
    
    <br>
    <div class="form-group">
        <?= Html::submitButton( Yii::t('backend', 'update'), ['class' => 'btn btn-primary']) ?>
    </div>

</div>