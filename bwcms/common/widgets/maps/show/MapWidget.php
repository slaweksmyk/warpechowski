<?php
namespace common\widgets\maps\show;

use Yii;
use common\modules\map\models\Map;
use common\modules\map\models\MapMarker;

class MapWidget extends \common\hooks\yii2\bootstrap\Widget
{
    
    public $widget_item_id;
    public $layout;
    public $map_id;
    
    /**
     * Build Menu widget
     */
    public function init()
    {
        parent::init();
    }
    
    /**
     * Run widget
     * 
     * @return menu HTML
     */
    public function run()
    {
        $oMap = Map::findOne($this->map_id);
        if(is_null($oMap)) { return; }
         
        $oMarkersRowset = $oMap->getMapMarkers()->where(["=", "is_active", 1])->all();
        
        return $this->display([
            'oMap' => $oMap,
            'oMarkersRowset' => $oMarkersRowset
        ]);
    }
    
}