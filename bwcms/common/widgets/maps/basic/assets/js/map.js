$(document).ready(function(){
    
    var map_lat = parseFloat( $('#google-map').data('lat') );
    var map_lng = parseFloat( $('#google-map').data('lng') );
    var mzoom = parseInt( $('#google-map').data('zoom' ));
    var mtitle = $('#google-map').data('title');
    
    if( map_lat && map_lng && mzoom ) {
        var map;
        var draggable = true;
        if( $(window).width()<768 ){ /*draggable = false;*/ }
        function init() {
            var mapOptions = {
                center: new google.maps.LatLng(map_lat, map_lng),
                zoom: mzoom,
                zoomControl: true,
                zoomControlOptions: { style: google.maps.ZoomControlStyle.DEFAULT },
                disableDoubleClickZoom: false,
                mapTypeControl: false,
                scaleControl: true,
                scrollwheel: false,
                panControl: false,
                streetViewControl: false,
                draggable : draggable,
                overviewMapControl: false,
                overviewMapControlOptions: { opened: false },
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: []
            };
            var mapElement = document.getElementById('google-map');
            
            map = new google.maps.Map(mapElement, mapOptions);
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(map_lat, map_lng),
                map: map,
                title: mtitle
            });
        }
        google.maps.event.addDomListener(window, 'load', init);
    }
        
});