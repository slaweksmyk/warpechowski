<?php
//add CSS
$this->registerCssFile('/bwcms/common/widgets/maps/basic/assets/css/map.css');

//add script
$this->registerJsFile('/bwcms/common/widgets/maps/basic/assets/js/map.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<script src='https://maps.googleapis.com/maps/api/js' ></script>

<section>
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12">
                <h3>Znajdziesz nas</h3>
            </div>
            <div class="col-12 col-sm-12 col-md-12">
                <div id="google-map" data-key="<?php echo $key; ?>" data-lat="<?php echo $lat; ?>" data-lng="<?php echo $lng; ?>" data-title="<?php echo $title; ?>" data-zoom="<?php echo $zoom; ?>"></div>
            </div>
        </div>
    </div>
</section>