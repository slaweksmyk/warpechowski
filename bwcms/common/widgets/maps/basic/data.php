<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="widget-main-form" >
    
    <!-- ADD WIDGET CUSTOM DATA -->
    <div class="row" style="background: #f8f8f8; padding-top: 15px; padding-bottom: 15px; " >
        <div class="col-12 col-sm-12" >
            <?php echo $form->field($model, 'data[api_key]')->textInput()->label( Yii::t('backend_widget_maps', 'Api key') ); ?>
        </div>
        <div class="col-12 col-sm-4" >
            <?php $zoom = array(); $start = 1; for($i = 10; $i < 17; $i++){ $zoom[$i] = $start; $start++; } ?>
            <?php echo $form->field($model, 'data[map_zoom]')->dropDownList( $zoom )->label( Yii::t('backend_widget_maps', 'Zoom') ); ?>
        </div>
        <div class="col-12 col-sm-4" >
            <?php echo $form->field($model, 'data[lat]')->textInput()->label( 'LAT' ); ?>
        </div>
        <div class="col-12 col-sm-4" >
            <?php echo $form->field($model, 'data[lng]')->textInput()->label( 'LNG' ); ?>
        </div>
        <div class="col-12" >
            <?php echo $form->field($model, 'data[title]')->textInput()->label( Yii::t('backend', 'name') ); ?>
        </div>
    </div>
    <!-- END CUSTOM DATA -->
    
    <br>
    <div class="form-group">
        <?= Html::submitButton( Yii::t('backend', 'update'), ['class' => 'btn btn-primary']) ?>
    </div>

</div>