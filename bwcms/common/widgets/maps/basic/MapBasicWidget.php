<?php
/**
 * Widget MENU (Link list)
 * 
 * PHP version 7
 *
 * @author Volodymyr Shelelo <contact@profficode.com>
 * @copyright (c) 2016 Volodymyr Shelelo
 * @version 1.0
 */

namespace common\widgets\maps\basic;

use Yii;
use yii\helpers\Html;

class MapBasicWidget extends \common\hooks\yii2\bootstrap\Widget
{
    
    public $widget_item_id;
    public $layout;
    public $api_key;
    public $map_zoom;
    public $lat;
    public $lng;
    public $title;
    
    /**
     * Build Menu widget
     */
    public function init()
    {
        parent::init();
    }
    
    /**
     * Run widget
     * 
     * @return menu HTML
     */
    public function run()
    {
        
        return $this->display([
            'key' => $this->api_key,
            'zoom' => $this->map_zoom,
            'lat' => $this->lat,
            'lng' => $this->lng,
            'title' => $this->title
        ]);
    }
    
}