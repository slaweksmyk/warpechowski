<h1><?= $oCategory->name ?></h1>
<?php foreach($oCategory->getChildrens()->all() as $oSubCategory){ ?>
    <?php if(count($oSubCategory->getFiles()->all()) > 0){ ?>
        <table class="table-file-list">
            <?php foreach($oSubCategory->getFiles()->all() as $oFile){ ?>
                <tr>
                    <td>
                        <img src="/upload/<?= $oFile->getFile()->one()->filename ?>" alt="<?= $oFile->name; ?>"/>
                    </td>
                    <td>
                        <h5><?= $oFile->name; ?></h5>
                    </td>
                    <td>
                        <a class="btn" href="/download.php?id=<?= $oFile->getFile()->one()->id ?>">Pobierz</a>
                    </td>
                </tr>
            <?php } ?>
        </table>
    <?php } else { ?>
        <p>nie dodano jeszcze żadnych plików</p>
    <?php } ?>
<?php } ?>