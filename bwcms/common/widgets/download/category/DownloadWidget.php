<?php

namespace common\widgets\download\category;

use Yii;
use common\modules\download\models\DownloadsCategories;

class DownloadWidget extends \common\hooks\yii2\bootstrap\Widget
{
    
    public $widget_item_id;
    public $category_id;
    public $layout;
    public $limit;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $oCategory = DownloadsCategories::find()->where(["id" => $this->category_id])->one();

        return $this->display([
            'oCategory' => $oCategory,
            'limit' => $this->md_limit
        ]);
    }
    
}
