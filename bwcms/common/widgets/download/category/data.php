<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\modules\download\models\DownloadsCategories;
?>

<div class="widget-main-form" >

    <!-- ADD WIDGET CUSTOM DATA -->
    <div class="row" style="background: #f8f8f8; padding-top: 15px; padding-bottom: 15px; " >
        <div class="col-4">
            <?php 
                echo $form->field($model, 'data[category_id]')->dropDownList(ArrayHelper::map(DownloadsCategories::find()->all(), 'id', 'name'))->label( Yii::t('backend', 'category'));
            ?>
        </div>
        <div class="col-4">
            <?php 
                $widgetLayouts = Yii::$app->WidgetHelper->getWidgetLayouts( $widget );
                echo $form->field($model, 'data[layout]')->dropDownList( $widgetLayouts )->label( Yii::t('backend', 'layout') );
            ?>
        </div>
        <div class="col-4" >
            <?php echo $form->field($model, 'data[limit]')->textInput(["type" => "number"])->label("Limit"); ?>
        </div>
    </div>
    <!-- END CUSTOM DATA -->
    
    <br>
    <div class="form-group">
        <?= Html::submitButton( Yii::t('backend', 'update'), ['class' => 'btn btn-primary']) ?>
    </div>

</div>