
<div class="lang">
    <ul>
        <?php foreach($aLanguages as $sLang){ ?>
        <li class="<?php if($sLang["isCurrent"]){ ?>active<?php } ?>">
            <a title="<?= $sLang["short"] ?>" href="/<?= strtolower($sLang["short"]) ?>/"><?= $sLang["short"] ?></a>
        </li>
        <?php } ?>
    </ul>
</div>