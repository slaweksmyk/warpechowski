<?php
/**
 * Widget HTML
 * 
 * PHP version 7
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @copyright (c) 2016 Krzysztof Borecki
 * @version 1.0
 */

namespace common\widgets\language\changer;

use Yii;
use yii\helpers\BaseUrl;

class ChangerWidget extends \common\hooks\yii2\bootstrap\Widget
{
    
    public $widget_item_id;
    public $html_code;

    /**
     * Build widget
     */
    public function init()
    {
        parent::init();
    }

    
    /**
     * Run widget
     */
    public function run()
    {
        $config = yii\helpers\ArrayHelper::merge(
            [], require Yii::getAlias('@common')."/config/i18n.php"
        );

        $aLanguages = [];
        foreach($config["languages"] as $sLang){
            $isCurrent = Yii::$app->session->get('frontend_language') == $sLang;
            $sShort = strtoupper(current(explode("-", $sLang)));
            
            $aLanguages[] = ["full" => $sLang, "short" => $sShort, "isCurrent" => $isCurrent];
        }
        
        return $this->display([
            "aLanguages" => $aLanguages
        ]);
    }
    
}
