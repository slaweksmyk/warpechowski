<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\shop\models\Category;
?>

<div class="widget-main-form" >
    
    <!-- ADD WIDGET CUSTOM DATA -->
    <div class="row" style="background: #f8f8f8; padding-top: 15px; padding-bottom: 15px; " >
        <div class="col-3">
            <?= $form->field($model, 'data[version]')->textInput(["value" => 'v2.9'])->label("Wersja API"); ?>
            <small style="position: relative; top: -10px;">* Podstrona Aplikacji FB</small>
        </div>
        <div class="col-3">
            <?= $form->field($model, 'data[appID]')->textInput()->label("ID Aplikacji (appID)"); ?>
            <small style="position: relative; top: -10px;">* Podstrona Aplikacji FB</small>
        </div>
        <div class="col-3">
            <?= $form->field($model, 'data[appSecret]')->textInput()->label("App Secret"); ?>
            <small style="position: relative; top: -10px;">* Podstrona Aplikacji FB</small>
        </div>
        <div class="col-3">
            <?= $form->field($model, 'data[pageID]')->textInput()->label("ID Fanpage (appID)"); ?>
            <small style="position: relative; top: -10px;">* Generujemy <a href="https://findmyfbid.com/" target="_blank" style="text-decoration: underline;">tutaj</a></small>
        </div>
        <div class="col-3">
            <?= $form->field($model, 'data[fields]')->textInput(["placeholder" => "id,created_time,message,picture,permalink_url,likes,comments,shares"])->label("Uprawnienia (fields)"); ?>
            <small style="position: relative; top: -10px;">* wartość,wartość,wartość,wartość</small>
        </div>
        <div class="col-9">
            <?= $form->field($model, 'data[userAccessToken]')->textInput()->label("Token administratora"); ?>
            <small style="position: relative; top: -10px;">* Generujemy <a href="https://developers.facebook.com/tools/explorer" target="_blank" style="color: black;">tutaj</a></small>
        </div>
    </div>
    <!-- END CUSTOM DATA -->
    
    <br>
    <div class="form-group">
        <?= Html::submitButton( Yii::t('backend', 'update'), ['class' => 'btn btn-primary']) ?>
    </div>

</div>