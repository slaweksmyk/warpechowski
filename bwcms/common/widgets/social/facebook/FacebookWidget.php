<?php
/**
 * Widget Shop
 * 
 * PHP version 7
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @copyright (c) 2016 Krzysztof Borecki
 * @version 1.0
 */
namespace common\widgets\social\facebook;

use Facebook;

class FacebookWidget extends \common\hooks\yii2\bootstrap\Widget
{
    public $widget_item_id;
    public $layout;
    
    public $appID, $version, $appSecret, $pageID, $fields, $userAccessToken;
    protected $oFacebook;

    /**
     * Build widget
     */
    public function init()
    {
        parent::init();
        
        $this->initFacebook();
    }
    
    /**
     * Run widget
     */
    public function run()
    {
        return $this->display([
            'oFacebookPosts' => $this->getPosts()
        ]);
    }
   
    private function initFacebook(){
        $sRaw = file_get_contents("https://graph.facebook.com/oauth/access_token?client_id={$this->appID}&client_secret={$this->appSecret}&grant_type=fb_exchange_token&fb_exchange_token={$this->userAccessToken}");
        $this->oFacebook = new Facebook\Facebook([
            'app_id' => $this->appID,
            'app_secret' => $this->appSecret,
            'default_graph_version' => $this->version,
            'default_access_token' => json_decode($sRaw)->access_token,
        ]);
    }
    
    private function getPosts(){
        try {
            $response = $this->oFacebook->get("/{$this->pageID}/feed?fields={$this->fields}");
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        return $response->getGraphEdge('GraphPage');
    }
    
}
