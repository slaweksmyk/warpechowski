<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\modules\types\models\Type;
use common\modules\menu\models\Menu;

$menuList = Menu::find()->select(['id', 'name'])->all();
$menuDrop = array();
foreach ($menuList as $one_menu) {
    $menuDrop[$one_menu['id']] = $one_menu['name'];
}
?>

<div class="widget-main-form" >

    <!-- ADD WIDGET CUSTOM DATA -->
    <div class="row" style="background: #f8f8f8; padding-top: 15px; padding-bottom: 15px; " >
        <div class="col-12 col-sm-4" >
            <?php echo $form->field($model, 'data[menu_id]')->dropDownList($menuDrop)->label(Yii::t('backend_widget_menu', 'select_menu')); ?>
        </div>
        <div class="col-12 col-sm-4" >
            <?php echo $form->field($model, 'data[show_child]')->dropDownList(array(1 => 'Tak', 0 => 'Nie'))->label(Yii::t('backend_widget_menu', 'show_child')); ?>
        </div>
        <div class="col-12 col-sm-4" >
            <?php echo $form->field($model, 'data[parent]')->dropDownList(array('nav' => 'Nav', 'div' => 'Div', 'section' => 'Section'))->label(Yii::t('backend_widget_menu', 'menu_parent')); ?>
        </div>
        <div class="col-12 col-sm-4" >
            <?php echo $form->field($model, 'data[html_class]')->textInput()->label(Yii::t('backend_widget_menu', 'menu_class')); ?>
        </div>
        <div class="col-12 col-sm-4" >
            <?php echo $form->field($model, 'data[html_id]')->textInput()->label(Yii::t('backend_widget_menu', 'menu_id')); ?>
        </div>
        <div class="col-12 col-sm-4" >
            <?php echo $form->field($model, 'data[html_parent_class]')->textInput()->label(Yii::t('backend_widget_menu', 'parent_class')); ?>
        </div>
        <div class="col-12 col-sm-4" >
            <?php echo $form->field($model, 'data[type_id]')->dropDownList(ArrayHelper::map(Type::find()->all(), 'id', 'name'), ["prompt" => "- wybierz listę artykułową -"])->label(Yii::t('backend_widget_menu', 'type_id'));; ?>
        </div>
         <div class="col-12 col-sm-4" >
            <?php echo $form->field($model, 'data[type_id_second]')->dropDownList(ArrayHelper::map(Type::find()->all(), 'id', 'name'), ["prompt" => "- wybierz listę artykułową -"])->label(Yii::t('backend_widget_menu', 'type_id_second')); ?>
        </div>
    </div>
    <!-- END CUSTOM DATA -->

    <br>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'update'), ['class' => 'btn btn-primary']) ?>
    </div>

</div>