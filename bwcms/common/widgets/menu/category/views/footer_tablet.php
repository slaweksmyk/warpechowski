<div class="row footer-main-link">
    <section class="col-12">
        <h5 class="border-tablet d-flex justify-content-center align-items-center">
            <a href="/" title="Defence24" class="d-flex justify-content-start align-items-start"><img src="/img/logo/defence24/defence24.svg" alt="Defence"></a>
        </h5>
    </section>
    
    <section class="col-12">
        <ul class="footer-tablet-link">
            <?php foreach($menu as $oMenu){ ?>
                <li><a href="<?= $oMenu->url; ?>" title="<?= $oMenu->name; ?>"><?= $oMenu->name; ?></a></li>
            <?php } ?>
        </ul>
    </section>
</div>