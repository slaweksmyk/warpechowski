<?php

use common\modules\articles\models\Article;
use common\modules\files\models\File;
use common\modules\lists\models\ArticlesList;

$formatter = \Yii::$app->formatter;
Yii::$app->formatter->locale = 'pl-PL';
?>
<nav>
    <div class="container d-flex align-items-center flex-wrap nav-container">
        <ul class="nav nav-list mr-auto">
            <?php foreach ($menu as $oMenu) { ?>
                <li <?php if ($oMenu->url == $curent_url) { ?>class="active"<?php } ?>> 
                    <a href="<?= $oMenu->url; ?>" title="<?= $oMenu->name; ?>" class="dropdown-toggle" data-action="open-head-submenu" data-id="<?= $oMenu->id; ?>" data-list-important="<?= $oMenu->list_id ?>" data-list-news="<?= $oMenu->list_id_second ?>"><?= $oMenu->name; ?></a>
                </li>
            <?php } ?>
        </ul>
        <?php
        foreach ($menu as $oMenu) {
            if (isset($oMenu->children) && count($oMenu->children) > 0) {
                $childMenu = true;
            } else {
                $childMenu = false;
            }
            ?>
            <div class="head-submenu" data-item="head-submenu" data-id="<?= $oMenu->id; ?>">
                <div class="row">
                    <?php if ($childMenu) { ?>
                        <div class="col-sm-3">
                            <h4><span><?= $oMenu->name ?></span><span class="line"></span></h4>
                            <ul class="head-submenu-categories" data-item="head-submenu-categories" data-action="change-submenu-categories">
                                <?php foreach ($oMenu->children as $oChildren) { ?>
                                    <li><a href="<?= $oChildren->url ?>" title="<?= $oChildren->name ?>" data-id="<?= $oChildren->id ?>" data-widget="<?= $oMenu->id ?>" data-list-important="<?= $oChildren->list_id ?>" data-list-news="<?= $oChildren->list_id_second ?>"><?= $oChildren->name ?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    <?php } ?>
                    <div class="<?= $childMenu ? 'col-sm-5' : 'col-sm-6'; ?>">
                        <h4><span>Ważne</span><span class="line"></span></h4>
                        <ul class="head-submenu-important" data-item="head-submenu-important" data-id="<?= $oMenu->id ?>" data-id="<?= $oMenu->id; ?>" data-list-important="<?= $oMenu->list_id ?>" data-list-news="<?= $oMenu->list_id_second ?>"></ul>
                        <div data-item="clone-item" data-id="<?= $oMenu->id ?>" style="display:none"></div>
                    </div>
                    <div class="<?= $childMenu ? 'col-sm-4' : 'col-sm-6'; ?>">
                        <h4><span>Wiadomości</span><span class="line"></span></h4>
                        <ul class="head-submenu-news" data-item="head-submenu-news" data-id="<?= $oMenu->id ?>"></ul>
                        <div data-item="clone-item-news" data-id="<?= $oMenu->id ?>" style="display:none"></div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="nav-search s-pc">
            <form action="/wyszukiwanie/">
                <div class="input-group">
                    <input type="text" name="search" class="form-control" placeholder="Wyszukaj artykuł..." aria-label="Search for...">
                    <span class="input-group-btn">
                        <button class="btn btn-secondary" type="submit"><i class="icon icon-search"></i></button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</nav>