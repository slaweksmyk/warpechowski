<div class="row footer-main-link">
    <section class="col">
        <h5><a href="/" title="Defence24" class="d-flex justify-content-start align-items-start"><img src="/img/logo/defence24/defence24.svg" alt="Defence"></a></h5>
    </section>
    <?php foreach($menu as $oMenu){ ?>
        <section class="col">
            <h5><a href="<?= $oMenu->url; ?>" title="<?= $oMenu->name; ?>"><?= $oMenu->name; ?></a></h5>
            <?php if(isset($oMenu->children) && count($oMenu->children) > 0){ ?>
                <ul>
                    <?php foreach($oMenu->children as $oChild){ ?>
                        <li><a href="<?= $oChild->url ?>" title="<?= $oChild->name ?>"><?= $oChild->name ?></a></li>
                    <?php } ?>
                </ul>
            <?php } ?>
        </section>
    <?php } ?>
</div>