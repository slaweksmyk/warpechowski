<?php
namespace common\widgets\menu\category;

use Yii;
use yii\helpers\Html;
use common\modules\menu\models\Menu;
use common\modules\pages\models\PagesSite;

class SiteMenuCategoryWidget extends \common\hooks\yii2\bootstrap\Widget
{
    
    public $widget_item_id;
    public $parent;
    public $html_class;
    public $html_parent_class;
    public $html_id;
    public $show_child;
    public $layout;
    public $menu_id;
    public $type_id;
    public $type_id_second;

    /**
     * Build Menu widget
     */
    public function init()
    {
        parent::init();
    }
    
    
    /**
     * Run widget
     * 
     * @return menu HTML
     */
    public function run()
    {
        $menuModel = new Menu();
        $menu = $menuModel->find()->select(['rowset'])->where([ 'id' => $this->menu_id ])->one();
        $aMenuRowset = json_decode( $menu['rowset'] );
 
        $curent_url = Yii::$app->request->url;

        if( $menu ){
            return $this->display([
                'widget' => $this,
                'menu' => $aMenuRowset,
                'curent_url' => $curent_url,
                'html_class' => $this->html_class
            ]);
        } else {
            return '<h3>'.Html::encode('Menu not found').'</h3>';
        }
    }
  
}