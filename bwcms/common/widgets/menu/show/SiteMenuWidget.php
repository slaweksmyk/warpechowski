<?php
/**
 * Widget MENU (Link list)
 * 
 * PHP version 7
 *
 * @author Volodymyr Shelelo <contact@profficode.com>
 * @copyright (c) 2016 Volodymyr Shelelo
 * @version 1.0
 */

namespace common\widgets\menu\show;

use Yii;
use common\modules\menu\models\Menu;
use common\modules\pages\models\PagesSite;

class SiteMenuWidget extends \common\hooks\yii2\bootstrap\Widget
{
    
    public $widget_item_id;
    public $layout;
    public $menu_id;

    private $oConfig;
    
    /**
     * Build Menu widget
     */
    public function init()
    {
        parent::init();
    }
    
    
    /**
     * Run widget
     * 
     * @return menu HTML
     */
    public function run()
    {
        $this->oConfig = Yii::$app->params['oConfig'];
        
        $menuModel = new Menu();
        $menu = $menuModel->find()->select(['rowset'])->where([ 'id' => $this->menu_id ])->one();
        $aMenuRowset = json_decode( $menu['rowset'] );

        $aMenu = $this->generateMenu($aMenuRowset);
        $curent_url = Yii::$app->request->url;

        return $this->display([
            'aMenu' => $aMenu,
            'oPage' => Yii::$app->params['oPage'],
            'curent_url' => $curent_url,
        ]);
    }
    
    private function generateMenu($aMenuRowset){
        $sLangRaw = Yii::$app->session->get('frontend_language');
        $sLang = strtolower(current(explode("-", $sLangRaw)));
        
        $aMenu = [];
        foreach($aMenuRowset as $index => $aData){
            if($this->oConfig->default_language != Yii::$app->session->get('frontend_language')){
                $sUrl  = "/{$sLang}/";
            } else { $sUrl = "/"; }
            
            $oPage = PagesSite::findOne($aData->id);

            if(!$oPage->is_home){ $sUrl .= "{$oPage->slug}/"; }
            
            $aMenu[$index] = ["oPage" => $oPage, "sUrl" => $sUrl];
            $aMenu[$index]["children"] = $this->generateChild(isset($aData->children) ? $aData->children : null, $sUrl, $aMenu[$index], $index);
        }
        
        return $aMenu;
    }
    
    private function generateChild($aChildrens, $sUrl, $index){
        $aChildMenu = [];
        if($aChildrens){
            foreach($aChildrens as $cIndex => $aChild){
                $oChild = PagesSite::findOne($aChild->id);

                $sNewUrl = "{$sUrl}{$oChild->slug}/";

                $aChildMenu[$cIndex] = ["oPage" => $oChild, "sUrl" => $sNewUrl];
                $aChildMenu[$cIndex]["children"] = $this->generateChild(isset($aChild->children) ? $aChild->children : null, isset($aChild->children) ? $sNewUrl : $sUrl, $index);
            }
        }
        return $aChildMenu;
    }
  
}