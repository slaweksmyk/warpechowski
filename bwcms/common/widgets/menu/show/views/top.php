
<div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
        <?php foreach($aMenu as $aPage){ ?>
        <?php if(count($aPage["children"]) == 0){ ?>
        <li class="nav-item <?php if ($curent_url) { ?>active<?php } ?>">
            <a class="nav-link" href="<?= $aPage["sUrl"] ?>" title="<?= $aPage["oPage"]->name ?>"><?= $aPage["oPage"]->name ?></a>
        </li>
        <?php } else { ?>
        <li class="nav-item dropdown active">
          <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" title="<?= $aPage["oPage"]->name ?>"><?= $aPage["oPage"]->name ?> <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <?php foreach($aPage["children"] as $aChild){ ?>
            <li><a class="dropdown-menu-item" href="<?= $aChild["sUrl"] ?>" title="<?= $aChild["oPage"]->name ?>"><?= $aChild["oPage"]->name ?></a></li>
            <?php } ?>
          </ul>
        </li>
        <?php } ?>
        <?php } ?>
    </ul>
</div>