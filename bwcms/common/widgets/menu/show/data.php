<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use common\modules\menu\models\Menu;

$menuList = Menu::find()->select(['id', 'name'])->all();
$menuDrop = array();
foreach($menuList as $one_menu){
    $menuDrop[ $one_menu['id'] ] = $one_menu['name'];
}
?>

<div class="widget-main-form" >

    <!-- ADD WIDGET CUSTOM DATA -->
    <div class="row" style="background: #f8f8f8; padding-top: 15px; padding-bottom: 15px; " >
        <div class="col-12 col-sm-4" >
            <?php echo $form->field($model, 'data[menu_id]')->dropDownList( $menuDrop )->label("Menu"); ?>
        </div>
    </div>
    <!-- END CUSTOM DATA -->
    
    <br>
    <div class="form-group">
        <?= Html::submitButton( Yii::t('backend', 'update'), ['class' => 'btn btn-primary']) ?>
    </div>

</div>