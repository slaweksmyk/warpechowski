<?php
    use yii\widgets\ActiveForm;
    use common\modules\shop\models\PriceList;
?>

<section>
    <div class="container section-header">
        <div class="row">
            <h1 data-head="Twój profil">Twój profil</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent venenatis velit ultrices commodo sodales. Duis nec nunc sapien. Aenean interdum leo nec ultrices ultricies. Etiam rutrum velit sit amet.</p>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="form form-page">
                <div class="col-12 col-sm-12 col-md-6 noPadding">
                    <?php $form = ActiveForm::begin();  ?>
                        <div class="register-form">
                            <h3>Twoje Dane</h3>
                            <div class="col-6 col-sm-6 col-md-6">
                                <?= $form->field($oUserDataCompany, "firstname")->textInput(["id" => "firstname"]) ?>
                            </div>
                            <div class="col-6 col-sm-6 col-md-6">
                                <?= $form->field($oUserDataCompany, "surname")->textInput(["id" => "lastname"]) ?>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12">
                                <?= $form->field($oUserDataCompany, "email")->textInput(["id" => "email"]) ?>
                            </div>
                            <div class="col-6 col-sm-6 col-md-6">
                                <?= $form->field($oUserDataCompany, "phone")->textInput(["id" => "phone"]) ?>
                            </div>
                            <div class="col-6 col-sm-6 col-md-6">
                                <?= $form->field($oUserDataCompany, "mobile")->textInput(["id" => "phone2"]) ?>
                            </div>
                            <div class="col-12 col-sm-6 col-md-12">
                                <?= $form->field($oUserDataCompany, "street")->textInput(["id" => "ulica"]) ?>
                            </div>
                            <div class="col-6 col-sm-6 col-md-6">
                                <?= $form->field($oUserDataCompany, "city")->textInput(["id" => "miejscowosc"]) ?>
                            </div>
                            <div class="col-6 col-sm-6 col-md-6">
                                <?= $form->field($oUserDataCompany, "postcode")->textInput(["id" => "kod"]) ?>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12">
                                <?= $form->field($oUserDataCompany, "country")->textInput(["id" => "kraj"]) ?>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12">
                                <label for="nip">Nip</label>
                            </div>
                            <div class="col-2 col-sm-2 col-md-2">
                                <?php 
                                    $aNip = []; 
                                    foreach(PriceList::find()->all() as $oPl){
                                        $aNip[$oPl->short_name] = $oPl->short_name;
                                    }
                                ?>

                                <?= $form->field($oUserDataCompany, 'nip_sulf')->textInput()->dropDownList($aNip)->label(false); ?>
                            </div>
                            <div class="col-10 col-sm-10 col-md-10">
                                <?= $form->field($oUserDataCompany, 'nip')->textInput()->label(false) ?>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 noPadding send-button-row">
                                <div class="send-button dane-do-wysylki-btn" type="text">Dane do wysyłki</div>
                            </div>
                        </div>
                        <div class="register-form dane-do-wysylki">
                            <h3>Dane do wysyłki</h3>
                            <div class="col-12 noPadding">
                                <label class="control control-checkbox copy">
                                    <input type="checkbox" name="copy">Takie same jak dane do faktury
                                    <span class="control_indicator"></span>
                                </label>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12">
                                <?= $form->field($oUserDataCompany, "deliver_email")->textInput(["id" => "wysylka_email"]) ?>
                            </div>
                            <div class="col-6 col-sm-6 col-md-6">
                                <?= $form->field($oUserDataCompany, "deliver_phone")->textInput(["id" => "wysylka_phone"]) ?>
                            </div>
                            <div class="col-6 col-sm-6 col-md-6">
                                <?= $form->field($oUserDataCompany, "deliver_mobile")->textInput(["id" => "wysylka_phone2"]) ?>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12">
                                <?= $form->field($oUserDataCompany, "deliver_street")->textInput(["id" => "wysylka_ulica"]) ?>
                            </div>
                            <div class="col-6 col-sm-6 col-md-6">
                                <?= $form->field($oUserDataCompany, "deliver_city")->textInput(["id" => "wysylka_miejscowosc"]) ?>
                            </div>
                            <div class="col-6 col-sm-6 col-md-6">
                                <?= $form->field($oUserDataCompany, "deliver_postcode")->textInput(["id" => "wysylka_kod"]) ?>
                            </div>
                        </div>
                        <?= $form->field($oUserDataCompany, "nip_id")->hiddenInput()->label(false) ?>
                        <?= $form->field($oUserDataCompany, "regon_id")->hiddenInput()->label(false) ?>
                        <?= $form->field($oUserDataCompany, "krs_id")->hiddenInput()->label(false) ?>
                        <div class="change-password">
                            <h3>Zmień hasło</h3>
                            <div class="login-form">
                                <div class="col-12 noPadding">
                                    <?= $form->field($oUser, "oldPassword")->passwordInput(["placeholder" => "Stare hasło"])->label(false) ?>
                                </div>
                                <div class="col-12 noPadding">
                                    <?= $form->field($oUser, "newPassword")->passwordInput(["placeholder" => "Nowe hasło"])->label(false) ?>
                                </div>
                                <div class="col-12 noPadding">
                                    <?= $form->field($oUser, "new2Password")->passwordInput(["placeholder" => "Powtórz hasło"])->label(false) ?>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-6 noPadding send-button-row">
                                <a href="/profil/"><div class="send-button send-button-gray">Odrzuć zmiany</div></a>
                            </div>
                            <div class="col-12 col-sm-6 col-md-6 noPadding send-button-row">
                                <input class="send-button" type="submit" value="Akceptuje zmiany">
                            </div>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <div class="col-12 col-sm-12 col-md-6 additional-options noPadding">
                    <h3>Opcje dodatkowe</h3>
                    <div class="col-12 col-sm-6 col-md-6">
                        <p>Twój opiekun<br />
                            <strong><?= $oUserDataCompany->getPatron()->one()->getDataCompany()->one()->firstname; ?> <?= $oUserDataCompany->getPatron()->one()->getDataCompany()->one()->surname; ?></strong>
                        </p>
                        <a href="/kontakt/" class="send-button send-button-gray">Poproś o zmianę</a>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6">
                        <p>
                            <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent venenatis veli.</small>
                        </p>
                        <a data-toggle="modal" data-target="#myModal" href="#link" class="send-button send-button-gray">Negocjacja rabatów</a>

                        <div id="myModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-body">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <div class="section-header">
                                    <div class="row">
                                        <h1 data-head="Negocjacje rabatów">Negocjacje rabatów</h1>
                                        <p>Jeśli w najbliższym czasie planujesz wykonać na naszym serwisie większe zakupy, poinformuj nas o tym i sprawdź jakie przysługują Ci promocje.</p>
                                    </div>
                                </div>
                                <form method="POST" id="negotiations">
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <textarea placeholder="Treść wiadomości" name="Rabat[text]" id="text"></textarea>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12">
                                        <a href="#" onClick="$('#negotiations').submit(); return false;" class="send-button send-order floatRight">Wyślij</a>
                                    </div>
                                </form>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 documents">
                        <h3>Dokumenty rejestracyjne</h3>
                        <div class="col-12 col-sm-12 col-md-12 documents-item">
                            <div class="col-3 col-sm-3 col-md-3 noPadding">CEIDG/KRS</div>
                            <div class="col-9 col-sm-9 col-md-9 noPadding">
                                <?= $form->field($oUserDataCompany, 'krs_file', ["template" => "{input}", "options" => ["tag" => false]])->fileInput(["id" => "file", "class" => "inputfile"])->label(false) ?>
                                <label data-text="Wybierz plik" class="filenamelabel" for="file">- nazwa pliku -</label>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 documents-item">
                            <div class="col-3 col-sm-3 col-md-3 noPadding">NIP</div>
                            <div class="col-9 col-sm-9 col-md-9 noPadding">
                                <?= $form->field($oUserDataCompany, 'nip_file', ["template" => "{input}", "options" => ["tag" => false]])->fileInput(["id" => "file2", "class" => "inputfile"])->label(false) ?>
                                <label data-text="Wybierz plik" class="filenamelabel" for="file2">- nazwa pliku -</label>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 documents-item">
                            <div class="col-3 col-sm-3 col-md-3 noPadding">REGON</div>
                            <div class="col-9 col-sm-9 col-md-9 noPadding">
                                <?= $form->field($oUserDataCompany, 'regon_file', ["template" => "{input}", "options" => ["tag" => false]])->fileInput(["id" => "file3", "class" => "inputfile"])->label(false) ?>
                                <label data-text="Wybierz plik" class="filenamelabel" for="file3">- nazwa pliku -</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 orders-history">
                        <h3>Historia Twoich zamówień</h3>
                        <div class="panel-group" id="accordion">
                            <?php foreach($oOrdersClientRowset as $oOrderClient){ ?>
                                <?php $oOrder = $oOrderClient->getOrder()->one(); ?>
                                <?php $oOrderItems = $oOrder->getShopOrdersItems()->all(); ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $oOrder->id ?>"><?= date("d.m.Y", strtotime($oOrder->order_date)) ?></a>
                                            <span>koszt zamówienia: <span class="order-price"><?= $oOrder->getFullPrice() ?> zł</span></span>
                                        </h4>
                                    </div>
                                    <div id="collapse<?= $oOrder->id ?>" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <?php foreach($oOrderItems as $oItem){ ?>
                                                <div class="col-12 col-sm-12 col-md-12 noPadding">
                                                    <div class="col-6 col-sm-6 col-md-6 order-name"><?= $oItem->getProduct()->one()->name ?></div>
                                                    <div class="col-3 col-sm-3 col-md-3 order-amount">ilosć: <?= $oItem->amount ?></div>
                                                    <div class="col-3 col-sm-3 col-md-3 order-price"><?= $oItem->price_brutto ?> zł</div>
                                                </div> 
                                            <?php } ?>
                                            <div class="col-12 col-sm-12 col-md-12 noPadding">
                                                <div class="col-12 col-sm-6 col-md-6 center noFloat noPadding">
                                                    <a href="/koszyk/?remakeOrder=<?= $oOrder->id ?>" class="send-button">Ponów zamówienie</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>