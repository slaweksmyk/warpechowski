<?php
/**
 * Widget HTML
 * 
 * PHP version 7
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @copyright (c) 2016 Krzysztof Borecki
 * @version 1.0
 */

namespace common\widgets\user\profile;

use Yii;
use common\modules\users\models\UserDataCompany;
use common\modules\shop\models\ShopOrdersClient;

class ProfileWidget extends \common\hooks\yii2\bootstrap\Widget
{
    
    public $widget_item_id;
    public $html_code;
    
    private $slug;

    /**
     * Build widget
     */
    public function init()
    {
        parent::init();
        
        if(array_key_exists("slug", Yii::$app->params)){
            $this->slug = Yii::$app->params['slug'];
        }
    }

    
    /**
     * Run widget
     */
    public function run()
    {
        if(Yii::$app->request->get('logout') == "true"){
            Yii::$app->session->set('oUser', null);
            Yii::$app->session->set('is_logged', null);
            Yii::$app->getResponse()->redirect("/logowanie/")->send();
            return;
        }
        
        $oUser = Yii::$app->session->get('oUser', null);
        if(is_null($oUser)){ return; }

        $oUserDataCompany = UserDataCompany::find()->where(["=", "user_id", $oUser->id])->one();
        $oOrdersClientRowset = ShopOrdersClient::find()->where(["=", "user_id", $oUser->id])->all();

        if(Yii::$app->request->isPost && $oUserDataCompany->load(Yii::$app->request->post())) {
            $oUserDataCompany->save();
        }
        
        if(Yii::$app->request->isPost && $oUser->load(Yii::$app->request->post())) {
            if(Yii::$app->request->post('User')["newPassword"] != ""){
                $oUser->setPassword(Yii::$app->request->post('User')["newPassword"]);
                $oUser->generateAuthKey();
                $oUser->save();
            }
        }
        
        if(Yii::$app->request->isPost){
            if(Yii::$app->request->post('Rabat')["text"] != ""){
                $sBody = "";
                $sBody .= "Klient: {$oUserDataCompany->firstname} {$oUserDataCompany->surname} (ID: $oUser->id)<br/>";
                $sBody .= "Treść: <br/>".Yii::$app->request->post('Rabat')["text"];
                
                Yii::$app->mailer->compose()
                    ->setTo($oUserDataCompany->getPatron()->one()->getDataCompany()->one()->email)
                    ->setSubject('Negocjacja rabatu')
                    ->setHtmlBody($sBody)
                    ->send();
            }
        }
        
        return $this->display([
            'html_code' => $this->html_code,
            'oUser' => $oUser,
            'oOrdersClientRowset' => $oOrdersClientRowset,
            'oUserDataCompany' => $oUserDataCompany
        ]);
    }
    
}
