<?php
use yii\widgets\ActiveForm;
?>
<section style="padding-bottom: 50px;">
    <div class="container section-header">
        <div class="row">
            <h1 data-head="Logowanie">Logowanie</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent venenatis velit ultrices commodo sodales. Duis nec nunc sapien. Aenean interdum leo nec ultrices ultricies. Etiam rutrum velit sit amet.</p>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="form form-page">
                <?php $form = ActiveForm::begin();  ?>
                    <div class="login-form">
                        <div class="col-12 noPadding">
                            <?= $form->field($model, "username")->textInput(["placeholder" => "Login"])->label(false) ?>
                        </div>
                        <div class="col-12 noPadding">
                            <?= $form->field($model, "password")->passwordInput(["placeholder" => "Hasło"])->label(false) ?>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 noPadding send-button-row">
                            <input class="send-button" type="submit" value="Zaloguj się">
                        </div>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>