<?php
/**
 * Widget HTML
 * 
 * PHP version 7
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @copyright (c) 2016 Krzysztof Borecki
 * @version 1.0
 */

namespace common\widgets\user\login;

use Yii;
use common\widgets\user\login\models\LoginForm;

class LoginWidget extends \common\hooks\yii2\bootstrap\Widget
{
    
    public $widget_item_id;
    public $html_code;
    public $layout;

    /**
     * Build widget
     */
    public function init()
    {
        parent::init();
    }

    
    /**
     * Run widget
     */
    public function run()
    {
        $model = new LoginForm();
        
        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            if($model->login()){
                Yii::$app->getResponse()->redirect("/profil/")->send();
                return;
            }
        }
        
        return $this->display([
            'model' => $model,
            'html_code' => $this->html_code
        ]);
    }
    
}
