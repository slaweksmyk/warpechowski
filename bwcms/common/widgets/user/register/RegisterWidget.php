<?php
/**
 * Widget HTML
 * 
 * PHP version 7
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @copyright (c) 2016 Krzysztof Borecki
 * @version 1.0
 */

namespace common\widgets\user\register;

use Yii;
use yii\web\UploadedFile;
use common\modules\files\models\File;
use common\modules\users\models\User;
use common\modules\users\models\UserDataCompany;
use common\modules\promotion\models\PromotionConfig;

use common\modules\loyalty\models\LoyaltyReferral;
use common\modules\loyalty\models\LoyaltyReferralConfig;

use common\modules\shop\models\ShopDiscountCodes;

class RegisterWidget extends \common\hooks\yii2\bootstrap\Widget
{
    
    public $widget_item_id;
    public $html_code;
    public $layout;

    /**
     * Build widget
     */
    public function init()
    {
        parent::init();
    }

    
    /**
     * Run widget
     */
    public function run()
    {
        $model = new UserDataCompany();

        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            $user = new User();
            $user->username = $model->email;
            $user->email = $model->email;
            $user->role = "user";
            $user->setPassword(Yii::$app->request->post('User')["password"]);
            $user->generateAuthKey();
            $user->save();
            
            $user->created_at = date('Y-m-d H:i:s');
            $user->save();
            
            $auth = \Yii::$app->authManager;
            $user_role = $auth->getRole("user");
            $user_id = $user->getId();
            $auth->assign($user_role, $user_id);
            
            $model->user_id = $user->id;

            $model->krs_file = UploadedFile::getInstance($model, 'krs_file');
            if($model->krs_file){
                $model->krs_id = $this->upload($model->krs_file);
            }

            $model->nip_file = UploadedFile::getInstance($model, 'nip_file');
            if($model->nip_file){
                $model->nip_id = $this->upload($model->nip_file);
            }
            
            $model->regon_file = UploadedFile::getInstance($model, 'regon_file');
            if($model->regon_file){
                $model->regon_id = $this->upload($model->regon_file);
            }
            
            $model->save();
            
            $this->loyaltyCheck($model);
            $this->promoCheck($model);

            Yii::$app->response->redirect("/register-ready/");
        }
        
        return $this->display([
            'html_code' => $this->html_code,
            'model' => $model
        ]);
    }
    
    private function promoCheck($model){
        $oPromoConfig = PromotionConfig::find()->where(["=", "id", 1])->one();
        if($oPromoConfig->register_title){
            $oCode = $oPromoConfig->getRegisterCode()->one()->code;
            if(is_null($oCode)){
                $oCode = ShopDiscountCodes::find()->where(["=", "promotion_id", 4])->andWhere(["=", "promotion_subtype", "register_code"])->andWhere(["=", "is_used", 0])->one();
            
                if(is_null($oCode)){
                    return;
                }
            }
   
            Yii::$app->mailer->compose()
                ->setTo($model->email)
                ->setSubject($oPromoConfig->register_title)
                ->setHtmlBody(str_replace("{CODE}", $oCode->code, $oPromoConfig->register_content))
                ->send();
        }
    }
    
    private function loyaltyCheck($model){
        $oLoyaltyReferralConfig = LoyaltyReferralConfig::findOne(1);
        if($oLoyaltyReferralConfig->is_enabled && Yii::$app->request->get('ref')){
            $oLoyaltyReferral = new LoyaltyReferral();
            $oLoyaltyReferral->user_id = Yii::$app->request->get('ref');
            $oLoyaltyReferral->ref_user_id = $model->id;
            $oLoyaltyReferral->save();
        }
    }
    
    public function upload($oFileData)
    {
        $hash = base_convert(time(), 10, 36);
        
        $file_name = preg_replace("/[^a-zA-Z0-9.]/", "", $oFileData->baseName);
        $save_name = $hash."_" . $file_name . '.' . $oFileData->extension;
        
        $oFileData->saveAs(Yii::getAlias('@root/upload/') . $save_name);
        
        $info = pathinfo($oFileData->name);

        $oFile = new File();
        $oFile->category_id = 3;
        $oFile->name = $info['filename'];
        $oFile->filename = $save_name;
        $oFile->type = $oFileData->type;
        $oFile->size = $oFileData->size;
        $oFile->thumbnail = Yii::$app->ImageHelper->createBaseThumbnail(null, $save_name);
        $oFile->save(); 
        
        return $oFile->id;
    }
    
}
