<?php
    use yii\widgets\ActiveForm;
    use common\modules\shop\models\PriceList;
?>
<section>
   <div class="container section-header">
       <div class="row">
           <h1 data-head="Załóż konto">Załóż konto</h1>
           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent venenatis velit ultrices commodo sodales. Duis nec nunc sapien. Aenean interdum leo nec ultrices ultricies. Etiam rutrum velit sit amet.</p>
       </div>
   </div>
   <div class="container">
       <div class="row">
           <div class="form form-page register">
               <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);  ?>
                   <div class="login-form">
                   <div class="col-12 noPadding">
                       <input placeholder="Hasło" type="password" name="User[password]" value="" />
                   </div>
                   <div class="col-12 noPadding">
                       <input placeholder="Powtórz hasło" type="password" name="User[password2]" value="" />
                   </div>
                   </div>
                   <div class="col-12 col-sm-12 col-md-6 register-form">
                       <h3>Twoje Dane</h3>
                        <div class="col-6 col-sm-6 col-md-6">
                            <?= $form->field($model, 'firstname')->textInput() ?>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <?= $form->field($model, 'surname')->textInput() ?>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12">
                            <?= $form->field($model, 'email')->textInput(["id" => "email"]) ?>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <?= $form->field($model, 'mobile')->textInput(["id" => "phone"]) ?>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <?= $form->field($model, 'phone')->textInput(["id" => "phone2"]) ?>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <?= $form->field($model, 'street[]')->textInput(["id" => "ulica"]) ?>
                        </div>
                        <div class="col-3 col-sm-3 col-md-3">
                            <?= $form->field($model, 'street[]')->textInput(["id" => "nr_domu"])->label("Nr. domu") ?>
                        </div>
                        <div class="col-3 col-sm-3 col-md-3">
                            <?= $form->field($model, 'street[]')->textInput(["id" => "nr_lokalu"])->label("Nr. lokalu") ?>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <?= $form->field($model, 'city')->textInput(["id" => "miejscowosc"]) ?>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <?= $form->field($model, 'postcode')->textInput(["id" => "kod"]) ?>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12">
                            <?= $form->field($model, 'country')->textInput() ?>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12">
                            <label for="nip">Nip</label>
                        </div>
                        <div class="col-2 col-sm-2 col-md-2">
                            <?php 
                                $aNip = []; 
                                foreach(PriceList::find()->all() as $oPl){
                                    $aNip[$oPl->short_name] = $oPl->short_name;
                                }
                            ?>
                            
                            <?= $form->field($model, 'nip_sulf')->textInput()->dropDownList($aNip)->label(false); ?>
                        </div>
                        <div class="col-10 col-sm-10 col-md-10">
                            <?= $form->field($model, 'nip')->textInput()->label(false) ?>
                        </div>
                   </div>
                   <div class="col-12 col-sm-12 col-md-6 register-form">
                       <h3>Dane do wysyłki</h3>
                       <div class="col-12 noPadding">
                           <label class="control control-checkbox copy">
                               <input type="checkbox" name="copy">Takie same jak dane do faktury
                               <span class="control_indicator"></span>
                           </label>
                       </div>
                        <div class="col-12 col-sm-12 col-md-12">
                            <?= $form->field($model, 'deliver_email')->textInput(["id" => "wysylka_email"]) ?>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <?= $form->field($model, 'deliver_mobile')->textInput(["id" => "wysylka_phone"]) ?>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <?= $form->field($model, 'deliver_phone')->textInput(["id" => "wysylka_phone2"]) ?>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <?= $form->field($model, 'deliver_street[]')->textInput(["id" => "wysylka_ulica"]) ?>
                        </div>
                        <div class="col-3 col-sm-3 col-md-3">
                            <?= $form->field($model, 'deliver_street[]')->textInput(["id" => "wysylka_nr_domu"])->label("Nr. domu") ?>
                        </div>
                        <div class="col-3 col-sm-3 col-md-3">
                            <?= $form->field($model, 'deliver_street[]')->textInput(["id" => "wysylka_nr_lokalu"])->label("Nr. lokalu") ?>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <?= $form->field($model, 'deliver_city')->textInput(["id" => "wysylka_miejscowosc"]) ?>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <?= $form->field($model, 'deliver_postcode')->textInput(["id" => "wysylka_kod"]) ?>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 documents">
                            <h3>Dokumenty rejestracyjne</h3>
                            <div class="col-12 col-sm-12 col-md-12 documents-item">
                                <div class="col-3 col-sm-3 col-md-3 noPadding">CEIDG/KRS</div>
                                <div class="col-9 col-sm-9 col-md-9 noPadding">
                                    <?= $form->field($model, 'krs_file', ["template" => "{input}", "options" => ["tag" => false]])->fileInput(["id" => "file", "class" => "inputfile"])->label(false) ?>
                                    <label data-text="Wybierz plik" class="filenamelabel" for="file">- nazwa pliku -</label>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 documents-item">
                                <div class="col-3 col-sm-3 col-md-3 noPadding">NIP</div>
                                <div class="col-9 col-sm-9 col-md-9 noPadding">
                                    <?= $form->field($model, 'nip_file', ["template" => "{input}", "options" => ["tag" => false]])->fileInput(["id" => "file2", "class" => "inputfile"])->label(false) ?>
                                    <label data-text="Wybierz plik" class="filenamelabel" for="file2">- nazwa pliku -</label>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 documents-item">
                                <div class="col-3 col-sm-3 col-md-3 noPadding">REGON</div>
                                <div class="col-9 col-sm-9 col-md-9 noPadding">
                                    <?= $form->field($model, 'regon_file', ["template" => "{input}", "options" => ["tag" => false]])->fileInput(["id" => "file3", "class" => "inputfile"])->label(false) ?>
                                    <label data-text="Wybierz plik" class="filenamelabel" for="file3">- nazwa pliku -</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 noPadding send-button-row">
                        <input class="send-button" type="submit" value="Zarejestruj">
                    </div>
                <?php ActiveForm::end(); ?>
           </div>
       </div>
   </div>
</section>