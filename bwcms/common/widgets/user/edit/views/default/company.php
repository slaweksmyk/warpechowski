<?php
    use yii\widgets\ActiveForm;
    use common\helpers\TranslateHelper;
    
    $form = ActiveForm::begin(["id" => "edit-form"]); 
?>

<section id="informations" class="list acc-pager basic_info">
    <div class="container-fluid">
        <div class="row edit-row">
            <div class="col-2"></div>
            <div class="col-8">
                <div class="contact-txt">
                    <h2><?= TranslateHelper::t('EDIT_PROFILE_TITLE') ?></h2>
                    <div class='rectangle' data-bottom=" width:50px;" data-center=" width:74px;"></div>
                </div>
                <h3 class='green'><?= TranslateHelper::t('EDIT_ORG_DATA') ?></h3>
                <div class="col-12 col-lg-6 no-padding">
                    <div class="col-12 ">
                        <div class="input-group input-group-form">
                            <?= $form->field($oUserDataCompany, "name")->textInput(["class" => "form-control", "placeholder" => TranslateHelper::t('EDIT_PH_ORG')])->label(false) ?>
                        </div>
                    </div>
                    <div class="col-12 ">
                        <div class="input-group input-group-form">
                            <input type="text" name="mail" id="pac-inpu" class="form-control" value="<?= $oUserDataCompany->street ?>, <?= $oUserDataCompany->district ?>, <?= $oUserDataCompany->city ?>" placeholder="<?= TranslateHelper::t('EDIT_PH_ADRESS') ?>">
                            <?= $form->field($oUserDataCompany, "city")->hiddenInput()->label(false) ?>
                            <?= $form->field($oUserDataCompany, "district")->hiddenInput()->label(false) ?>
                            <?= $form->field($oUserDataCompany, "street")->hiddenInput()->label(false) ?>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="input-group input-group-form">
                            <?= $form->field($oUserDataCompany, "phone")->textInput(["class" => "form-control", "placeholder" => TranslateHelper::t('EDIT_PH_PHONE')])->label(false) ?>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="input-group input-group-form">
                            <?= $form->field($oUserDataCompany, "nip")->textInput(["class" => "form-control", "placeholder" => TranslateHelper::t('EDIT_PH_NIP')])->label(false) ?>
                        </div>
                    </div>
                    <div class="col-12">
                        <h3 class='green'><?= TranslateHelper::t('EDIT_PH_DEF_EMAIL') ?></h3>
                    </div> 
                    <div class="col-12">
                        <div class="input-group input-group-form">
                            <?= $form->field($oUserDataCompany, "email")->textInput(["class" => "form-control", "placeholder" => TranslateHelper::t('EDIT_PH_EMAIL')])->label(false) ?>
                        </div>
                    </div>
                    <div class="col-12">
                        <h3 class='green'><?= TranslateHelper::t('EDIT_PH_ADD_EMAILS') ?></h3>
                    </div> 
                    <div class="col-12 ">
                        <div class="input-group input-group-form">
                            <?= $form->field($oUserDataCompany, "add_email[]")->textInput(["style" => "position:relative;z-index:0", "class" => "form-control width-80", "placeholder" => TranslateHelper::t('EDIT_PH_EMAIL')])->label(false) ?>
                            <input style="position:relative; margin-left: 3px;" value="<?= TranslateHelper::t('EDIT_PH_ADD') ?>" type="submit" class="reg-email-add" style="margin-left:5px">
                        </div>
                        <?php foreach(explode(",", $oUserDataCompany->add_email) as $sEmail){ ?>
                            <div class="input-group input-group-form">
                                <?= $form->field($oUserDataCompany, "add_email[]")->textInput(["class" => "form-control", "placeholder" => TranslateHelper::t('EDIT_PH_EMAIL'), "value" => $sEmail])->label(false) ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>   
                <div class="col-12 col-lg-6 no-padding">
                    <div class="col-12 ">
                        <div class="input-group input-group-form">
                            <?= $form->field($oUserDataCompany, "hours")->textInput(["class" => "form-control", "placeholder" => TranslateHelper::t('EDIT_PH_HOURS')])->label(false) ?>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="input-group input-group-form">
                            <?= $form->field($oUserDataCompany, "website")->textInput(["class" => "form-control", "placeholder" => TranslateHelper::t('EDIT_PH_WEBSITE')])->label(false) ?>
                        </div>
                    </div>
                    <div class="col-12">
                        <h3 class='green'><?= TranslateHelper::t('EDIT_PH_SOCIALS') ?></h3>
                    </div> 

                    <div class="adding-form">
                        <div class="col-3 col-lg-3 text-right">
                            <p>Facebook</p>
                        </div>
                        <div class="col-9">
                            <div class="input-group input-group-form">
                                <?= $form->field($oUserDataCompany, "social_fb")->textInput(["class" => "form-control", "placeholder" => TranslateHelper::t('EDIT_PH_SOCIAL')])->label(false) ?>
                            </div>
                        </div>
                        <div class="col-3 col-lg-3 text-right">
                            <p>Instagram</p>
                        </div>
                        <div class="col-9">
                            <div class="input-group input-group-form">
                                <?= $form->field($oUserDataCompany, "social_in")->textInput(["class" => "form-control", "placeholder" => TranslateHelper::t('EDIT_PH_SOCIAL')])->label(false) ?>
                            </div>
                        </div>
                        <div class="col-3 col-lg-3 text-right">
                            <p>YouTube</p>
                        </div>
                        <div class="col-9">
                            <div class="input-group input-group-form">
                                <?= $form->field($oUserDataCompany, "social_yt")->textInput(["class" => "form-control", "placeholder" => TranslateHelper::t('EDIT_PH_SOCIAL')])->label(false) ?>
                            </div>
                        </div>
                        <div class="col-3 col-lg-3 text-right">
                            <p>Twitter</p>
                        </div>
                        <div class="col-9 ">
                            <div class="input-group input-group-form">
                                <?= $form->field($oUserDataCompany, "social_tw")->textInput(["class" => "form-control", "placeholder" => TranslateHelper::t('EDIT_PH_SOCIAL')])->label(false) ?>
                            </div>
                        </div>
                    </div>
                </div>  
                <div class="col-12">
                    <h3 class='green'><?= TranslateHelper::t('EDIT_PERSON_DATA') ?></h3>
                </div> 
                <div class="col-12 col-lg-6 no-padding">
                    <div class="col-12 ">
                        <div class="input-group input-group-form">
                            <?= $form->field($oUserData, "firstname")->textInput(["class" => "form-control", "placeholder" => TranslateHelper::t('EDIT_NAME')])->label(false) ?>
                        </div>
                    </div>
                    <div class="col-12 ">
                        <div class="input-group input-group-form">
                            <?= $form->field($oUserData, "surname")->textInput(["class" => "form-control", "placeholder" => TranslateHelper::t('EDIT_SURNAME')])->label(false) ?>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="input-group input-group-form">
                            <?= $form->field($oUserData, "position")->textInput(["class" => "form-control", "placeholder" => TranslateHelper::t('EDIT_POSITION')])->label(false) ?>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6 no-padding">
                    <div class="col-12 ">
                        <div class="input-group input-group-form">
                            <?= $form->field($oUser, "email")->textInput(["class" => "form-control", "placeholder" => TranslateHelper::t('EDIT_EMAIL')])->label(false) ?>
                        </div>
                    </div>
                    <div class="col-12 ">
                        <div class="input-group input-group-form">
                            <?= $form->field($oUserData, "phone")->textInput(["class" => "form-control", "placeholder" => TranslateHelper::t('EDIT_PHONE')])->label(false) ?>
                        </div>
                    </div>
                </div>
                <div class="col-12 text-right">
                    <div class="basic_reg-continue" style="text-align: right;margin-top:75px;">
                        <a class="btn-yellow button-list account-next_1" onclick="$('#edit-form').submit()">
                            <?= TranslateHelper::t('EDIT_SUBMIT') ?>
                            <span>&#9658;</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php ActiveForm::end(); ?>