<?php
    use yii\widgets\ActiveForm;
    use common\helpers\TranslateHelper; 
    
    $form = ActiveForm::begin(["id" => "edit-form"]); 
?>

<section id="informations" class="list acc-pager basic_info">
    <div class="container-fluid">
        <div class="row edit-row">
            <div class="col-2"></div>
            <div class="col-8">
                <div class="contact-txt">
                    <h2><?= TranslateHelper::t('EDIT_PROFILE_TITLE') ?></h2>
                    <div class='rectangle' data-bottom=" width:50px;" data-center=" width:74px;"></div>
                </div>
                <div class="col-12">
                    <h3 class='green'><?= TranslateHelper::t('EDIT_PERSON_DATA_SINGLE') ?></h3>
                </div> 
                <div class="col-12 col-lg-6 no-padding">
                    <div class="col-12 ">
                        <div class="input-group input-group-form">
                            <?= $form->field($oUserData, "firstname")->textInput(["class" => "form-control", "placeholder" => TranslateHelper::t('EDIT_NAME')])->label(false) ?>
                        </div>
                    </div>
                    <div class="col-12 ">
                        <div class="input-group input-group-form">
                            <?= $form->field($oUserData, "surname")->textInput(["class" => "form-control", "placeholder" => TranslateHelper::t('EDIT_SURNAME')])->label(false) ?>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="input-group input-group-form">
                            <?= $form->field($oUserData, "position")->textInput(["class" => "form-control", "placeholder" => TranslateHelper::t('EDIT_POSITION')])->label(false) ?>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6 no-padding">
                    <div class="col-12 ">
                        <div class="input-group input-group-form">
                            <?= $form->field($oUser, "email")->textInput(["class" => "form-control", "placeholder" => TranslateHelper::t('EDIT_EMAIL')])->label(false) ?>
                        </div>
                    </div>
                    <div class="col-12 ">
                        <div class="input-group input-group-form">
                            <?= $form->field($oUserData, "phone")->textInput(["class" => "form-control", "placeholder" => TranslateHelper::t('EDIT_PHONE')])->label(false) ?>
                        </div>
                    </div>
                </div>
                <div class="col-12 text-right">
                    <div class="basic_reg-continue" style="text-align: right;margin-top:75px;">
                        <a class="btn-yellow button-list account-next_1" onclick="$('#edit-form').submit()">
                            <?= TranslateHelper::t('EDIT_SUBMIT') ?>
                            <span>&#9658;</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php ActiveForm::end(); ?>