<?php
/**
 * Widget HTML
 * 
 * PHP version 7
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @copyright (c) 2016 Krzysztof Borecki
 * @version 1.0
 */

namespace common\widgets\user\edit;

use Yii;

class EditWidget extends \common\hooks\yii2\bootstrap\Widget
{
    
    public $widget_item_id;
    public $html_code;
    public $layout;

    /**
     * Build widget
     */
    public function init()
    {
        parent::init();
    }

    
    /**
     * Run widget
     */
    public function run()
    {
        $mode = "private";
        
        $oUser = Yii::$app->session->get('oUser');
        $oUserData = $oUser->getData()->one();
        $oUserDataCompany = $oUser->getDataCompany()->one();
        
        if(is_null($oUser)){
            Yii::$app->getResponse()->redirect("/konto/zaloguj-sie/")->send();
            Yii::$app->end();
            exit;
        }
        
        if(Yii::$app->request->isPost){
            $oUser->load(Yii::$app->request->post());
            $oUser->save();

            $oUserData->load(Yii::$app->request->post());
            $oUserData->save();
        }

        if(!is_null($oUserDataCompany)){
            $mode = "company";
            
            if(Yii::$app->request->isPost){
                $oUserDataCompany->load(Yii::$app->request->post());
                $oUserDataCompany->add_email = implode(",", array_filter($oUserDataCompany->add_email));
                $oUserDataCompany->save();
            }
        }
        
        return $this->display([
            'oUser' => $oUser,
            'oUserData' => $oUserData,
            'oUserDataCompany' => $oUserDataCompany,
            'html_code' => $this->html_code
        ], $mode);
       
    }
    
}
