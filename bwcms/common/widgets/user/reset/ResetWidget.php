<?php
/**
 * Widget HTML
 * 
 * PHP version 7
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @copyright (c) 2016 Krzysztof Borecki
 * @version 1.0
 */

namespace common\widgets\user\reset;

use Yii;
use common\widgets\user\reset\models\ResetForm;

class ResetWidget extends \common\hooks\yii2\bootstrap\Widget
{
    
    public $widget_item_id;
    public $html_code;

    /**
     * Build widget
     */
    public function init()
    {
        parent::init();
    }

    
    /**
     * Run widget
     */
    public function run()
    {
        $isSubmit = false;
        $oModel = new ResetForm();
        
        if(Yii::$app->request->isPost && $oModel->load(Yii::$app->request->post())) {
            if($oModel->validate()) {
                $oModel->sendNewPassword();
                $isSubmit = true;
            }
        }

        return $this->display([
            'isSubmit' => $isSubmit,
            'model' => $oModel,
            'html_code' => $this->html_code
        ]);
    }
    
}
