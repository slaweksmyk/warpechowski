<?php
    
namespace common\widgets\user\reset\models;
   
use Yii;
use yii\base\Security;
use yii\db\ActiveRecord;
use common\modules\users\models\User;

class ResetForm extends ActiveRecord implements \yii\web\IdentityInterface
{
    public $email;
    
    public static function tableName()
    {
        return 'user';
    }

    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\common\modules\users\models\User',
                'message' => 'Nie ma użytkownika o takim adresie e-mail.'
            ],
        ];
    }

    public function getAuthKey() {
        return $this->auth_key;
    }

    public function getId() {
        return $this->id;
    }

    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    public static function findIdentity($id) {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        return static::findOne(['access_token' => $token]);
    }
    
    public function sendNewPassword(){
        $oSecurity = new Security();
        $sPassword = $oSecurity->generateRandomString(8);
        
        $oUser = User::findOne(['email' => $this->email]);
        $oUser->setPassword($sPassword);
        $oUser->generateAuthKey();
        $oUser->save();

        return Yii::$app
            ->mailer
            ->compose()
            ->setTo($this->email)
            ->setFrom('from@domain.com')
            ->setSubject("Reset hasła")
            ->setHtmlBody("Dane do Twojego konta: <br/><br/>Login: {$oUser->username}<br/>Hasło: {$sPassword}")
            ->send();
        
    }
    

}