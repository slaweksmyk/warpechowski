<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<section class="reminder">
    <div class="container" style="padding: 50px 0px;">
        <div class="row">
            <div class="col-3"></div>
            <div class="col-6">
                <h2>Nie pamiętasz hasła?</h2>
                <h3>Prosimy podać adres email, na który zostanie wysłane wygenerowane nowe hasło.</h3>
                <?php $form = ActiveForm::begin(); ?>
                    <?= $form->field($model, "email")->textInput(["placeholder" => "Adres email"])->label(false) ?>
                    <?php if($isSubmit){ echo "<div style='text-align: center; padding-bottom: 5px;'>Nowe hasło zostało wysłane.</div>"; } ?>
                    <?= Html::submitButton('Wyślij') ?>
                <?php ActiveForm::end(); ?>
            </div>
            <div class="col-3"></div>
        </div>
    </div>
</section>