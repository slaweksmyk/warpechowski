<?php
/**
 * Widget MENU (Link list)
 * 
 * PHP version 7
 *
 * @author Volodymyr Shelelo <contact@profficode.com>
 * @copyright (c) 2016 Volodymyr Shelelo
 * @version 1.0
 */

namespace common\widgets\forms\contact;

use Yii;
use common\widgets\forms\contact\models\ContactDefault;
use common\modules\forms\models\ModuleForms;
use common\modules\articles\models\Article;
use common\helpers\TranslateHelper;

class ContactFormWidget extends \common\hooks\yii2\bootstrap\Widget
{
    
    public $widget_item_id;
    public $emails;
    public $success;
    public $error;
    public $confirm;
    public $confirm_title;
    public $confirm_text;
    public $layout;
    public $model;
    public $article_id;
    
    public $recaptcha_public_key;
    public $recaptcha_private_key;
    public $recaptcha_theme;
    public $recaptcha_size;
    public $recaptcha_badge;
    
    /**
     * Build form widget
     */
    public function init()
    {
        parent::init();
    }
    
    
    /**
     * Run widget
     */
    public function run()
    {
        if ( $this->layout == 'default' ) {
            $this->model = new ContactDefault();
        }
        
        if( $this->model ){ 
            $oArticle = Article::findOne($this->article_id);
            if ( $this->model->load( Yii::$app->request->post()) && $this->model->validate(null, null, $this->recaptcha_private_key) ) {
                self::sendEmail($this);
                return $this->display([
                    'oArticle' => $oArticle,
                ], "send");
            } else {
                return $this->display([
                    'oArticle' => $oArticle,
                    'model' => $this->model,
                    'widget_item_id' => $this->widget_item_id,
                    'reCaptcha' => self::getRecaptchaCode($this)
                ], "form");
            }
        }
    }
    
    
    /**
     * Send e-mails action
     * 
     * @return controller refresh
     */
    protected static function sendEmail($widget, $img = null)
    {
        Yii::$app->session->setFlash('send_by_id', $widget->widget_item_id);
        $lang = Yii::$app->language;
        $sendOptions = array(
            'emails'        => $widget->emails,
            'confirm'       => $widget->confirm,
            'confirm_title' => $widget->confirm_title,
            'confirm_text'  => $widget->confirm_text,
        );
        
        if ( $widget->model->sendEmail($sendOptions) ) {
            self::saveToDatabase($widget);
        }
    }
    
    
    /**
     * Save Form data to database
     */
    protected static function saveToDatabase($widget)
    {
        $moduleDB = new ModuleForms();
        
        $moduleDB->widget_id = $widget->widget_item_id;
        if( !empty($widget->emails) ){
            $moduleDB->recipients = $widget->emails;
        } else {
            $moduleDB->recipients = Yii::$app->params['adminEmail'];
        }
        
        $data = [
            'name'      => $widget->model->name,
            'email'     => $widget->model->email,
            'phone'   => $widget->model->phone,
            'body'      => $widget->model->body
        ];
        
        $moduleDB->data = serialize($data);
        $moduleDB->save();
    }


    // -------------------------------------------------------------------------
    
    /**
     * Get language field in the string 
     * 
     * @param string $str
     * @param string $lang
     * @return string
     */
    protected static function getStringLangByRegex($str, $lang)
    {
        $matches = array();
        
        $regex = "/\[:{$lang}:]((.|\n)*)\[:\/{$lang}:]/";
        preg_match_all($regex, $str, $matches);
        
        if( !empty($matches[1]) ){ return $matches[1][0]; } 
        else{ return $str; }
    }
    
    /**
     * Generate reCaptcha code
     * 
     * @return string
     */
    protected static function getRecaptchaCode($widget){
        $is_error = false;
        $sReturn = "";
        
        if(!empty($widget->model->getErrors())){
            $aErrors = $widget->model->getErrors();
            
            if(!empty($aErrors["g-recaptcha-response"])){
                $is_error = true;
            }
        }

        $sReturn .= "<script src='https://www.google.com/recaptcha/api.js' async defer></script>";
        $sReturn .= "<button class='g-recaptcha btn' data-badge='{$widget->recaptcha_badge}' data-sitekey='{$widget->recaptcha_public_key}' data-callback='contactSubmit' id='send'>".TranslateHelper::t('FM_SEND')."</button>";
	$sReturn .= "<script>function contactSubmit(token) { document.getElementById('form1').submit(); }</script>";	
		
		
		

        if($is_error){
            $sReturn .= '<p class="help-block help-block-error" style="color: #a94442;">'.$aErrors["g-recaptcha-response"][0].'</p>';
        }
        
        return $sReturn;
    }

    
}