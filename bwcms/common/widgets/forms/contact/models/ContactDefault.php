<?php

namespace common\widgets\forms\contact\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactDefault extends Model
{
    public $widget_item_id;

    public $name;
    public $email;
    public $accept;
    public $phone;
    public $body;
    public $article_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required', 'message' => 'Imię i nazwisko jest wymagane'],
            [['email'], 'required', 'message' => 'Adres e-mail jest wymagany'],
            [['body'], 'required', 'message' => 'Treść wiadomości jest wymagana'],
            [['phone'], 'required', 'message' => 'Telefon jest wymagany'],
            [['widget_item_id'], 'required', 'message' => 'To pole jest wymagane'],
            [['email'], 'email'],
            [['subject'], 'string', 'min' => 4, 'max' => 150],
            [['body'], 'string', 'min' => 5, 'max' => 1000],
            ['accept', 'required', 'requiredValue' => 1, 'message' => 'Musisz zaakceptować zgodę'],
            
            [['name', 'email', 'phone', 'body'], 'filter', 'filter' => function($value) {
                return trim( htmlentities(strip_tags($value), ENT_QUOTES, 'UTF-8') );
            }]
        ];
    }
    
    public function validate($attributeNames = null, $clearErrors = true, $reCaptchaKey = null)
    {
        if ($clearErrors) {
            $this->clearErrors();
        }

        if (!$this->beforeValidate()) {
            return false;
        }

        $scenarios = $this->scenarios();
        $scenario = $this->getScenario();
        if (!isset($scenarios[$scenario])) {
            throw new InvalidParamException("Unknown scenario: $scenario");
        }

        if ($attributeNames === null) {
            $attributeNames = $this->activeAttributes();
        }

        foreach ($this->getActiveValidators() as $validator) {
            $validator->validateAttributes($this, $attributeNames);
        }
        
        $this->validateRecaptcha($reCaptchaKey);
        
        $this->afterValidate();

        return !$this->hasErrors();
    }
    
    /**
     * Validate recaptcha response
     * 
     * @param string $reCaptchaKey
     * @return boolean whether the validate is successful
     */
    private function validateRecaptcha($reCaptchaKey){
        $request = Yii::$app->request;
        return;
        
        if(!$request->post('g-recaptcha-response')){
            $this->addError('g-recaptcha-response', "Formularz wymaga weryfikacji");
            return;
        }

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify?secret={$reCaptchaKey}&response={$request->post('g-recaptcha-response')}&remoteip=".$_SERVER['REMOTE_ADDR']);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
        $curlData = curl_exec($curl);
        curl_close($curl);	

        $res = json_decode($curlData, true);
        
        if(!$res['success']){
            $this->addError('g-recaptcha-response', "Nieprawidłowa weryfikacja");
        }
        
        return;
    }
    
    /**
     * Sends an email to the specified email address using the information collected by this model.
     * 
     * @param array $options
     * @return boolean whether the email was sent
     */
    public function sendEmail($options)
    {
        if( !isset($options['emails']) || empty($options['emails']) ) {
            $sendTo = Yii::$app->params['adminEmail'];
            $send = self::sendToAdmins( $sendTo );
        } else {
            $sendToArray = preg_split("/\r\n|\n|\r/", $options['emails']); $send = false;
            foreach ($sendToArray as $value) {
                if( self::sendToAdmins( $value ) ){
                    $send = true;
                }
            }
        }
        
        if( $send && $options['confirm'] && $this->email ){
            self::sendToUser( $options );
        }
        
        return $send;  
    }
    
    
    /**
     * Send mail to administrators
     * 
     * @param string $sendTo
     * @return boolean
     */
    protected function sendToAdmins($sendTo)
    {
        $body  = $this->name;
        $body .= $this->email.'<br />';
        $body .= $this->phone.'<br />';
        $body .= $this->body;
        
        $body = html_entity_decode($body);
        
        return Yii::$app->mailer->compose()
                ->setTo($sendTo)
                ->setSubject('Kontakt ze strony Warpechowski ')
                ->setTextBody($body)
                ->send();
    }
    
    
    /**
     * Send email to user
     * 
     * @param array $options
     * @return boolean
     */
    protected function sendToUser( $options ) 
    {
        if( !empty($options['confirm_title']) ){
            $mailSubject = str_replace('[[USERNAME]]', $this->name, $options['confirm_title']);
        } else {
            $mailSubject = "Dziękujemy za kontakt";
        }
        
        if( !empty($options['confirm_text']) ){
            $mailBody = str_replace('[[USERNAME]]', $this->name, $options['confirm_text']);
        } else {
            $mailBody = "Dziękujemy za kontakt na naszej stronie.";
        }
        
        return Yii::$app->mailer->compose()
                ->setTo($this->email)
                ->setSubject($mailSubject)
                ->setTextBody($mailBody)
                ->send();
    }
    
    
}
