<?php
    use yii\bootstrap\ActiveForm;
    use common\helpers\TranslateHelper;
?>

<section class="contact">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="content">
                    <h1><?= $oArticle->title ?></h1>

                    <div class="row">
                        <?php $form = ActiveForm::begin(['id' => 'form1', 'action' => '#send', 'options' => ['class' => 'form', 'enctype' => 'multipart/form-data']]); ?>
                            <?= $form->field($model, 'widget_item_id', ['options' => ['class' => '']])->hiddenInput(['value' => $widget_item_id])->label(false); ?>
                            <div class="row">
                                <div class="col-12 col-md-5">
                                    <?= $form->field($model, "name")->textInput(["label" => false, 'class' => 'form-control', "placeholder" => '*'.TranslateHelper::t('FM_NAME')], false)->label(false) ?>
                                    <?= $form->field($model, "email")->textInput(["label" => false, 'class' => 'form-control', "placeholder" => '*'.TranslateHelper::t('FM_EMAIL')], false)->label(false) ?>
                                    <?= $form->field($model, "phone")->textInput(["label" => false, 'class' => 'form-control', "placeholder" => '*'.TranslateHelper::t('FM_PHONE')], false)->label(false) ?>
                                    
                                </div>
                                <div class="col-12 col-md-7">
                                    <?= $form->field($model, "body")->textarea(["label" => false, 'class' => '', "placeholder" => '*'.TranslateHelper::t('FM_BODY')], false)->label(false) ?>
                                </div>
                                <div class="col-12 custom-control custom-checkbox">
                                    <div class='form-group twocheck field-contactdefault-accept required'>
                                        <?= $form->field($model, "accept", ["template" => "{input}<label for='contactdefault-accept' class='custom-control-label' >".TranslateHelper::t('FM_ACCEPT')."</label>{error}", "options" => ["tag" => false]])->checkbox(["label" => false, 'class' => 'custom-control-input'], false)->label(false) ?>
                                    </div>
                                    
                                </div>
                                <div class="col-12 text-center">
                                    <p class="required">*<?= TranslateHelper::t('REQUIRE_FIELDS') ?></p>
                                    <?= $reCaptcha ?>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <div class="row text-center realizatons">
                        <?= $oArticle->short_description ?>
                        <div>
                             <?= $oArticle->full_description ?>
                        </div>
                    </div>

                </div> 
            </div>
        </div>

    </div>
    <div class="mouse">
        <svg width="30" height="60">
            <path class="mouse_path" d="M 2 14 Q 2 2 14 2 Q 25 2 25 14 L 25 22 Q 25 34 14 34 Q 2 34 2 22 Z" stroke-linecap="round" />
            <path class="line" d="M 13 10 L15 10 L 15 16 L 13 16" />
            <path class="arrow_1 arrow" d="M 10 39 L18 39 L14 44 Z" />
            <path class="arrow_2 arrow" d="M 10 47 L18 47 L14 52 Z" />
            <path class="arrow_3 arrow" d="M 10 55 L18 55 L14 60 Z" />
        </svg>
    </div>
</section>