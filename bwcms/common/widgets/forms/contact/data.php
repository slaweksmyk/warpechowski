<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\articles\models\Article;

$oArticle = Article::findOne($model->data["article_id"] ?? null);
?>

<div class="widget-main-form" >
    
    <!-- ADD WIDGET CUSTOM DATA -->
    <div class="row" style="background: #f8f8f8; padding-top: 15px; padding-bottom: 15px; " >
        <div class="col-12 col-sm-6" >
            <?php echo $form->field($model, 'data[emails]')->textarea(['rows' => 8, 'class' => 'form-control noTynyMCE noresize'])->label( Yii::t('backend_widget_forms', 'r_emails') ); ?>
        </div>
        <div class="col-12 col-sm-6" >
            <?php echo $form->field($model, 'data[confirm]')->dropDownList( array( 0 => 'Nie', 1 => 'Tak' ) )->label( Yii::t('backend_widget_forms', 'send_confirm') ); ?>
            <?php echo $form->field($model, 'data[confirm_title]')->textInput()->label( Yii::t('backend_widget_forms', 'confirm_title') ); ?>
            <?php echo $form->field($model, 'data[confirm_text]')->textarea(['rows' => 5, 'class' => 'form-control noTynyMCE noresize'])->label( Yii::t('backend_widget_forms', 'confirm_text') ); ?>
            <p><small style="font-size: 0.85em; opacity: 0.7;" >[[USERNAME]] - <?php echo Yii::t('backend_widget_forms', 'user_name'); ?></small></p>
        </div>
        <div class="col-12" >
            
            <div class="row" >
                <div class="col-12 text-center" >
                    <hr>
                    <p><b><?php echo Yii::t('backend_widget_forms', 'info'); ?></b></p>
                    <hr>
                </div>
                <div class="col-12 col-sm-6" >
                    <?php echo $form->field($model, 'data[success]')->textarea(['rows' => 6, 'class' => 'form-control noTynyMCE noresize'])->label( Yii::t('backend_widget_forms', 'success') ); ?>
                </div>
                <div class="col-12 col-sm-6" >
                    <?php echo $form->field($model, 'data[error]')->textarea(['rows' => 6, 'class' => 'form-control noTynyMCE noresize'])->label( Yii::t('backend_widget_forms', 'error') ); ?>
                </div>
                <div class="col-12" >
                    <p>
                        <b><?php echo Yii::t('backend_widget_forms', 'example'); ?></b><br>
                        <small style="font-size: 0.85em; opacity: 0.7;" >
                            [:pl-PL:]Wystąpił błąd podczas wysyłania wiadomości e-mail.[:/pl-PL:]
                            <br>
                            [:en-US:]There was an error sending email.[:/en-US:]
                        </small>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-12" >
            
            <div class="row" >
                <div class="col-12 text-center" >
                    <hr>
                    <p><b>Zabezpieczenie reCaptcha</b></p>
                    <hr>
                </div>
                
                <div class="col-6">
                    <?php echo $form->field($model, 'data[recaptcha_public_key]')->textInput()->label("Klucz publiczny"); ?>
                </div>
                
                <div class="col-6">
                    <?php echo $form->field($model, 'data[recaptcha_private_key]')->textInput()->label("Klucz prywatny"); ?>
                </div>
                
                <div class="col-4">
                    <?php echo $form->field($model, 'data[recaptcha_badge]')->dropDownList(["bottomright" => "Dolny, prawy róg ekranu", "bottomleft" => "Dolny, lewy róg ekranu", "inline" => "W formularzu"])->label("Położenie informacji"); ?>
                </div>
                
                <div class="col-3">
                    <?php echo $form->field($model, 'data[recaptcha_theme]')->dropDownList( array( 'light' => 'Jasny', 'dark' => 'Czarny' ) )->label("Motyw"); ?>
                </div>
                
                <div class="col-3">
                    <?php echo $form->field($model, 'data[recaptcha_size]')->dropDownList( array( 'normal' => 'Normalny', 'compact' => 'Kompaktowy' ) )->label("Rozmiar"); ?>
                </div>
                
            </div>
        </div>
        <div class="col-12" >
            
            <div class="row" >
                <div class="col-12 text-center" >
                    <hr>
                    <p><b>Treści</b></p>
                    <hr>
                </div>
                
                <div class="col-6">
                    <div class="form-group">
                        <label class="control-label" for="related">Artykuł</label>
                        <?= $form->field($model, 'data[article_id]')->dropDownList(isset($model->data["article_id"]) ? [$model->data["article_id"] => $oArticle->title] : [], ["class" => "ajax-load-articles-single"])->label(false); ?>
                    </div>
                </div>
                
            </div>
        </div>

        
    </div>
    <!-- END CUSTOM DATA -->
    
    <br>
    <div class="form-group">
        <?= Html::submitButton( Yii::t('backend', 'update'), ['class' => 'btn btn-primary']) ?>
    </div>

</div>