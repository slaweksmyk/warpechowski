<?php

use yii\widgets\ListView;
?>

<?=

ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => 'item/item',
    'layout' => "{items}",
    'options' => [
        'tag' => false,
    ],
    'itemOptions' => [
        'tag' => false,
    ],
]);
?>
 