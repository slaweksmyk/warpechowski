<?php

/**
 * Quotes Api Widget
 *
 * @author Tomasz Załucki <z4lucki@gmail.com>
 * @copyright (c) 2017 Tomasz Załucki
 * @version 1.0
 */

namespace common\widgets\quotes\all;

use Yii;
use common\modules\quotes\models\QuotesSearch;

class SearchWidget extends \common\hooks\yii2\bootstrap\Widget {

    public $widget_item_id;
    public $limit;
    public $layout;

    /**
     * Build widget
     */
    public function init() {
        $session = Yii::$app->session;
        if ($session->get('preview_mode')) {
            $this->preview = $session->get('preview_mode');
        }

        if (array_key_exists("slug", Yii::$app->params)) {
            $this->slug = Yii::$app->params['slug'];
        }

        parent::init();
    }

    /**
     * Run widget
     */
    public function run() {
       
        $searchModel = new QuotesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        

        return $this->render($this->layout, [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

}
