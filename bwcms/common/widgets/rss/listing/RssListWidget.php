<?php
/**
 * Widget Rss
 * 
 * PHP version 7
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @copyright (c) 2016 Krzysztof Borecki
 * @version 1.0
 */

namespace common\widgets\rss\listing;

use common\modules\publishing\models\RssProvide;

class RssListWidget extends \common\hooks\yii2\bootstrap\Widget
{
    
    public $widget_item_id;

    /**
     * Build widget
     */
    public function init()
    {
        parent::init();
    }

    
    /**
     * Run widget
     */
    public function run()
    {
        $oRssRowset = RssProvide::find()->all();
        
        return $this->display([
            'oRssRowset' => $oRssRowset
        ]);
    }
    
}
