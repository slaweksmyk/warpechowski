<section id="other-article" data-item="other-article" class="section block block-title block-subtitle other-article ">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2><span>Kanały RSS</span><span class="line"></span></h2>
                <div class="other-article-content mt-30">
                    <ul class="list dotted column no-line mt-30">
                        <?php foreach($oRssRowset as $oRSS){ ?>
                            <li><a href="/feed/?category=<?= $oRSS->article_category_id ?>&type=<?= $oRSS->article_type_id ?>&limit=<?= $oRSS->amount ?>" title="<?= $oRSS->name ?>"><?= $oRSS->name ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>