<section class="blogi">
    <div class="container">
        <div class="title-main col-12">
            <span class="title-main-span" ><h2><?= $title; ?></h2></span>	
        </div>
    </div>
    <div class="container">
        <div class="blogi-list row row2">
            <?php foreach($aRss as $rssID => $oRssData){ ?>
                <!--single-->
                <a class="transition_0_5" href="<?= $oRssData["items"][0]->link; ?>">
                    <div class="col-12 col-sm-6 col-md-4 blogi-single col2">
                        <div class="blogi-single-img col-12 no-padding">
                            <img class="img-responsive greyscale" src="/upload/<?= $oRssData["channel"]->getPhoto()->filename; ?>" alt="<?= $oRssData["channel"]->name; ?>">
                        </div>
                        <div class="blogi-single-text col-12">
                            <div class="blogi-single-text-autor col-12 no-padding">
                                <?= $oRssData["channel"]->name; ?>
                            </div>
                            <div class="blogi-single-text-o-autorze col-12 no-padding"><?= $oRssData["channel"]->description; ?></div>
                        </div>
                        <div class="blogi-single-tresc col-12 no-padding">
                            <div class="col-12">
                                <div class="col-12 blogi-single-tresc-tytul no-padding">
                                    <?= $oRssData["items"][0]->title; ?>
                                </div>
                                <div class="col-12 blogi-single-tresc-2 no-padding">
                                    <?= $oRssData["items"][0]->description; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            <?php } ?>
        </div>
        <div class="blogi-list row">
            <div class="see_more col-12 ">
                <a href="#" class="transition_0_5">Zobacz wszystkie oferty <img src="/image/right_arrow_small.png" alt="arrow"></a>
            </div>
        </div>
    </div>
</section>