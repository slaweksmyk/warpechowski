<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\rss\models\Rss;

?>

<div class="widget-main-form" >

    <!-- ADD WIDGET CUSTOM DATA -->
    <div class="row" style="background: #f8f8f8; padding-top: 15px; padding-bottom: 15px; " >
        <div class="col-4">
            <?php 
                $aRSS["all"] = "- wszystkie -";
                foreach(Rss::find()->all() as $oRss){
                    $aRSS[$oRss->id] = $oRss->url;
                }
                echo $form->field($model, 'data[rss_id]')->dropDownList($aRSS)->label( Yii::t('backend', 'rss_channel'));
            ?>
        </div>
        <div class="col-4" >
            <?php echo $form->field($model, 'data[title]')->textInput()->label(Yii::t('backend', 'rss_title')); ?>
        </div>
        <div class="col-4" >
            <?php echo $form->field($model, 'data[limit]')->textInput()->label(Yii::t('backend', 'rss_limit')); ?>
        </div>
    </div>
    <!-- END CUSTOM DATA -->
    
    <br>
    <div class="form-group">
        <?= Html::submitButton( Yii::t('backend', 'update'), ['class' => 'btn btn-primary']) ?>
    </div>

</div>