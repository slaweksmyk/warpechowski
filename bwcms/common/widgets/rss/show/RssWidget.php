<?php
/**
 * Widget Rss
 * 
 * PHP version 7
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @copyright (c) 2016 Krzysztof Borecki
 * @version 1.0
 */

namespace common\widgets\rss\show;

use common\modules\rss\models\Rss;

class RssWidget extends \common\hooks\yii2\bootstrap\Widget
{
    
    public $widget_item_id;
    public $rss_id;
    public $title;
    public $limit;

    /**
     * Build widget
     */
    public function init()
    {
        parent::init();
    }

    
    /**
     * Run widget
     */
    public function run()
    {
        $oRss = null;
        $items = [];
        $aRss = [];
        if($this->rss_id == "all"){
            $oRssRowset = Rss::find()->limit($this->md_limit)->all();
            
            foreach($oRssRowset as $oRss){
                $items = [];
                $xml = simplexml_load_file($oRss->url);
                
                foreach ($xml->xpath('//item') as $item) {
                    $items[] = $item;
                }

                $aRss[$oRss->id] = [];
                $aRss[$oRss->id]["channel"] = $oRss;
                $aRss[$oRss->id]["items"] = $items;
            }
            
        } else {
            $oRss = Rss::find()->where(['and', "id = {$this->rss_id}"])->one();

            // Check if there is any article
            if(!$oRss){ return; }

            $xml = simplexml_load_file($oRss->url);
            if ($xml === false) {
                die('Error parse Rss');
            }
            foreach ($xml->xpath('//item') as $item) {
                $items[] = $item;
            }
        }
        
        return $this->display([
            'aRss' => $aRss,
            'oRss' => $oRss,
            'aItems' => $items,
            'title' => $this->title
        ]);
    }
    
}
