<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use common\modules\files\models\File;

?>

<div class="widget-main-form" >
    
    <!-- ADD WIDGET CUSTOM DATA -->
    
    <datalist id="images">
        <?php foreach(File::find()->where(['like', 'type', 'image'])->all() as $oFile){ ?>
            <option value="<?= $oFile->id; ?>"><?= $oFile->name; ?>"</option>
        <?php } ?>
    </datalist>
    
    <div class="row" style="background: #f8f8f8; padding-top: 15px; padding-bottom: 15px; " >
        <div class="col-12">
            <div class="row">
                <div class="col-12 col-sm-2"><?= $form->field($model, 'data[thumbnail1]')->textInput(["list" => "images"])->label("Ikona #1") ?></div>
                <div class="col-12 col-sm-5"><?php echo $form->field($model, 'data[link1]')->textInput(['maxlength' => 255])->label("Link #1"); ?></div>
                <div class="col-12 col-sm-5"><?php echo $form->field($model, 'data[text1]')->textInput(['maxlength' => 255])->label("Podpis #1"); ?></div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-2"><?= $form->field($model, 'data[thumbnail2]')->textInput(["list" => "images"])->label("Ikona #2") ?></div>
                <div class="col-12 col-sm-5"><?php echo $form->field($model, 'data[link2]')->textInput(['maxlength' => 255])->label("Link #2"); ?></div>
                <div class="col-12 col-sm-5"><?php echo $form->field($model, 'data[text2]')->textInput(['maxlength' => 255])->label("Podpis #2"); ?></div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-2"><?= $form->field($model, 'data[thumbnail3]')->textInput(["list" => "images"])->label("Ikona #3") ?></div>
                <div class="col-12 col-sm-5"><?php echo $form->field($model, 'data[link3]')->textInput(['maxlength' => 255])->label("Link #3"); ?></div>
                <div class="col-12 col-sm-5"><?php echo $form->field($model, 'data[text3]')->textInput(['maxlength' => 255])->label("Podpis #3"); ?></div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-2"><?= $form->field($model, 'data[thumbnail4]')->textInput(["list" => "images"])->label("Ikona #4") ?></div>
                <div class="col-12 col-sm-5"><?php echo $form->field($model, 'data[link4]')->textInput(['maxlength' => 255])->label("Link #4"); ?></div>
                <div class="col-12 col-sm-5"><?php echo $form->field($model, 'data[text4]')->textInput(['maxlength' => 255])->label("Podpis #4"); ?></div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-2"><?= $form->field($model, 'data[thumbnail5]')->textInput(["list" => "images"])->label("Ikona #5") ?></div>
                <div class="col-12 col-sm-5"><?php echo $form->field($model, 'data[link5]')->textInput(['maxlength' => 255])->label("Link #5"); ?></div>
                <div class="col-12 col-sm-5"><?php echo $form->field($model, 'data[text5]')->textInput(['maxlength' => 255])->label("Podpis #5"); ?></div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-2"><?= $form->field($model, 'data[thumbnail6]')->textInput(["list" => "images"])->label("Ikona #6") ?></div>
                <div class="col-12 col-sm-5"><?php echo $form->field($model, 'data[link6]')->textInput(['maxlength' => 255])->label("Link #6"); ?></div>
                <div class="col-12 col-sm-5"><?php echo $form->field($model, 'data[text6]')->textInput(['maxlength' => 255])->label("Podpis #6"); ?></div>
            </div>
        </div>
        
    </div>
    <!-- END CUSTOM DATA -->
    
    <br>
    <div class="form-group">
        <?= Html::submitButton( Yii::t('backend', 'update'), ['class' => 'btn btn-primary']) ?>
    </div>

</div>