<?php
    use common\modules\files\models\File;
?>
<div class="col-12 blok-ikon">
    <div class="row">
        <?php if($link1 && $text1){ ?>
            <a class="transition_0_5" href="<?= $link1 ?>">
                <div class="col-12 col-sm-6  blok-ikon-single">
                    <?php if(File::findOne(["id" => $thumbnail1])){ ?><div class="col-3 blok-ikon-single-img" style="background-image: url(/upload/<?= File::findOne(["id" => $thumbnail1])->filename; ?>); background-size: contain;"></div><?php } ?>
                    <div class="<?php if(File::findOne(["id" => $thumbnail1])){ ?>col-9<?php } else { ?>col-12<?php } ?> blok-ikon-single-text">
                        <div class="blok-ikon-single-text-1">
                            <div class="blok-ikon-single-text-2">
                                <h2><?= $text1 ?></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        <?php } ?>
        <?php if($link2 && $text2){ ?>
            <a class="transition_0_5" href="<?= $link2 ?>">
                <div class="col-12 col-sm-6  blok-ikon-single">
                    <?php if(File::findOne(["id" => $thumbnail2])){ ?><div class="col-3 blok-ikon-single-img" style="background-image: url(/upload/<?= File::findOne(["id" => $thumbnail2])->filename; ?>); background-size: contain;"></div><?php } ?>
                    <div class="<?php if(File::findOne(["id" => $thumbnail2])){ ?>col-9<?php } else { ?>col-12<?php } ?> blok-ikon-single-text">
                        <div class="blok-ikon-single-text-1">
                            <div class="blok-ikon-single-text-2">
                                <h2><?= $text2 ?></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        <?php } ?>
        <?php if($link3 && $text3){ ?>
            <a class="transition_0_5" href="<?= $link3 ?>">
                <div class="col-12 col-sm-6  blok-ikon-single">
                    <?php if(File::findOne(["id" => $thumbnail3])){ ?><div class="col-3 blok-ikon-single-img" style="background-image: url(/upload/<?= File::findOne(["id" => $thumbnail3])->filename; ?>); background-size: contain;"></div><?php } ?>
                    <div class="<?php if(File::findOne(["id" => $thumbnail3])){ ?>col-9<?php } else { ?>col-12<?php } ?> blok-ikon-single-text">
                        <div class="blok-ikon-single-text-1">
                            <div class="blok-ikon-single-text-2">
                                <h2><?= $text3 ?></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        <?php } ?>
        <?php if($link4 && $text4){ ?>
            <a class="transition_0_5" href="<?= $link4 ?>">
                <div class="col-12 col-sm-6  blok-ikon-single">
                    <?php if(File::findOne(["id" => $thumbnail4])){ ?><div class="col-3 blok-ikon-single-img" style="background-image: url(/upload/<?= File::findOne(["id" => $thumbnail4])->filename; ?>); background-size: contain;"></div><?php } ?>
                    <div class="<?php if(File::findOne(["id" => $thumbnail4])){ ?>col-9<?php } else { ?>col-12<?php } ?> blok-ikon-single-text">
                        <div class="blok-ikon-single-text-1">
                            <div class="blok-ikon-single-text-2">
                                <h2><?= $text4 ?></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        <?php } ?>
        <?php if($link5 && $text5){ ?>
            <a class="transition_0_5" href="<?= $link5 ?>">
                <div class="col-12 col-sm-6  blok-ikon-single">
                    <?php if(File::findOne(["id" => $thumbnail5])){ ?><div class="col-3 blok-ikon-single-img" style="background-image: url(/upload/<?= File::findOne(["id" => $thumbnail5])->filename; ?>); background-size: contain;"></div><?php } ?>
                    <div class="<?php if(File::findOne(["id" => $thumbnail5])){ ?>col-9<?php } else { ?>col-12<?php } ?> blok-ikon-single-text">
                        <div class="blok-ikon-single-text-1">
                            <div class="blok-ikon-single-text-2">
                                <h2><?= $text5 ?></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        <?php } ?>
        <?php if($link6 && $text6){ ?>
            <a class="transition_0_5" href="<?= $link6 ?>">
                <div class="col-12 col-sm-6  blok-ikon-single">
                    <?php if(File::findOne(["id" => $thumbnail6])){ ?><div class="col-3 blok-ikon-single-img" style="background-image: url(/upload/<?= File::findOne(["id" => $thumbnail6])->filename; ?>); background-size: contain;"></div><?php } ?>
                    <div class="<?php if(File::findOne(["id" => $thumbnail6])){ ?>col-9<?php } else { ?>col-12<?php } ?> blok-ikon-single-text">
                        <div class="blok-ikon-single-text-1">
                            <div class="blok-ikon-single-text-2">
                                <h2><?= $text6 ?></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        <?php } ?>
    </div>
</div>