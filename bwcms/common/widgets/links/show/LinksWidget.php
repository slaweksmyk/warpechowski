<?php
/**
 * Widget Article
 * 
 * PHP version 7
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @copyright (c) 2016 Krzysztof Borecki
 * @version 1.0
 */

namespace common\widgets\links\show;

use Yii;

class LinksWidget extends \common\hooks\yii2\bootstrap\Widget
{
    
    public $widget_item_id;
    public $layout;
    
    public $text1;
    public $link1;
    public $thumbnail1;
    
    public $text2;
    public $link2;
    public $thumbnail2;
    
    public $text3;
    public $link3;
    public $thumbnail3;
    
    public $text4;
    public $link4;
    public $thumbnail4;
    
    public $text5;
    public $link5;
    public $thumbnail5;
    
    public $text6;
    public $link6;
    public $thumbnail6;
    
    /**
     * Build Menu widget
     */
    public function init()
    {
        parent::init();
    }
    
    /**
     * Run widget
     * 
     * @return menu HTML
     */
    public function run()
    {
        
        return $this->display([
            'text1' => $this->text1,
            'text2' => $this->text2,
            'text3' => $this->text3,
            'text4' => $this->text4,
            'text5' => $this->text5,
            'text6' => $this->text6,
            
            'link1' => $this->link1,
            'link2' => $this->link2,
            'link3' => $this->link3,
            'link4' => $this->link4,
            'link5' => $this->link5,
            'link6' => $this->link6,
            
            'thumbnail1' => $this->thumbnail1,
            'thumbnail2' => $this->thumbnail2,
            'thumbnail3' => $this->thumbnail3,
            'thumbnail4' => $this->thumbnail4,
            'thumbnail5' => $this->thumbnail5,
            'thumbnail6' => $this->thumbnail6,
        ]);
    }
    
}