<?php
    use common\modules\files\models\File;
?>

<?php foreach($slider->getSliderItems() as $oSlide){?>
    <div class="adv adv-300-300 adv-m-top">
        <span class="adv-info">Reklama</span>
        <a href="<?= $oSlide->extlink ?>" <?php if($oSlide->settings_target_blank){ ?>target="_blank"<?php } ?>>
            <img src="<?= File::getThumbnail($oSlide->getThumbnail()->one()->id, 300, 300) ?>" alt="reklama" />
        </a>
    </div>
<?php } ?>