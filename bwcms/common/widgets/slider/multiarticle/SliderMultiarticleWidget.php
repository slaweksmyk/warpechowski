<?php

/**
 * Slider Multi Article
 * 
 * PHP version 7
 *
 * @author Tomasz Załucki <tomek@fuleo.pl>
 * @copyright (c) 2017 Tomasz Załucki
 * @version 1.0
 */

namespace common\widgets\slider\multiarticle;

use common\modules\slider\models\{
    SliderItems,
    Slider
};

class SliderMultiarticleWidget extends \common\hooks\yii2\bootstrap\Widget {

    public $widget_item_id;
    public $layout;
    public $slider_id;

    /**
     * Build slider widget
     */
    public function init() {
        parent::init();
    }

    /**
     * Run slider
     * 
     * @return slider html
     */
    public function run() {
        $slider = Slider::find()->where(['id' => $this->slider_id])->one();
        $oMainSliderItems = SliderItems::find()->where(["=", "slider_id", $slider->id])->andWhere(["=", "parent_id", 0])->limit($this->md_limit)->all();

        return $this->display([
            'slider' => $slider,
            'oMainSliderItems' => $oMainSliderItems,
            'widget_item_id' => $this->widget_item_id
        ]);
    }

}
