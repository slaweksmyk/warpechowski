<?php

use common\modules\articles\models\Article; 
use common\modules\slider\models\{
    SliderItems,
    Slider
};
use \common\helpers\TextHelper;

$slider = Slider::find()->where(['id' => $slider->id])->one();
$oMainSliderItems = SliderItems::find()->where(["=", "slider_id", $slider->id])->andWhere(["=", "parent_id", 0])->all();
$oSubSliderItems = SliderItems::find()->where(["=", "slider_id", $slider->id])->andWhere(["!=", "parent_id", 0])->orderBy("sort")->all();
$oSubSliderFirstItem = SliderItems::find()->where(["=", "slider_id", $slider->id])->andWhere(["!=", "parent_id", 0])->orderBy("sort")->one();
?>
<section id="main-slider-live" data-item="main-slider-live" class="block block-title block-subtitle  block-subtitle  block-skaner">
    <div class="container">
        <div class="block-slider">
            <h2 class="mr-auto">Defence24 TV live<span class="line"></span></h2>
            <div class="mobile-slider">
                <div class="block-card-shadow">
                    <?php if ($oSubSliderFirstItem) { ?>
                        <?php $oArticle = Article::find()->where(["=", "id", $oSubSliderFirstItem->data_id])->one(); ?>
                        <?php if ($oArticle) { ?>
                            <?php if ($oArticle->getVideo()) { ?>
                                <?= $oArticle->getVideo()->generateEmbed("238px", "100%"); ?>
                            <?php } ?> 
                        <?php } ?> 
                    <?php } ?>
                </div>
                <ul class="block-list-image list-light mt-20">
                    <?php foreach ($oSubSliderItems as $index => $oSItem) { ?>
                        <?php if ($index > 0) { ?>
                            <?php $oArticle = Article::find()->where(["=", "id", $oSItem->data_id])->one(); ?>
                            <?php if ($oArticle) { ?>
                                <li>
                                    <a href="<?= $oArticle->getUrl() ?>" title="<?= TextHelper::substr($oArticle->title, 0, 300) ?>">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="block-card-image img-res">
                                                    <?php if ($oArticle->hasThumbnail()) { ?>
                                                        <img src="<?= $oArticle->getThumbnail(172, 114, "matched") ?>" alt="<?= TextHelper::substr($oArticle->title, 0, 300) ?>">
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="col-8 no-p-left">
                                                <h4><?= TextHelper::substr($oArticle->title, 0, 80) ?></h4>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</section>