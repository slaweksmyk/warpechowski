<?php

use yii\widgets\ListView;

$this->registerJs(
        '$(\'#pills-tab-' . $slider->id . ' a\').click(function (e) {
  e.preventDefault()
  $(this).tab(\'show\')
})'
);
?>

<div class="slider">

    <h5><?= $slider->title ?></h5>
    <!-- Nav tabs -->


    <?=
    ListView::widget([
        'dataProvider' => $sliderItems,
        'itemView' => 'nav',
        'summary' => false,
        'itemOptions' => [
            'tag' => false
        ],
        'options' => [
            'class' => 'nav nav-pills mb-3',
            'role' => 'tablist',
            'id' => 'pills-tab-' . $slider->id,
            'tag' => 'ul'
        ]
    ]);
    ?>




    <?=
    ListView::widget([
        'dataProvider' => $sliderItems,
        'itemView' => 'tab',
        'summary' => false,
        'itemOptions' => [
            'tag' => false
        ],
        'options' => [
            'class' => 'tab-content',
            'id' => false,
            'tag' => 'div'
        ],
        'viewParams' => [
            'showText' => $slider->settings_show_text
        ],
    ]);
    ?>


</div>