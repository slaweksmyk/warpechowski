<?php

use common\modules\articles\models\Article;
use common\modules\slider\models\{
    SliderItems,
    Slider
};
use \common\helpers\TextHelper;
?>
<section id="main-slider" data-item="main-slider" class="block block-title block-subtitle block-light block-skaner">
    <div class="container">
        <div class="block-slider">
            <div class="d-flex flex-nowrap align-items-center block-slider-top">
                <h2>
                    <span><?= Yii::$app->params['serviceName'] ?> TV live</span>
                </h2>
                <span class="line"></span>

                <ul class="nav nav-pills justify-content-end" id="pills-tab2" role="tablist">
                    <?php
                    foreach ($oMainSliderItems as $index => $oMainItem) {
                        $this->registerJs("  
                            $('[data-item=\"slider-2-{$index}-aside\"]').slick({
                                dots: false,
                                infinite: true,
                                speed: 300,
                                autoplay: false,
                                draggable: false,
                                autoplaySpeed: 300,
                                slidesToShow: 5,
                                adaptiveHeight: true,
                                vertical: true,
                                arrows: true,
                                nextArrow: $('[data-arrow=\"slider-2-{$index}-top\"]'),
                                prevArrow: $('[data-arrow=\"slider-2-{$index}-bottom\"]'),
                                });
                            $('[data-item=\"slider\"][data-id=\"slider-2-0\"] > .slider-loader').hide();
                            $('[data-item=\"slider\"][data-id=\"slider-2-0\"] [data-item=\"slider-content\"]').animate({opacity: 1});
                            ");
                        ?>
                        <li class="nav-item">
                            <a class="nav-link <?php if ($index == 0) { ?>active<?php } ?>" id="slider-2-<?= $index ?>-tab" data-action="change-slider" data-id="slider-2-<?= $index ?>" data-toggle="pill" href="#slider-2-<?= $index ?>" role="tab" aria-controls="slider-2-<?= $index ?>" aria-expanded="true"><?= $oMainItem->title ?></a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            <div class="block-slider-content d-flex flex-nowrap">
                <div class="block-slider-tab">
                    <div class="tab-content" id="pills-tab2Content">
                        <?php foreach ($oMainSliderItems as $index => $oMainItem) {
                            $oSubSliderItems = SliderItems::find()->where(["=", "parent_id", $oMainItem->id])->orderBy("sort")->limit($slider->settings_quantity_slide)->all();
                            ?>  
                            <div class="tab-pane fade <?php if ($index == 0) { ?>show active<?php } ?>" data-item="slider" data-id="slider-2-<?= $index ?>" id="slider-2-<?= $index ?>" role="tabpanel" aria-labelledby="slider-2-<?= $index ?>-tab">
                                <div class="slider-loader">
                                    <div class="loading loading--double"></div>
                                </div>
                                <div class="row d-flex" data-item="slider-content" style="opacity: 0">
                                    <div class="col-8">
                                        <?php if ($oSubSliderItems) { ?>
                                            <div class="slider-main-video">
                                                <?php $oArticle = Article::find()->where(["=", "id", $oSubSliderItems[0]->data_id])->one(); ?>
                                                <?php if ($oArticle) { ?>
                                                    <h3><a href="<?= $oArticle->getUrl() ?>" title="<?= htmlspecialchars($oArticle->title) ?>"><?= $oArticle->title ?></a></h3>
                                                    <?php if ($oArticle->getVideo()) { ?>
                                                        <?php if (strpos($oArticle->getVideo()->youtube_id, 'redcdn') !== false || $oArticle->getVideo()->youtube_id == "") { ?>
                                                            <?php if($oArticle->getRedGalaxy()->one()){ ?>
                                                                <?= $oArticle->getRedGalaxy()->one()->generateEmbed($this, "450px", "100%"); ?>
                                                            <?php } else { ?>
                                                                <iframe width="100%" height="450px" src="<?= $oArticle->getVideo()->youtube_id ?>" allowfullscreen="" frameborder="0"></iframe>
                                                            <?php } ?>
                                                        <?php } else { ?>
                                                            <?= $oArticle->getVideo()->generateEmbed("450px", "100%"); ?>
                                                        <?php } ?>
                                                    <?php } else if($oArticle->getRedGalaxy()->one()){ ?> 
                                                        <?= $oArticle->getRedGalaxy()->one()->generateEmbed($this, "450px", "100%"); ?> 
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="col-4 slider-aside-size-video">
                                        <div class="slider-aside" data-item="slider-2-<?= $index ?>-aside">
                                            <?php foreach ($oSubSliderItems as $i => $oSItem) { ?>
                                                <?php if ($i > 0) { ?>
                                                    <?php $oArticle = Article::find()->where(["=", "id", $oSItem->data_id])->one(); ?>
                                                    <?php if ($oArticle) { ?>
                                                        <div class="block-card nb vertical text-light v-80">
                                                            <div class="card-wrapper">
                                                                <a href="<?= $oArticle->getUrl() ?>" title="<?= $oSItem->title ?>">
                                                                    <div class="image img-res ">
                                                                        <?php if ($oSItem->hasThumbnail()) { ?>
                                                                            <img src="<?= $oSItem->getThumbnail($oSItem->thumbnail_id, 82, 126, "crop") ?>" alt="<?= $oSItem->title ?>">
                                                                        <?php } ?>
                                                                    </div>
                                                                    <div class="title">
                                                                        <h4><?= TextHelper::substr($oSItem->title, 0, 60) ?></h4>
                                                                    </div>
                                                                </a>
                                                                <div class="social d-flex justify-content-end align-content-end text-right">
                                                                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?= $oArticle->getAbsoluteUrl() ?>" title="Udostępnij na facebooku"><i class="icon icon-icon-facebook-small"></i></a> 
                                                                    <a href="https://twitter.com/home?status=<?= $oArticle->getAbsoluteUrl() ?>" title="Udostępnij na twitterze"><i class="icon icon-icon-twitter-small"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                        <div class="slider-aside-arrow d-flex">
                                            <div data-arrow="slider-2-<?= $index ?>-top" class="slider-aside-arrow-block light mr-auto slider-aside-arrow-top d-flex justify-content-center align-items-center"><i class="icon icon-arrow-slider-top"></i></div>
                                            <div data-arrow="slider-2-<?= $index ?>-bottom" class="slider-aside-arrow-block light slider-aside-arrow-bottom d-flex justify-content-center align-items-center"><i class="icon icon-arrow-slider-bottom"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>