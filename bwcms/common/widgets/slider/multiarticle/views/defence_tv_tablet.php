<?php

use common\modules\articles\models\Article;
use common\modules\slider\models\{
    SliderItems,
    Slider
};
use \common\helpers\TextHelper;

$slider = Slider::find()->where(['id' => $slider->id])->one();
$oMainSliderItems = SliderItems::find()->where(["=", "slider_id", $slider->id])->andWhere(["=", "parent_id", 0])->limit($slider->settings_quantity_slide_max)->all();
?>

<section id="main-slider-live" data-item="main-slider-live" class="block block-title block-subtitle  block-subtitle block-dark block-skaner">
    <div class="container">
        <div class="block-slider">
            <h2 class="mr-auto">Defence24 TV live<span class="line"></span></h2>
            <ul class="nav-slider-pills nav nav-pills justify-content-center align-items-center" id="tv-tab" role="tablist">            
                <?php foreach ($oMainSliderItems as $index => $oMainItem) { ?>
                    <li class="nav-item">
                        <a class="nav-link <?php if ($index == 0) { ?>active<?php } ?>" id="slider-3-<?= $index ?>-tab" data-action="change-slider" data-id="slider-3-<?= $index ?>" data-toggle="pill" href="#slider-3-<?= $index ?>" role="tab" aria-controls="slider-3-<?= $index ?>" aria-expanded="true"><?= $oMainItem->title ?></a>
                    </li>
                <?php } ?>
            </ul>

            <div class="block-slider-content d-flex flex-nowrap">
                <div class="block-slider-tab">
                    <div class="tab-content" id="tv-tabContent">
                        <?php
                        foreach ($oMainSliderItems as $index => $oMainItem) {

                            $oSubSliderItems = SliderItems::find()->where(["=", "parent_id", $oMainItem->id])->orderBy("sort")->limit($slider->settings_quantity_slide)->all();
                            ?>
                            <div class="tab-pane fade <?php if ($index == 0) { ?>show active<?php } ?>" data-item="slider" data-id="slider-3-<?= $index ?>" id="slider-3-<?= $index ?>" role="tabpanel" aria-labelledby="slider-3-<?= $index ?>-tab">
                                <div class="slider-loader" <?php if ($index == 0) { ?>style="display: none"<?php } ?>>
                                    <div class="loading loading--double"></div>
                                </div>
                                <!--START SLIDER-->
                                <div class="row">
                                    <div class="col-12">
                                        <?php if ($oSubSliderItems) { ?>
                                            <div class="slider-main-video">
                                                <?php
                                                $oArticle = Article::find()->where(["=", "id", $oSubSliderFirstItem->data_id])->one();
                                                if ($oArticle) {
                                                    ?>
                                                    <h3><?= $oSubSliderFirstItem->title ?></h3>
                                                    <?php if ($oArticle->getVideo()) { ?>
                                                        <?= $oArticle->getVideo()->generateEmbed("450px", "100%"); ?>
                                                        <?php
                                                    }
                                                }
                                                ?> 
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="col-12 slider-aside-size ">
                                        <div class="row slider-aside">
                                            <?php foreach ($oSubSliderItems as $index => $oSItem) { ?>
                                                <?php if ($index > 0) { ?>
                                                    <?php $oArticle = Article::find()->where(["=", "id", $oSItem->data_id])->one(); ?>
                                                    <div class="col-6 block-card nb vertical text-light v-80 mt-10">
                                                        <div class="card-wrapper">
                                                            <a href="<?= $oArticle->getUrl() ?>" title="<?= htmlspecialchars($oArticle->title) ?>">

                                                                <?php if ($oArticle->hasThumbnail()) { ?>
                                                                    <div class="image img-res ">
                                                                        <img src="<?= $oArticle->getThumbnail(158, 88, "crop") ?>" alt="<?= $oSItem->title ?>">
                                                                    </div>
                                                                <?php } ?>

                                                                <div class="title">
                                                                    <h4><?= TextHelper::substr($oSItem->title, 0, 40) ?></h4>
                                                                </div>
                                                            </a>
                                                            <div class="social d-flex justify-content-end align-content-end text-right">
                                                                <a href="https://www.facebook.com/sharer/sharer.php?u=<?= $oArticle->getUrl() ?>" title="Udostępnij na facebooku"><i class="icon icon-icon-facebook-small"></i></a> 
                                                                <a href="https://twitter.com/home?status=<?= $oArticle->getUrl() ?>" title="Udostępnij na twitterze"><i class="icon icon-icon-twitter-small"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- END SLIDER -->
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
<?php
foreach ($oMainSliderItems as $index => $oMainItem) {


    echo "
$('[data-item=\"slider-3-{$index}-aside\"]').slick({
    dots: false,
    infinite: true,
    speed: 300,
    autoplay: false,
    draggable: false,
    autoplaySpeed: 300,
    slidesToShow: 5,
    adaptiveHeight: true,
    vertical: true,
    arrows: true,
    nextArrow: $('[data-arrow=\"slider-3-{$index}-top\"]'),
    prevArrow: $('[data-arrow=\"slider-3-{$index}-bottom\"]'),
    });
$('[data-item=\"slider\"][data-id=\"slider-3-0\"] > .slider-loader').hide();
$('[data-item=\"slider\"][data-id=\"slider-3-0\"] [data-item=\"slider-content\"]').animate({opacity: 1});
";
}
?>
        $('[data-action="change-slider"]').on('click', function () {
            var id = $(this).data('id'); //slider-1-2
            var slider = $('[data-item="' + id + '-aside"]');
            setTimeout(function () {
                slider.slick("reinit");
                slider.slick("resize");

                $('.slider-loader').hide();
                $('[data-id="' + id + '"] [data-item="slider-content"]').animate({opacity: 1});
            }, 300);
        });
    });

</script>
