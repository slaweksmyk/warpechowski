<?php

use common\modules\articles\models\Article;
use common\modules\slider\models\{
    SliderItems,
    Slider
};
use \common\helpers\TextHelper;

$slider = Slider::find()->where(['id' => $slider->id])->one();
$oMainSliderItems = SliderItems::find()->where(["=", "slider_id", $slider->id])->andWhere(["=", "parent_id", 0])->limit($slider->settings_quantity_slide_max)->all();
?>

<section id="main-slider" data-item="main-slider" class="block block-title block-subtitle block-dark block-skaner">
    <div class="container">
        <div class="block-slider">
            <div class="d-flex flex-nowrap align-items-center block-slider-top">
                <h2>
                    <span><?= Yii::$app->params['serviceName'] ?> TV</span>
                </h2>
                <span class="line"></span>
            </div>
            <div class="block-slider-content d-flex flex-nowrap">
                <div class="block-slider-tab">
                    <div class="tab-content" id="pills-tab2Content">


                        <?php
                        foreach ($oMainSliderItems as $index => $oMainItem) {
                            $oSubSliderItems = SliderItems::find()->where(["=", "parent_id", $oMainItem->id])->orderBy("sort")->limit($slider->settings_quantity_slide)->all();
                            
                            if($index == 0) {
                            ?>  
                            <div class="tab-pane fade <?php if ($index == 0) { ?>show active<?php } ?>" data-item="slider" data-id="slider-tv-sub" id="slider-tv-sub" role="tabpanel" aria-labelledby="slider-tv-sub-tab">

                                <div class="slider-loader" <?php if ($index == 0) { ?>style="display: none"<?php } ?>>
                                    <div class="loading loading--double"></div>
                                </div>

                                <div class="row d-flex" data-item="slider-content" <?php if ($index > 0) { ?>style="opacity: 0"<?php } ?>>
                                    <div class="col-8">
                                        <?php if ($oSubSliderItems) { ?>
                                            <div class="slider-main-video">
                                                <?php $oArticle = Article::find()->where(["=", "id", $oSubSliderItems[0]->data_id])->one(); ?>
                                                <?php if ($oArticle) { ?>
                                                    <h3><a href="<?= $oArticle->getUrl() ?>" title="<?= htmlspecialchars($oArticle->title) ?>"><?= $oArticle->title ?></a></h3>
                                                    <?php if ($oArticle->getVideo()) { ?>
                                                        <?php if (strpos($oArticle->getVideo()->youtube_id, 'redcdn') !== false || $oArticle->getVideo()->youtube_id == "") { ?>
                                                            <?php if ($oArticle->getRedGalaxy()->one()) { ?>
                                                                <?= $oArticle->getRedGalaxy()->one()->generateEmbed($this, "450px", "100%"); ?>
                                                            <?php } else { ?>
                                                                <iframe width="100%" height="450px" src="<?= $oArticle->getVideo()->youtube_id ?>" allowfullscreen="" frameborder="0"></iframe>
                                                            <?php } ?>
                                                        <?php } else { ?>
                                                            <?= $oArticle->getVideo()->generateEmbed("450px", "100%"); ?>
                                                        <?php } ?>
                                                    <?php } else if ($oArticle->getRedGalaxy()->one()) { ?> 
                                                        <?= $oArticle->getRedGalaxy()->one()->generateEmbed($this, "450px", "100%"); ?> 
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="col-4 slider-aside-size-video">
                                        <div class="slider-aside" data-item="slider-tv-sub">
                                            <?php foreach ($oSubSliderItems as $i => $oSItem) { ?>
                                                <?php if ($i > 0) { ?>
                                                    <?php $oArticle = Article::find()->where(["=", "id", $oSItem->data_id])->one(); ?>

                                                    <div class="block-card nb vertical text-light v-80">
                                                        <div class="card-wrapper">
                                                            <a href="<?= $oArticle ? $oArticle->getUrl() : "#" ?>" title="<?= $oSItem->title ?>">

                                                                <?php if ($oSItem->hasThumbnail()) { ?>
                                                                    <div class="image img-res ">
                                                                        <img src="<?= $oSItem->getThumbnail($oSItem->thumbnail_id, 82, 126, "crop") ?>" alt="<?= $oSItem->title ?>">
                                                                    </div>
                                                                <?php } ?>

                                                                <div class="title">
                                                                    <h4><?= TextHelper::substr($oSItem->title, 0, 60) ?></h4>
                                                                </div>
                                                            </a>
                                                            <div class="social d-flex justify-content-end align-content-end text-right">
                                                                <a href="https://www.facebook.com/sharer/sharer.php?u=<?= $oArticle ? $oArticle->getAbsoluteUrl() : "#" ?>" title="Udostępnij na facebooku"><i class="icon icon-icon-facebook-small"></i></a> 
                                                                <a href="https://twitter.com/home?status=<?= $oArticle ? $oArticle->getAbsoluteUrl() : "#" ?>" title="Udostępnij na twitterze"><i class="icon icon-icon-twitter-small"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                        <div class="slider-aside-arrow d-flex">
                                            <div data-arrow="slider-tv-sub-top" class="slider-aside-arrow-block light mr-auto slider-aside-arrow-top d-flex justify-content-center align-items-center"><i class="icon icon-arrow-slider-top"></i></div>
                                            <div data-arrow="slider-tv-sub-bottom" class="slider-aside-arrow-block light slider-aside-arrow-bottom d-flex justify-content-center align-items-center"><i class="icon icon-arrow-slider-bottom"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>