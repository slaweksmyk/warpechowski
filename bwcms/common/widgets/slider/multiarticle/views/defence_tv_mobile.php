<?php

use common\modules\articles\models\Article;
use common\modules\slider\models\SliderItems;
use \common\helpers\TextHelper;

$oMainSliderItems = SliderItems::find()->where(["=", "slider_id", $slider->id])->andWhere(["=", "parent_id", 0])->all();
$oSubSliderItems = SliderItems::find()->where(["=", "slider_id", $slider->id])->andWhere(["!=", "parent_id", 0])->orderBy("sort")->all();
$oSubSliderFirstItem = SliderItems::find()->where(["=", "slider_id", $slider->id])->andWhere(["!=", "parent_id", 0])->orderBy("sort")->one();
?>
<section class="section section-list block block-title block-subtitle">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 ><span>Defence24 TV</span><span class="line"></span></h2>
                <div class="mobile-slider">
                    <?php foreach ($oSubSliderItems as $index => $oSItem) { ?>
                        <?php $oArticle = Article::find()->where(["=", "id", $oSItem->data_id])->one(); ?>
                        <?php if ($index == 0) { ?>
                            <div class="block-card shadow h-200">
                                <div class="card-wrapper">
                                    <a href="<?= $oArticle->getUrl() ?>" title="<?= $oSItem->title ?>">
                                        <div class="image img-res">
                                            <?php if ($oArticle->hasThumbnail()) { ?>
                                                <img src="<?= $oArticle->getThumbnail(430, 250, "crop") ?>" alt="<?= $oSItem->title; ?>">
                                            <?php } ?>
                                        </div>
                                        <div class="title">
                                            <div class="txt">
                                                <strong><?= $oArticle->getCategory()->name ?></strong>
                                                <h4><?= $oSItem->title ?></h4>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <ul class="block-list-image mt-20">
                            <?php } ?>
                            <?php if ($index > 0) { ?>
                                <li>
                                    <a href="<?= $oArticle->getUrl() ?>" title="<?= $oSItem->title ?>">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="block-card-image img-res">
                                                    <?php if ($oArticle->hasThumbnail()) { ?>
                                                        <img src="<?= $oArticle->getThumbnail(430, 250, "crop") ?>" alt="<?= $oSItem->title; ?>">
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="col-8 no-p-left">
                                                <h4><?= $oSItem->title ?></h4>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                    <div class="list-more">
                        <a href="#" title="Więcej">Więcej</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>