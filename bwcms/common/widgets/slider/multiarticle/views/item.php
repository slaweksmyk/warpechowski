<div class="card" style="width: 20rem;">
    <?php if ($model->settings_thumbnail_show) { ?>
        <div class="card-img"><img class="card-img-top" src="<?= $model->thumbnail_id ? common\modules\files\models\File::getThumbnail($model->thumbnail_id, 2000, 2000) : '' ?>" alt="<?= $showText ? $model->thumbnail_title : ''; ?>"></div>
    <?php } ?>
    <div class="card-body">
        <h4 class="card-title"><?= $showText ? $model->settings_show_title ? $model->title : '' : ''; ?></h4>
        <p class="card-text"><?= $showText ? $model->settings_show_description ? $model->description : '' : ''; ?></p>
        <a href="<?= $model->extlink; ?>" <?= $model->settings_target_blank ? 'target="_blank' : ''; ?>" class="btn btn-primary"><?= $showText ? ($model->text_button ? $model->text_button : Yii::t('frontend', 'Read More')) : ''; ?></a>
    </div>
</div>
