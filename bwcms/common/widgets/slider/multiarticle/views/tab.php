<?php

use common\modules\slider\models\{
    SliderItemsSearch
};
use yii\widgets\ListView;

$searchTab = new SliderItemsSearch();
$dataProviderTab = $searchTab->search(Yii::$app->request->queryParams);

$dataProviderTab->query->where(["parent_id" => $model->id]);
$dataProviderTab->query->orderBy('sort ASC, date_updated DESC');

$this->registerJs(
        '$(\'#slider-multiarticle-' . $model->id . '\').slick({
             dots:  ' . ($slider->settings_dots ? 'true' : 'false') . ',
             arrows: ' . ($slider->settings_arrow ? 'true' : 'false') . ',
             vertical: true,
             slidesToShow: ' . ($slider->settings_quantity_slide ? $slider->settings_quantity_slide : 1 ) . ',
             slidesToScroll: 1,
             swipe: false
        });'
);


?>
<!-- Tab panels -->
<div class="tab-pane  fade <?= $index == 0 ? 'show active' : ''; ?>" id="pills-<?= $model->id ?>" role="tabpanel" aria-labelledby="pills-tab-<?= $model->id ?>">
    <div class="d-flex flex-row">
        <div class="p-2">
            <div class="card" style="width: 20rem;">
   
  <?php if($model->settings_thumbnail_show){ ?>
    <div class="card-img"><img class="card-img-top" src="<?= $model->thumbnail_id ? common\modules\files\models\File::getThumbnail($model->thumbnail_id, 2000, 2000) : ''?>" alt="<?= $showText ? $model->thumbnail_title : ''; ?>"></div>
 <?php } ?>
  <div class="card-body">
    <h4 class="card-title"><?= $showText ? $model->settings_show_title ? $model->title : '' : ''; ?></h4>
    <p class="card-text"><?= $showText ? $model->settings_show_description ? $model->description : '' : ''; ?></p>
    <a href="<?= $model->extlink; ?>" <?= $model->settings_target_blank ? 'target="_blank' : ''; ?>" class="btn btn-primary"><?= $showText ? ($model->text_button ? $model->text_button : Yii::t('frontend', 'Read More')) : ''; ?></a>
  </div>
</div>
        </div>
        <div class="p-2">
            <?=
            ListView::widget([
                'dataProvider' => $dataProviderTab,
                'itemView' => 'item',
                'emptyText' => Yii::t('frontend', 'No Results'),
                'summary' => false,
                'itemOptions' => [
                    'tag' => false
                ],
                'options' => [
                    'class' => 'slider-multiarticle',
                    'role' => 'listbox',
                    'id' => 'slider-multiarticle-' . $model->id,
                    'tag' => 'div'
                ],
                'viewParams' => [
                    'showText' => $showText
                ],
            ]);
            ?>
        </div>
    </div>

</div>