<?php

use yii\widgets\ListView;

$this->registerJs(
        '$(\'#slider-article-' . $slider->id . '\').slick({
             dots: ' . ($slider->settings_dots ? 'true' : 'false') . ',
             arrows: ' . ($slider->settings_arrow ? 'true' : 'false') . ',
                 
             slidesToShow: ' . ($slider->settings_quantity_slide ? $slider->settings_quantity_slide : 1 ) . ',
             slidesToScroll: ' . ($slider->settings_quantity_slide ? $slider->settings_quantity_slide : 1 ) . '
                 
       
        });'
);
?>

<div class="slider">

    <h5><?= $slider->title ?></h5>
    <?=
    ListView::widget([
        'dataProvider' => $sliderItems,
        'itemView' => 'item',
        'emptyText' => Yii::t('frontend', 'No Results'),
        'summary' => false,
        'itemOptions' => [
            'tag' => false
        ],
        'options' => [
            'class' => 'slider-article',
            'role' => 'listbox',
            'id' => 'slider-article-' . $slider->id,
            'tag' => 'div'
        ],
        'viewParams' => [
            'showText' => $slider->settings_show_text
        ],
    ]);
    ?>

</div>