<?php

/**
 * Slider Article
 * 
 * PHP version 7
 *
 * @author Tomasz Załucki <tomek@fuleo.pl>
 * @copyright (c) 2017 Tomasz Załucki
 * @version 1.0
 */

namespace common\widgets\slider\article;

use Yii;
use yii\helpers\Html;
use common\modules\slider\models\{
    SliderItemsSearch,
    Slider
};

class SliderArticleWidget extends \common\hooks\yii2\bootstrap\Widget {

    public $widget_item_id;
    public $layout;
    public $slider_id;

    /**
     * Build Menu widget
     */
    public function init() {
        parent::init();
    }

    /**
     * Run widget
     * 
     * @return menu HTML
     */
    public function run() {
//       $type_id = SliderType::find()->where(["code" => "static"])->one()->id;
//        $slider = Slider::find()->where([ 'id' => $this->slider_id ])->one();
//        $sliderItems = SliderItems::find()->where([ 'id' => $this->slider_id ])->one();

        /*
         * Slider
         */
        $slider = Slider::find()->where(['id' => $this->slider_id])->one();

        /*
         * Slider Items
         */

        $sliderItems = new SliderItemsSearch();
        $sliderItems = $sliderItems->search(Yii::$app->request->queryParams);
        $sliderItems->query->where(["slider_id" => $this->slider_id]);
        $sliderItems->sort = [
            'defaultOrder' => [
                'sort' => SORT_ASC,
            ]
        ];
        
        return $this->display([
            'slider' => $slider,
            'sliderItems' => $sliderItems,
            'widget_item_id' => $this->widget_item_id
        ]);
    }

}
