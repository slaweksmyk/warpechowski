<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\slider\models\{Slider, SliderType};
use yii\helpers\ArrayHelper;
?>

<div class="widget-main-form" >
    
    <!-- ADD WIDGET CUSTOM DATA -->
    <div class="row" style="background: #f8f8f8; padding-top: 15px; padding-bottom: 15px; " >
        <div class="col-6">
            <?php 
          
                echo $form->field($model, 'data[slider_id]')
                       
                        ->dropDownList(
                               
                                ArrayHelper::map(Slider::find()->where(['type_id' => SliderType::find()->where(["code" => "article"])->one()->id])->all(), 'id', 'title')
                                )
                        ->label( Yii::t('backend_module_slider', 'Slider ID'));
            ?>
        </div>
    </div>
    <!-- END CUSTOM DATA -->
    
    <br>
    <div class="form-group">
        <?= Html::submitButton( Yii::t('backend', 'update'), ['class' => 'btn btn-primary']) ?>
    </div>

</div>