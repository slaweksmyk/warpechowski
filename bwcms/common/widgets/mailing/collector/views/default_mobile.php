<?php

use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Html;

$this->registerJs(
        '$("document").ready(function(){ 
        $("#newsletter-popup").on("pjax:end", function() {
            //$.pjax.reload({container:"#countries"});  //Reload GridView
           // console.log("pjax przeładowany");
        });
    });'
);
?>

<div class="modal fade newsletter-modal" id="newsletterModal" tabindex="-1" role="dialog" aria-labelledby="Newsletter" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><strong>Bądź na bieżąco!</strong>
                    Zapisz się na nasz newsletter</h5>
                <p>Najnowsze treści prosto do Twojej skrzynki mailowej. Artykuły z zakresu bezpieczeństwa w porannym przeglądzie prasy. Codziennie informacje i analizy z Defence24.pl</p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body block-newsletter">
                <?php yii\widgets\Pjax::begin(['id' => 'newsletter-popup','class' =>'newsletter-box']) ?>
                <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true, 'class' => 'col-12 form-newsletter'],]); ?>
                <input type="hidden" name="funct" value="newsletterHandler">
                <?= $form->field($model, 'group_id')->hiddenInput(['value' => $group_id])->label(false); ?>
                <?= $form->field($model, 'firstname', ['options' => ['class' => 'form-group field-newsletteremail-email required newsletter-input']])->textInput(['maxlength' => true, 'placeholder' => Yii::t('backend_module_mailing', 'Firstname Label')])->label(false) ?>
                <div class="form-group field-newsletteremail-email required newsletter-input">
                    <?= $form->field($model, 'email', ['options' => ['class' => 'form-group field-newsletteremail-email required newsletter-input']])->textInput(['maxlength' => true, 'placeholder' => Yii::t('backend_module_mailing', 'Email Label')])->label(false) ?>
                </div>
                <?= Html::submitButton(Yii::t('backend_module_mailing', 'Save Us'), ['class' => 'form-control newsletter-button']) ?>
                <?= $form->field($model, 'is_accept_rules', ['options' => ['class' => 'form-group field-newsletteremail-email required newsletter-input newsletter-rules']])->checkbox(['label' => 'Wyrażam zgodę na przetwarzanie przez Defence Sp. z o.o. moich danych osobowych']); ?>
                <?php ActiveForm::end(); ?>
                <?php Pjax::end(); ?>
            </div>
            <div class="modal-footer">
                <ul class="row">
                    <li class="col-6">
                        <a href="<?= $config->seo_tw_profile ?>" title="Twitter" target="blank" >
                            <i class="icon icon-i-n-tw mb-5"></i> <?= Yii::$app->params['serviceName'] ?> <br>na Twitterze
                        </a>
                    </li>
                    <li class="col-6"><a href="<?= $config->seo_yt_profile ?>" title="Youtube" target="blank"><i class="icon icon-i-n-yt mb-5"></i> <?= Yii::$app->params['serviceName'] ?> <br>na Youtubie</a></li>
                    <li class="col-6"><a href="<?= $config->seo_fb_profile ?>" title="Facebook" target="blank"><i class="icon icon-i-n-fb mb-5"></i> <?= Yii::$app->params['serviceName'] ?> <br>na facebooku</a></li>
                    <li class="col-6"><a href="/kanaly-rss/" title="Nasze kanały RSS" target="blank"><i class="icon icon-i-n-rss mb-5"></i> Nasze <br>kanały RSS</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>