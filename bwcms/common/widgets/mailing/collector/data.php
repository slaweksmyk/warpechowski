<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\newsletter\models\NewsletterGroup;
use yii\helpers\ArrayHelper;
use common\modules\mailing\models\MailingGroups;
?>

<div class="widget-main-form" >
    
    <!-- ADD WIDGET CUSTOM DATA -->
    <div class="row" style="background: #f8f8f8; padding-top: 15px; padding-bottom: 15px; " >
        <div class="col-6">
            <?php 
               
                echo $form->field($model, 'data[group_id]')->dropDownList(ArrayHelper::map(MailingGroups::find()->asArray()->all(), 'id', 'name'))->label( Yii::t('backend_module_newsletter', 'Group'));
            ?>
        </div>
    </div>
    <!-- END CUSTOM DATA -->
    
    <br>
    <div class="form-group">
        <?= Html::submitButton( Yii::t('backend', 'update'), ['class' => 'btn btn-primary']) ?>
    </div>

</div>