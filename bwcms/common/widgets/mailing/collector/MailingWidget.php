<?php

/**
 * Widget Mailing Block
 * 
 *
 * @author Tomasz Załucki <tomek@fuleo.pl
 * @copyright (c) 2017 Tomasz Załucki
 * @version 1.0
 */

namespace common\widgets\mailing\collector;

use Yii;
use common\modules\mailing\models\{
    MailingEmail
};

class MailingWidget extends \common\hooks\yii2\bootstrap\Widget {

    public $group_id;
    public $widget_item_id;

    /**
     * Build widget
     */
    public function init() {
        parent::init();
    }

    /**
     * Run widget
     */
//    protected function performAjaxValidation($model) {
//        if (isset($_POST['ajax'])) {
//            echo ActiveForm::validate($model);
//            Yii::app()->end();
//        }
//    }

    public function run() {

        $model = new MailingEmail();
        // $this->performAjaxValidation($model);
        return $this->display([
                    'group_id' => $this->group_id,
                    'config' => Yii::$app->params['oConfig'],
                    'model' => $model
        ]);
    }

}
