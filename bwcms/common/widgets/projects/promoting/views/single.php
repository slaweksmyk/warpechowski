<?php
    use yii\widgets\ActiveForm;
    use common\helpers\TranslateHelper; 
    
    $form = ActiveForm::begin(["id" => "promo-form"]); 
?>
    <section id="promo_step-2" style="display: block;" class="list promo-pager">
        <div class="container-fluid">
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <div class="col-8">
                        <div class="intro">
                            <div class="col-12 no-padding text-left">
                                <div class="marks">
                                    <img src="/img/ico/stats_mark.png" alt="statystyki" data-bottom=" -webkit-transform: rotate(-100deg); transform: rotate(-100deg);" data-center="-webkit-transform: rotate(0deg); transform: rotate(0deg);"> 
                                </div>
                                <h2>OPCJE PROMOWANIA</h2>
                                <div class='rectangle' data-bottom=" width:50px;" data-center=" width:74px;" ></div>
                                <h4>Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque.</h4>
                                <p>Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor. Pellentesque facilisis.</p>
                            </div>  
                        </div>
                    </div>
                    <div class="col-12 col-md-5 promotions-project-picker">
                        <h4 class="green">Promuj swój projekt:</h4>
                        <?=
                            $form->field($model, 'iType')
                                ->radioList(
                                    [1 => 'Na liście projektów', 0 => 'Baner reklamowy - boczny', 2 => 'Baner reklamowy - poziomy'],
                                    [
                                        'item' => function($index, $label, $name, $checked, $value) {
                                            $check = "";
                                            if($checked) { $check = "checked"; }
                                    
                                            $return = "<div class='radio'>";
                                                $return .= "<label>";
                                                    $return .= "<input class='promo-radio-if' type='radio' name='{$name}' value='{$value}' {$check}>{$label}";
                                                $return .= "</label>";
                                            $return .= "</div>";

                                            return $return;
                                        }
                                    ]
                                )
                            ->label(false);
                        ?>
                    </div>
                    <div class="col-12 col-md-5">
                        <h4 class="green">Wymagana ilość pkt:</h4>
                        <?php
                            $iMaxLocalPoints = 0;
                            if($oMacLocalAd){
                                $iMaxLocalPoints = $oMacLocalAd->points;
                            }
                        ?>

                        <?=
                            $form->field($model, 'is_global')
                                ->radioList(
                                    [
                                        0 => "<b>Lokalizacja projektu - </b> <span class='green'>{$iMaxLocalPoints} pkt</span> (wymagana ilość punktów dla 1 miejsca)",
                                        1 => '<b>Ogólnopolska - </b><span class="green">150 pkt</span> (wymagana ilość punktów dla 1 miejsca)'
                                    ],
                                    [
                                        'item' => function($index, $label, $name, $checked, $value) {
                                            $check = "";
                                            if($checked) { $check = "checked"; }
                                    
                                            $return = "<div class='radio'>";
                                                $return .= "<label>";
                                                    $return .= "<input class='promo-radio-if' type='radio' name='{$name}' value='{$value}' {$check}>{$label}";
                                                $return .= "</label>";
                                            $return .= "</div>";

                                            return $return;
                                        }
                                    ]
                                )
                            ->label(false);
                        ?>
                    </div>
                    <div class="global" style="display: none; clear: both;">
                        <div class="col-12 ">
                            <div class="long-rectangle"></div>
                        </div>
                        <div class="col-12 ">
                            <h4 class="green">Ilość punktów</h4>
                            <p>Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim.</p>
                        </div>
                        <div class="col-12 buttons">
                            <input type="hidden" name="ProjectPromo[iScore]" />
                            <div class="button-div" >  
                                <a class="btn-green button-list" data-value="10">10 pkt</a>
                            </div> 
                            <div class="button-div" > 
                                <a class="btn-green button-list" data-value="20">20 pkt</a>
                            </div> 
                            <div class="button-div" >  
                                <a class="btn-green button-list" data-value="30">30 pkt</a>
                            </div> 
                            <div class="button-div" >   
                                <a class="btn-green button-list" data-value="50">50 pkt</a>
                            </div> 
                            <div class="button-div" >   
                                <a class="btn-green button-list" data-value="100">100 pkt</a>
                            </div> 
                            <div class="button-div" >  
                                <a class="btn-green button-list" data-value="200">200 pkt</a>
                            </div> 
                        </div>
                        <div class="col-12">
                            <h4 class="green">Okres ważności punktów</h4>
                            <p>Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim.</p>
                        </div>
                        <div class="col-12 buttons">
                            <input type="hidden" name="ProjectPromo[iTime]" />
                            <div class="button-div" > 
                                <a class="btn-green button-list" data-value="3">3 dni</a>
                            </div> 
                            <div class="button-div" >  
                                <a class="btn-green button-list" data-value="5">5 dni</a>
                            </div> 
                            <div class="button-div" >   
                                <a class="btn-green button-list" data-value="7">1 Tydzień</a>
                            </div> 
                            <div class="button-div" >   
                                <a class="btn-green button-list" data-value="14">2 tygodnie</a>
                            </div> 
                            <div class="button-div" >  
                                <a class="btn-green button-list" data-value="30">1 miesiąc</a>
                            </div> 
                            <div class="button-div" > 
                                <a class="btn-green button-list" data-value="90">3 miesiące</a>
                            </div> 
                        </div>
                    </div>
                    <div class="local" style="display: none; clear: both;">
                        <div class="col-12 ">
                            <div class="long-rectangle"></div>
                        </div>
                        <div class="col-12 no-padding">
                            <div class="col-12 ">
                                <h4 class="green">Promuj w innej lokalizacji</h4>
                                <p>Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim.</p>
                            </div>
                            <div class="col-12 col-sm-10 col-md-6 col-lg-4 ">
                                <div class="input-group input-group-form">
                                    <input style="position:relative;z-index:0" type="text" name="city" id="pac-inpu" class="form-control width-80" placeholder="Nazwa miejscowości">
                                    <input type="hidden" name="RegisterForm[city]"/>
                                    <input style="position:relative;" class="submit-city" value="Dodaj" type="submit">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 add-banner" style="display: none;">
                        <div class="long-rectangle"></div>
                        <h4 class="green">Dodaj baner reklamowy</h4>
                        <p>Do prawidłowego wyświetlania baneru reklamowego, plik powinien mieć wymiary... małego domu mieszkalnego</p>
                        <div class="col-12 upload no-padding">
                            <?= $form->field($model, 'banner')->fileInput(["id" => "file", "class" => "inputfile"])->label(false) ?>
                            <label for="file">Dodaj plik</label>
                        </div>
                    </div>
                    <div class="col-12 ">
                        <div class='rectangle' data-bottom=" width:50px;" data-center=" width:74px;"></div>  
                    </div> 
                    <div class="col-12 price">
                        <p>
                            <span class="green">Koszt promowania -</span>
                            <b><b id="full-cost">0</b> PLN</b>
                            <input type="hidden" name="ProjectPromo[iCost][]" value="0" />
                        </p>
                    </div> 
                    <div class="col-12" style="margin-top:30px;">
                        <a id="accept-next_button" class="btn-green button-list" onclick="$('#promo-form').submit()">
                            ZATWIERDŹ <img src="/img/ico/accept.png" alt="akceptacja">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php ActiveForm::end(); ?>