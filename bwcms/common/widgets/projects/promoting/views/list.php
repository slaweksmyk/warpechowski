<?php
    use common\helpers\TranslateHelper; 
?>

<section id="my_projects" style="display: block;" class="list pr-pager" data-title="<?= TranslateHelper::t('PROJECT_SECTION_MY_PROJECTS') ?>">
    <div class="container-fluid">
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <?php if(count($oMineProjects) > 0){ ?>
                    <div class="intro">
                        <div class="col-12">
                            <div class="marks">
                                <img src="/img/ico/star_mark.png" alt="gwiazdka" data-bottom=" -webkit-transform: rotate(-100deg); transform: rotate(-100deg);" data-center="-webkit-transform: rotate(0deg); transform: rotate(0deg);"> 
                            </div>
                            <h2><?= TranslateHelper::t('PROJECT_LIST_TITLE') ?></h2>
                            <div class='rectangle' data-bottom=" width:50px;" data-center=" width:74px;"></div>
                            <?= TranslateHelper::t('PROJECT_LIST_DESC') ?>
                        </div>
                    </div>
                    <div class="content">
                        <?php foreach($oMineProjects as $oProject){ ?>
                            <div class="col-6 col-lg-3">
                                <div class="project" style="border-color: <?= $oProject->getCategory()->one()->color ?>">
                                    <div class="project-img" <?php if($oProject->hasThumbnail()){ ?>style="background-image:url('<?= $oProject->getThumbnail(273, 183)  ?>')"<?php } ?>>
                                        <div class="col-6 no-padding">
                                            <div class="star" style="background-color: <?= $oProject->getCategory()->one()->color ?>">
                                                <img src="/img/ico/project_star.png" alt="gwiazdka"> <span><?= $oProject->getAverageIdeaScore(); ?></span>
                                            </div>
                                        </div>
                                        <div class="col-6 no-padding text-right">  
                                            <div class="category">
                                                <?php if($oProject->getCategory()->one()->getThumbnail()->one()){ ?>
                                                    <img src="/upload/<?= $oProject->getCategory()->one()->getThumbnail()->one()->filename; ?>" style="max-width: 35px; background-color: <?= $oProject->getCategory()->one()->color ?>; box-shadow: 0px 0px 10px 15px <?= $oProject->getCategory()->one()->color ?>" alt="<?= $oProject->getCategory()->one()->getThumbnail()->one()->name; ?>">
                                                <?php }?>
                                            </div>
                                        </div>
                                        <div class="col-5 no-padding">
                                            <div class="description">
                                                <span><?= date("d.m.Y", strtotime($oProject->date_start)) ?></span>
                                            </div>
                                        </div>
                                        <div class="col-7 no-padding text-right">
                                            <div class="description">
                                                <span><?= $oProject->getProjectData()->one()->city; ?>, <?= $oProject->getProjectData()->one()->district; ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="project-txt">
                                        <h6><?= $oProject->getProjectData()->one()->name; ?></h6>
                                        <?= $oProject->getProjectData()->one()->short_description; ?>
                                        <div class="text-right read-more"><a href="/promuj/<?= $oProject->getProjectData()->one()->slug; ?>">Promuj <span style="color: <?= $oProject->getCategory()->one()->color ?>">&#9658;</span> </a></div>   
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                <?php } else { ?>
                    <div class="intro">
                        <div class="col-8 col-offset-2">
                            <div class="marks">
                                <img src="/img/ico/star_mark.png" alt="gwiazdka" data-bottom=" -webkit-transform: rotate(-100deg); transform: rotate(-100deg);" data-center="-webkit-transform: rotate(0deg); transform: rotate(0deg);"> 
                            </div>
                            <h2>LISTA PROJEKTÓW</h2>
                            <div class='rectangle' data-bottom=" width:50px;" data-center=" width:74px;"></div>
                            <h4>Nie posiadasz jeszcze żadnych swoich projektów!</h4>
                            <p>Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor. Pellentesque facilisis.</p>
                        </div>
                        <div class="col-12">
                            <div class="button-div">   
                                <a href="/konto/zaloguj-sie/" class="btn-green button-list">ZALOGUJ SIĘ LUB ZAREJESTRUJ</a>
                            </div> 
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="col-2"></div>
        </div>
    </div>
</section>