<?php
/**
 * Widget HTML
 * 
 * PHP version 7
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @copyright (c) 2016 Krzysztof Borecki
 * @version 1.0
 */

namespace common\widgets\projects\promoting;

use Yii;
use yii\data\Pagination;
use yii\web\UploadedFile;
use common\modules\projects\models\Project;
use common\modules\projects\models\ProjectAds;
use common\modules\projects\models\ProjectData;
use common\widgets\projects\promoting\models\ProjectPromo;

class ProjectPromotingWidget extends \common\hooks\yii2\bootstrap\Widget
{
    public $widget_item_id, $layout;
    
    private $slug;

    /**
     * Build widget
     */
    public function init()
    {
        if(array_key_exists("slug", Yii::$app->params)){
            $this->slug = Yii::$app->params['slug'];
        }

        parent::init();
    }

    
    /**
     * Run widget
     */
    public function run()
    {
        $oUser = Yii::$app->session->get('oUser');
        
        if(is_null($oUser)){
            Yii::$app->getResponse()->redirect("/konto/zaloguj-sie/")->send();
            Yii::$app->end();
            exit;
        }
        
        if($this->slug){
            return $this->displaySingle();
        } else {
            return $this->displayList();
        }
    }
    
    private function displayList(){
        $iLimit = Yii::$app->request->get('limit', 999);

        $oProjectRowsetQuery = Project::find();
        $oProjectRowsetQuery->where(['=', "is_active", 1]);
        $oProjectRowsetQuery->orderBy("id DESC");
        
        $pageUrl = explode("?", Yii::$app->request->url);
        if(count($pageUrl) > 0){ $pageUrl = $pageUrl[0]; } else { $pageUrl = Yii::$app->request->url; }
        
        $sCountQuery = clone $oProjectRowsetQuery;
        $pages = new Pagination([
            'totalCount' => $sCountQuery->count(), 
            'pageSize' => $iLimit, 
            'pageSizeParam' => 'limit',
            'route' => $pageUrl
        ]);
        
        $aMineProjects = [];
        
        $oUser = Yii::$app->session->get('oUser');
        if(is_null($oUser)){
            Yii::$app->getResponse()->redirect("/konto/zaloguj-sie/")->send();
            Yii::$app->end();
            exit;
        }
        
        $oProjectRowset = $oProjectRowsetQuery->offset($pages->offset)->limit($pages->limit)->all();
        foreach($oProjectRowset as $oProj){
            foreach($oProj->getProjectOrganizers()->where(["=", "is_main", 1])->all() as $oOrg){
                if($oOrg->user_id == $oUser->id){
                    array_push($aMineProjects, $oProj);
                }
            }
        }
        
        return $this->display([
            'limit' => $iLimit,
            'pages' => $pages,
            'oMineProjects' => $aMineProjects,
        ]);
    }
    
    private function displaySingle(){
        $oProjectData = ProjectData::find()->where(['and', "slug = '{$this->slug}'"])->one();
        
        // Check if there is any article
        if(!$oProjectData){ return; }
        
        $oProject = $oProjectData->getProject()->one();
        
        $model = new ProjectPromo();
        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            $model->banner = UploadedFile::getInstance($model, 'banner');
            if($model->banner === null){
                $model->save($oProject->id);
            } else {
                $iBanner = $model->upload();
                if ($iBanner != false) {
                    $model->save($oProject->id, $iBanner);
                }
            }
            
        }
        
        
        $oMacLocalAd = ProjectAds::find()->where(["<", "type", 2])->andWhere([">", "date_expire", date("Y-m-d H:i:s")])->orderBy("points DESC")->one();
        
        return $this->display([
            'model' => $model,
            'oProjectData' => $oProjectData,
            'oProject' => $oProject,
            'oMacLocalAd' => $oMacLocalAd
        ]);
    }
    
}
