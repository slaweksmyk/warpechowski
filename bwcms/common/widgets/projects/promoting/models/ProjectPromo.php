<?php

namespace common\widgets\projects\promoting\models;

use Yii;
use yii\base\Model;

use common\modules\files\models\File;
use common\modules\projects\models\ProjectAds;

/**
 * ContactForm is the model behind the contact form.
 */
class ProjectPromo extends Model
{
    public $widget_item_id;

    public $iType,$iScore,$iTime,$iCost;
    public $city;
    public $banner;
    public $is_global;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['iType', 'is_global'], 'required', 'message' => 'To pole jest wymagane'],
            [['iScore', 'iTime', 'is_global', 'city'], 'string'],
            
            [['banner'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    public function save($iProjectID, $iBanner = null){
        $aScore = $aTime = [];

        if(is_array($this->iScore)){
            foreach($this->iScore as $sCity => $iScor){ 
                if($iScor != ""){
                    $aScore[$sCity] = $iScor;
                }
            }
        }
        
        if(is_array($this->iTime)){
            foreach($this->iTime as $sCity => $iTim){ 
                if($iTim != ""){
                    $aTime[$sCity] = $iTim;
                }
            }
        }
        
        if(count($aScore) > 0){
            foreach($aScore as $city => $points){
                $oAd = new ProjectAds();
                $oAd->type = $this->iType;
                $oAd->project_id = $iProjectID;
                $oAd->banner_id = $iBanner;
                $oAd->points = $points;
                $oAd->city = $city;
                $oAd->date_created = date("Y-m-d H:i:s");
                $oAd->date_expire = date("Y-m-d H:i:s", strtotime("+".$aTime[$city]." days"));
                $oAd->save();
            }
        } else {
            $oAd = new ProjectAds();
            $oAd->type = $this->iType;
            $oAd->project_id = $iProjectID;
            $oAd->banner_id = $iBanner;
            $oAd->points = $this->iScore;
            $oAd->date_created = date("Y-m-d H:i:s");
            $oAd->date_expire = date("Y-m-d H:i:s", strtotime("+".$this->iTime." days"));
            $oAd->save();
        }
             
        exit;
    }
    
    public function upload(){
        if ($this->validate()) {
            $hash = base_convert(time(), 10, 36);

            $file_name = preg_replace("/[^a-zA-Z0-9.]/", "", $this->banner->baseName);
            $save_name = $hash."_" . $file_name . '.' . $this->banner->extension;

            $this->banner->saveAs(Yii::getAlias('@root/upload/promo/') . $save_name);
            $filename = $save_name;

            if( $filename ) {
                $info = pathinfo($this->banner->name);

                $oFile = new File();
                $oFile->category_id = 4;
                $oFile->name = $info['filename'];
                $oFile->filename = $filename;
                $oFile->type = $this->banner->type;
                $oFile->size = $this->banner->size;
                $oFile->thumbnail = 0;
                $oFile->save(); 
            }

            return $oFile->id;
        } else {
            return false;
        }
    }
    
}
