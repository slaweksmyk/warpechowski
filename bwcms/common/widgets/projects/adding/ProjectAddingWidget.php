<?php
/**
 * Widget HTML
 * 
 * PHP version 7
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @copyright (c) 2016 Krzysztof Borecki
 * @version 1.0
 */

namespace common\widgets\projects\adding;

use Yii;
use common\modules\projects\models\Project;

class ProjectAddingWidget extends \common\hooks\yii2\bootstrap\Widget
{
    public $widget_item_id, $layout;

    /**
     * Build widget
     */
    public function init()
    {
        parent::init();
    }

    
    /**
     * Run widget
     */
    public function run()
    {
        $oUser = Yii::$app->session->get('oUser');
        
        if(is_null($oUser)){
            Yii::$app->getResponse()->redirect("/konto/zaloguj-sie/")->send();
            Yii::$app->end();
            exit;
        }
        
        return $this->display([]);
    }
    
}
