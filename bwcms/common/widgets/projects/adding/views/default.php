<section id="basic_info_logged" class="add basic_info">     
    <div class="step-2">
        <div class="cont">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-2"></div>
                    <div class="col-8">
                        <h2>Dodaj projekt</h2>
                        <div class='rectangle' data-bottom=" width:50px;" data-center=" width:74px;"></div>
                        <div class="col-12 no-padding">
                        <div class="col-12 col-lg-6">
                        <h3 class='green'>Informacje o projekcie</h3>
                        <span class='red'>* Pola obowiązkowe (w przypadku zbiórki oraz aukcji, pola adresu i miejscowości są nieobowiązkowe)</span>
                       </div>
                          <div class="col-lg-5 col-lg-offset-1 hidden-xs hidden-sm hidden-md">
                        <h3 class='green'>Serwisy społecznościowe</h3>
                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at arcu sit.</span>
                       </div>
                         </div>
                        <div class="col-12 col-lg-6 no-padding">
                            <div class="col-12 ">
                                <div class="input-group input-group-form">
                                    <input type="text" name="mail" class="form-control" placeholder="Nazwa projektu*">
                                </div>
                            </div>
                            <div class="col-8" style="padding-right:1.5%;">
                                <div class="input-group input-group-form">
                                    <input id="address" type="text" name="mail" class="form-control" placeholder="Adres* (np. Warszawa, ul. Nowa 22)">
                                </div>
                            </div>
                            <div class="col-8  col-lg-3" style="padding-right:1.5%;">
                                <div class="input-group input-group-form">
                                    <input type="text" name="mail" class="form-control" placeholder="Nr. lokalu">
                                </div>
                            </div>
                                 <div class="col-12 ">
                                <div class="input-group input-group-form">
                                    <input type="text" name="mail" class="form-control" placeholder="Data rozpoczęcia*">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="input-group input-group-form">
                                    <input type="text" name="mail" class="form-control" placeholder="Data zakończenia*">
                                </div>
                            </div>
                                <div class="col-10 col-offset-2 col-lg-12 col-lg-offset-0 orange">
                                <h3>Dodaj język</h3>
                            </div>
                            <div class="col-12 ">
                                <div class="input-group input-group-form senderino">
                                    <select id="example1" class="width-80 select-transparent" name="test">
                                        <option value="1">Polski</option>
                                        <option value="2">Angielski</option>
                                        <option value="3">Niemiecki</option>
                                    </select>
                                    <input value="Wyślij" type="submit">
                                </div>
                            </div>
                        </div>
<div class="col-10 col-offset-2 hidden-lg">
                        <h3 class='green'>Serwisy społecznościowe</h3>
                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at arcu sit.</span>
                       </div>
                        <div class="col-12 col-lg-6 no-padding">
                            <div class="adding-form">
                            <div class="col-2 col-lg-3 text-right">
                                <p>Facebook</p>
                            </div>
                          <div class="col-10 col-lg-9  no-padding">
                                <div class="input-group input-group-form">
                                    <input type="text" name="mail" class="form-control width-80" placeholder="Dodaj linki z portalu">
                                       <input style="display: inline-block;" value="Dodaj" type="submit">
                                </div>

                            </div>
                                     <div class="col-2 col-lg-3 text-right">
                                <p>Instagram</p>
                            </div>
                          <div class="col-10 col-lg-9  no-padding">
                                <div class="input-group input-group-form">
                                    <input type="text" name="mail" class="form-control width-80" placeholder="Dodaj linki z portalu">
                                       <input style="display: inline-block;" value="Dodaj" type="submit">
                                </div>

                            </div>
                                     <div class="col-2 col-lg-3 text-right">
                                <p>YouTube</p>
                            </div>
                          <div class="col-10 col-lg-9  no-padding">
                                <div class="input-group input-group-form">
                                    <input type="text" name="mail" class="form-control width-80" placeholder="Dodaj linki z portalu">
                                       <input style="display: inline-block;" value="Dodaj" type="submit">
                                </div>

                            </div>
                                     <div class="col-2 col-lg-3 text-right">
                                <p>Twitter</p>
                            </div>
                          <div class="col-10 col-lg-9  no-padding">
                                <div class="input-group input-group-form">
                                    <input type="text" name="mail" class="form-control width-80" placeholder="Dodaj linki z portalu">
                                       <input style="display: inline-block;" value="Dodaj" type="submit">
                                </div>

                            </div>
                            <div class="col-10 col-offset-2">
                                <h3 class="green">Strona www</h3>
                            </div>
                                     <div class="col-2 col-lg-3 text-right">
                                <p>WWW</p>
                            </div>
                          <div class="col-10 col-lg-9 no-padding">
                                <div class="input-group input-group-form">
                                    <input type="text" name="mail" class="form-control width-80" placeholder="Dodaj linki ze strony">
                                       <input style="display: inline-block;" value="Dodaj" type="submit">
                                </div>
                                 </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div style="text-align: left;float:left;color:#e6b32a">
                                <h6>Lokalizacja na mapie:</h6>
                            </div>
                            <div style="text-align: right;">
                                <h6>Lokalizacja:</h6>
                                <input type="text" name="mail" value="52.229676, 21.012229" id="latlng" class="form-control latlong" placeholder="°N, °E">
                            </div>
                        </div>
                        <div class="col-12">
                            <div id="map_add">
                            </div>
                        </div>
                        <div class="col-12 text-right">
                            <div class="button-div">
                                <a id="basic_info-next_button" class="btn-green button-list">Dalej</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="description" class="add">
    <div class="step-1">
        <div class="cont">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-2"></div>
                    <div class="col-8">
                        <h2>Dodaj projekt</h2>
                        <div class='rectangle' data-bottom=" width:50px;" data-center=" width:74px;"></div>
                        <h3 class='green'>Skrócony opis projektu</h3>
                        <ul class="short">
                            <li class="ger active"> Niemiecki</li>
                            <li class="eng">Angielski</li>
                            <li class="pl">Polski</li>
                        </ul>
                        <div class="form-group">
                            <textarea class="form-control" id="short-content" placeholder="Wypełnij"></textarea>
                        </div>
                        <h3 class='orange'>Pełny opis projektu</h3>
                        <ul class="long">
                            <li class="ger active"> Niemiecki</li>
                            <li class="eng">Angielski</li>
                            <li class="pl">Polski</li>
                        </ul>
                        <div class="form-group">
                            <textarea class="form-control" id="full-content" placeholder="Wypełnij"></textarea>
                        </div>
                        <div class="col-6 no-padding">
                            <div class="button-div">
                                <a id="description-prev_button" class="btn-green button-list">Wstecz</a>
                            </div>
                        </div>
                        <div class="col-6  no-padding text-right">
                            <div class="button-div">
                                <a id="description-next_button" class="btn-green button-list">Dalej</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="category-add" class="add section-category">
    <div class="step-1">
        <div class="cont">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-2"></div>
                    <div class="col-8">
                        <h2>Dodaj projekt</h2>
                        <div class='rectangle' data-bottom=" width:50px;" data-center=" width:74px;"></div>
                        <h3 class='green'>Wybierz kategorię Twojego projektu</h3>
                        <div class="category-list">
                            <div class="cat-top">
                                <div class="col-12 no-padding">
                                    <div class="left">
                                        <span class="pre-cat-prev">
                                            <img src="/img/ico/triangle.png" alt="trójkąt"style="transform:rotate(180deg);-webkit-transform:rotate(180deg);margin-right:3px;" >Poprzednie </span>
                                    </div>
                                    <div class="right">
                                        <span class="pre-cat-next">
                                            Następne 
                                            <img src="/img/ico/triangle.png" alt="trójkąt" >
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="cat-bottom">
                                <div class="row">
                                    <div id="category-01" class="col-4 col-md-2 categories pre-category active">
                                        <img src="/img/ico/all.png" alt="wszystko">
                                        <h6>WSZYSTKIE</h6>
                                    </div>
                                    <div id="category-02" class="col-4 col-md-2 pre-category categories">
                                        <img src="/img/ico/runner.png" alt="biegacz">
                                        <h6>DWIE LINIJKI </h6>
                                    </div>
                                    <div id="category-03" class="col-4 col-md-2 pre-category categories">
                                        <img src="/img/ico/all.png" alt="wszystko">
                                        <h6>RODZINNE</h6>
                                    </div>
                                    <div id="category-04" class="col-4 col-md-2 pre-category categories">
                                        <img src="/img/ico/family.png" alt="rodzina">
                                        <h6>WSZYSTKIE</h6>
                                    </div>
                                    <div id="category-05" class="col-4 col-md-2 pre-category categories">
                                        <img src="/img/ico/runner.png" alt="biegacz">
                                        <h6>AMBITNE</h6>
                                    </div>
                                    <div id="category-06" class="col-4 col-md-2 pre-category categories">
                                        <img src="/img/ico/all.png" alt="wszystko">
                                        <h6>JAKIEŚ TAM</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 no-padding">
                            <span class="input input--ruri">
                                <input class="input__field input__field--ruri" type="text" id="input-28" />
                                <label class="input__label input__label--ruri" for="input-28">
                                    <span class="input__label-content input__label-content--ruri">Wpisz adres www strony zewnętrznej aukcji</span>
                                </label>
                            </span>
                        </div>
                        <h3 class='orange'>Wybierz podkategorię Twojego projektu</h3>
                        <div class="post-category-list">
                            <div class="cat-top">
                                <div class="col-12 no-padding">
                                    <div class="left">
                                        <span class="pre-cat-prev"><img src="/img/ico/triangle.png" alt="trójkąt"style="transform:rotate(180deg);-webkit-transform:rotate(180deg);margin-right:3px;" >Poprzednie </span>
                                    </div>
                                    <div class="right">
                                        <span class="pre-cat-next">Następne <img src="/img/ico/triangle.png" alt="trójkąt" ></span>
                                    </div>
                                </div>
                            </div>
                            <div class="cat-bottom">
                                <div class="row">
                                    <div id="post-category-single-01" class="col-4 col-md-2 categories post-category-single">
                                        <img src="/img/ico/all-post.png" alt="wszystko">
                                        <h6>WSZYSTKIE</h6>
                                    </div>
                                    <div id="post-category-single-02" class="col-4 col-md-2 categories post-category-single">
                                        <img src="/img/ico/runner-post.png" alt="biegacz">
                                        <h6>WSZYSTKIE</h6>
                                    </div>
                                    <div id="post-category-single-03" class="col-4 col-md-2 categories post-category-single">
                                        <img src="/img/ico/all-post.png" alt="wszystko">
                                        <h6>WSZYSTKIE</h6>
                                    </div>
                                    <div id="post-category-single-04" class="col-4 col-md-2 categories post-category-single">
                                        <img src="/img/ico/family-post.png" alt="rodzina">
                                        <h6>WSZYSTKIE</h6>
                                    </div>
                                    <div id="post-category-single-05" class="col-4 col-md-2 categories post-category-single">
                                        <img src="/img/ico/runner-post.png" alt="biegacz">
                                        <h6>WSZYSTKIE</h6>
                                    </div>
                                    <div id="post-category-single-06" class="col-4 col-md-2 categories post-category-single">
                                        <img src="/img/ico/all-post.png" alt="wszystko">
                                        <h6>WSZYSTKIE</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="buttons">
                            <div class="button-div col-12 col-md-4 no-padding">
                                <a id="add-photos_button" class="btn-yellow button-list">
                                    DODAJ ZDJĘCIA 
                                    <img src="/img/ico/photo.png" alt="zdjęcie" >
                                </a>
                            </div>
                            <div class="button-div col-12 col-md-6 no-padding"> 
                                <a href="#contact" class="btn-black button-list">
                                    ZAPROPONUJ KATEGORIĘ
                                </a>
                            </div>
                        </div>
                        <div class="col-6 no-padding margin-top-30">
                            <div class="button-div"> 
                                <a id="category-add-prev_button" class="btn-green button-list">
                                    Wstecz
                                </a>
                            </div>
                        </div>
                        <div class="col-6  no-padding text-right margin-top-30">
                            <div class="button-div"> 
                                <a id="category-add-next_button" class="btn-green button-list">
                                    Dalej
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="step-2">
        <div class="cont">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-2"></div>
                    <div class="col-8">
                        <h2>Dodaj zdjęcia</h2>
                        <div class='rectangle' data-bottom=" width:50px;" data-center=" width:74px;"></div>
                        <div class='photos'>
                            <div class="col-12 col-sm-6 col-lg-3">
                                <div class="photo" style="background-image:url('/img/examples/add-photo-1.jpg')">
                                    <div class='photo-person'>
                                        <img src="/img/ico/person-small-green.png" alt="osoba">
                                        <span>Zdjęcie wydarzenia</span> 
                                    </div>
                                    <div class='cross'>
                                        <img src="/img/ico/cross.png" alt="krzyżyk">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3">
                                <div class="photo" style="background-image:url('/img/examples/add-photo-2.jpg')">
                                    <div class='photo-person'>
                                        <img src="/img/ico/person-small-white.png" alt="osoba">
                                    </div>
                                    <div class='cross'>
                                        <img src="/img/ico/cross.png" alt="krzyżyk">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3">
                                <div class="photo" style="background-image:url('/img/examples/add-photo-3.jpg')">
                                    <div class='photo-person'>
                                        <img src="/img/ico/person-small-white.png" alt="osoba">
                                    </div>
                                    <div class='cross'>
                                        <img src="/img/ico/cross.png" alt="krzyżyk">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3">
                                <div class="add-photo" style="background-image:url('/img/ico/add-photo.png');background-color:#fff;">
                                    <div class='photo-person'>
                                        <img src="/img/ico/person-small-white.png" alt="osoba">
                                    </div>
                                    <div class='cross'>
                                        <img src="/img/ico/cross.png" alt="krzyżyk">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class='col-12'>
                            <div class="buttons">
                                <div class="button-div">
                                    <a id="add-photos_back-button" class="btn-yellow button-list">
                                        <img src="/img/ico/triangle-white.png" alt="trójkąt" style="transform:rotate(90deg);-webkit-transform:rotate(90deg);margin-right:3px;">
                                        POWRÓT 
                                    </a>
                                </div>
                                <div class="button-div"> <a href="index.php" class="btn-green button-list">
                                        AKCEPTUJ
                                        <img src="/img/ico/accept.png" alt="akceptacja">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class='list single-event' id='preview'>
    <div class="step-1">
        <div class="cont">
            <div class='container-fluid'>
                <div class='row'>
                    <div class='col-2'>
                    </div>
                    <div class='col-8'>
                        <div class="intro">
                            <div class='marks'>
                                <img src="/img/ico/scream_mark.png" alt="wykrzyknik">
                            </div>
                            <h2>PODGLĄD PROJEKTU</h2>
                            <div class="rectangle"></div>
                            <h4>Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque.</h4>
                            <p>Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor. Pellentesque facilisis.</p>
                        </div>
                        <div class="content">
                            <h3>Bieg wolności im. Boleslawa Chrobrego</h3>
                            <div class="rectangle"></div>
                            <div class="col-12 col-lg-8 " style="padding-left:0px;">
                                <p>
                                    <b>WARSZAWA, Bemowo, ul. dostępna 13 </b>
                                    <a class="orange"> (Pokaż lokalizacje na mapie)</a>
                                </p>
                                <p>Data rozpoczęcia: 23.10.2016</p>
                                <p>Data zakończenia: 10.11.2016</p>
                                <div class="content-text">
                                    <p>Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor. Pellentesque facilisis. This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.</p>
                                    <p>Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. tum sit amet a augue.This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed </p>
                                    <div class="col-12 no-padding" style='margin-top:30px;'>
                                        <a  class="btn-green button-list">CHCĘ WZIĄĆ UDZIAŁ W REALIZACJI PROJEKTU</a>
                                    </div>
                                    <div class="col-12 no-padding " style='margin:30px 0 20px 0;'>
                                        <a href="index.php" class="btn-yellow button-list">
                                            JESTEM ZAINTERESOWANY
                                        </a>
                                    </div>
                                    <div class="text-left button-div" style='margin-top:160px;'> 
                                        <a href="index.php" class="btn-green button-list">
                                            EDYTUJ 
                                            <img src="/img/ico/pen.png" alt="długopis">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4">
                                <div class="right-side">
                                    <h6>Ocena pomysłu:</h6>
                                    <div class="star">
                                        <img src="/img/ico/project_star_big.png" alt="gwiazdka">
                                        <span>5</span>
                                    </div>
                                    <p>274 głosy</p>
                                    <h6>Oceń pomysł:</h6>
                                    <div class="rates">
                                    </div>
                                    <div class="photo-event" style="background-image: url('/img/examples/event-1.jpg')">
                                    </div>
                                    <div class='share'>
                                        <a>
                                            Udostępnij 
                                            <img src="/img/ico/facebook.jpg" alt="ikonka"> 
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 no-padding">
                            <div class="button-div"> <a id="preview-prev_button" class="btn-green button-list">Wstecz</a>
                            </div>
                        </div>
                        <div class="col-6  no-padding text-right">
                            <div class="button-div"> <a id="preview-next_button" class="btn-green button-list">Dalej</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="accept-add" class="add">
    <div class="step-1">
        <div class="cont">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-2"></div>
                    <div class="col-8">
                        <h2>Zatwierdź projekt</h2>
                        <div class="rectangle"></div>
                        <h3 class='green'>Zaakceptuj regulamin</h3>
                        <div class="acceptation">
                            <div class="row">
                                <div class="col-1">
                                    <p>
                                        <input type="checkbox" id="test3" />
                                        <label for="test3"></label>
                                    </p>
                                </div>
                                <div class="col-11">
                                    This is Photoshop's version REGULAMIN. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.
                                </div>
                            </div>
                        </div>
                        <div class="acceptation">
                            <div class="row">
                                <div class="col-1">
                                    <p>
                                        <input type="checkbox" id="test4" />
                                        <label for="test4"></label>
                                    </p>
                                </div>
                                <div class="col-11">
                                    This is Photoshop's version REGULAMIN. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.
                                </div>
                            </div>
                        </div>
                        <h3 class='orange'>Promuj swój projekt</h3>
                        <div class="acceptation">
                            <div class="row">
                                <div class="col-1">
                                    <p>
                                        <input type="checkbox" id="test5" />
                                        <label for="test5"></label>
                                    </p>
                                </div>
                                <div class="col-11">
                                    This is Photoshop's version REGULAMIN. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6  col-md-6 col-lg-4 col-12 no-padding" style="margin-top:30px;"> <a id="accept-next_button" class="btn-green button-list">ZATWIERDŹ <img src="/img/ico/accept.png" alt="akceptacja"></a>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-8 col-12">
                            <div class="g-recaptcha" data-sitekey="6LfqpBYUAAAAADty7Aw3Wo-1q_r-9p8oL2-Weozh"></div>
                        </div>

                        <div class="col-6 no-padding margin-top-30">
                            <div class="button-div">
                                <a id="accept-prev_button" class="btn-green button-list">
                                    Wstecz
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="step-2">
        <div class="cont">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-2"></div>
                    <div class="col-5">
                        <img src="/img/ico/accept_mark.png" alt="akceptacja">
                        <h2>Dziękujemy!</h2>
                        <div class="rectangle"></div>
                        <h6>Na podany adres email zostanie wysłana wiadomość itp.</h6>
                        <p>Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor. Pellentesque facilisis.</p>
                        <p>Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor. Pellentesque facilisis.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>