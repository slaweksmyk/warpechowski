<?php
    use common\helpers\TranslateHelper; 
?>

<section id="list-archive" class="list pr-pager noscrollevent" style="display: block;" data-title="<?= TranslateHelper::t('PROJECT_SECTION') ?>">
    <div class="container-fluid">
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <div class="intro">
                    <div class="col-12">
                        <div class="marks">
                            <img src="/img/ico/star_mark.png" alt="gwiazdka" data-bottom=" -webkit-transform: rotate(-100deg); transform: rotate(-100deg);" data-center="-webkit-transform: rotate(0deg); transform: rotate(0deg);"> 
                        </div>
                        <h2><?= TranslateHelper::t('PROJECT_LIST_TITLE') ?></h2>
                        <div class='rectangle' data-bottom=" width:50px;" data-center=" width:74px;"></div>
                        <?= TranslateHelper::t('PROJECT_LIST_DESC') ?>
                    </div>
                </div>
                <div class="content">
                    <div class="col-12 text-center">
                        <ul><?= TranslateHelper::t('SORT_BY') ?>:	
                            <li class="active"><a href="/archiwum/?sort=date_start"><?= TranslateHelper::t('SORT_BY_DATE') ?></a></li>
                            <li><a href="/archiwum/?sort=date_start"><?= TranslateHelper::t('SORT_BY_LOCALITY') ?></a></li>
                            <li><a href="/archiwum/?sort=date_start"><?= TranslateHelper::t('SORT_BY_SCORE') ?></a></li>
                            <li><a href="/archiwum/?sort=date_start"><?= TranslateHelper::t('SORT_BY_INTEREST') ?></a></li>
                        </ul>
                    </div>
                    
                    <?php foreach($oProjectRowset as $oProject){ ?>
                        <div class="col-6 col-lg-3">
                            <div class="project" style="border-color: <?= $oProject->getCategory()->one()->color ?>">
                                <div class="project-img" <?php if($oProject->hasThumbnail()){ ?>style="background-image:url('<?= $oProject->getThumbnail(273, 183)  ?>')"<?php } ?>>
                                    <div class="col-6 no-padding">
                                        <div class="star" style="background-color: <?= $oProject->getCategory()->one()->color ?>">
                                            <img src="/img/ico/project_star.png" alt="gwiazdka"> <span><?= $oProject->getAverageIdeaScore(); ?></span>
                                        </div>
                                    </div>
                                    <div class="col-6 no-padding text-right">  
                                        <div class="category">
                                            <?php if($oProject->getCategory()->one()->getThumbnail()->one()){ ?>
                                                <img src="/upload/<?= $oProject->getCategory()->one()->getThumbnail()->one()->filename; ?>" style="max-width: 35px; background-color: <?= $oProject->getCategory()->one()->color ?>; box-shadow: 0px 0px 10px 15px <?= $oProject->getCategory()->one()->color ?>" alt="<?= $oProject->getCategory()->one()->getThumbnail()->one()->name; ?>">
                                            <?php }?>
                                        </div>
                                    </div>
                                    <div class="col-5 no-padding">
                                        <div class="description">
                                            <span><?= date("d.m.Y", strtotime($oProject->date_start)) ?></span>
                                        </div>
                                    </div>
                                    <div class="col-7 no-padding text-right">
                                        <div class="description">
                                            <span><?= $oProject->getProjectData()->one()->city; ?>, <?= $oProject->getProjectData()->one()->district; ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="project-txt">
                                    <h6><?= $oProject->getProjectData()->one()->name; ?></h6>
                                    <?= $oProject->getProjectData()->one()->short_description; ?>
                                    <div class="text-right read-more"><a href="/archiwum/<?= $oProject->getProjectData()->one()->slug; ?>">Czytaj więcej <span style="color: <?= $oProject->getCategory()->one()->color ?>">&#9658;</span> </a></div>   
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div> 
            <div class="col-2"></div>
        </div>
    </div>
</section>

<section id="around" class="list pr-pager" data-title="<?= TranslateHelper::t('PROJECT_SECTION_AREA') ?>">
    <div class="container-fluid">
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <div class="intro">
                    <div class="col-12">
                        <div class="marks">
                            <img src="/img/ico/star_mark.png" alt="gwiazdka" data-bottom=" -webkit-transform: rotate(-100deg); transform: rotate(-100deg);" data-center="-webkit-transform: rotate(0deg); transform: rotate(0deg);"> 
                        </div>
                        <h2><?= TranslateHelper::t('PROJECT_LIST_TITLE') ?></h2>
                        <div class='rectangle' data-bottom=" width:50px;" data-center=" width:74px;"></div>
                        <?= TranslateHelper::t('PROJECT_LIST_DESC') ?>
                    </div>
                </div>
                <div class="content">
                    <div class="col-12 text-center">
                        <p><b>Twoja lokalizacja: <span class="orange text-uppercase"><?= $aGeoData["name"] ?></span></b></p>
                        <ul><?= TranslateHelper::t('BY_RADIUS') ?>:	
                            <li <?php if(Yii::$app->request->get('radius', 5) == 5){ ?>class="active"<?php } ?>>
                                <a href="/archiwum/?radius=5#around">5<?= TranslateHelper::t('KM') ?></a>
                            </li>
                            <li <?php if(Yii::$app->request->get('radius', 5) == 10){ ?>class="active"<?php } ?>>
                                <a href="/archiwum/?radius=10#around">10<?= TranslateHelper::t('KM') ?></a>
                            </li>
                            <li <?php if(Yii::$app->request->get('radius', 5) == 15){ ?>class="active"<?php } ?>>
                                <a href="/archiwum/?radius=15#around">15<?= TranslateHelper::t('KM') ?></a>
                            </li>
                            <li <?php if(Yii::$app->request->get('radius', 5) == 20){ ?>class="active"<?php } ?>>
                                <a href="/archiwum/?radius=20#around">20<?= TranslateHelper::t('KM') ?></a>
                            </li>
                            <li <?php if(Yii::$app->request->get('radius', 5) == 30){ ?>class="active"<?php } ?>>
                                <a href="/archiwum/?radius=30#around">30<?= TranslateHelper::t('KM') ?></a>
                            </li>
                            <li <?php if(Yii::$app->request->get('radius', 5) == 50){ ?>class="active"<?php } ?>>
                                <a href="/archiwum/?radius=50#around">50<?= TranslateHelper::t('KM') ?></a>
                            </li>
                            <li <?php if(Yii::$app->request->get('radius', 5) == 100){ ?>class="active"<?php } ?>>
                                <a href="/archiwum/?radius=100#around">100<?= TranslateHelper::t('KM') ?></a>
                            </li>
                        </ul>
                    </div>
                    <?php foreach($oProjectInAreaRowset as $oProjectData){ $oProject = $oProjectData->getProject()->one(); ?>
                        <div class="col-6 col-lg-3">
                            <div class="project" style="border-color: <?= $oProject->getCategory()->one()->color ?>">
                                <div class="project-img" <?php if($oProject->hasThumbnail()){ ?>style="background-image:url('<?= $oProject->getThumbnail(273, 183)  ?>')"<?php } ?>>
                                    <div class="col-6 no-padding">
                                        <div class="star" style="background-color: <?= $oProject->getCategory()->one()->color ?>">
                                            <img src="/img/ico/project_star.png" alt="gwiazdka"> <span><?= $oProject->getAverageIdeaScore(); ?></span>
                                        </div>
                                    </div>
                                    <div class="col-6 no-padding text-right">  
                                        <div class="category">
                                            <?php if($oProject->getCategory()->one()->getThumbnail()->one()){ ?>
                                                <img src="/upload/<?= $oProject->getCategory()->one()->getThumbnail()->one()->filename; ?>" style="max-width: 35px; background-color: <?= $oProject->getCategory()->one()->color ?>; box-shadow: 0px 0px 10px 15px <?= $oProject->getCategory()->one()->color ?>" alt="<?= $oProject->getCategory()->one()->getThumbnail()->one()->name; ?>">
                                            <?php }?>
                                        </div>
                                    </div>
                                    <div class="col-5 no-padding">
                                        <div class="description">
                                            <span><?= date("d.m.Y", strtotime($oProject->date_start)) ?></span>
                                        </div>
                                    </div>
                                    <div class="col-7 no-padding text-right">
                                        <div class="description">
                                            <span><?= $oProject->getProjectData()->one()->city; ?>, <?= $oProject->getProjectData()->one()->district; ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="project-txt">
                                    <h6><?= $oProject->getProjectData()->one()->name; ?></h6>
                                    <?= $oProject->getProjectData()->one()->short_description; ?>
                                    <div class="text-right read-more"><a href="/archiwum/<?= $oProject->getProjectData()->one()->slug; ?>">Czytaj więcej <span style="color: <?= $oProject->getCategory()->one()->color ?>">&#9658;</span> </a></div>   
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div> 
            <div class="col-2"></div>
        </div>
    </div>
</section>

<section id="my_projects" class="list pr-pager" data-title="<?= TranslateHelper::t('PROJECT_SECTION_MY_PROJECTS') ?>">
    <div class="container-fluid">
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <?php if(count($oMineProjects) > 0){ ?>
                    <div class="intro">
                        <div class="col-12">
                            <div class="marks">
                                <img src="/img/ico/star_mark.png" alt="gwiazdka" data-bottom=" -webkit-transform: rotate(-100deg); transform: rotate(-100deg);" data-center="-webkit-transform: rotate(0deg); transform: rotate(0deg);"> 
                            </div>
                            <h2><?= TranslateHelper::t('PROJECT_LIST_TITLE') ?></h2>
                            <div class='rectangle' data-bottom=" width:50px;" data-center=" width:74px;"></div>
                            <?= TranslateHelper::t('PROJECT_LIST_DESC') ?>
                        </div>
                    </div>
                    <div class="content">
                        <?php foreach($oMineProjects as $oProject){ ?>
                            <div class="col-6 col-lg-3">
                                <div class="project" style="border-color: <?= $oProject->getCategory()->one()->color ?>">
                                    <div class="project-img" <?php if($oProject->hasThumbnail()){ ?>style="background-image:url('<?= $oProject->getThumbnail(273, 183)  ?>')"<?php } ?>>
                                        <div class="col-6 no-padding">
                                            <div class="star" style="background-color: <?= $oProject->getCategory()->one()->color ?>">
                                                <img src="/img/ico/project_star.png" alt="gwiazdka"> <span><?= $oProject->getAverageIdeaScore(); ?></span>
                                            </div>
                                        </div>
                                        <div class="col-6 no-padding text-right">  
                                            <div class="category">
                                                <?php if($oProject->getCategory()->one()->getThumbnail()->one()){ ?>
                                                    <img src="/upload/<?= $oProject->getCategory()->one()->getThumbnail()->one()->filename; ?>" style="max-width: 35px; background-color: <?= $oProject->getCategory()->one()->color ?>; box-shadow: 0px 0px 10px 15px <?= $oProject->getCategory()->one()->color ?>" alt="<?= $oProject->getCategory()->one()->getThumbnail()->one()->name; ?>">
                                                <?php }?>
                                            </div>
                                        </div>
                                        <div class="col-5 no-padding">
                                            <div class="description">
                                                <span><?= date("d.m.Y", strtotime($oProject->date_start)) ?></span>
                                            </div>
                                        </div>
                                        <div class="col-7 no-padding text-right">
                                            <div class="description">
                                                <span><?= $oProject->getProjectData()->one()->city; ?>, <?= $oProject->getProjectData()->one()->district; ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="project-txt">
                                        <h6><?= $oProject->getProjectData()->one()->name; ?></h6>
                                        <?= $oProject->getProjectData()->one()->short_description; ?>
                                        <div class="text-right read-more"><a href="/archiwum/<?= $oProject->getProjectData()->one()->slug; ?>">Czytaj więcej <span style="color: <?= $oProject->getCategory()->one()->color ?>">&#9658;</span> </a></div>   
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                <?php } else { ?>
                    <div class="intro">
                        <div class="col-8 col-offset-2">
                            <div class="marks">
                                <img src="/img/ico/star_mark.png" alt="gwiazdka" data-bottom=" -webkit-transform: rotate(-100deg); transform: rotate(-100deg);" data-center="-webkit-transform: rotate(0deg); transform: rotate(0deg);"> 
                            </div>
                            <h2>LISTA PROJEKTÓW</h2>
                            <div class='rectangle' data-bottom=" width:50px;" data-center=" width:74px;"></div>
                            <h4>Nie posiadasz jeszcze żadnych swoich projektów!</h4>
                            <p>Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor. Pellentesque facilisis.</p>
                        </div>
                        <div class="col-12">
                            <div class="button-div">   
                                <a href="index.php" class="btn-green button-list">ZALOGUJ SIĘ LUB ZAREJESTRUJ</a>
                            </div> 
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="col-2"></div>
        </div>
    </div>
</section>

<section id="im_in" class="list pr-pager" data-title="<?= TranslateHelper::t('PROJECT_SECTION_PARTICIPATE') ?>">
    <div class="container-fluid">
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <?php if(count($oMineProjects) > 0){ ?>
                    <div class="intro">
                        <div class="col-12">
                            <div class="marks">
                                <img src="/img/ico/star_mark.png" alt="gwiazdka" data-bottom=" -webkit-transform: rotate(-100deg); transform: rotate(-100deg);" data-center="-webkit-transform: rotate(0deg); transform: rotate(0deg);"> 
                            </div>
                            <h2><?= TranslateHelper::t('PROJECT_LIST_TITLE') ?></h2>
                            <div class='rectangle' data-bottom=" width:50px;" data-center=" width:74px;"></div>
                            <?= TranslateHelper::t('PROJECT_LIST_DESC') ?>
                        </div>
                    </div>
                    <div class="content">
                        <?php foreach($oMineProjects as $oProject){ ?>
                            <div class="col-6 col-lg-3">
                                <div class="project" style="border-color: <?= $oProject->getCategory()->one()->color ?>">
                                    <div class="project-img" <?php if($oProject->hasThumbnail()){ ?>style="background-image:url('<?= $oProject->getThumbnail(273, 183)  ?>')"<?php } ?>>
                                        <div class="col-6 no-padding">
                                            <div class="star" style="background-color: <?= $oProject->getCategory()->one()->color ?>">
                                                <img src="/img/ico/project_star.png" alt="gwiazdka"> <span><?= $oProject->getAverageIdeaScore(); ?></span>
                                            </div>
                                        </div>
                                        <div class="col-6 no-padding text-right">  
                                            <div class="category">
                                                <?php if($oProject->getCategory()->one()->getThumbnail()->one()){ ?>
                                                    <img src="/upload/<?= $oProject->getCategory()->one()->getThumbnail()->one()->filename; ?>" style="max-width: 35px; background-color: <?= $oProject->getCategory()->one()->color ?>; box-shadow: 0px 0px 10px 15px <?= $oProject->getCategory()->one()->color ?>" alt="<?= $oProject->getCategory()->one()->getThumbnail()->one()->name; ?>">
                                                <?php }?>
                                            </div>
                                        </div>
                                        <div class="col-5 no-padding">
                                            <div class="description">
                                                <span><?= date("d.m.Y", strtotime($oProject->date_start)) ?></span>
                                            </div>
                                        </div>
                                        <div class="col-7 no-padding text-right">
                                            <div class="description">
                                                <span><?= $oProject->getProjectData()->one()->city; ?>, <?= $oProject->getProjectData()->one()->district; ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="project-txt">
                                        <h6><?= $oProject->getProjectData()->one()->name; ?></h6>
                                        <?= $oProject->getProjectData()->one()->short_description; ?>
                                        <div class="text-right read-more"><a href="/archiwum/<?= $oProject->getProjectData()->one()->slug; ?>">Czytaj więcej <span style="color: <?= $oProject->getCategory()->one()->color ?>">&#9658;</span> </a></div>   
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                <?php } else { ?>
                    <div class="intro">
                        <div class="col-8 col-offset-2">
                            <div class="marks">
                                <img src="/img/ico/star_mark.png" alt="gwiazdka" data-bottom=" -webkit-transform: rotate(-100deg); transform: rotate(-100deg);" data-center="-webkit-transform: rotate(0deg); transform: rotate(0deg);"> 
                            </div>
                            <h2>LISTA PROJEKTÓW</h2>
                            <div class='rectangle' data-bottom=" width:50px;" data-center=" width:74px;"></div>
                            <h4>Nie posiadasz jeszcze żadnych swoich projektów!</h4>
                            <p>Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor. Pellentesque facilisis.</p>
                        </div>
                        <div class="col-12">
                            <div class="button-div">   
                                <a href="index.php" class="btn-green button-list">ZALOGUJ SIĘ LUB ZAREJESTRUJ</a>
                            </div> 
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="col-2"></div>
        </div>
    </div>
</section>

<section id="interested" class="list pr-pager" data-title="<?= TranslateHelper::t('PROJECT_SECTION_INTERESTED') ?>">
    <div class="container-fluid">
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <?php if(count($aMineInterested) > 0){ ?>
                    <div class="intro">
                        <div class="col-12">
                            <div class="marks">
                                <img src="/img/ico/star_mark.png" alt="gwiazdka" data-bottom=" -webkit-transform: rotate(-100deg); transform: rotate(-100deg);" data-center="-webkit-transform: rotate(0deg); transform: rotate(0deg);"> 
                            </div>
                            <h2><?= TranslateHelper::t('PROJECT_LIST_TITLE') ?></h2>
                            <div class='rectangle' data-bottom=" width:50px;" data-center=" width:74px;"></div>
                            <?= TranslateHelper::t('PROJECT_LIST_DESC') ?>
                        </div>
                    </div>
                    <div class="content">
                        <?php foreach($aMineInterested as $oProject){ ?>
                            <div class="col-6 col-lg-3">
                                <div class="project" style="border-color: <?= $oProject->getCategory()->one()->color ?>">
                                    <div class="project-img" <?php if($oProject->hasThumbnail()){ ?>style="background-image:url('<?= $oProject->getThumbnail(273, 183)  ?>')"<?php } ?>>
                                        <div class="col-6 no-padding">
                                            <div class="star" style="background-color: <?= $oProject->getCategory()->one()->color ?>">
                                                <img src="/img/ico/project_star.png" alt="gwiazdka"> <span><?= $oProject->getAverageIdeaScore(); ?></span>
                                            </div>
                                        </div>
                                        <div class="col-6 no-padding text-right">  
                                            <div class="category">
                                                <?php if($oProject->getCategory()->one()->getThumbnail()->one()){ ?>
                                                    <img src="/upload/<?= $oProject->getCategory()->one()->getThumbnail()->one()->filename; ?>" style="max-width: 35px; background-color: <?= $oProject->getCategory()->one()->color ?>; box-shadow: 0px 0px 10px 15px <?= $oProject->getCategory()->one()->color ?>" alt="<?= $oProject->getCategory()->one()->getThumbnail()->one()->name; ?>">
                                                <?php }?>
                                            </div>
                                        </div>
                                        <div class="col-5 no-padding">
                                            <div class="description">
                                                <span><?= date("d.m.Y", strtotime($oProject->date_start)) ?></span>
                                            </div>
                                        </div>
                                        <div class="col-7 no-padding text-right">
                                            <div class="description">
                                                <span><?= $oProject->getProjectData()->one()->city; ?>, <?= $oProject->getProjectData()->one()->district; ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="project-txt">
                                        <h6><?= $oProject->getProjectData()->one()->name; ?></h6>
                                        <?= $oProject->getProjectData()->one()->short_description; ?>
                                        <div class="text-right read-more"><a href="/archiwum/<?= $oProject->getProjectData()->one()->slug; ?>">Czytaj więcej <span style="color: <?= $oProject->getCategory()->one()->color ?>">&#9658;</span> </a></div>   
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                <?php } else { ?>
                    <div class="intro">
                        <div class="col-8 col-offset-2">
                            <div class="marks">
                                <img src="/img/ico/star_mark.png" alt="gwiazdka" data-bottom=" -webkit-transform: rotate(-100deg); transform: rotate(-100deg);" data-center="-webkit-transform: rotate(0deg); transform: rotate(0deg);"> 
                            </div>
                            <h2>LISTA PROJEKTÓW</h2>
                            <div class='rectangle' data-bottom=" width:50px;" data-center=" width:74px;"></div>
                            <h4>Nie posiadasz jeszcze żadnych swoich projektów!</h4>
                            <p>Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor. Pellentesque facilisis.</p>
                        </div>
                        <div class="col-12">
                            <div class="button-div">   
                                <a href="index.php" class="btn-green button-list">ZALOGUJ SIĘ LUB ZAREJESTRUJ</a>
                            </div> 
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="col-2"></div>
        </div>
    </div>
</section>