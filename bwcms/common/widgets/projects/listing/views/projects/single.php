<?php
    use common\helpers\TranslateHelper;
    
    $oProject = $oProjectData->getProject()->one();
    $oMainOrganizer = $oProject->getProjectOrganizers()->where(["=", "is_main", 1])->one()->getUser()->one();
?>

<div class="modal fade" id="modal_report" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-right">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">           
                        <img src="/img/ico/close_modal.png" alt="logo">
                    </span>
                </button>
            </div>
            <div class="modal-body text-center">
                <h2>Zgłoś nadużycie</h2>
                <div class="col-12 text-left no-padding">
                    <div class="input-group input-group-form senderino">
                        <select class="width-100 select-transparent" name="Abuse[topic]">
                            <option disabled="disabled" selected="selected">- Wybierz temat, którego dotyczy nadużycie -</option>
                            <option value="Nieodpowiednia treść">Nieodpowiednia treść</option>
                            <option value="Próba oszustwa">Próba oszustwa</option>
                            <option value="Jakaś trzecia opcja">Jakaś trzecia opcja</option>
                        </select>
                    </div>
                </div>
                <div class="col-12 no-padding">
                    <div class="input-group textarea-input">
                        <textarea name="Abuse[description]" class="form-control" placeholder="Treść wiadomości"></textarea>
                    </div>
                </div>
                <div class="col-12">
                    <div class="button-div">
                        <a href="/" class="btn-green button-list abuse-submit">Wyślij</a>
                    </div>
                </div>
                <br style="clear: both;"/>
            </div>
        </div>
    </div>
</div>

<section class='list single-event pr-pager noscrollevent' id='main' data-pid="<?= $oProject->id ?>" data-title="<?= TranslateHelper::t('SINGLE_PROJECT_MAIN') ?>">
    <div class="step-1">
        <div class="cont">
            <div class='container'>
                <div class='row'>
                    <div class='col-12'>
                        <div class="col-12 no-padding">
                            <div class="intro">
                                <div class="circles">
                                    <div class="col-6 col-md-4 col-lg-2">
                                        <div class="circle" >
                                            <?php if($oMainOrganizer->getData()->one()->getAvatar()->one()){ ?>
                                                <img class='img-responsive' src="/upload/<?= $oMainOrganizer->getData()->one()->getAvatar()->one()->filename ?>" alt="<?= $oMainOrganizer->getData()->one()->firstname  ?>">
                                            <?php } ?>
                                            <p> 
                                                <?= $oMainOrganizer->getData()->one()->firstname  ?>
                                                <span><?= $oMainOrganizer->getData()->one()->surname  ?></span>
                                            </p>
                                            <p class='role'><?= TranslateHelper::t('SINGLE_PROJECT_ORGANIZER') ?></p>
                                        </div>
                                    </div>
                                    <?php if($oMainOrganizer->getDataCompany()->one()){ ?>
                                        <div class="col-6  col-md-4 col-lg-2">
                                            <div class="circle" >
                                                <?php if($oMainOrganizer->getDataCompany()->one()->getAvatar()->one()){ ?>
                                                    <img class='img-responsive' src="/upload/<?= $oMainOrganizer->getDataCompany()->one()->getAvatar()->one()->filename ?>" alt="<?= $oMainOrganizer->getDataCompany()->one()->name ?>">
                                                <?php } ?>
                                                <p><?= $oMainOrganizer->getDataCompany()->one()->name ?></p> 
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <h3><?= $oProjectData->name; ?></h3>
                            <div class="flags">
                                <a href="index.php">
                                    <img class="img-dark" src="/img/ico/flag-pl.jpg" alt="flaga">
                                </a>
                                <a href="index.php">
                                    <img class="img-dark" src="/img/ico/flag-eng.jpg" alt="flaga">
                                </a>
                                <a href="index.php">
                                    <img class="img-dark" src="/img/ico/flag-n.jpg" alt="flaga">
                                </a>
                            </div>
                            <div class="rectangle"></div>
                            <div class="col-12 col-lg-8 " style="padding-left:0px;">
                                <p>
                                    <b><?= $oProjectData->city; ?>, <?= $oProjectData->district; ?>, <?= $oProjectData->street; ?> </b>
                                    <a href="#project-local" class="orange">(Pokaż lokalizacje na mapie)</a>
                                </p>
                                <p>Data rozpoczęcia: <?= date("d.m.Y", strtotime($oProject->date_start)); ?></p>
                                <p>Data zakończenia: <?= date("d.m.Y", strtotime($oProject->date_end)); ?></p>
                                <div class="content-text ">
                                    <?= $oProjectData->full_description; ?>
                                    <?php if($oProject->type == "collection"){ ?>
                                        <div class="part-1">
                                            <div class="col-12 no-padding"  style='margin-top:30px;'> 
                                                <a href="index.php" class="btn-green button-list" >
                                                    ZŁÓŻ DOTACJĘ
                                                    <img src="/img/ico/money.png" alt="pieniądze" style="margin:-2px 0 0 7px;">
                                                </a>
                                                <p class='counter' style="margin-top:5px;"> 
                                                    zebrano
                                                    <span class='green'>75,054 zł</span>
                                                    (ostatnia aktualizacja:
                                                    <span class='green'>
                                                        25.10.2016
                                                    </span>)
                                                </p>
                                            </div>  
                                        </div> 
                                    <?php } ?>
                                    <div class="col-12 no-padding"  style='margin-top:30px;'>  
                                        <a  class="btn-green button-list want-realize" >
                                            CHCĘ WZIĄĆ UDZIAŁ W REALIZACJI PROJEKTU
                                        </a>
                                        <p class='counter'>
                                            <span class='green'><?= count($oProject->getProjectOrganizers()->all()) ?></span> uczestników
                                        </p>
                                    </div>  
                                    <div class="col-12 no-padding " style='margin-top:20px;'>    
                                        <a href="/" class="btn-yellow button-list" id="button-interested" >JESTEM ZAINTERESOWANY</a>
                                        <p class='counter'>
                                            <span class='orange'><?= count($oProject->getProjectSignups()->all()) ?></span> zainteresowanych
                                        </p>
                                    </div>  
                                    <div class="part-2">
                                        <div class='col-12 no-padding'>
                                            <div class='single-description'>
                                                <p>Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor. Pellentesque facilisis. This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.</p>
                                            </div>
                                            <div class="col-12 no-padding " style='margin-top:20px;'>    
                                                <a href="/" class="btn-green button-list" id="join-organizer" >
                                                    TAK, CHCĘ ZOSTAĆ WSPÓŁ<?= TranslateHelper::t('SINGLE_PROJECT_ORGANIZER') ?>EM
                                                </a>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4" style="padding-left:0px;">
                                <div class="right-side" >
                                    <h6>Ocena pomysłu:</h6>
                                    <div class="star">
                                        <img src="/img/ico/project_star_big.png" alt="gwiazdka">
                                        <span><?= $oProject->getAverageIdeaScore() ?></span>
                                    </div>
                                    <p><?= $oProject->getAmount() ?> głosy</p>
                                    <h6>Oceń pomysł:</h6>
                                    <div class="rates" id="ideaScore"></div>
                                    <?php if($oProject->hasThumbnail()){ ?><div class="photo-event" style="background-image: url('<?= $oProject->getThumbnail(412, 267) ?>')"></div><?php } ?>
                                    <div class='share'>
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                          var js, fjs = d.getElementsByTagName(s)[0];
                                          if (d.getElementById(id)) return;
                                          js = d.createElement(s); js.id = id;
                                          js.src = "//connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.8&appId=122583028206324";
                                          fjs.parentNode.insertBefore(js, fjs);
                                        }(document, 'script', 'facebook-jssdk'));</script>
                                        <div class="fb-share-button" data-href="<?= Yii::$app->request->absoluteUrl ?>" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Udostępnij</a></div>
                                    </div>
                                    <div class="warn">
                                        <a data-toggle="modal" data-target="#modal_report">Zgłoś nadużycie 
                                            <img src="/img/ico/warning.png" alt="ikonka">
                                        </a>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id='members' class='single-event list pr-pager' data-title="<?= TranslateHelper::t('SINGLE_PROJECT_MEMBERS') ?>">
    <div class='container'>
        <div class='row'>   
            <div class='col-12'>
                <div class='col-12 col-md-8'>
                    <div class="content">
                        <h3><?= $oProjectData->name; ?></h3>
                        <div class="rectangle"></div>
                        <div class="col-12 col-lg-8 " style="padding-left:0px;">
                            <p >
                                <b><?= $oProjectData->city; ?>, <?= $oProjectData->district; ?>, <?= $oProjectData->street; ?></b>
                            </p>
                            <p>Data rozpoczęcia: <?= date("d.m.Y", strtotime($oProject->date_start)); ?></p>
                            <p>Data zakończenia: <?= date("d.m.Y", strtotime($oProject->date_end)); ?></p>
                        </div>
                    </div>
                </div>   
                <div class='col-12 col-md-4'>
                    <div class="circles">
                        <div class="col-6 col-sm-3 col-md-6">
                            <div class="circle" >
                                <img class='img-responsive' src="/upload/<?= $oMainOrganizer->getData()->one()->getAvatar()->one()->filename ?>" alt="<?= $oMainOrganizer->getData()->one()->firstname  ?>">
                                <p> 
                                    <?= $oMainOrganizer->getData()->one()->firstname  ?>
                                    <span><?= $oMainOrganizer->getData()->one()->surname  ?></span>
                                </p>
                                <p class='role'><?= TranslateHelper::t('SINGLE_PROJECT_ORGANIZER') ?></p>
                            </div>
                        </div>
                        <?php if($oMainOrganizer->getDataCompany()->one()){ ?>
                            <div class="col-6 col-sm-3 col-md-6">
                                <div class="circle" >
                                    <img class='img-responsive' src="/upload/<?= $oMainOrganizer->getDataCompany()->one()->getAvatar()->one()->filename ?>" alt="<?= $oMainOrganizer->getDataCompany()->one()->name ?>">
                                    <p><?= $oMainOrganizer->getDataCompany()->one()->name ?></p> 
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class='col-12 no-padding'>
                    <div class="intro">
                        <div class='marks'>
                            <img src="/img/ico/person_mark.png" alt="osoba">
                        </div>
                        <h2>LISTA UCZESTNIKÓW</h2>
                        <div class="rectangle"></div>
                    </div>
                    <div class='members-list'>
                        <?php foreach($oProject->getProjectOrganizers()->where(["!=", "is_main", 1])->all() as $oOrganizer){ ?>
                            <div class='col-12 col-sm-6 col-lg-3'>
                                <div class='member-single'>
                                    <div class='photo' style="background-image: url('/img/examples/projects-person-1.png');"></div>
                                    <div class='text'>
                                        <p><?= $oOrganizer->getUser()->one()->getData()->one()->firstname ?> <span><?= $oOrganizer->getUser()->one()->getData()->one()->surname ?></span></p>
                                        <?php if($oOrganizer->getUser()->one()->getDataCompany()->one()){ ?>
                                            <p class='role'><?= $oOrganizer->getUser()->one()->getDataCompany()->one()->name ?></p>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>        
                </div>
            </div>
        </div>
    </div>
</section>

<section id='table' class='single-event list pr-pager' data-title="<?= TranslateHelper::t('SINGLE_PROJECT_TABLE') ?>">
    <div class='container'>
        <div class='row'> 
            <div class='col-12'>
                <div class='col-12 col-md-8'>
                    <div class="content">
                        <h3><?= $oProjectData->name; ?></h3>
                        <div class="rectangle"></div>
                        <div class="col-12 col-lg-8 " style="padding-left:0px;">
                            <p >
                                <b><?= $oProjectData->city; ?>, <?= $oProjectData->district; ?>, <?= $oProjectData->street; ?></b>
                            </p>
                            <p>Data rozpoczęcia: <?= date("d.m.Y", strtotime($oProject->date_start)); ?></p>
                            <p>Data zakończenia: <?= date("d.m.Y", strtotime($oProject->date_end)); ?></p>
                        </div>
                    </div>
                </div>   
                <div class='col-12 col-md-4'>
                    <div class="circles">
                        <div class="col-6 col-sm-3 col-md-6">
                            <div class="circle" >
                                <img class='img-responsive' src="/upload/<?= $oMainOrganizer->getData()->one()->getAvatar()->one()->filename ?>" alt="<?= $oMainOrganizer->getData()->one()->firstname  ?>">
                                <p> 
                                    <?= $oMainOrganizer->getData()->one()->firstname  ?>
                                    <span><?= $oMainOrganizer->getData()->one()->surname  ?></span>
                                </p>
                                <p class='role'><?= TranslateHelper::t('SINGLE_PROJECT_ORGANIZER') ?></p>
                            </div>
                        </div>
                        <?php if($oMainOrganizer->getDataCompany()->one()){ ?>
                            <div class="col-6 col-sm-3 col-md-6">
                                <div class="circle" >
                                    <img class='img-responsive' src="/upload/<?= $oMainOrganizer->getDataCompany()->one()->getAvatar()->one()->filename ?>" alt="<?= $oMainOrganizer->getDataCompany()->one()->name ?>">
                                    <p><?= $oMainOrganizer->getDataCompany()->one()->name ?></p> 
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class='col-12 no-padding'>
                    <div class="intro">
                        <div class='marks'>
                            <img src="/img/ico/table_mark.png" alt="tabelka" >
                        </div>
                        <h2>TABLICA PROJEKTU</h2>
                        <div class="rectangle"></div>
                    </div>
                    <div class="content">
                        <div class="text-center">
                            <ul>Rok:	
                                <li class="active"><a href="#">2017</a></li>
                            </ul>
                        </div>
                        <div class="event-texter">
                            <?php foreach($oProject->getTimeline()->all() as $oTimeline){ ?>
                                <div class="row">
                                    <div class="col-12 no-padding">
                                        <div class="col-4 col-md-3 col-lg-2">
                                            <?php if($oTimeline->getOrganizer()->one()->getUser()->one()->getData()->one()->getAvatar()->one()){ ?>
                                                <div class="t-left" style="background-image:url('/upload/<?= $oTimeline->getOrganizer()->one()->getUser()->one()->getData()->one()->getAvatar()->one()->filename ?>')"></div>
                                            <?php } ?>
                                        </div>
                                        <div class="col-12 col-lg-10">
                                            <div class="t-right">
                                                <p style="margin-right:15px;"><?= date("d.m.Y", strtotime($oTimeline->date)); ?></p>
                                                <p><?= date("H:i", strtotime($oTimeline->date)); ?></p>
                                                <div class="col-12 no-padding">  
                                                    <h4>
                                                        <?= $oTimeline->getOrganizer()->one()->getUser()->one()->getData()->one()->firstname ?> 
                                                        <?= $oTimeline->getOrganizer()->one()->getUser()->one()->getData()->one()->surname ?>
                                                        <?php if($oTimeline->getOrganizer()->one()->getUser()->one()->getDataCompany()->one()){ ?>
                                                        - <?= $oTimeline->getOrganizer()->one()->getUser()->one()->getDataCompany()->one()->name ?>
                                                        <?php } ?>
                                                    </h4> 
                                                    <img class="starr" src="/img/ico/star-on.png" alt="gwiazdka">
                                                    <div class="rectangle"></div>
                                                </div>

                                                <div class="col-12 no-padding">
                                                    <?= $oTimeline->description ?>
                                                </div>
                                                <?php if($oTimeline->getGallery()->one()){ ?>
                                                    <div class="col-12 no-padding">
                                                        <div class="photos">
                                                            <?php foreach($oTimeline->getGallery()->one()->galleryItems as $image){ ?>
                                                                <?php if($image->crop){ ?>
                                                                    <img class="img-light" src="/app-upload-files/<?php echo $image->crop->filename; ?>" alt="<?php echo $image->alt; ?>">
                                                                <?php } else { ?>
                                                                    <img class="img-light" src="/app-upload-files/<?php echo $image->file->filename; ?>" alt="<?php echo $image->alt; ?>">
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class='col-12'>
                                        <div class='long-rectangle'></div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="project-local" class='single-event pr-pager sec-padding-only sec-photos' data-title="<?= TranslateHelper::t('SINGLE_PROJECT_LOCALITY') ?>">
    <div class="container">
        <div class="row">
            <div class="col-12 no-padding">
                <div class="intro">
                    <div class="circles">
                        <div class="col-6 col-md-4 col-lg-2">
                            <div class="circle">
                                <img class='img-responsive' src="/upload/<?= $oMainOrganizer->getData()->one()->getAvatar()->one()->filename ?>" alt="<?= $oMainOrganizer->getData()->one()->firstname  ?>">
                                <p> 
                                    <?= $oMainOrganizer->getData()->one()->firstname  ?>
                                    <span><?= $oMainOrganizer->getData()->one()->surname  ?></span>
                                </p>
                                <p class='role'><?= TranslateHelper::t('SINGLE_PROJECT_ORGANIZER') ?></p>
                            </div>
                        </div>
                        <?php if($oMainOrganizer->getDataCompany()->one()){ ?>
                            <div class="col-6  col-md-4 col-lg-2">
                                <div class="circle">
                                    <img class='img-responsive' src="/upload/<?= $oMainOrganizer->getDataCompany()->one()->getAvatar()->one()->filename ?>" alt="<?= $oMainOrganizer->getDataCompany()->one()->name ?>">
                                    <p><?= $oMainOrganizer->getDataCompany()->one()->name ?></p> 
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="content">
                <h3><?= $oProjectData->name; ?></h3>
                <div class="flags">
                    <a href="index.php">
                        <img class="img-dark" src="/img/ico/flag-pl.jpg" alt="flaga">
                    </a>
                    <a href="index.php">
                        <img class="img-dark" src="/img/ico/flag-eng.jpg" alt="flaga">
                    </a>
                    <a href="index.php">
                        <img class="img-dark" src="/img/ico/flag-n.jpg" alt="flaga">
                    </a>
                </div>
                <div class="rectangle"></div>
                <div class="col-12 col-lg-8 " style="padding-left:0px;">
                    <p >
                        <b><?= $oProjectData->city; ?>, <?= $oProjectData->district; ?>, <?= $oProjectData->street; ?></b>
                    </p>
                    <p>Data rozpoczęcia: <?= date("d.m.Y", strtotime($oProject->date_start)); ?></p>
                    <p>Data zakończenia: <?= date("d.m.Y", strtotime($oProject->date_end)); ?></p>

                </div>
            </div>
            <div class="col-12 no-padding">
                <h6 class="orange">Lokalizacja na mapie:</h6>
            </div>
            <div class="col-12">
                <div id="map_projects"></div>
            </div>
        </div>
    </div>
</section>

<section id="project-contact" class='contacts pr-pager' data-title="<?= TranslateHelper::t('SINGLE_PROJECT_CONTACT') ?>">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4">
                <h1>KONTAKT</h1>
                <div class="rectangle" data-bottom=" width:50px;" data-center=" width:74px;"></div>
                <h4>Administracja portalu Lokalnie Aktywni</h4>
                <h6>Telefon: 
                    <b>(22) 654 52 55, 548 657 556</b>
                </h6>
                <h6>E-mail: 
                    <b> 
                        <a href="mailto:administracja@lokalnieaktywni.pl">administracja@lokalnieaktywni.pl</a>
                    </b>
                </h6>
                <h4>Formularz kontaktowy</h4>
                <div class="col-12 no-padding">
                    <span class="input input--ruri">
                        <input class="input__field input__field--ruri" type="text" id="input-28" />
                        <label class="input__label input__label--ruri" for="input-28">
                            <span class="input__label-content input__label-content--ruri">Imię i nazwisko</span>
                        </label>
                    </span>
                </div> 
                <div class="col-12 no-padding">
                    <span class="input input--ruri">
                        <input class="input__field input__field--ruri" type="text" id="input-28" />
                        <label class="input__label input__label--ruri" for="input-28">
                            <span class="input__label-content input__label-content--ruri">Adres E-mail</span>
                        </label>
                    </span>
                </div> 
                <div class="col-12 no-padding">
                    <span class="input input--ruri">
                        <input class="input__field input__field--ruri" type="text" id="input-28" />
                        <label class="input__label input__label--ruri" for="input-28">
                            <span class="input__label-content input__label-content--ruri">Temat</span>
                        </label>
                    </span>
                </div> 
                <div class="col-12 no-padding">
                    <div class="input-group textarea-input">
                        <textarea name="text" class="form-control" placeholder="Treść wiadomości"></textarea>
                    </div>
                </div>
                <div class="col-12">
                    <div class="send">
                        <div class="col-12 col-md-2 checker">
                            <p>
                                <input type="checkbox" id="test2" />
                                <label for="test2"></label>
                            </p>
                        </div>
                        <div class=" col-12 col-md-10">
                            <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
                            <div class="button-div" >   
                                <a href="index.php" class="btn-green button-list">
                                    WYŚLIJ
                                </a>
                            </div> 
                        </div>
                    </div>
                </div>                    
            </div>
            
            <div class="col-12 col-md-3 col-lg-2">
                <div class="contact-ico" style="background-image:url('/upload/<?= $oMainOrganizer->getData()->one()->getAvatar()->one()->filename ?>'); background-size: cover;">
                    <?php if($oMainOrganizer->getDataCompany()->one()){ ?>
                        <img src="/upload/<?= $oMainOrganizer->getDataCompany()->one()->getAvatar()->one()->filename ?>" style="max-height: 150px; max-width: 150px;" alt="<?= $oMainOrganizer->getDataCompany()->one()->name ?>" >
                    <?php } ?>
                </div>
            </div>
            
            <div class="col-2"></div>
        </div>
    </div>
</section>

<section id="gallery" class='single-event sec-photos sec-padding-only pr-pager' data-title="<?= TranslateHelper::t('SINGLE_PROJECT_GALLERY') ?>">
    <div class="container">
        <div class="row">
            <div class="col-12 no-padding">
                <div class="intro">
                    <div class="circles">
                        <div class="col-6 col-md-4 col-lg-2">
                            <div class="circle">
                                <img class='img-responsive' src="/upload/<?= $oMainOrganizer->getData()->one()->getAvatar()->one()->filename ?>" alt="<?= $oMainOrganizer->getData()->one()->firstname  ?>">
                                <p> <?= $oMainOrganizer->getData()->one()->firstname  ?>
                                    <span><?= $oMainOrganizer->getData()->one()->surname  ?></span>
                                </p>
                                <p class="role"><?= TranslateHelper::t('SINGLE_PROJECT_ORGANIZER') ?></p>
                            </div>
                        </div>
                        <?php if($oMainOrganizer->getDataCompany()->one()){ ?>
                            <div class="col-6  col-md-4 col-lg-2">
                                <div class="circle">
                                    <img class='img-responsive' src="/upload/<?= $oMainOrganizer->getDataCompany()->one()->getAvatar()->one()->filename ?>" alt="<?= $oMainOrganizer->getDataCompany()->one()->name ?>">
                                    <p><?= $oMainOrganizer->getDataCompany()->one()->name ?></p> 
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="content">
                    <h3><?= $oProjectData->name; ?></h3> 
                    <div class="flags">
                        <a href="index.php">
                            <img class="img-dark" src="/img/ico/flag-pl.jpg" alt="flaga">
                        </a>
                        <a href="index.php">
                            <img class="img-dark" src="/img/ico/flag-eng.jpg" alt="flaga">
                        </a>
                        <a href="index.php">
                            <img class="img-dark" src="/img/ico/flag-n.jpg" alt="flaga">
                        </a>
                    </div>
                    <div class='rectangle' data-bottom=" width:50px;" data-center=" width:74px;"></div>
                    <?php if($oProject->getGallery()->one()){ ?>
                        <div class="col-12 no-padding">
                            <h6><?= $oProject->getGallery()->one()->name ?></h6>
                            <div class='photos'>
                                <?php foreach($oTimeline->getGallery()->one()->galleryItems as $image){ ?>
                                    <?php if($image->crop){ ?>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <div class="photo" style="background-image:url('/app-upload-files/<?php echo $image->crop->filename; ?>')">
                                                <div class='photo-person'></div>
                                            </div>
                                        </div>
                                    <?php } else { ?>
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <div class="photo" style="background-image:url('/app-upload-files/<?php echo $image->file->filename; ?>')">
                                                <div class='photo-person'></div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>

</section>