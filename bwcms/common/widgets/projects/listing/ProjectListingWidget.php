<?php
/**
 * Widget HTML
 * 
 * PHP version 7
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @copyright (c) 2016 Krzysztof Borecki
 * @version 1.0
 */

namespace common\widgets\projects\listing;

use Yii;
use yii\data\Pagination;
use common\modules\users\models\User;
use common\modules\projects\models\Project;
use common\modules\projects\models\ProjectAds;
use common\modules\projects\models\ProjectData;
use common\modules\projects\models\ProjectSignup;
use common\modules\projects\models\ProjectOrganizers;


class ProjectListingWidget extends \common\hooks\yii2\bootstrap\Widget
{
    public $widget_item_id, $category_id, $layout, $limit, $order, $isArchive;
    private $slug;

    /**
     * Build widget
     */
    public function init()
    {
        if(array_key_exists("slug", Yii::$app->params)){
            $this->slug = Yii::$app->params['slug'];
        }

        parent::init();
    }

    
    /**
     * Run widget
     */
    public function run()
    {
        if($this->slug){
            return $this->displaySingle();
        } else {
            return $this->displayList();
        }
    }
    
    private function displayList(){
        $mode = "list";
        $iLimit = Yii::$app->request->get('limit', $this->md_limit);

        $oProjectRowsetQuery = Project::find();
        $oProjectRowsetQuery->where(['=', "is_active", 1]);
        if($this->category_id){
            $oProjectRowsetQuery->where(['=', "category_id", $this->category_id]);
        }
        if(!$this->isArchive){
            $oProjectRowsetQuery->where(['>=', "date_start", date("Y-m-d H:i:s")]);
        } else {
            $oProjectRowsetQuery->where(['<', "date_start", date("Y-m-d H:i:s")]);
        }
        $oProjectRowsetQuery->orderBy($this->order);
        
        $pageUrl = explode("?", Yii::$app->request->url);
        if(count($pageUrl) > 0){ $pageUrl = $pageUrl[0]; } else { $pageUrl = Yii::$app->request->url; }
        
        $sCountQuery = clone $oProjectRowsetQuery;
        $pages = new Pagination([
            'totalCount' => $sCountQuery->count(), 
            'pageSize' => $iLimit, 
            'pageSizeParam' => 'limit',
            'route' => $pageUrl
        ]);
        
        $oProjectInAreaRowset = [];
        $aMineProjects = [];
        $aMineOrganized = [];
        $aMineInterested = [];
        
        $oProjectRowset = $oProjectRowsetQuery->offset($pages->offset)->limit($pages->limit)->all();
        $aGeoData = Yii::$app->request->cookies->getValue('aGeoData', ["lat" => "52.3217858", "long" => "20.9757926", "name" => "none"]);
        
        $oUser = Yii::$app->session->get('oUser');
        if(!is_null($oUser)){
            $oData = $oUser->getData()->one();

            $aCoords  = explode(",", $this->getCoordinates("{$oData->city} {$oData->district} {$oData->street}"));
            $aGeoData = ["lat" => $aCoords[0], "long" => $aCoords[1], "name" => "{$oData->city}, {$oData->street}"];
        }
        
        if($this->layout == "projects"){
            $oProjectInAreaRowset = $this->getProjectInArea($aGeoData["lat"], $aGeoData["long"], Yii::$app->request->get('radius', 5));

            if(isset(Yii::$app->user->identity->id)){
                foreach($oProjectRowset as $oProj){
                    foreach($oProj->getProjectOrganizers()->where(["=", "is_main", 1])->all() as $oOrg){
                        if($oOrg->user_id == Yii::$app->user->identity->id){
                            array_push($aMineProjects, $oProj);
                        }
                    }
                }
                
                foreach(ProjectOrganizers::find()->where(["=", "user_id", Yii::$app->user->identity->id])->all() as $oOrg){
                    array_push($aMineOrganized, $oOrg->getProject()->one());
                }
                
                foreach(ProjectSignup::find()->where(["=", "user_id", Yii::$app->user->identity->id])->all() as $oSign){
                    array_push($aMineOrganized, $oSign->getProject()->one());
                }
            }
        }
        
        $oBannerAds  = ProjectAds::find()->where(["=", "type", 0])->andWhere([">", "date_expire", date("Y-m-d H:i:s")])->orderBy("points DESC")->all();
        $oContentAds = ProjectAds::find()->where(["=", "type", 1])->andWhere([">", "date_expire", date("Y-m-d H:i:s")])->orderBy("points DESC")->all();

        return $this->display([
            'limit' => $iLimit,
            'pages' => $pages,
            'aGeoData' => $aGeoData,
            'oProjectRowset' => $oProjectRowset,
            'oProjectInAreaRowset' => $oProjectInAreaRowset,
            'oMineProjects' => $aMineProjects,
            'aMineOrganized' => $aMineOrganized,
            'aMineInterested' => $aMineInterested,
            'oBannerAds' => $oBannerAds,
            'oContentAds' => $oContentAds
        ], $mode);
    }
    
    private function displaySingle(){
        $oProjectData = ProjectData::find()->where(['and', "slug = '{$this->slug}'"])->one();
        
        // Check if there is any article
        if(!$oProjectData){ return; }
        
        if(Yii::$app->request->get('addorganizer')){
            $oUser = User::find()->where(["=", "md5(`created_at`)", Yii::$app->request->get('addorganizer')])->one();
            
            $oCheckOrganizer = ProjectOrganizers::find()->where(["=", "project_id", $oProjectData->getProject()->one()->id])->andWhere(["=", "user_id", $oUser->id])->one();
            if(is_null($oCheckOrganizer)){
                $oOrganizer = new ProjectOrganizers();
                $oOrganizer->project_id = $oProjectData->getProject()->one()->id;
                $oOrganizer->user_id = $oUser->id;
                $oOrganizer->is_main = 0;
                $oOrganizer->save();
            }
        }
        
        return $this->display([
            'oProjectData' => $oProjectData
        ], "single");
    }

    private function getProjectInArea($userLat, $userLong, $radius){
        $iLimit = Yii::$app->request->get('limit', $this->md_limit);
        
        $oProjectRowsetQuery = ProjectData::find()->select('xmod_projects_data.*, (6371 * acos(cos(radians('.$userLat.')) * cos(radians(map_lat)) * cos(radians('.$userLong.') - radians(map_long)) + sin(radians('.$userLat.')) * sin(radians(map_lat)))) AS distance ');
        $oProjectRowsetQuery->joinWith('project');
        
        $oProjectRowsetQuery->where(['=', "is_active", 1]);
        $oProjectRowsetQuery->where(['<>', "map_lat", '']);
        $oProjectRowsetQuery->where(['<>', "map_long", '']);
        if($this->category_id){
            $oProjectRowsetQuery->where(['=', "category_id", $this->category_id]);
        }
        if(!$this->isArchive){
            $oProjectRowsetQuery->where(['>=', "date_start", date("Y-m-d H:i:s")]);
        } else {
            $oProjectRowsetQuery->where(['<', "date_start", date("Y-m-d H:i:s")]);
        }
        $oProjectRowsetQuery->having("distance < {$radius}");
        
        $oProjectRowsetQuery->orderBy("distance ASC");
        
        $pageUrl = explode("?", Yii::$app->request->url);
        if(count($pageUrl) > 0){ $pageUrl = $pageUrl[0]; } else { $pageUrl = Yii::$app->request->url; }
        
        $sCountQuery = clone $oProjectRowsetQuery;
        $pages = new Pagination([
            'totalCount' => $sCountQuery->count(), 
            'pageSize' => $iLimit, 
            'pageSizeParam' => 'limit',
            'route' => $pageUrl
        ]);
        
        return $oProjectRowsetQuery->offset($pages->offset)->limit($pages->limit)->all();
    }
    
    private function getCoordinates($address){
        $address = str_replace(" ", "+", $address);

        $json = json_decode(file_get_contents("http://maps.google.com/maps/api/geocode/json?sensor=false&address=$address"),TRUE);
        return ($json['results'][0]['geometry']['location']['lat'].",".$json['results'][0]['geometry']['location']['lng']);
    }

    
}
