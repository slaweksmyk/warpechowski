<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\modules\projects\models\ProjectCategory;

?>

<div class="widget-main-form" >

    <!-- ADD WIDGET CUSTOM DATA -->
    <div class="row" style="background: #f8f8f8; padding-top: 15px; padding-bottom: 15px; " >
        <div class="col-3">
            <?= $form->field($model, 'data[category_id]')->dropDownList(ArrayHelper::map(ProjectCategory::find()->all(), 'id', 'name'), ["prompt" => "- wybierz kategorie -"]) ?>
        </div>
        <div class="col-3" >
            <?php echo $form->field($model, 'data[limit]')->textInput(["type" => "number"])->label("Limit"); ?>
        </div>
        <div class="col-3">
            <?php 
                echo $form->field($model, 'data[order]')->dropDownList(["sort ASC" => "Kolejności rosnąco", "sort DESC" => "Kolejności malejąco", "date_start ASC" => "Data rosnąco", "date_start DESC" => "Data malejąco", "title ASC" => "Alfabetycznie A-Z", "title DESC" => "Alfabetycznie Z-A", "id ASC" => "Kolejności dodania - rosnąco", "id DESC" => "Kolejności dodania - malejąco", "RAND()" => "Losowo"])->label("Sortowanie");
            ?>
        </div>
        <div class="col-3">
            <?php 
                echo $form->field($model, 'data[isArchive]')->dropDownList([0 => "Nie", 1 => "Tak"])->label("Czy archiwum");
            ?>
        </div>
        
    </div>
    <!-- END CUSTOM DATA -->
    
    <br>
    <div class="form-group">
        <?= Html::submitButton( Yii::t('backend', 'update'), ['class' => 'btn btn-primary']) ?>
    </div>

</div>