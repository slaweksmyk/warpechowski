<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\newsletter\models\NewsletterGroup;

?>

<div class="widget-main-form" >
    
    <!-- ADD WIDGET CUSTOM DATA -->
    <div class="row" style="background: #f8f8f8; padding-top: 15px; padding-bottom: 15px; " >
        <div class="col-6">
            <?php 
                $aGroups = [];
                foreach(NewsletterGroup::find()->all() as $oGroup){
                    $aGroups[$oGroup->id] = $oGroup->name;
                }
                echo $form->field($model, 'data[group_id]')->dropDownList($aGroups)->label( Yii::t('backend_module_newsletter', 'Group'));
            ?>
        </div>
    </div>
    <!-- END CUSTOM DATA -->
    
    <br>
    <div class="form-group">
        <?= Html::submitButton( Yii::t('backend', 'update'), ['class' => 'btn btn-primary']) ?>
    </div>

</div>