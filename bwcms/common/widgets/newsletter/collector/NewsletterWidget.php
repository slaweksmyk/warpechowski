<?php
/**
 * Widget Newsletter
 * 
 * PHP version 7
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @copyright (c) 2016 Krzysztof Borecki
 * @version 1.0
 */

namespace common\widgets\newsletter\collector;

use Yii;
use yii\helpers\BaseUrl;
use common\modules\newsletter\models\NewsletterEmail;
use common\modules\newsletter\models\NewsletterGroup;

class NewsletterWidget extends \common\hooks\yii2\bootstrap\Widget
{
    
    public $widget_item_id;
    public $group_id;
    

    /**
     * Build widget
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Run widget
     */
    public function run()
    {
        $request = Yii::$app->request;
        
        if($request->isGet && isset($request->get()["hash"])){
            $this->activateEmail($request->get());
        }
        
        return $this->showWidget();
    }
    
    /*
     * Display widget
     */
    private function showWidget()
    {
        $oEmail = new NewsletterEmail();
        
        if(Yii::$app->request->isPost && $oEmail->load(Yii::$app->request->post()) && $oEmail->validate()){
            $this->submitConfirmRequest($oEmail);
        }

        return $this->display([
            'model' => $oEmail,
            'group_id' => $this->group_id,
            'widget_item_id' => $this->widget_item_id,
            'oGroupRowset' => NewsletterGroup::find()->all()
        ]);
    }
    
    /*
     * Submits e-mail with e-mail confirmation
     */
    private function submitConfirmRequest($oEmail){
        if(is_null(NewsletterEmail::find()->where(["email" => $oEmail->email])->one())){
            $hash = substr(md5(rand()), 0, 10);

            $oEmail->hash = $hash;
            $oEmail->is_active = 0;
            $oEmail->is_accept_rules = 1;
            $oEmail->save();

            $sUrl = BaseUrl::base(true)."?hash={$hash}";

            $sBody = "Dziękujemy za zapisanie się do newslettera, prosimy potwierdzić adres email za pomocą poniższego linku:<br/><br/>";
            $sBody .= "<a href='{$sUrl}'>link potwierdzający adres email</a>";

            Yii::$app->mailer->compose()
                ->setTo($oEmail->email)
                ->setSubject('[Newsletter] Potwierdzenie adresu email')
                ->setHtmlBody($sBody)
                ->send();
        }
    }
    
    /*
     * Activate newsletter email
     */
    private function activateEmail($get){
        $oEmail = NewsletterEmail::find()->where(["hash" => $get["hash"]])->one();
        if(!is_null($oEmail)){
            $oEmail->is_active = 1;
            $oEmail->save();
        }
    }
    
    
}
