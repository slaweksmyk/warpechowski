<?php
    use yii\widgets\ActiveForm;
?>

<div class="modal" id="newsletter-sent" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header d-flex justify-content-center">
                <h2 class="text-center">Dziękujemy</h2>
            </div>
            <div class="modal-body text-center">
                <h4>Na Twój adres została wysłana wiadomość.</h4>
            </div>
            <div class="modal-footer justify-center">
                <button type="button" class="button btn-outline btn-magenta" data-dismiss="modal">Zamknij
                    <i class="i-exit-magenta icon"></i>
                </button>
            </div>
        </div>
    </div>
</div>
<section class="section-newsletter white-text">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-5 col-xl-6 collum">
                <a href="#" class="d-block d-md-flex justify-content-center justify-content-lg-start">
                    <img class="img-fluid" src="images/static/newsletter.jpg" alt="newsletter">
                </a>
            </div>
            <div class="col-12 col-lg-7 col-xl-6 collum text-right d-flex justify-content-center justify-content-lg-end align-items-center">
                <div>
                    <h4>Zapisz się do newslettera!</h4>
                    <p>Zapisz się, by otrzymywać najnowsze informacje o naszych produktach.</p>
                    <?php $form = ActiveForm::begin(['options' => ['class' => 'd-flex justify-content-end align-content-center']]); ?>
                        <?= $form->field($model, 'group_id')->hiddenInput(['value' => $group_id])->label(false); ?>

                        <?= $form->field($model, 'email')->textInput(["class" => "input-white w-100"])->label(false) ?>
                        <div class="form-group d-inline-flex">
                            <button class="button btn-full btn-magenta easy-submit" value="" type="submit">
                                <i class="i-exit-white icon"></i>
                            </button>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>