<?php

namespace common\widgets\ads\show;

use Yii;
use common\modules\ads\models\Ad;

class AdsWidget extends \common\hooks\yii2\bootstrap\Widget
{
    
    public $widget_item_id;
    public $ad_id;

    public function init()
    {
        parent::init();
    }

    
    public function run()
    {
        $iAdID = $this->ad_id;
        $oAd = Ad::find()->where(['id' => $iAdID])->one(); 
        
        if(is_null($oAd)) { return; }
        if(($oAd->code == "" || is_null($oAd->code)) && is_null($oAd->thumbnail_id)){ return; }
        
        if($oAd->url && $oAd->thumbnail_id){
            $oAd->code .= "<a href='{$oAd->url}' target='_blank'><img src='{$oAd->getThumbnail($oAd->width, $oAd->height, "crop")}' alt='{$oAd->name}' style='display: block; margin: 0 auto;'/></a>";
        }
        
        return $this->display([
            'oAd' => $oAd
        ]);
    }
 
    
}
