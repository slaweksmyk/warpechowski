<?php
/**
 * Widget MENU (Link list)
 * 
 * PHP version 7
 *
 * @author Volodymyr Shelelo <contact@profficode.com>
 * @copyright (c) 2016 Volodymyr Shelelo
 * @version 1.0
 */

namespace common\widgets\gallery\show;

use Yii;
use yii\helpers\Html;
use common\modules\gallery\models\Gallery;

class GalleryWidget extends \common\hooks\yii2\bootstrap\Widget
{
    
    public $widget_item_id;
    public $layout;
    public $gallery_id;
    
    /**
     * Build Menu widget
     */
    public function init()
    {
        parent::init();
    }
    
    /**
     * Run widget
     * 
     * @return menu HTML
     */
    public function run()
    {
        $galleryModel = new Gallery();
        $gallery = $galleryModel->find()->where([ 'id' => $this->gallery_id ])->one();
        
        if( $gallery ){
            $galleryHtml = $this->render($this->layout, [
                'gallery' => $gallery,
                'widget_item_id' => $this->widget_item_id
            ]);
            return $galleryHtml;
        } else {
            return '<h3>'.Html::encode('Gallery not found').'</h3>';
        }
    }
    
}