<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use common\modules\gallery\models\Gallery;

$galleryList = Gallery::find()->select(['id', 'name', 'category_id'])->orderBy('category_id DESC')->all();
$galleryDrop = array();

foreach($galleryList as $one){ 
    if($one->category_id){
        $galleryDrop[ $one['id'] ] = $one->category->name.': '.$one['name'];
    } else {
        $galleryDrop[ $one['id'] ] = $one['name'];
    }
}

?>

<div class="widget-main-form" >
    
    <!-- ADD WIDGET CUSTOM DATA -->
    <div class="row" style="background: #f8f8f8; padding-top: 15px; padding-bottom: 15px; " >
        <div class="col-12 col-sm-6" >
            <?php echo $form->field($model, 'data[gallery_id]')->dropDownList( $galleryDrop )->label( Yii::t('backend_widget_menu', 'select_menu') ); ?>
        </div>
    </div>
    <!-- END CUSTOM DATA -->
    
    <br>
    <div class="form-group">
        <?= Html::submitButton( Yii::t('backend', 'update'), ['class' => 'btn btn-primary']) ?>
    </div>

</div>