<?php
//add CSS
$this->registerCssFile('/bwcms/common/widgets/gallery/show/assets/css/gallery.css', ['depends' => 'frontend\assets\SiteAsset']);
?>

<div class='image-gallery' data-widget-item='<?php echo $widget_item_id; ?>' >
    <?php echo "<h4>{$gallery->name}</h4><p>{$gallery->description}</p>"; ?>

    <div class="images" >
    <?php foreach ($gallery->galleryItems as $image) { ?>
        <div class="image" >
        <?php if($image->crop){ ?>
            <a href="/app-upload-files/<?php echo $image->crop->filename; ?>" title="<?php echo $image->name; ?>" target="_blank">
                <img src='/app-upload-files/<?php echo $image->crop->filename; ?>' alt='<?php echo $image->alt; ?>'/>
            </a>
        <?php } else { ?>
            <a href="/app-upload-files/<?php echo $image->file->filename; ?>" title="<?php echo $image->name; ?>" target="_blank">
                <img src='/app-upload-files/<?php echo $image->file->filename; ?>' alt='<?php echo $image->alt; ?>'/>
            </a>
        <?php } ?>
        </div> 
    <?php } ?>
    </div>
    
</div>