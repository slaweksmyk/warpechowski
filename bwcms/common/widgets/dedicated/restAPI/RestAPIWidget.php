<?php

namespace common\widgets\dedicated\restAPI;

use Yii;
use common\helpers\FileCacheHelper;
use common\modules\rest\models\RestApi;
use yii\data\Pagination;
use common\modules\widget\models\WidgetItems;

class RestAPIWidget extends \common\hooks\yii2\bootstrap\Widget {

    public $widget_item_id;
    public $rest_id, $article_list, $article_list_group;
    public $limit;
    
    private $oRestAPI;
    private $oApiList;
    private $oApiGroup;

    // Unused
    public $article_category_id, $type_id;
    
    public function init() {
        parent::init();

        $this->oRestAPI = RestApi::find()->where(["=", "id", $this->rest_id])->one();
        $db = \Yii::$app->db_api;
        
        if($this->article_list){
            $id = $this->article_list;
            $obj = (object) $db->cache(function ($db) use ($id) {
                return $db->createCommand("SELECT * FROM `articles_lists` WHERE `id` = {$id}")->queryOne();
            }, 90000);
            $db->close();
            $this->oApiList = (object) $obj;
        }
    }

    public function run() {
        if($this->article_list_group){
            return $this->displayGroup();
        }
        
        if(!$this->article_list) { return; }
        if(isset($this->oApiList->scalar) && !$this->oApiList->scalar) { return; }

        $sCustomName = "";
        $oWidgetItems = WidgetItems::find()->where(["=", "id", $this->widget_item_id])->one();
        if(!is_null($oWidgetItems)){
            $sCustomName = $oWidgetItems->custom_name;
        }
        
        return $this->display([
            'limit' => $this->md_limit,
            'aReturnNews' => $this->getArticlesFromList($this->article_list, false),
            'aOnlyArticles' => $this->getArticlesFromList($this->article_list, true),
            'oApiList' => $this->oApiList,
            //'pagination' => $pagination,
            'sCustomName' => $sCustomName
        ]);
    }
    
    private function displayGroup(){
        $db = \Yii::$app->db_api;
        
        $iGroupID = $this->article_list_group;
        $aArticleApiLists = $db->cache(function ($db) use ($iGroupID) {
            return $db->createCommand("SELECT * FROM `articles_lists` WHERE `category_id` = {$iGroupID}")->queryAll();
        }, 300);
        $db->close();

        $i = 0;
        $aReturnArticleLists = [];
        foreach($aArticleApiLists as $index => $aList){
            $iServiceID = $aList["service_id"];
            if($iServiceID){
                $aService = $db->createCommand("SELECT * FROM `services` WHERE `id` = {$iServiceID}")->queryOne();
                $db->close();

                $aReturnArticleLists[$i] = [];
                $aReturnArticleLists[$i]["list"] = $aList;
                $aReturnArticleLists[$i]["service"] = (object) $aService;
                $aReturnArticleLists[$i]["articles"] = $this->getArticlesFromList($aList["id"], true);
                
                $i++;
            }
        }
        
        $sCustomName = "";
        $oWidgetItems = WidgetItems::find()->where(["=", "id", $this->widget_item_id])->one();
        if(!is_null($oWidgetItems)){
            $sCustomName = $oWidgetItems->custom_name;
        }

        return $this->display([
            'aReturnArticleLists' => $aReturnArticleLists,
            'limit' => $this->md_limit,
            'sCustomName' => $sCustomName
        ]);
    }
    
    private function getArticlesFromList($iArticleList, $isOnlyArticles = false){
        $db = \Yii::$app->db_api;
        
        $aReturnNews = [];
        $oServices = $this->callAPI("GET", "services");
        
        $i = 0;
        if(!is_array($oServices)){ return "Brak połączenia z API"; }
        foreach ($oServices as $oService) {
            $oService->service = $oService->title;
            $oService->service_image = $oService->logo;
            
            $aArticles = $this->callAPI("GET", "service", ["id" => $oService->id, "list" => $iArticleList, "per-page" => $this->md_limit, "page" => \Yii::$app->request->get('page', 1)]);
            if(!$aArticles) { $aArticles = []; } 
            
            $aReturnNews[$i]["service"] = $oService;
            $aReturnNews[$i]["articles"] = $aArticles;
            $i++;
        }

        $aElements = [];
        if(!Yii::$app->request->get('page', false)){
            $iLists = $iArticleList;
            
            $aArticlesListElements = $db->cache(function ($db) use ($iLists) {
                return $db->createCommand("SELECT * FROM `articles_lists_elements` WHERE `list_id` = {$iLists}")->queryAll();
            }, 300);
            $db->close();
            
            foreach($aArticlesListElements as $aArticlesListElement){
                $aElements[$aArticlesListElement["position"]] = $aArticlesListElement["article_id"];
            }
        }
        
        $pageUrl = explode("?", \Yii::$app->request->url);
        if (count($pageUrl) > 0) { $pageUrl = $pageUrl[0]; } else { $pageUrl = \Yii::$app->request->url; }        
        $pagination = new Pagination([
            'totalCount' => 999999, 
            'pageSize'=> $this->md_limit,
            'route' => $pageUrl
        ]);
        
        if($iArticleList == 10){
            $aOnlyArticlesAPI = $this->callAPI("GET", "service", ["id" => 6, "list" => $iArticleList, "per-page" => $pagination->limit, "page" => \Yii::$app->request->get('page', 1)]);
        } else {
            $aOnlyArticlesAPI = $this->callAPI("GET", "service", ["list" => $iArticleList, "per-page" => $pagination->limit, "page" => \Yii::$app->request->get('page', 1)]);
        }

        if($isOnlyArticles){
            $aOnlyArticles = [];
            if($aOnlyArticlesAPI){
                foreach($aOnlyArticlesAPI as $index => $oNews) {
                    $id = $oNews->id;
                    $obj = $db->cache(function ($db) use ($id) {
                        return $db->createCommand("SELECT articles.*, services.prefix, services.domain, services.color, concat(services.domain, articles.url) as `url` FROM `articles` LEFT JOIN `articles_types_hash` ON `articles_types_hash`.`article_id` = `articles`.`id` LEFT JOIN `services` ON `articles`.`service_id` = `services`.`id` WHERE `articles`.`id` = {$id}")->queryOne();
                    }, 90000);
                    $db->close();

                    $aOnlyArticles[$index] = (object) $obj;
                }

                foreach($aOnlyArticles as $index => $oNews){
                    if(isset($aElements[$index])){
                        $id = $aElements[$index];
                        $obj = (object) $db->cache(function ($db) use ($id) {
                            return $db->createCommand("SELECT articles.*, services.prefix, services.domain, services.color, concat(services.domain, articles.url) as `url` FROM `articles` LEFT JOIN `articles_types_hash` ON `articles_types_hash`.`article_id` = `articles`.`id` LEFT JOIN `services` ON `articles`.`service_id` = `services`.`id` WHERE `articles`.`id` = {$id}")->queryOne();
                        }, 90000);
                        $db->close();
                        
                        $aNewArticle = (object) $obj;
                        $aNewArticle->forced_position = true;

                        array_splice($aOnlyArticles, $index, 0, [$aNewArticle]);
                    }
                }
                
                $aIDs = [];
                foreach($aOnlyArticles as $index => $oNews){
                    if(in_array($oNews->id, $aIDs)){
                        unset($aOnlyArticles[$index]);
                    }

                    $aIDs[] = $oNews->id;
                }
            }
        }

        $aTmpReturnNews = $aReturnNews;
        foreach($aTmpReturnNews as $serviceIndex => $aService) {
            $aReturnNews[$serviceIndex]["articles"] = [];
            foreach ($aService["articles"] as $index => $oNews) {
                if(isset($aElements[$index])){
                    $id = $aElements[$index];
                    $obj = (object) $db->cache(function ($db) use ($id) {
                        return $db->createCommand("SELECT articles.*, services.prefix, services.domain, services.color, concat(services.domain, articles.url) as `url` FROM `articles` LEFT JOIN `articles_types_hash` ON `articles_types_hash`.`article_id` = `articles`.`id` LEFT JOIN `services` ON `articles`.`service_id` = `services`.`id` WHERE `articles`.`id` = {$id}")->queryOne();
                    }, 90000);
                    $db->close();
                    $aNewArticle = (object) $obj;
                    
                    $oNews->color = $aService["service"]->color;
                    $oNews->service_prefix = $aService["service"]->prefix;

                    if($aNewArticle->service_id == $aService["service"]->id){
                        $aReturnNews[$serviceIndex]["articles"][] = $aNewArticle;
                    }

                    $aReturnNews[$serviceIndex]["articles"][] = $oNews;
                } else {
                    $oNews->color = $aService["service"]->color;
                    $oNews->service_prefix = $aService["service"]->prefix;
                    
                    $aReturnNews[$serviceIndex]["articles"][] = $oNews;
                }
            }
        }
        
        if($isOnlyArticles){
            if(isset($aOnlyArticles[0]) && isset($aOnlyArticles[0]->scalar) && $aOnlyArticles[0]->scalar === false) { return; }

            $aOnlyArticles = array_slice($aOnlyArticles, 0, $this->md_limit, true);
        }
        
        if($isOnlyArticles){
            return $aOnlyArticles;
        } else {
            return $aReturnNews;
        }
    }

    private function callAPI($method, $function = null, $data = false) {
        $dataHash = json_encode($data);
        $hashName = md5(strtolower("{$method}_{$function}_{$dataHash}"));

        $result = FileCacheHelper::getFromCache($hashName);

        
        if (!$result) {
            $curl = curl_init();
            $url = $this->oRestAPI->url;

            if ($function) {
                $url .= "/{$function}/";
            }

            switch ($method) {
                case "POST":
                    curl_setopt($curl, CURLOPT_POST, 1);

                    if ($data)
                        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                    break;

                case "PUT":
                    curl_setopt($curl, CURLOPT_PUT, 1);
                    break;

                default:
                    if ($data)
                        $url = sprintf("%s?%s", $url, http_build_query($data));
            }

            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_USERPWD, "{$this->oRestAPI->login}:{$this->oRestAPI->password}");

            
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            
            $result = curl_exec($curl);

            curl_close($curl);

            if ($result != "") {
                FileCacheHelper::saveToCache($hashName, $result);
            }
        }

        return json_decode($result);
    }

}
