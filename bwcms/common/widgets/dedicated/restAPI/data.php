<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\modules\rest\models\RestApi;

?>

<div class="widget-main-form" >

    <!-- ADD WIDGET CUSTOM DATA -->
    <div class="row" style="background: #f8f8f8; padding-top: 15px; padding-bottom: 15px; " >
        <div class="col-3">
            <?= $form->field($model, 'data[rest_id]')->dropDownList(ArrayHelper::map(RestApi::find()->all(), 'id', 'url'), ["prompt" => "Wybierz konfiguracje"])->label("Konfiguracja API"); ?>
        </div>
        <div class="col-3">
            <?= $form->field($model, 'data[article_list]')->dropDownList(ArrayHelper::map(Yii::$app->db_api->createCommand("SELECT * FROM `articles_lists`")->queryAll(), 'id', 'name'), ["prompt" => "Wybierz listę artykułową"])->label("Lista artykułowa"); ?>
        </div>
        <div class="col-3">
            <?= $form->field($model, 'data[article_list_group]')->dropDownList(ArrayHelper::map(Yii::$app->db_api->createCommand("SELECT * FROM `articles_lists_categories`")->queryAll(), 'id', 'name'), ["prompt" => "Wybierz grupę artykułową"])->label("Grupa artykułowa"); ?>
        </div>
    </div>
    <!-- END CUSTOM DATA -->
    
    <br>
    <div class="form-group">
        <?= Html::submitButton( Yii::t('backend', 'update'), ['class' => 'btn btn-primary']) ?>
    </div>

</div>