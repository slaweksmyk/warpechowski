<?php

use common\helpers\TextHelper;
?>
<div class="col-5">
    <h2><span><a href="<?= $oApiList->read_more_url ?>" title="Wiadomości">Wiadomości</a></span><span class="line"></span></h2>
    <ul class="main-news-list">
        <?php $i = 0; ?>
        <?php foreach ($aOnlyArticles as $index => $oNews) { ?>
            <?php if ($i < $limit) { ?>
                <li>
                    <a href="<?= $oNews->domain.$oNews->slug ?>" title="<?= htmlspecialchars($oNews->title) ?>"><span class="flag flag-danger" style="color: <?= $oNews->color ?>;"><?= $oNews->prefix !="D24" ? $oNews->prefix : ''  ?></span> <?= TextHelper::substr($oNews->title, 0, 80) ?></a>
                </li>
                <?php $i++; ?>
            <?php } ?>
        <?php } ?>
    </ul>
    <a href="<?= $oApiList->read_more_url ?>" title="Więcej" class="more dark float-right">Więcej</a>
</div>