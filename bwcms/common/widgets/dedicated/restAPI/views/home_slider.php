<?php

use common\helpers\TextHelper;

$this->registerJs(
        "        $('[data-item=\"slider\"][data-id=\"slider-1-0\"] [data-item=\"slider-1-0-aside\"]').slick({
        dots: false,
        infinite: true,
        speed: 300,
        autoplay: true,
        draggable: false,
        autoplaySpeed: 2000,
        slidesToShow: 4,
        adaptiveHeight: true,
        vertical: true,
        arrows: true,
        nextArrow: $('[data-arrow=\"slider-1-0-top\"].slider-aside-arrow-top'),
        prevArrow: $('[data-arrow=\"slider-1-0-bottom\"].slider-aside-arrow-bottom'),
    });
    $('[data-item=\"slider\"][data-id=\"slider-1-0\"] > .slider-loader').hide();
    $('[data-item=\"slider\"][data-id=\"slider-1-0\"] [data-item=\"slider-content\"]').animate({opacity: 1});"
);
?>

<section id="main-slider" data-item="main-slider" class="section-slider block block-title block-subtitle block-dark block-border-top-danger">
    <div class="container">
        <h2 class="logo mt-10"><span><?= $sCustomName ?></span><div class="line"></div></h2>
        <div class="block-slider">
            <div class="block-slider-content d-flex flex-nowrap">
                <div class="block-slider-tab">
                    <div class="tab-content" id="slider-1-1-tabContent">
                        <div class="tab-pane show active fade" data-item="slider" data-id="slider-1-0" id="slider-1-0" role="tabpanel" aria-labelledby="slider-1-0-tab">
                            <div class="slider-loader">
                                <div class="loading loading--double"></div>
                            </div>
                            <div class="d-flex row" data-item="slider-content" style="opacity: 0">
                                <div class="col-8">
                                    <?php foreach ($aOnlyArticles as $index => $oArticle) { ?>
                                        <?php if ($index == 0) { ?>
                                            <div class="block-card shadow big-title h-400">
                                                <div class="card-wrapper">
                                                    <a href="<?= $oArticle->domain.$oArticle->slug ?>" title="<?= $oArticle->title ?>"> 
                                                        <?php if ($oArticle->image) { ?>
                                                            <div class="image img-res img-api">
                                                                <img src="<?= $oArticle->image ?>" alt="<?= $oArticle->title ?>">
                                                            </div>
                                                        <?php } ?>
                                                        <div class="title">
                                                            <div class="txt">
                                                                <strong><?= $oArticle->category ?></strong>
                                                                <h3><?= TextHelper::substr($oArticle->title, 0, 80); ?></h3>
                                                                <p><?= TextHelper::substr($oArticle->short_description, 0, 300); ?></p>
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <div class="social d-flex justify-content-start align-content-start text-left">
                                                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?= $oArticle->domain.$oArticle->slug ?>" title="Udostępnij na facebooku"><i class="icon icon-icon-facebook-small"></i></a> 
                                                        <a href="https://twitter.com/home?status=<?= $oArticle->domain.$oArticle->slug ?>" title="Udostępnij na twitterze"><i class="icon icon-icon-twitter-small"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                                <div class="col-4 slider-aside-size">
                                    <div class="slider-aside" data-item="slider-1-0-aside">
                                        <?php foreach ($aOnlyArticles as $index => $oArticle) { ?>
                                            <?php if ($index > 0) { ?>
                                                <div class="block-card nb vertical text-light v-80">
                                                    <div class="card-wrapper">
                                                        <a href="<?= $oArticle->domain.$oArticle->slug ?>" title="<?= $oArticle->title ?>">
                                                            <?php if ($oArticle->image) { ?>
                                                                <div class="image img-res img-api">
                                                                    <img src="<?= $oArticle->image ?>" alt="<?= $oArticle->title ?>">
                                                                </div>
                                                            <?php } ?>

                                                            <div class="title">
                                                                <h4><?= TextHelper::substr($oArticle->title, 0, 80); ?></h4>
                                                            </div>
                                                        </a>
                                                        <div class="social d-flex justify-content-end align-content-end text-right">
                                                            <a href="https://www.facebook.com/sharer/sharer.php?u=<?= $oArticle->domain.$oArticle->slug ?>" title="Udostępnij na facebooku"><i class="icon icon-icon-facebook-small"></i></a> 
                                                            <a href="https://twitter.com/home?status=<?= $oArticle->domain.$oArticle->slug ?>" title="Udostępnij na twitterze"><i class="icon icon-icon-twitter-small"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                    <?php } ?>
                                    </div>
                                    <?php if (count($aOnlyArticles) > 0) { ?>
                                        <div class="slider-aside-arrow d-flex">
                                            <div data-arrow="slider-1-0-top" class="slider-aside-arrow-block mr-auto slider-aside-arrow-top d-flex justify-content-center align-items-center"><i class="icon icon-arrow-slider-top"></i></div>
                                            <div data-arrow="slider-1-0-bottom" class="slider-aside-arrow-block slider-aside-arrow-bottom d-flex justify-content-center align-items-center"><i class="icon icon-arrow-slider-bottom"></i></div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>