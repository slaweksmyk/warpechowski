<?php

use common\helpers\TextHelper;

foreach ($aReturnArticleLists as $serviceIndex => $aService) {
    if ($serviceIndex == 0) {
        $this->registerJs(
                "       
                $('[data-item=\"slider\"][data-id=\"slider-1-0\"] > .slider-loader').hide();
                $('[data-item=\"slider\"][data-id=\"slider-1-0\"] [data-item=\"slider-content\"]').animate({opacity: 1});"
        );
    } else {
        $this->registerJs(
                "$('[data-action=\"change-slider\"][data-id=\"slider-1-{$serviceIndex}\"]').click(function () {
                if (!$('[data-item=\"slider\"][data-id=\"slider-1-{$serviceIndex}\"] [data-item=\"slider-content\"]').hasClass(\"slick\")) {
                    $('[data-item=\"slider\"][data-id=\"slider-1-{$serviceIndex}\"] .slider-loader').show();
                    $('[data-item=\"slider\"][data-id=\"slider-1-{$serviceIndex}\"] [data-item=\"slider-content\"]').animate({opacity: 0});
                    setTimeout(function () {

                        $('[data-item=\"slider\"][data-id=\"slider-1-{$serviceIndex}\"] .slider-loader').hide();
                        $('[data-item=\"slider\"][data-id=\"slider-1-{$serviceIndex}\"] [data-item=\"slider-content\"]').animate({opacity: 1}).addClass('slick');
                    }, 800);
                }
            });"
        );
    }
}
?>

<section id="main-slider" data-item="main-slider" class="block block-title block-subtitle block-dark block-border-top-danger">
    <div class="container">
        <div class="block-slider">
            <h2 class="mr-auto">W naszych serwisach<span class="line"></span></h2>
            <ul class="nav-slider-logo nav nav-pills justify-content-center align-items-center" id="nav-1" role="tablist"><!-- !!!!!!!!! nav ID -->
                <?php foreach ($aReturnArticleLists as $serviceIndex => $aService) { ?>
                    <li class="nav-item">
                        <a class="nav-link <?php
                        if ($serviceIndex == 0) {
                            echo "active";
                        }
                        ?>" id="slider-1-<?= $serviceIndex ?>-tab"  data-action="change-slider" data-id="slider-1-<?= $serviceIndex ?>"  data-toggle="pill" href="#slider-1-<?= $serviceIndex ?>" role="tab" aria-controls="slider-1-<?= $serviceIndex ?>" aria-expanded="true"><?= $aService["service"]->service ?></a>
                    </li>
                <?php } ?>
            </ul>
            <div class="block-slider-content d-flex flex-nowrap">
                <div class="block-slider-tab">
                    <div class="tab-content" id="slider-1-1-tabContent">
                        <?php foreach ($aReturnArticleLists as $serviceIndex => $aService) { ?>
                            <div class="tab-pane <?php
                            if ($serviceIndex == 0) {
                                echo "show active";
                            }
                            ?> fade" data-item="slider" data-id="slider-1-<?= $serviceIndex ?>" id="slider-1-<?= $serviceIndex ?>" role="tabpanel" aria-labelledby="slider-1-<?= $serviceIndex ?>-tab">
                                <!-- START SLIDER -->
                                <div class="slider-loader">
                                    <div class="loading loading--double"></div>
                                </div>
                                <div class="row" data-item="slider-content" style="opacity: 0">
                                    <div class="col-12">
                                        <div class="block-card shadow big-title h-400 ">
                                            <div class="card-wrapper">
                                                <a href="<?= $aService["service"]->domain. $aService['articles'][0]->slug ?>" title="<?= $aService['articles'][0]->title ?>"> 
                                                    <div class="image img-res">
                                                        <img src="<?= $aService['articles'][0]->image ?>" alt="<?= $aService['articles'][0]->title ?>">
                                                    </div>
                                                    <div class="title">
                                                        <div class="txt">
                                                            <strong><?= $aService['articles'][0]->category ?></strong>
                                                            <h3><?= $aService['articles'][0]->title ?></h3>
                                                            <?php /*<p><?= $aService['articles'][0]->short_description ?></p>*/ ?>
                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="social d-flex justify-content-start align-content-start text-left">
                                                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?= $aService["service"]->domain. $aService['articles'][0]->slug ?>" title="Udostępnij na facebooku"><i class="icon icon-icon-facebook-small"></i></a> 
                                                    <a href="https://twitter.com/home?status=<?= $aService["service"]->domain. $aService['articles'][0]->slug ?>" title="Udostępnij na twitterze"><i class="icon icon-icon-twitter-small"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 slider-aside-size">
                                        <div class="row slider-aside" data-item="slider-1-<?= $serviceIndex ?>-aside">
                                            <?php $i = 0; ?>
                                            <?php foreach ($aService['articles'] as $oNews) { ?>
                                                <?php if ($i > 0) { ?>
                                                    <div class="col-6 block-card nb vertical text-light v-80 mt-10">
                                                        <div class="card-wrapper">
                                                            <a href="<?= trim($aService["service"]->domain, "/") . $oNews->url ?>" title="<?= htmlspecialchars($oNews->title) ?>">
                                                                <?php if ($oNews->image) { ?>
                                                                    <div class="image img-res ">
                                                                        <img src="<?= $oNews->image ?>" alt="<?= htmlspecialchars($oNews->title) ?>">
                                                                    </div>
                                                                <?php } ?>

                                                                <div class="title">
                                                                    <h4><?= TextHelper::substr($oNews->title, 0, 40) ?></h4>
                                                                </div>
                                                            </a>
                                                            <div class="social d-flex justify-content-end align-content-end text-right">
                                                                <a href="https://www.facebook.com/sharer/sharer.php?u=<?= trim($aService["service"]->domain, "/") . $oNews->url ?>" title="Udostępnij na facebooku"><i class="icon icon-icon-facebook-small"></i></a> 
                                                                <a href="https://twitter.com/home?status=<?= trim($aService["service"]->domain, "/") . $oNews->url ?>" title="Udostępnij na twitterze"><i class="icon icon-icon-twitter-small"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <?php $i++; ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- END SLIDER -->
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>