<section id="main-slider" data-item="main-slider" class="block block-title block-subtitle block-dark block-border-top-danger">
    <div class="container">
        <div class="block-slider">
            <h2 class="logo mt-10"><span><?= $sCustomName ?></span><div class="line"></div></h2>
            <div class="mobile-slider">
                <?php foreach ($aOnlyArticles as $index => $row) { ?>
                    <?php if ($index == 0) { ?>
                        <div class="block-card shadow h-200">
                            <div class="card-wrapper">
                                <a href="<?= $row->domain.$row->slug ?>" title="<?= htmlspecialchars($row->title) ?>">
                                    <div class="image img-res img-api-height">
                                        <?php if ($row->image) { ?>
                                            <img src="<?= $row->image ?>" alt="<?= htmlspecialchars($row->title) ?>">
                                        <?php } ?>
                                    </div>
                                    <div class="title">
                                        <div class="txt">
                                            <strong><?= $row->category ?></strong>
                                            <h4><?= $row->title ?></h4>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>

                <ul class="block-list-image mt-20">
                    <?php foreach ($aOnlyArticles as $index => $row) { ?>
                        <?php if ($index > 0) { ?>
                            <li>
                                <a href="<?= $row->domain.$row->slug ?>" title="<?= htmlspecialchars($row->title) ?>">
                                    <div class="row">
                                        <div class="col-4">
                                            <div class="block-card-image img-res img-api-height">
                                                <?php if ($row->image) { ?>
                                                    <img src="<?= $row->image ?>" alt="<?= htmlspecialchars($row->title) ?>">
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="col-8 no-p-left">
                                            <h4><?= $row->title ?></h4>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</section>