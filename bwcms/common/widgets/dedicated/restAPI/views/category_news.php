<?php
    use common\helpers\TextHelper;
?>
<div class="col-6">
    <h2><span><a href="<?= $oApiList->read_more_url ?>">Wiadomości</a></span><span class="line"></span></h2>
    <?php foreach ($aOnlyArticles as $index => $oNews) { ?>
        <div class="block-card background b-horizontal h-300 mt-20">
            <div class="card-wrapper">
                <a href="<?= $oNews->domain.$oNews->slug ?>" title="<?= htmlspecialchars($oNews->title) ?>">
                    <div class="image img-res">
                        <img src="<?= $oNews->image ?>" alt="<?= htmlspecialchars($oNews->title) ?>">
                    </div>
                    <div class="title">
                        <div class="txt">
                            <strong><?= $oNews->category ?></strong>
                            <h4><?= $oNews->title ?></h4>
                            <?= $oNews->short_description ?>
                        </div>
                    </div>
                </a>
                <div class="social d-flex justify-content-start align-content-start text-left">
                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?= $oNews->domain.$oNews->slug ?>" title="Udostępnij na facebooku"><i class="icon icon-icon-facebook-small"></i></a> 
                    <a href="https://twitter.com/home?status=<?= $oNews->domain.$oNews->slug ?>" title="Udostępnij na twitterze"><i class="icon icon-icon-twitter-small"></i></a>
                </div>
            </div>
        </div>
    <?php unset($aOnlyArticles[0]); break; } ?>
    <ul class="main-news-list">
        <?php $i = 0; ?>
        <?php foreach ($aOnlyArticles as $index => $oNews) { ?>
            <?php if ($i < $limit) { ?>
                <li>
                    <a href="<?= $oNews->domain.$oNews->slug ?>" title="<?= htmlspecialchars($oNews->title) ?>"><span class="flag flag-danger" style="color: <?= $oNews->color ?>;"><?= $oNews->prefix !="D24" ? $oNews->prefix : ''  ?></span> <?= TextHelper::substr($oNews->title, 0, 80) ?></a>
                </li>
                <?php $i++; ?>
            <?php } ?>
        <?php } ?>
    </ul>
</div>