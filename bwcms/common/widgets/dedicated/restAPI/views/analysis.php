<?php
    use common\helpers\TextHelper;
?>
<section id="analysis-news" data-item="analysis-news" class="section block block-title block-subtitle block-analysis ">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2><span><a href="<?= $oApiList->read_more_url ?>" title="<?= $sCustomName; ?>"><?= $sCustomName; ?></a></span><span class="line"></span></h2>
            </div>
            <div class="col-8">
                <div class="row mt-25">
                    <?php foreach ($aOnlyArticles as $index => $oNews) { ?>
                        <?php if ($index < 4) { ?>
                            <div class="col-6 block-card min shadow h-200 mt-5">
                                <div class="card-wrapper">
                                    <a href="<?= $oNews->domain.$oNews->slug ?>" title="<?= htmlspecialchars($oNews->title) ?>">
                                        <div class="image img-res img-api">
                                            <?php if ($oNews->image) { ?>
                                                <img src="<?= $oNews->image ?>" alt="<?= htmlspecialchars($oNews->title) ?>">
                                            <?php } ?>
                                        </div>
                                        <div class="title">
                                            <div class="txt">
                                                <strong><?= $oNews->category ?? '' ?></strong>
                                                <h4><?= TextHelper::substr($oNews->title, 0, 80); ?></h4>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="social d-flex justify-content-end align-content-end text-right">
                                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?= $oNews->domain.$oNews->slug ?>" title="Udostępnij na facebooku"><i class="icon icon-icon-facebook-small"></i></a> 
                                        <a href="https://twitter.com/home?status=<?= $oNews->domain.$oNews->slug ?>" title="Udostępnij na twitterze"><i class="icon icon-icon-twitter-small"></i></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
            <div class="col-4 mt-30">
                <ul class="main-news-list mt-0">
                    <?php foreach ($aOnlyArticles as $index => $oNews) { ?>
                        <?php if ($index >= 4) { ?>
                            <li>
                                <a href="<?= $oNews->domain.$oNews->slug ?>" title="<?= htmlspecialchars($oNews->title); ?>"><span class="flag flag-danger" style="color: <?= $oNews->color ?? "" ?>;"><?= isset($oNews->prefix) ? ($oNews->prefix !="D24" ? $oNews->prefix : '') : ""  ?></span> <?= TextHelper::substr($oNews->title, 0, 120); ?></a>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </ul>
                <a href="<?= $oApiList->read_more_url ?>" title="Więcej" class="more dark float-right">Więcej</a>
            </div>
        </div>
    </div>
</section>