<?php
use common\helpers\TextHelper;
?>
<h2><span><a href="<?= $oApiList->read_more_url ?>" title="Wywiady">Wywiady</a></span><span class="line"></span></h2>

<div class="row">
    <div class="col-4">
        <?php foreach ($aOnlyArticles as $index => $oNews) { ?>
            <?php if ($index == 0) { ?>
                <div class="block-card h-300 mt-30">
                    <div class="card-wrapper">
                        <a href="<?= $oNews->domain.$oNews->slug ?>" title="<?= htmlspecialchars($oNews->title) ?>">
                            <div class="image img-res img-api">
                                <?php if ($oNews->image) { ?>
                                    <img src="<?= $oNews->image ?>" alt="<?= htmlspecialchars($oNews->title) ?>">
                                <?php } ?>
                            </div>
                            <div class="title">
                                <strong><?= $oNews->category ?? '' ?></strong>
                                <h4><?= TextHelper::substr($oNews->title, 0, 80); ?></h4>
                            </div>
                        </a>
                        <div class="social d-flex justify-content-end align-content-end text-right">
                            <a href="https://www.facebook.com/sharer/sharer.php?u=<?= $oNews->domain.$oNews->slug ?>" title="Udostępnij na facebooku"><i class="icon icon-icon-facebook-small"></i></a> 
                            <a href="https://twitter.com/home?status=<?= $oNews->domain.$oNews->slug ?>" title="Udostępnij na twitterze"><i class="icon icon-icon-twitter-small"></i></a>
                        </div>
                    </div>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
    <div class="col-8">
        <ul class="list mt-30">
            <?php foreach ($aOnlyArticles as $index => $oNews) { ?>
                <?php if ($index > 0) { ?>
                    <li class="block-card col-12 vertical nb v-100">
                        <div class="card-wrapper">
                            <a href="<?= $oNews->domain.$oNews->slug ?>" title="<?= htmlspecialchars($oNews->title) ?>">
                                <div class="image img-res img-api">
                                    <?php if ($oNews->image) { ?>
                                        <img src="<?= $oNews->image ?>" alt="<?= htmlspecialchars($oNews->title) ?>">
                                    <?php } ?>
                                </div>
                                <div class="title">
                                    <strong><?= $oNews->category ?? '' ?></strong>
                                    <h4><span class="flag flag-danger" style="color: <?= $oNews->color ?? "" ?>;"><?= isset($oNews->prefix) ? ($oNews->prefix !="D24" ? $oNews->prefix : '') : ""  ?></span> <?= TextHelper::substr($oNews->title, 0, 80); ?></h4>
                                </div>
                            </a>
                            <div class="social d-flex justify-content-end align-content-end text-right">
                                <a href="https://www.facebook.com/sharer/sharer.php?u=<?= $oNews->domain.$oNews->slug ?>" title="Udostępnij na facebooku"><i class="icon icon-icon-facebook-small"></i></a> 
                                <a href="https://twitter.com/home?status=<?= $oNews->domain.$oNews->slug ?>" title="Udostępnij na twitterze"><i class="icon icon-icon-twitter-small"></i></a>
                            </div>
                        </div>
                    </li>
                <?php } ?>
            <?php } ?>
        </ul>
    </div>
</div>