<h2 class="mr-auto">Wiadomości <span class="line"></span></h2>
<div class="mobile-slider">
    <?php foreach ($aReturnNews as $serviceIndex => $aService) { ?>
        <?php foreach ($aService["articles"] as $index => $oNews) { ?>
            <div class="block-card shadow h-200">
                <div class="card-wrapper">
                    <a href="<?= $oNews->url ?>" title="<?= htmlspecialchars($oNews->title) ?>">
                        <div class="image img-res img-api">
                            <?php if ($oNews->image) { ?>
                                <img src="<?= $oNews->image ?>" alt="<?= htmlspecialchars($oNews->title) ?>">
                            <?php } ?>
                        </div>
                        <div class="title">
                            <div class="txt">
                                <strong><?= $oNews->category ?></strong>
                                <h4><?= $oNews->title ?></h4>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <?php break;
        }
        ?>
        <?php break;
    }
    ?>
    <ul class="block-list-image mt-20">
        <?php $i = 0; ?>
        <?php foreach ($aReturnNews as $serviceIndex => $aService) { ?>
    <?php foreach ($aService["articles"] as $index => $oNews) { ?>
        <?php if ($i < $limit) { ?>
                    <li>
                        <a href="<?= $oNews->url ?>" title="<?= htmlspecialchars($oNews->title) ?>">
                            <div class="row">
                                <div class="col-4">
                                    <div class="block-card-image img-res img-api-height">
                                        <img src="<?= $oNews->image ?>" alt="<?= htmlspecialchars($oNews->title) ?>">
                                    </div>
                                </div>
                                <div class="col-8 no-p-left">
                                    <h4><?= $oNews->title ?></h4>
                                </div>
                            </div>
                        </a>
                    </li>
                    <?php $i++; ?>
                <?php } ?>
    <?php } ?>
<?php } ?>
    </ul>
    <div class="list-more">
        <a href="<?= $oApiList->read_more_url ?>" title="Więcej" >Więcej</a>
    </div>
</div>