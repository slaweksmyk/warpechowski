<?php
    use yii\helpers\BaseUrl;
    
    $aExcludedServices = [BaseUrl::base(true)."/", "http://www.mspo.defence24.pl/"];
?>

<?php foreach($aReturnNews as $serviceIndex => $aService){ ?>
    <?php if(!in_array($aService["service"]->domain, $aExcludedServices)){ ?>
        <div class="col-12 <?php if($serviceIndex != 0){ ?>mt-20<?php } ?>">
            <div class="block-others-news-logo">
                <a href="<?= trim( $aService["service"]->domain, "/") ?>" target="_blank" title="<?= $aService["service"]->title ?>" class="logo">
                    <img src="<?= '/img/logo/'.$aService["service"]->class.'/'.$aService["service"]->logo ?>" alt="<?= $aService["service"]->title ?>">
                </a>
            </div>

            <ul class="main-news-list">
                <?php foreach($aService["articles"] as $index => $oNews){ ?>
                    <li>
                        <a href="<?= $aService["service"]->domain.$oNews->slug ?>" title="<?= htmlspecialchars($oNews->title) ?>"><?= $oNews->title ?></a>
                    </li>
                <?php } ?>
            </ul>
        </div>
    <?php } ?>
<?php } ?>