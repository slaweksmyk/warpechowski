<?php
    $aNews = [];
    foreach($aReturnNews as $aService){
        if($aService["service"]->service == "Defence24.com"){
            $aNews = $aService["articles"];
        }
    }
?>
<div class="right-news-module mt-15 mb-15">
    <div class="block-subtitle"><h5><span><a href="http://www.defence24.com" title="Defence24" target="_blank">Defence24.com</a></span><span class="line"></span></h5></div>
    <div class="row">
        <?php foreach($aNews as $oNews){ ?>
            <div class="block-card col-12 h-300 mt-10">
                <div class="card-wrapper">
                    <a href="<?= $oNews->url ?>" title="<?= htmlspecialchars($oNews->title) ?>">
                        <?php if($oNews->image){ ?>
                            <div class="image img-res">
                                <img src="<?= $oNews->image ?>" alt="<?= htmlspecialchars($oNews->title) ?>">
                            </div>
                        <?php } ?>
                        <div class="title">
                            <strong><?= $oNews->category ?></strong>
                            <h4><?= $oNews->title ?></h4>
                        </div>
                    </a>
                    <div class="social d-flex justify-content-end align-content-end text-right">
                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?= $oNews->url ?>" title="Udostępnij na facebooku"><i class="icon icon-icon-facebook-small"></i></a> 
                        <a href="https://twitter.com/home?status=<?= $oNews->url ?>" title="Udostępnij na twitterze"><i class="icon icon-icon-twitter-small"></i></a>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>