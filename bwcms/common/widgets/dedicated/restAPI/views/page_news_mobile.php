<?php
use common\helpers\TextHelper;
?>
<section id="news" data-item="news" class="section-top block block-title block-subtitle block-news ">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2><span><?= $oApiList->name ?></span><span class="line"></span></h2>
            </div>
                <?php foreach($aOnlyArticles as $index => $oNews){ ?>
                    <?php if($index == 0){ ?>
                         <div class="col-12 block-card shadow h-200 mt-20">
                                <div class="card-wrapper">
                                    <a href="<?= $oNews->domain.$oNews->slug ?>" title="<?= htmlspecialchars($oNews->title) ?>">
                                        <div class="image img-res">
                                            <img src="<?= $oNews->image ?>" alt="<?= htmlspecialchars($oNews->title) ?>">
                                        </div>
                                        <div class="title">
                                            <div class="txt">
                                                <strong><?= $oNews->category ?></strong>
                                                <h4><?= TextHelper::substr($oNews->title, 0, 60) ?></h4>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="social d-flex justify-content-end align-content-end text-right">
                                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?= $oNews->domain.$oNews->slug ?>" title="Udostępnij na facebooku"><i class="icon icon-icon-facebook-small"></i></a> 
                                        <a href="https://twitter.com/home?status=<?= $oNews->domain.$oNews->slug ?>" title="Udostępnij na twitterze"><i class="icon icon-icon-twitter-small"></i></a>
                                    </div>
                                </div>
                            </div>
                    <?php } ?>
                <?php } ?>
            <div class="col-12">
                <ul class="main-news-list mt-30">
                    <?php foreach($aOnlyArticles as $index => $oNews){ ?>
                        <?php if($index > 0){ ?>
                            <li>
                                <a href="<?= $oNews->domain.$oNews->slug ?>" title="<?= htmlspecialchars($oNews->title) ?>"><span class="flag flag-danger" style="color: <?= $oNews->color ?>;"><?= $oNews->prefix !="D24" ? $oNews->prefix : ''  ?></span> <?= $oNews->title ?></a>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</section>