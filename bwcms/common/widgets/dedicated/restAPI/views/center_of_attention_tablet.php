<div class="col-12">
    <h2><span>W centrum uwagi</span><span class="line"></span></h2>
    <div class="d-flex flex-nowrap">
        <div class="main-news-cards">
            <div class="row">
                <?php $i = 0; ?>
                <?php foreach ($aReturnNews as $serviceIndex => $aService) { ?>
                    <?php foreach ($aService["articles"] as $index => $oNews) { ?>
                        <?php if ($i < $limit) { ?>
                            <!-- news block one -->
                            <div class="block-card min col-6 h-300">
                                <div class="card-wrapper">
                                    <a href="<?= $oNews->url ?>" title="<?= htmlspecialchars($oNews->title) ?>">
                                        <div class="image img-res">
                                            <?php if ($oNews->image) { ?>
                                                <img src="<?= $oNews->image ?>" alt="<?= htmlspecialchars($oNews->title) ?>">
                                            <?php } ?>
                                        </div>
                                        <div class="title">
                                            <strong><?= $oNews->category ?></strong>
                                            <h4><?= $oNews->title ?></h4>
                                        </div>
                                    </a>
                                    <div class="social d-flex justify-content-end align-content-end text-right">
                                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?= $oNews->url ?>" title="Udostępnij na facebooku"><i class="icon icon-icon-facebook-small"></i></a> 
                                        <a href="https://twitter.com/home?status=<?= $oNews->url ?>" title="Udostępnij na twitterze"><i class="icon icon-icon-twitter-small"></i></a>
                                    </div>
                                </div>
                            </div>
                            <?php $i++; ?>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>