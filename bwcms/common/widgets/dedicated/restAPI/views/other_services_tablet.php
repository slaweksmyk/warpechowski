<?php
    use yii\helpers\BaseUrl;
    
    $aExcludedServices = [BaseUrl::base(true)."/", "http://www.mspo.defence24.pl/"];
?>

<div class="block-others-news-content ">
    <?php foreach($aReturnNews as $serviceIndex => $aService){ ?>
        <?php if(!in_array($aService["service"]->domain, $aExcludedServices)){ ?>
            <ul class="block-others-news-list d-flex align-content-center s-<?= $aService["service"]->service_class ?>">
                <li>
                    <a href="<?= trim( $aService["service"]->domain, "/") ?>" target="_blank" title="<?= $aService["service"]->title ?>" class="logo d-flex align-items-center justify-content-center ">
                        <img src="<?= '/img/logo/'.$aService["service"]->class.'/'.$aService["service"]->logo ?>" alt="<?= $aService["service"]->title ?>">
                    </a>
                </li>
                <?php foreach($aService["articles"] as $index => $oNews){ ?>
                    <?php if($index == 0){ ?>
                       <li>
                        <div class="block-card shadow only-title">
                            <div class="card-wrapper">
                                <a href="<?= $aService["service"]->domain.$oNews->slug ?>" title="<?= htmlspecialchars($oNews->title) ?>">
                                    <div class="image img-res img-api">
                                        <img src="<?= $oNews->image ?>" alt="<?= htmlspecialchars($oNews->title) ?>">
                                    </div>
                                    <div class="title">
                                        <div class="txt">
                                            <h4><?= $oNews->title ?></h4>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </li>
                    <?php } ?>
                <?php } ?>
            </ul>
        <?php } ?>
    <?php } ?>
</div>