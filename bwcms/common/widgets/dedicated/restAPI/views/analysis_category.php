<?php
    use common\helpers\TextHelper;
?>
<div class="col-6">
    <h2><span><a href="<?= $oApiList->read_more_url ?>" title="<?= $sCustomName; ?>"><?= $sCustomName; ?></a></span><span class="line"></span></h2>
    <ul class="list mt-30">
        <?php foreach ($aOnlyArticles as $index => $oNews) { ?>
            <li class="block-card col-12 vertical v-100">
                <div class="card-wrapper">
                    <a href="<?= $oNews->domain.$oNews->slug ?>" title="<?= htmlspecialchars($oNews->title) ?>">
                        <div class="image img-res img-api">
                            <?php if ($oNews->image) { ?>
                                <img src="<?= $oNews->image ?>" alt="<?= htmlspecialchars($oNews->title) ?>">
                            <?php } ?>
                        </div>
                        <div class="title">
                            <strong><?= $oNews->category ?? '' ?></strong>
                            <h4><?= TextHelper::substr($oNews->title, 0, 80); ?></h4>
                        </div>
                    </a>
                    <div class="social d-flex justify-content-end align-content-end text-right">
                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?= $oNews->domain.$oNews->slug ?>" title="Udostępnij na facebooku"><i class="icon icon-icon-facebook-small"></i></a> 
                        <a href="https://twitter.com/home?status=<?= $oNews->domain.$oNews->slug ?>" title="Udostępnij na twitterze"><i class="icon icon-icon-twitter-small"></i></a>
                    </div>
                </div>
            </li>
        <?php } ?>
    </ul>
</div>