<?php
use common\helpers\TextHelper;
?>
<section id="analysis-news" data-item="analysis-news" class="block block-title block-subtitle block-analysis ">
    <div class="container">
        <div class="block-slider">
            <h2 class="mr-auto"><a href="<?= $oApiList->read_more_url ?>" title="<?= $sCustomName; ?>"><?= $sCustomName; ?></a> <span class="line"></span></h2>
            <div class="mobile-slider">
                <?php foreach ($aOnlyArticles as $index => $oNews) { ?>
                    <div class="block-card shadow h-200">
                        <div class="card-wrapper">
                            <a href="<?= $oNews->domain.$oNews->slug ?>" title="<?= htmlspecialchars($oNews->title) ?>">
                                <div class="image img-res img-api">
                                    <?php if ($oNews->image) { ?>
                                        <img src="<?= $oNews->image ?>" alt="<?= htmlspecialchars($oNews->title); ?>">
                                    <?php } ?>
                                </div>
                                <div class="title">
                                    <div class="txt">
                                        <strong><?= $oNews->category ?></strong>
                                        <h4><?= TextHelper::substr($oNews->title, 0, 80); ?></h4>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <?php
                    break;
                } ?>
                <ul class="block-list-image mt-20">
                    <?php foreach ($aOnlyArticles as $index => $oNews) { ?>
                        <li>
                            <a href="<?= $oNews->domain.$oNews->slug ?>" title="<?= htmlspecialchars($oNews->title); ?>">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="block-card-image img-res img-api">
                                            <?php if ($oNews->image) { ?>
                                                <img src="<?= $oNews->image ?>" alt="<?= htmlspecialchars($oNews->title); ?>">
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="col-8 no-p-left">
                                        <h4><?= TextHelper::substr($oNews->title, 0, 80); ?></h4>
                                    </div>
                                </div>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
                <div class="list-more">
                    <a href="<?= $oApiList->read_more_url ?>" title="Zobacz więcej">Więcej</a>
                </div>
            </div>
        </div>
    </div>
</section>