<section id="main-slider" data-item="main-slider" class="block block-title block-subtitle block-dark block-border-top-danger">
    <div class="container">
        <div class="block-slider">
            <h2 class="mr-auto">Defence24 <span class="line"></span></h2>
            <div class="mobile-slider">
                <?php foreach ($aReturnArticleLists as $serviceIndex => $aService) { ?>
                    <?php if ($serviceIndex == 0) { ?>
                        <?php $i = 0; ?>
                        <?php foreach ($aService['articles'] as $oNews) { 

                            $domain = $oNews->domain ?? '';
                            ?>
                            <?php if ($i == 0) { ?>
                                <div class="block-card shadow h-200">
                                    <div class="card-wrapper">

                                        <a href="<?= $aService["service"]->domain.$oNews->slug ?>" title="<?= htmlspecialchars($oNews->title) ?>" 
                                            data-url="<?=$oNews->url?>"
                                            data-domain="<?= $domain; ?>"
                                            data-trim="<?= trim( $domain, "/").$oNews->url ?>"



                                            > 
                                            <div class="image img-res img-api">
                                                <?php if ($oNews->image) { ?>
                                                    <img src="<?= $oNews->image ?>" alt="<?= htmlspecialchars($oNews->title) ?>">
                                                <?php } ?>
                                            </div>
                                            <div class="title">
                                                <div class="txt">
                                                    <strong><?= $oNews->category ?></strong>
                                                    <h4><?= $oNews->title ?></h4>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <?php $i++; ?>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
                <ul class="block-list-image mt-20">
                    <?php foreach ($aReturnArticleLists as $serviceIndex => $aService) { ?>
                        <?php if ($serviceIndex == 0) { ?>
                            <?php $i = 0; ?>
                            <?php foreach ($aService['articles'] as $oNews) {

                                $domain = $oNews->domain ?? '';

                                ?>
                                <?php if ($i > 0) { ?>
                                    <li>
                                        <a href="<?= $aService["service"]->domain.$oNews->slug ?>" title="<?= htmlspecialchars($oNews->title) ?>"
                                            data-url="<?=$oNews->url?>"
                                            data-domain="<?= $domain; ?>"
                                            data-trim="<?= trim( $domain, "/").$oNews->url ?>"
                                            > 
                                            <div class="row">
                                                <div class="col-4">
                                                    <div class="block-card-image img-res  img-api-height">
                                                        <?php if ($oNews->image) { ?>
                                                            <img src="<?= $oNews->image ?>" alt="<?= htmlspecialchars($oNews->title) ?>">
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="col-8 no-p-left">
                                                    <h4><?= $oNews->title ?></h4>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                <?php } ?>
                                <?php $i++; ?>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</section>