<?php
    $oAuthorRowset = (object) \Yii::$app->db_api->createCommand("SELECT * FROM `authors`")->queryAll();
    \Yii::$app->db_api->close();
?>
<h2><span>Redakcja</span><span class="line"></span></h2>
<div class="col-12">
    <ul class="editorial-list mt-30" data-item="group-editorial">
        <?php foreach($oAuthorRowset as $oAuthor){ ?>
            <li>
                <span><?= $oAuthor->firstname ?> <?= $oAuthor->surname ?><small><?= $oAuthor->position ?></small></span>
            </li>
        <?php } ?>
    </ul>
</div>