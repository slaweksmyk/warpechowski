<h2><span><a href="<?= $oApiList->read_more_url ?>">Defence24.com</a></span><span class="line line-danger"></span></h2>

<?php foreach ($aOnlyArticles as $index => $oNews) { ?>
    <div class="block-card min col-12 h-300 mt-30 no-p">
        <div class="card-wrapper">
            <a href="<?= $oNews->domain.$oNews->slug ?>" title="<?= htmlspecialchars($oNews->title) ?>">
                <div class="image img-res">
                    <?php if ($oNews->image) { ?>
                        <img src="<?= $oNews->image ?>" alt="<?= $oNews->title; ?>">
                    <?php } ?>
                </div>
                <div class="title">
                    <strong><?= $oNews->category; ?></strong>
                    <h4><?= $oNews->title; ?></h4>
                </div>
            </a>
            <div class="social d-flex justify-content-end align-content-end text-right">
                <a href="https://www.facebook.com/sharer/sharer.php?u=<?= $oNews->domain.$oNews->slug ?>" title="Udostępnij na facebooku"><i class="icon icon-icon-facebook-small"></i></a> 
                <a href="https://twitter.com/home?status=<?= $oNews->domain.$oNews->slug ?>" title="Udostępnij na twitterze"><i class="icon icon-icon-twitter-small"></i></a>
            </div>
        </div>
    </div>
<?php break; } ?>