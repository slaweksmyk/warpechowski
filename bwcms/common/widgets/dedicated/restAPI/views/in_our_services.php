<?php
use common\helpers\TextHelper;

foreach ($aReturnArticleLists as $serviceIndex => $aService) {
    if ($serviceIndex == 0) {
        $this->registerJs(
                "        $('[data-item=\"slider\"][data-id=\"slider-1-0\"] [data-item=\"slider-1-0-aside\"]').slick({
                    dots: false,
                    infinite: true,
                    speed: 300,
                    autoplay: true,
                    draggable: false,
                    autoplaySpeed: 2000,
                    slidesToShow: 4,
                    adaptiveHeight: true,
                    vertical: true,
                    arrows: true,
                    nextArrow: $('[data-arrow=\"slider-1-0-top\"].slider-aside-arrow-top'),
                    prevArrow: $('[data-arrow=\"slider-1-0-bottom\"].slider-aside-arrow-bottom'),
                });
                $('[data-item=\"slider\"][data-id=\"slider-1-0\"] > .slider-loader').hide();
                $('[data-item=\"slider\"][data-id=\"slider-1-0\"] [data-item=\"slider-content\"]').animate({opacity: 1});"
        );
    } else {
        $this->registerJs(
                "$('[data-action=\"change-slider\"][data-id=\"slider-1-{$serviceIndex}\"]').click(function () {
                if (!$('[data-item=\"slider\"][data-id=\"slider-1-{$serviceIndex}\"] [data-item=\"slider-content\"]').hasClass(\"slick\")) {
                    $('[data-item=\"slider\"][data-id=\"slider-1-{$serviceIndex}\"] .slider-loader').show();
                    $('[data-item=\"slider\"][data-id=\"slider-1-{$serviceIndex}\"] [data-item=\"slider-content\"]').animate({opacity: 0});
                    setTimeout(function () {
                        $('[data-item=\"slider\"][data-id=\"slider-1-{$serviceIndex}\"] [data-item=\"slider-1-{$serviceIndex}-aside\"]').slick({
                            dots: false,
                            infinite: true,
                            speed: 300,
                            autoplay: true,
                            draggable: false,
                            autoplaySpeed: 2000,
                            slidesToShow: 4,
                            adaptiveHeight: true,
                            vertical: true,
                            arrows: true,
                            nextArrow: $('[data-arrow=\"slider-1-{$serviceIndex}-top\"].slider-aside-arrow-top'),
                            prevArrow: $('[data-arrow=\"slider-1-{$serviceIndex}-bottom\"].slider-aside-arrow-bottom'),
                        });
                        $('[data-item=\"slider\"][data-id=\"slider-1-{$serviceIndex}\"] .slider-loader').hide();
                        $('[data-item=\"slider\"][data-id=\"slider-1-{$serviceIndex}\"] [data-item=\"slider-content\"]').animate({opacity: 1}).addClass('slick');
                    }, 800);
                }
            });"
        );
    }
}
?>

<section id="main-slider" data-item="main-slider" class="block block-title block-dark block-border-top-danger">
    <div class="container">
        <div class="block-slider">
            <div class="d-flex">
                <h2 class="mr-auto">W naszych serwisach</h2>
                <ul class="nav-slider-logo nav nav-pills mb-3 justify-content-end" id="nav-1" role="tablist">
                    <?php foreach ($aReturnArticleLists as $serviceIndex => $aServiceData) { ?>
                        <li class="nav-item">
                            <a class="nav-link <?= $serviceIndex == 0 ? 'active' : '' ?>" id="slider-1-<?= $serviceIndex ?>-tab"  data-action="change-slider" data-id="slider-1-<?= $serviceIndex ?>"  data-toggle="pill" href="#slider-1-<?= $serviceIndex ?>" role="tab" aria-controls="slider-1-<?= $serviceIndex ?>" aria-expanded="true"><img src="<?= '/img/logo/'.$aServiceData["service"]->class.'/'.$aServiceData["service"]->logo_light ?>" alt="<?= $aServiceData["service"]->title ?>"></a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            <div class="block-slider-content d-flex flex-nowrap">
                <div class="block-slider-arrow d-flex justify-content-center align-items-center" data-action="slider-previous" data-id="nav-1">
                    <i class="icon icon-slider-arrow-left"></i>
                </div>
                <div class="block-slider-tab">
                    <div class="tab-content" id="slider-1-1-tabContent">
                        <?php foreach ($aReturnArticleLists as $serviceIndex => $aService) { ?>
                            <div class="tab-pane <?= $serviceIndex == 0 ? 'show active' : '' ?> fade" data-item="slider" data-id="slider-1-<?= $serviceIndex ?>" id="slider-1-<?= $serviceIndex ?>" role="tabpanel" aria-labelledby="slider-1-<?= $serviceIndex ?>-tab">
                                <!-- START SLIDER -->
                                <div class="slider-loader">
                                    <div class="loading loading--double"></div>
                                </div>
                                <div class="d-flex" data-item="slider-content" style="opacity: 0">
                                    <div class="col-sm-8">
                                        <?php if (isset($aService['articles'][0])) { ?>
                                            <div class="block-card shadow big-title  h-400">
                                                <div class="card-wrapper">
                                                    <a href="<?= $aService["service"]->domain.$aService['articles'][0]->slug ?>" title="<?= $aService['articles'][0]->title ?>"> 
                                                        <div class="image img-res">
                                                            <img src="<?= $aService['articles'][0]->image ?>" alt="<?= $aService['articles'][0]->title ?>">
                                                        </div>
                                                        <div class="title">
                                                            <div class="txt">
                                                                <strong><?= $aService['articles'][0]->category ?></strong>
                                                                <h3><?= TextHelper::substr($aService['articles'][0]->title, 0, 60) ?></h3>
                                                                <?php /*<p><?= TextHelper::substr($aService['articles'][0]->short_description, 0, 300) ?></p> */ ?>
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <div class="social d-flex justify-content-start align-content-start text-left">
                                                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?= $aService["service"]->domain.$aService['articles'][0]->slug ?>" title="Udostępnij na facebooku"><i class="icon icon-icon-facebook-small"></i></a> 
                                                        <a href="https://twitter.com/home?status=<?= $aService["service"]->domain.$aService['articles'][0]->slug ?>" title="Udostępnij na twitterze"><i class="icon icon-icon-twitter-small"></i></a>
                                                    </div> 
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="col-sm-4 slider-aside-size">
                                        <div class="slider-aside" data-item="slider-1-<?= $serviceIndex ?>-aside">
                                            <?php $i = 0; ?>
                                            <?php foreach ($aService['articles'] as $oNews) { ?>
                                                <?php if ($i > 0) { ?>
                                                    <div class="block-card nb vertical text-light v-80">
                                                        <div class="card-wrapper">
                                                            <a href="<?= $aService["service"]->domain.$oNews->slug ?>" title="<?= htmlspecialchars($oNews->title) ?>">
                                                                <div class="image img-res img-api-height">
                                                                    <?php if ($oNews->image) { ?>
                                                                        <img src="<?= $oNews->image ?>" alt="<?= htmlspecialchars($oNews->title) ?>">
                                                                    <?php } ?>
                                                                </div>
                                                                <div class="title">
                                                                    <h4><?= TextHelper::substr($oNews->title, 0, 50) ?></h4> 
                                                                </div>
                                                            </a>
                                                            <div class="social d-flex justify-content-end align-content-end text-right">
                                                                <a href="https://www.facebook.com/sharer/sharer.php?u=<?= $aService["service"]->domain.$oNews->slug ?>" title="Udostępnij na facebooku" target="_blank"><i class="icon icon-icon-facebook-small"></i></a> 
                                                                <a href="https://twitter.com/home?status=<?= $aService["service"]->domain.$oNews->slug ?>" title="Udostępnij na twitterze" target="_blank"><i class="icon icon-icon-twitter-small"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <?php $i++; ?>
                                            <?php } ?>
                                        </div>
                                        <div class="slider-aside-arrow d-flex">
                                            <div data-arrow="slider-1-<?= $serviceIndex ?>-top" class="slider-aside-arrow-block mr-auto slider-aside-arrow-top d-flex justify-content-center align-items-center"><i class="icon icon-arrow-slider-top"></i></div>
                                            <div data-arrow="slider-1-<?= $serviceIndex ?>-bottom" class="slider-aside-arrow-block slider-aside-arrow-bottom d-flex justify-content-center align-items-center"><i class="icon icon-arrow-slider-bottom"></i></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END SLIDER -->
                            </div>
                            <?php $serviceIndex++; ?>
                        <?php } ?>
                    </div>
                </div>
                <div class="block-slider-arrow d-flex justify-content-center align-items-center" data-action="slider-next"  data-id="nav-1"><!-- !!!!!!!!! nav ID -->
                    <span class="block-slider-arrow-span"><i class="icon icon-slider-arrow-right"></i></span>
                </div>
            </div>
        </div>
    </div>
</section>