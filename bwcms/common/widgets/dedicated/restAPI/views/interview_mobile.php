<div class="block-slider">
    <h2 class="mr-auto"><a href="<?= $oApiList->read_more_url ?>" title="Wywiady">Wywiady</a> <span class="line"></span></h2>
    <div class="mobile-slider">
        <ul class="block-list-image mt-20">
            <?php foreach ($aOnlyArticles as $index => $oNews) { ?>
                <li>
                    <a href="<?= $oNews->url; ?>" title="<?= htmlspecialchars($oNews->title) ?>">
                        <div class="row">
                            <div class="col-4">
                                <div class="block-card-image img-res img-api-height">
                                    <?php if ($oNews->image) { ?>
                                        <img src="<?= $oNews->image ?>" alt="<?= htmlspecialchars($oNews->title) ?>">
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-8 no-p-left">
                                <h4><?= $oNews->title; ?></h4>
                            </div>
                        </div>
                    </a>
                </li>
            <?php } ?>
        </ul>
        <div class="list-more">
            <a href="<?= $oNews->url; ?>" title="Więcej" >Więcej</a>
        </div>
    </div>
</div>