<?php
    $oAuthorRowset = \Yii::$app->db_api->createCommand("SELECT * FROM `authors` WHERE `type` = 'blogger'")->queryAll();
    \Yii::$app->db_api->close();
?>

<ul class="reviewer-list mt-30" data-item="reviewer-list" data-slick='{"slidesToShow": 6}'>
    <?php foreach($oAuthorRowset as $oAuthor){ ?>
        <?php $oAuthor = (object) $oAuthor; ?>
        <li>
            <a href="<?= $oAuthor->url ?>" title="<?= $oAuthor->firstname ?> <?= $oAuthor->surname ?>" class="d-flex justify-content-end align-content-center flex-column">
                <span><?= $oAuthor->firstname ?> <?= $oAuthor->surname ?></span>
                <small>Czytaj więcej</small>
            </a>
            <?php if($oAuthor->photo){ ?>
                <div class="img-res img-api">
                    <img src="<?= $oAuthor->photo ?>" alt="<?= $oAuthor->firstname ?> <?= $oAuthor->surname ?>">
                </div>
            <?php } ?>
        </li>
    <?php } ?>
</ul>