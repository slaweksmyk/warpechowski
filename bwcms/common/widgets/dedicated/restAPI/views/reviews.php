<div class="main-interviews-content">
    <h2><span><a href="<?= $oApiList->read_more_url ?>">Opinie i komentarze</a></span><span class="line"></span></h2>
    <div class="row block-reviews-panel mt-30">
        <div class="col-6 no-p-right">
            <ul data-item="reviews-list">
                <?php foreach ($aOnlyArticles as $index => $oNews) { ?>
                    <li <?php if ($index == 0) { ?>class="active"<?php } ?>><a href="<?= $oNews->domain.$oNews->slug ?>" title="<?= htmlspecialchars($oNews->title) ?>" data-action="change-reviews" data-src="<?php if ($oNews->image) { ?><?= $oNews->image ?><?php } ?>"><span class="flag flag-danger" style="color: <?= $oNews->color ?? "" ?>;"><?= isset($oNews->prefix) ? ($oNews->prefix !="D24" ? $oNews->prefix : '') : ""  ?></span> <?= $oNews->title ?></a></li>
                <?php } ?>
            </ul>
        </div>
        <div class="col-6 no-p-left">
            <?php foreach ($aOnlyArticles as $index => $oNews) { ?>
                <?php if ($index == 0) { ?>
                    <div class="block-reviews-image">
                        <div class="img-res img-api">
                            <img data-item="reviews-image" src="<?php if ($oNews->image) { ?><?= $oNews->image ?><?php } ?>" alt="<?= htmlspecialchars($oNews->title) ?>">
                        </div>
                        <div class="card-social d-flex justify-content-end align-content-end text-right">
                            <a href="https://www.facebook.com/sharer/sharer.php?u=<?= $oNews->domain.$oNews->slug; ?>" data-item="reviews-fb-share" title="Udostępnij na facebooku"><i class="icon icon-icon-facebook-small"></i></a> 
                            <a href="https://twitter.com/home?status=<?= $oNews->domain.$oNews->slug; ?>" data-item="reviews-tt-share" title="Udostępnij na twitterze"><i class="icon icon-icon-twitter-small"></i></a>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</div>

<script>
    $('[data-item="reviewer-list"]').slick({
        dots: false,
        infinite: true,
        speed: 300,
        autoplay: false,
        draggable: true,
        autoplaySpeed: 2000,
        slidesToShow: 6,
        adaptiveHeight: true,
        arrows: true
    });
    $('[data-action="change-reviews"]').hover(function () {

        var src = $(this).data('src');
        var href = $(this).attr('href');
        var title = $(this).attr('title');

        $('[data-item="reviews-list"] li').removeClass('active');
        $(this).parent().addClass('active');
        $('[data-item="reviews-image"]').attr('src', src);
        $('[data-item="reviews-image"]').attr('alt', title);
        $('[data-item="reviews-fb-share"]').attr('href', href);
        $('[data-item="reviews-tt-share"]').attr('href', href);

    });
</script>