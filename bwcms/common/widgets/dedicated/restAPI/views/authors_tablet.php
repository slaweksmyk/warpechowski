<?php
    $oAuthor = (object) \Yii::$app->db_api->createCommand("SELECT * FROM `authors` WHERE `id` = {$oApiList->author_id}")->queryOne();
    \Yii::$app->db_api->close();
?>
<?php if($oAuthor->type == "author"){ ?>
    <section id="news" data-item="news" class="section-top block block-title block-subtitle block-news ">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="news-author row ">
                        <div class="col-4">
                            <div class="row news-author-content d-flex justify-content-start align-items-center no-border" >
                                <div class="col-3 ">
                                    <div class="img-res news-author-image">
                                        <img src="<?= $oAuthor->photo ?>" alt="<?= $oAuthor->firstname ?> <?= $oAuthor->surname ?>">
                                    </div>
                                </div>
                                <div class="col-9 news-author-info">
                                    <span><?= $oAuthor->firstname ?> <?= $oAuthor->surname ?></span>
                                    <a href="mailto:<?= $oAuthor->email ?>" title="Napisz do mnie e-mail"><?= $oAuthor->email ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <?php foreach ($aOnlyArticles as $index => $oArticle) { ?>
                        <?php if($index == 0){ ?>
                            <div class=" block-card background b-vertical h-300 mt-30">
                                <div class="card-wrapper">
                                    <a href="<?= $oArticle->getUrl() ?>" title="<?= $oArticle->title ?>">
                                        <?php if($oArticle->image){ ?>
                                            <div class="image img-res">
                                                <img src="<?= $oArticle->image ?>" alt="<?= $oArticle->title ?>">
                                            </div>
                                        <?php } ?>
                                        <div class="title">
                                            <div class="txt">
                                                <strong><?= $oArticle->category ?? "" ?></strong>
                                                <h4><?= $oArticle->title ?></h4>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="social d-flex justify-content-end align-content-end text-right">
                                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?= $oArticle->url ?>" title="Udostępnij na facebooku"><i class="icon icon-icon-facebook-small"></i></a> 
                                        <a href="https://twitter.com/home?status=<?= $oArticle->url ?>" title="Udostępnij na twitterze"><i class="icon icon-icon-twitter-small"></i></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>

                    <div class="row">
                        <?php foreach ($aOnlyArticles as $index => $oArticle) { ?>
                            <?php if($index > 0 && $index < 5){ ?>
                                <div class="col-6 block-card shadow h-200 mt-20">
                                    <div class="card-wrapper">
                                        <a href="<?= $oArticle->getUrl() ?>" title="<?= $oArticle->title ?>">
                                            <?php if($oArticle->image){ ?>
                                                <div class="image img-res">
                                                    <img src="<?= $oArticle->image?>" alt="<?= $oArticle->title ?>">
                                                </div>
                                            <?php } ?>
                                            <div class="title">
                                                <div class="txt">
                                                    <strong><?= $oArticle->category ?? "" ?></strong>
                                                    <h4><?= $oArticle->title ?></h4>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="social d-flex justify-content-end align-content-end text-right">
                                            <a href="https://www.facebook.com/sharer/sharer.php?u=<?= $oArticle->url ?>" title="Udostępnij na facebooku"><i class="icon icon-icon-facebook-small"></i></a> 
                                            <a href="https://twitter.com/home?status=<?= $oArticle->url ?>" title="Udostępnij na twitterze"><i class="icon icon-icon-twitter-small"></i></a>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-12">
                    <ul class="list dotted mt-30">
                        <?php foreach ($aOnlyArticles as $index => $oArticle) { ?>
                            <?php if($index > 5){ ?>
                                <li>
                                    <a href="<?= $oArticle->url ?>" title="<?= $oArticle->title ?>"><?= $oArticle->title ?></a>
                                </li>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>
<?php } else { ?>
    <section id="author" data-item="news" class="section-top block block-title block-subtitle block-news ">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2><span><?= $oAuthor->firstname ?> <?= $oAuthor->surname ?></span><div class="line"></div></h2>
                    <div class="author-profil">
                        <div class="image mt-30">
                            <img src="<?= $oAuthor->photo ?>" alt="<?= $oAuthor->firstname ?> <?= $oAuthor->surname ?>">
                        </div>
                        <?= $oAuthor->description ?>
                    </div>
                </div>
                <div class="col-12">
                    <h2><span>Wpisy autora</span><div class="line"></div></h2>
                    <div class="row">
                        <div class="col-6"> 
                            <div class="row">
                                <?php foreach ($aOnlyArticles as $index => $oArticle) { ?>
                                    <?php if($index < 4){ ?>
                                        <div class="col-12 block-card shadow h-200 mt-30">
                                            <div class="card-wrapper">
                                                <a href="<?= $oArticle->url ?>" title="<?= $oArticle->title ?>">
                                                    <?php if($oArticle->image){ ?>
                                                        <div class="image img-res">
                                                            <img src="<?= $oArticle->image ?>" alt="<?= $oArticle->title ?>">
                                                        </div>
                                                    <?php } ?>
                                                    <div class="title">
                                                        <div class="txt">
                                                            <strong><?= $oArticle->category ?? "" ?></strong>
                                                            <h4><?= $oArticle->title ?></h4>
                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="social d-flex justify-content-end align-content-end text-right">
                                                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?= $oArticle->url ?>" title="Udostępnij na facebooku"><i class="icon icon-icon-facebook-small"></i></a> 
                                                    <a href="https://twitter.com/home?status=<?= $oArticle->url ?>" title="Udostępnij na twitterze"><i class="icon icon-icon-twitter-small"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-6">
                            <ul class="list dotted mt-30">
                                <?php foreach ($aOnlyArticles as $index => $oArticle) { ?>
                                    <?php if($index > 4){ ?>
                                        <li>
                                            <a href="<?= $oArticle->url ?>" title="<?= $oArticle->title ?>"><?= $oArticle->title ?></a>
                                        </li>
                                    <?php } ?>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php 
        $oAuthorRowset = \Yii::$app->db_api->createCommand("SELECT * FROM `authors` WHERE `id` != {$oApiList->author_id} AND `type` != 'author'")->queryAll();
        \Yii::$app->db_api->close();
    ?>
    <section id="author" data-item="news" class="section-top block block-title block-subtitle block-news ">
        <div class="container">

            <h2><span>Pozostali blogerzy</span><div class="line"></div></h2>
            <div class="row">
                <ul class="reviewer-list mt-30" data-item="reviewer-list" data-slick='{"slidesToShow":6, "slidesToScroll": 1}'>
                    <?php foreach($oAuthorRowset as $oAuthor){ ?>
                        <?php $oAuthor = (object) $oAuthor; ?>
                        <li>
                            <a href="<?= $oAuthor->url ?>" title="<?= $oAuthor->firstname ?> <?= $oAuthor->surname ?>" class="d-flex justify-content-end align-content-center flex-column">
                                <span><?= $oAuthor->firstname ?> <?= $oAuthor->surname ?></span>
                                <small>Czytaj więcej</small>
                            </a>
                            <?php if($oAuthor->photo){ ?>
                                <div class="img-res">
                                    <img src="<?= $oAuthor->photo ?>" alt="<?= $oAuthor->firstname ?> <?= $oAuthor->surname ?>">
                                </div>
                            <?php } ?>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </section>
<?php } ?>

