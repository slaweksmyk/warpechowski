<div class="main-interviews-content mt-30">
    <h2><span><a href="<?= $oApiList->read_more_url ?>">Opinie i komentarze</a></span><span class="line"></span></h2>
    <div class="row block-reviews-panel mt-15">
        <div class="col-12">
            <ul data-item="reviews-list">
                <?php foreach ($aOnlyArticles as $index => $oNews) { ?>
                    <li><a href="<?= $oNews->domain.$oNews->slug ?>" title="<?= htmlspecialchars($oNews->title) ?>" data-action="change-reviews" data-src="<?php if ($oNews->image) { ?><?= $oNews->image ?><?php } ?>"><?= $oNews->title ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>

<script>
    $('[data-item="reviewer-list"]').slick({
        dots: false,
        infinite: true,
        speed: 300,
        autoplay: false,
        draggable: true,
        autoplaySpeed: 2000,
        slidesToShow: 4,
        adaptiveHeight: true,
        arrows: true
    });
    $('[data-action="change-reviews"]').hover(function () {

        var src = $(this).data('src');
        var href = $(this).attr('href');

        $('[data-item="reviews-list"] li').removeClass('active');
        $(this).parent().addClass('active');
        $('[data-item="reviews-image"]').attr('src', src);
        $('[data-item="reviews-fb-share"]').attr('href', href);
        $('[data-item="reviews-tt-share"]').attr('href', href);

    });
</script>