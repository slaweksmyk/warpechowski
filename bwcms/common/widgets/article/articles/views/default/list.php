
<section class="performances">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-11 col-lg-6 col-xl-5">
                <div class="content content-left">
                    <h1>Performances</h1>
                    <form>
                        <input class="form-control" type="text" name="search" value="" placeholder="wpisz szukaną frazę..." />
                        <input class="btn" type="submit" value="szukaj" />
                    </form>

                    <h2>wybierz okres twórczości</h2>
                    <div id="slider"></div>

                    <ul class="performances-items">
                        <?php foreach($oArticleRowset as $index => $oArticle){ ?>
                        <li data-number="#<?= $oArticle->number ?>">
                            <a href="<?= $oArticle->getAbsoluteUrl() ?>" title="Przejdź do <?= $oArticle->title ?>">
                                <h3><?= $oArticle->title ?></h3><br />
                                <p><?= $oArticle->place ?>, <?= $oArticle->city ?>, <?= $oArticle->date_text ?> <?= $oArticle->year ?></p>
                            </a>
                        </li>
                        <?php } ?>

                    </ul>

                </div> 
                <div class="mouse">
                    <svg width="30" height="60">
                        <path class="mouse_path" d="M 2 16 Q 2 2 15 2 Q 28 2 28 16 L 28 20 Q 28 34 15 34 Q 2 34 2 20 Z" stroke-linecap="round" />
                        <path class="line" d="M 14 10 L16 10 L 16 16 L 14 16" />
                        <path class="arrow_1 arrow" d="M 11 39 L19 39 L15 44 Z" />
                        <path class="arrow_2 arrow" d="M 11 47 L19 47 L15 52 Z" />
                        <path class="arrow_3 arrow" d="M 11 55 L19 55 L15 60 Z" />
                    </svg>
                </div>
            </div>
        </div>

    </div>
</section>