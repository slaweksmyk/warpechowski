<?php
use common\modules\files\models\File;
?>
<section class="performance">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-4 col-lg-6 performance-left">
                <div class="gallery">
                    <?php
                    if ($oArticle->getGallery()) { 
                    if ($oArticle->getGallery()->galleryItems) { 
                    ?>
                    <?php foreach ($oArticle->getGallery()->galleryItems as $index => $image) { ?>
                    
                    <div class="slide">
                        <a <?php if ($image->extlink) { ?>class="is_video"<?php } ?> data-caption='<span data-author="<?= strip_tags($image->description) ?>">#<?= $oArticle->number ?> <?= strip_tags($oArticle->short_description) ?></span><h3><?= $oArticle->title ?></h3><p><?= $oArticle->date_text ?> <?= $oArticle->year ?>, <?= $oArticle->place ?>, <?= $oArticle->city ?></p>' title="<?= $oArticle->title ?>" <?php if ($image->extlink) { ?>data-video="<?= $image->extlink ?>"<?php } ?> href="<?= File::getThumbnail($image->file_id, 800, 800, "matched") ?>">
                            <img alt="" src="<?= File::getThumbnail($image->file_id, 400, 400, "matched") ?>" />
                        </a>
                    </div>
                    <?php } ?>
                    <?php } ?>
                    <?php } ?>

                </div>
            </div>
            <div class="col-12 col-md-8 col-lg-6 performance-right">
                <div class="content">
                    <h1><?= $oArticle->title ?></h1>
                    <div class="short_description">
                        <p>
                            <?= $oArticle->place ?>, <?= $oArticle->city ?>,<br />
                            <?= $oArticle->date_text ?> <?= $oArticle->year ?>
                        </p>
                    </div>
                    <?= $oArticle->full_description ?>

                </div> 
            </div>
        </div>

    </div>
    <div class="mouse">
        <svg width="30" height="60">
            <path class="mouse_path" d="M 2 14 Q 2 2 14 2 Q 25 2 25 14 L 25 22 Q 25 34 14 34 Q 2 34 2 22 Z" stroke-linecap="round" />
            <path class="line" d="M 13 10 L15 10 L 15 16 L 13 16" />
            <path class="arrow_1 arrow" d="M 10 39 L18 39 L14 44 Z" />
            <path class="arrow_2 arrow" d="M 10 47 L18 47 L14 52 Z" />
            <path class="arrow_3 arrow" d="M 10 55 L18 55 L14 60 Z" />
        </svg>
    </div>
</section>