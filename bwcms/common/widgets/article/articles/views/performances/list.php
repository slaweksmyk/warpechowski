<?php
    use common\helpers\TranslateHelper;
?>
<section class="performances">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-11 col-lg-6 col-xl-5">
                <div class="content content-left">
                    <h1><?= $oCategory->name ?></h1>
                    <form>
                        <input class="form-control" type="text" name="search" value="<?= Yii::$app->request->get('search') ?>" placeholder="<?= TranslateHelper::t('SEARCH_PLACECHOLDER') ?>" />
                        <input type="hidden" name="min" value="<?= $min ?>" />
                        <input type="hidden" name="max" value="<?= $max ?>" />
                        <input class="btn" type="submit" value="<?= TranslateHelper::t('SEARCH_BTN') ?>" />
                    </form>

                    <h2><?= TranslateHelper::t('PERIOD_OF_CREATIVITY') ?></h2>
                    <div aria-controls="performances" id="slider" data-min="<?= $min_start ?>" data-max="<?= $max_start ?>"></div>

                    <ul class="performances-items" id="performances" aria-live="polite">
                        <?php foreach($oArticleRowset as $index => $oArticle){ ?>
                        <li data-number="#<?= $oArticle->number ?> <?= strip_tags($oArticle->short_description) ?>" class="<?php if ($oArticle->is_more_info == 1) { ?>is_more_info<?php } ?>">
                            <?php if ($oArticle->is_more_info == 1) { ?>
                            <a href="<?= $oArticle->getAbsoluteUrl() ?>" title="<?= TranslateHelper::t('GO_TO') ?> <?= $oArticle->title ?>">
                                <h3><?= $oArticle->title ?></h3><br />
                                <p><?= $oArticle->place ?>, <?= $oArticle->city ?>,<br />
                                <?= $oArticle->date_text ?> <?= $oArticle->year ?></p>
                            </a>
                            <?php } else { ?>
                                <h3><?= $oArticle->title ?></h3><br />
                                <p><?= $oArticle->place ?>, <?= $oArticle->city ?>,<br />
                                <?= $oArticle->date_text ?> <?= $oArticle->year ?></p>
                            <?php } ?>
                        </li>
                        <?php } ?>

                    </ul>

                </div> 
            </div>
        </div>

    </div>
    <div class="mouse">
        <svg width="30" height="60">
            <path class="mouse_path" d="M 2 14 Q 2 2 14 2 Q 25 2 25 14 L 25 22 Q 25 34 14 34 Q 2 34 2 22 Z" stroke-linecap="round" />
            <path class="line" d="M 13 10 L15 10 L 15 16 L 13 16" />
            <path class="arrow_1 arrow" d="M 10 39 L18 39 L14 44 Z" />
            <path class="arrow_2 arrow" d="M 10 47 L18 47 L14 52 Z" />
            <path class="arrow_3 arrow" d="M 10 55 L18 55 L14 60 Z" />
        </svg>
    </div>
</section>