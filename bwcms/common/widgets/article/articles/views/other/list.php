<?php
    use common\helpers\TranslateHelper;
?>
<section class="archiwum">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="content">
                    <h1><?= $oCategory->name ?></h1>

                    <div class="row">
                        <?php foreach($oArticleRowset as $index => $oArticle){ ?>
                        <div class="col-item">
                            <a href="<?= $oArticle->getAbsoluteUrl() ?>" title="<?= TranslateHelper::t('GO_TO') ?> <?= $oArticle->title ?>">
                                <?php if ($oArticle->thumbnail_id) { ?>
                                <div class="img">
                                    <img src="<?= $oArticle->getThumbnail(500, 500, "matched") ?>" alt="<?= $oArticle->title ?>" />
                                </div>
                                <?php } ?>
                                <div class="text">
                                    <h2><?= $oArticle->title ?></h2>
                                    <p>
                                        <?= $oArticle->date_text ?> <?= $oArticle->year ?><br />
                                        <?= $oArticle->technique ?><br />
                                        <?= $oArticle->size ?><br />
                                        <?= $oArticle->dimensions ?>
                                    </p>
                                </div>
                            </a>
                        </div>
                        <?php } ?>
                    </div>

                </div> 
            </div>
        </div>

    </div>
    <div class="mouse">
        <svg width="30" height="60">
            <path class="mouse_path" d="M 2 14 Q 2 2 14 2 Q 25 2 25 14 L 25 22 Q 25 34 14 34 Q 2 34 2 22 Z" stroke-linecap="round" />
            <path class="line" d="M 13 10 L15 10 L 15 16 L 13 16" />
            <path class="arrow_1 arrow" d="M 10 39 L18 39 L14 44 Z" />
            <path class="arrow_2 arrow" d="M 10 47 L18 47 L14 52 Z" />
            <path class="arrow_3 arrow" d="M 10 55 L18 55 L14 60 Z" />
        </svg>
    </div>
</section>