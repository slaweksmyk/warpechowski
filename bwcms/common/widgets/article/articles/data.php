<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\categories\models\ArticlesCategories;
?>

<div class="widget-main-form" >

    <?php $form = ActiveForm::begin(); ?>
    
    <!-- ADD WIDGET CUSTOM DATA -->
    <div class="row" style="background: #f8f8f8; padding-top: 15px; padding-bottom: 15px; " >
        <div class="col-3">
            <?php 
                $aCategories = [];
                foreach(ArticlesCategories::find()->all() as $oCategory){
                    $aCategories[$oCategory->id] = $oCategory->name;
                }
                echo $form->field($model, 'data[category_id]')->dropDownList($aCategories)->label( Yii::t('backend', 'category'));
            ?>
        </div>
        <div class="col-3">
            <?php 
                echo $form->field($model, 'data[order]')->dropDownList(
                        [
                            "sort ASC" => "Kolejności rosnąco", 
                            "sort DESC" => "Kolejności malejąco", 
                            "date_display ASC" => "Data rosnąco", 
                            "date_display DESC" => "Data malejąco", 
                            "title ASC" => "Alfabetycznie A-Z", 
                            "title DESC" => "Alfabetycznie Z-A", 
                            "id ASC" => "Kolejności dodania - rosnąco", 
                            "id DESC" => "Kolejności dodania - malejąco", 
                            "RAND()" => "Losowo", 
                            "number ASC" => "Numer rosnąco", 
                            "number DESC" => "Numer malejąco"
                            ]
                        )->label("Sortowanie");
            ?>
        </div>
    </div>
    <!-- END CUSTOM DATA -->
    
    <br>
    <div class="form-group">
        <?= Html::submitButton( Yii::t('backend', 'update'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>