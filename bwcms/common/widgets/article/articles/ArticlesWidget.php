<?php

namespace common\widgets\article\articles;

use Yii;
use yii\data\Pagination;
use common\modules\articles\models\Article;
use common\modules\categories\models\ArticlesCategories;

class ArticlesWidget extends \common\hooks\yii2\bootstrap\Widget
{
    
    public $widget_item_id;
    public $category_id;
    public $layout;
    public $limit;
    public $order;
    
    private $slug;
    private $preview = false;

    public function init()
    {
        $session = Yii::$app->session;
        if($session->get('preview_mode')){ $this->preview = $session->get('preview_mode');}
        
        if(array_key_exists("slug", Yii::$app->params)){
            $this->slug = Yii::$app->params['slug'];
        }
        
        parent::init();
    }

    
    public function run()
    {
        if($this->slug && file_exists($this->getViewPath()."/{$this->layout}/single.php")){
            return $this->displaySingle();
        } else {
            return $this->displayList();
        }
    }
    
    private function displayList(){
        $oPage = Yii::$app->params['oPage'];
        $oArticleRowsetQuery = Article::find()->leftJoin('bwcms_xmod_articles_i18n', '`bwcms_xmod_articles_i18n`.`article_id` = `bwcms_xmod_articles`.`id`');
        //$oArticleRowsetQuery->andWhere("(active_from <= '".date('Y-m-d h:i:s')."') OR active_from IS NULL");
        //$oArticleRowsetQuery->andWhere("(active_to >= '".date('Y-m-d h:i:s')."') OR active_to IS NULL");
        
        //if(!$this->preview){
            $oArticleRowsetQuery->andWhere(['=', "bwcms_xmod_articles_i18n.status_id", 2]);
            $oArticleRowsetQuery->andWhere(['=', "bwcms_xmod_articles_i18n.language", Yii::$app->session->get('frontend_language', 'pl-PL')]);
        //}
        
        if ($oPage->id == 5) {
            $oArticleRowsetQuery->andWhere(['=', "bwcms_xmod_articles_i18n.is_more_info", 1]);
        }
        
        if(Yii::$app->request->get('search')){
            $search = Yii::$app->request->get('search');
            $oArticleRowsetQuery->andWhere(['LIKE', "bwcms_xmod_articles_i18n.title", $search]);
        }
        
        if(Yii::$app->request->get('min')){
            $min = Yii::$app->request->get('min');
            $oArticleRowsetQuery->andWhere(['>=', 'year', $min]);
        }
        
        if(Yii::$app->request->get('max')){
            $max = Yii::$app->request->get('max');
            $oArticleRowsetQuery->andWhere(['<=', 'year', $max]);
        }

        $oArticleRowsetQuery->andWhere(["OR", 
            ["IN", "category_id", $this->category_id],
            ["LIKE", "add_categories_hash", $this->category_id]
        ]);
        
        $oCategory = ArticlesCategories::find()->where(["id" => $this->category_id])->one();
        foreach(ArticlesCategories::find()->where(["=", "parent_id", $oCategory->id])->all() as $oCat){
            $oArticleRowsetQuery->andWhere(["OR", 
                ["IN", "category_id", $oCat->id],
                ["LIKE", "add_categories_hash", $oCat->id]
            ]);
        }

        $oArticleRowsetQuery->orderBy($this->order);
        
        $pageUrl = explode("?", Yii::$app->request->url);
        if(count($pageUrl) > 0){ $pageUrl = $pageUrl[0]; } else { $pageUrl = Yii::$app->request->url; }
        
        $sCountQuery = clone $oArticleRowsetQuery;
        $pages = new Pagination([
            'totalCount' => $sCountQuery->count(), 
            'pageSize' => $this->md_limit, 
            'pageSizeParam' => 'limit',
            'route' => $pageUrl
        ]);
        $oArticleRowset = $oArticleRowsetQuery->offset($pages->offset)->limit($pages->limit)->all();
        
        $min = 1965;
        $min_start = $min;
        if (Yii::$app->request->get('min')) {
            $min = Yii::$app->request->get('min');
        }
        
        $max = Article::find()->where(['=', 'category_id', 2])->max('year');
        $max_start = $max ;
        if (Yii::$app->request->get('max')) {
            $max = Yii::$app->request->get('max');
        }

        return $this->display([
            'pages' => $pages,
            'oCategory' => $oCategory,
            'oArticleRowset' => $oArticleRowset,
            'order' => $this->order,
            'min' => $min,
            'max' => $max,
            'min_start' => $min_start,
            'max_start' => $max_start
        ], "list");
    }
    
    private function displaySingle(){
        $oArticleQuery = Article::find()->leftJoin('bwcms_xmod_articles_i18n', '`bwcms_xmod_articles_i18n`.`article_id` = `bwcms_xmod_articles`.`id`')->where(["=", "bwcms_xmod_articles_i18n.slug", $this->slug]);
        
        if(!$this->preview){
            $oArticleQuery->andWhere(['=', "bwcms_xmod_articles_i18n.status_id", 2]);
        }
        
        $oArticle = $oArticleQuery->one();
        
        // Check if there is any article
        if(!$oArticle){ 
            throw new \yii\web\NotFoundHttpException();
        }
        
        return $this->display([
            'oArticle' => $oArticle
        ], "single");
    }
    
}
