<?php

use yii\helpers\Html;
use common\modules\articles\models\Article;

$oArticle = Article::findOne($model->data["article_id"]);
?>

<div class="widget-main-form" >

    <!-- ADD WIDGET CUSTOM DATA -->
    <div class="row" style="background: #f8f8f8; padding-top: 15px; padding-bottom: 15px; " >
        <div class="col-6">
            <div class="form-group">
                <label class="control-label" for="related">Artykuł</label>
                <?= $form->field($model, 'data[article_id]')->dropDownList(isset($model->data["article_id"]) ? [$model->data["article_id"] => $oArticle->title] : [], ["class" => "ajax-load-articles-single"])->label(false); ?>
            </div>
        </div>
    </div>
    <!-- END CUSTOM DATA -->

    <br>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'update'), ['class' => 'btn btn-primary']) ?>
    </div>

</div>