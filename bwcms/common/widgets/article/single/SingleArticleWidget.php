<?php
/**
 * Widget Article
 * 
 * PHP version 7
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @copyright (c) 2016 Krzysztof Borecki
 * @version 1.0
 */

namespace common\widgets\article\single;

use Yii;
use common\modules\articles\models\Article;

class SingleArticleWidget extends \common\hooks\yii2\bootstrap\Widget
{
    
    public $widget_item_id;
    public $article_id;
    public $layout = "default";
    
    private $preview = false;

    /**
     * Build widget
     */
    public function init()
    {
        $session = Yii::$app->session;
        if($session->get('preview_mode')){ $this->preview = $session->get('preview_mode'); }

        parent::init();
    }
    /**
     * Run widget
     */
    public function run()
    {
        $oArticle = Article::find()->where(["=", "bwcms_xmod_articles.id", $this->article_id])->one();
        
        // Check if there is any article
        if(!$oArticle){ return; }

        $oConfig = Yii::$app->params['oConfig'];

        return $this->display([
            'oArticle' => $oArticle,
            'config' => $oConfig
        ]);
    }
    
}