<?php

namespace common\widgets\article\category;

use Yii;
use yii\data\Pagination;
use common\modules\widget\models\WidgetItems;
use common\modules\articles\models\Article;
use common\modules\lists\models\ArticlesList;
use common\modules\lists\models\ArticlesListCategory;

class CategoryArticleWidget extends \common\hooks\yii2\bootstrap\Widget {

    public $widget_item_id;
    public $category_id;
    public $type_id;
    public $layout;
    public $limit;
    public $order;
    public $article_list_id;
    public $article_category_id;
    private $slug;
    private $preview = false;

    public function init() {
        $session = Yii::$app->session;
        if ($session->get('preview_mode')) {
            $this->preview = $session->get('preview_mode');
        }

        if (array_key_exists("slug", Yii::$app->params)) {
            $this->slug = Yii::$app->params['slug'];
        }

        parent::init();
    }

    public function run() {
        if ($this->slug && file_exists($this->getViewPath() . "/{$this->layout}/single.php")) {
            return $this->displaySingle();
        } else {
            if ($this->article_category_id) {
                return $this->displayGroup();
            } else {
                return $this->displayList();
            }
        }
    }

    private function displayList() {
        $oWidgetItems  = WidgetItems::find()->where(["=", "id", $this->widget_item_id])->one();
        $oArticlesList = ArticlesList::find()->where(["=", "id", $this->article_list_id])->one();
        
        if (is_null($oArticlesList)) { return; }

        $oArticleQuery = Article::find()->orderBy($oArticlesList->default_sort);

        if (!Yii::$app->user->id) { $oArticleQuery->where(['=', "status_id", 2]); }

        /* Pagination */
        $pageUrl = explode("?", Yii::$app->request->url);
        if (count($pageUrl) > 0) {
            $pageUrl = $pageUrl[0];
        } else {
            $pageUrl = Yii::$app->request->url;
        }
        $pages = new Pagination([
            'totalCount' => $oArticlesList->getArticles($this->md_limit, true)->count(),
            'pageSize' => $this->md_limit,
            'pageSizeParam' => 'limit',
            'route' => $pageUrl
        ]);
        
        /* Get Articles */
        $oArticleRowset = $oArticlesList->getArticles($this->md_limit, true, $pages->offset);

        return $this->display([
            'pages' => $pages,
            'sTitle' => $oWidgetItems->custom_name,
            'oArticleRowset' => $oArticleRowset,
            'oArticlesList' => $oArticlesList
        ], "list");
    }

    private function displaySingle() {
        $sSlug = $this->slug;
        $isPreview = $this->preview;

        $oArticleQuery = Article::find()->where(["=", "slug", $sSlug]);

        if (!Yii::$app->user->id) {
            $oArticleQuery->andWhere(['=', "status_id", 2]);
        }

        $oArticle = $oArticleQuery->one();

        if (is_null($oArticle)) {
            throw new \yii\web\NotFoundHttpException();
        }

        return $this->display([
            'oArticle' => $oArticle
        ], "single");
    }

    private function displayGroup() {
        $oArticlesListCategory = ArticlesListCategory::find()->where(["=", "id", $this->article_category_id])->one();
        $oWidgetItems = WidgetItems::find()->where(["=", "id", $this->widget_item_id])->one();

        return $this->display([
            'oArticlesListCategory' => $oArticlesListCategory,
            'sTitle' => $oWidgetItems->custom_name,
            'limit' => $this->md_limit,
        ], "list");
    }

}
