<?php
    use yii\helpers\Html;
    use common\modules\categories\models\ArticlesCategories;
    
    use yii\helpers\ArrayHelper;
    use common\modules\lists\models\ArticlesList;
    use common\modules\lists\models\ArticlesListCategory;
?>

<div class="widget-main-form" >

    <!-- ADD WIDGET CUSTOM DATA -->
    <div class="row" style="background: #f8f8f8; padding-top: 15px; padding-bottom: 15px; " >
        <div class="col-6">
            <?= $form->field($model, 'data[article_list_id]')->dropDownList(ArrayHelper::map(ArticlesList::find()->all(), 'id', 'name'), ["prompt" => "- wybierz listę artykułową -"])->label("Lista artykułowa"); ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'data[article_category_id]')->dropDownList(ArrayHelper::map(ArticlesListCategory::find()->all(), 'id', 'name'), ["prompt" => "- wybierz grupę list artykułowych -"])->label("Lista grup artykułowych"); ?>
        </div>
    </div>
    <!-- END CUSTOM DATA -->
    
    <br>
    <div class="form-group">
        <?= Html::submitButton( Yii::t('backend', 'update'), ['class' => 'btn btn-primary']) ?>
    </div>


</div>