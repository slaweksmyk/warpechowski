<div class="col-12 col-sm-5">
    <h2><span>Wiadomości</span><span class="line"></span></h2>
    <ul class="main-news-list">
        <?php foreach($aReturnRss as $index => $aRss){ ?>
            <?php $i = 0; ?>
            <?php foreach($aRss["rss"]->channel->item as $oRssItem){ ?>
                <?php if(isset($category) && $oRssItem->category == $category){ ?>
                    <li>
                        <a href="<?= $oRssItem->guid ?>" title="<?= $oRssItem->title ?>"><span class="flag flag-danger">Raport</span> <?= $oRssItem->title ?></a>
                    </li>
                    <?php $i++; ?>
                <?php } ?>
            <?php } ?>
        <?php } ?>
    </ul>
    <a href="#" title="Więcej" class="more dark float-right">Więcej</a>
</div>