<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->registerJsFile('/bwcms/common/widgets/article/rss/assets/js/script.js');
?>

<div class="widget-main-form" >

    <!-- ADD WIDGET CUSTOM DATA -->
    <div class="row" style="background: #f8f8f8; padding-top: 15px; padding-bottom: 15px; " >
        <div class="col-6">
            <?php echo $form->field($model, 'data[category]')->textInput()->label("Kategoria"); ?>
        </div>
    </div>
    <!-- END CUSTOM DATA -->
    
    <br>
    <div class="form-group">
        <?= Html::submitButton( Yii::t('backend', 'update'), ['class' => 'btn btn-primary']) ?>
    </div>

</div>