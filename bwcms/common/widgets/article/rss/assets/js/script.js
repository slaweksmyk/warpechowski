$(document).ready(function(){
    
    $("body").on("click", ".rss-more", function(){
        var row = $(this).parent().parent().clone();
        
        row.find("input").each(function(){
            var name = $(this).attr("name");
            $(this).attr("name", name.replace("[0]", "["+(row.find("input").length-1)+"]"));
            $(this).val("");
        });
        
        $(".rss-channels").append(row);
    });
    
    $("body").on("click", ".rss-less", function(){
        var row = $(this).parent().parent().remove();
    });
    
});