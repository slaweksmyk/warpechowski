<?php
/**
 * Widget Article
 * 
 * PHP version 7
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @copyright (c) 2016 Krzysztof Borecki
 * @version 1.0
 */

namespace common\widgets\article\rss;

use common\modules\rss\models\Rss;

class RssArticleWidget extends \common\hooks\yii2\bootstrap\Widget
{
    
    public $widget_item_id, $rss, $article_id, $category;
    public $layout = "default";
    
    /**
     * Build widget
     */
    public function init()
    {
        parent::init();
    }
    
    /**
     * Run widget
     */
    public function run()
    {
        $aReturnRss = [];
        $oRssRowset = Rss::find()->all();
        
        foreach($oRssRowset as $index => $oRss){
            $aReturnRss[$index]["name"] = $oRss->name;
            $aReturnRss[$index]["rss"]  = simplexml_load_file($oRss->url);
        }
        
        return $this->display([
            'category' => $this->category,
            'aReturnRss' => $aReturnRss
        ]);
    }
    
}
