<?php

namespace common\widgets\article\author;

use Yii;
use common\modules\authors\models\Author;
use common\modules\articles\models\Article;

class AuthorWidget extends \common\hooks\yii2\bootstrap\Widget {

    public $widget_item_id;
    private $slug;
    public $layout;

    public function init() {
        if (array_key_exists("slug", Yii::$app->params)) {
            $this->slug = Yii::$app->params['slug'];
        }

        parent::init();
    }

    public function run() {
        if ($this->slug) {
            return $this->author();
        } else {
            return $this->authorList();
        }
    }

    public function author() {
        $sSlug = $this->slug;

        $oAuthor = Author::find()->where(["=", "slug", $sSlug])->one();
        $oArticleRowset = Article::find()->where(["=", "author_id", $oAuthor->id])->andWhere(["=", "status_id", 2])->orderBy("date_published DESC")->all();

        return $this->display([
            'oAuthor' => $oAuthor,
            'oArticleRowset' => $oArticleRowset
        ]);
    }

    public function authorList() {
        $oAuthor = Author::find()->where(["type" => 'author'])->all();

        return $this->display([
            'oAuthor' => $oAuthor,
        ]);
    }

}
