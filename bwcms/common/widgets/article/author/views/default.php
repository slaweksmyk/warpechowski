<?php
use common\modules\authors\models\Author;
?>
<?php if($oAuthor->type == "author"){ ?>
    <section id="news" data-item="news" class="section-top block block-title block-subtitle block-news ">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="news-author row ">
                        <div class="col-4">
                            <div class="row news-author-content d-flex justify-content-start align-items-center no-border" >
                                <div class="col-3 ">
                                    <div class="img-res news-author-image">
                                        <?php if($oAuthor->photo_id){ ?>
                                            <img src="/upload/<?= $oAuthor->getPhoto()->one()->filename ?>" alt="<?= $oAuthor->firstname ?> <?= $oAuthor->surname ?>">
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="col-9 news-author-info">
                                    <span><?= $oAuthor->firstname ?> <?= $oAuthor->surname ?></span>
                                    <a href="mailto:<?= $oAuthor->email ?>" title="Napisz do mnie e-mail"><?= $oAuthor->email ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-8">
                    <?php foreach($oArticleRowset as $index => $oArticle){ ?>
                        <?php if($index == 0){ ?>
                            <div class=" block-card background b-vertical h-300 mt-30">
                                <div class="card-wrapper">
                                    <a href="<?= $oArticle->getUrl() ?>" title="<?= htmlspecialchars($oArticle->title) ?>">
                                        <?php if($oArticle->hasThumbnail()){ ?>
                                            <div class="image img-res">
                                                <img src="<?= $oArticle->getThumbnail(770,300, "crop") ?>" alt="<?= $oArticle->title ?>">
                                            </div>
                                        <?php } ?>
                                        <div class="title">
                                            <div class="txt">
                                                <strong><?= $oArticle->getCategory()->name ?? "" ?></strong>
                                                <h4><?= $oArticle->title ?></h4>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="social d-flex justify-content-end align-content-end text-right">
                                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?= $oArticle->getAbsoluteUrl() ?>" title="Udostępnij na facebooku"><i class="icon icon-icon-facebook-small"></i></a> 
                                        <a href="https://twitter.com/home?status=<?= $oArticle->getAbsoluteUrl() ?>" title="Udostępnij na twitterze"><i class="icon icon-icon-twitter-small"></i></a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>

                    <div class="row">
                        <?php foreach($oArticleRowset as $index => $oArticle){ ?>
                            <?php if($index > 0 && $index < 5){ ?>
                                <div class="col-6 block-card shadow h-200 mt-20">
                                    <div class="card-wrapper">
                                        <a href="<?= $oArticle->getUrl() ?>" title="<?= htmlspecialchars($oArticle->title) ?>">
                                            <?php if($oArticle->hasThumbnail()){ ?>
                                                <div class="image img-res">
                                                    <img src="<?= $oArticle->getThumbnail(370,300, "crop") ?>" alt="<?= $oArticle->title ?>">
                                                </div>
                                            <?php } ?>
                                            <div class="title">
                                                <div class="txt">
                                                    <strong><?= $oArticle->getCategory()->name ?? "" ?></strong>
                                                    <h4><?= $oArticle->title ?></h4>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="social d-flex justify-content-end align-content-end text-right">
                                            <a href="https://www.facebook.com/sharer/sharer.php?u=<?= $oArticle->getAbsoluteUrl() ?>" title="Udostępnij na facebooku"><i class="icon icon-icon-facebook-small"></i></a> 
                                            <a href="https://twitter.com/home?status=<?= $oArticle->getAbsoluteUrl() ?>" title="Udostępnij na twitterze"><i class="icon icon-icon-twitter-small"></i></a>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-4">
                    <ul class="list dotted mt-30">
                        <?php foreach($oArticleRowset as $index => $oArticle){ ?>
                            <?php if($index > 5){ ?>
                                <li>
                                    <a href="<?= $oArticle->getUrl() ?>" title="<?= htmlspecialchars($oArticle->title) ?>"><?= $oArticle->title ?></a>
                                </li>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>
<?php } else { ?>
    <section id="author" data-item="news" class="section-top block block-title block-subtitle block-news ">
        <div class="container">
            <div class="row">
                <div class="col-4">
                    <h2><span><?= $oAuthor->title ?> <?= $oAuthor->firstname ?> <?= $oAuthor->surname ?></span><span class="line"></span></h2>
                    <div class="author-profil">
                        <div class="image mt-30">
                            <?php if($oAuthor->photo_id){ ?>
                                <img src="/upload/<?= $oAuthor->getPhoto()->one()->filename ?>" alt="<?= $oAuthor->firstname ?> <?= $oAuthor->surname ?>">
                            <?php } ?>
                        </div>
                        <?= $oAuthor->description ?>
                    </div>
                </div>
                <div class="col-8">
                    <h2><span>Wpisy autora</span><span class="line"></span></h2>
                    <div class="row">
                        <div class="col-6"> 
                            <div class="row">
                                <?php foreach($oArticleRowset as $index => $oArticle){ ?>
                                    <?php if($index < 4){ ?>
                                        <div class="col-12 block-card shadow h-200 mt-30">
                                            <div class="card-wrapper">
                                                <a href="<?= $oArticle->getUrl() ?>" title="<?= htmlspecialchars($oArticle->title) ?>">
                                                    <?php if($oArticle->hasThumbnail()){ ?>
                                                        <div class="image img-res">
                                                            <img src="<?= $oArticle->getThumbnail(370, 200, "crop") ?>" alt="<?= $oArticle->title ?>">
                                                        </div>
                                                    <?php } ?>
                                                    <div class="title">
                                                        <div class="txt">
                                                            <strong><?= $oArticle->getCategory()->name ?? "" ?></strong>
                                                            <h4><?= $oArticle->title ?></h4>
                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="social d-flex justify-content-end align-content-end text-right">
                                                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?= $oArticle->getAbsoluteUrl() ?>" title="Udostępnij na facebooku"><i class="icon icon-icon-facebook-small"></i></a> 
                                                    <a href="https://twitter.com/home?status=<?= $oArticle->getAbsoluteUrl() ?>" title="Udostępnij na twitterze"><i class="icon icon-icon-twitter-small"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-6">
                            <ul class="list dotted mt-30">
                                <?php foreach($oArticleRowset as $index => $oArticle){ ?>
                                    <?php if($index > 4){ ?>
                                        <li>
                                            <a href="<?= $oArticle->getUrl() ?>" title="<?= htmlspecialchars($oArticle->title) ?>"><?= $oArticle->title ?></a>
                                        </li>
                                    <?php } ?>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="author" data-item="news" class="section-top block block-title block-subtitle block-news ">
        <div class="container">

            <h2><span>Pozostali blogerzy</span><span class="line"></span></h2>
            <div class="row">
                <ul class="reviewer-list mt-30" data-item="reviewer-list" data-slick='{"slidesToShow":6, "slidesToScroll": 1}'>
                    <?php foreach(Author::find()->all() as $oAuthor){ ?>
                        <li>
                            <a href="<?= $oAuthor->getUrl() ?>" title="<?= $oAuthor->firstname ?> <?= $oAuthor->surname ?>" class="d-flex justify-content-end align-content-center flex-column">
                                <span><?= $oAuthor->firstname ?> <?= $oAuthor->surname ?></span>
                                <small>Czytaj więcej</small>
                            </a>
                            <?php if($oAuthor->photo_id){ ?>
                                <div class="img-res">
                                    <img src="/upload/<?= $oAuthor->getPhoto()->one()->filename ?>" alt="<?= $oAuthor->firstname ?> <?= $oAuthor->surname ?>">
                                </div>
                            <?php } ?>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </section>
<?php } ?>

