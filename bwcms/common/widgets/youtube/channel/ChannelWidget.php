<?php
/**
 * Widget Youtube
 * 
 * PHP version 7
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @copyright (c) 2016 Krzysztof Borecki
 * @version 1.0
 */

namespace common\widgets\youtube\channel;

use Yii;

class ChannelWidget extends \common\hooks\yii2\bootstrap\Widget
{
    
    public $widget_item_id;
    public $channel_id;
    public $layout;
    public $title;
    public $limit;
    
    private $slug;
    private $youtube;

    /**
     * Build widget
     */
    public function init()
    {
        if(array_key_exists("slug", Yii::$app->params)){
            $this->slug = Yii::$app->params['slug'];
        }
        
        parent::init();
    }

    
    /**
     * Run widget
     */
    public function run()
    {
        $client = new \Google_Client();
        $client->setDeveloperKey("AIzaSyAWTgJuJvvlLtTWcv_KdVJ6lzj0t73W2eE");
        
        $this->youtube = new \Google_Service_YouTube($client);
        
        if($this->slug){
            return $this->displaySingle();
        } else {
            return $this->displayList();
        }
    }
    
    private function displayList(){
        $searchResponse = $this->youtube->search->listSearch('id,snippet', array(
            'channelId' => "UCKNNt33KPIi4SAvY1IBXoBQ",
            'maxResults' => $this->md_limit,
            'order' => "date"
        ));
        
        return $this->display([
            'aVideoRowset' => $searchResponse['items'],
            'title' => $this->title
        ], "list");
    }
    
    private function displaySingle(){
        
        $searchResponseSingle = $this->youtube->videos->listVideos('id,snippet', array(
            "id" => $this->slug
        ));
        
        $searchResponseList = $this->youtube->search->listSearch('id,snippet', array(
            'channelId' => "UCKNNt33KPIi4SAvY1IBXoBQ",
            'maxResults' => 6,
            'order' => "date"
        ));
        
        if(count($searchResponseSingle["items"]) == 0){ return; }
        
        return $this->display([
            'title' => $this->title,
            'sVideoID' => $this->slug,
            'aVideo' => $searchResponseSingle["items"][0]["snippet"],
            'aVideoRowset' => $searchResponseList['items']
        ], "single");
    }
    
}
