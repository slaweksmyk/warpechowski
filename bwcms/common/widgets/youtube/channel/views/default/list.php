<section class="video">
    <div class="container">
        <div class="title-main col-12">
            <span class="title-main-span" ><h2><?= $title ?></h2></span>	
        </div>
    </div>
    <div class="container">
        <div class="video-list row row2">
            <?php foreach($aVideoRowset as $aVideo){ ?>
                <!--single-->
                <a class="transition_0_5" href="/video/<?= $aVideo["id"]["videoId"]; ?>">
                    <div class="col-12 col-sm-6 col-md-4 video-single col2">
                        <div class="video-single-img col-12 no-padding">
                            <img class="img-responsive" src="<?= $aVideo['snippet']['thumbnails']["high"]["url"] ?>" alt="<?= $aVideo['snippet']['title'] ?>">
                            <img class="play" src="/image/play.png" alt="play">
                        </div>
                        <div class="video-single-tresc col-12 no-padding">
                            <div class="col-12">
                                <div class="col-12 video-single-tresc-tytul no-padding">
                                    <h3><?= $aVideo['snippet']['title'] ?></h3>
                                </div>
                                <div class="col-12 video-single-tresc-2 no-padding">
                                    <?= $aVideo['snippet']['description'] ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            <?php } ?>
        </div>
        <div class="video-list row">
            <div class="see_more col-12 ">
                <a href="/video/" class="transition_0_5">Zobacz wszystkie oferty <img src="/image/right_arrow_small.png" alt="arrow"></a>
            </div>
        </div>
    </div>
</section>