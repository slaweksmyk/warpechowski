<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\shop\models\Category;
?>

<div class="widget-main-form" >

    <!-- ADD WIDGET CUSTOM DATA -->
    <!-- END CUSTOM DATA -->
    
    <br>
    <div class="form-group">
        <?= Html::submitButton( Yii::t('backend', 'update'), ['class' => 'btn btn-primary']) ?>
    </div>

</div>