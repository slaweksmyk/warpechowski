<?php
use yii\widgets\ActiveForm;
?>

    <div class="container maxWidth cart-footer">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-5 col-lg-6 form-rabat">
                <?php $form = ActiveForm::begin() ?>
                    <input type="text" placeholder="kod rabatowy" value="" name="ShopDiscount[code]" />
                    <input type="submit" value="Podaj kod" />
                <?php ActiveForm::end() ?>
            </div>
            <div class="col-12 col-sm-12 col-md-7 col-lg-6 form-order">
                <div class="col-7 col-sm-7 col-md-7">
                    <p class="cart-total-price">Koszt całkowity: <span><?= explode(".", $fFullCost)[0] ?><sup><?= explode(".", $fFullCost)[1] ?></sup> <small>zł</small></span><p>
                </div>
                <div class="col-5 col-sm-5 col-md-5">
                    <a href="/dane/" class="send-button send-order">Zamawiam</a>
                </div>
            </div>
        </div>
    </div>
</section>