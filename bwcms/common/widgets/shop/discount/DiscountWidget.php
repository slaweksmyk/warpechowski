<?php
/**
 * Widget Shop
 * 
 * PHP version 7
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @copyright (c) 2016 Krzysztof Borecki
 * @version 1.0
 */
namespace common\widgets\shop\discount;

use Yii;
use common\modules\shop\models\Product;
use common\modules\shop\models\ShopDiscountCodes;

class DiscountWidget extends \common\hooks\yii2\bootstrap\Widget
{
    
    public $widget_item_id;
    public $layout;

    /**
     * Build widget
     */
    public function init()
    {
        parent::init();
    }

    
    /**
     * Run widget
     */
    public function run()
    {

        if(Yii::$app->request->isPost){
            $aPost = Yii::$app->request->post();
            
            $this->checkDiscountCode($aPost["ShopDiscount"]["code"]);
        }

        return $this->display([
            'fFullCost' => Yii::$app->session->get('fFullCost', 0.00)
        ]);
    }
    
    private function checkDiscountCode($sCode){
        $oDiscount = ShopDiscountCodes::find()->where(["=", "code", $sCode])->one();
        
        // if exists
        if(is_null($oDiscount)){
            $this->returnError("wrong-code", $sCode);
        }

        // if is used
        if($oDiscount->is_one_time && $oDiscount->is_used){
            $this->returnError("already-used", $sCode);
        }
        
        
        // if is still active
        if($oDiscount->active_from && $oDiscount->active_to){
            $current = time();
            if($current < strtotime($oDiscount->active_from) || $current > strtotime($oDiscount->active_to)){
                $this->returnError("already-outdated", $sCode);
            }
        } 
            
        // check restrictions
        if($oDiscount->restriction_type){
            foreach(Yii::$app->session->get('cart', []) as $iItem => $iQuantity){
                $oProduct = Product::find()->where(["=", "id", $iItem])->one();
                
                if($oDiscount->active_from_price > 0 && $oDiscount->active_from_price > $oProduct->price_brutto){
                    $this->returnError("wrong-code", $sCode);
                }
        
                switch($oDiscount->restriction_type){
                    case 0:
                        if(in_array($oProduct->getCategory()->id, explode(",", $oDiscount->category_hash))){
                            Yii::$app->session->set('discount', $oDiscount);
                        }
                        break;

                    case 1:
                        if(in_array($iItem, explode(",", $oDiscount->product_hash))){
                            Yii::$app->session->set('discount', $oDiscount);
                        }
                        break;
                }
            }
        } else {
            foreach(Yii::$app->session->get('cart', []) as $iItem => $iQuantity){
                $oProduct = Product::find()->where(["=", "id", $iItem])->one();
                
                // if product is on promotion
                if($oProduct->isOnPromotion){
                    $this->returnError("already-on-promotion", $sCode);
                }
            }
            Yii::$app->session->set('discount', $oDiscount);
        }
        
        if($oDiscount->is_one_time){
            $oDiscount->is_used = 1;
            $oDiscount->save();
        }
        
        Yii::$app->getResponse()->redirect("/".Yii::$app->request->pathInfo."?status=sucess&error=ok")->send();
        Yii::$app->end();
        exit;
    }

    private function returnError($sErrorCode, $sCode){
        Yii::$app->getResponse()->redirect("/".Yii::$app->request->pathInfo."?status=fail&error={$sErrorCode}&code={$sCode}")->send();
        Yii::$app->end();
        exit;
    }
    
}
