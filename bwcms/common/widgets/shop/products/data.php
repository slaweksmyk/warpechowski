<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\shop\models\Category;
?>

<div class="widget-main-form" >
    
    <!-- ADD WIDGET CUSTOM DATA -->
    <div class="row" style="background: #f8f8f8; padding-top: 15px; padding-bottom: 15px; " >
        <div class="col-3">
            <?php 
                $aCategories = [];
                $aCategories["all"] = "- wszystkie -";
                foreach(Category::find()->all() as $oCategory){
                    $aCategories[$oCategory->id] = $oCategory->name;
                }
                echo $form->field($model, 'data[category_id]')->dropDownList($aCategories)->label( Yii::t('backend_module_shop', 'categories'));
            ?>
        </div>
        <div class="col-3">
            <?php echo $form->field($model, 'data[limit]')->textInput(["type" => "number"])->label("Limit"); ?>
        </div>
        <div class="col-3">
            <?php 
                echo $form->field($model, 'data[order]')->dropDownList(["name ASC" => "Alfabetycznie A-Z", "name DESC" => "Alfabetycznie Z-A", "id ASC" => "Kolejności dodania - rosnąco", "id DESC" => "Kolejności dodania - malejąco", "RAND()" => "Losowo"])->label("Sortowanie");
            ?>
        </div>
    </div>
    <!-- END CUSTOM DATA -->
    
    <br>
    <div class="form-group">
        <?= Html::submitButton( Yii::t('backend', 'update'), ['class' => 'btn btn-primary']) ?>
    </div>

</div>