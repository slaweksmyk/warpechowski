<?php
/**
 * Widget Shop
 * 
 * PHP version 7
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @copyright (c) 2016 Krzysztof Borecki
 * @version 1.0
 */
namespace common\widgets\shop\products;

use Yii;
use common\modules\shop\models\Product;
use common\modules\shop\models\Category;

class ProductsWidget extends \common\hooks\yii2\bootstrap\Widget
{
    
    public $widget_item_id;
    public $category_id;
    public $layout;
    public $limit;
    public $order;
    
    private $slug;
    private $isCat = false;
    private $forcedCat;

    /**
     * Build widget
     */
    public function init()
    {
        parent::init();
        
        if(!empty(Yii::$app->params['slug'])){
            $this->slug = Yii::$app->params['slug'];
        }
        
    }

    /**
     * Run widget
     */
    public function run()
    {
        $oCategory = Category::find()->where(["=","slug", $this->slug])->andWhere(["=", "is_active", 1])->one();

        if($this->slug && is_null($oCategory)){
            return $this->displaySingle();
        } else {
            return $this->displayList($oCategory);
        }
    }
    
    private function displayList($oCategory){
        $sTemplate = "list";
        $oProductRowset = Product::find();

        if(!is_null($oCategory)){ $this->category_id = $oCategory->id; }

        if($this->category_id != "all" || !is_null($oCategory)){
            $category = $this->category_id;
            if($this->forcedCat){ $category = $this->forcedCat; }

            $aCatQuery = ["OR",                 
                ["IN", "category_id", intval($category)],
                ["LIKE", "add_category_hash", intval($category)]
            ];
            
            if(is_null($oCategory)){ $oCategory = Category::findOne($this->category_id); }
            
            foreach($oCategory->getCategories() as $oCat){
                $aCatQuery[] = ["IN", "category_id", $oCat->id];
                $aCatQuery[] = ["LIKE", "add_category_hash", $oCat->id];
            }

            $oProductRowset->andWhere($aCatQuery);
        }

        $oProductRowset->andWhere(["=", "is_active", 1]);
        $oProductRowset->limit($this->md_limit)->orderBy($this->order);

        return $this->display([
            'oCategory' => $oCategory,
            'oProductRowset' => $oProductRowset
        ], $sTemplate);
    }
    
    private function displaySingle(){
        $oProduct = Product::findOne(["slug" => $this->slug]);
        
        // Check if there is any products
        if(!$oProduct){
            throw new \yii\web\NotFoundHttpException();
        }
        
        return $this->display([
            'oProduct' => $oProduct
        ], "single");
    }
    
}
