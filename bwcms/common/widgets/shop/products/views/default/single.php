<h2><?= $oProduct->name; ?></h2>

<div class="row">
    <div class="col-6">
        <img src="/upload/<?= $oProduct->getThumbnail()->filename; ?>" style="max-width: 100%; margin-bottom: 6px;" alt="<?= $oProduct->name; ?>"/>
    </div>
    <div class="col-6">
        <p><?= $oProduct->short_description; ?></p>
        <div style="font-size: 18px; font-weight: bold;"><?= $oProduct->price_brutto ?> zł</div>
        <div style="font-size: 13px;"><strike><?= $oProduct->old_price_brutto ?> zł</strike></div>
        <br/>
        <a href="/sklep/<?= $oProduct->slug; ?>?cart=add&item=<?= $oProduct->id; ?>&quantity=1">Dodaj do koszyka</a>
        <?php if(Yii::$app->request->isGet && Yii::$app->request->get('success', 0) == 1){ ?>
            <div style="color: green;">Produkt został dodany do koszyka.</div>
        <?php } ?>
        <br/>
        <table border="1">
            <tr>
                <th style="padding: 3px;">Atrubut</th>
                <th style="padding: 3px;">Wartość</th>
            </tr>
            <?php foreach($oProduct->getProductAttributes() as $oProdAtt){ ?>
                <tr>
                    <td style="padding: 3px;"><?= $oProdAtt->getProductAttribute()->one()->name ?></td>
                    <td style="text-align: center; padding: 3px;"><?= $oProdAtt->getValue()->one()->name ?></td>
                </tr>
            <?php } ?>
        </table>
    </div>
</div>

<p><?= $oProduct->description; ?></p>

<?php if(count($oProduct->getProductsRelated()) > 0){ ?>
    <br/><hr/>
    <h2>Produkty powiązane</h2>
    <div class="row">
        <?php foreach($oProduct->getProductsRelated() as $oRelated){ ?>
            <div class="col-6">
                <h3><?= $oRelated->getRelated()->name ?></h3>
                <a href="./<?= $oRelated->getRelated()->slug; ?>">
                    <img src="/upload/<?= $oRelated->getRelated()->getThumbnail()->filename; ?>" style="max-width: 100%; margin-bottom: 6px;" alt="<?= $oRelated->getRelated()->name; ?>"/>
                </a>

                <div style="text-align: justify;">
                    <?= $oRelated->getRelated()->short_description ?>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div style="font-size: 18px; font-weight: bold;"><?= $oRelated->getRelated()->price_brutto ?> zł</div>
                        <div style="font-size: 13px;"><strike><?= $oRelated->getRelated()->old_price_brutto ?> zł</strike></div>
                    </div>
                    <div class="col-6" style="text-align: right;">
                        <a href="./<?= $oRelated->getRelated()->slug; ?>">Zobacz produkt</a>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
    <br/><br/>
<?php } ?>