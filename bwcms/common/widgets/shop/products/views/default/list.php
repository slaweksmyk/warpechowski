<?php foreach($oProductRowset->all() as $oProduct){ ?>
    <h3><?= $oProduct->name ?></h3>

    Cena netto: <strike><?= $oProduct->getPriceNetto(true, true) ?></strike> <?= $oProduct->getPriceNetto(true) ?><br/>
    Cena brutto: <strike><?= $oProduct->getPriceBrutto(true, true) ?></strike> <?= $oProduct->getPriceBrutto(true) ?><br/><br/>
    
    <a href="/koszyk/?action=add&item=<?= $oProduct->id ?>&quantity=1">Dodaj do koszyka</a>
<?php } ?>