<div class="category-menu">
    <h3>Kategorie</h3>
    <ul>
        <?php foreach($oCategoryRowset as $oCategory){ ?>
            <li <?php if($oCategory->id == $currentCat){ ?>class="active"<?php }?>><a href="<?= $oCategory->getUrl(); ?>"><?= $oCategory->name; ?></a></li>
        <?php } ?>
    </ul>
</div>