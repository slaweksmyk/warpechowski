<?php
/**
 * Widget Shop
 * 
 * PHP version 7
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @copyright (c) 2016 Krzysztof Borecki
 * @version 1.0
 */
namespace common\widgets\shop\menu;

use Yii;
use common\modules\shop\models\Category;

class MenuWidget extends \common\hooks\yii2\bootstrap\Widget
{
    
    public $widget_item_id;
    public $level;
    private $currentCat;

    /**
     * Build widget
     */
    public function init()
    {
        parent::init();
        
        if(!empty(Yii::$app->params['cat_id'])){
            $this->currentCat = Yii::$app->params['cat_id'];
        }
    }

    
    /**
     * Run widget
     */
    public function run()
    {
        $whereArr = ['and', "is_active = 1"];
        $whereArr = array_merge($whereArr, ["parent_id IS NULL"]);
        
        $oCategoryRowset = Category::find()->where($whereArr)->all();
        
        // Check if there is any products
        if(!$oCategoryRowset){ return; }

        return $this->display([
            'currentCat' => $this->currentCat,
            'oCategoryRowset' => $oCategoryRowset
        ]);
    }
    
}
