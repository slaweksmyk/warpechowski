<?php
    use yii\widgets\ActiveForm;
    use common\modules\shop\models\PriceList;
?>

<section>
    <div class="container section-header">
        <div class="row">
            <h1 data-head="Dane do wysyłki">Dane do wysyłki</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent venenatis velit ultrices commodo sodales. Duis nec nunc sapien. Aenean interdum leo nec ultrices ultricies. Etiam rutrum velit sit amet.</p>
        </div>
    </div>
    <div class="container cart">
        <div class="row">
            <div class="form form-page">
                <?php $form = ActiveForm::begin(["id" => "order-data-form"]) ?>
                    <div class="col-12 col-sm-12 col-md-6 register-form">
                        <h3>Dane do faktury</h3>
                        <div class="col-6 col-sm-6 col-md-6">
                            <?= $form->field($model, 'firstname')->textInput() ?>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <?= $form->field($model, 'surname')->textInput() ?>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12">
                            <?= $form->field($model, 'email')->textInput(["id" => "email"]) ?>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <?= $form->field($model, 'mobile')->textInput(["id" => "phone"]) ?>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <?= $form->field($model, 'phone')->textInput(["id" => "phone2"]) ?>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12">
                            <?= $form->field($model, 'street')->textInput(["id" => "ulica"]) ?>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <?= $form->field($model, 'city')->textInput(["id" => "miejscowosc"]) ?>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <?= $form->field($model, 'postcode')->textInput(["id" => "kod"]) ?>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12">
                            <?= $form->field($model, 'country')->textInput() ?>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12">
                            <label for="nip">Nip</label>
                        </div>
                        <div class="col-2 col-sm-2 col-md-2">
                            <?php 
                                $aNip = []; 
                                foreach(PriceList::find()->all() as $oPl){
                                    $aNip[$oPl->short_name] = $oPl->short_name;
                                }
                            ?>

                            <?= $form->field($model, 'nip_sulf')->textInput()->dropDownList($aNip)->label(false); ?>
                        </div>
                        <div class="col-10 col-sm-10 col-md-10">
                            <?= $form->field($model, 'nip')->textInput(["id" => "nip"])->label(false) ?>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 register-form">
                        <h3>Adres do wysyłki</h3>
                        <div class="col-12 noPadding">
                            <label class="control control-checkbox copy">
                                <input type="checkbox" name="copy">Takie same jak dane do faktury
                                <span class="control_indicator"></span>
                            </label>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12">
                            <?= $form->field($model, 'deliver_email')->textInput(["id" => "wysylka_email"]) ?>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <?= $form->field($model, 'deliver_mobile')->textInput(["id" => "wysylka_phone"]) ?>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <?= $form->field($model, 'deliver_phone')->textInput(["id" => "wysylka_phone2"]) ?>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12">
                            <?= $form->field($model, 'deliver_street')->textInput(["id" => "wysylka_ulica"]) ?>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <?= $form->field($model, 'deliver_city')->textInput(["id" => "wysylka_miejscowosc"]) ?>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6">
                            <?= $form->field($model, 'deliver_postcode')->textInput(["id" => "wysylka_kod"]) ?>
                        </div>
                    </div>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
    <div class="container maxWidth cart-footer cart-footer-white">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-5 col-lg-6 form-rabat">
            </div>
            <div class="col-12 col-sm-12 col-md-7 col-lg-6 form-order">
                <div class="col-7 col-sm-7 col-md-7">
                    <p class="cart-total-price">Koszt całkowity: <span><?= $fFullCost ?> <small>zł</small></span><p>
                </div>
                <div class="col-5 col-sm-5 col-md-5">
                    <a href="#" onClick="$('#order-data-form').submit(); return false;" class="send-button send-order">Zamawiam</a>
                </div>
            </div>
        </div>
    </div>
</section>