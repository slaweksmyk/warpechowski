<?php
/**
 * Widget Shop
 * 
 * PHP version 7
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @copyright (c) 2016 Krzysztof Borecki
 * @version 1.0
 */
namespace common\widgets\shop\data;

use Yii;
use common\modules\shop\models\Product;
use common\modules\shop\models\ShopOrders;
use common\modules\shop\models\ShopOrdersItems;
use common\modules\shop\models\ShopOrdersClient;

class DataWidget extends \common\hooks\yii2\bootstrap\Widget
{
    public $widget_item_id;
    public $layout;
    
    private $iOrderID;

    /**
     * Build widget
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Run widget
     */
    public function run()
    {
        $model = new ShopOrdersClient();
        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            $this->createOrder();
            $this->saveOrderItems();

            $oUser = Yii::$app->session->get('oUser');
            if(!is_null($oUser)){ $model->user_id = $oUser->id; }
            
            $model->order_id = $this->iOrderID;
            $model->save();
            
            Yii::$app->session->set('iOrderID', $this->iOrderID);
            
            Yii::$app->getResponse()->redirect("/platnosci/")->send();
            Yii::$app->end();
            exit;
        } else {
            if(Yii::$app->session->get('is_logged')){
                $this->populateFields($model);
            }
            
            return $this->display([
                'model' => $model,
                'fFullCost' => Yii::$app->session->get('fFullCost', 0.00)
            ]);
        }
    }
    
    private function populateFields(&$model){
        $oUser = Yii::$app->session->get('oUser');
        
        foreach($oUser->getData()->one()->getAttributes() as $sName => $sValue){
            if(!in_array($sName, ["id"]) && $model->hasProperty($sName) && $sValue != ""){
                $model->$sName = $sValue;
            }
        }
        
        foreach($oUser->getDataCompany()->one()->getAttributes() as $sName => $sValue){
            if(!in_array($sName, ["id"]) && $model->hasProperty($sName) && $sValue != ""){
                $model->$sName = $sValue;
            }
        }
    }
    
    private function createOrder(){
        $oOrder = new ShopOrders();
        $oOrder->status = -1;
        $oOrder->order_date = date("Y-m-d H:i:s");
        $oOrder->save();
        
        $oOrder->order_fv = sprintf('%02d', $oOrder->id)."/".date("m/y");
        $oOrder->save();
        
        $this->iOrderID = $oOrder->id;
    }
    
    private function saveOrderItems(){
        foreach(Yii::$app->session->get('aFullBasketData', []) as $iItem => $aBasketData){
            $oProduct = Product::findOne(["id" => $iItem]);
            
            $oCartItem = new ShopOrdersItems();
            $oCartItem->order_id = $this->iOrderID;
            $oCartItem->product_id = $iItem;
            $oCartItem->discount_id = $aBasketData["iDiscountID"];
            $oCartItem->amount = $aBasketData["iQuantity"];
            $oCartItem->price_netto = $aBasketData["fPriceNetto"];
            $oCartItem->price_brutto = $aBasketData["fPriceBrutto"];
            $oCartItem->org_price_netto = $oProduct->price_netto;
            $oCartItem->org_price_brutto = $oProduct->price_brutto;
            $oCartItem->save();
        }
    }

}
