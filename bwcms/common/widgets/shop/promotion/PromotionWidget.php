<?php
/**
 * Widget HTML
 * 
 * PHP version 7
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @copyright (c) 2016 Krzysztof Borecki
 * @version 1.0
 */

namespace common\widgets\shop\promotion;

use common\modules\promotion\models\PromotionsList;
use common\modules\promotion\models\PromotionDaily;

class PromotionWidget extends \common\hooks\yii2\bootstrap\Widget
{
    
    public $widget_item_id;
    public $promotion_id;
    public $html_code;

    /**
     * Build widget
     */
    public function init()
    {
        parent::init();
    }

    
    /**
     * Run widget
     */
    public function run()
    {
        $oPromotion = PromotionsList::findOne(["=", "id", $this->promotion_id]);
        return call_user_func([$this, "{$oPromotion->sys_name}Promotion"]);
    }
    
    private function dailyPromotion(){
        $oPromotionDaily = PromotionDaily::find()->where(["<=", "active_from", date("Y-m-d h:i:s")])->andWhere([">=", "active_to", date("Y-m-d h:i:s")])->one();

        if(is_null($oPromotionDaily)) { return; }
        
        return $this->display([
            'html_code' => $this->html_code,
            'oPromotionDaily' => $oPromotionDaily,
            'oProduct' => $oPromotionDaily->getProduct()->one()
        ]);
    }
    
}
