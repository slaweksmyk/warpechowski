<div class="category-menu" style="padding-bottom: 5px;">
    <h3>Produkt dnia</h3>
    <div class="col-12 product-list-item">
        <div class="product-list-item-img">
            <a href="/produkt/<?= $oProduct->slug ?>">
                <?php if($oProduct->hasThumbnail()){ ?><img src="<?= $oProduct->getThumbnail(236, 125, "matched") ?>" alt="<?= $oProduct->name ?>"/><?php } ?>
            </a>
        </div>
        <div class="product-list-item-detail">
            <h3><a href="/produkt/<?= $oProduct->slug ?>"><?= $oProduct->name ?></a></h3>
            <div class="slide-content-detail-desc">
                <?= $oProduct->short_description ?>              
            </div>
            <div class="slide-content-detail-price" style="width: 100%; margin-top: 10px;">
                <p>Cena od: <span><?= explode(".",$oProduct->price_netto)[0] ?><sup><?= explode(".",$oProduct->price_netto)[1] ?></sup> zł</span></p>
            </div>
        </div>
    </div>
    <br style="clear: both"/>
</div>