<?php
use yii\widgets\ActiveForm;
?>

<section>
    <div class="container section-header">
        <div class="row">
            <h1 data-head="Zamów produkt">Zamów produkt</h1>
            <p> Wybierz formę płatności oraz sposób dostawy. Następnie zaakceptuj regulamin oraz warunki dostawy.</p>
        </div>
    </div>
    <div class="container">
        <?php $form = ActiveForm::begin(["id" => "pay-form"]) ?>
            <div class="row paymant">
                <div class="form form-page">
                    <div class="col-12 col-sm-12 col-md-12">
                        <h6>Forma płatności</h6>
                    </div>
                    <?php foreach($oPaymentRowset as $oPayment){ ?>
                        <div class="col-12 col-sm-6 col-md-3">
                            <label class="control control-checkbox copy">
                                <input type="radio" name="ShopOrders[payment_id]" value="<?= $oPayment->id ?>">
                                <?php if($oPayment->thumbnail_id){ ?>
                                    <img src="/upload/<?= $oPayment->getThumbnail()->one()->filename ?>" alt="<?= $oPayment->name ?>"/>
                                <?php } ?>
                                <span class="control_indicator"></span>
                            </label>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="row delivery">
                <div class="form form-page">
                    <div class="col-12 col-sm-12 col-md-12">
                        <h6>Forma wysyłki</h6>
                    </div>
                    <?php foreach($oDeliverRowset as $oDeliver){ ?>
                        <div class="col-12 col-sm-2 col-md-2">
                            <label class="control control-checkbox copy">
                                <input type="radio" name="ShopOrders[deliver_id]" value="<?= $oDeliver->id ?>"><?= $oDeliver->name ?>
                                <span class="control_indicator"></span>
                            </label>
                        </div>
                    <?php } ?>
                </div>
            </div>
        <?php ActiveForm::end() ?>
    </div>
    <div class="container maxWidth cart-footer cart-footer-white">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-5 col-lg-6 form-copy">
                <label class="control control-checkbox copy">
                    <input type="checkbox" name="check"> Akceptuję regulamin oraz warunki dostawy.

                    <span class="control_indicator"></span>
                </label>
            </div>
            <div class="col-6 col-sm-6 col-md-7 col-lg-6 form-order">
                <div class="col-12 col-sm-12 col-md-12 text-right">
                    <p class="cart-total-price-delivery">Koszt wysyłki <span>0.00 <small>zł</small></span><p>
                </div>
                <div class="col-12 col-sm-12 col-md-12 text-right">
                    <p class="cart-total-price">Koszt całkowity: <span><?= $fFullCost ?> <small>zł</small></span><p>
                </div>
                <div class="col-5 col-sm-5 col-md-5 floatRight">
                    <a href="#" onClick="$('#pay-form').submit(); return false;" class="send-button send-order">Zapłać</a>
                </div>
            </div>
        </div>
    </div>
</section>