<?php
namespace common\widgets\shop\payment\models;

interface iPayment
{
    function __construct($sAPI, $oOrder = null, $isSending = true);
    public function send();
}