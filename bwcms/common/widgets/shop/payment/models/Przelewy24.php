<?php

namespace common\widgets\shop\payment\models;

use Yii;
use yii\helpers\Url;

use common\modules\shop\models\PriceList;
use common\modules\shop\models\ShopPayment;
use common\widgets\shop\payment\models\iPayment;

class Przelewy24 extends yii\base\Model implements iPayment
{
    protected $oPay;
    
    private $oConfig;
    private $oOrder;
    
    function __construct($sAPI, $oOrder = null, $isSending = true)
    {
        $this->loadConfig($sAPI);
        
        if($isSending){
            $this->oOrder = $oOrder;
            $this->loadOrder();
        }
    }
    
    private function loadConfig($sAPI)
    {
        require_once Yii::getAlias('@root/bwcms/common/hooks/payments/Przelewy24.php');
        
        $sUrl = substr(Url::home(true), 0, -1);
        $oPayment = ShopPayment::find()->where(["=", "api", $sAPI])->one();

        $this->oConfig = json_decode($oPayment->config);

        $this->oPay = new \common\hooks\payments\Przelewy24($this->oConfig->MerchantID, $this->oConfig->ShopID, $this->oConfig->Salt, $this->oConfig->Environment == "sandbox" ? true : false);
        $this->oPay->addValue("p24_url_return", $sUrl.$oPayment->thankyou);
        $this->oPay->addValue("p24_url_status", $sUrl);
        $this->oPay->addValue("p24_api_version", "3.2");
        $this->oPay->addValue("p24_encoding", "UTF-8");
    }
    
    private function loadOrder()
    {
        $oPriceList = PriceList::find()->where(["=", "is_default", 1])->one();
        
        $this->oPay->addValue("p24_description", str_replace("{ID}", $this->oOrder->id, $this->oConfig->Description));
        $this->oPay->addValue("p24_order_id", $this->oOrder->id);
        $this->oPay->addValue("p24_session_id", "{$this->oOrder->id}_" . session_id());
        $this->oPay->addValue("p24_currency", $oPriceList->short_currency);
        $this->oPay->addValue("p24_country", $this->oConfig->CountryCode);
        $this->oPay->addValue("p24_shipping", floatval($this->oOrder->getDeliver()->one()->price_brutto) * 100);
        $this->oPay->addValue("p24_transfer_label", "Zamówienie nr.{$this->oOrder->id}");
        
        $fPrice = 0;
        foreach($this->oOrder->getShopOrdersItems()->all() as $iIndex => $oOrderItem){
            $fPrice += $oOrderItem->price_brutto * $oOrderItem->amount;
        }
        
        if(Yii::$app->session->get('fFullCost')){
            $this->oPay->addValue("p24_amount", Yii::$app->session->get('fFullCost')*100);
        } else {
            $this->oPay->addValue("p24_amount", $fPrice*100);
        }
        
        if($this->oOrder->getShopOrdersClients()->one()){
            $this->loadClient($this->oOrder->getShopOrdersClients()->one());
        }
    }
    
    private function loadClient($oClient)
    {
        $this->oPay->addValue("p24_email", $oClient->email);
        $this->oPay->addValue("p24_client", "{$oClient->firstname} {$oClient->surname}");
        $this->oPay->addValue("p24_address", $oClient->deliver_street);
        $this->oPay->addValue("p24_zip", $oClient->deliver_postcode);
        $this->oPay->addValue("p24_city", $oClient->deliver_city);
        $this->oPay->addValue("p24_phone", $oClient->phone);
    }
    
    public function send()
    {
        $this->oPay->trnVerify();
        $RET = $this->oPay->trnRegister(true);

        if($RET["error"]!==0) {
            echo $RET["errorMessage"];
            exit;
        }       
    }

}
