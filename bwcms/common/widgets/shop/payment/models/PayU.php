<?php

namespace common\widgets\shop\payment\models;

use Yii;
use yii\helpers\Url;
use kartik\mpdf\Pdf;

use common\modules\shop\models\PriceList;
use common\modules\shop\models\ShopOrders;
use common\modules\shop\models\ShopPayment;
use common\modules\users\models\UserDataCompany;

use common\widgets\shop\payment\models\iPayment;

class PayU extends yii\base\Model implements iPayment
{
    private $aSettings = [];
    
    private $oOrder;
    private $oConfig;
    
    function __construct($sAPI, $oOrder = null, $isSending = true)
    {
        $this->loadConfig($sAPI);
        
        if($isSending){
            $this->oOrder = $oOrder;
            $this->loadOrder();
        }
    }
    
    private function loadConfig($sAPI)
    {
        $sUrl = substr(Url::home(true), 0, -1);
        $oPayment = ShopPayment::find()->where(["=", "api", $sAPI])->one();
        
        $this->oConfig = json_decode($oPayment->config);

        \OpenPayU_Configuration::setEnvironment($this->oConfig->Environment);

        \OpenPayU_Configuration::setMerchantPosId($this->oConfig->MerchantPosId);
        \OpenPayU_Configuration::setSignatureKey($this->oConfig->SignatureKey);
        
        \OpenPayU_Configuration::setOauthClientId($this->oConfig->OauthClientId);
        \OpenPayU_Configuration::setOauthClientSecret($this->oConfig->OauthClientSecret);

        $this->aSettings['continueUrl'] = $sUrl.$oPayment->thankyou;
        $this->aSettings['notifyUrl']   = $sUrl;
        
        $this->aSettings['customerIp']    = Yii::$app->request->userIP;
        $this->aSettings['merchantPosId'] = \OpenPayU_Configuration::getMerchantPosId();
        $this->aSettings['settings']["invoiceDisabled"] = !boolval($this->oConfig->isFv);
    }
    
    private function loadOrder()
    {
        $oPriceList = PriceList::find()->where(["=", "is_default", 1])->one();
        
        $this->aSettings['extOrderId']   = $this->oOrder->id."-".substr(md5(rand()),0,10);
        $this->aSettings['description']  = str_replace("{ID}", $this->oOrder->id, $this->oConfig->Description);
        $this->aSettings['currencyCode'] = $oPriceList->short_currency;
        
        $fPrice = 0;
        foreach($this->oOrder->getShopOrdersItems()->all() as $iIndex => $oOrderItem){
            $oItem = $oOrderItem->getProduct()->one();

            $this->aSettings['products'][$iIndex]['name']      = $oItem->name;
            $this->aSettings['products'][$iIndex]['unitPrice'] = $oOrderItem->price_brutto*100;
            $this->aSettings['products'][$iIndex]['quantity']  = $oOrderItem->amount;
            
            $fPrice += $oOrderItem->price_brutto * $oOrderItem->amount;
        }
        
        if(Yii::$app->session->get('fFullCost')){
            $this->aSettings['totalAmount'] = Yii::$app->session->get('fFullCost')*100;
        } else {
            $this->aSettings['totalAmount'] = $fPrice*100;
        }

        if($this->oOrder->getShopOrdersClients()->one()){
            $this->loadClient($this->oOrder->getShopOrdersClients()->one());
        }
    } 
    
    private function loadClient($oClient)
    {
        $this->aSettings['buyer']['email'] = $oClient->email;
        $this->aSettings['buyer']['phone'] = $oClient->phone;
        $this->aSettings['buyer']['firstName'] = $oClient->firstname;
        $this->aSettings['buyer']['lastName']  = $oClient->surname;
        $this->aSettings['buyer']['nin']       = $oClient->nip;
        
        $this->aSettings['buyer']['delivery']["street"]      = $oClient->deliver_street;
        $this->aSettings['buyer']['delivery']["postalCode"]  = $oClient->deliver_postcode;
        $this->aSettings['buyer']['delivery']["city"]        = $oClient->deliver_city;
        $this->aSettings['buyer']['delivery']["countryCode"]    = $this->oConfig->CountryCode;
        $this->aSettings['buyer']['delivery']["recipientName"]  = "{$oClient->firstname} {$oClient->surname}";
        $this->aSettings['buyer']['delivery']["recipientPhone"] = $oClient->deliver_phone;
        $this->aSettings['buyer']['delivery']["recipientEmail"] = $oClient->deliver_email;
        
        $this->aSettings['buyer']['billing']["street"]      = $oClient->street;
        $this->aSettings['buyer']['billing']["postalCode"]  = $oClient->postcode;
        $this->aSettings['buyer']['billing']["city"]        = $oClient->city;
        $this->aSettings['buyer']['billing']["countryCode"]    = $this->oConfig->CountryCode;
        $this->aSettings['buyer']['billing']["recipientName"]  = "{$oClient->firstname} {$oClient->surname}";
        $this->aSettings['buyer']['billing']["recipientPhone"] = $oClient->phone;
        $this->aSettings['buyer']['billing']["recipientEmail"] = $oClient->email;
        
        $this->aSettings['buyer']['invoice']["street"]      = $oClient->street;
        $this->aSettings['buyer']['invoice']["postalCode"]  = $oClient->postcode;
        $this->aSettings['buyer']['invoice']["city"]        = $oClient->city;
        $this->aSettings['buyer']['invoice']["countryCode"]    = $this->oConfig->CountryCode;
        $this->aSettings['buyer']['invoice']["recipientName"]  = "{$oClient->firstname} {$oClient->surname}";
        $this->aSettings['buyer']['invoice']["recipientPhone"] = $oClient->phone;
        $this->aSettings['buyer']['invoice']["recipientEmail"] = $oClient->email;
        $this->aSettings['buyer']['invoice']["tin"]            = $oClient->nip;
        $this->aSettings['buyer']['invoice']["einvoiceRequested"] = boolval($this->oConfig->isFv);
    }
    
    public function send()
    {
        Yii::$app->getResponse()->redirect(
            \OpenPayU_Order::create($this->aSettings)->getResponse()->redirectUri
        )->send();
    }
    
    public function receiveOrder($response)
    {
        return ["oOrder" => ShopOrders::find()->where(["=", "id", $response->getResponse()->order->extOrderId])->one(), "sResponse" => $response];
    }
    
    public function changeOrderStatus($oOrder, $response){
        $oOrder->payment_order_id = $response->getResponse()->order->orderId;
        $oOrder->payment_date = date("Y-m-d H:i:s");
        $oOrder->status = 1;
        $oOrder->save();
    }
    
    public function cancel($oOrder)
    {
        $response = \OpenPayU_Order::cancel($oOrder->payment_order_id);
        if($response->getStatus() == 'SUCCESS'){
            $oOrder->status = 2;
            $oOrder->save();
        }
    }
    
    public function refund($oOrder)
    {
        $response = \OpenPayU_Refund::create($oOrder->payment_order_id, "Zwrot zainicjalizowany przez CMS'a");
        if($response->getStatus() == 'SUCCESS'){
            $oOrder->status = 3;
            $oOrder->save();
        }
    }
    
    public function sendConfirmation($oOrder, $sInvoiceHTML = null, $sSpecificationHTML = null)
    {
        $sTitle = "Potwierdzenie zaksięgowania płatności.";
        $sBody  = "Potwierdzamy zaksięgowanie wpłaty.";
        
        $oEmail = Yii::$app->mailer->compose()
            ->setTo($oOrder->getShopOrdersClients()->one()->email)
            ->setSubject($sTitle)
            ->setHtmlBody($sBody);

        if(!is_null($sInvoiceHTML)){
            $oEmail->attachContent($this->generatePDF(true, $sInvoiceHTML), ['fileName' => "Faktura {$oOrder->order_fv}.pdf", 'contentType' => 'application/pdf']);
        }
        
        if(!is_null($sSpecificationHTML)){
            $oEmail->attachContent($this->generatePDF(true, $sSpecificationHTML), ['fileName' => "Specyfikacja zamówienia {$oOrder->id}/".date("Y").".pdf", 'contentType' => 'application/pdf']);
        }
            
        $oEmail->send();
        
        $oUserComp = UserDataCompany::find()->where(["=", "user_id", $oOrder->getShopOrdersClients()->one()->user_id])->one();
        $oPatron = $oUserComp->getPatron()->one();
        
        $sBody2 = "";
        $sBody2 .= "<b>Klient</b>: {$oOrder->getShopOrdersClients()->one()->firstname} {$oOrder->getShopOrdersClients()->one()->surname}<br/>";
        $sBody2 .= "W załączniku znajduje się specyfikacja zamówienia.";

        Yii::$app->mailer->compose()
            ->setTo($oPatron->email)
            ->setSubject("Nowy zakup klienta")
            ->setHtmlBody($sBody2)
            ->attachContent($this->generatePDF(true, $sSpecificationHTML), ['fileName' => "Specyfikacja zamówienia {$oOrder->id}/".date("Y").".pdf", 'contentType' => 'application/pdf'])
            ->send();
        
    }
    
    public function generatePDF($returnString = false, $sHTML = null)
    {
        $eReturnType = Pdf::DEST_BROWSER;
        if($returnString){
            $eReturnType = Pdf::DEST_STRING;
        }
        
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8, 
            'format' => Pdf::FORMAT_A4, 
            'orientation' => Pdf::ORIENT_PORTRAIT, 
            'destination' => $eReturnType, 
            'content' => $sHTML
        ]);

        return $pdf->render(); 
    }
    
}
