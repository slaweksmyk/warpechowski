<?php
/**
 * Widget Shop
 * 
 * PHP version 7
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @copyright (c) 2016 Krzysztof Borecki
 * @version 1.0
 */
namespace common\widgets\shop\payment;

use Yii;
use common\modules\shop\models\ShopOrders;
use common\modules\shop\models\ShopPayment;
use common\modules\shop\models\ShopDelivers;

class PaymentWidget extends \common\hooks\yii2\bootstrap\Widget
{
    public $widget_item_id;
    public $layout;

    /**
     * Build widget
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Run widget
     */
    public function run()
    {
        if(Yii::$app->request->isPost){
            $aPost    = Yii::$app->request->post();
            $oOrder   = ShopOrders::findOne(["=", "id", Yii::$app->session->get('iOrderID')]);
            $oPayment = ShopPayment::findOne(["=", "id", $aPost["ShopOrders"]["payment_id"]]);

            $oOrder->payment_id = $aPost["ShopOrders"]["payment_id"];
            $oOrder->deliver_id = $aPost["ShopOrders"]["deliver_id"];
            $oOrder->status = 0;
            $oOrder->save();
            
            foreach($oOrder->getShopOrdersItems()->all() as $oOrderItem){
                $oProduct = $oOrderItem->getProduct()->one();
                $oProduct->getStocks()->one()->substractAmount($oOrderItem->amount);
            }
            
            
            if($oPayment->is_local){
                Yii::$app->session->remove('cart');
                Yii::$app->session->remove('aFullBasketData');

                Yii::$app->getResponse()->redirect($oPayment->thankyou)->send();
                Yii::$app->end();
            } else {
                $sClass = "common\widgets\shop\payment\models\\".$oPayment->api;

                $oPaymentAPI = new $sClass($oPayment->api, $oOrder);
                $oPaymentAPI->send();

                Yii::$app->end();
            }
            
        }
        
        $oPaymentRowset = ShopPayment::find()->where(["=", "is_active", 1])->all();
        $oDeliverRowset = ShopDelivers::find()->all();
        
        return $this->display([
            'oPaymentRowset' => $oPaymentRowset,
            'oDeliverRowset' => $oDeliverRowset,
            'fFullCost' => Yii::$app->session->get('fFullCost', 0.00)
        ]);
    }
    
}
