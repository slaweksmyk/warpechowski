<section class="thankyou-page">
    <div class="container section-header">
        <div class="row">
            <h1 data-head="" class="normalHead">Dziękujemy za zakup</h1>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 text-center">
                <p><br />Twoje zamówienie <strong>numer <?= sprintf('%08d', $oOrder->id) ?></strong> jest w trakcie realizacji. Na podany adres e-mail zostanie<br />wysłane potwierdzenie oraz faktura VAT. Prosimy o dokonanie płatności <strong>w terminie 5 dni.</strong></p>
                <p>
                    <strong>DG Group Dariusz Grzelak<br />
                    05-840 Warszawa, ul.Nowa 70<br /><br />
                    Wartość zamówienia: <span class="colorRed"><?= $fFullCost ?> zł</span><br /><br />
                    BZ WBK S.A. VI o/Warszawa<br />
                    22 1322 1123 0000 0300 9765 3124</strong><br /><br />

                    W tytule przelewy należy wpisać <strong>numer zamówienia</strong>
                </p>
            </div>
            <div class="col-12 col-sm-12 col-md-6 noFloat center">
                <div class="col-12 col-sm-6 col-md-6">
                    <a href="/produkty/" class="send-button send-button-gray">Kontynuuj zakupy</a>
                </div>
                <div class="col-12 col-sm-6 col-md-6">
                    <a href="/" class="send-button send-order">Strona główna</a>
                </div>
            </div>
        </div>
    </div>
</section>