<?php
/**
 * Widget Shop
 * 
 * PHP version 7
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @copyright (c) 2016 Krzysztof Borecki
 * @version 1.0
 */
namespace common\widgets\shop\thankyou;

use Yii;
use common\modules\shop\models\Product;
use common\modules\shop\models\ShopOrders;

class ThankyouWidget extends \common\hooks\yii2\bootstrap\Widget
{
    
    public $widget_item_id;
    public $html_code;
    public $layout;

    /**
     * Build widget
     */
    public function init()
    {
        parent::init();
    }

    
    /**
     * Run widget
     */
    public function run()
    {
        $oOrder = ShopOrders::findOne(["=", "id", Yii::$app->session->get('iOrderID')]);
        $aDiscounts = Yii::$app->session->get('discounts', []);
        
        $fFullCost = 0.0;
        foreach($oOrder->getShopOrdersItems()->all() as $oOrderItem){
            $oProduct = Product::find()->where(["=", "id", $oOrderItem->product_id])->one();

            if(isset($aDiscounts[0][$oOrderItem->product_id])){
                $oDiscountData = $aDiscounts[0][$oOrderItem->product_id];

                switch($oDiscountData["oDiscount"]->type){
                    case 0:
                        $fPriceNetto  = $this->recalculatePriceAmount($oDiscountData["oProduct"]->price_netto, $oDiscountData["oDiscount"]);
                        $fPriceBrutto = $this->recalculatePriceAmount($oDiscountData["oProduct"]->price_brutto, $oDiscountData["oDiscount"]);
                        break;

                    case 1:
                        $fPriceNetto  = $this->recalculatePricePercent($oDiscountData["oProduct"]->price_netto, $oDiscountData["oDiscount"]);
                        $fPriceBrutto = $this->recalculatePricePercent($oDiscountData["oProduct"]->price_brutto, $oDiscountData["oDiscount"]);
                        break;
                }

                $fFullPriceNetto = $fPriceNetto * $oOrderItem->amount;
                $fFullPriceBrutto = $fPriceBrutto * $oOrderItem->amount;

                $fFullCost += $fFullPriceBrutto;
            } else {
                $fFullPriceNetto = $oProduct->price_netto * $oOrderItem->amount;
                $fFullPriceBrutto = $oProduct->price_brutto * $oOrderItem->amount;

                $fFullCost += $fFullPriceBrutto;
            }
        }

        return $this->display([
            "oOrder" => $oOrder,
            "html_code" => $this->html_code,
            "fFullCost" => number_format($fFullCost, 2, '.', '')
        ]);
    }
    
    private function recalculatePriceAmount($fPrice, $oDiscount){
        return $fPrice - $oDiscount->value;
    }
    
    private function recalculatePricePercent($fPrice, $oDiscount){
        return $fPrice - ($fPrice * ($oDiscount->value/100));
    }
    
}
