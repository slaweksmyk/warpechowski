<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\shop\models\Category;
?>

<div class="widget-main-form" >
    
    <!-- ADD WIDGET CUSTOM DATA -->
    <div class="row" style="background: #f8f8f8; padding-top: 15px; padding-bottom: 15px; " >
        <div class="col-12">
            <?= $form->field($model, 'data[html_code]')->textarea(['rows' => 6]) ?>
        </div>
    </div>
    <!-- END CUSTOM DATA -->
    
    <br>
    <div class="form-group">
        <?= Html::submitButton( Yii::t('backend', 'update'), ['class' => 'btn btn-primary']) ?>
    </div>

</div>