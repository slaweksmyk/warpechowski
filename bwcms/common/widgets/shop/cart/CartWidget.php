<?php

/**
 * Widget Shop
 * 
 * PHP version 7
 *
 * @author Krzysztof Borecki <k.borecki@throk.pl>
 * @copyright (c) 2016 Krzysztof Borecki
 * @version 1.0
 */

namespace common\widgets\shop\cart;

use Yii;
use yii\helpers\Url;
use common\modules\shop\models\Cart;

class CartWidget extends \common\hooks\yii2\bootstrap\Widget
{

    public $widget_item_id;
    public $layout;


    /**
     * Build widget
     */
    public function init()
    {
        $this->isUserLogged();
        
        if (Yii::$app->request->isGet && Yii::$app->request->get('action') /*&& $this->layout == "top_page"*/) {
            
            switch (Yii::$app->request->get('action')) {

                case "add":
                    return $this->addProduct(Yii::$app->request->get('item'), Yii::$app->request->get('quantity'), Yii::$app->request->get('cart'));

                case "substract":
                    return $this->substractProduct(Yii::$app->request->get('item'), Yii::$app->request->get('quantity'), Yii::$app->request->get('cart'));

                case "remove":
                    return $this->removeProduct(Yii::$app->request->get('item'), Yii::$app->request->get('cart'));
                    
                case "addCart":
                    return $this->addNewCart(Yii::$app->request->get('name'));
                    
                case "changeCart":
                    return $this->changeCart(Yii::$app->request->get('cart'));
                    
                case "removeCart":
                    return $this->removeCart(Yii::$app->request->get('cart'));

                default:
                    break;
            }
        }
        parent::init();
    }
    
    private function isUserLogged()
    {
        $oUser = Yii::$app->session->get('oUser');
        if(!is_null($oUser)){
            $oCartRowset = Cart::find()->where(["=", "session_id", Yii::$app->session->getId()])->all();

            foreach($oCartRowset as $oCart){
                $oCart->user_id = $oUser->id;
            }
        }
    }
    
    /**
     * Remove cart
     * @param type $iCart
     * @return type
     */
    private function removeCart($iCart)
    {
        $oCart = Cart::findOne($iCart);
        if($oCart){
            $oCart->delete();
        }
        return Yii::$app->response->redirect(Url::to(Yii::$app->request->hostInfo . "/" . Yii::$app->request->pathInfo . "?success=1", true));
    }
    
    /**
     * Change cart
     * @param type $iCart
     * @return type
     */
    private function changeCart($iCart)
    {
        $oUser = Yii::$app->session->get('oUser');
        if(!is_null($oUser)){
            $oCartRowset = Cart::find()->where(["OR", ["=", "user_id", $oUser->id], ["=", "session_id", Yii::$app->session->getId()]])->andWhere(["=", "is_active", 1])->all();
        } else {
            $oCartRowset = Cart::find()->where(["=", "session_id", Yii::$app->session->getId()])->andWhere(["=", "is_active", 1])->all();
        }
        
        foreach($oCartRowset as $oCart){
            $oCart->is_active = 0;
            $oCart->save();
        }
        
        $oCart = $this->getCart($iCart);
        $oCart->is_active = 1;
        $oCart->save();
        
        return Yii::$app->response->redirect(Url::to(Yii::$app->request->hostInfo . "/" . Yii::$app->request->pathInfo . "?success=1", true));
    }

    /**
     * Create new cart
     * @param string $sName
     * @return mixed
     */
    private function addNewCart($sName)
    {
        $this->createCart($sName);
        return Yii::$app->response->redirect(Url::to(Yii::$app->request->hostInfo . "/" . Yii::$app->request->pathInfo . "?success=1", true));
    }
    
    /**
     * Create new cart object
     * @return Cart
     */
    private function createCart($sName = "Domyślny koszyk", $isActive = 0)
    {
        $oCart = new Cart();
        if(!is_null(Yii::$app->session->get('oUser'))){ 
            $oCart->user_id = $oUser->id; 
        }
        $oCart->name = $sName;
        $oCart->is_active = $isActive;
        $oCart->session_id = Yii::$app->session->getId();
        $oCart->create_date = date("Y-m-d H:i:s");
        $oCart->save();
        
        return $oCart;
    }

    /**
     * Get the cart object
     * @param int $id
     * @return Cart
     */
    private function getCart($id = null)
    {
        $oCart = Cart::findOne($id);
        if(is_null($oCart)){
            $oUser = Yii::$app->session->get('oUser');
            if(!is_null($oUser)){
                $oCart = Cart::find()->where(["=", "user_id", $oUser->id])->andWhere(["=", "is_active", 1])->one();
                if(!is_null($oCart)){ return $oCart; }
            } else {
                $oCart = Cart::find()->where(["=", "session_id", Yii::$app->session->getId()])->andWhere(["=", "is_active", 1])->one();
                if(!is_null($oCart)){ return $oCart; }
            }
        }
        
        if(is_null($oCart)){
            $oCart = $this->createCart("Domyślny koszyk", 1);
        }
        
        return $oCart;
    }

    private function addProduct($iItemID, $iQuantity = 1, $iCartID = null)
    {
        $oCart = $this->getCart($iCartID);
        if($oCart->addProduct($iItemID, $iQuantity)){
            return Yii::$app->response->redirect(Url::to(Yii::$app->request->hostInfo . "/" . Yii::$app->request->pathInfo . "?success=1", true));
        } else {
            return Yii::$app->response->redirect(Yii::$app->request->hostInfo . "/" . Yii::$app->request->pathInfo . "?" . http_build_query($oCart->aErrors));
        }
    }

    private function substractProduct($iItemID, $iQuantity = 1, $iCartID = null)
    {
        $oCart = $this->getCart($iCartID);
        if($oCart->substractProduct($iItemID, $iQuantity)){
            return Yii::$app->response->redirect(Url::to(Yii::$app->request->hostInfo . "/" . Yii::$app->request->pathInfo . "?success=1", true));
        } else {
            return Yii::$app->response->redirect(Yii::$app->request->hostInfo . "/" . Yii::$app->request->pathInfo . "?" . http_build_query($oCart->aErrors));
        }
    }

    private function removeProduct($iItemID, $iCartID = null)
    {
        $oCart = $this->getCart($iCartID);
        if($oCart->removeProduct($iItemID)){
            return Yii::$app->response->redirect(Url::to(Yii::$app->request->hostInfo . "/" . Yii::$app->request->pathInfo . "?success=1", true));
        } else {
            return Yii::$app->response->redirect(Yii::$app->request->hostInfo . "/" . Yii::$app->request->pathInfo . "?" . http_build_query($oCart->aErrors));
        }
    }

    /**
     * Run widget
     */
    public function run()
    {
        $oUser = Yii::$app->session->get('oUser');
        if(!is_null($oUser)){
            $oCartRowset = Cart::find()->where(["OR", ["=", "user_id", $oUser->id], ["=", "session_id", Yii::$app->session->getId()]])->andWhere(["=", "is_active", 0])->all();
        } else {
            $oCartRowset = Cart::find()->where(["=", "session_id", Yii::$app->session->getId()])->andWhere(["=", "is_active", 0])->all();
        }
        
        $oCart = $this->getCart();
        $this->checkProductAvailability($oCart);
        return $this->display([
            'oCart' => $oCart,
            'oCartRowset' => $oCartRowset
        ]);
    }
    
    private function checkProductAvailability($oCart)
    {
        $aErrors = [];
        $isChanged = false;
        foreach($oCart->getCartItems()->all() as $oCartItem){
            $oProduct = $oCartItem->getItem();
            $oStock = $oProduct->getStocks()->one();
            
            if(!$oProduct->is_active || $oStock->amount == 0){
                $oCart->removeProduct($oProduct->id);
                
                $aErrors[] = ["oItemID" => $oProduct->id, 'sMessage' => "Wyprzedany produkt został usunięty z Twojego koszyka."];
                $isChanged = true;
            } else {
                if($oCartItem->quantity > $oStock->amount){
                    $oCartItem->quantity = $oStock->amount;
                    $oCartItem->save();
                    
                    $aErrors[] = ["oItemID" => $oProduct->id, 'sMessage' => "Stan magazynowy produktu uległ zmianie."];
                    $isChanged = true;
                }
            }
        }
        if($isChanged){
            return Yii::$app->response->redirect(Yii::$app->request->hostInfo . "/" . Yii::$app->request->pathInfo . "?" . http_build_query($aErrors));
        }
    }

}
