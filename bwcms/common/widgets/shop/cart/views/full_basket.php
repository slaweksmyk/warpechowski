<?php
    use common\modules\shop\models\Product;
?>

<style>
    td { text-align: center; }
</style>

<h2 style="text-align: center;">Koszyk: <?= $oCart->name ?></h2>
<table style="width: 100%;">
    <tr>
        <th>Produkt</th>
        <th>Ilość sztuk</th>
        <th>Cena za sztukę netto</th>
        <th>Cena za sztukę brutto</th>
        <th>Cena netto</th>
        <th>Cena brutto</th>
        <th></th>
    </tr>
    <?php foreach($oCart->getCartItems()->all() as $oCartItem){ ?>
        <tr>
            <td><?= $oCartItem->getItem()->name ?></td>
            <td>
                <div class="change-qty">
                    <a href="/koszyk/?action=substract&item=<?= $oCartItem->item_id ?>&quantity=1" style="text-decoration: none;">-</a>
                    <input name="amount" style="width: 40px; text-align: center;" value="<?= $oCartItem->quantity ?>" type="text" disabled>
                    <a href="/koszyk/?action=add&item=<?= $oCartItem->item_id ?>&quantity=1" style="text-decoration: none;">+</a>
                </div>
            </td>
            <td><?= $oCartItem->getItem()->getPriceNetto(true) ?></td>
            <td><?= $oCartItem->getItem()->getPriceBrutto(true) ?></td>
            <td><?= number_format((float)$oCartItem->getItem()->getPriceNetto()*$oCartItem->quantity, 2, '.', '')." ".$oCartItem->getItem()->currency ?></td>
            <td><?= number_format((float)$oCartItem->getItem()->getPriceBrutto()*$oCartItem->quantity, 2, '.', '')." ".$oCartItem->getItem()->currency ?></td>
            <td><a href="/koszyk/?action=remove&item=<?= $oCartItem->item_id ?>&quantity=1">x</a></td>
        </tr>
    <?php } ?>
</table>
<br/>
<b>Suma brutto:</b> <?= $oCart->getSumBrutto() ?><br/>
<b>Suma netto:</b> <?= $oCart->getSumNetto() ?><br/>
<br/>
<hr/>
<?php foreach($oCartRowset as $oCart){ ?>
    <h2 style="text-align: center;">Koszyk: <?= $oCart->name ?> <a href="/koszyk/?action=changeCart&cart=<?= $oCart->id ?>" style="font-size: 13px;">[wybierz]</a> <a href="/koszyk/?action=removeCart&cart=<?= $oCart->id ?>" style="font-size: 13px;">[usuń]</a></h2>
    <table style="width: 100%;">
        <tr>
            <th>Produkt</th>
            <th>Ilość sztuk</th>
            <th>Cena za sztukę netto</th>
            <th>Cena za sztukę brutto</th>
            <th>Cena netto</th>
            <th>Cena brutto</th>
            <th></th>
        </tr>
        <?php foreach($oCart->getCartItems()->all() as $oCartItem){ ?>
            <tr>
                <td><?= $oCartItem->getItem()->name ?></td>
                <td>
                    <div class="change-qty">
                        <a href="/koszyk/?cart=<?= $oCart->id ?>&action=substract&item=<?= $oCartItem->item_id ?>&quantity=1" style="text-decoration: none;">-</a>
                        <input name="amount" style="width: 40px; text-align: center;" value="<?= $oCartItem->quantity ?>" type="text" disabled>
                        <a href="/koszyk/?cart=<?= $oCart->id ?>&action=add&item=<?= $oCartItem->item_id ?>&quantity=1" style="text-decoration: none;">+</a>
                    </div>
                </td>
                <td><?= $oCartItem->getItem()->getPriceNetto(true) ?></td>
                <td><?= $oCartItem->getItem()->getPriceBrutto(true) ?></td>
                <td><?= number_format((float)$oCartItem->getItem()->getPriceNetto()*$oCartItem->quantity, 2, '.', '')." ".$oCartItem->getItem()->currency ?></td>
                <td><?= number_format((float)$oCartItem->getItem()->getPriceBrutto()*$oCartItem->quantity, 2, '.', '')." ".$oCartItem->getItem()->currency ?></td>
                <td><a href="/koszyk/?cart=<?= $oCart->id ?>&action=remove&item=<?= $oCartItem->item_id ?>&quantity=1">x</a></td>
            </tr>
        <?php } ?>
    </table>
    <br/>
    <b>Suma brutto:</b> <?= $oCart->getSumBrutto() ?><br/>
    <b>Suma netto:</b> <?= $oCart->getSumNetto() ?><br/>
    <br/>
<?php } ?>


<a href="/koszyk/?action=addCart&name=Koszyk">Dodaj koszyk</a>