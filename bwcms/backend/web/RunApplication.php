<?php
/**
 * Run Backend application
 * 
 * Load modules and start application
 * 
 * PHP version 7
 *
 * @author Volodymyr Shelelo <contact@profficode.com>
 * @copyright (c) 2016 Volodymyr Shelelo
 * @version 1.0
 */

namespace backend\web;

use yii\web\Application;
use common\modules\pages\models\PagesAdmin;

class RunApplication extends Application
{
    
    /**
     * init application
     */
    public function init()
    {
        parent::init();
        $this->modules = PagesAdmin::getModulesConfig();
    }

}