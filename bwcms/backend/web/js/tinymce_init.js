$(document).ready(function () {

    if (window.location.href.indexOf("article") > -1) {
        var unsaved;
        $("input").change(function () {
            unsaved = true;
        });
        $("form").submit(function () {
            unsaved = false;
        });
        $(window).bind('beforeunload', function () {
            if (unsaved) {
                return "Masz niezapisane zmiany. Czy na pewno chcesz opuścić teraz tą stronę?";
            }
        });
    }

    tinymce.PluginManager.add('charactercount', function (editor) {
        var self = this;

        function update() {
            editor.theme.panel.find('#charactercount').text(['Znaków: {0}', self.getCount()]);
        }

        editor.on('init', function () {
            var statusbar = editor.theme.panel && editor.theme.panel.find('#statusbar')[0];

            if (statusbar) {
                window.setTimeout(function () {
                    statusbar.insert({
                        type: 'label',
                        name: 'charactercount',
                        text: ['Znaków: {0}', self.getCount()],
                        classes: 'charactercount',
                        disabled: editor.settings.readonly
                    }, 0);

                    editor.on('setcontent beforeaddundo', update);

                    editor.on('keyup', function (e) {
                        update();
                    });
                }, 0);
            }
        });

        self.getCount = function () {
            var tx = editor.getContent({format: 'raw'});
            var decoded = decodeHtml(tx);
            var decodedStripped = decoded.replace(/(<([^>]+)>)/ig, "").trim();
            var tc = decodedStripped.length;
            return tc;
        };

        function decodeHtml(html) {
            var txt = document.createElement("textarea");
            txt.innerHTML = html;
            return txt.value;
        }
    });

    self.getCount = function () {
        var tx = editor.getContent({format: 'raw'});
        var decoded = decodeHtml(tx);
        var decodedStripped = decoded.replace(/(<([^>]+)>)/ig, "").trim();
        var tc = decodedStripped.length;
        return tc;
    };

    //tiny MCE 
    tinymce.init({
        selector: 'textarea:not(.noTynyMCE)',
        content_css: $("#homeDir").text()+'css/tiny.css',
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount charactercount visualblocks visualchars code fullscreen spellchecker',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample autoresize'
        ],
        toolbar1: 'bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | forecolor backcolor | table hr ',
        toolbar2: 'styleselect | fontsizeselect | fontselect | cut copy paste | link unlink image media | code | quote other-article | pdf new-image new-video | facebook twitter',
        image_advtab: true,
        verify_html: false,
        language: 'pl',
        convert_urls: false,
        relative_urls: false,
        browser_spellcheck: true,
        entity_encoding: "raw",
        remove_script_host: true,
        paste_data_images: true,
        autoresize_bottom_margin: 10,
        autoresize_min_height: 120,
        autoresize_max_height: 500,
        fontsize_formats: '8px 10px 12px 14px 16px 18px 20px 22px 24px 36px',
        templates: $("#homeDir").text()+"mailing/mailing/templates-ajax/",
        extended_valid_elements:'script[language|type|src]',
        setup: function (editor) {

            editor.addButton('new-image', {
                icon: 'image',
                tooltip: 'Wstaw grafikę',
                text: "Wstaw grafikę",
                onclick: function () {
                    editor.windowManager.open({
                        title: 'Dodaj zdjęcie',
                        body: [
                            {
                                name: 'image_data',
                                type: 'selectbox',
                                minWidth: '500',
                                classes: 'image'
                            },
                            {
                                type: 'textbox', multiline: true, name: 'text', label: 'Podpis zdjęcia', style: 'min-height: 150px; min-width: 300px;', autofocus: true
                            }
                        ],
                        onsubmit: function (e) {
                            file = jQuery.parseJSON($('.mce-image option').last().val());

                            var sReturn = '';

                            sReturn += '<figure>';
                            sReturn += '<img src="/upload/' + file[0] + '" alt="' + e.data.author + '">';
                            sReturn += '<figcaption>';
                            sReturn += '<main>' + e.data.text + '</main>';
                            sReturn += '</figcaption>';
                            sReturn += '</figure><br/>';

                            editor.insertContent(sReturn);
                        }
                    });

                    setTimeout(function () {
                        $(".mce-image").select2({
                            width: "100%",
                            placeholder: "Wyszukaj i dodaj zdjęcie",
                            ajax: {
                                url: $("#homeDir").text()+"files/manager/get-image-files-for-mce/",
                                dataType: 'json',
                                delay: 250,
                                data: function (params) {
                                    return {
                                        key: params.term,
                                    };
                                },
                                processResults: function (data, params) {
                                    return {
                                        results: data.items
                                    };
                                },
                                cache: true
                            },
                            escapeMarkup: function (markup) {
                                return markup;
                            },
                            minimumInputLength: 1
                        });
                    }, 300);
                }
            });

            editor.addButton('pdf', {
                icon: 'newdocument',
                tooltip: 'Dodaj plik PDF',
                text: 'Dodaj plik PDF',
                onclick: function () {
                    editor.windowManager.open({
                        title: 'Dodaj plik PDF',
                        body: [
                            {
                                name: 'pdf_data',
                                type: 'selectbox',
                                minWidth: '500',
                                classes: 'pdf'
                            }
                        ],
                        onsubmit: function (e) {
                            file = jQuery.parseJSON($('.mce-pdf option').last().val());
                            editor.insertContent('<embed class="pdf-embed" src="/upload/' + file[0] + '" type="application/pdf" width="100%" height="500px" internalinstanceid="30" title="">');
                        }
                    });

                    setTimeout(function () {
                        $(".mce-pdf").select2({
                            width: "100%",
                            placeholder: "Wyszukaj i dodaj plik PDF",
                            ajax: {
                                url: $("#homeDir").text()+"files/manager/get-pdf-files-for-mce/",
                                dataType: 'json',
                                delay: 250,
                                data: function (params) {
                                    return {
                                        key: params.term,
                                    };
                                },
                                processResults: function (data, params) {
                                    return {
                                        results: data.items
                                    };
                                },
                                cache: true
                            },
                            escapeMarkup: function (markup) {
                                return markup;
                            },
                            minimumInputLength: 1
                        });
                    }, 300);
                }
            });

            editor.addButton('quote', {
                icon: 'blockquote',
                tooltip: 'Wstaw cytat',
                text: "Wstaw cytat",
                onclick: function () {
                    editor.windowManager.open({
                        title: 'Dodaj cytat',
                        body: [
                            {
                                type: 'textbox', multiline: true, name: 'text', label: 'Treść cytatu', style: 'min-height: 150px; min-width: 300px;', autofocus: true
                            },
                            {
                                type: 'textbox', name: 'author', label: 'Podpis'
                            }
                        ],
                        onsubmit: function (e) {
                            var sReturn = '<blockquote class="blockquote"><p>' + e.data.text + '</p><footer class="blockquote-footer"><cite title="' + e.data.author + '">' + e.data.author + '</cite></footer></blockquote>';
                            editor.insertContent(sReturn);
                        }
                    });
                }
            });

            editor.addButton('other-article', {
                icon: 'link',
                tooltip: 'Dodaj odnośnik do innego artykułu',
                text: 'Wstaw odnośnik',
                buttons: [{
                        text: 'Close',
                        onclick: 'close'
                    }],
                onclick: function () {
                    editor.windowManager.open({
                        title: 'Wybierz artykuł',

                        body: [
                            {
                                name: 'article_data',
                                type: 'selectbox',
                                minWidth: '500',
                                classes: 'article',
                            }
                        ],
                        onsubmit: function (e) {
                            var aData = JSON.parse($('.mce-article option').last().val());
                            var sReturn = '<div class="news-similar-link"><p><strong>Czytaj też:</strong> <a title="' + aData[1] + '" href="' + aData[2] + '" target="_blank">' + aData[1] + '</a></p></div>';
                            editor.insertContent(sReturn);
                        }
                    });
                    setTimeout(function () {
                        $(".mce-article").select2({
                            width: "100%",
                            containerCssClass: "klasa-select",
                            placeholder: "Wyszukaj i dodaj artykuł",
                            ajax: {
                                url: $("#homeDir").text()+"articles/article/get-articles-for-mce/",
                                dataType: 'json',
                                delay: 250,
                                data: function (params) {
                                    return {
                                        key: params.term,
                                    };
                                },
                                processResults: function (data, params) {
                                    return {
                                        results: data.items
                                    };
                                },
                                cache: true
                            },
                            escapeMarkup: function (markup) {
                                return markup;
                            },
                            minimumInputLength: 1
                        });
                    }, 300);
                }
            });

            editor.addButton('new-video', {
                icon: 'media',
                tooltip: 'Wstaw film Youtube',
                text: 'Wstaw film Youtube',
                buttons: [{
                        text: 'Close',
                        onclick: 'close'
                    }],
                onclick: function () {
                    editor.windowManager.open({
                        title: 'Wstaw link do filmu',
                        body: [
                            {
                                type: 'textbox', multiline: true, name: 'url', label: 'Adres URL', style: 'min-height: 150px; min-width: 300px;', autofocus: true
                            }
                        ],
                        onsubmit: function (e) {
                            var url = e.data.url;

                            url = url.replace("https://www.youtube.com/watch?v=", "");
                            url = url.replace("http://www.youtube.com/watch?v=", "");
                            url = url.replace("https://youtu.be/", "");
                            url = url.replace("http://youtu.be/", "");

                            var sReturn = '<iframe width="100%" height="400" src="https://www.youtube.com/embed/' + url + '?rel=0" style="width: 100%;" frameborder="0" allowfullscreen></iframe>';
                            editor.insertContent(sReturn);
                        }
                    });
                }
            });

            editor.addButton('facebook', {
                icon: 'plus',
                tooltip: 'Wstaw post facebook',
                text: "Facebook",
                onclick: function () {
                    editor.windowManager.open({
                        title: 'Wstaw link do postu',
                        body: [

                            {
                                type: 'textbox', name: 'link', label: 'Link'
                            }
                        ],
                        onsubmit: function (e) {
                            var sReturn = '<iframe src="https://www.facebook.com/plugins/post.php?href=' + e.data.link + '&width=500&show_text=false&height=496&appId" width="500" height="496" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>';
                            editor.insertContent(sReturn);
                        }
                    });
                }
            });

            editor.addButton('twitter', {
                icon: 'plus',
                tooltip: 'Wstaw post twitter',
                text: "Twitter",
                onclick: function () {
                    editor.windowManager.open({
                        title: 'Wstaw kod postu',
                        body: [

                            {
                                type: 'textbox', name: 'code', label: 'Kod', multiline: true, style: 'min-height: 150px; min-width: 300px;', autofocus: true
                            }
                        ],
                        onsubmit: function (e) {
                            editor.insertContent('<div class="twitter-post">' + e.data.code + '</div>');
                        }
                    });
                }
            });

            editor.on('change', function (e) {
                previewContent(editor);
            });
            editor.on('keypress', function (e) {
                previewContent(editor);
                countSeoScore(editor);
                unsaved = true;
            });
            editor.on('init', function (e) {
                previewContent(editor);
                countSeoScore(editor);
            });
        }
    });
    function previewContent(editor) {
        if ($("#tinymce-preview").length > 0) {
            $('#tinymce-preview').html(editor.getContent());
        }
    }

    function countSeoScore(editor) {
        var tips = "<h4>Audyt treści:</h4>";
        var score = 0;
        var words = editor.getContent().split(" ");
        var $_content = $(editor.getContent());
        // Text length
        if (words.length >= 500 && words.length < 2000) {
            tips += "- Ilość słów większa niż 500" + "<br/>";
            score += 10;
        }

// Text length
        if (words.length >= 2000) {
            tips += "- Ilość słów większa niż 2000" + "<br/>";
            score -= 10;
        }

// Links quality
        if ($_content.find("a").length > 0) {
            var titled = 0;
            var untitled = 0;
            $_content.find("a").each(function () {
                var title = $(this).attr("title");
                if (typeof title !== typeof undefined && title !== false) {
                    score += 1;
                    titled++;
                } else {
                    score -= 1;
                    untitled++;
                }
            });
            tips += "- Linki z tytułem: " + titled + "<br/>";
            tips += "- Linki z bez tytułu: " + untitled + "<br/>";
        }

// Images quality
        if ($_content.find("img").length > 0) {
            var alted = 0;
            var unalted = 0;
            $_content.find("img").each(function () {
                var alt = $(this).attr("alt");
                if (typeof alt !== typeof undefined && alt !== false) {
                    score += 1;
                    alted++;
                } else {
                    score -= 1;
                    unalted++;
                }
            });
            tips += "- Obrazki z altem: " + alted + "<br/>";
            tips += "- Obrazki bez altów: " + unalted + "<br/>";
        }

        if (score < 10) {
            tips += "<br/>Poziom SEO: <b>niski</b> <span style='font-size: 12px;'>(" + score + ")</span><br/>";
        } else if (score >= 10) {
            tips += "<br/>Poziom SEO: <b>dobry</b> <span style='font-size: 12px;'>(" + score + ")</span><br/>";
        } else {
            tips += "<br/>Poziom SEO: <b>zły</b> <span style='font-size: 12px;'>(" + score + ")</span><br/>";
        }

        $(editor.getElement()).parent().parent().parent().find(".seo-tips").html(tips);
    }


});