$(document).ready(function(){
   
    var is_sort = false;
    $(".toggle-sort").click(function(){
        
        if(!is_sort){
            var script_url = $("#sortable_data>form").attr("action");
            $(".sortable>tbody").sortable({
                placeholder: "ui-state-highlight",
                update: function(event, ui) {
                    var i = 0;
                    $(".ui-sortable-handle").each(function(){
                        var modelID = $(this).data("id");

                        $.post(script_url, { modelID: modelID, index: i })
                        .done(function( data ) {
                            console.log(data);
                        });
                        i++;
                    });
                }
            }).disableSelection();

            $(".sortable>tbody").find("tr").each(function(){
                var text = $(this).find("td:first-child").text();

                $(this).find("td:first-child").html('<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>' + text);
            });
            
            $(this).text(sort_disable);
            $(".sortable").addClass("enabled");
            is_sort = true;
        } else {
            $(".sortable>tbody").find("tr").each(function(){
                var text = $(this).find("td:first-child").text();

                $(this).find("td:first-child").text(text);
            });
            $(".sortable>tbody").sortable("destroy");
            $(".sortable>tbody li").removeClass('ui-state-default');
            $(".sortable>tbody li span").remove();
    
            $(this).text(sort_enable);
            $(".sortable").removeClass("enabled");
            is_sort = false;
        }
    });
    
});