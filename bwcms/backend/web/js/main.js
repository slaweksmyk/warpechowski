$(document).ready(function () {
    
    $(function () {
        $("#article-list table tbody").sortable({
            placeholder: "ui-state-highlight",
            stop: function( event, ui ) {
                var aArticleSort = [];
                
                var sort = 0;
                $("#article-list .ui-sortable>tr").each(function(){
                    var aArtSort = {};
                    aArtSort["id"] = $(this).data("key");
                    aArtSort["sort"] = sort;

                    aArticleSort.push(aArtSort);
                    sort++;
                });

                $.post($("#homeDir").text()+"articles/article/sort/", { aArtData: JSON.stringify(aArticleSort) })
                .done(function( data ) {
                    console.log(data);
                });
            }
        });
        
        $("#products-list table tbody").sortable({
            placeholder: "ui-state-highlight",
            stop: function( event, ui ) {
                var aProductSort = [];
                
                var sort = 0;
                $("#products-list .ui-sortable>tr").each(function(){
                    var aProdSort = {};
                    aProdSort["id"] = $(this).data("key");
                    aProdSort["sort"] = sort;

                    aProductSort.push(aProdSort);
                    sort++;
                });

                $.post($("#homeDir").text()+"shop-product/shop-product/sort/", { aArtData: JSON.stringify(aProductSort) })
                .done(function( data ) {
                    console.log(data);
                });
            }
        });
        
        $("#sortable").sortable({
            placeholder: "ui-state-highlight"
        });
        $("#sortable").disableSelection();
    });
    
    $("body").on("click", ".remove-favorite", function(){
        var id = $(this).data("id");
        $(this).parent().fadeOut(200);
        
        $.ajax({
            url: $("#homeDir").text(),
            type: 'GET',
            dataType: 'text',
            data: {id: id},
            success: function (response) {}
        });
    });
    
    $("#generate-code").click(function(){
       $(this).parent().prev().find("input").val(Math.random().toString(36).substring(3).toUpperCase()); 
    });

    if($(".datepicker-range").length > 0){
        $(".datepicker-range").dateRangePicker({
            autoClose: true,
            startOfWeek: 'monday',
            language:'pl',
            showShortcuts: true,
            separator : ' - ',
            format: 'DD.MM.YY HH:mm',
            shortcuts : 
            {
                next: ['week','month', 'year'],
                prev: ['week','month', 'year'],
            },
            time: {
                enabled: true
            }
        }).bind('datepicker-change',function(event,obj){
            var aRaw = obj.value.split(" - ");
            location.href = $("#homeDir").text()+"articles/?from="+aRaw[0]+"&to="+aRaw[1];
        });
    }

    $('[data-action="prompt"]').click(function () {

        var email = prompt("Testowy e-mail", "");
        var ID = $(this).data('id');


        if (email !== null) {
            //post the field with ajax
            $.ajax({
                url: $("#homeDir").text()+'mailing/mailing/campaign-test/',
                type: 'GET',
                dataType: 'text',
                data: {id: ID, email: email},
                success: function (response) {
                    //do anything with the response
                    alert(response);

                    $('[data-key="' + ID + '"]').attr('class', '').addClass('new-row');

                    $('[data-key="' + ID + '"]').find('.text-status').text('Szkic');
                }
            });
        }

    });

    $(".getGalleries").select2({
        width: "100%",
        //multiple: true,
        placeholder: "Wyszukaj i dodaj galerię",
        ajax: {
            url: $("#homeDir").text()+"gallery/gallery/get-galeries-for-mce/",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    query: params.term,
                };
            },
            processResults: function (data, params) {
                return {
                    results: data.items
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        minimumInputLength: 1
    });


    $('[data-toggle="popover"]').popover();

    $(".article-types>span>span").click(function () {
        $(this).parent().remove();
    });

    var $_GET = {};
    document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
        function decode(s) {
            return decodeURIComponent(s.split("+").join(" "));
        }
        $_GET[decode(arguments[1])] = decode(arguments[2]);
    });

    $("aside.left .main-menu").find(".dropdown").each(function () {
        if ($(this).children("ul").length == 0) {
            $(this).remove();
        }
    });

    $("[data-init='file-manager']").click(function () {
        $(".modal-title").text("Manager plików");
        $(".modal-body").html("<div class='folders-area'></div>");
        $(".modal-body .folders-area").load($("#homeDir").text()+"files/", {target: $(this).data("target")});
        $('#modal').modal();
    });

    $(".article-qmenu-toggle").click(function () {
        $(".category-list").toggleClass("open");
        $(".cat-list-box").toggleClass("open");
        $(".cat-list-button").toggle();
    });

    $(".list-close").click(function () {
        $(".category-list").toggleClass("open");
        $(".cat-list-box").toggleClass("open");
        $(".cat-list-button").toggle();
    });

    $("body").on("click", ".has-child", function () {
        var thisID = $(this).data('id');

        if ($(this).hasClass("open")) {
            $(this).removeClass("open").children().children(".article-toggle-ico").html("+");
        } else {
            $(this).addClass("open").children().children(".article-toggle-ico").html("-");
        }

        $(this).parent().children("li").each(function () {
            if ($(this).data("parent") == thisID) {
                $(this).toggle();
            }
        });
        return false;
    });

    $("[data-action='label-delete']").click(function () {
        $(this).parent().remove();
    });
    

    $(window).on("load", function(){
        $("table .glyphicon").each(function () {
            if (
                    !$(this).hasClass("btn-xs") &&
                    !$(this).parent().hasClass("btn-xs") &&
                    !$(this).parent().hasClass("col-xs-2") &&
                    !$(this).parent().parent().hasClass("buttons") &&
                    !$(this).parent().hasClass("buttons")
                    ) {
                $(this).addClass("btn btn-xs");

                if ($(this).parent().attr("title") == "Edytuj") {
                    $(this).addClass("btn-primary");
                } else if ($(this).parent().attr("title") == "Usuń") {
                    $(this).addClass("btn-danger");
                } else if ($(this).parent().attr("title") == "Podkategorie") {
                    $(this).addClass("btn-warning");
                } else {
                    $(this).addClass("btn-warning-yellow");
                }

            }
        });
    });

    $(".ajax-load-authors").select2({
        width: "100%",
        multiple: true,
        placeholder: "Wyszukaj i dodaj autora",
        ajax: {
            url: $("#homeDir").text()+"articles/article/ajaxgetauthors/",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    query: params.term,
                };
            },
            processResults: function (data, params) {
                return {
                    results: data.items
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        minimumInputLength: 1
    });

    $(".ajax-load-categories").select2({
        width: "100%",
        multiple: true,
        placeholder: "Wyszukaj i dodaj kategorię",
        ajax: {
            url: $("#homeDir").text()+"articles/article/ajaxgetcategories/",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    query: params.term,
                };
            },
            processResults: function (data, params) {
                return {
                    results: data.items
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        minimumInputLength: 1
    });

    $(".ajax-load-articles").select2({
        width: "100%",
        multiple: true,
        placeholder: "Wyszukaj i dodaj artykuł",
        ajax: {
            url: $("#homeDir").text()+"articles/article/ajaxgetarticles/",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    query: params.term,
                };
            },
            processResults: function (data, params) {
                return {
                    results: data.items
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        minimumInputLength: 1
    });

    $(".ajax-load-articles-single").select2({
        width: "100%",

        placeholder: "Wyszukaj i dodaj artykuł",
        ajax: {
            url: $("#homeDir").text()+"articles/article/ajaxgetarticles/",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    query: params.term,
                };
            },
            processResults: function (data, params) {
                return {
                    results: data.items
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        minimumInputLength: 1
    });




    $('.ajax-load-photos').select2({
        width: "100%",
        placeholder: "Wyszukaj i dodaj zdjęcie",
        ajax: {
            url: $("#homeDir").text()+"files/manager/get-image-files/",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    key: params.term,
                };
            },
            processResults: function (data, params) {
                return {
                    results: data.items
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        minimumInputLength: 1
    });
    $(".ajax-load-article-tags").select2({
        tags: true,
        multiple: true,
        width: "100%",
        minimumInputLength: 1,
        placeholder: "Wyszukaj i dodaj tag artykułu",
        createSearchChoice: function (term, data) {
            if ($(data).filter(function() {
              return this.text.localeCompare(term)===0;
            }).length===0) {
              return {id:term, text:term};
            }
        },        
        ajax: {
            url: $("#homeDir").text()+"articles/article/ajaxgettags/",
            dataType: 'json',
            delay: 250,
            cache: true,
            data: function (params) {
                return {
                    query: params.term,
                };
            },
            processResults: function (data, params) {
                return {
                    results: data.items
                };
            },
        },
    });

    $(".ajax-load-file-categories").select2({
        width: "100%",
        multiple: true,
        placeholder: "Wyszukaj i dodaj katalogi",
        ajax: {
            url: $("#homeDir").text()+"files/manager/get-categories-ajax/",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    query: params.term,
                };
            },
            processResults: function (data, params) {
                return {
                    results: data.items
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        minimumInputLength: 1
    });

    $(".ajax-load-article-types").select2({
        width: "100%",
        multiple: true,
        placeholder: "Wyszukaj i dodaj typ artykułu",
        ajax: {
            url: $("#homeDir").text()+"articles/article/ajaxgettypes/",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    query: params.term,
                };
            },
            processResults: function (data, params) {
                return {
                    results: data.items
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        minimumInputLength: 1
    });

    $(".ajax-load-product-categories").select2({
        width: "100%",
        multiple: true,
        ajax: {
            url: $("#homeDir").text()+"shop-product/shop-product/ajaxgetcategories/",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    query: params.term,
                };
            },
            processResults: function (data, params) {
                return {
                    results: data.items
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        minimumInputLength: 1
    });

    $(".ajax-load-products").select2({
        width: "100%",
        multiple: true,
        ajax: {
            url: $("#homeDir").text()+"shop-product/shop-product/ajaxgetproducts/",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    query: params.term,
                };
            },
            processResults: function (data, params) {
                return {
                    results: data.items
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            if (markup != "") {
                return markup;
            }
        },
        minimumInputLength: 3,
        templateResult: formatProducts
    });

    $(".ajax-load-product").select2({
        width: "100%",
        multiple: false,
        ajax: {
            url: $("#homeDir").text()+"shop-product/shop-product/ajaxgetproducts/",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    query: params.term,
                };
            },
            processResults: function (data, params) {
                return {
                    results: data.items
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            if (markup != "") {
                return markup;
            }
        },
        minimumInputLength: 3,
        templateResult: formatProducts
    });


    function formatProducts(product) {
        if (!product.id) {
            return product.text;
        }

        var sReturn = "<div style='font-size: 12px;' data-id='" + product.id + "'>";
        if (product.thumbnail) {
            sReturn += "<div style='width: 25%;float:left;'>";
            sReturn += "<img src='" + product.thumbnail + "' style='max-width: 100%;'/><br/>";
            sReturn += "</div>";
        }
        if (product.thumbnail) {
            sReturn += "<div style='width: 75%;float:left;'>";
        }
        sReturn += "<b>ID:</b> " + product.id + "<br/>";
        sReturn += "<b>Kod:</b> " + product.code + "<br/>";
        sReturn += "<b>Nazwa:</b> " + product.text + "<br/></div>";
        if (product.thumbnail) {
            sReturn += "</div>";
            sReturn += "<br style='clear:both;'/>";
        }
        sReturn += "</div>";

        return sReturn;
    }

    function formatRepo(repo) {
        if (repo.loading)
            return repo.text;

        var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
                "<div class='select2-result-repository__meta'>" +
                "<div class='select2-result-repository__title'>" + repo.full_name + "</div>";

        if (repo.description) {
            markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
        }

        markup += "<div class='select2-result-repository__statistics'>" +
                "<div class='select2-result-repository__forks'><i class='fa fa-flash'></i> " + repo.forks_count + " Forks</div>" +
                "<div class='select2-result-repository__stargazers'><i class='fa fa-star'></i> " + repo.stargazers_count + " Stars</div>" +
                "<div class='select2-result-repository__watchers'><i class='fa fa-eye'></i> " + repo.watchers_count + " Watchers</div>" +
                "</div>" +
                "</div></div>";

        return markup;
    }

    var savedPlaceholder;
    $("input").focusin(function () {
        savedPlaceholder = $(this).attr("placeholder");
        $(this).attr("placeholder", "");
    });

    $("input").focusout(function () {
        $(this).attr("placeholder", savedPlaceholder);
    });

    if ($(".action-action").length > 0) {

        $(".action-action").change(function () {

            var color = $(this).find(":selected").data("color");



            var secondary = $(this).find(":selected").data("secondary");
            $('[data-action="btn"]').removeClass().addClass('btn  action-button btn-' + color);
            if (secondary) {
                $(".action-secondary").fadeIn(300);
            } else {
                $(".action-secondary").fadeOut(300);
            }
        });

        $(".action-button").click(function () {
            var rows = $('#list').yiiGridView('getSelectedRows');
            var action = $(this).parent().parent().find(".action-action").val();
            var value = $(this).parent().parent().find(".action-value").val();

            $.ajax({
                method: "POST",
                url: $(".action-bar").data("url"),
                data: {rows: JSON.stringify(rows), action: action, value: value}
            })
                    .done(function (data) {
                        location.reload();
                    });
            return false;
        });

    }

    if ($('select').length > 0) {
        $('select').each(function () {
            if ($(this).children().length > 5) {
                if ($(this).attr("name") != "per-page") {
                    if (!$(this).hasClass("no-select2")) {
                        $(this).select2({width: "100%"});

                        $("select").on("select2:open", function () {
                            $(".select2-search__field").attr("placeholder", "Wyszukaj...");
                        });
                        $("select").on("select2:close", function () {
                            $(".select2-search__field").attr("placeholder", null);
                        });
                    }
                }
            }
        });
    }


    $('[data-toggle="tooltip"]').tooltip();

    $(".quick-edit").click(function () {
        if (!$(this).hasClass("edit-mode")) {
            $(this).children().remove();
            var val = $(this).html();
            var id = $(this).parent().parent().data("key");

            $(this).html("<input value='" + val + "'><div class='finish-edit' data-id='" + id + "'>✓</div>");
            $(this).addClass("edit-mode");
        }
    });

    $("body").on("click", ".finish-edit", function () {
        var val = $(this).parent().find("input").val();
        $(this).parent().removeClass("edit-mode");
        $(this).parent().html(val + "<div style='float: right;' title='Edytuj'>✎</div>");

        $.post($("#homeDir").text()+"shop-product/shop-product/quickedit-category/", {value: val, id: $(this).data("id")})
                .done(function (data) {
                    console.log(data);
                });
    });

    $(".dir").click(function () {
        $(this).toggleClass("open").parent().children("ul").slideToggle();
    });

    $(".clt").find("li").each(function () {
        if ($(this).find("ul").length == 0) {
            $(this).find(".dir").addClass("no-child");
        }
    });

    if ($("#product-price_netto").length > 0) {

        $("#product-price_netto").keyup(function () {
            var vat = $("#product-vat").val();
            var newBrutto = (parseFloat($(this).val()) * (vat / 100)) + parseFloat($(this).val());

            $("#product-price_brutto").val(newBrutto.toFixed(2));
        });

        $("#product-price_brutto").keyup(function () {
            var vat = $("#product-vat").val();
            var newNetto = parseFloat($(this).val()) / (1 + parseFloat($("#product-vat").val()) / 100);

            $("#product-price_netto").val(newNetto.toFixed(2));
        });

    }

    $("#per-page select").change(function () {
        var form = $(this).parent();

        form.find("input[type='hidden']").each(function () {
            $(this).remove();
        });

        $.each($_GET, function (key, value) {
            if (key != "per-page") {
                form.append('<input type="hidden" name="' + key + '" value="' + value + '"/>');
            }
        });

        setTimeout(function () {
            form.submit();
        }, 300);
    });

    $(window).on("load", function(){
        var curAmount = $_GET["per-page"];

        $("#per-page select").children().each(function () {
            if ($(this).text() == curAmount) {
                $(this).attr("selected", "selected");
            }
        });
    });

    var submit = $("<input>").attr("type", "submit").val("szukaj");
    $(".filters").find("td:last-child").html(submit);

    $(".choice_selector").click(function () {
        $(this).toggleClass("active").prev().val(+$(this).hasClass("active"));
    });

    $("#menu-toggle").click(function () {
        asideCompact(true);
    });

    $("section.title-area h1 a").click(function () {
        var url = window.location.href;
        var name = $(this).parent().text();

        url = url.replace("http://", "");
        url = url.replace("https://", "");
        url = url.replace(window.location.hostname, "");
        url = url.replace("#", "");

        $.post($("#homeDir").text(), {action: "addFav", name: name, url: url})
        .done(function (id) {
            $(".favorite").append('<li><a href="#" class="remove-favorite" data-id="'+id+'" title="Usuń moduł z listy"><span class="glyphicon glyphicon-trash"></span></a><a href="' + url + '">' + name + '</a></li>');
        });
    });

    $(".yellow-select").click(function () {
        $(this).toggleClass("open").find("ul").slideToggle();
        $(this).closest(".row").toggleClass("open");
        $(this).parent().parent().toggleClass("open");
    });

    $(".custom-dropdown").click(function () {
        $(this).find("ul").stop().slideToggle();
    });

    $("aside.left li.dropdown > a").click(function () {
        $(this).next().slideToggle();
        return false;
    });

    $(window).on("load", function(){
        var currentEl = $("aside.left").find(".active");
        currentEl.parent().parent().addClass("active").parent().parent().addClass("active");
    });

    /*
     * Campaign Mailing Material    
     */
    $('#campaign-material').on('shown.bs.modal', function () {

        setTimeout(function () {
            $('.select2-search__field').focus();

        }, 300);

    });
    $(document).on('click', '[data-action="material-delete"]', function (e) {
        console.log('del');
        $(this).closest('[data-item="material-item"]').remove();
    });


    $('[data-action="campaign-material-add"]').click(function () {

        var select = $('[data-item="material-article"]').val();
        var checkbox = [];
        $('[data-item="material-checkbox"]:checkbox:checked').each(function (i) {
            checkbox[i] = $(this).val();
        });


        if (Array.isArray(select) && Array.isArray(checkbox)) {
            var arraySelect = select.concat(checkbox);
        } else if (!Array.isArray(select) && Array.isArray(checkbox)) {
            var arraySelect = checkbox;
        } else {
            var arraySelect = select;
        }



        var template = $(this).data('template');

        /*
         * Default template
         * @article
         */
        if (template == "article" || !template) {
            var template = "article"
            var url = $("#homeDir").text()+'articles/article/get-by-id/';
        }

        arraySelect.forEach(function (element, index, array) {

            $.get(url, {id: element}, function (data, textStatus, jqXHR, id) {

            }).done(function (data) {


                $('[data-item="campaign-material-selected"]').append(templateMaterial(template, jQuery.parseJSON(data)));


                cl('? Wczytano materiał: ' + element);
                /*
                 * Clear value select2
                 */
                $(".ajax-load-articles").html('');

                $('[data-item="material-checkbox"]:checkbox').each(function () {
                    this.checked = false;
                });


            }).fail(function (data) {

                cl('? Błąd wczytywania sekcji: ' + element);
            });
        });



    });
    $(function () {
        $("#sortable").sortable({
            placeholder: "ui-state-highlight"
        });
        $("#sortable").disableSelection();
    });

    $('#myModal').on('shown.bs.modal', function () {      //correct here use 'shown.bs.modal' event which comes in bootstrap3
        console.log('fds');
        var link = $(this).data('link');
        var iframe = $(this).find('iframe').attr('src', link);
        iframe.attr('width', '535px');
        iframe.attr('height', '700px');
        iframe.css({border: 0});
    })

    $('[data-action="mailing-extend"]').click(function () {

        if ($(this).hasClass('btn-default')) {
            $(this).removeClass('btn-default');
            $(this).addClass('btn-primary');
           // $(this).text('Wersja rozszerzona');
        } else {

            $(this).removeClass('btn-primary');
            $(this).addClass('btn-default');
            //$(this).text('Wersja standardowa');
        }
        var checker = $('[data-item="hidden-checkbox"]');

        if (parseInt(checker.val()) == 1) {
            checker.removeAttr('value');
        } else {
            checker.val(1);
        }
        console.log(checker.val());

        $('.mailing-extend').toggle();
    });
});
function templateMaterial(template, data) {

    if (template == "article") {

        var html = '<li class="material-item ui-state-default" data-id="' + data.id + '" data-item="material-item"><div class="media"><div class="media-left"><img alt="64x64" class="media-object" data-src="holder.js/64x64" src="' + data.src + '" style="width: 64px; height: 64px;"></div><div class="media-body"> <h4 class="media-heading">' + data.title + '</h4><p>' + data.short_description + '</p></div><div class="media-right media-middle"> <span class="label label-danger" data-action="material-delete"><i class="fa fa-trash"></i> Usuń</span></div></div><input type="hidden" name="MailingCampaign[data][' + template + '][]" value="' + data.id + '"></li>';
    }

    return html;
}
function cl(data) {
    return console.log('%c ' + data, 'background: #fff;border:2px solid #b00100;border-radius:4px; color: #666; font-size:14px;padding: 1px;');
}
function asideCompact($event)
{
    var mainBody = $("body>main");
    var aside = $("aside.left");
    var main = $("section.main");
    var $compact = aside.hasClass('compact');

    if ($event) {
        mainBody.addClass('menu-action');
        setTimeout(function () {
            mainBody.removeClass('menu-action');
        }, 600);
    }
    if ($event && $compact) {
        aside.removeClass('compact');
        main.removeClass('big');
        saveToCookie(0, 3600);
    } else if ($event || $compact) {
        aside.addClass('compact');
        main.addClass('big');
        saveToCookie(1, 3600);
    }
}


function saveToCookie($val, $time) {
    var $url = $('#hide-menu-url').data('url');

    $.ajax({
        method: "POST",
        url: $url,
        data: {name: "cms-menu-compact", value: $val, time: $time}
    }).done(function (msg) {
        console.log(msg);
    });
}