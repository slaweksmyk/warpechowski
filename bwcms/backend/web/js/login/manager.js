/* 
 * File Manager Ajax
 * @author: Tomasz Załucki <tomek@fuleo.pl>
 * 2017 2017 2017 2017
 */

$("[data-action='manager-file']").click(function (e) {

  e.preventDefault();
        var id = $(this).data("slide");
        
      
        $("#manager-file").attr("data-slide", id);
        
        
        
    $("#manager-file [data-item='manager-body']").load($("#homeDir").text()+"files/", function () {
          console.log('id');
        // chnage category files
         changeCategory();
         
         // add img to slider option
         changeFileSlider();
    });


});


function changeCategory(){
  
$("[data-action='category-change']").click(function () {
    var id = $(this).data('category');
    
    console.log("zmieniam kategorię plików..");

     $("#manager-file [data-item='file-list']").load("/__cms/files/manager/files/?id="+id, function () {
        // load file list
          console.log("Kategoria zmieniona"+id);
    });
    

});  
}


function changeFileSlider(){

 $("[data-action='items-files']").dblclick(function () {


        var img = $(this);
        var srcImg = img.find('img').attr('src');
        var idImg = img.data('file');


        var id = $(".modal").data('slide');


        // Change src value on main image on slide
        $("[data-itemfiles='" + id + "']").attr('src', srcImg);
        //Change value in input thumbnail_id
        $("[data-key='" + id + "']").find("#slideritems-thumbnail_id").val(idImg);
        // Hidden modal
        $('#manager-file').modal('hide');
    });
 
 }
