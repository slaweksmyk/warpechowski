$(document).ready(function () {
    // Add datepicker -------------
    $(".datepicker").datetimepicker({
        dateFormat: "yy-mm-dd",
        timeFormat: 'HH:mm:ss'
    });
    $(".datepicker-today").datetimepicker({
        stepping: 30,
        dateFormat: "yy-mm-dd",
        timeFormat: 'HH:mm',
        minDate: 0,

    });
});