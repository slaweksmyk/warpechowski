<?php
/**
 * Language Controller
 * 
 * Supports operation on Language Module
 * 
 * PHP version 7
 *
 * @author Throk <k.borecki@throk.pl>
 * @copyright (c) 2016 Throk
 * @version 1.0
 */

namespace backend\controllers;

use Yii;

class LanguageController extends \yii\web\Controller
{
    /**
     * Change language Action - operating on GET
     *
     * @param String $lang ICU Locale tag
     * @return String Redirection URL
     */
    public function actionChange($lang)
    {
        $i18n_config = require(__DIR__."/../../common/config/i18n.php");
        if(in_array($lang, $i18n_config["languages"])){
            Yii::$app->session->set('backend_language', $lang);
        }
        
        return $this->redirect(Yii::$app->request->referrer); 
    }

}
