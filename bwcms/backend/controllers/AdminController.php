<?php
/**
 * Backend main controller
 * 
 * Deffault controller from admin panel
 * 
 * PHP version 7
 *
 * @author Volodymyr Shelelo <contact@profficode.com>
 * @copyright (c) 2016 Volodymyr Shelelo
 * @version 1.0
 */

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;

use backend\models\Favorites;
use common\modules\users\models\LoginForm;


/**
 * Admin controller
 * 
 * @author Volodymyr Shelelo
 */
class AdminController extends Controller
{
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

    
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [];
    }
    
    
    /**
     * User permission to controller action
     * 
     * @param string $permission
     * @return boolean
     */
    private function permissionsUser($permission)
    {
        if ( Yii::$app->user->can($permission) ) {
            return true;
        } else {
            return false;
        }
    }
    
    
    /**
     * Before load controller action
     * 
     * @param string $action
     * @return boolean
     */
    public function beforeAction($action)
    {
        if ( parent::beforeAction($action) ) {
            
            //permission for viewing the admin panel
            $adminPanelView = $this->permissionsUser('adminPanel');
            
            if( $action->actionMethod == 'actionLogin' && !$adminPanelView ){
                Yii::$app->user->logout();
            }
            elseif( !$adminPanelView ){
                Yii::$app->user->logout();
                return $this->redirect(Yii::$app->homeUrl.'/login/');
            }
            return true;
            
        } else {
            return false;
        }
    }
        
    
    //---------------------- ACTIONS -------------------------------------------
    
    
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if(Yii::$app->request->isAjax && !Yii::$app->request->isPost){
            $aGet = Yii::$app->request->get();
            Favorites::findOne($aGet["id"])->delete();
        }
        
        if(Yii::$app->request->isPost){
            $aPost = Yii::$app->request->post();
            
            if(isset($aPost["action"]) && $aPost["action"] == "addFav"){
                $oFavorites = Favorites::find()->where(["=", "url", $aPost["url"]])->andWhere(["=", "user_id", Yii::$app->user->id ? Yii::$app->user->id : null])->one();
                if(is_null($oFavorites)){
                    $oFavorites = new Favorites();
                    $oFavorites->user_id = Yii::$app->user->id;
                    $oFavorites->name = $aPost["name"];
                    $oFavorites->url = $aPost["url"];
                    $oFavorites->save();
                    
                    echo $oFavorites->id;
                } else {
                    echo "0";
                }
                exit;
            }
        }

        return $this->render('index');
    }

    
    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        $aBrute = Yii::$app->session->get('bruteforce', []);
       
        if(is_array($aBrute)){
            $aBruteTmp = [];
            foreach($aBrute as $iTime => $sIP){
                $iDiff = time() - $iTime;

                if($iDiff < 300){
                    $aBruteTmp[$iTime] = $sIP;
                }
            }
            $aBrute = $aBruteTmp;
        }
        
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if(count($aBrute) >= 5){
            echo "Blokada dostępu. <br/>Proszę spróbować ponownie za 5min.";
            exit;
        }
        
        $this->layout = 'login';
        $model = new LoginForm();
        
        
         
        if(Yii::$app->request->isPost){
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                Yii::$app->session->remove('bruteforce');
                return $this->goBack();
            } else {
                $aBrute[time()] = Yii::$app->getRequest()->getUserIP();
                Yii::$app->session->set('bruteforce', $aBrute); 
            }
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }
    
    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        
        return $this->goHome();
    }
    
    
    /**
     * Error page action
     * 
     * @return string
     */
    public function actionError()
    {
        $exception = \Yii::$app->errorHandler->exception;
        
        if ($exception !== null) {
            
            $statusCode = $exception->statusCode;
            $name = $exception->getName();
            $message = $exception->getMessage();

            return $this->render('error', [
                'exception' => $exception,
                'statusCode' => $statusCode,
                'name' => $name,
                'message' => $message
            ]);
        }
    }
    
}