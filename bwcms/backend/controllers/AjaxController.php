<?php
/**
 * Ajax backend controller
 * 
 * PHP version 7
 *
 * @author Volodymyr Shelelo <contact@profficode.com>
 * @copyright (c) 2016 Volodymyr Shelelo
 * @version 1.0
 */

namespace backend\controllers;

use yii\filters\VerbFilter;

class AjaxController extends \yii\web\Controller
{
    
    /**
     * init ajax controller
     */
    public function init() {
        \Yii::$app->errorHandler->errorAction = 'ajax/error';
    }
    
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'setcookies' => ['post'],
                ],
            ],
        ];
    }

    
    /**
     * Set Cookies before aJax
     * Params POST: 'name', 'value', 'time'
     * 
     * @return string
     */
    public function actionSetcookies()
    {
        $cookies = \Yii::$app->response->cookies;
        $name   = \Yii::$app->request->post('name');
        $value  = \Yii::$app->request->post('value');
        $time   = \Yii::$app->request->post('time');

        if($name !== null && $value !== null && $time !== null){
            if( $cookies->get($name) ) { $cookies->remove($name); } 
            
            $cookies->add(new \yii\web\Cookie([
                'name' => $name,
                'value' => $value,
                'expire' => time() + intval($time),
            ]));
            
            return 'save cookies';
        } else {
            return 'no all params';
        }
    }
    
    
    /**
     * Error action
     * 
     * @return string
     */
    public function actionError()
    {
        $exception = \Yii::$app->errorHandler->exception;
        
        if ($exception !== null) {
            
            //$statusCode = $exception->statusCode;
            $name = $exception->getName();
            $message = $exception->getMessage();

            echo $name.'<br>';
            echo $message.'<br>';
        }
    }
    
}
