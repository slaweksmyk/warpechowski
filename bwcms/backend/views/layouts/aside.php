<?php 
use yii\widgets\Menu;
use common\modules\pages\models\PagesSite;
use common\modules\pages\models\PagesAdmin;
use common\helpers\FileCacheHelper;

$controller = \Yii::$app->controller->id;

$curent_url = "";
if( $controller === 'admin' ){ $curent_url = '/'; }
else { $curent_url = str_replace(\Yii::$app->request->baseUrl, "", $_SERVER["REQUEST_URI"]); }

?>

<header>
    <div class="wrapper">
        <div class="top">
            <img src="<?= \Yii::$app->request->baseUrl ?>images/ico/menu-ico.png" alt="menu" id="menu-toggle"/>
            <a data-toggle="tooltip" data-placement="bottom" title="Wróć do strony głównej" target="_blaank" data-toggle="tooltip" data-placement="bottom" href="<?php echo $curent_url[0]; ?>"><img src="<?= \Yii::$app->request->baseUrl ?>images/ico/house-ico.png" alt="house" class="go_to_page"></a>
            <h2>Black Wolf 4.00</h2>
            <p>content management system</p>
        </div>
        <div class="bottom">
            <a data-toggle="tooltip" data-placement="bottom" title="Wróć do strony głównej" target="_blank" href="<?php echo $curent_url[0]; ?>"><img src="<?= \Yii::$app->request->baseUrl ?>images/bw-logo.png" alt="thumbnail" class="thumb" /></a>
            <div class="alert-ico" style="display: none;">
                <div class="no">0</div>
            </div>
        </div>
        <div id="hide-menu-url" data-url="<?php echo \Yii::$app->request->baseUrl; ?>/admin-ajax/setcookies/" ></div>
    </div>
</header>

<section class="main-menu">
    <?php
        function getItems($parentID = NULL){
            $curent_url = "";
            if( \Yii::$app->controller->id === 'admin' ){ $curent_url = '/'; }
            else { $curent_url = str_replace(\Yii::$app->request->baseUrl, "", $_SERVER["REQUEST_URI"]); }

            if(is_null($parentID)){
                $oPagesRowset = PagesAdmin::find()->where(["IS", "parent_id", NULL])->andWhere(["=", "is_active", 1])->orderBy("sort ASC")->all();
            } else {
                $oPagesRowset = PagesAdmin::find()->where(["=", "parent_id", $parentID])->andWhere(["=", "is_active", 1])->orderBy("sort ASC")->all();
            }
            
            $aCurrItems = [];
            foreach($oPagesRowset as $oPage){
                if(is_null($parentID)){
                    $aCurrItems[$oPage->id]["template"] = '<a href="{url}">{label} <img src='.\Yii::$app->request->baseUrl.'images/ico/'.$oPage->ico_uri.' alt="ico"/></a>';
                    $aCurrItems[$oPage->id]["activateParents"] = true;
                    $aCurrItems[$oPage->id]["activeCssClass"]  = "active";
                }
                
                $aCurrItems[$oPage->id]["label"] = $oPage->name;

                if($oPage->id == 5) {
                    $aChildren = [];
                    
                    foreach(PagesSite::find()->all() as $oFrontPage){
                        $aChildren[$oFrontPage->id]["label"] = $oFrontPage->name;
                        $aChildren[$oFrontPage->id]["url"] = \Yii::$app->request->baseUrl."pages-site/page/update/?id={$oFrontPage->id}";
                        $aChildren[$oFrontPage->id]["active"] = $curent_url == "pages-site/page/update/?id={$oFrontPage->id}";
                    }
                } else {
                    $aChildren = getItems($oPage->id);
                }
                
                if(count($aChildren) > 0){
                    $aCurrItems[$oPage->id]["url"] = "#";
                    $aCurrItems[$oPage->id]["options"] = ['class'=>'dropdown'];
                    $aCurrItems[$oPage->id]["items"] = $aChildren;
                } else {
                    $aCurrItems[$oPage->id]["active"] = $curent_url == "{$oPage->url}/";
                    
                    if($oPage->url == "/"){
                        $aCurrItems[$oPage->id]["url"] = \Yii::$app->request->baseUrl;
                    } else {
                        if($oPage->id == 79){
                            $aCurrItems[$oPage->id]["url"] = \Yii::$app->request->baseUrl."{$oPage->url}";
                        } else {
                           $aCurrItems[$oPage->id]["url"] = \Yii::$app->request->baseUrl."{$oPage->url}/"; 
                        }
                    }

                    if($oPage->getModule()->one()){
                        $sModuleName = strtolower($oPage->getModule()->one()->name);
                        $aCurrItems[$oPage->id]["visible"] = Yii::$app->user->can("{$sModuleName}_view") && $oPage->isModuleEnabled();
                    }
                }
                
                if(!$oPage->is_active){
                    $aCurrItems[$oPage->id]["visible"] = false;
                }

                $aCurrItems[$oPage->id]["parent_id"] = $oPage->parent_id;
                $aCurrItems[$oPage->id]["items_no"]  = count($aChildren);
            }
            
            return $aCurrItems;
        }

        $hashName = md5(strtolower(Yii::$app->request->absoluteUrl));
        $result = FileCacheHelper::getFromCache($hashName);
        if (!$result) {
            $result = getItems();
            
            $result = json_encode($result);
            if ($result != "") {
                FileCacheHelper::saveToCache($hashName, $result);
            }
            $result = json_decode($result, true);
        } else {
            $result = json_decode($result, true);
        }
        
        echo Menu::widget([
            'items' => $result
        ]);
    ?>
</section>