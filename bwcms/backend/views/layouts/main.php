<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use backend\models\Favorites;
use common\components\LogPanel;

if(Yii::$app->user->isGuest){ return; }

AppAsset::register($this);

if( ($cookie = \Yii::$app->request->cookies->get('cms-menu-compact')) !== null ) { $compactMenu = intval($cookie->value); } 
else { $compactMenu = 0; }
?>

<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language; ?>">
    <head>
        <meta charset="<?php echo Yii::$app->charset; ?>">
         <link rel="shortcut icon" href="<?= \Yii::$app->request->baseUrl ?>favico.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php echo Html::csrfMetaTags(); ?>
        <title><?php echo strip_tags( $this->title ); if($this->title){ echo ' - '; } echo Yii::$app->name; ?></title>
        <?php echo $this->head(); ?>
    </head>
    <body>
    <?php $this->beginBody(); ?>
        <div id="homeDir" style="display: none;"><?= \Yii::$app->request->baseUrl ?></div>
        <!-- Blank modal -->
        <div class="modal fade" id="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Modal title</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body"></div>
                </div>
            </div>
        </div>

        <main>
            <aside class="left <?php if($compactMenu){ echo 'compact'; } ?>" >
                <div class="aside-bg" ></div>
                <?php echo $this->render('aside'); ?>
            </aside>
            <section class="main <?php if($compactMenu){ echo 'big'; } ?>" >
                <aside class="top">
                    <div class="row">
                        <div class="col-6">
                            <ul class="top-menu">
                                <li>
                                    Ulubione moduły
                                    <span class="caret"></span>
                                    <ul class="favorite">
                                        <?php foreach(Favorites::find()->where(["=", "user_id", Yii::$app->user->identity->id ? Yii::$app->user->identity->id : null])->all() as $oFavorite){ ?>
                                            <li>
                                                <a href="#" class="remove-favorite" data-id="<?= $oFavorite->id ?>" title="Usuń moduł z listy"><span class="glyphicon glyphicon-trash"></span></a>
                                                <a href="<?= $oFavorite->url ?>"><?= $oFavorite->name ?></a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="col-6">
                            <a href="<?= \Yii::$app->request->baseUrl ?>logout/" class="logout btn btn-danger btn-sm"><?= Yii::t('backend', 'logout') ?></a>
                            <div class="custom-dropdown pull-right" style="cursor: default;">
                                <img src="<?= \Yii::$app->request->baseUrl ?>images/ico/cd-login.png" alt="house"/> 
                                <?php if (!Yii::$app->user->isGuest) { echo Yii::$app->user->identity->username; } ?>
                            </div>
                            <div class="custom-dropdown pull-right">
                                <?php echo Yii::t('backend', Yii::$app->language); ?>
                                <span class="caret"></span>
                                <ul>
                                    <li><a href="<?= \Yii::$app->getUrlManager()->createUrl('language/change') ?>?lang=pl-PL"><?= Yii::t('backend', 'lang_polish') ?></a></li>
                                    <li><a href="<?= \Yii::$app->getUrlManager()->createUrl('language/change') ?>?lang=en-US"><?= Yii::t('backend', 'lang_english') ?></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </aside>
                
                <div class="wrapper">
                    <section class="title-area">
                        <h1 id="m_title"><?php if($this->title){ echo $this->title; } else { echo Yii::t('backend', 'home_title'); } ?> <a href="#" title="przypnij moduł"></a></h1>
                    </section>
                    <?php echo $content; ?>
                    <?= LogPanel::displayPanel($this->context->module->id); ?>
                </div>
            </section>
        </main>
    <?php $this->endBody(); ?>
    </body>
</html>
<?php $this->endPage(); ?>
