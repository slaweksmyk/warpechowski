<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-wrap">
    <div class="login-box">
        <div class="title"><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> Zaloguj się do Panelu BLACK WOLF 4.10</div>
        <div class="cnt">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

            <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'placeholder' => "login", 'autocomplete' => 'off'])->label(false) ?>

            <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'hasło'])->label(false) ?>

<?= $form->field($model, 'rememberMe')->checkbox()->label("Zapamiętaj mnie") ?>

            <div class="form-group">
<?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>

<?php ActiveForm::end(); ?>
            <p class="reminder"></p>
        </div>
    </div>

    <div class="info-box">

    </div>
</div>