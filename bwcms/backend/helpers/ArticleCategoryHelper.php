<?php 
namespace backend\helpers;

use common\modules\categories\models\ArticlesCategories;

class ArticleCategoryHelper {
    
    public static function generateCategoryTree($iParentID = NULL, $addSymbol = "") {
        if(is_null($iParentID)){ $sOperator = "IS"; $l = ""; } else { $sOperator = "="; $l = "└"; }
        $oCategoryRowset = ArticlesCategories::find()->where([$sOperator, "parent_id", $iParentID])->all();
        $s = "".$addSymbol;
        
        $aElements = [];
        foreach($oCategoryRowset as $oCategory){
            if($oCategory->getCategories()->all()){
                $aElements[$oCategory->id] = "{$s}{$l} {$oCategory->name}";
                $aElements = array_merge($aElements, self::generateCategoryTree($oCategory->id, "{$s}&nbsp;&nbsp;"));
            } else {
                $aElements[$oCategory->id] = "{$s}{$l} {$oCategory->name}";
            }
        }
        
        return $aElements;
    }
    
    private static $level = 0;
    public static function generateCategoryTreeAsTable($iParentID = NULL, $addSymbol = "", $bgColor = "#ffffff") {
        $addClass = "";
        $s = "".$addSymbol;
        
        if(is_null($iParentID)){ $sOperator = "IS"; } else { $sOperator = "="; $l = ""; }
        $oCategoryRowset = ArticlesCategories::find()->where([$sOperator, "parent_id", $iParentID])->all();

        if(self::$level == 0){
            $sReturnHtml = "<ul class='dropdown-menu'>"; 
        } else { 
            $sReturnHtml = ""; 
            $addClass = "li-hidden";
        }
        foreach($oCategoryRowset as $oCategory){
            if($oCategory->getCategories()->all()){
                self::$level++;

                $sReturnHtml .= "<li style='background: {$bgColor}' class='has-child {$addClass}' data-parent='{$oCategory->parent_id}' data-id='{$oCategory->id}'>";
                    $sReturnHtml .= "<a href='#'>{$s}{$l} {$oCategory->name}<span class='article-toggle-ico'>+</span></a>";
                    $sReturnHtml .= self::generateCategoryTreeAsTable($oCategory->id, "{$s}&nbsp;&nbsp;&nbsp;", ($bgColor == "#ffffff")? "#eaeaea" : "#ffffff");
                $sReturnHtml .= "</li>";
            } else {
                $sReturnHtml .= "<li style='background: {$bgColor}' class='{$addClass}' data-parent='{$oCategory->parent_id}'>";
                    $sReturnHtml .= "<a href='".\Yii::$app->request->baseUrl."articles/article/articles/?id={$oCategory->id}'>{$s}{$l} {$oCategory->name}</a>";
                $sReturnHtml .= "</li>";
            }
        }
        if(self::$level == 0){ $sReturnHtml .= "</ul>"; }
        
        return $sReturnHtml;
    }
    
    
}