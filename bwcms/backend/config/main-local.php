<?php
$config = [
    'components' => [
        'request' => [
            'cookieValidationKey' => 'wsWtGFb-9koccaKEqMj0_UuHXOtz2R78',
        ],
    ],
];

if (!YII_ENV_TEST) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['185.102.191.90']
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'common\hooks\yii2\gii\Module',
        'allowedIPs' => ['185.102.191.90']
    ];
}

return $config;
