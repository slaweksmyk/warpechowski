<?php

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php')
);
return [
    'id' => 'app-backend',
    'name' => 'BlackWolf 4.0',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log', 'languageSetter', 'dataTableSorter'],
    'modules' => [],
    'homeUrl' => '/__fBFyVd/',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
            'baseUrl' => '/__fBFyVd/',
            'class' => 'common\hooks\yii2\base\Request'
        ],
        'formatter' => [
            'dateFormat' => 'dd.MM.yyyy',
            'decimalSeparator' => ',',
            'thousandSeparator' => '',
            'numberFormatterOptions' => [
            ]
        ],
        'user' => [
            'identityClass' => 'common\modules\users\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            'name' => 'bwcms_admin',
            'cookieParams' => [
                'httpOnly' => true,
                //'secure' => true,
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'admin/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'baseUrl' => '/__fBFyVd/',
            'suffix' => '/',
            'rules' => [
                '' => 'admin/index',
                'index' => 'admin/index',
                'login' => 'admin/login',
                'logout' => 'admin/logout',
                'admin-ajax/<action:\w+>' => 'ajax/<action>',
            ],
        ],
        'languageSetter' => [
            'class' => 'backend\components\LanguageSetter',
        ],
        'dataTableSorter' => [
            'class' => 'backend\components\DataTableSorter',
        ],
    ],
    'params' => $params,
    'modules' => [
     //   'gii' => ['class' => 'yii\gii\Module', 'allowedIPs' => ['185.102.191.90'],
    ]
];
