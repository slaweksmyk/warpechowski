<?php

namespace backend\assets;

use yii\web\AssetBundle;

class AdminAsset extends AssetBundle
{
    public $sourcePath = '@backend/assets';
    public $depends = [
        'backend\assets\AppAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}
