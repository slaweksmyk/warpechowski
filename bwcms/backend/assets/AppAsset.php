<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        // Library CSS
        'library/jquery-ui-1.12.0/jquery-ui.min.css',
        'library/jquery-ui-1.12.0/external/timepicker/src/jquery-ui-timepicker-addon.css',
        'library/select2/css/select2.min.css',
        // CSS
        'css/glyphicons.css',
        'css/main.css',
        'css/login.css',
        'css/manager.css',
        'css/daterangepicker.min.css',
    ];
    public $js = [
        // Library JS
        'library/jquery-cookie/jquery.cookie.js',
        'library/jquery-ui-1.12.0/jquery-ui.min.js',
        'library/jquery-ui-1.12.0/external/timepicker/src/jquery-ui-timepicker-addon.js',
        'library/tinymce/tinymce.min.js',
        'library/select2/js/select2.full.min.js',
        // JS
        'js/datapicker_init.js',
        'js/tinymce_init.js',
        'js/login/login.js',
        'js/login/manager.js',
        'js/moment-with-locales.js',
        'js/jquery.daterangepicker.min.js',
        'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
        'yii\bootstrap4\BootstrapPluginAsset'
    ];
}
