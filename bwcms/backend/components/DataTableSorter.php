<?php

/**
 * Language Setter
 * 
 * Initialize choosed website language. Uses session for data-storage
 * 
 * PHP version 7
 *
 * @author Throk <k.borecki@throk.pl>
 * @copyright (c) 2016 Throk
 * @version 1.0
 */
namespace backend\components;
 
use Yii;
use yii\base\Component;
 
class DataTableSorter extends Component
{
    /**
     * The constructor.
     */
    public function init(){
        parent::init();
        
        $aUrl = parse_url($_SERVER['REQUEST_URI']);
        if(
            isset($aUrl["path"]) && 
            Yii::$app->request->cookies->getValue($aUrl["path"]) && 
            !Yii::$app->request->get('sort')
        ){
            if(!empty($_SERVER['QUERY_STRING'])){
                Yii::$app->response->redirect($aUrl["path"]."?".$_SERVER['QUERY_STRING']."&sort=".Yii::$app->request->cookies->getValue($aUrl["path"]))->send();
            } else {
                Yii::$app->response->redirect($aUrl["path"]."?&sort=".Yii::$app->request->cookies->getValue($aUrl["path"]))->send();
            } 
        }
        
        if(isset($aUrl["path"]) && Yii::$app->request->get('sort')){
            Yii::$app->response->cookies->add(new \yii\web\Cookie([
                'name' => $aUrl["path"],
                'value' => Yii::$app->request->get('sort'),
                'expire' => time() + 60 * 60 * 24 * 180
            ]));
        }
        
    }
}