<?php

/**
 * Language Setter
 * 
 * Initialize choosed website language. Uses session for data-storage
 * 
 * PHP version 7
 *
 * @author Throk <k.borecki@throk.pl>
 * @copyright (c) 2016 Throk
 * @version 1.0
 */
namespace backend\components;
 
use Yii;
use yii\base\Component;
 
class LanguageSetter extends Component
{
    /**
     * The constructor.
     * Is setting language into default Yii language app
     */
    public function init(){
        parent::init();

        if(Yii::$app->session->get('backend_language')){
            \Yii::$app->language = Yii::$app->session->get('backend_language');
        }
    }
}