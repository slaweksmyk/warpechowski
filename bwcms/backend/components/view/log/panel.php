<?php
use common\hooks\yii2\grid\GridView;
use common\modules\users\models\User;
?>

<section class="panel full" >
    <header><img src="<?= \Yii::$app->request->baseUrl ?>images/login/logo-gray.png" alt="logo"/> <?= Yii::t('backend', 'module_logs') ?></header>
    <div class="panel-body" >
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'attribute' => 'user_id',
                    'value' => function ($data) {
                        return User::findOne($data["user_id"])->username;
                    }
                ],
                [
                    'attribute' => 'action',
                    'value' => function ($data) {
                        return Yii::t('backend', $data["action"]);
                    }
                ],
                'value',
                [
                    'attribute' => 'date',
                    'value' => function ($data) {
                        return date("d.m.Y H:i:s", strtotime($data["date"]));
                    }
                ],
                [
                    'value' => function ($data) {
                        return "";
                    }
                ], 
            ],
        ]); ?>
    </div>
</section>